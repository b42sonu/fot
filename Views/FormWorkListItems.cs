﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormWorkListItems.
    /// </summary>
    public class FormDataWorkListItems : BaseFormData
    {
        public List<WorkListItem> WorkListItems { get; set; }
    }

    public partial class FormWorkListItems : BaseForm
    {

        #region Private, Readonly and const variables
        private FormDataWorkListItems _formData;
        private List<WorkListItem> _workListItems;
        private WorkListItem _workListItem;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        
        #endregion

        /// <summary>
        /// Acts as default constructor for the form.
        /// </summary>
        public FormWorkListItems()
        {
            InitializeComponent();
            SetStandardControlProperties(labelModuleName, lblOrgUnitName , null, null, null, null, null);
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            AddButtons();
        }

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormWorkListItems.AddButtons()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Continue, BtnContinueClick, TabButtonType.Confirm) { Name = ButtonOk });
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormWorkListItems.AddButtons");
            }
        }

        private void SetTextToGui()
        {
            if (ModelUser.UserProfile.OrgUnitName != null) lblOrgUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblHeading.Text = GlobalTexts.SelectAddCarrier;
            labelModuleName.Text = GlobalTexts.WorkList;
        }

        #region Methods and Functions

        

        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnUpdateView(IFormData formData)
        {
            if (formData != null)
            {
                _formData = (FormDataWorkListItems)formData;
                _workListItems = _formData.WorkListItems;
            }
            BindWorkList();
        }
        
        /// <summary>
        /// This method deselects all rows in listOperations, if any rows are there.
        /// </summary>
        private void UnSelectOtherRows()
        {
            if (listCarriers == null || listCarriers.DataRows.Count == 0) return;
            for (var i = 0; i <= listCarriers.DataRows.Count - 1; i++)
            {
                var row = listCarriers.DataRows[i];
                row.Selected = false;
            }

        }

        /// <summary>
        /// This method binds the list of carriers to listCarriers advance list.
        /// </summary>
        private void BindWorkList()
        {
            listCarriers.BeginUpdate();
            listCarriers.DataSource = _workListItems;
            listCarriers.EndUpdate();
        }


       


        /// <summary>
        /// This method executes every time, we select a row in listCarriers. Ans performs following tasks.
        /// 1) Deselect all rows
        /// 2) Selects the clicked row
        /// 3) Get values for loadCarrier, Operation Type (loading/unloading) and stopId/tripId from selected row
        /// so that it can be used later on.
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSelectedWorkListItem(RowEventArgs e)
        {
            
            //_workListItem = (WorkListItem) e.DataRow[e.RowIndex];
            _workListItem = _workListItems[e.RowIndex];
            UnSelectOtherRows();
            e.DataRow.Selected = true;
            
        }

        #endregion

        #region Events

        
        /// <summary>
        /// This method acts as handler for click event of continue button. It does the following:
        /// 1) Sends the message to flow that, user clicked continue button and passes the required data, so that flow
        /// can handle this event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnContinueClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(WorkListItemsEvent.Continue, _workListItem);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormWorkListItems.BtnContinueClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of cancel button and send the message to flow that, user clicked
        /// on cancel button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(WorkListItemsEvent.CancelWorkList);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormWorkListItems.BtnBackClick");                
            }
        }

        /// <summary>
        /// This method acts as handler for RowSelect event for advance list listCarriers. it process the selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListCarriersRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                switch (e.DataRow.CurrentTemplateIndex)
                {
                    case 1: // The master row has to be collapsed
                        _multiButtonControl.SetButtonEnabledState(ButtonOk, false);
                        break;
                    case 2: // The master row has to be expanded  
                        ProcessSelectedWorkListItem(e);
                        _multiButtonControl.SetButtonEnabledState(ButtonOk, true);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.ListOperationRowSelect");
            }
        }

        #endregion

    }


    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectCarrier.
    /// </summary>
    public class WorkListItemsEvent : ViewEvent
    {
        public const int CancelWorkList = 0;
        public const int Continue = 2;


        public static implicit operator WorkListItemsEvent(int viewEvent)
        {
            return new WorkListItemsEvent { Value = viewEvent };
        }
    }
}
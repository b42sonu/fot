﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    public partial class FormMessage : BaseForm
    {
        private int _returnState;
        public FormMessage()
        {
            InitializeComponent();
            SetTextToGui();
        }

        private void SetTextToGui()
        {
            buttonBack.Text = GlobalTexts.Back;
        }

        public MessageHolder Message
        {
            set
            {
                lblMessage.Text = value.Text;
            }
        }

        public override void OnShow(IFormData iformData)
        {
            if (iformData != null)
            {
                _returnState = ((FormDataMessage)iformData).ReturnState;
            }
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewEvent != null)
                {
                    ViewEvent.Invoke(_returnState, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessage.ButtonBackClick");
            }
        }
    }

    public class FormDataMessage : BaseFormData
    {
        public int ReturnState;
    }
}
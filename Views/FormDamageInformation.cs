﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    public partial class FormDamageInformation : BaseForm
    {
        #region Private Variables and Properties
        public FormDamageInformation()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
        }
        #endregion

        #region Methods and Funcions

        public override void UpdateView(params Object[] data)
        {
            if (data[0] is MessageHolder)
            {
                MessageHolder messageHolder = (MessageHolder)data[0];
                if (messageHolder.State == MessageState.ModalMessage)
                    ShowModalMessage(messageHolder.Text);
            }
            else if (data[0] is DamageInformationData)
            {
                DamageInformationData damageInformationData = (DamageInformationData)data[0];
                PopulateComboBoxes(damageInformationData);
            }
        }

        private static void ShowModalMessage(String message)
        {
            ModalMessageBoxArgument msgArg = new ModalMessageBoxArgument("", message,
                                                                         new ModalMessageBoxButton("OK"));
            ModalMessageBox.Show(msgArg);
        }

        private void PopulateComboBoxes(DamageInformationData damageInformationData)
        {
            foreach (var eventType in damageInformationData.EventTypeList)
                cmbEvent.Items.Add(eventType);
            foreach (var reasonType in damageInformationData.ReasonTypeList)
                cmbReason.Items.Add(reasonType);
            foreach (var actionType in damageInformationData.ActionTypeList)
                cmbAction.Items.Add(actionType);

            cmbEvent.SelectedItem = damageInformationData.EventTypeList[0];
            cmbReason.SelectedItem = damageInformationData.ReasonTypeList[0];
            cmbAction.SelectedItem = damageInformationData.ActionTypeList[0];
        }



        #endregion

        #region Events

        private void ButtonOkClick(object sender, EventArgs e)
        {
            EnteredDamageInformation enteredDamageInformation = new EnteredDamageInformation();
            enteredDamageInformation.ActionType = (ActionType)cmbAction.SelectedItem;
            enteredDamageInformation.ReasonType = (ReasonType)cmbReason.SelectedItem;
            enteredDamageInformation.EventType = (EventType)cmbEvent.SelectedItem;
            enteredDamageInformation.Comment = txtComments.Text;
            ViewEvent.Invoke(DamageInfoViewEvents.Ok, enteredDamageInformation);
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            ViewEvent.Invoke(DamageInfoViewEvents.Back);
        }


        public class DamageInfoViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
        }
        #endregion

        #region View specific classes
        public struct EnteredDamageInformation
        {
            public EventType EventType { get; set; }
            public ActionType ActionType { get; set; }
            public ReasonType ReasonType { get; set; }
            public String Comment { get; set; }
        }

        public struct DamageInformationData
        {
            public List<EventType> EventTypeList { get; set; }
            public List<ActionType> ActionTypeList { get; set; }
            public List<ReasonType> ReasonTypeList { get; set; }
        }
        #endregion
    }
}
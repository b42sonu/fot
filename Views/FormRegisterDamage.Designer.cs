﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Goods.Views
{
    partial class FormRegisterDamage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGoodsMenu));
            this.pnlButtons=new Panel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.linkLabelBack = new System.Windows.Forms.LinkLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            

            

            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.pnlButtons);
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 519, 464, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(464, 0, 16, 519);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            this.touchPanel.BackgroundImage = GuiCommon.Instance.BackgroundImage;


            // 
            // linkLabelBack
            // 
            this.linkLabelBack.BackColor = System.Drawing.Color.Black;
            this.linkLabelBack.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.linkLabelBack.ForeColor = System.Drawing.Color.White;
            this.linkLabelBack.Location = new System.Drawing.Point(22, 15);
            this.linkLabelBack.Name = "linkLabelBack";
            this.linkLabelBack.Size = new System.Drawing.Size(69, 41);
            this.linkLabelBack.TabIndex = 0;
            this.linkLabelBack.Text = "Back";
            this.linkLabelBack.Click += new System.EventHandler(this.ButtonBackClick);

            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(201, 5);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(79, 27);
            this.labelModuleName.Text = "Register Damage";

            this.pnlButtons.BackColor = System.Drawing.Color.Black;
            this.pnlButtons.Location = new System.Drawing.Point(0, 475);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(480, 60);
            this.pnlButtons.Controls.Add(this.linkLabelBack);
            // 
            // FormCorrectionMenu
            // 

            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormRegisterDamage";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private System.Windows.Forms.Panel pnlButtons;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private System.Windows.Forms.LinkLabel linkLabelBack;
    }
}
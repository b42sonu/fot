﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    partial class FormDeliveryInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();

            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsignmentItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentNumber = new Resco.Controls.CommonControls.TransparentLabel();
            //this.lblConsignorHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeDetails = new Resco.Controls.CommonControls.TransparentLabel();
            this.listConsignmentItems = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellConsignmentNumber = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellAlternateConsignmentNumber = new Resco.Controls.AdvancedList.TextCell();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();


            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.lblConsignorDetails)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.lblConsignorHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeDetails)).BeginInit();
            this.SuspendLayout();
            
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.lblConsignmentItems);
            //this.touchPanel.Controls.Add(this.lblConsignorDetails);
            //this.touchPanel.Controls.Add(this.lblConsignorHeading);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblConsigneeHeading);
            this.touchPanel.Controls.Add(this.lblConsigneeDetails);
            this.touchPanel.Controls.Add(this.listConsignmentItems);
            this.touchPanel.Controls.Add(this.lblConsignmentNumber);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);


            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            //lblConsignmentNumber
            //
            this.lblConsignmentNumber.AutoSize = false;
            this.lblConsignmentNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsignmentNumber.Location = new System.Drawing.Point(4, 65);
            this.lblConsignmentNumber.Name = "lblConsignmentNumber";
            this.lblConsignmentNumber.Size = new System.Drawing.Size(300, 30);
            
            
            // 
            // lblConsignmentItems
            // 
            this.lblConsignmentItems.AutoSize = false;
            this.lblConsignmentItems.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsignmentItems.Location = new System.Drawing.Point(4, 95);
            this.lblConsignmentItems.Name = "lblConsignmentItems";
            this.lblConsignmentItems.Size = new System.Drawing.Size(112, 30);
            

            


            // 
            // lblConsigneeHeading
            // 
            this.lblConsigneeHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsigneeHeading.Location = new System.Drawing.Point(4, 290);
            this.lblConsigneeHeading.Name = "lblConsigneeHeading";
            this.lblConsigneeHeading.Size = new System.Drawing.Size(66, 15);

            // 
            // lblConsigneeDetails
            // 
            this.lblConsigneeDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeDetails.Location = new System.Drawing.Point(4, 315);
            this.lblConsigneeDetails.Name = "lblConsigneeDetails";
            this.lblConsigneeDetails.Size = new System.Drawing.Size(0, 45);
            this.lblConsigneeDetails.Text = "";
            // 
            // listConsignmentItems
            // 
            this.listConsignmentItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listConsignmentItems.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listConsignmentItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listConsignmentItems.DataRows.Clear();
            this.listConsignmentItems.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listConsignmentItems.Location = new System.Drawing.Point(0, 125);
            this.listConsignmentItems.MultiSelect = true;
            this.listConsignmentItems.Name = "listConsignmentItems";
            this.listConsignmentItems.ScrollbarSmallChange = 32;
            this.listConsignmentItems.ScrollbarWidth = 26;
            this.listConsignmentItems.SelectedTemplateIndex = 2;
            this.listConsignmentItems.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectOnly;//SelectDeselect;
            this.listConsignmentItems.ShowHeader = true;
            this.listConsignmentItems.Size = new System.Drawing.Size(480, 160);
            this.listConsignmentItems.TabIndex = 0;
            this.listConsignmentItems.TemplateIndex = 1;
            this.listConsignmentItems.Templates.Add(this.RowTemplateHeader);
            this.listConsignmentItems.Templates.Add(this.RowTemplatePlannedOp);
            this.listConsignmentItems.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.listConsignmentItems.TouchScrolling = true;
            this.listConsignmentItems.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListConsignmentItemsRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation Details";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellConsignmentNumber);
            this.RowTemplatePlannedOp.Height = 40;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // textCellConsignmentNumber
            // 
            this.textCellConsignmentNumber.CellSource.ColumnName = "ConsignmentItemNumber";
            this.textCellConsignmentNumber.DesignName = "textCellConsignmentNumber";
            this.textCellConsignmentNumber.Location = new System.Drawing.Point(15, 0);
            this.textCellConsignmentNumber.Size = new System.Drawing.Size(370, 40);
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateConsignmentNumber);
            this.RowTemplatePlannedOpAlternateTemp.Height = 40;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // textCellAlternateConsignmentNumber
            // 
            this.textCellAlternateConsignmentNumber.CellSource.ColumnName = "ConsignmentItemNumber";
            this.textCellAlternateConsignmentNumber.DesignName = "textCellAlternateConsignmentNumber";
            this.textCellAlternateConsignmentNumber.Location = new System.Drawing.Point(15, 0);
            this.textCellAlternateConsignmentNumber.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateConsignmentNumber.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateConsignmentNumber.Size = new System.Drawing.Size(370, 40);
            // 
            // FormDeliveryInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormDeliveryInfo";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion


        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listConsignmentItems;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeDetails;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentNumber;

        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;

        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;

        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.TextCell textCellConsignmentNumber;

        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateConsignmentNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItems;
    }
}
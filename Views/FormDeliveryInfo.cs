﻿using System;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using System.Text;
using System.Drawing;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    /// <summary>
    /// This class acts as initial data to screen FormDataDeliveryInfo.
    /// </summary>
    public class FormDataDeliveryInfo : BaseFormData
    {
        public PlannedConsignmentsType Consignment { get; set; }
        public string SelectedTabName { get; set; }
        public GetGoodsListResponseOperationLoadCarrier SelectedLoadCarrier { get; set; }
        public DeliveryOperationType SelectedOperationType { get; set; }
        public bool IsStaredFromScan { get; set; }
    }

    /// <summary>
    /// This class shows the details of delivery type operation.
    /// </summary>
    public partial class FormDeliveryInfo : BaseForm
    {
        #region Private, readonly and constant variables

        private PlannedConsignmentsType _selectedConsignment;
        private string _selectedTabName;
        private GetGoodsListResponseOperationLoadCarrier _selectedLoadCarrier;
        private DeliveryOperationType _selectedOperationType;
        private FormDataDeliveryInfo _formData;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();
        private const string ButtonBackName = "ButtonBack";
        private const string ButtonScanName = "ButtonScan";
        private readonly DetailsPanel _detailsPanel;
        #endregion

        #region Methods and Functions

        /// <summary>
        /// Default constructor for form.
        /// </summary>
        public FormDeliveryInfo()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToUi();
            _detailsPanel = new DetailsPanel { Visible = false };
            touchPanel.Controls.Add(_detailsPanel);
        }

        public override void OnShow(IFormData formData)
        {
            OnUpdateView(formData);
        }

        /// <summary>
        /// This method refreshes the existing list of operation, with specified list of operations
        /// </summary>
        /// <param name="iformData">Specifies parameters sent to method.</param>
        public override void OnUpdateView(IFormData iformData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.OnUpdateView");


            if (iformData != null)
            {
                _formData = (FormDataDeliveryInfo)iformData;
                _selectedConsignment = _formData.Consignment;
                _selectedTabName = _formData.SelectedTabName;
                _selectedLoadCarrier = _formData.SelectedLoadCarrier;
                _selectedOperationType = _formData.SelectedOperationType;
                if (_formData.IsStaredFromScan)
                {
                    _multiButtonControl.SetButtonEnabledState(ButtonBackName, !_formData.IsStaredFromScan);
                    _multiButtonControl.SetButtonEnabledState(ButtonScanName, _formData.IsStaredFromScan);
                }
            }


            ShowSelectedOperationDetails();
        }

        private void SetTextToUi()
        {
            if (ModelUser.UserProfile.OrgUnitName != null)
                lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            labelModuleName.Text = GlobalTexts.OperationDetails;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBackName });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Scan, ButtonScanClick, TabButtonType.Confirm) { Name = ButtonScanName });
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
            lblConsigneeHeading.Text = GlobalTexts.Consignee + ":";
        }



        /// <summary>
        /// This method iterate through all the rows in listConsignmentItems and unselects them one by one.
        /// </summary>
        private void UnSelectOtherRows()
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.UnSelectOtherRows");
            for (var i = 0; i <= listConsignmentItems.DataRows.Count - 1; i++)
            {
                var row = listConsignmentItems.DataRows[i];
                if (row.Selected)
                    row.Selected = false;
            }
        }

        /// <summary>
        /// This method binds the consignmentItems for the selected consignment to listConsignmentItems
        /// advance list control.
        /// </summary>
        private void BindConsignmentItems()
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.BindOperations");
            if (_selectedConsignment == null || _selectedConsignment.ConsignmentItem == null) return;
            listConsignmentItems.DataRows.Clear();
            listConsignmentItems.DataSource = null;
            listConsignmentItems.BeginUpdate();
            listConsignmentItems.DataSource = _selectedConsignment.ConsignmentItem;
            listConsignmentItems.EndUpdate();
        }

        private void ShowSelectedOperationDetails()
        {
            if (_selectedOperationType == DeliveryOperationType.Consignment)
                ShowConsignmentDetails();
            else
                ShowLoadCarrierDetails();
        }


        private void ShowLoadCarrierDetails()
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.ShowLoadCarrierDetails");
            _detailsPanel.AddSections(ItemDetailsUtil.PrepareInfoList(_selectedLoadCarrier));
            _detailsPanel.Location = new Point(17, 78); 
            ShowHideControls(true);

        }

        private void ShowHideControls(bool status)
        {
            lblConsigneeDetails.Visible = !status;
            lblConsigneeHeading.Visible = !status;
            listConsignmentItems.Visible = !status;
            lblConsignmentItems.Visible = !status;
            lblConsignmentNumber.Visible = !status;
            _detailsPanel.Visible = status;
        }

        /// <summary>
        /// This method shows the consignee and consignor details at the bottom of screen.
        /// </summary>
        private void ShowConsignmentDetails()
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.ShowConsignmentDetails");
            ShowHideControls(false);
            BindConsignmentItems();
            if (_selectedConsignment.ConsignmentItem != null)
                lblConsignmentItems.Text = GlobalTexts.ConsignmentItems + "(" + _selectedConsignment.ConsignmentItem.Length + ")";
            else
                lblConsignmentItems.Text = GlobalTexts.ConsignmentItems;
            lblConsignmentNumber.Text = GlobalTexts.Consignment + ":" + StringUtil.ToDisplayEntity(_selectedConsignment.ConsignmentNumber);
            lblConsigneeDetails.Text = _selectedConsignment != null ? GetConsigneeDetails() : string.Empty;
        }

        private string GetConsigneeDetails()
        {
            var consigneeDetails = new StringBuilder();
            consigneeDetails.Append(_selectedConsignment.GetConsigneeName());
            consigneeDetails.Append("," + Environment.NewLine + _selectedConsignment.GetConsigneeDetails());
            return consigneeDetails.ToString();
        }



        /*/// <summary>
        /// This method retrieves the consignee details from consignment and returns 
        /// concatenated string for Address, PostalCode , PostalName and Name.
        /// </summary>
        /// <param name="consignment">Specifies the consignment from which to retrieve consignee details.</param>
        /// <returns></returns>
        private string GetConsigneeDetails(PlannedConsignmentsType consignment)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.GetConsigneeDetails");
            var details = new StringBuilder();

            var consignee = consignment.Consignee;
            if (consignee == null) return details.ToString();

            //Append name
            if (!string.IsNullOrEmpty(consignee.Name1))
                details.Append(consignee.Name1);

            //Append address if not null
            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1))
                details.Append(Environment.NewLine + consignment.Consignee.DeliveryAddress1);

            //Append Postal code if not null
            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalCode))
            {
                //If delivery address is not null, then append comma
                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1))
                    details.Append(",");

                //Append postal code
                details.Append(Environment.NewLine + consignment.Consignee.DeliveryPostalCode);


                //Append postal name if not null
                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
                {
                    details.Append(" " + consignment.Consignee.DeliveryPostalName);
                }
            }
            else
            {
                //Append postal name if not null
                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
                {
                    if (!string.IsNullOrEmpty(details.ToString()))
                        details.Append(",");
                    details.Append(consignment.Consignee.DeliveryPostalName);
                }    
            }
            
            return details.ToString();

        }*/

        ///// <summary>
        ///// This method retrieves the consignor details from consignment and returns 
        ///// concatenated string for Address, PostalCode , PostalName and Name.
        ///// </summary>
        ///// <param name="consignment">Specifies the consignment from which to retrieve consignor details.</param>
        ///// <returns></returns>
        //private string GetConsignorDetails(PlannedConsignmentsType consignment)
        //{
        //    Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.GetConsignorDetails");
        //    var details = new StringBuilder();

        //    var consignor = consignment.Consignor;
        //    if (consignor == null) return details.ToString();

        //    //Append name
        //    if (!string.IsNullOrEmpty(consignor.Name1))
        //        details.Append(consignor.Name1);

        //    //Append address if not null
        //    if (!string.IsNullOrEmpty(consignment.Consignor.Address1))
        //        details.Append(Environment.NewLine + consignment.Consignor.Address1);

        //    //Append Postal code if not null
        //    if (!string.IsNullOrEmpty(consignment.Consignor.PostalCode))
        //    {
        //        //If delivery address is not null, then append comma
        //        if (!string.IsNullOrEmpty(consignment.Consignor.Address1))
        //            details.Append(",");
        //        //Append postal code
        //        details.Append(consignment.Consignor.PostalCode);
        //    }

        //    //Append postal name if not null
        //    if (!string.IsNullOrEmpty(consignment.Consignor.PostalName))
        //    {
        //        if (!string.IsNullOrEmpty(consignment.Consignor.Address1) || !string.IsNullOrEmpty(consignment.Consignor.PostalCode))
        //            details.Append(",");
        //        details.Append(consignment.Consignor.PostalName);
        //    }

        //    return details.ToString();
        //}

        #endregion

        #region Events

        /// <summary>
        /// This event runs when user clicks on back button and sends message to flow that, user clicked back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.ButtonBackClick");
                ViewEvent.Invoke(DeliveryInfoEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDeliveryInfo.ButtonBackClick");
            }
        }

        /// <summary>
        /// This event runs when user clicks on back button and sends message to flow that user clicked
        /// on Scan button, along with parameters that flow needs to handle this event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonScanClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.ButtonScanClick");
                ViewEvent.Invoke(DeliveryInfoEvent.Scan);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDeliveryInfo.ButtonScanClick");
            }
        }

        /// <summary>
        /// This is the haldler for listConsignmentItems.RowSelect event. It marks the corrently selected
        /// row as selected in advance list control and unselectes all other rows.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListConsignmentItemsRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormDeliveryInfo.ListConsignmentItemsRowSelect");

                switch (e.DataRow.CurrentTemplateIndex)
                {
                    case 1: // The master row has to be collapsed
                        break;
                    case 2: // The master row has to be expanded  
                        UnSelectOtherRows();
                        e.DataRow.Selected = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDeliveryInfo.ListConsignmentItemsRowSelect");
            }
        }

        #endregion



    }
    /// <summary>
    /// This class is having list of constants, corresponding to each event that can be raised on this
    /// screen, these constants will be used by flow to check which type of event occured on screen.
    /// </summary>
    public class DeliveryInfoEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Scan = 1;

        public static implicit operator DeliveryInfoEvent(int viewEvent)
        {
            return new DeliveryInfoEvent { Value = viewEvent };
        }
    }

}
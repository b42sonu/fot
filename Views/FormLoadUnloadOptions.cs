﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Resco.Controls.OutlookControls;
using System.Drawing;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    public enum LoadUnloadOptions
    {
        LoadLineHaul,
        LoadDistributionTruck,
        UnloadLineHaul,
        UnloadPickUpTruck,
        LoadCombinationTrip
    }


    public class LoadUnloadMenuItem
    {
        public string MenuItemText { get; set; }
        public LoadUnloadOptions MenuItemName { get; set; }
    }

    public partial class FormLoadUnloadOptions : BaseForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";

        private LoadUnloadMenuItem[] _menu;

        public FormLoadUnloadOptions()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();


            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            AddButtons();
        }


        private void SetMenuOptions(BarcodeType scannedBarcodeType)
        {

            switch (BaseModule.CurrentFlow.CurrentProcess)
            {
                case Process.UnloadLineHaul:
                case Process.LoadDistribTruck:
                case Process.LoadLineHaul:

                    if (scannedBarcodeType == BarcodeType.LoadCarrier)
                        _menu = new[]
                                               {
                                                   new LoadUnloadMenuItem{ MenuItemName = LoadUnloadOptions.LoadLineHaul , MenuItemText =  GlobalTexts.LoadLineHaul},
                                                   new LoadUnloadMenuItem{ MenuItemName = LoadUnloadOptions.UnloadLineHaul , MenuItemText =  GlobalTexts.UnloadLineHaul}
                                               };
                    else
                    {
                        _menu = new[]
                                               {
                                                   new LoadUnloadMenuItem{ MenuItemName = LoadUnloadOptions.LoadLineHaul , MenuItemText =  GlobalTexts.LoadLineHaul},
                                                   new LoadUnloadMenuItem{ MenuItemName = LoadUnloadOptions.LoadDistributionTruck , MenuItemText =  GlobalTexts.LoadDistributionTruck},//todo to buttontext
                                                   new LoadUnloadMenuItem{ MenuItemName = LoadUnloadOptions.UnloadLineHaul , MenuItemText =  GlobalTexts.UnloadLineHaul},
                                                   new LoadUnloadMenuItem{ MenuItemName = LoadUnloadOptions.UnloadPickUpTruck , MenuItemText =  GlobalTexts.UnloadPickUpTruck}//todo to buttontext
                                               };
                    }
                    break;
            }

            ShowOptions();

            touchPanel.Controls.Add(_multiButtonControl);
        }



        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormLoadUnloadOptions.AddButtons()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadUnloadOptions.AddButtons");
            }
        }


        public override void OnShow(IFormData iformData)
        {
            if (iformData != null)
            {
                var formData = (FormDataLoadUnloadOptions)iformData;
                SetMenuOptions(formData.ScannedBarcodeType);
                labelModuleName.Text = formData.HeaderText;
                touchPanel.Controls.Add(labelModuleName);
            }
        }


        private void ShowOptions()
        {
            var x = 20;
            var y = 80;
            Color backolor = Color.FromArgb(46, 48, 50), foreColor = Color.White;
            Size buttonSize = ScaleUtil.GetScaledSize(209, 100);
            int buttonSpace = ScaleUtil.GetScaledPosition(20);

            SuspendLayout();

            touchPanel.Controls.Clear();

            int count = 0;
            foreach (var menuItem in _menu)
            {

                var btnMenuItem = new ImageButton
                                      {
                                          Location = new System.Drawing.Point(x, y),
                                          Size = buttonSize,
                                          Text = menuItem.MenuItemText,
                                          WordWrap = true,
                                          ForeColor = System.Drawing.Color.White,
                                          BackColor = System.Drawing.Color.FromArgb(46, 48, 50),
                                          Font = new System.Drawing.Font("Arial", 8F, FontStyle.Regular)
                                      };
                btnMenuItem.Click += OnMenuItemClicked;
                btnMenuItem.Tag = menuItem.MenuItemName;
                touchPanel.Controls.Add(btnMenuItem);

                //Update vertical point by adding control space and control height.
                if (btnMenuItem.Text.Length > 19)
                    AdjustFont(btnMenuItem);
                if (count % 2 == 0)
                {
                    x = btnMenuItem.Size.Width + buttonSpace + 20;
                }
                else
                {
                    x = 20;
                    y += (btnMenuItem.Size.Height + buttonSpace);
                }
                count++;
            }
        }

        void AdjustFont(ImageButton btn)
        {
            if (btn.Text.Contains("\r\n")) return;

            string newText = string.Empty;
            string[] words = btn.Text.Split(' ');
            string line = string.Empty;
            foreach (string word in words)
            {
                if ((line.Length + word.Length) > 18)
                {
                    newText += Environment.NewLine + word;
                    line = word;
                }
                else
                {
                    line += " " + word;
                    newText += " " + word;
                }
            }
            newText.Replace('_', ' ');
            btn.Text = newText;
        }

        private void OnMenuItemClicked(object sender, EventArgs e)
        {
            try
            {
                var menuItem = ((ImageButton)sender).Tag;
                switch ((LoadUnloadOptions)menuItem)
                {
                    case LoadUnloadOptions.LoadLineHaul:
                        ViewEvent.Invoke(LoadUnloadViewEvents.LoadLineHaul);
                        break;
                    case LoadUnloadOptions.LoadDistributionTruck:
                        ViewEvent.Invoke(LoadUnloadViewEvents.LoadDistributionTruck);
                        break;
                    case LoadUnloadOptions.UnloadLineHaul:
                        ViewEvent.Invoke(LoadUnloadViewEvents.UnloadLineHaul);
                        break;
                    case LoadUnloadOptions.UnloadPickUpTruck:
                        ViewEvent.Invoke(LoadUnloadViewEvents.UnloadPickUpTruck);
                        break;

                    default:
                        throw new ArgumentException(string.Format("Menuitem {0} in OnMenuItemClicked is handled in switch", Convert.ToString(menuItem)));
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadUnloadOptions.OnMenuItemClicked");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadUnloadViewEvents.Back, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadUnloadOptions.ButtonBackClick");
            }
        }
    }

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormWorkListItems.
    /// </summary>
    public class FormDataLoadUnloadOptions : BaseFormData
    {
        public BarcodeType ScannedBarcodeType { get; set; }
    }

    public class LoadUnloadViewEvents : ViewEvent
    {
        public const int Back = 0;
        public const int LoadLineHaul = 1;
        public const int LoadDistributionTruck = 2;
        public const int UnloadLineHaul = 3;
        public const int UnloadPickUpTruck = 4;
    }
}
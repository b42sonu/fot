﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity
{
    /// <summary>
    /// Represents the ConsignmentItemNumber entity.
    /// </summary>
    public class Parcel : EntityBase
    {
        /// <summary>
        /// Gets or sets the parcel number.
        /// </summary>
        /// <value>The parcel number.</value>
        public string ParcelNumber { get; set; }

        /// <summary>
        /// Gets or sets the product code.
        /// </summary>
        /// <value>The product code.</value>
        public string ProductCode { get; set; }

        /// <summary>
        /// Gets or sets the value added services.
        /// </summary>
        /// <value>The value added services.</value>
        public ValueAddedService[] ValueAddedServices { get; set; }
    }
}

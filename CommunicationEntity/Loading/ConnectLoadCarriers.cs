﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.0.30319.17929.
// 
namespace CommunicationEntity.Loading {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:posten.no/eConnect/ConnectLoadCarriers")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="urn:posten.no/eConnect/ConnectLoadCarriers", IsNullable=false)]
    public partial class ConnectLoadCarriersRequest {
        
        private string userLogonIdField;
        
        private string tMS_AffiliationField;
        
        private string orgUnitIdField;
        
        private string logicalIdField;
        
        private string physicalIdField;
        
        private string nameOfTypeField;
        
        /// <remarks/>
        public string UserLogonId {
            get {
                return this.userLogonIdField;
            }
            set {
                this.userLogonIdField = value;
            }
        }
        
        /// <remarks/>
        public string TMS_Affiliation {
            get {
                return this.tMS_AffiliationField;
            }
            set {
                this.tMS_AffiliationField = value;
            }
        }
        
        /// <remarks/>
        public string OrgUnitId {
            get {
                return this.orgUnitIdField;
            }
            set {
                this.orgUnitIdField = value;
            }
        }
        
        /// <remarks/>
        public string LogicalId {
            get {
                return this.logicalIdField;
            }
            set {
                this.logicalIdField = value;
            }
        }
        
        /// <remarks/>
        public string PhysicalId {
            get {
                return this.physicalIdField;
            }
            set {
                this.physicalIdField = value;
            }
        }
        
        /// <remarks/>
        public string NameOfType {
            get {
                return this.nameOfTypeField;
            }
            set {
                this.nameOfTypeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:posten.no/eConnect/ConnectLoadCarriers")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="urn:posten.no/eConnect/ConnectLoadCarriers", IsNullable=false)]
    public partial class ConnectLoadCarriersResponse {
        
        private string physicalIdField;
        
        private string userLogonIdField;
        
        private System.DateTime timeOfCreationField;
        
        private string nameOfTypeField;
        
        private long versionField;
        
        /// <remarks/>
        public string PhysicalId {
            get {
                return this.physicalIdField;
            }
            set {
                this.physicalIdField = value;
            }
        }
        
        /// <remarks/>
        public string UserLogonId {
            get {
                return this.userLogonIdField;
            }
            set {
                this.userLogonIdField = value;
            }
        }
        
        /// <remarks/>
        public System.DateTime TimeOfCreation {
            get {
                return this.timeOfCreationField;
            }
            set {
                this.timeOfCreationField = value;
            }
        }
        
        /// <remarks/>
        public string NameOfType {
            get {
                return this.nameOfTypeField;
            }
            set {
                this.nameOfTypeField = value;
            }
        }
        
        /// <remarks/>
        public long Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
    }
}

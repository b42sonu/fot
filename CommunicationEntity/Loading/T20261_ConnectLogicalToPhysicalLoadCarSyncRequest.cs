﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading
{
    public class T20261_ConnectLogicalToPhysicalLoadCarSyncRequest : EntityBase
    {
        public SyncHeader Header;
        public string UserLogonId;
        public string TMS_Affiliation;
        public string OrgUnitId;
        public string LogicalId;
        public string PhysicalId;
        public string NameOfType;
    }
}

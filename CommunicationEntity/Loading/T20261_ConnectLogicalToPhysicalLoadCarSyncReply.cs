﻿
using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading
{
    public class T20261_ConnectLogicalToPhysicalLoadCarSyncReply : EntityBase
    {
        public string PhysicalId;// { get; set; }
        public string UserLogonId;// { get; set; }
        public DateTime TimeOfCreation;// { get; set; }
        public string NameOfType;// { get; set; }
        public long Version;// { get; set; }
    }
}

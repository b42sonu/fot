﻿using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading
{
    public class T20240_GetGoodsListFromFOTReply : EntityBase
    {
        public SyncHeader Header;
        public ResponseInformationType ResponseInformation;
        public GetGoodsListResponseOperation Operation;
        public GetGoodsListResponsePlannedStop[] PlannedStop;
    }

    public class ResponseInformationType
    {
        public string procedureName { get; set; }
        public string backendEnvironment { get; set; }
        public string returnState { get; set; }
        public string returnText { get; set; }
        public string sQLCode { get; set; }
        public string sQLState { get; set; }
        public string messageId { get; set; }
        public int timeConsumption { get; set; }
        public string validationCode { get; set; }
    }

    public class GetGoodsListResponseOperation
    {
        public string tripId { get; set; }
        public string stopId { get; set; }
        public string actionType { get; set; }
        public string externalTripId { get; set; }
        public string route { get; set; }
        public string locationId { get; set; }
        [System.Xml.Serialization.XmlArray("consignment")]
        [System.Xml.Serialization.XmlArrayItem("GetGoodsListResponseOperationConsignment")]
        public GetGoodsListResponseOperationConsignment[] consignment { get; set; }
        [System.Xml.Serialization.XmlArray("loadCarrier")]
        [System.Xml.Serialization.XmlArrayItem("GetGoodsListResponseOperationLoadCarrier")]
        public GetGoodsListResponseOperationLoadCarrier[] loadCarrier { get; set; }
    }

    public class GetGoodsListResponseOperationConsignment
    {
        public string consignmentNumber { get; set; }
        public int consignmentItemCount { get; set; }
        public GetGoodsListResponseOperationConsignmentConsignmentItem[] consignmentItem { get; set; }
        public bool consignmentIsLoaded { get; set; }
        public bool consignmentIsUnloaded { get; set; }
        public bool consignmentIsPlanned { get; set; }
        public ConsigneeTypeLoading consignee { get; set; }
    }

    public class GetGoodsListResponseOperationLoadCarrier 
    {
        public string loadCarrierId { get; set; }
        public int physicalLoadCarrierId{ get; set; }
        public string parentLoadCarrier{ get; set; }
        public string destinationId{ get; set; }
        public string destinationName1{ get; set; }
        public string destinationName2{ get; set; }
        public string postgangCode{ get; set; }
        public string logisticsStream{ get; set; }
        public string orgUnitId{ get; set; }
        public string cDCustomerId{ get; set; }
        public string cDConsigneeId{ get; set; }
        public string cDAddress1{ get; set; }
        public string cDAddress2{ get; set; }
        public string cDAddress3{ get; set; }
        public string[] sortInfo{ get; set; }
        public string locationId{ get; set; }
        public string shoppingCentreId{ get; set; }
    }

    public class ConsigneeTypeLoading
    {
        public string name1 { get; set; }
        public string name2 { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string postalCode { get; set; }
        public string postalName { get; set; }
        public string countryCode { get; set; }
        public string deliveryAddress1 { get; set; }
        public string deliveryAddress2 { get; set; }
        public string deliveryPostalCode { get; set; }
        public string deliveryPostalName { get; set; }
        public string deliveryCountryCode { get; set; }
        public string phoneNumber { get; set; }
    }

    public class GetGoodsListResponseOperationConsignmentConsignmentItem
    {
        public string consignmentItemNumber { get; set; }
        public bool consignmentItemIsLoaded { get; set; }
        public bool consignmentItemIsUnloaded { get; set; }
        public bool consignmentItemIsPlanned { get; set; }
    }

    public class GetGoodsListResponsePlannedStop
    {
        private DateTime _latestEndTime;
        private DateTime _earliestStartTime;

        public GetGoodsListResponsePlannedStop()
        {
            _latestEndTime = new DateTime(0L, DateTimeKind.Utc);
            _earliestStartTime = new DateTime(0L, DateTimeKind.Utc);
        }

        public string tripId { get; set; }
        public string stopId { get; set; }
        public string locationName { get; set; }
        public string locationAddress1 { get; set; }
        public string locationPostalCode { get; set; }
        public string stopSequence { get; set; }
        public DateTime latestEndTime
        {
            get
            {
                if (_latestEndTime == DateTime.MinValue && _latestEndTime.Kind == DateTimeKind.Unspecified)
                {
                    _latestEndTime = new DateTime(0L, DateTimeKind.Utc);
                }

                return _latestEndTime;
            }
            set { _latestEndTime = value; }
        }

        public bool latestEndTimeSpecified { get; set; }

        public System.DateTime earliestStartTime
        {
            get
            {
                if (_earliestStartTime == DateTime.MinValue && _earliestStartTime.Kind == DateTimeKind.Unspecified)
                {
                    _earliestStartTime = new DateTime(0L, DateTimeKind.Utc);
                }

                return _earliestStartTime;
            }
            set { _earliestStartTime = value; }
        }

        public bool earliestStartTimeSpecified { get; set; }
        public string route { get; set; }
        public string powerUnitId { get; set; }
        public string loadCarrierId { get; set; }
    }
}

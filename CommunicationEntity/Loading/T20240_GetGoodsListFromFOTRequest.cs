﻿using System.Xml.Serialization;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading
{
    public class T20240_GetGoodsListFromFOTRequest : EntityBase
    {
        public SyncHeader Header;
        public GetGoodsListRequestPlannedOperationInformation PlannedOperationInformation;
        public GetGoodsListRequestLocationInformation LocationInformation;
        public string TMSAffiliationId;
        public LoggingInformationType LoggingInformation;
        public GetGoodsListRequestRequestType RequestType;
    }

    public class GetGoodsListRequestPlannedOperationInformation
    {
        public string TripId{ get; set; }
        public string StopId{ get; set; }
        public string ActionType{ get; set; }
        public string ExternalTripId{ get; set; }
        public string Route{ get; set; }
        public string LocationId{ get; set; }
    }

    public class GetGoodsListRequestLocationInformation
    {
        public string LocationId { get; set; }
        public string OrgUnitId { get; set; }
        public string PowerUnitId { get; set; }
        public string LoadCarrierId { get; set; }
        public string ActionType { get; set; }
        public string EventCode { get; set; }
        public string Route { get; set; }
        public string TripId { get; set; }
        public string ExternalTripId { get; set; }
        public int NumberOfHours { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string Role { get; set; }
    }

    [XmlRoot("Loading", Namespace = "http://example.com/schemas/Loading")]
    public class LoggingInformationType
    {
        public string LogonName { get; set; }
        public string SystemCode { get; set; }
        public string LanguageCode { get; set; }
        public string CompanyCode { get; set; }
        public string CountryCode { get; set; }
        public string OrgUnitId { get; set; }
        public string UserRole { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
        public string SubProcessVersion { get; set; }
        public string InterfaceVersion { get; set; }
        public System.DateTime InvocationTime { get; set; }
        public string MessageId { get; set; }
    }

    public enum GetGoodsListRequestRequestType
    {
        Plan,
        Stop,
        StopList,
        LoadList,
        Route,
        StopListAmphora,
    }
}

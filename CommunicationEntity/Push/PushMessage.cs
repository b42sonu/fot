﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Push
{
    public class PushMessage : EntityBase
    {
        public string User { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
    }
}

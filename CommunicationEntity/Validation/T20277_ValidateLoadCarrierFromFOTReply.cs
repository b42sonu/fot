﻿using System;

using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC
{
    public class T20277_ValidateLoadCarrierFromFOTReply : EntityBase
    {
        public ReqRespType ResponseType;
        public bool DidValidateOk;
        public string Version;
//        public ValidateLoadCarrierResponseLogicalLoadCarrierDetails LogicalLoadCarrierDetails;
        public PhysicalLoadCarrierType PhysicalLoadCarrier;

        [System.Xml.Serialization.XmlArray("LogicalLoadCarrierDetails")]
        [System.Xml.Serialization.XmlArrayItem("LogicalLoadCarrierType", IsNullable = false)]
        public LogicalLoadCarrierType[] LogicalLoadCarrierDetails;
        public ValidateLoadCarrierResponseRBTValidationFailureDetails RBTValidationFailureDetails;
        public ValidateLoadCarrierResponseEventValidationFailureDetails EventValidationFailureDetails;
    }

/*    public class ValidateLoadCarrierResponseLogicalLoadCarrierDetails 
    {
        public PhysicalLoadCarrierType PhysicalLoadCarrier { get; set; }

        [System.Xml.Serialization.XmlArray("LogicalLoadCarrier")]
        [System.Xml.Serialization.XmlArrayItem("LogicalLoadCarrierType", IsNullable = false)]
        public LogicalLoadCarrierType[] LogicalLoadCarrier { get; set; }
        public string Version { get; set; }
    }
*/
    public class PhysicalLoadCarrierType
    {
        public string PhysicalId { get; set; }
        public string PhysicalDescription { get; set; } 
        public string PhysicalLoadCarrierId { get; set; }
        public string LoadCarrierCode { get; set; }
        public string LoadCarrierType { get; set; }
        public string CompanycodeLogicalLoadCarrier { get; set; }
    }

    public class LogicalLoadCarrierType
    {
        public string PhysicalId { get; set; }
        public string PhysicalDescription { get; set; }
        public string PhysicalLoadCarrierId { get; set; }
        public string DestinationId { get; set; }
        public string DestinationName1 { get; set; }
        public string DestinationName2 { get; set; }
        public string LoadCarrierCode { get; set; }
        public string LoadCarrierType { get; set; }
        public string LogicalLoadCarrierId { get; set; }
        public string LogicalLoadCarrierParent { get; set; }
        public string RecipientEmailAddress { get; set; }
        public string RecipientId { get; set; }
        public string PostgangCode { get; set; }
        public string CompanycodeLogicalLoadCarrier { get; set; }
        public string ProductionFlowCode { get; set; }
        public string TerminalOrgUnitId { get; set; }
        public string TerminalName { get; set; }
        private DateTime _timeCreated;
        private DateTime _timeLabelPrinted;
        private DateTime _timeTerminated;
        public DateTime TimeCreated 
        {
             get { 
                if (_timeCreated == DateTime.MinValue && _timeCreated.Kind == DateTimeKind.Unspecified)
                    _timeCreated = new DateTime(0L, DateTimeKind.Utc);
                return _timeCreated;
            }

            set {
                _timeCreated = value;
            }
        }

        public DateTime TimeLabelPrinted
        {
             get {
                 if (_timeLabelPrinted == DateTime.MinValue && _timeLabelPrinted.Kind == DateTimeKind.Unspecified)
                     _timeLabelPrinted = new DateTime(0L, DateTimeKind.Utc);
                 return _timeLabelPrinted;
            }

            set {
                _timeLabelPrinted = value;
            }
        }
        public DateTime TimeTerminated
        {
             get {
                 if (_timeTerminated == DateTime.MinValue && _timeTerminated.Kind == DateTimeKind.Unspecified)
                     _timeTerminated = new DateTime(0L, DateTimeKind.Utc);
                 return _timeTerminated;
            }

            set {
                _timeTerminated = value;
            }
        }
    }

    public class ValidateLoadCarrierResponseRBTValidationFailureDetails
    {
        public bool PowerUnitIdDidValidateOk { get; set; }
        public bool LoadCarrierDidValidateOk { get; set; }
        public string ValidationMessage { get; set; }
    }

    public class ValidateLoadCarrierResponseEventValidationFailureDetails
    {
        public string ValidationMessage { get; set; }
        public string ReturnStatusCode { get; set; }
    }
}

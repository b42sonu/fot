﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using PreCom.Core.Communication;
using System.Xml.Serialization;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{
    /// <summary>
    /// Represents the ValidateParcelReply entity.
    /// </summary>
    [XmlInclude(typeof(ScriptValidateParcelReply)), XmlInclude(typeof(DeliveryValidateParcelReply))]
    public class ValidateParcelReply : EntityBase
    {
        /// <summary>
        /// Gets or sets the returned status.
        /// </summary>
        /// <value>The returned status.</value>
        public string ReturnedStatus { get; set; }

        /// <summary>
        /// Gets or sets the consignment.
        /// </summary>
        /// <value>The consignment.</value>
        public Consignment Consignment { get; set; }

        /// <summary>
        /// Gets or sets the identification.
        /// </summary>
        /// <value>The identification.</value>
        public Guid Identification { get; set; }
    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{
#if PocketPC
    public partial class T20200_ValidateGoodsFromFOTReply
    {
        // Retrieve consignment number with corrections if it is synthetic
        public string GetConsignmentNumber()
        {
            try
            {
                if (Consignment.IsSynthetic == false)
                    return Consignment.ConsignmentNumber;

                if (Consignment.ConsignmentItem[0].EDIReceived)
                    return Consignment.ConsignmentItem[0].ConsignmentItemNumber;

                string productCode = Consignment.ConsignmentItem[0].ProductCode;
                if (productCode == "3050" || productCode == "3267" || productCode == ConsignmentEntity.UnknownProduct || string.IsNullOrEmpty(productCode))
                    return Entity.EntityMap.Consignment.SyntheticConsignmentIdWarning;

                return Consignment.ConsignmentItem[0].ConsignmentItemNumber;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "T20200_ValidateGoodsFromFOTReply.GetConsignmentNumber");
            }
            return Consignment.ConsignmentNumber;
        }
    }
#endif
}

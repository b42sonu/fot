﻿using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC
{
    public class T20277_ValidateLoadCarrierFromFOTRequest : EntityBase
    {
        public SyncHeader Header;
        public ReqRespType RequestType;
        public string LoadCarrierId;
        public string PowerUnitId;
        public string LogicalLoadCarrierType;
        public EventType Event;
        public LoggingInformationType LoggingInformation;
    }

    public class EventType
    {
        public string LoadCarrierParentId { get; set; }
        public string ReasonCode { get; set; }
        public string Attributte1 { get; set; }
        public string Attributte2 { get; set; }
        public string Attributte3 { get; set; }
        public string Consignor { get; set; }
        public string DeviationCode { get; set; }
        public string OrgUnitIdForEvent { get; set; }
        public string OrgUnitIdTransferredTo{ get; set; }
        public string EventType1 { get; set; }
        public string Comment { get; set; }
        public string Consignee { get; set; }
        public string Position { get; set; }
        public string ZipCodeEvent { get; set; }
        public DateTime Timestamp { get; set; }
        public string ActionCode { get; set; }
    }

    public enum ReqRespType
    {
        AMPHORA,
        LMK,
    }

    public class LoggingInformationType
    {
        public string LogonName { get; set; }
        public string SystemCode { get; set; }
        public string LanguageCode { get; set; }
        public string CompanyCode { get; set; }
        public string CountryCode { get; set; }
        public string OrgUnitId { get; set; }
        public string UserRole { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
        public string SubProcessVersion { get; set; }
        public string InterfaceVersion { get; set; }
        public System.DateTime InvocationTime { get; set; }
        public string MessageId { get; set; }
    }
}

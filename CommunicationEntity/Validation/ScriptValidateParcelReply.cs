﻿namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{
    /// <summary>
    /// Represents the ScriptValidateParcelReply entity. Empty class that implements 
    /// base class due to the nature of how PreCom object communication works.
    /// </summary>
    public class ScriptValidateParcelReply : ValidateParcelReply
    {
    }
}

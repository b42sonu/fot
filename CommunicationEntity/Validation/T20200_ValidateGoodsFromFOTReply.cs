﻿using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{

    public partial class T20200_ValidateGoodsFromFOTReply : EntityBase
    {
        public SyncHeader Header;
        public ValidationResponseResponseInformation ResponseInformation;
        public ValidationResponseConsignment Consignment;
        public ValidationResponseValidationStatuses ValidationStatuses;
        [System.Xml.Serialization.XmlArray("LoadCarrierVAS")]
        [System.Xml.Serialization.XmlArrayItem("string", IsNullable = false)]
        public string[] LoadCarrierVAS;
    }

    public partial class ValidationResponseResponseInformation
    {
        private string procedureNameField;
        private string backendEnvironmentField;
        private string returnStateField;
        private string returnTextField;
        private string sQLCodeField;
        private string sQLStateField;
        private string messageIdField;
        private int timeConsumptionField;
        private string validationCodeField;

        /// <remarks/>
        public string ProcedureName
        {
            get
            {
                return this.procedureNameField;
            }
            set
            {
                this.procedureNameField = value;
            }
        }

        /// <remarks/>
        public string BackendEnvironment
        {
            get
            {
                return this.backendEnvironmentField;
            }
            set
            {
                this.backendEnvironmentField = value;
            }
        }

        /// <remarks/>
        public string ReturnState
        {
            get
            {
                return this.returnStateField;
            }
            set
            {
                this.returnStateField = value;
            }
        }

        /// <remarks/>
        public string ReturnText
        {
            get
            {
                return this.returnTextField;
            }
            set
            {
                this.returnTextField = value;
            }
        }

        /// <remarks/>
        public string SQLCode
        {
            get
            {
                return this.sQLCodeField;
            }
            set
            {
                this.sQLCodeField = value;
            }
        }

        /// <remarks/>
        public string SQLState
        {
            get
            {
                return this.sQLStateField;
            }
            set
            {
                this.sQLStateField = value;
            }
        }

        /// <remarks/>
        public string MessageId
        {
            get
            {
                return this.messageIdField;
            }
            set
            {
                this.messageIdField = value;
            }
        }

        /// <remarks/>
        public int TimeConsumption
        {
            get
            {
                return this.timeConsumptionField;
            }
            set
            {
                this.timeConsumptionField = value;
            }
        }

        public string ValidationCode
        {
            get
            {
                return this.validationCodeField;
            }
            set
            {
                this.validationCodeField = value;
            }
        }
    }

    public partial class AddressType
    {

        private string name1Field;
        private string name2Field;
        private string address1Field;
        private string address2Field;
        private string postalCodeField;
        private string postalNameField;
        private string countryCodeField;

        /// <remarks/>
        public string Name1
        {
            get
            {
                return this.name1Field;
            }
            set
            {
                this.name1Field = value;
            }
        }

        /// <remarks/>
        public string Name2
        {
            get
            {
                return this.name2Field;
            }
            set
            {
                this.name2Field = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string PostalName
        {
            get
            {
                return this.postalNameField;
            }
            set
            {
                this.postalNameField = value;
            }
        }

        /// <remarks/>
        public string CountryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }
    }

    public partial class PaymentType
    {
        private decimal amountField;
        private string currencyField;
        private bool paidField;
        private string vATCodeField;
        private decimal vATPercentageField;
        private bool vATPercentageFieldSpecified;
        private string descriptionField;

        /// <remarks/>
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public bool Paid
        {
            get
            {
                return this.paidField;
            }
            set
            {
                this.paidField = value;
            }
        }

        /// <remarks/>
        public string VATCode
        {
            get
            {
                return this.vATCodeField;
            }
            set
            {
                this.vATCodeField = value;
            }
        }

        /// <remarks/>
        public decimal VATPercentage
        {
            get
            {
                return this.vATPercentageField;
            }
            set
            {
                this.vATPercentageField = value;
            }
        }

        /// <remarks/>

        public bool VATPercentageSpecified
        {
            get
            {
                return this.vATPercentageFieldSpecified;
            }
            set
            {
                this.vATPercentageFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    public partial class EventType
    {
        private string eventCodeField;
        private System.DateTime eventTimeField;
        private string reasonCodeField;
        private string actionCodeField;
        private string eventOrgUnitIdField;
        private string tripIdField;
        private string stopIdField;
        private string externalTripIdField;
        private string locationIdField;
        private string loadCarrierField;
        private string eventUserIdField;
        private string damageTypeField;
        private string freeTextField;

        public EventType()
        {
            eventTimeField = new DateTime(0L, DateTimeKind.Utc);    
        }

        /// <remarks/>
        public string EventCode
        {
            get
            {
                return this.eventCodeField;
            }
            set
            {
                this.eventCodeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime EventTime
        {
            get
            {
                if (this.eventTimeField == DateTime.MinValue && this.eventTimeField.Kind == DateTimeKind.Unspecified)
                {
                    this.eventTimeField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.eventTimeField;
            }
            set
            {
                this.eventTimeField = value;
            }
        }

        /// <remarks/>
        public string ReasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }

        /// <remarks/>
        public string ActionCode
        {
            get
            {
                return this.actionCodeField;
            }
            set
            {
                this.actionCodeField = value;
            }
        }

        /// <remarks/>
        public string EventOrgUnitId
        {
            get
            {
                return this.eventOrgUnitIdField;
            }
            set
            {
                this.eventOrgUnitIdField = value;
            }
        }

        /// <remarks/>
        public string TripId
        {
            get
            {
                return this.tripIdField;
            }
            set
            {
                this.tripIdField = value;
            }
        }

        /// <remarks/>
        public string StopId
        {
            get
            {
                return this.stopIdField;
            }
            set
            {
                this.stopIdField = value;
            }
        }

        /// <remarks/>
        public string ExternalTripId
        {
            get
            {
                return this.externalTripIdField;
            }
            set
            {
                this.externalTripIdField = value;
            }
        }

        /// <remarks/>
        public string LocationId
        {
            get
            {
                return this.locationIdField;
            }
            set
            {
                this.locationIdField = value;
            }
        }

        /// <remarks/>
        public string LoadCarrier
        {
            get
            {
                return this.loadCarrierField;
            }
            set
            {
                this.loadCarrierField = value;
            }
        }

        /// <remarks/>
        public string EventUserId
        {
            get
            {
                return this.eventUserIdField;
            }
            set
            {
                this.eventUserIdField = value;
            }
        }

        public string DamageType
        {
            get
            {
                return this.damageTypeField;
            }
            set
            {
                this.damageTypeField = value;
            }
        }

        public string FreeText
        {
            get
            {
                return this.freeTextField;
            }
            set
            {
                this.freeTextField = value;
            }
        }
    }

    public partial class VASType
    {
        private string vASCodeField;
        private decimal amountField;
        private bool amountFieldSpecified;
        private string currencyField;
        private string vasTextField;
        private bool isPaidField;
        private bool isPaidFieldSpecified;
        private bool isSignatureRequiredField;
#pragma warning disable 169
        private bool isSignatureRequiredFieldSpecified;
#pragma warning restore 169

        /// <remarks/>
        public string VASCode
        {
            get
            {
                return this.vASCodeField;
            }
            set
            {
                this.vASCodeField = value;
            }
        }

        /// <remarks/>
        public decimal Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public bool AmountSpecified
        {
            get
            {
                return this.amountFieldSpecified;
            }
            set
            {
                this.amountFieldSpecified = value;
            }
        }


        public string Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        public string VasText
        {
            get
            {
                return this.vasTextField;
            }
            set
            {
                this.vasTextField = value;
            }
        }

        public bool IsPaid
        {
            get
            {
                return this.isPaidField;
            }
            set
            {
                this.isPaidField = value;
            }
        }

        public bool IsPaidSpecified
        {
            get
            {
                return this.isPaidFieldSpecified;
            }
            set
            {
                this.isPaidFieldSpecified = value;
            }
        }

        public bool IsSignatureRequired
        {
            get
            {
                return this.isSignatureRequiredField;
            }
            set
            {
                this.isSignatureRequiredField = value;
            }
        }
    }

    public partial class ValidationResponseConsignment
    {
        private string consignmentNumberField;

        private ValidationResponseConsignmentConsignmentItem[] consignmentItemField;

        private PaymentType[] paymentField;
        private string customerNumberField;
        private string orderNumberField;
        private decimal weightField;
        private bool weightFieldSpecified;
        private string weightUnitField;
        private decimal volumeField;
        private bool volumeFieldSpecified;
        private string volumeUnitField;
        private short consignmentItemCountField;
        private bool consignmentItemCountFieldSpecified;
        private decimal loadingMetersField;
        private bool loadingMetersFieldSpecified;
        private short palletPlacesField;
        private bool palletPlacesFieldSpecified;
        private string productCodeField;
        private string productCategoryField;
        private string productGroupField;

        private ValidationResponseConsignmentPackageType[] packageTypeField;
        private ValidationResponseConsignmentTransportEquipment[] transportEquipmentField;

        private bool eDIReceivedField;
        private bool temperatureRequiredField;
        private decimal temperatureMaxField;
        private bool temperatureMaxFieldSpecified;
        private decimal temperatureMinField;
        private bool temperatureMinFieldSpecified;
        private decimal temperatureIdealField;
        private bool temperatureIdealFieldSpecified;
        private string temperatureUnitField;

        private ValidationResponseConsignmentConsignor consignorField;
        private ValidationResponseConsignmentConsignee consigneeField;
        private ValidationResponseConsignmentThirdPartyReturnInformation thirdPartyReturnInformationField;

        private string companyCodeField;
        private VASType[] vASField;
        private EventType[] eventField;
        private bool returnedField;
        private bool returnedFieldSpecified;
        private bool partySuspendedField;
        private bool isConsignmentEventRegAllowedField;
        private bool isConsignmentEventRegAllowedFieldSpecified;
        private bool isMassRegistrationAllowedField;
        private string actorNumberField;
        private bool isActorBlockedField;
        private bool isActorBlockedFieldSpecified;
        private string shoppingCenterIdField;
        private bool isSyntheticField;
        private bool isSyntheticFieldSpecified;

        /// <remarks/>
        public string ConsignmentNumber
        {
            get
            {
                return this.consignmentNumberField;
            }
            set
            {
                this.consignmentNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArray("ConsignmentItem")]
        [System.Xml.Serialization.XmlArrayItem("ValidationResponseConsignmentConsignmentItem", IsNullable = false)]
        public ValidationResponseConsignmentConsignmentItem[] ConsignmentItem
        {
            get
            {
                return this.consignmentItemField;
            }
            set
            {
                this.consignmentItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Payment")]
        public PaymentType[] Payment
        {
            get
            {
                return this.paymentField;
            }
            set
            {
                this.paymentField = value;
            }
        }

        /// <remarks/>
        public string CustomerNumber
        {
            get
            {
                return this.customerNumberField;
            }
            set
            {
                this.customerNumberField = value;
            }
        }

        /// <remarks/>
        public string OrderNumber
        {
            get
            {
                return this.orderNumberField;
            }
            set
            {
                this.orderNumberField = value;
            }
        }

        /// <remarks/>
        public decimal Weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        /// <remarks/>

        public bool WeightSpecified
        {
            get
            {
                return this.weightFieldSpecified;
            }
            set
            {
                this.weightFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string WeightUnit
        {
            get
            {
                return this.weightUnitField;
            }
            set
            {
                this.weightUnitField = value;
            }
        }

        /// <remarks/>
        public decimal Volume
        {
            get
            {
                return this.volumeField;
            }
            set
            {
                this.volumeField = value;
            }
        }

        /// <remarks/>

        public bool VolumeSpecified
        {
            get
            {
                return this.volumeFieldSpecified;
            }
            set
            {
                this.volumeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string VolumeUnit
        {
            get
            {
                return this.volumeUnitField;
            }
            set
            {
                this.volumeUnitField = value;
            }
        }

        /// <remarks/>
        public short ConsignmentItemCount
        {
            get
            {
                return this.consignmentItemCountField;
            }
            set
            {
                this.consignmentItemCountField = value;
            }
        }

        /// <remarks/>

        public bool ConsignmentItemCountSpecified
        {
            get
            {
                return this.consignmentItemCountFieldSpecified;
            }
            set
            {
                this.consignmentItemCountFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal LoadingMeters
        {
            get
            {
                return this.loadingMetersField;
            }
            set
            {
                this.loadingMetersField = value;
            }
        }

        /// <remarks/>

        public bool LoadingMetersSpecified
        {
            get
            {
                return this.loadingMetersFieldSpecified;
            }
            set
            {
                this.loadingMetersFieldSpecified = value;
            }
        }

        /// <remarks/>
        public short PalletPlaces
        {
            get
            {
                return this.palletPlacesField;
            }
            set
            {
                this.palletPlacesField = value;
            }
        }

        /// <remarks/>

        public bool PalletPlacesSpecified
        {
            get
            {
                return this.palletPlacesFieldSpecified;
            }
            set
            {
                this.palletPlacesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ProductCode
        {
            get
            {
                return this.productCodeField;
            }
            set
            {
                this.productCodeField = value;
            }
        }

        /// <remarks/>
        public string ProductCategory
        {
            get
            {
                return this.productCategoryField;
            }
            set
            {
                this.productCategoryField = value;
            }
        }

        /// <remarks/>
        public string ProductGroup
        {
            get
            {
                return this.productGroupField;
            }
            set
            {
                this.productGroupField = value;
            }
        }

        /// <remarks/>
        /// 
        [System.Xml.Serialization.XmlArray("PackageType")]
        [System.Xml.Serialization.XmlArrayItem("ValidationResponseConsignmentPackageType", IsNullable = false)]
        public ValidationResponseConsignmentPackageType[] PackageType
        {
            get
            {
                return this.packageTypeField;
            }
            set
            {
                this.packageTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArray("TransportEquipment")]
        [System.Xml.Serialization.XmlArrayItem("ValidationResponseConsignmentTransportEquipment", IsNullable = false)]
        public ValidationResponseConsignmentTransportEquipment[] TransportEquipment
        {
            get
            {
                return this.transportEquipmentField;
            }
            set
            {
                this.transportEquipmentField = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool EDIReceived
        {
            get
            {
                return this.eDIReceivedField;
            }
            set
            {
                this.eDIReceivedField = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool TemperatureRequired
        {
            get
            {
                return this.temperatureRequiredField;
            }
            set
            {
                this.temperatureRequiredField = value;
            }
        }

        /// <remarks/>
        public decimal TemperatureMax
        {
            get
            {
                return this.temperatureMaxField;
            }
            set
            {
                this.temperatureMaxField = value;
            }
        }

        /// <remarks/>

        public bool TemperatureMaxSpecified
        {
            get
            {
                return this.temperatureMaxFieldSpecified;
            }
            set
            {
                this.temperatureMaxFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TemperatureMin
        {
            get
            {
                return this.temperatureMinField;
            }
            set
            {
                this.temperatureMinField = value;
            }
        }

        /// <remarks/>

        public bool TemperatureMinSpecified
        {
            get
            {
                return this.temperatureMinFieldSpecified;
            }
            set
            {
                this.temperatureMinFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TemperatureIdeal
        {
            get
            {
                return this.temperatureIdealField;
            }
            set
            {
                this.temperatureIdealField = value;
            }
        }

        /// <remarks/>

        public bool TemperatureIdealSpecified
        {
            get
            {
                return this.temperatureIdealFieldSpecified;
            }
            set
            {
                this.temperatureIdealFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TemperatureUnit
        {
            get
            {
                return this.temperatureUnitField;
            }
            set
            {
                this.temperatureUnitField = value;
            }
        }

        /// <remarks/>
        public ValidationResponseConsignmentConsignor Consignor
        {
            get
            {
                return this.consignorField;
            }
            set
            {
                this.consignorField = value;
            }
        }

        /// <remarks/>
        public ValidationResponseConsignmentConsignee Consignee
        {
            get
            {
                return this.consigneeField;
            }
            set
            {
                this.consigneeField = value;
            }
        }

        /// <remarks/>
        public ValidationResponseConsignmentThirdPartyReturnInformation ThirdPartyReturnInformation
        {
            get
            {
                return this.thirdPartyReturnInformationField;
            }
            set
            {
                this.thirdPartyReturnInformationField = value;
            }
        }

        /// <remarks/>
        public string CompanyCode
        {
            get
            {
                return this.companyCodeField;
            }
            set
            {
                this.companyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Vas")]
        public VASType[] VAS
        {
            get
            {
                return this.vASField;
            }
            set
            {
                this.vASField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArray("Event")]
        [System.Xml.Serialization.XmlArrayItem("EventType", IsNullable = false)]
        public EventType[] Event
        {
            get
            {
                return this.eventField;
            }
            set
            {
                this.eventField = value;
            }
        }

        /// <remarks/>
        public bool Returned
        {
            get
            {
                return this.returnedField;
            }
            set
            {
                this.returnedField = value;
            }
        }

        /// <remarks/>

        public bool ReturnedSpecified
        {
            get
            {
                return this.returnedFieldSpecified;
            }
            set
            {
                this.returnedFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool PartySuspended
        {
            get
            {
                return this.partySuspendedField;
            }
            set
            {
                this.partySuspendedField = value;
            }
        }

        public bool IsConsignmentEventRegAllowed
        {
            get
            {
                return this.isConsignmentEventRegAllowedField;
            }
            set
            {
                this.isConsignmentEventRegAllowedField = value;
            }
        }

        public bool IsConsignmentEventRegAllowedSpecified
        {
            get
            {
                return this.isConsignmentEventRegAllowedFieldSpecified;
            }
            set
            {
                this.isConsignmentEventRegAllowedFieldSpecified = value;
            }
        }

        public bool IsMassRegistrationAllowed
        {
            get
            {
                return this.isMassRegistrationAllowedField;
            }
            set
            {
                this.isMassRegistrationAllowedField = value;
            }
        }

        public string ActorNumber
        {
            get
            {
                return this.actorNumberField;
            }
            set
            {
                this.actorNumberField = value;
            }
        }

        public bool IsActorBlocked
        {
            get
            {
                return this.isActorBlockedField;
            }
            set
            {
                this.isActorBlockedField = value;
            }
        }

        public bool IsActorBlockedSpecified
        {
            get
            {
                return this.isActorBlockedFieldSpecified;
            }
            set
            {
                this.isActorBlockedFieldSpecified = value;
            }
        }

        public string ShoppingCenterId
        {
            get
            {
                return this.shoppingCenterIdField;
            }
            set
            {
                this.shoppingCenterIdField = value;
            }
        }

        public bool IsSynthetic
        {
            get
            {
                return this.isSyntheticField;
            }
            set
            {
                this.isSyntheticField = value;
            }
        }

        public bool IsSyntheticSpecified
        {
            get
            {
                return this.isSyntheticFieldSpecified;
            }
            set
            {
                this.isSyntheticFieldSpecified = value;
            }
        }
    }

    public partial class ValidationResponseConsignmentConsignmentItem
    {

        private string consignmentItemNumberField;

        private string stopCodeField;

        private bool stopOrderField;

        private string productCodeField;

        private string packageTypeField;

        private decimal weightField;

        private bool weightFieldSpecified;

        private string weightUnitField;

        private decimal heightField;

        private bool heightFieldSpecified;

        private decimal lengthField;

        private bool lengthFieldSpecified;

        private decimal widthField;

        private bool widthFieldSpecified;

        private string hLWUnitField;

        private decimal volumeField;

        private bool volumeFieldSpecified;

        private string volumeUnitField;

        private bool eDIReceivedField;

        private string recipientIdField;

        private EventType[] eventField;

        private VASType[] vASField;

        private bool temperatureRequiredField;

        private decimal temperatureMaxField;

        private bool temperatureMaxFieldSpecified;

        private decimal temperatureMinField;

        private bool temperatureMinFieldSpecified;

        private decimal temperatureIdealField;

        private bool temperatureIdealFieldSpecified;

        private string temperatureUnitField;

        private string companyCodeField;

        private bool handedInField;

        private string destinationPostalCodeField;

        private string destinationCountryCodeField;

        private bool multiItemConsignmentField;

        private System.DateTime plannedDeliveryDateField;

        private bool plannedDeliveryDateFieldSpecified;

        private System.DateTime plannedReturnDateField;

        private bool plannedReturnDateFieldSpecified;

        private bool returnedField;

        private string shelfLocationField;
        private string shelfLocationOrgUnitIdField;
        private System.DateTime dateArrivedField;
        private bool dateArrivedFieldSpecified;
        private System.DateTime dateReturnedField;
        private bool dateReturnedFieldSpecified;
        private System.DateTime dateDeliveredField;
        private bool dateDeliveredFieldSpecified;
        private System.DateTime datePlannedDeliveryField;
        private bool datePlannedDeliveryFieldSpecified;
        private string codePackingField;

        public ValidationResponseConsignmentConsignmentItem()
        {
            plannedDeliveryDateField = new DateTime(0L, DateTimeKind.Utc);
            plannedReturnDateField = new DateTime(0L, DateTimeKind.Utc);
            dateArrivedField = new DateTime(0L, DateTimeKind.Utc);
            dateReturnedField = new DateTime(0L, DateTimeKind.Utc);
            dateDeliveredField = new DateTime(0L, DateTimeKind.Utc);
            datePlannedDeliveryField = new DateTime(0L, DateTimeKind.Utc);
        }

        [System.Xml.Serialization.XmlElementAttribute("ConsignmentItemNumber")]
        public string ConsignmentItemNumber
        {
            get
            {
                return this.consignmentItemNumberField;
            }
            set
            {
                this.consignmentItemNumberField = value;
            }
        }

        /// <remarks/>
        public string StopCode
        {
            get
            {
                return this.stopCodeField;
            }
            set
            {
                this.stopCodeField = value;
            }
        }

        /// <remarks/>
        public bool StopOrder
        {
            get
            {
                return this.stopOrderField;
            }
            set
            {
                this.stopOrderField = value;
            }
        }

        /// <remarks/>
        public string ProductCode
        {
            get
            {
                return this.productCodeField;
            }
            set
            {
                this.productCodeField = value;
            }
        }

        /// <remarks/>
        public string PackageType
        {
            get
            {
                return this.packageTypeField;
            }
            set
            {
                this.packageTypeField = value;
            }
        }

        /// <remarks/>
        public decimal Weight
        {
            get
            {
                return this.weightField;
            }
            set
            {
                this.weightField = value;
            }
        }

        public bool WeightSpecified
        {
            get
            {
                return this.weightFieldSpecified;
            }
            set
            {
                this.weightFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string WeightUnit
        {
            get
            {
                return this.weightUnitField;
            }
            set
            {
                this.weightUnitField = value;
            }
        }

        /// <remarks/>
        public decimal Height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }

        public bool HeightSpecified
        {
            get
            {
                return this.heightFieldSpecified;
            }
            set
            {
                this.heightFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Length
        {
            get
            {
                return this.lengthField;
            }
            set
            {
                this.lengthField = value;
            }
        }

        public bool LengthSpecified
        {
            get
            {
                return this.lengthFieldSpecified;
            }
            set
            {
                this.lengthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Width
        {
            get
            {
                return this.widthField;
            }
            set
            {
                this.widthField = value;
            }
        }

        public bool WidthSpecified
        {
            get
            {
                return this.widthFieldSpecified;
            }
            set
            {
                this.widthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string HLWUnit
        {
            get
            {
                return this.hLWUnitField;
            }
            set
            {
                this.hLWUnitField = value;
            }
        }

        /// <remarks/>
        public decimal Volume
        {
            get
            {
                return this.volumeField;
            }
            set
            {
                this.volumeField = value;
            }
        }

        public bool VolumeSpecified
        {
            get
            {
                return this.volumeFieldSpecified;
            }
            set
            {
                this.volumeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string VolumeUnit
        {
            get
            {
                return this.volumeUnitField;
            }
            set
            {
                this.volumeUnitField = value;
            }
        }

        /// <remarks/>
        public bool EDIReceived
        {
            get
            {
                return this.eDIReceivedField;
            }
            set
            {
                this.eDIReceivedField = value;
            }
        }

        /// <remarks/>
        public string RecipientId
        {
            get
            {
                return this.recipientIdField;
            }
            set
            {
                this.recipientIdField = value;
            }
        }

        [System.Xml.Serialization.XmlArray("Event")]
        [System.Xml.Serialization.XmlArrayItem("EventType", IsNullable = false)]
        public EventType[] Event
        {
            get
            {
                return this.eventField;
            }
            set
            {
                this.eventField = value;
            }
        }

        public VASType[] VAS
        {
            get
            {
                return this.vASField;
            }
            set
            {
                this.vASField = value;
            }
        }

        /// <remarks/>
        public bool TemperatureRequired
        {
            get
            {
                return this.temperatureRequiredField;
            }
            set
            {
                this.temperatureRequiredField = value;
            }
        }

        /// <remarks/>
        public decimal TemperatureMax
        {
            get
            {
                return this.temperatureMaxField;
            }
            set
            {
                this.temperatureMaxField = value;
            }
        }

        public bool TemperatureMaxSpecified
        {
            get
            {
                return this.temperatureMaxFieldSpecified;
            }
            set
            {
                this.temperatureMaxFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TemperatureMin
        {
            get
            {
                return this.temperatureMinField;
            }
            set
            {
                this.temperatureMinField = value;
            }
        }

        /// <remarks/>

        public bool TemperatureMinSpecified
        {
            get
            {
                return this.temperatureMinFieldSpecified;
            }
            set
            {
                this.temperatureMinFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TemperatureIdeal
        {
            get
            {
                return this.temperatureIdealField;
            }
            set
            {
                this.temperatureIdealField = value;
            }
        }

        /// <remarks/>

        public bool TemperatureIdealSpecified
        {
            get
            {
                return this.temperatureIdealFieldSpecified;
            }
            set
            {
                this.temperatureIdealFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TemperatureUnit
        {
            get
            {
                return this.temperatureUnitField;
            }
            set
            {
                this.temperatureUnitField = value;
            }
        }

        /// <remarks/>
        public string CompanyCode
        {
            get
            {
                return this.companyCodeField;
            }
            set
            {
                this.companyCodeField = value;
            }
        }

        /// <remarks/>
        public bool HandedIn
        {
            get
            {
                return this.handedInField;
            }
            set
            {
                this.handedInField = value;
            }
        }

        /// <remarks/>
        public string DestinationPostalCode
        {
            get
            {
                return this.destinationPostalCodeField;
            }
            set
            {
                this.destinationPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string DestinationCountryCode
        {
            get
            {
                return this.destinationCountryCodeField;
            }
            set
            {
                this.destinationCountryCodeField = value;
            }
        }

        /// <remarks/>
        public bool MultiItemConsignment
        {
            get
            {
                return this.multiItemConsignmentField;
            }
            set
            {
                this.multiItemConsignmentField = value;
            }
        }


        public System.DateTime PlannedDeliveryDate
        {
            get
            {
                if (this.plannedDeliveryDateField == DateTime.MinValue && this.plannedDeliveryDateField.Kind == DateTimeKind.Unspecified)
                {
                    this.plannedDeliveryDateField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.plannedDeliveryDateField;
            }
            set
            {
                this.plannedDeliveryDateField = value;
            }
        }

        /// <remarks/>

        public bool PlannedDeliveryDateSpecified
        {
            get
            {
                return this.plannedDeliveryDateFieldSpecified;
            }
            set
            {
                this.plannedDeliveryDateFieldSpecified = value;
            }
        }


        public System.DateTime PlannedReturnDate
        {
            get
            {
                if (this.plannedReturnDateField == DateTime.MinValue && this.plannedReturnDateField.Kind == DateTimeKind.Unspecified)
                {
                    this.plannedReturnDateField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.plannedReturnDateField;
            }
            set
            {
                this.plannedReturnDateField = value;
            }
        }

        /// <remarks/>

        public bool PlannedReturnDateSpecified
        {
            get
            {
                return this.plannedReturnDateFieldSpecified;
            }
            set
            {
                this.plannedReturnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Returned
        {
            get
            {
                return this.returnedField;
            }
            set
            {
                this.returnedField = value;
            }
        }

        public string ShelfLocation
        {
            get
            {
                return this.shelfLocationField;
            }
            set
            {
                this.shelfLocationField = value;
            }
        }

        public string ShelfLocationOrgUnitId
        {
            get
            {
                return this.shelfLocationOrgUnitIdField;
            }
            set
            {
                this.shelfLocationOrgUnitIdField = value;
            }
        }

        public System.DateTime DateArrived
        {
            get
            {
                if (this.dateArrivedField == DateTime.MinValue && this.dateArrivedField.Kind == DateTimeKind.Unspecified)
                {
                    this.dateArrivedField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.dateArrivedField;
            }
            set
            {
                this.dateArrivedField = value;
            }
        }

        public bool DateArrivedSpecified
        {
            get
            {
                return this.dateArrivedFieldSpecified;
            }
            set
            {
                this.dateArrivedFieldSpecified = value;
            }
        }

        public System.DateTime DateReturned
        {
            get
            {
                if (this.dateReturnedField == DateTime.MinValue && this.dateReturnedField.Kind == DateTimeKind.Unspecified)
                {
                    this.dateReturnedField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.dateReturnedField;
            }
            set
            {
                this.dateReturnedField = value;
            }
        }

        public bool DateReturnedSpecified
        {
            get
            {
                return this.dateReturnedFieldSpecified;
            }
            set
            {
                this.dateReturnedFieldSpecified = value;
            }
        }

        public System.DateTime DateDelivered
        {
            get
            {
                if (this.dateDeliveredField == DateTime.MinValue && this.dateDeliveredField.Kind == DateTimeKind.Unspecified)
                {
                    this.dateDeliveredField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.dateDeliveredField;
            }
            set
            {
                this.dateDeliveredField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DateDeliveredSpecified
        {
            get
            {
                return this.dateDeliveredFieldSpecified;
            }
            set
            {
                this.dateDeliveredFieldSpecified = value;
            }
        }

        public System.DateTime DatePlannedDelivery
        {
            get
            {
                if (this.datePlannedDeliveryField == DateTime.MinValue && this.datePlannedDeliveryField.Kind == DateTimeKind.Unspecified)
                {
                    this.datePlannedDeliveryField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.datePlannedDeliveryField;
            }
            set
            {
                this.datePlannedDeliveryField = value;
            }
        }

        public bool DatePlannedDeliverySpecified
        {
            get
            {
                return this.datePlannedDeliveryFieldSpecified;
            }
            set
            {
                this.datePlannedDeliveryFieldSpecified = value;
            }
        }

        public string CodePacking
        {
            get
            {
                return this.codePackingField;
            }
            set
            {
                this.codePackingField = value;
            }
        }
    }

    public partial class ValidationResponseConsignmentPackageType
    {

        private string typeField;

        private string numberOfField;

        //        private decimal weightEstimateField;
        //
        //        private bool weightEstimateFieldSpecified;
        //
        //        private string weightEstimateUnitField;
        //
        //        private object volumeEstimateField;
        //
        //        private string volumeEstimateUnitField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string NumberOf
        {
            get
            {
                return this.numberOfField;
            }
            set
            {
                this.numberOfField = value;
            }
        }

        /// <remarks/>
        //        public decimal WeightEstimate
        //        {
        //            get
        //            {
        //                return this.weightEstimateField;
        //            }
        //            set
        //            {
        //                this.weightEstimateField = value;
        //            }
        //        }
        //
        //        /// <remarks/>
        //        
        //        public bool WeightEstimateSpecified
        //        {
        //            get
        //            {
        //                return this.weightEstimateFieldSpecified;
        //            }
        //            set
        //            {
        //                this.weightEstimateFieldSpecified = value;
        //            }
        //        }
        //
        //        /// <remarks/>
        //        public string WeightEstimateUnit
        //        {
        //            get
        //            {
        //                return this.weightEstimateUnitField;
        //            }
        //            set
        //            {
        //                this.weightEstimateUnitField = value;
        //            }
        //        }
        //
        //        /// <remarks/>
        //        public object VolumeEstimate
        //        {
        //            get
        //            {
        //                return this.volumeEstimateField;
        //            }
        //            set
        //            {
        //                this.volumeEstimateField = value;
        //            }
        //        }
        //
        //        /// <remarks/>
        //        public string VolumeEstimateUnit
        //        {
        //            get
        //            {
        //                return this.volumeEstimateUnitField;
        //            }
        //            set
        //            {
        //                this.volumeEstimateUnitField = value;
        //            }
        //        }
    }

    public partial class ValidationResponseConsignmentTransportEquipment
    {

        private string typeField;

        private string numberOfField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
        public string NumberOf
        {
            get
            {
                return this.numberOfField;
            }
            set
            {
                this.numberOfField = value;
            }
        }
    }

    public partial class ValidationResponseConsignmentConsignor : AddressType
    {
    }

    public partial class ValidationResponseConsignmentConsignee : AddressType
    {

        private AddressType deliveryAddressField;

        private string phoneNumberField;

        /// <remarks/>
        public AddressType DeliveryAddress
        {
            get
            {
                return this.deliveryAddressField;
            }
            set
            {
                this.deliveryAddressField = value;
            }
        }

        /// <remarks/>
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }
    }

    public partial class ValidationResponseConsignmentThirdPartyReturnInformation : AddressType
    {
    }

    public partial class ValidationResponseValidationStatuses
    {
        private string consignmentStatusField;
        private string otherConsignmentItemField;
        private string otherExternalTripIdField;
        private string otherLoadCarrierField;
        private System.DateTime otherEventDateField;
        private bool itemCountWarningField;
        private string validationCodePlanField;
        private string validationCodeRoutingField;
        private string validationEventCodeField;

        public ValidationResponseValidationStatuses()
        {
            otherEventDateField = new DateTime(0L, DateTimeKind.Utc);
        }

        /// <remarks/>
        public string ConsignmentStatus
        {
            get
            {
                return this.consignmentStatusField;
            }
            set
            {
                this.consignmentStatusField = value;
            }
        }

        /// <remarks/>
        public string OtherConsignmentItem
        {
            get
            {
                return this.otherConsignmentItemField;
            }
            set
            {
                this.otherConsignmentItemField = value;
            }
        }

        /// <remarks/>
        public string OtherExternalTripId
        {
            get
            {
                return this.otherExternalTripIdField;
            }
            set
            {
                this.otherExternalTripIdField = value;
            }
        }

        /// <remarks/>
        public string OtherLoadCarrier
        {
            get
            {
                return this.otherLoadCarrierField;
            }
            set
            {
                this.otherLoadCarrierField = value;
            }
        }

        /// <remarks/>
        public System.DateTime OtherEventDate
        {
            get
            {
                if (this.otherEventDateField == DateTime.MinValue && this.otherEventDateField.Kind == DateTimeKind.Unspecified)
                {
                    this.otherEventDateField = new DateTime(0L, DateTimeKind.Utc);
                }

                return this.otherEventDateField;
            }
            set
            {
                this.otherEventDateField = value;
            }
        }

        /// <remarks/>
        [System.ComponentModel.DefaultValueAttribute(false)]
        public bool ItemCountWarning
        {
            get
            {
                return this.itemCountWarningField;
            }
            set
            {
                this.itemCountWarningField = value;
            }
        }

        /// <remarks/>
        public string ValidationCodePlan
        {
            get
            {
                return this.validationCodePlanField;
            }
            set
            {
                this.validationCodePlanField = value;
            }
        }

        public string ValidationCodeRouting
        {
            get
            {
                return this.validationCodeRoutingField;
            }
            set
            {
                this.validationCodeRoutingField = value;
            }
        }

        public string ValidationEventCode
        {
            get
            {
                return this.validationEventCodeField;
            }
            set
            {
                this.validationEventCodeField = value;
            }
        }
    }
}

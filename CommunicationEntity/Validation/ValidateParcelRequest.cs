﻿using System;
using System.Xml.Serialization;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{
    /// <summary>
    /// Represents the ValidateParcelRequest tntity.
    /// </summary>
    [XmlInclude(typeof(DeliveryValidateParcelRequest)), XmlInclude(typeof(ScriptValidateParcelRequest))]
    public class ValidateParcelRequest : EntityBase
    {
        /// <summary>
        /// Gets or sets the carrier id.
        /// </summary>
        /// <value>The carrier id.</value>
        public string CarrierId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>The user id.</value>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the event date time.
        /// </summary>
        /// <value>The event date time.</value>
        public DateTime EventDateTime { get; set; }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        /// <value>The location id.</value>
        public string LocationId { get; set; }

        /// <summary>
        /// Gets or sets the parcel id.
        /// </summary>
        /// <value>The parcel id.</value>
        public string ParcelId { get; set; }

        /// <summary>
        /// Gets or sets the identification.
        /// </summary>
        /// <value>The identification.</value>
        public Guid Identification { get; set; }
    }
}

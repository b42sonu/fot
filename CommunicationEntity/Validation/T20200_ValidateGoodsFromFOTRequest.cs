﻿using System.Xml.Serialization;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{
    public enum ValidateGoodsRequestValidationType
    {
        Consignment,
        ConsignmentItem,
    }

    public enum ValidateGoodsRequestValidationLevel
    {
        Goods,
        Plan,
        Routing,
//        ControlScann,
    }

    public class ValidateGoodsRequestCombinationTripInfo
    {
        public string ExternalTripId { get; set; }
        public string RouteId { get; set; }
        public int StopSequenceNo { get; set; }
        public bool StopSequenceNoFieldSpecified { get; set; }
    }

    [XmlRoot("Validation", Namespace = "http://example.com/schemas/Validation")]
    public class LoggingInformationType
    {
        public string LogonName { get; set; }
        public string SystemCode { get; set; }
        public string LanguageCode { get; set; }
        public string CompanyCode { get; set; }
        public string CountryCode { get; set; }
        public string OrgUnitId { get; set; }
        public string UserRole { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
        public string SubProcessVersion { get; set; }
        public string InterfaceVersion { get; set; }
        public System.DateTime InvocationTime { get; set; }
        public string MessageId { get; set; }
    }

    public class T20200_ValidateGoodsFromFOTRequest : EntityBase
    {
        public SyncHeader Header;
        public string ConsignmentNumber;
        public string ConsignmentItemNumber;
        public string DeliveryCode;
        public string TripId;
        public string StopId;
        public string ActionType;
        public string EventCode;
        public string ExternalTripId;
        public string RouteId;
        public int StopSequenceNo;
        public string LoadCarrierId;
        public string PowerUnitId;
        public string OrgUnitIdForAction;
        public string LocationID;
        public string GetEvents;
        public bool GetVASPayment;
        public bool GetExtended;
        public bool GetPackageType;
        public bool GetAllItems;

        public string CustomerId;
        public string OrderNumber;
        public string TMSAffiliationId;

        public ValidateGoodsRequestCombinationTripInfo CombinationTripInfo;
        public ValidateGoodsRequestValidationType ValidationType;
        public ValidateGoodsRequestValidationLevel ValidationLevel;
        public Loading.LoggingInformationType LoggingInformation;
    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation
{
    /// <summary>
    /// Represents the DeliveryValidateParcelReply entity. Empty class that implements 
    /// base class due to the nature of how PreCom object communication works.
    /// </summary>
    public class DeliveryValidateParcelReply : ValidateParcelReply
    {
    }
}

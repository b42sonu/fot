﻿namespace Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList
{
    public partial class StopType
    {
        /// <summary>
        /// below properties added for implemnt sorting of stops in amphora locally...
        /// </summary>
        private int _originalSortNo;
        public int OriginalSortNo
        {
            get
            {
                return _originalSortNo;
            }
            set
            {
                _originalSortNo = value;
            }
        }
    }
}
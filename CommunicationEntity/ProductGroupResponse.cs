﻿using System;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity
{
    public class ProductGroupResponse
    {
        public List<ProductGroupItem> ProductGroupItemList { get; set; }
        public List<ProductGroupTransitionItem> ProductGroupTransitionList { get; set; }
    }

    public class ProductGroupTransitionItem
    {
        public string FromCode { get; set; }
        public string ToCode { get; set; }
        public string TmsId { get; set; }
    }

    public class ProductGroupItem
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string TmsId { get; set; }
    }
}

﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity
{
    /// <summary>
    /// Represents the PdfWaybill entity.
    /// </summary>
    public class Consignment : EntityBase
    {
        /// <summary>
        /// Gets or sets the consignment number.
        /// </summary>
        /// <value>The consignment number.</value>
        public string ConsignmentNumber { get; set; }

        /// <summary>
        /// Gets or sets the number of parcels.
        /// </summary>
        /// <value>The number of parcels.</value>
        public int NumberOfParcels { get; set; }

        /// <summary>
        /// Gets or sets the value added services.
        /// </summary>
        /// <value>The value added services.</value>
        public ValueAddedService[] ValueAddedServices { get; set; }

        /// <summary>
        /// Gets or sets the parcels.
        /// </summary>
        /// <value>The parcels.</value>
        public Parcel[] Parcels { get; set; }
    }
}

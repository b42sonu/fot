﻿using System;
using System.Globalization;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList
{
    public partial class PlannedOperationType
    {
        public string GetTitle(bool useDefaultIfNoInfoFound)
        {
            string title = String.Empty;

            switch (OperationType)
            {
                case PlannedOperationTypeOperationType.DELIVERY:
                    title = GetConsigneeName();
                    break;

                case PlannedOperationTypeOperationType.LOAD:
                case PlannedOperationTypeOperationType.LOAD_DISTRIBUTION:
                case PlannedOperationTypeOperationType.LOAD_LINE_HAUL:
                case PlannedOperationTypeOperationType.UNLOAD:
                case PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL:
                case PlannedOperationTypeOperationType.UNLOAD_PICKUP:
                    title = GetTitleForUnAndLoading();
                    break;

                case PlannedOperationTypeOperationType.PICKUP:
                    title = GetConsignorAddressOrName();
                    break;
            }
            if (useDefaultIfNoInfoFound && title == String.Empty)
                return GetDefaultChildTitle();
            return title;
        }

        private string GetConsignorAddressOrName()
        {
            var consignor = GetFirstConsignor();
            if (consignor != null)
            {
                bool hasConsignorAddress = consignor.Address2 != null;
                if (hasConsignorAddress)
                    return consignor.Address2;

                bool hasConsignorName = consignor.Name1 != null;
                if (hasConsignorName)
                    return consignor.Name1;
            }
            return String.Empty;
        }

        public string GetConsignorName()
        {
            var consignor = GetFirstConsignor();
            if (consignor != null)
            {
                bool hasConsignorName = consignor.Name1 != null;
                if (hasConsignorName)
                    return consignor.Name1;
            }
            return String.Empty;
        }
        public string GetConsignorFullAddress()
        {
            var consignor = GetFirstConsignor();
            if (consignor != null)
            {
                bool hasConsignorAddress = consignor.Address1 != null;
                if (hasConsignorAddress)
                {
                    return consignor.Address1 + " " + consignor.PostalCode + " " + consignor.PostalName;
                }

            }
            return String.Empty;
        }
        public string GetConsigneeFullAddress()
        {
            var consignee = GetFirstConsignee();
            if (consignee != null)
            {
                bool hasConsigneeAddress = consignee.Address1 != null;
                if (hasConsigneeAddress)
                {
                    return consignee.Address1 + " " + consignee.PostalCode + " " + consignee.PostalName;
                }

            }
            return String.Empty;
        }



        public String GetConsigneeName()
        {
            bool hasConsignment = PlannedConsignments != null &&
                                  PlannedConsignments.Length > 0;
            if (hasConsignment)
            {
                bool hasConsignee = PlannedConsignments[0].Consignee != null;
                if (hasConsignee)
                {
                    bool hasConsigneeName = PlannedConsignments[0].Consignee.Name1 != null;
                    if (hasConsigneeName)
                        return PlannedConsignments[0].Consignee.Name1;

                }

            }
            return String.Empty;
        }

        private String GetTitleForUnAndLoading()
        {
            bool hasLoadingCarrierId = LoadCarrierId != null;
            if (hasLoadingCarrierId)
            {
                return LoadCarrierId;
            }
            return String.Empty;
        }


        public String GetDefaultChildTitle()
        {
            return String.Format("{0:t}", EarliestStartTime) + "-" + String.Format("{0:t}", LatestStartTime);
        }

        private ConsignorType GetFirstConsignor()
        {
            bool hasConsignment = PlannedConsignments != null &&
                                  PlannedConsignments.Length > 0;
            if (hasConsignment)
            {
                bool hasConsignor = PlannedConsignments[0].Consignor != null;
                if (hasConsignor)
                    return PlannedConsignments[0].Consignor;
            }
            return null;
        }

        private ConsigneeType GetFirstConsignee()
        {
            bool hasConsignment = PlannedConsignments != null &&
                                  PlannedConsignments.Length > 0;
            if (hasConsignment)
            {
                bool hasConsignor = PlannedConsignments[0].Consignee != null;
                if (hasConsignor)
                    return PlannedConsignments[0].Consignee;
            }
            return null;
        }

        public string GetFormattedWeight()
        {
            return Weight != null ? Decimal.Round(Weight.Amount, 5).ToString(CultureInfo.InvariantCulture) + " " + Weight.MeasureUnit : string.Empty;
        }

        public string GetFormattedVolume()
        {
            return Volume != null ? Decimal.Round(Volume.Amount, 5).ToString(CultureInfo.InvariantCulture) + " " + Volume.MeasureUnit : string.Empty;
        }

        public string GetWeightWithUnitFormat()
        {
            return Weight != null ? Weight.MeasureUnit + ":" + Convert.ToInt32(Weight.Amount) : string.Empty;
        }

        public string GetVolumeWithUnitFormat()
        {
            return Volume != null ? Volume.MeasureUnit + ":" + Convert.ToInt32(Volume.Amount) : string.Empty;
        }


        public string GetOrderNumber()
        {
            bool hasConsignment = PlannedConsignments != null &&
                                 PlannedConsignments.Length > 0;
            if (hasConsignment)
            {
                bool hasConsignor = PlannedConsignments[0].Consignee != null;
                if (hasConsignor)
                    return PlannedConsignments[0].OrderNumber;
            }
            return String.Empty;
        }

        public string GetCountryCode()
        {
            bool hasConsignment = PlannedConsignments != null && PlannedConsignments.Length > 0;
            if (hasConsignment)
            {
                bool hasConsignor = PlannedConsignments[0].Consignee != null;
                if (hasConsignor)
                    return PlannedConsignments[0].OrderNumber;
            }
            return String.Empty;
        }

        public string GetStopInfo()
        {
            bool hasNotes = operationNotesField != null &&
                                 operationNotesField.Length > 0;
            if (hasNotes)
            {
                return operationNotesField[0].Text;
            }
            return String.Empty;
        }

        public string GetConsigneeBothAddress()
        {
            var consignee = GetFirstConsignee();
            if (consignee != null)
            {
                bool hasConsigneeAddress = consignee.Address1 != null;
                if (hasConsigneeAddress)
                {
                    return consignee.Address1 + " " + consignee.Address2 + consignee.PostalCode + " " + consignee.PostalName;
                }

            }
            return String.Empty;
        }
    }
}
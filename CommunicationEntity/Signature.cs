﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity
{
    /// <summary>
    /// Represents the Signature entity.
    /// </summary>
    public class Signature : EntityBase
    {
        /// <summary>
        /// Gets or sets the name of the printed.
        /// </summary>
        /// <value>The name of the printed.</value>
        public string PrintedName { get; set; }

        /// <summary>
        /// Gets or sets the sketch.
        /// </summary>
        /// <value>The sketch.</value>
        public byte[] Sketch { get; set; }

        public Signature Clone()
        {
            var result = (Signature)MemberwiseClone();
            return result;
        }
    }
}

﻿using System;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity
{
    public partial class MessageToDriver
    {
       
        public string UserId { get; set; }
        public string Status { get; set; }
        public int ImageSign
        {
            get
            {
                if (Status == "U")
                    return 1;
                return 0;
            }
        }
        public string FormattedDate
        {
            get
            {
                if (MessageTime.Date != DateTime.Now.Date)
                    return MessageTime.Day + "/" + MessageTime.Month;
                //return MessageTime.Hour + ":" + MessageTime.Minute;//TODO: Sweden Time Check
                return MessageTime.TimeOfDay.ToString().Substring(0, 5);
            }
        }
    }
}

﻿using System;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity
{
    public class SyncHeader
    {
        private DateTime _firstProcessedTimestamp;
        private DateTime _processedTimestamp;
        private DateTime _sourceSystemTimestamp;
        private DateTime _targetSystemTimestamp;

        public SyncHeader()
        {
            _firstProcessedTimestamp = new DateTime(0L, DateTimeKind.Utc);
            _processedTimestamp = new DateTime(0L, DateTimeKind.Utc);
            _sourceSystemTimestamp = new DateTime(0L, DateTimeKind.Utc);
            _targetSystemTimestamp = new DateTime(0L, DateTimeKind.Utc);
        }

        public string MessageId { get; set; }
        public string MessageType { get; set; }
        public string MessageMode { get; set; }
        public string ContextReference { get; set; }
        public string Action { get; set; }
        public string Version { get; set; }
        public System.DateTime FirstProcessedTimestamp
        {
            get
            {
                if (this._firstProcessedTimestamp == DateTime.MinValue && this._firstProcessedTimestamp.Kind == DateTimeKind.Unspecified)
                {
                    this._firstProcessedTimestamp = new DateTime(0L, DateTimeKind.Utc);
                }

                return _firstProcessedTimestamp;
            }
            set { _firstProcessedTimestamp = value; }
        }

        public bool FirstProcessedTimestampSpecified{ get; set; }
        public System.DateTime ProcessedTimestamp
        {
            get
            {
                if (this._processedTimestamp == DateTime.MinValue && this._processedTimestamp.Kind == DateTimeKind.Unspecified)
                {
                    this._processedTimestamp = new DateTime(0L, DateTimeKind.Utc);
                }
                
                return _processedTimestamp;
            }
            set { _processedTimestamp = value; }
        }

//        public bool ProcessedTimestampFieldSpecified { get; set; }
        public bool ProcessedTimestampSpecified { get; set; }
        public System.DateTime SourceSystemTimestamp
        {
            get
            {
                if (this._sourceSystemTimestamp == DateTime.MinValue && this._sourceSystemTimestamp.Kind == DateTimeKind.Unspecified)
                {
                    this._sourceSystemTimestamp = new DateTime(0L, DateTimeKind.Utc);
                } 
                
                return _sourceSystemTimestamp;
            }
            set { _sourceSystemTimestamp = value; }
        }

        public System.DateTime TargetSystemTimestamp
        {
            get
            {
                if (this._targetSystemTimestamp == DateTime.MinValue && this._targetSystemTimestamp.Kind == DateTimeKind.Unspecified)
                {
                    this._targetSystemTimestamp = new DateTime(0L, DateTimeKind.Utc);
                } 
                
                return _targetSystemTimestamp;
            }
            set { _targetSystemTimestamp = value; }
        }

//        public bool TargetSystemTimestampFieldSpecified { get; set; }
        public bool TargetSystemTimestampSpecified { get; set; }
        public string SecurityToken { get; set; }
        public string SourceCompany { get; set; }
        public string SourceSystem { get; set; }
        public string SourceSystemUser { get; set; }
        public string SourceSystemRef { get; set; }
    }
}

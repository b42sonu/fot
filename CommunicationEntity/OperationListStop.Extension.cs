﻿using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Entity;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList
{
    public partial class OperationListStop
    {
        protected bool Equals(OperationListStop other)
        {
            if (other.PlannedOperation != null && other.plannedOperationField[0] != null && PlannedOperation[0] != null && PlannedOperation[0] != null)
                return other.PlannedOperation[0].OrderID == PlannedOperation[0].OrderID;
            return false;
        }

        public bool IsActive
        {
            get
            {
                return PlannedOperation.Any(operation => operation != null &&
                    (operation.Status == OperationStatus.New || operation.Status == OperationStatus.Started));
            }
        }

        public bool IsNew
        {
            get
            {
                return PlannedOperation.Any(operation => operation != null && operation.Status == OperationStatus.New);
            }
        }


        public bool IsStarted
        {
            get
            {
                return PlannedOperation.Any(operation => operation != null && operation.Status == OperationStatus.Started);
            }
        }

        public bool IsOrderMember(string orderId)
        {
            return PlannedOperation[0].OrderID == orderId;
        }

        public string GetCustomerName()
        {
            string customerName = string.Empty;
            if (AlystraBexOrderLevel != null && AlystraBexOrderLevel.OrderInfo != null && AlystraBexOrderLevel.OrderInfo.Any())
            {
                customerName = AlystraBexOrderLevel.OrderInfo[0].CustomerName;
            }
            return customerName;
        }

        public string GetOrderReference()
        {
            string orderReference = string.Empty;
            if (AlystraBexOrderLevel != null && AlystraBexOrderLevel.OrderInfo != null && AlystraBexOrderLevel.OrderInfo.Any())
            {
                orderReference = AlystraBexOrderLevel.OrderInfo[0].OrderReference;
            }
            return orderReference;
        }

        public string GetDriverInformation()
        {
            string driverInformation = string.Empty;
            if (AlystraBexOrderLevel != null && AlystraBexOrderLevel.OrderInfo != null && AlystraBexOrderLevel.OrderInfo.Any())
            {
                driverInformation = AlystraBexOrderLevel.OrderInfo[0].DriverInformation;
            }
            return driverInformation;
        }
    }
}

﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity
{
    /// <summary>
    /// Represents the ValueAddedService entity.
    /// </summary>
    public class ValueAddedService : EntityBase
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets the currency code.
        /// </summary>
        /// <value>The currency code.</value>
        public string CurrencyCode { get; set; }
    }
}

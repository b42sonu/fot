namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    /// <summary>
    /// Custom user status.
    /// </summary>
    public enum CustomUserStatus
    {
        Created = 0,
        Active = 1,
        Disabled = 2,
        Deleted = 3,
        DisabledUntilStartDate = 4
    }
}
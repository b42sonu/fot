﻿using PreCom.Core.Communication;
namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    public class UserProfileRequest: EntityBase
    {
        public string UserId { get; set; }
    }
}

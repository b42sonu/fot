using System.Collections.Generic;
using PreCom.DirectoryServices.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    /// <summary>
    /// Entity containing group information.
    /// </summary>
    public class GroupInformation
    {
        /// <summary>
        /// Gets or sets group.
        /// </summary>
        public Group Group
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or set custom group.
        /// </summary>
        public CustomGroup GroupDetail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets setting list.
        /// </summary>
        public List<CustomSetting> Settings
        {
            get;
            set;
        }
    }
}

using PreCom.DirectoryServices.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    /// <summary>
    /// Custom group class.
    /// </summary>
    public class CustomGroup : DirectoryEntity<CustomGroup>
    {
        /// <summary>
        /// Get or set language.
        /// </summary>
        public string Language
        {
            get;
            set;
        }

        /// <summary>
        /// Get display information of the custom group.
        /// </summary>
        /// <returns>Custom group information.</returns>
        public override string ToString()
        {
            return Language;
        }
    }
}

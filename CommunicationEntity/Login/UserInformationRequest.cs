using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    /// <summary>
    /// Class containing user detail.
    /// </summary>
    public class UserInformationRequest : EntityBase
    {
        /// <summary>
        /// Gets or sets user credential id.
        /// </summary>
        public int CredentialId
        {
            get;
            set;
        }
    }
}

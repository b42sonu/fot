﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    public class T20210_DriverAvailabilityToTMSRequest : EntityBase
    {
        public SyncHeader Header; 
        public string UserLogonId; 
        public string UserRoleId;
        public string OrgUnitId;
        public string CountryCode;
        public string CompanyCode;
        public string TMSAffiliationId;
        public string LoadCarrierId;
        public string PowerLoadCarrierId;
        public string PostalCode;
        public string UserPhoneNumber;
        public string HeartbeatIntervalMinutes;
        public bool Available;
        public string CaptureEquipmentId;
        public bool Override;
        public string CorrelationId;
        public string SessionId;
        public string Version;
        public string ProcessName;
        public string SubProcessName;
        public string SubProcessVersion;
    }
}

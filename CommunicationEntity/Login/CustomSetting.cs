﻿using PreCom.DirectoryServices.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    /// <summary>
    /// Setting entity.
    /// </summary>
    public class CustomSetting : DirectoryEntity<CustomSetting>
    {
        /// <summary>
        /// Gets or sets setting name.
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or set setting value.
        /// </summary>
        public string Value
        {
            get;
            set;
        }

        /// <summary>
        /// Get display value of setting instance.
        /// </summary>
        /// <returns>Setting diplay value.</returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}", Name, Value);
        }
    }
}

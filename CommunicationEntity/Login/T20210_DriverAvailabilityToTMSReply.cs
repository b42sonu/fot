﻿using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    public class DriverAvailabilityResponseNextBreak
    {
        private DateTime _startTime;

        public DriverAvailabilityResponseNextBreak()
        {
            _startTime = new DateTime(0L, DateTimeKind.Utc);
        }

        public DateTime StartTime
        {
            get
            {
                if (_startTime == DateTime.MinValue && _startTime.Kind == DateTimeKind.Unspecified)
                {
                    _startTime = new DateTime(0L, DateTimeKind.Utc);
                }

                return _startTime;
            }
            set { _startTime = value; }
        }

        public int DurationInSeconds { get; set; }

    }

    public class T20210_DriverAvailabilityToTMSReply : EntityBase
    {
        private DateTime _tmsLogOnTime;
        private DateTime _sessionOverTakenTime;

        public T20210_DriverAvailabilityToTMSReply()
        {
            _tmsLogOnTime = new DateTime(0L, DateTimeKind.Utc);
            _sessionOverTakenTime = new DateTime(0L, DateTimeKind.Utc);
        }

        public SyncHeader Header;
        public DateTime TMSLogOnTime
        {
            get
            {
                if (_tmsLogOnTime == DateTime.MinValue && _tmsLogOnTime.Kind == DateTimeKind.Unspecified)
                {
                    _tmsLogOnTime = new DateTime(0L, DateTimeKind.Utc);
                }

                return _tmsLogOnTime;
            }
            set { _tmsLogOnTime = value; }
        }

        public string State;
        public string ErrorMessage;
        public string SessionId;
        public DriverAvailabilityResponseNextBreak NextBreak;
        public bool SessionOvertaken;
        public string SessionOvertakenBy;
        public DateTime SessionOvertakenTime
        {
            get
            {
                if (_sessionOverTakenTime == DateTime.MinValue && _tmsLogOnTime.Kind == DateTimeKind.Unspecified)
                {
                    _sessionOverTakenTime = new DateTime(0L, DateTimeKind.Utc);
                }

                return _sessionOverTakenTime;
            }
            set { _sessionOverTakenTime = value; }
        }

        public string UserLogonId;
        public string CaptureEquipmentId;
        public string CorrelationId;
    }
}

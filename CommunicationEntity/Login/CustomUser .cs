using System;
using PreCom.DirectoryServices.Entity;
using System.Text;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    public class CustomUser : DirectoryEntity<CustomUser>, ICloneable
    {
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _deprovisioningDate;

        public CustomUser()
        {
            _deprovisioningDate = new DateTime(0L, DateTimeKind.Utc);
            _endDate = new DateTime(0L, DateTimeKind.Utc);
            _startDate = new DateTime(0L, DateTimeKind.Utc);
        }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public CustomUserStatus Status { get; set; }
        public bool IsEmployee { get; set; }
        public int EmployeeNumber { get; set; }
        public DateTime StartDate
        {
            get
            {
                if (_startDate == DateTime.MinValue && _startDate.Kind == DateTimeKind.Unspecified)
                {
                    _startDate = new DateTime(0L, DateTimeKind.Utc);
                }

                return _startDate;
            }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get
            {
                if (_endDate == DateTime.MinValue)
                {
                    _endDate = new DateTime(0L, DateTimeKind.Utc);
                }

                return _endDate;
            }
            set { _endDate = value; }
        }

        public DateTime DeprovisioningDate
        {
            get
            {
                if (_deprovisioningDate == DateTime.MinValue)
                {
                    _deprovisioningDate = new DateTime(0L, DateTimeKind.Utc);
                }

                return _deprovisioningDate;
            }
            set { _deprovisioningDate = value; }
        }

        public string OrganizationUnitId { get; set; }   //Enhetsnr
        public string WorkContactEmail { get; set; }
        public string Language { get; set; }

        public string ProfileUserRole { get; set; }
        public string ProfileOrgUnitId { get; set; }
        public string ProfilePowerUnit { get; set; }
        public string ProfileLoadCarrier { get; set; }
        public string ProfileTmsAffiliationId { get; set; }
        public string ProfileRouteId { get; set; }
        public string ProfileCompanyCode { get; set; }
        public string ProfileTelephoneNumber { get; set; }
        public bool ProfileGPSEnabled { get; set; }
        public string ProfileDefaultPrinter { get; set; }
        
        /// <summary>
        /// Get display information of the custom user.
        /// </summary>
        public override string ToString()
        {
            var sbUser = new StringBuilder();
            sbUser.Append(FirstName);
            if (!string.IsNullOrEmpty(MiddleName))
            {
                sbUser.AppendFormat(", {0}", MiddleName);
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                sbUser.AppendFormat(", {0}", LastName);
            }

            sbUser.AppendFormat(", {0}", Status.ToString());

            return sbUser.ToString();
        }

        public CustomUser Clone()
        {
            return (CustomUser)MemberwiseClone();
        }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return Clone();
        }

        #endregion
    }
}
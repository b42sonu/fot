﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;


namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    public enum Role
    {
        Unknown,
        Driver1,
        Driver2,
        HubWorker1,
        Customer,
        Landpostbud,
        PostOfficeAndStore,
        HomeDeliveryHub
    }

    public class RoleUtil
    {
        public const string Driver1 = "Driver1";
        public const string Driver2 = "Driver2";
        public const string HubWorker1 = "HubWorker1";
        public const string Customer = "Customer";
        public const string Landpostbud = "Landpostbud";

        public const string PostOfficeAndStore = "PostOfficeAndStore"; // PK/PIB
        public const string HomeDeliveryHub = "HomeDeliveryHub"; //HDH

        public static Role GetRole(string roleString)
        {
            switch (roleString)
            {
                case Driver1:
                    return Role.Driver1;
                case Driver2:
                    return Role.Driver2;
                case HubWorker1:
                    return Role.HubWorker1;
                case Customer:
                    return Role.Customer;
                case Landpostbud:
                    return Role.Landpostbud;
                case PostOfficeAndStore:
                    return Role.PostOfficeAndStore;
                case HomeDeliveryHub:
                    return Role.HomeDeliveryHub;

                default:
                    Logger.LogEvent(Severity.Error, "Cannot convert role string '{0}' to enum RoleName", roleString);
                    return Role.Unknown;
            }
        }
    };


    public class UserProfile
    {
        public UserProfile() { }


        public static UserProfile Copy(UserProfile userProfile)
        {
            return userProfile.MemberwiseClone() as UserProfile;
        }

        public UserProfile(UserInformation userInformation)
        {
            var customUser = userInformation.UserDetail;

            CredentialId = userInformation.User.Id;

            //get user name from database
            UserId = SettingDataProvider.Instance.GetCredentialsValue("username");

            RoleName = customUser.ProfileUserRole;
            Roles = userInformation.Roles;
            if (Roles.Count == 0)
                Logger.LogEvent(Severity.Error, "User has no roles defined");
            // If role is not set or role not a legal role, take first of authorized roles
            if (string.IsNullOrEmpty(RoleName) || Roles.All(role => role != RoleName))
            {
                RoleName = Roles[0];
            }

            OrgUnitId = customUser.ProfileOrgUnitId;
            PowerUnit = customUser.ProfilePowerUnit;
            LoadCarrier = customUser.ProfileLoadCarrier;
            TmsAffiliation = customUser.ProfileTmsAffiliationId;
            RouteId = customUser.ProfileRouteId;
            CompanyCode = customUser.ProfileCompanyCode;
            TelephoneNumber = customUser.ProfileTelephoneNumber;
            DefaultPrinter = customUser.ProfileDefaultPrinter;

            FirstName = userInformation.UserDetail.FirstName;
            LastName = userInformation.UserDetail.LastName;
            GPSEnabled = userInformation.UserDetail.ProfileGPSEnabled;
        }


        private int _credentialId;
        public int CredentialId
        {
            get
            {
                if (_credentialId == 0)
                {
                    _credentialId = ModelModules.Communication.GetClient().CredentialId;
                }

                return _credentialId;
            }

            private set { _credentialId = value; }
        }

        public string UserId { get; set; }
        public string RoleName { get; set; }

        private string _orgUnitId;
        public string OrgUnitId
        {
            get { return _orgUnitId; }
            set
            {
                _orgUnitId = value;
                PostalCode = SettingDataProvider.Instance.GetPostalCode(_orgUnitId);
                CountryCode = SettingDataProvider.Instance.GetCountryCode(_orgUnitId);
            }
        }

        public string PostalCode { get; private set; } // Retrieved from org-unit
        public string CountryCode { get; private set; } // Retrieved from org-unit

        public string CompanyCode { get; set; }
        public string PowerUnit { get; set; }
        public string LoadCarrier { get; set; }

        private string _tmsAffiliation;
        public string TmsAffiliation
        {
            get
            {
                if (RoleName == RoleUtil.Driver1)
                    return _tmsAffiliation;
                return Constants.TmsAffiliation.NoTms;
            }

            set { _tmsAffiliation = value; }
        }


        public string RouteId { get; set; }

        public string TelephoneNumber { get; set; }
        public string DefaultPrinter { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool GPSEnabled { get; set; }

        /// <summary>
        /// What Roles the user have access to
        /// </summary>
        public List<string> Roles { get; set; }

        /// <summary>
        /// Current Role
        /// </summary>
        public Role Role
        {
            get { return RoleUtil.GetRole(RoleName); }
        }

        public bool IsDriverOrTerminalWorker
        {
            get { return ModelUser.UserProfile.IsDriver || ModelUser.UserProfile.IsHubWorker; }
        }



        public bool IsHubWorker
        {
            get
            {
                var role = Role;
                return role == Role.HubWorker1;
            }
        }

        public bool IsDriver
        {
            get
            {
                var role = Role;
                return role == Role.Driver1 || role == Role.Driver2;
            }
        }

        public bool IsDriverAvailabilityUser
        {
            get
            {
                bool result = false;
                Logger.LogEvent(Severity.Debug, "IsDriverAvailabilityUser");
                if (RoleName == string.Empty)
                {
                    Logger.LogEvent(Severity.ExpectedError, "IsDriverAvailabilityUser;RoleName is blank");
                    return false;
                }
                switch (RoleUtil.GetRole(RoleName))
                {
                    case Role.Driver1:
                        if (TmsAffiliation != Constants.TmsAffiliation.NoTms)
                            result = true;
                        break;
                }
                return result;
            }
        }

        //Returs the language code corresponding LangCode in synced table like Action and Reason.
        public string LanguageCode
        {
            get
            {
                var preComLanguage = PreCom.Utils.Language.CurrentLanguage;

                switch (preComLanguage)
                {
                    case "no-Norwegian":
                    case "rf-Ref":
                    case "re-Ref":
                        return "nob";
                    case "en-US":
                        return "eng";
                    case "se-Swedish":
                        return "se";
                }
                return preComLanguage;
            }
        }

        /// <summary>
        ///  property used for get the login user org. unit name from local database on the basis of org. unit id..
        /// </summary>
        public string OrgUnitName
        {
            get
            {
                try
                {
                    return SettingDataProvider.Instance.GetLocationName(OrgUnitId);
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "ModelUser.OrgUnitName");
                    return null;
                }
            }
        }
    }
}

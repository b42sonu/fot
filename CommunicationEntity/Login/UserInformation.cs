﻿using System.Collections.Generic;
using PreCom.Core.Communication;
using PreCom.DirectoryServices.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    /// <summary>
    /// Entity containing user information.
    /// </summary>

    public class UserInformation : EntityBase
    {
        /// <summary>
        /// Gets or sets user.
        /// </summary>
        public User User
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets custom user.
        /// </summary>
        public CustomUser UserDetail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets user setting list.
        /// </summary>
        public List<CustomSetting> Settings
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets user information list.
        /// </summary>
        public List<GroupInformation> Groups
        {
            get;
            set;
        }

        /// <summary>
        /// What Roles the user have access to
        /// </summary>
        public List<string> Roles { get; set; } 
    }
}

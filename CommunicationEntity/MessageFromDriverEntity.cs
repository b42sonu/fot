﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity
{
    public partial class MessageFromDriver : EntityBase
    {
        public string UserId { get; set; }
        public string Status { get; set; }
        public int ImageSign
        {
            get
            {
                if (Status == "U")
                    return 1;
                else
                    return 0;
            }
        }
    }
}

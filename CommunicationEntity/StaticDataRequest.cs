﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity
{
    public enum InterfaceTypes
    {
        i334,   // HentOrgEnhet (OrganisationalUnit)
        i311,   // ValiderKollihendelse (ValidateGoods)
        i335,   // HentArsak    (Reason)
        i336,   // HentTiltak   (Actions)
        i338,   // HentLovligKodekombinasjon (EventReasonAction-combo)
        i20284, // GetLocationUnit (OrgUnit)
        i20286, // GetLegalCodeCombinagtions
        i20237, // GetProductGroupInfo
    }

    public class StaticDataRequest : PreCom.Core.Communication.EntityBase
    {
        public InterfaceTypes Interface { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity
{
    public class StaticDataResponse
    {
        public List<StaticDataResponseItem> StaticDataResponseItemList { get; set; }
    }

    public class StaticDataResponseItem
    {
        // OrgUnit.
        public string Name { get; set; }            // 0
        public string Number { get; set; }          // 1
        public string PostalCode { get; set; }      // 2
        public string Type { get; set; }            // 3
        // Reason and Action (Arsak and Tiltak)
        public string Code { get; set; }            // 4
        public string Text { get; set; }            // 5
        public string LangCode { get; set; }        // 6
        public string TMSId { get; set; }           // 7
        // ReasonActionEvent
        public string Code2 { get; set; }           // 8  (Action code)
        public string Event { get; set; }           // 9
        // More OrgUnit
        public string CountryCode { get; set; }     // 10
        public string LocationUnit { get; set; }    // 11   
        // ReasonActionEvent
        public string Role { get; set; }            // 12
    }
}

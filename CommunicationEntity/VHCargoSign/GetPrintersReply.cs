﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.VHCargoSign
{
    public class GetPrintersReply : EntityBase
    {
        [System.Xml.Serialization.XmlArray("PrintersForLocation")]
        [System.Xml.Serialization.XmlArrayItem("string", IsNullable = false)]
        public string[] PrintersForLocation { get; set; }
    }
}

﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.VHCargoSign
{
    public class VH_PrintBCReportRequest : EntityBase
    {
        public string locationNumber;
        public string reportNumber;
        public string reportOutputFormat;
        public string extraParameters;
    }
}

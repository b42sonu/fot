﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.VHCargoSign
{
    public class VH_PrintCDReply : EntityBase
    {
        public string PrintCDResult;
    }
}

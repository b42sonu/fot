﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.VHCargoSign
{
    public class VH_PrintCDRequest : EntityBase
    {
        // The members have to be "fields" since PrintCDRequestBody is defined with fields and not properties.
        public string locationNumber;
        public string LBid;            // Logical loadcarrier
        public string printerName;
    }
}

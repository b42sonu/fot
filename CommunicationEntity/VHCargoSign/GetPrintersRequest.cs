﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.VHCargoSign
{
    public class GetPrintersRequest : EntityBase
    {
        public string locationNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class for making detailed readonly views for controller webBrowser
    /// </summary>
    public class HtmlUtil
    {
        private readonly StringBuilder _html;

        public HtmlUtil()
        {
            _html = new StringBuilder();
            Start();
            
        }

        private void Start()
        {
            _html.Append("<html><body bgcolor='#D6D7DE'>");

        }

        private void End()
        {
           _html.Append("</body></html>");
        }

        public void AddTitle(string title)
        {
            _html.Append(String.Format("<p><strong>{0}</strong></p>", title));
        }

        public void Newline(string text)
        {
            _html.Append(String.Format("<p>{0}</p>", text));
        }
        public void Newline()
        {
            _html.Append("<p></p>");
        }
        public void NewlineWithOutSpace()
        {
            _html.Append("</br>");
        }

        public void StartTable()
        {
            _html.Append("<table width=\"100%\"><tbody>");
        }

        public void StartRow()
        {
            _html.Append("<tr width=\"100%\"><td>");
        }

        public void AddRowInTable(string text)
        {
            StartRow();
            _html.Append(text);
            EndRow();
        }


        public void EndRow()
        {
            _html.Append("</td></tr>");
        }



        public void EndTable()
        {
            _html.Append("</tbody></table>");
        }

        public void AddToSimplTable(string description, string value)
        {
            AddToSimplTableWithEmptyRow(description, value, true);
        }

        public void AddToSimplTableWithEmptyRow(string description, string value, bool needEmptyRow)
        {
            var tableContent = new List<string> {Bold(description + ":"), value};
            AddToTable(2, false, tableContent, needEmptyRow);
        }




        public void AddToTable(int columns, bool boldHeader, List<string> tableContent, bool needEmptyRow)
        {
            int col = 1;
            int row = 1;
            foreach (var content in tableContent)
            {
                bool isHeader = row == 1;
                bool firstColumn = col == 1;
                bool lastColum = col == columns;
                if (firstColumn)
                {
                    _html.Append("<tr>");
                }

                _html.Append("<td>");
                if (boldHeader && isHeader)
                    _html.Append("<strong>");
                _html.Append(content);
                if (boldHeader && isHeader)
                    _html.Append("</strong>");
                _html.Append("</td>");

                if (lastColum)
                {
                    _html.Append("</tr>");
                    //Activate next rox
                    col = 1;
                    row++;
                }
                else
                    col++;
            }
            if(needEmptyRow)
            AddEmptyRow();
        
        }

        

        public void AddEmptyRow()
        {
            _html.Append("<tr class=\"spacer\"><td>&nbsp;</td></tr>");
        }


        public String Bold(string text)
        {
            return String.Format("<strong>{0}</strong>", text);
        }

        public string GetResult()
        {
            End();
            return _html.ToString();
        }

        public void Append(string text)
        {
            _html.Append(text);
        }
    }
}

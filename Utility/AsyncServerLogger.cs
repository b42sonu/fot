using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class AsyncServerLogger : AsyncQueueWorker<ClientLogItem>
    {
        public void SendAsync(Severity severity, string eventMessage, string dump, string uniqueValue)
        {
            if (ModelMain.IsUnitTesting == false)
            {
                var clientLogItem = new ClientLogItem()
                    {
                        Description = eventMessage,
                        LogDateTime = DateTime.Now,
                        Severity = (int) severity,
                        StackTrace = dump,
                        Hash = uniqueValue
                    };

                ExecuteAsync(clientLogItem);
            }
        }

        protected override void DoWork(Queue<ClientLogItem> workQueue)
        {
#pragma warning disable 618
            var application = PreCom.Application.Instance;
            var communication = application.Communication;
            var hardware = application.Platform.Hardware;
#pragma warning restore 618
            var userName = ModelUser.UserProfile != null ? ModelUser.UserProfile.UserId : "not logged in";

            if (!communication.IsInitialized || !communication.IsLoggedIn)
                return;

            var hashBuilder = new Md5HashBuilder();

            while (workQueue.Count > 0)
            {
                ClientLogItem item = workQueue.Dequeue();
                item.Pda = hardware.Tag;
                item.UserId = userName;
                item.Hash = hashBuilder.ComputeHash(item.Hash);
    
                try
                {
                    communication.Send(item, new RequestArgs(QueuePriority.Normal, Persistence.UntilDelivered));
                }
                catch (Exception e)
                {
                    e.ToString();
                }
            }
        }
    }
}
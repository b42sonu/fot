using System;
using System.Runtime.InteropServices;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class HideKeyBoardCommand
    {
        private const int SwHide = 0x0000;
        private const int SwShow = 0x0001;

        [DllImport("coredll.dll")]
        private static extern bool SipShowIM(int dwFlag);

        [DllImport("coredll.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string caption, string className);

        [DllImport("coredll.dll", SetLastError = true)]
        public static extern bool ShowWindow(IntPtr hwnd, int state);

        [DllImport("coredll.dll")]
        private static extern IntPtr GetWindow(IntPtr hWnd, uint uCmd);

        public void HideAll()
        {
            CloseKeyboard();
            HideKeyboardIcon();
        }

        public void OpenKeyboard()
        {
            SipShowIM(1);
        }

        public void CloseKeyboard()
        {
            SipShowIM(0);
        }

        private void HideKeyboardIcon()
        {
            ShowHideKeyboardIcon(SwHide);
            // ReSharper disable InconsistentNaming
            const int SWP_HIDE = 0x0080;
            // ReSharper restore InconsistentNaming
            IntPtr hWnd = Win32.FindWindow(null, "MS_SIPBUTTON");
            Win32.SetWindowPos(hWnd, IntPtr.Zero, 0, 0, 0, 0, SWP_HIDE);
        }

        private void ShowHideKeyboardIcon(int value)
        {
            var hSipWindow = FindWindow("MS_SIPBUTTON", "MS_SIPBUTTON");
            if (hSipWindow == IntPtr.Zero)
                return;

            var hSipButton = GetWindow(hSipWindow, 5);

            if (hSipButton != IntPtr.Zero)
                ShowWindow(hSipButton, value);

            Marshal.Release(hSipWindow);
            Marshal.Release(hSipButton);
        }
    }
    //public class HideKeyBoardCommand
    //{
    //    public const Int32 WM_USER = 1024;
    //    public const Int32 WM_CSKEYBOARD = WM_USER + 192;
    //    public const Int32 WM_CSKEYBOARDMOVE = WM_USER + 193;
    //    public const Int32 WM_CSKEYBOARDRESIZE = WM_USER + 197;

    //    [DllImport("user32.dll", EntryPoint = "FindWindow")]
    //    private static extern Int32 FindWindow(string _ClassName, string _WindowName);

    //    [DllImport("User32.DLL")]
    //    public static extern Boolean PostMessage(Int32 hWnd, Int32 Msg, Int32 wParam, Int32 lParam);

    //    public void Execute()
    //    {
    //        HideKeyboardIcon();
    //        CloseKeyboard();
    //    }

    //    private void CloseKeyboard()
    //    {
    //        PostMessage(FindWindow("TFirstForm", "hvkFirstForm"), WM_CSKEYBOARD, 2, 0);
    //    }

    //    private void HideKeyboardIcon()
    //    {
    //        // ReSharper disable InconsistentNaming
    //        const int SWP_HIDE = 0x0080;
    //        // ReSharper restore InconsistentNaming
    //        IntPtr hWnd = Win32.FindWindow(null, "MS_SIPBUTTON");
    //        Win32.SetWindowPos(hWnd, IntPtr.Zero, 0, 0, 0, 0, SWP_HIDE);
    //    }
    //}
}
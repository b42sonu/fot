﻿using System;
using System.Drawing;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class for getting the right density of pictures
    /// </summary>
    public static class ScaleUtil
    {
        public const String OnScreenKeyboardIcon = "OnscreenKeyboardButton";
        public const String Info = "Info";
        public const String HeaderLogo = "HeaderLogo";

        public static Bitmap GetImage(String imageName)
        {
            imageName += "_hdpi";
            
            var obj = Properties.Resources.ResourceManager.GetObject(imageName, Properties.Resources.Culture);
            
            var image = ((Bitmap)(obj));
            if (image == null)
            {
                Logger.LogEvent(Severity.Error, "Could not find the image " + imageName + " in resources");
            }

            return image;
        }


        public static int GetScaledPosition(int position)
        {
            return position;
        }

        public static Size GetScaledSize(int width, int height)
        {
            return new Size(width, height);
        }

        public static Size GetScaledSize(Size size)
        {
            return size;
        }

        public static Point GetScaledPoint(Point location)
        {
            return location;
        }

        public static Point GetScaledPoint(int x, int y)
        {
            return new Point(x, y);
        }

        public static Bitmap ResizeImage(Bitmap image)
        {
            return image;
        }
    }
}
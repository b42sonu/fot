﻿using Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability;
using PMC.PreComX.Mock;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class for playing sound. Mute device to turn off sound.
    /// If sound is already playing, then wait til it finishes to play it
    /// </summary>
    public class SoundUtil
    {
        private static SoundUtil _instance;
        private static IPlatformBase _platform;
        private static bool _silent;

        public static SoundUtil Instance
        {
            get { return _instance ?? (_instance = new SoundUtil()); }
        }

        public SoundUtil()
#pragma warning disable 618
            : this(PlatformBaseWrapper.Wrap(PreCom.Application.Instance.Platform))
#pragma warning restore 618
        { }

        public SoundUtil(IPlatformBase platform)
        {
            _platform = platform;
        }

        private static void PlayResource(byte[] soundResource)
        {
            if (_platform != null&&!_silent)
            {
                _platform.Media.Sound.Play(soundResource, false);
            }
        }

        public void PlaySuccessSound()
        {
            PlayResource(Properties.Resources.sound_barcode_ok);
        }

        public void PlayScanSound()
        {
            PlayResource(Properties.Resources.sound_barcode_scan);
        }

        public void PlayScanErrorSound()
        {
            PlayResource(Properties.Resources.sound_barcode_stop);
        }

        public void PlayWarningSound()
        {
            PlayResource(Properties.Resources.sound_barcode_warning);
        }

        public void PlayNewOperationListSound()
        {
            if(ActionCommandsChangeTmsAvailability.Instance.IsStatusPaused == false)
                PlayResource(Properties.Resources.sound_new_opeation_list);
        }

        public void PlayNewMessageSound()
        {
            if (ActionCommandsChangeTmsAvailability.Instance.IsStatusPaused == false)
                PlayResource(Properties.Resources.sound_text_message);
        }

        /// <summary>
        /// make PDA silent
        /// </summary>
        public void TurnSoundOff()
        {
            _silent = true;
        }

        /// <summary>
        /// make PDA sound on
        /// </summary>

        public void TurnSoundOn()
        {
            _silent = false;
        }

    }
}
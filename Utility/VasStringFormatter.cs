﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    // Parses a string of format param1name:param1value|....|paramNname, paramNvalue
    public static class VasStringFormatter
    {
        public static string Format(string message, ConsignmentEntity consignmentEntity)
        {
            // Arg 0: Consignment item
            // Arg 1: Consignment
            // Arg 2: Amount
            // Arg 3: Currency
            // Arg 4: Payment description
            // Arg 5: Weight
            // Arg 6: Volume
            // Arg 7: Number of items in consignment
            // Arg 8: Length
            // Arg 9: Height
            // Arg 10: Width
            try
            {
                if (message == null)
                    return "Message is null in VasStringFormatter.Format";
                
                if (consignmentEntity != null)
                {
                    decimal amount = 0;
                    string currency = "kr.";
                    string paymentDescription = string.Empty;
                    var payments = consignmentEntity.ConsignmentInstance.Payments;
                    if (payments != null)
                    {
                        amount = payments[0].Amount;
                        currency = payments[0].Currency;
                        paymentDescription = payments[0].Description;
                    }

                    int consignmentItemCount = consignmentEntity.ConsignmentInstance.ConsignmentItemCountFromLm;

                    decimal weight = 0, length = 0, height = 0, width = 0;
                    decimal volume = 0;
                    if (consignmentEntity.Measures != null)
                    {
                        weight = consignmentEntity.Measures.WeightKg;
                        volume = consignmentEntity.Measures.VolumeDm3;
                        length = consignmentEntity.Measures.LengthCm;
                        height = consignmentEntity.Measures.HeightCm;
                        width = consignmentEntity.Measures.WidthCm;
                    }

                    return string.Format(message, consignmentEntity.EntityDisplayId, consignmentEntity.ConsignmentDisplayId, amount,
                                         currency, paymentDescription, weight, volume, consignmentItemCount, length, height, width);
                }
            }
            catch (FormatException)
            {
                return "The format from VAS does not match the parameters supplied";
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsVasMatrix.FormatVasMessage");
            }

            return message;
        }

    }
}

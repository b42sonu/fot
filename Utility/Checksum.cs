using System;
using System.Globalization;

// ReSharper disable EmptyGeneralCatchClause

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// This class contains algorithms that calculates checksums
    /// </summary>
    public class Checksum
    {
        // **********************************************************
        // ********************    Modulus 10    ********************
        // **********************************************************
        public enum Modulus10Type
        {
            Modulus102,
            Modulus103
        }


        /// <summary>
        /// Calculates the modulus 10 checksum for a numeric string.
        /// </summary>
        /// <param name="input">A numeric string</param>
        /// <param name="type">The type of modulus checksum number to calculate</param>
        /// <returns>A modulus 10 checksum digit</returns>
        /// <exception cref="System.FormatException">input is not a numeric string</exception>
        public static short CalcMod10ChecksumDigit(string input, Modulus10Type type)
        {
            var digitSum = 0;

            // Check string format
            if (!StringUtil.IsLeadingZerosNumeric(input))
            {
                throw new FormatException("Checksum.CalcMod10ChecksumDigit(): The string attribute is not numeric");
            }

            // Calculates the checksum digit.
            for (var currentIndex = input.Length - 1; currentIndex >= 0; currentIndex--)
            {
                int currentDigit = Int16.Parse(input[currentIndex].ToString(CultureInfo.InvariantCulture));

                // Odd index (backwards)
                // Activate with last digit = first odd
                if ((input.Length - (currentIndex + 1)) % 2 == 0)
                {
                    switch (type)
                    {
                        case Modulus10Type.Modulus102:
                            // Multiply digit with TWO and sub 9 from sums greater than 9.
                            if ((currentDigit * 2) > 9)
                            {
                                digitSum += (currentDigit * 2) - 9;
                            }
                            else
                            {
                                digitSum += currentDigit * 2;
                            }

                            break;
                        case Modulus10Type.Modulus103:
                            // Multiply digit with THREE.
                            digitSum += currentDigit * 3;
                            break;
                    }
                }
                // Even index (backwards)
                // Activate with second last digit = first even
                else
                {
                    digitSum += currentDigit * 1; // Multiply digit with ONE.
                }
            }

            return (short)((10 - (digitSum % 10)) % 10);
        }

        /// <summary>
        /// Check if an numeric string has a legal checksum digit.
        /// A FormatException will be thrown if the string not is numeric. 
        /// </summary>
        /// <param name="input">A numeric string that ends with an modulus 10 checksum digit</param>
        /// <param name="type">The type of modulus calculation that shall be used</param>
        /// <returns>true if checksum is correct</returns>
        public static bool CheckMod10Checksum(string input, Modulus10Type type)
        {
            if (!StringUtil.IsLeadingZerosNumeric(input))
            {
                return false;
            }

            var inputChecksum = Int16.Parse(input[input.Length - 1].ToString(CultureInfo.InvariantCulture));
            return inputChecksum == CalcMod10ChecksumDigit(input.Substring(0, input.Length - 1), type);
        }

        // **********************************************************
        // ******************    Weigthed modulus 11   ******************
        // **********************************************************

        /// <summary>
        /// Calculates the weigthed modulus 11 check digit checksum for a numeric string and concatenates the cheksum to the element string.
        /// </summary>
        /// <param name="elementString">A numeric string</param>
        /// <param name="weigthNumber">A numeric string</param>
        /// <returns>The input string with a weigthed modulus 11 Check digit concatenated</returns>
        /// <exception cref="System.FormatException">elementString is not a numeric string</exception>
        public static string AddWm11Checksum(string elementString, string weigthNumber)
        {
            return elementString + CalcWm11ChecksumDigit(elementString, weigthNumber);
        }

        /// <summary>
        /// Calculates the weigthed modulus 11 check digit checksum for a numeric string.
        /// </summary>
        /// <param name="elementString">A numeric string</param>
        /// <param name="weigthNumber">A numeric string</param>
        /// <returns>A weigthed modulus 11 check digit</returns>
        /// <exception cref="System.FormatException">elementString is not a numeric string</exception>
        public static short CalcWm11ChecksumDigit(string elementString, string weigthNumber)
        {
            var digitSum = 0;

            // Check string format
            if (!StringUtil.IsLeadingZerosNumeric(elementString) || !StringUtil.IsLeadingZerosNumeric(weigthNumber))
            {
                throw new FormatException("CalcWM11ChecksumDigit: A string attribute is not numeric");
            }

            if (elementString.Length != weigthNumber.Length)
            {
                throw new FormatException("CalcWM11ChecksumDigit: The element string and the weigth number doesn't have an equal length");
            }

            // Calculate the checksum digit.
            for (var currentIndex = elementString.Length - 1; currentIndex >= 0; currentIndex--)
            {
                // Multiply element string forwards with weigthed number backwards.
                int currentElementDigit = Int16.Parse(elementString[(elementString.Length - 1) - currentIndex].ToString(CultureInfo.InvariantCulture));
                int currentWeigthDigit = Int16.Parse(weigthNumber[currentIndex].ToString(CultureInfo.InvariantCulture));

                digitSum += currentElementDigit * currentWeigthDigit;
            }

            var chcksumNumber = (short)(11 - (digitSum % 11));

            switch (chcksumNumber)
            {
                case 10:
                    return 0;
                case 11:
                    return 5;
                default:
                    return chcksumNumber;
            }
        }

        /// <summary>
        /// Check if an numeric barcode string has a legal checksum digit.
        /// A FormatException will be thrown if the string not is numeric. 
        /// </summary>
        /// <param name="elementString">A numeric string that ends with an weigthed modulus 11 checksum digit</param>
        /// <param name="weigthNumber">A numeric string</param>
        /// <returns>true if checksum is correct</returns>
        public static bool CheckWm11Checksum(string elementString, string weigthNumber)
        {
            try
            {
                if (!StringUtil.IsLeadingZerosNumeric(elementString))
                {
                    return false;
                }

                var inputChecksum = Int16.Parse(elementString[elementString.Length - 1].ToString(CultureInfo.InvariantCulture));
                return inputChecksum == CalcWm11ChecksumDigit(elementString.Substring(0, elementString.Length - 1), weigthNumber);
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// Check sum used for GS1 barcodes
        /// </summary>
        /// <param name="content"></param>
        /// <param name="checkSum"></param>
        /// <returns></returns>
        public static bool IsValidCheckSumGs1(string content, char checkSum)
        {
            try
            {
                int multiplier = 3;
                int sum = 0;
                for (int i = 0; i < content.Length; i++)
                {
                    int valueContent = Convert.ToInt32(content.Substring(i, 1));
                    sum += valueContent * multiplier;

                    multiplier = multiplier == 3 ? 1 : 3;
                }

                sum = 10 - (sum % 10);
                if (sum == 10)
                    sum = 0;

                if (sum == (int)Char.GetNumericValue(checkSum))
                {
                    return true;
                }
            }
            // Dont care about exception. If this fails, then input conatianed non-numeric characters
            catch (Exception)
            { }
            return false;
        }

        /// <summary>
        /// Check sum used for code 39 barcodes
        /// </summary>
        public static bool IsValidCheckSumCode39(string content, string checkSum)
        {
            bool result = false;
            try
            {
                Logger.LogAssert(content.Length == 8);

                const string mask = "86423597";

                int sum = 0;
                for (int i = 0; i < content.Length; i++)
                {
                    int valNum = Convert.ToInt32(content.Substring(i, 1));
                    int valCheckSum = Convert.ToInt32(mask.Substring(i, 1));
                    sum += valNum * valCheckSum;
                }

                sum = sum % 11;
                sum = 11 - sum;

                //sum = sum == 10 ? 0 : sum == 11 ? 5 : sum;
                if (sum == 10)
                    sum = 0;
                else
                {
                    if (sum == 11)
                        sum = 5;
                }

                if (sum == Convert.ToInt32(checkSum))
                    result = true;

            }
            catch (Exception) { }
            return result;
        }

        public static bool IsValidCheckSumCargo1(string content)
        {
            bool result = false;
            try
            {
                int multiplier = 3, sum = 0;
                for (int i = content.Length - 2; i >= 0; i--)
                {
                    int val = int.Parse(content[i].ToString(CultureInfo.InvariantCulture));
                    sum = sum + (val * multiplier);

                    multiplier = multiplier == 3 ? 1 : 3;
                }

                int checkSum = int.Parse(content[content.Length - 1].ToString(CultureInfo.InvariantCulture));

                if (checkSum == (10 - (sum % 10)))
                {
                    result = true;
                }

            }
            catch (Exception)
            {
            }

            return result;
        }

        public static bool IsValidCheckSumCargo2(string content, string checksumDigit)
        {
            try
            {
                int multiplier = 3, sum = 0;
                for (int i = content.Length - 1; i >= 0; i--)
                {
                    int val = int.Parse(content[i].ToString(CultureInfo.InvariantCulture));
                    sum = sum + (val * multiplier);

                    multiplier = multiplier == 3 ? 1 : 3;
                }

                sum = 10 - (sum % 10);
                if (sum == 10)
                    sum = 0;

                return (sum == Convert.ToInt32(checksumDigit));
            }
            catch (Exception)
            {
                return false;
            }
        }


        public static bool IsValidCheckSumFrigo(string content, string checksumDigit)
        {
            try
            {
                Logger.LogAssert(content.Length == 9);

                int digitSum = 0;

                //Find the digit sum with every second digit handled like: 
                //    multiply by two and let be result > 10, first digit + second digit, else 0
                int i = 0;
                while (i <= 9)
                {
                    int digit = (int)Char.GetNumericValue(content[i]) * 2;
                    i++;
                    int product = digit >= 10 ? 1 + (digit % 10) : digit;
                    digitSum += product;
                    if (i < 8)
                    {
                        product = (int)Char.GetNumericValue(content[i]);
                        digitSum += product;
                    }
                    i++;
                }

                int sum = digitSum + Convert.ToInt32(checksumDigit);

                return sum % 10 == 0;

            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}

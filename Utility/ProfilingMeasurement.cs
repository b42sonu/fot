﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using PreCom.Core.Modules;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class ProfilingMeasurement : IDisposable
    {
        public static bool IsProfiling { get; set; }

#if PROFILING
        private static Type _type;
        private static long _ticksForDb, _ticksForTmi, _ticksForLogging, _ticksForAsync, _ticksForSync, _ticksForKeyboard, _ticksForScanner, _ticksForBusyUtil;
        private readonly long _ticksFunctionMeasurement;
        private static List<ProfileLogEntry> _logEntries = new List<ProfileLogEntry>();
        private static readonly List<string> FunctionEntries = new List<string>();
        private static long _stampStartProfiling;




        private readonly TuningMeasurementType _tuningMeasurementType;


        private readonly long _ticksStartDbMeasurement;
        private readonly long _ticksStartTmiMeasurement;
        private readonly long _ticksStartAsyncMeasurement;
        private readonly long _ticksStartSyncMeasurement;
        private readonly long _ticksStartLoggingMeasurement;

        private readonly long _ticksStartKeyboardMeasurement;
        private readonly long _ticksStartScannerMeasurement;

        private readonly long _ticksStartBusyUtilMeasurement;


        private readonly string _functionName;


        private struct ProfileLogEntry
        {
            internal readonly string Message;
            internal readonly long TickStamp;

            internal ProfileLogEntry(string message)
            {
                Message = message;
                TickStamp = Environment.TickCount;
            }
        }
#endif

        private ProfilingMeasurement(string functionName)
        {
#if PROFILING
            _functionName = functionName;
            _ticksFunctionMeasurement = Environment.TickCount;
#endif
        }

        public static ProfilingMeasurement MeasureFunction(string functionName)
        {
            return new ProfilingMeasurement(functionName);
        }

        public static ProfilingMeasurement Measure(TuningMeasurementType tuningMeasurementType)
        {
            return new ProfilingMeasurement(tuningMeasurementType);
        }


        public static void StartProfiling(Type type)
        {
#if PROFILING
            if (_logEntries.Count > 0)
                WriteCachedLog();

            IsProfiling = true;
            _type = type;

            _ticksForDb = _ticksForTmi = _ticksForLogging = _ticksForAsync = _ticksForSync = _ticksForKeyboard = _ticksForScanner = _ticksForBusyUtil = 0;

            _logEntries = new List<ProfileLogEntry>
            {
                new ProfileLogEntry(""),
                new ProfileLogEntry("------ Starting logbatch -------"),
                new ProfileLogEntry("")
            };

            _stampStartProfiling = Environment.TickCount;
#endif
        }

        public static void EndProfilingForFlow()
        {
#if PROFILING
            if (BaseModule.CurrentFlow != null && BaseModule.CurrentFlow.CurrentView != null &&
                _type == BaseModule.CurrentFlow.CurrentView.GetType())
            {
                EndProfiling();
            }
#endif
        }

        public static void EndProfiling()
        {
#if PROFILING
            IsProfiling = false;
            Flush();

            _type = null;
#endif
        }



        private ProfilingMeasurement(TuningMeasurementType tuningMeasurementType)
        {
#if PROFILING
            if (IsProfiling)
            {
                _tuningMeasurementType = tuningMeasurementType;

                switch (_tuningMeasurementType)
                {
                    case TuningMeasurementType.Db:
                        _ticksStartDbMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.Logging:
                        _ticksStartLoggingMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.AsyncCom:
                        _ticksStartAsyncMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.SyncCom:
                        _ticksStartSyncMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.Tmi:
                        _ticksStartTmiMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.Keyboard:
                        _ticksStartKeyboardMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.Scanner:
                        _ticksStartScannerMeasurement = Environment.TickCount;
                        break;

                    case TuningMeasurementType.BusyUtil:
                        _ticksStartBusyUtilMeasurement = Environment.TickCount;
                        break;
                }
            }
#endif
        }

        public void Dispose()
        {
#if PROFILING
            if (_functionName != null)
            {

                long duration = Environment.TickCount - _ticksFunctionMeasurement;

                FunctionEntries.Add(string.Format("{0, 4}   {1}", duration, _functionName));

            }
            else if (IsProfiling)
            {
                switch (_tuningMeasurementType)
                {
                    case TuningMeasurementType.Db:
                        _ticksForDb += Environment.TickCount - _ticksStartDbMeasurement;
                        break;


                    case TuningMeasurementType.Logging:
                        _ticksForLogging += Environment.TickCount - _ticksStartLoggingMeasurement;
                        break;

                    case TuningMeasurementType.AsyncCom:
                        _ticksForAsync += Environment.TickCount - _ticksStartAsyncMeasurement;
                        break;

                    case TuningMeasurementType.SyncCom:
                        _ticksForSync += Environment.TickCount - _ticksStartSyncMeasurement;
                        break;

                    case TuningMeasurementType.Tmi:
                        _ticksForTmi += Environment.TickCount - _ticksStartTmiMeasurement;
                        break;

                    case TuningMeasurementType.Keyboard:
                        _ticksForKeyboard += Environment.TickCount - _ticksStartKeyboardMeasurement;
                        break;

                    case TuningMeasurementType.Scanner:
                        _ticksForScanner += Environment.TickCount - _ticksStartScannerMeasurement;
                        break;

                    case TuningMeasurementType.BusyUtil:
                        _ticksForBusyUtil += Environment.TickCount - _ticksStartBusyUtilMeasurement;
                        break;

                }
            }
#endif
        }




        internal static void LogEntry(string message, LogLevel logLevel)
        {
#if PROFILING
            string logEntry = String.Format("{0};{1};0; ;{2}", DateTime.Now, logLevel, message);

            _logEntries.Add(new ProfileLogEntry(logEntry));
            if (_logEntries.Count >= 200)
                Flush();
#endif
        }

#if PROFILING
        static private void Flush()
        {
            try
            {
                WriteCachedLog();
                WriteMeasurements();
                WriteFunctionMeasurements();
            }

            catch (Exception ex)
            {
                // Can not log here, so hopefully this does not happen....
                Debug.WriteLine("Exception while flushing Measurements: " + ex.Message);
            }
        }


        private static void WriteCachedLog()
        {
            try
            {
                using (var sw = new StreamWriter(Path.Combine(FileUtil.GetApplicationPath(), @"Profiling\CachedLog.log"), true))
                {
                    long total = 0;
                    int max = _logEntries.Count - 1;
                    for (int i = 0; i < max; i++)
                    {
                        long usage = (_logEntries[i + 1].TickStamp - _logEntries[i].TickStamp);
                        total += usage;
                        sw.WriteLine("{0,4}   {1}", usage, _logEntries[i].Message);
                    }
                    sw.WriteLine(_logEntries[max].Message);
                    sw.WriteLine("Total usage: " + total);
                }
                _logEntries.Clear();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ProfilingMeasurement.WriteCachedLog");
            }
        }

        private static void WriteFunctionMeasurements()
        {
            using (var sw = new StreamWriter(Path.Combine(FileUtil.GetApplicationPath(), @"Profiling\Functions.log"), true))
            {
                sw.WriteLine("");
                sw.WriteLine("Activate batch");
                sw.WriteLine("----");

                foreach (string entry in FunctionEntries)
                {
                    sw.WriteLine(entry);
                }
            }
            FunctionEntries.Clear();
        }


        private static void WriteMeasurements()
        {
            using (var sw = new StreamWriter(Path.Combine(FileUtil.GetApplicationPath(), @"Profiling\Profiling.log"), true))
            {
                long timeUsedTotal = Environment.TickCount - _stampStartProfiling;
                sw.WriteLine("");
                if (_type != null)
                    sw.WriteLine("Results for: {0}", _type.Name);
                sw.WriteLine("Time used for DB: {0}", _ticksForDb);
                sw.WriteLine("Time used for TMI: {0}", _ticksForTmi);
                sw.WriteLine("Time used for Keyboard: {0}", _ticksForKeyboard);
                sw.WriteLine("Time used for Scanner: {0}", _ticksForScanner);
                sw.WriteLine("Time used for BusyUtil: {0}", _ticksForBusyUtil);
                sw.WriteLine("Time used for Logging: {0}", _ticksForLogging);
                sw.WriteLine("Time used for Async communication: {0}", _ticksForAsync);
                sw.WriteLine("Time used for Sync communication: {0}", _ticksForSync);
                sw.WriteLine("");

                long measured = _ticksForDb + _ticksForAsync + _ticksForLogging + _ticksForSync + _ticksForKeyboard + _ticksForScanner + _ticksForBusyUtil;
                sw.WriteLine("Time used Measured: {0}", measured);
                sw.WriteLine("Time used Unknown: {0}", timeUsedTotal - measured);
                sw.WriteLine("Time used local: {0}", timeUsedTotal - _ticksForSync);
                sw.WriteLine("Time used Total: {0}", timeUsedTotal);
                sw.WriteLine("");
                sw.WriteLine("-----------------------------------------");
            }
        }
#endif
    }

    public enum TuningMeasurementType
    {
        Logging,
        Db,
        Tmi,
        SyncCom,
        AsyncCom,
        Keyboard,
        Scanner,
        BusyUtil
    }
}

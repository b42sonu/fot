using PreCom.Core.Modules;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class LogQueueItem
    {
        public string EventMessage { get; set; }
        public string Dump { get; set; }
        public LogLevel LogLevel { get; set; }
    }
}
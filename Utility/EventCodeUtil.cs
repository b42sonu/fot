﻿using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class EventCodeUtil
    {
        public static string GetEventCodeFromProcess()
        {
            string result = string.Empty;
            if (BaseModule.CurrentFlow != null)
                result = GetEventCodeFromProcess(BaseModule.CurrentFlow.CurrentProcess);
            else
            {
                Logger.LogEvent(Severity.Error, "Failed to retrieve current event code");
            }
            return result;
        }

        public static string GetEventCodeFromProcessForLoadCarrier()
        {
            string result = string.Empty;
            if (BaseModule.CurrentFlow != null)
                result = GetEventCodeFromProcessForLoadCarrier(BaseModule.CurrentFlow.CurrentProcess);
            else
            {
                Logger.LogEvent(Severity.Error, "Failed to retrieve current event code");
            }
            return result;
        }

        public static string GetEventCodeFromProcessForLoadCarrier(Process process)
        {
            switch (process)
            {
                case Process.LoadLineHaul:
                    return "LBO"; // "LBO"; 

                case Process.LoadDistribTruck:
                    return "LB3"; //"LB3"; 

                case Process.UnloadLineHaul:
                    return "LBL"; //"LBL"; 

                case Process.BuildLoadCarrier:
                    return "W"; // "KL"; 

                case Process.UnloadPickUpTruck:
                    return "6";

                case Process.ArrivalRegistration:
                    var eventCode = string.Empty;
                    switch (ModelUser.UserProfile.Role)
                    {
                        case Role.PostOfficeAndStore:
                            eventCode = "LBQ";
                            break;

                        case Role.HomeDeliveryHub:
                            eventCode = "LBI";
                            break;
                        case Role.HubWorker1:
                            eventCode = "LBG";
                            break;
                    }
                    return eventCode;

                case Process.GeneralDeviations:
                    return "LBA";

                case Process.DeliveredOtherLoc:
                    return "LB2";

                case Process.IntoHoldingAtHub:
                    return "QI";

                case Process.OutOfHoldingAtHub:
                    return "QU";

                case Process.LoadByCustomer:
                    return "";

                case Process.DelToCustomerAtPo:
                case Process.DeliveryToCustomer:
                    return "LBU";

                case Process.PickingFromWarehouse:
                    return "LBV";
                case Process.UnplannedPickup:
                    return "LBM";

                case Process.DeliveryToSubContrct:
                    return "LBV";
                case Process.RemainingGoodsAtHub:
                    return "LVG";

                case Process.InTerminal:
                    return "L1T"; 

                case Process.Pickup:
                    return "LBM";

                case Process.LoadCombinationTrip:
                    return "";

                default:
                    Logger.LogEvent(Severity.Error, "Unknown process encountered in GetEventCodeFromProcessForLoadCarrier {0}", process);
                    return "";
            }
        }



        public static string GetEventCodeFromProcess(Process process)
        {
            switch (process)
            {
                case Process.DeliveredToHubByCust:
                    return "1";

                case Process.Departure:
                    return ""; //"TRD" TRD code was set for TM_OTC, but as per the need for Amphora or Alystra it is not required.

                case Process.UnplannedPickup:
                case Process.Pickup:
                    return "A";

                case Process.AttemptedPickup:
                    return "7";

                case Process.RegisterDamage:
                    return "5";

                case Process.Unloading:
                    return "6A";

                case Process.DeliveryToCustomer:
                case Process.DelToCustomerAtPo:
                    return ModelUser.UserProfile.Role == Role.HubWorker1 ? "I3" : "I";

                case Process.Loading:
                    return "3A";

                case Process.DeliveryToSubContrct:
                    return "2A";

                case Process.RemainingGoodsAtHub:
                    return "VG";

                case Process.RegisterTemperature:
                    return "A1";

                case Process.AttemptedDelivery:
                    return "H";

                case Process.Arrival:
                    return ""; //"TRA" TRA code was set for TM_OTC, but as per the need for Amphora or Alystra it is not required.

                case Process.CorrectWeightVolume:
                    return "A3";

                case Process.ControlScan:
                    return "CC"; // todo check if this is correct

                case Process.ChangeProductGroup:
                    return "A4";

                case Process.BuildLoadCarrier:
                    return "W";

                case Process.GeneralDeviations:
                    return "V";//TODO : check  if the event code is correct

                case Process.LoadByCustomer:
                    return "4";//TODO : check  if the event code is correct

                case Process.DeliveredOtherLoc:
                    return "2";//TODO : check if correct

                case Process.DeliveryToPostOffice:
                    return "P";


                case Process.ExecuteLinking:
                    return "V";

                case Process.LoadLineHaul:
                    return "WO";

                case Process.LoadDistribTruck:
                    return "3";

                case Process.UnloadLineHaul:
                    return "KL";

                case Process.UnloadPickUpTruck:
                    return "6";

                case Process.ArrivalRegistration:
                    var eventCode = string.Empty;
                    switch (ModelUser.UserProfile.Role)
                    {
                        case Role.PostOfficeAndStore:
                            eventCode = "Q";
                            break;

                        case Role.HomeDeliveryHub:
                            eventCode = "QI";
                            break;
                        case Role.HubWorker1:
                            eventCode = "G";
                            break;
                    }
                    return eventCode;

                case Process.AgeValidation:
                    return "V";

                case Process.IntoHoldingAtHub:
                    return "QI";

                case Process.PickingFromWarehouse:
                    return "L";

                case Process.InTerminal:
                    return "1T";

                case Process.OutOfHoldingAtHub:
                    return "QU";

                case Process.RegisterTimeConsumption:
                    return "V";

                case Process.ArrAtCustWareHouse:
                    return "G1";//Mottak 769

                case Process.DeliveryByCustomer:
                    return "I2";

                case Process.NotDelivered:
                    return "VI";

                case Process.UnloadEntireLineHaul:
                    return "LBA";

                case Process.None:
                    return "";

                default:
                    Logger.LogEvent(Severity.Error, "Unknown process encountered in GetEventCodeFromProcess {0}", process);
                    return "";
            }
        }



        public static string GetEventLevelFromProcess()
        {
            string result = string.Empty;
            if (BaseModule.CurrentFlow != null)
                result = GetEventLevelFromProcess(BaseModule.CurrentFlow.CurrentProcess);
            else
            {
                Logger.LogEvent(Severity.Error, "Failed to retrieve current event level");
            }
            return result;
        }

        internal static ValidateGoodsRequestValidationLevel GetValidationLevelFromProcess()
        {
            var result = ValidateGoodsRequestValidationLevel.Goods;
            if (BaseModule.CurrentFlow != null)
                result = GetValidationLevelFromProcess(BaseModule.CurrentFlow.CurrentProcess);
            else
            {
                Logger.LogEvent(Severity.Error, "Failed to retrieve current validation level");
            }
            return result;
        }

        private static string GetEventLevelFromProcess(Process process)
        {
            switch (process)
            {
                case Process.DeliveryToCustomer:
                case Process.RegisterDamage:
                    return "2";

                case Process.AttemptedDelivery:
                case Process.RemainingGoodsAtHub:
                    return "4";

                case Process.ControlScan:
                case Process.DeliveredOtherLoc:
                    return "1"; // All events

                default:
                    return "N";
            }
        }


        private static ValidateGoodsRequestValidationLevel GetValidationLevelFromProcess(Process process)
        {
            return ValidateGoodsRequestValidationLevel.Goods;
        }

        public static Process GetProcessFromEventCode(string eventCode)
        {
            switch (eventCode)
            {
                case "1": return Process.DeliveredToHubByCust;
                case "TRD": return Process.Departure;
                case "A": return Process.Pickup;
                //case "A": return Process.UnplannedPickup;
                case "7": return Process.AttemptedPickup;
                case "5": return Process.RegisterDamage;
                case "6A": return Process.Unloading;
                case "I": return Process.DeliveryToCustomer;
                case "3A": return Process.Loading;
                case "2A": return Process.DeliveryToSubContrct;
                case "VG": return Process.RemainingGoodsAtHub;
                case "A1": return Process.RegisterTemperature;
                case "H": return Process.AttemptedDelivery;
                case "TRA": return Process.Arrival;
                case "A3": return Process.CorrectWeightVolume;
                case "CC": return Process.ControlScan;
                case "A4": return Process.ChangeProductGroup;
                case "BuildLoadCarrierEvent": return Process.BuildLoadCarrier;
                case "V": return Process.GeneralDeviations;
                case "4": return Process.LoadByCustomer;
                case "2": return Process.DeliveredOtherLoc;
                case "P": return Process.DeliveryToPostOffice;
                //case "I": return Process.DelToCustomerAtPo;
                //case "V": return Process.AgeValidation;
                //case "V": return Process.ExecuteLinking;
                //case "V": return Process.OutOfHoldingAtHub;
                //case "V": return Process.RegisterTimeConsumption;
                //case "": return Process.LoadLineHaul;
                //case "": return Process.LoadingOfDistributionTruck;
                //case "": return Process.ArrivalRegistration;
                case "QI": return Process.IntoHoldingAtHub;
                case "L": return Process.PickingFromWarehouse;
                case "1T": return Process.InTerminal;
                case "I1": return Process.ArrAtCustWareHouse;
                default:
                    Logger.LogEvent(Severity.ExpectedError, "Unknown eventcode {0} encountered in GetProcessFromEventCode()", eventCode);
                    return Process.Unknown;
            }
        }

        public static bool? IsLoading
        {
            get
            {
#if DEBUG
                if (ModelMain.IsUnitTesting)
                    return true;
#endif
                Process process = BaseModule.CurrentFlow.CurrentProcess;
                switch (process)
                {
                    case Process.UnloadPickUpTruck:
                    case Process.UnloadLineHaul:
                        return false;
                    case Process.LoadDistribTruck:
                    case Process.LoadLineHaul:
                        return true;

                    case Process.DeliveryToCustomer:
                    case Process.AttemptedPickup:
                    case Process.UnplannedPickup:
                    case Process.DeliveryToSubContrct:
                    case Process.RegisterDamage:
                    case Process.RemainingGoodsAtHub:
                    case Process.LoadCombinationTrip:
                    case Process.Pickup:
                        return null;

                    default:
                        Logger.LogEvent(Severity.Error, "Could not determine if process {0} is Loading or Unloading",
                                        process);
                        return null;
                }
            }
        }


    }
}

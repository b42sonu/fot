﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Forms;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Properties;


namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public enum UserProcess
    {
        Pickup,
        Unloading,
        Delivery,
        Goods,
        Loading,
        OperationList,
        Settings,
    };

    public class GuiCommon
    {
        private static GuiCommon _instance;

        private readonly List<UserProcess> _processesForDriver2 = new List<UserProcess>
        {
            UserProcess.Pickup,
            UserProcess.Loading,
            UserProcess.Delivery,
            UserProcess.Goods,
            UserProcess.Settings
        };

        private readonly List<UserProcess> _processesForHubworker1 = new List<UserProcess>
        {
            UserProcess.Unloading,
            UserProcess.Loading,
            UserProcess.Delivery,
            UserProcess.Goods,
            UserProcess.Settings
        };

        private readonly List<UserProcess> _processesForCustomer = new List<UserProcess>
        {
            UserProcess.Loading,
            UserProcess.Delivery,
            UserProcess.Settings,
            UserProcess.Goods
        };

        private readonly List<UserProcess> _processesForLandpostbud = new List<UserProcess>
        {
            UserProcess.Pickup,
            UserProcess.Loading,
            UserProcess.Delivery,
            UserProcess.Goods,
            UserProcess.Pickup,
            UserProcess.Settings
        };

        private readonly List<UserProcess> _processesForPostOfficeAndStore = new List<UserProcess>
        {
            UserProcess.Pickup,
            UserProcess.Delivery,
            UserProcess.Goods,
            UserProcess.Settings
        };

        private readonly List<UserProcess> _processesForHomeDeliveryHub = new List<UserProcess>
        {
            UserProcess.Goods,
            UserProcess.Settings,
            UserProcess.Delivery
        };

        public static GuiCommon Instance
        {
            get { return _instance ?? (_instance = new GuiCommon()); }
        }

        private Image _imageBackground;

        public Image BackgroundImage
        {
            get { return _imageBackground ?? (_imageBackground = InitBackgroundImage()); }
        }

        private Image InitBackgroundImage()
        {
            try
            {
                // return Resources.MC65_Background;
                return Resources.MC65_Background_nograd;
            }

            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.InitBackgroundImage");
                return null;
            }
        }

        private Image _okImage;

        public Image ImageForOk
        {
            get { return _okImage ?? (_okImage = InitOKImage()); }
        }

        private Image InitOKImage()
        {
            try
            {
                return Resources.FOT_icon_verified_ok;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.InitOKImage");
                return null;
            }
        }

        public Image Loading()
        {
            try
            {
                return Resources.Loading;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Loading");
                return null;
            }
        }

        public Image LoadingLineHaulTruck()
        {
            try
            {
                return Resources.LoadLineHaulTruck;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.LoadLineHaulTruck");
                return null;
            }
        }

        public Image LoadDistributionTruck()
        {
            try
            {
                return Resources.LoadDistributionTruck;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.LoadingOfDistributionTruck");
                return null;
            }
        }

        public Image UnloadLineHaul()
        {
            try
            {
                return Resources.UnloadLineHaul;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.UnloadLineHaul");
                return null;
            }
        }

        public Image UnloadPickUpTruck()
        {
            try
            {
                return Resources.UnloadPickUpTruck;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.UnloadPickUpTruck");
                return null;
            }
        }

        public Image Pickup()
        {
            try
            {
                return Resources.PickUp;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Pickup");
                return null;
            }
        }

        public Image PlusLarge()
        {
            try
            {
                return Resources.PlusLarge;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.PlusLarge");
                return null;
            }
        }

        public Image MinusLarge()
        {
            try
            {
                return Resources.MinusLarge;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.MinusLarge");
                return null;
            }
        }
        public Image ReturnedDelivery()
        {
            try
            {
                return Resources.ReturnedDelivery;
            }

            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.ReturnedDelivery");
                return null;
            }
        }
        public Image Unloading()
        {
            try
            {
                return Resources.Unloading;
            }

            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Unloading");
                return null;
            }
        }

        public Image Delivery()
        {
            try
            {
                return Resources.Delivery;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Delivery");
                return null;
            }
        }


        public Image Finished()
        {
            try
            {
                return Resources.Finished;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Finished");
                return null;
            }
        }

        public Image New()
        {
            try
            {
                return Resources.New;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.New");
                return null;
            }
        }

        public Image Started()
        {
            try
            {
                return Resources.Started;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Started");
                return null;
            }
        }

        public Image Attention()
        {
            try
            {
                return Resources.FOT_icon_attention;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Attention");
                return null;
            }
        }

        public Image MessageRead()
        {
            try
            {
                return Resources.message_read;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.MessageRead");
                return null;
            }
        }

        public Image MessageUnRead()
        {
            try
            {
                return Resources.message_unread;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.MessageUnRead");
                return null;
            }
        }
        public Image FotIconVerifiedOk()
        {
            try
            {
                return Resources.FOT_icon_verified_ok;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Started");
                return null;
            }
        }
        public Image FotIconAttention()
        {
            try
            {
                return Resources.FOT_icon_attention;
            }
            catch (Exception e)
            {
                Logger.LogExpectedException(e, "GuiCommon.Started");
                return null;
            }
        }
        public bool DisplayIcon(UserProcess process)
        {
            bool result = false;
            if (ModelUser.UserProfile != null)
            {
                switch (ModelUser.UserProfile.Role)
                {
                    // Shows all modules
                    case Role.Driver1:
                        result = process != UserProcess.OperationList || ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ||
                            ModelUser.IsLoggedInToTms;
                        break;

                    case Role.Driver2:
                        result = _processesForDriver2.Contains(process);
                        break;

                    case Role.HubWorker1:
                        result = _processesForHubworker1.Contains(process);
                        break;

                    case Role.Customer:
                        result = _processesForCustomer.Contains(process);
                        break;

                    case Role.Landpostbud:
                        result = _processesForLandpostbud.Contains(process);
                        break;

                    case Role.PostOfficeAndStore:
                        result = _processesForPostOfficeAndStore.Contains(process);
                        break;

                    case Role.HomeDeliveryHub:
                        result = _processesForHomeDeliveryHub.Contains(process);
                        break;
                }
            }
            return result;
        }

        public static string EditHelpText(string moduleName, string processName, string formName, string helpText)
        {
            BaseForm currentView = null;
            if (BaseModule.CurrentFlow != null)
            {
                currentView = BaseModule.CurrentFlow.CurrentView;
                if (currentView != null)
                {
                    //disable current view, otherwise user can clik button(showing popup) multiple times causing hanging 
                    // of device, untill the screen is disabled by the popup
                    currentView.Enabled = false;
                }
                else if (ViewCommands.CurrentView != null)
                {
                    ViewCommands.CurrentView.Enabled = false;
                }
            }

            var result = HelpMessageDialog.Show(moduleName, processName, formName, helpText);

            if (currentView != null)
                currentView.Enabled = true;//re-enable current view for user
            else if (ViewCommands.CurrentView != null)
            {
                ViewCommands.CurrentView.Enabled = true;
            }

            //Try to reset focus to last focussed control 
            SetFocusToInputControl();
            return result;
        }

        /// <summary>
        /// This method shows the modal pop for specified args and calls the call back method
        /// and plays sound for Error and Warning
        /// severity=NoLog is for use by LogEventAndDump to prevent looping
        /// user can select break on error message. If debugger is attached program steps into the debugger, if not the program is aborted
        /// 
        /// </summary>
        public static string ShowModalDialog(string title, string text, Severity severity, params string[] buttonTexts)
        {
            BaseForm currentView = null;
            if (BaseModule.CurrentFlow != null )
            {
                currentView = BaseModule.CurrentFlow.CurrentView;
                if (currentView != null && !currentView.IsDisposed)
                {
                    // disable current view, otherwise user can clik button(showing popup) multiple times causing hanging 
                    // of device, untill the screen is disabled by the popup
                    currentView.Enabled = false;
                }
                else if (ViewCommands.CurrentView != null && !ViewCommands.CurrentView.IsDisposed)
                {
                    ViewCommands.CurrentView.Enabled = false;
                }
            }

            var result = MessageDialog.Show(title, text, severity, buttonTexts);

            if (currentView != null)
                currentView.Enabled = true;//re-enable current view for user
            else if (ViewCommands.CurrentView != null)
            {
                ViewCommands.CurrentView.Enabled = true;
            }

            //Try to reset focus to last focussed control 
            SetFocusToInputControl();
            return result;
        }

        /// <summary>
        /// Set focus on control on screen which opened the popup
        /// </summary>
        private static void SetFocusToInputControl()
        {
            if (BaseModule.CurrentFlow != null && BaseModule.CurrentFlow.CurrentView != null)
            {
                BaseModule.CurrentFlow.CurrentView.FocusInputControl();
            }
            else
            {
                if (ViewCommands.CurrentView != null)
                {
                    ViewCommands.CurrentView.FocusInputControl();
                }
            }
        }
    }
}

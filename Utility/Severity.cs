namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public enum Severity
    {
        Debug,
        Info,
        Warning,
        ExpectedError,
        Error,
        NoLog
    }
}
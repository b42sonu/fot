﻿using System.Collections.Generic;
using System.Threading;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public abstract class AsyncQueueWorker<T>
    {
        private Queue<T> _logItemQueue = new Queue<T>();
        private readonly object _locker = new object();
        private readonly AutoResetEvent _writeLogEvent = new AutoResetEvent(false);

        protected AsyncQueueWorker()
        {
            var loggerThread = new Thread(Worker) { IsBackground = true, Priority = ThreadPriority.Lowest };
            loggerThread.Start();
        }

        protected void ExecuteAsync(T work)
        {
            lock (_locker)
            {
                _logItemQueue.Enqueue(work);
                _writeLogEvent.Set();
            }
        }

        protected abstract void DoWork(Queue<T> workQueue);

        private void Worker()
        {
            while (_writeLogEvent.WaitOne())
            {
                Thread.Sleep(200); // Many times logevents comes in bursts. Allows for saving up some evnts before we start writing
                while (CheckForWork()) { }
            }
        }

        private bool CheckForWork()
        {
            Queue<T> logItemQueue;
            lock (_locker)
            {
                if (_logItemQueue.Count == 0)
                    return false;

                logItemQueue = _logItemQueue;
                _logItemQueue = new Queue<T>();
            }

            DoWork(logItemQueue);

            return true; // Signal that there might be more entries in log
        }
    }
}
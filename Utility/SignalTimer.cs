﻿using System;
using System.Threading;
using Symbol.Display;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public enum SignalType
    {
        MessageToDriver,
        OperationList
    }

    public class SignalTimer
    {
        private static Timer _messageToDriverTimer;
        private static Timer _operationListTimer;

        public static void Start(SignalType type)
        {
            if (type == SignalType.MessageToDriver)
                _messageToDriverTimer = new Timer(MessageTimerCallback, null, 0, 10000);
            else
                _operationListTimer = new Timer(OperationListTimerCallback, null, 0, 30000);
            
        }

        public static void End(SignalType type)
        {
            if (type == SignalType.MessageToDriver && _messageToDriverTimer != null)
            {
                _messageToDriverTimer.Dispose();
                _messageToDriverTimer = null;
            }
            else if (type == SignalType.OperationList && _operationListTimer != null)
            {
                _operationListTimer.Dispose();
                _operationListTimer = null;
            }
        }

        private static void MessageTimerCallback(Object o)
        {
            SoundUtil.Instance.PlayNewMessageSound();
            BacklightUtil.Instance.FlashBacklight();
        }

        private static void OperationListTimerCallback(Object o)
        {
            SoundUtil.Instance.PlayNewOperationListSound();
            BacklightUtil.Instance.SetState(BacklightState.ON);
        }
    }
}

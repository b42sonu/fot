﻿

using System;
using System.Runtime.InteropServices;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{

    using System;
    using System.Runtime.InteropServices;
    /*
    In order to use this class in your program, just declare the variable 
    and hook up into HookEvent:
    HookKeys hook = new HookKeys();
    hook.HookEvent += new HookKeys.HookEventHandler(HookEvent);
    hook.Start();
    */
    public class HookKeys
    {
        #region delegates
        public delegate int HookProc(int code, IntPtr wParam, IntPtr lParam);
        public delegate void HookEventHandler(HookEventArgs e, KeyBoardInfo keyBoardInfo);
        public HookEventHandler HookEvent;
        #endregion
        #region fields
        private HookProc hookDeleg;
        private static int hHook = 0;
        #endregion

        //private static HookKeys _instance;
        //public static HookKeys GetInstance(HookEventHandler hookEvent)
        //{

        //    if (_instance == null)
        //    {
        //        _instance = new HookKeys();
        //        _instance.HookEvent = hookEvent;
        //        _instance.Start();
        //    }
        //    _instance.HookEvent = hookEvent;

        //    return _instance;

        //}

        public HookKeys()
        {
        }
        ~HookKeys()
        {
            if (hHook != 0)
                this.Stop();
        }
        #region public methods
        ///
        /// Starts the hook
        ///
        public void Start()
        {
            if (hHook != 0)
            {
                //Unhook the previous one
                this.Stop();
            }
            hookDeleg = new HookProc(HookProcedure);
            hHook = SetWindowsHookEx(WH_KEYBOARD_LL, hookDeleg, GetModuleHandle(null), 0);
            if (hHook == 0)
            {
                throw new SystemException("Failed acquiring of the hook.");
            }
            AllKeys(true);
        }
        ///
        /// Stops the hook
        ///
        public void Stop()
        {
            UnhookWindowsHookEx(hHook);
            AllKeys(false);
        }
        #endregion
        #region protected and private methods
        protected virtual void OnHookEvent(HookEventArgs hookArgs, KeyBoardInfo keyBoardInfo)
        {
            if (HookEvent != null)
            {
                HookEvent(hookArgs, keyBoardInfo);
            }
        }

        private int HookProcedure(int code, IntPtr wParam, IntPtr lParam)
        {
            KBDLLHOOKSTRUCT hookStruct =
         (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lParam, typeof(KBDLLHOOKSTRUCT));
            if (code < 0)
                return CallNextHookEx(hookDeleg, code, wParam, lParam);
            // Let clients determine what to do
            HookEventArgs e = new HookEventArgs();
            e.Code = code;
            e.wParam = wParam;
            e.lParam = lParam;
            KeyBoardInfo keyInfo = new KeyBoardInfo();
            keyInfo.vkCode = hookStruct.vkCode;
            keyInfo.scanCode = hookStruct.scanCode;
            OnHookEvent(e, keyInfo);
            // Yield to the next hook in the chain
            return CallNextHookEx(hookDeleg, code, wParam, lParam);
        }
        #endregion
        #region P/Invoke declarations

        [DllImport("coredll.dll")]
        private static extern int AllKeys(bool bEnable);

        [DllImport("coredll.dll")]
        private static extern int SetWindowsHookEx
         (int type, HookProc hookProc, IntPtr hInstance, int m);
        [DllImport("coredll.dll")]
        private static extern IntPtr GetModuleHandle(string mod);
        [DllImport("coredll.dll")]
        private static extern int CallNextHookEx(
                HookProc hhk,
                int nCode,
                IntPtr wParam,
                IntPtr lParam
                );
        [DllImport("coredll.dll")]
        private static extern int GetCurrentThreadId();
        [DllImport("coredll.dll", SetLastError = true)]
        private static extern int UnhookWindowsHookEx(int idHook);
        public struct KBDLLHOOKSTRUCT
        {
            // ReSharper disable InconsistentNaming
#pragma warning disable 649
            public int vkCode;
            public int scanCode;
#pragma warning restore 649
#pragma warning disable 169
            public int flags;
            public int time;
            public IntPtr dwExtraInfo;
#pragma warning restore 169
            // ReSharper restore InconsistentNaming

        }
        const int WH_KEYBOARD_LL = 20;
        #endregion
    }
    #region event arguments

    public class HookEventArgs : EventArgs
    {
        public int Code;    // Hook code
        public IntPtr wParam;   // WPARAM argument
        public IntPtr lParam;   // LPARAM argument
    }
    public class KeyBoardInfo
    {
        public int vkCode;
        public int scanCode;
        public int flags;
        public int time;
    }
    #endregion

}

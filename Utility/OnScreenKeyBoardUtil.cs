﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using PreCom.Controls;
using System.Drawing;
using PreCom.Core;


namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class for implementing OnScreen Keyboard for input controls
    /// If UserControl has a input control, an icon will be displayed at det bottom task bar when it has focus.
    /// Use TextChanged to know when the user is done editing the TextBox.
    /// To set the input control event, set OnScreenKeyboardProperties object to contronl.Tag.
    /// </summary>
    public class OnScreenKeyBoardUtil
    {
        private Control _focusedInputControl;
        private Control _firstInputControl;
        private readonly BaseModule _module;
        private readonly bool _notUsingArchitecture;

        public OnScreenKeyBoardUtil(BaseModule module)
        {
            _module = module;
        }

        public OnScreenKeyBoardUtil(BaseModule module, bool notUsingArchitecture)
        {
            _module = module;
            _notUsingArchitecture = notUsingArchitecture;
            if (_notUsingArchitecture)
                _module.OnScreenKeyboardIconItem = null;
        }

        public void OnViewCreate(Control.ControlCollection controls)
        {
            InitOnScreenKeyboard(controls, 0);
        }

        public void OnViewResume(Control.ControlCollection controls)
        {
            HideOnScreenKeyboardIcon();

            if (_firstInputControl != null)
            {
                _firstInputControl.Focus();
                ShowOnScreenKeyboardIcon();
            }
        }

        private void InitOnScreenKeyboard(Control.ControlCollection controlCollection, int depth)
        {
            // Safeguard against unlimited recusrion if the control-collection somehow is recursive
            if (depth < 10)
            {
                foreach (Control control in controlCollection)
                {
                    if (IsInputControl(control))
                    {
                        SetEventsToControl(control);
                        SetFirstInputControl(control);
                    }

                    InitOnScreenKeyboard(control.Controls, depth + 1);
                }
            }
            else
            {
                Logger.LogEvent(Severity.Error, "Detected deep recursion in OnScreenKeyBoardUtil.InitOnScreenKeyboard");
            }
        }

        private void SetFirstInputControl(Control control)
        {

            bool noInputControlHasFocus = _firstInputControl == null;
            bool isShowing = control.Visible;

            if (noInputControlHasFocus && isShowing)
            {
                _firstInputControl = control;
                _focusedInputControl = control;
            }
        }

        private static bool IsInputControl(Control control)
        {
            bool isTextBox = control is TextBox && ((TextBox)control).ReadOnly == false;
            bool isNumericUpDown = control is NumericUpDown;

            return isTextBox || isNumericUpDown;
        }

        public void InputControlClicked(Control control)
        {

            _focusedInputControl = control;
            ShowOnScreenKeyboardIcon();

        }

        private void SetEventsToControl(Control control)
        {
            control.GotFocus += InputControlGotFocus;
            control.LostFocus += InputControlLostFocus;

            if (GetTextChangedEvent(control) != null)
            {
                //For registring hardware enter events
                control.KeyPress += InputControlNumberKeyPress;
                control.TextChanged += InputControlTextChanged;
            }
        }

        private void ResetEventsToControl(Control control)
        {
            control.GotFocus -= InputControlGotFocus;
            control.LostFocus -= InputControlLostFocus;

            if (GetTextChangedEvent(control) != null)
            {
                //For registring hardware enter events
                control.KeyPress -= InputControlNumberKeyPress;
                control.TextChanged -= InputControlTextChanged;
            }
        }

        private void ShowOnScreenKeyboardIcon()
        {
            if (_module != null)
            {

                if (_notUsingArchitecture && _module.OnScreenKeyboardIconItem == null)
                {
                    Image keyBoardButtonImage = ScaleUtil.GetImage(ScaleUtil.OnScreenKeyboardIcon);
                    var statusIcon = new StatusIconItem(keyBoardButtonImage, 1000);
                    statusIcon.Click += OnScreenKeyboardIconClick;
                    _module.OnScreenKeyboardIconItem = statusIcon;
                }
                _module.ShowOnScreenKeyboardIcon();
            }

        }

        private void DisableOnScreenKeyboardIcon()
        {
            _module.DisableOnScreenKeyboardIcon();
        }
        private void EnableOnScreenKeyboardIcon()
        {
            _module.EnableOnScreenKeyboardIcon();
        }

        public void FocusInputControl()
        {
            if (_focusedInputControl != null)
            {
                _focusedInputControl.Focus();
            }
        }

        private void OnScreenKeyboardIconClick(object sender, MouseEventArgs e)
        {
            try
            {
                ShowOnScreenKeyboard();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OnScreenKeyboardUtil.OnScreenKeyboardIconClick");
            }
        }

        public void HideOnScreenKeyboardIcon()
        {
            if (_module != null)
            {
                _module.HideOnScreenKeyboardIcon();
            }
        }

        public void ShowOnScreenKeyboard()
        {

            if (_focusedInputControl != null && !_focusedInputControl.IsDisposed)
            {
                var isReadOnly = _focusedInputControl is TextBox && ((TextBox)_focusedInputControl).ReadOnly;
                if (isReadOnly == false)
                {
                    var properties = GetPropertiesForInputControl();

                    var item = properties.TextChanged == null
                                   ? new InputPanelItem(_focusedInputControl, properties.Title, ReactivateInputControl,
                                                        properties.KeyPadTypes)
                                   : new InputPanelItem(_focusedInputControl, properties.Title, InputPanelEvent,
                                                        properties.KeyPadTypes);

                    PreComInputPanel2.Show(item);
                }

            }
        }

        private OnScreenKeyboardProperties GetPropertiesForInputControl()
        {
            OnScreenKeyboardProperties properties;
            Boolean textBoxHasProperties = _focusedInputControl.Tag is OnScreenKeyboardProperties;

            if (textBoxHasProperties)
            {
                properties = _focusedInputControl.Tag as OnScreenKeyboardProperties;
            }
            else
            {
                //Default properties
                properties = new OnScreenKeyboardProperties("");
            }

            return properties;
        }

        private void InputPanelEvent(object sender, InputPanelArgs e)
        {
            bool userFinishedTyping = e.Reason == InputPanelReason.Ok;

            if (userFinishedTyping)
            {
                PreComInputPanel2.Hide();
                //Without this, backspacing a text will not work
                _focusedInputControl.Text = e.Text.Trim();

                // Workaround for Precom Bug 
                // As long as PreCom softkeyboard is up it eats all system input events. 
                // Hence we need to let the softkeyboard close before we invoke our own
                // windows. Messages to do this is already in queue and we put our event 
                // last in queue by doing BeginInvoke()
                Action<bool, String> doBarcodeScanned = InvokeTextChangedEvent;
                _focusedInputControl.BeginInvoke(doBarcodeScanned, new object[] { true, e.Text.Trim() });
            }
            ReactivateInputControl(sender, e);
        }

        private void ReactivateInputControl(object sender, InputPanelArgs e)
        {
            if (e.Reason != InputPanelReason.Show)
                _focusedInputControl.Focus();
        }

        private void InvokeTextChangedEvent(bool userFinishedTyping, string textEntered)
        {
            var textChangedEvent = GetTextChangedEvent(_focusedInputControl);
            if (textChangedEvent != null && HardwareKeyboard.IsExecuting == false)
            {
                lock (HardwareKeyboard.Lock)
                {
                    HardwareKeyboard.IsExecuting = true;
                    textChangedEvent.Invoke(userFinishedTyping, textEntered);
                    HardwareKeyboard.IsExecuting = false;
                }
            }
        }

        private OnScreenKeyboardProperties.TextEnteredDelegate GetTextChangedEvent(Control inputControl)
        {

            if (inputControl != null)
            {
                try
                {
                    var properties = inputControl.Tag as OnScreenKeyboardProperties;
                    if (properties != null)
                    {
                        return properties.TextChanged;
                    }
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch (Exception){}
                // ReSharper restore EmptyGeneralCatchClause
            }
            return null;
        }

        void InputControlGotFocus(object sender, EventArgs e)
        {
            try
            {

                _focusedInputControl = sender as Control;
                bool isTextBox = _focusedInputControl is TextBox && ((TextBox)_focusedInputControl).ReadOnly == false;

                if (isTextBox == false)
                    return;

                if (_module.OnScreenKeyboardIconItem == null)
                    ShowOnScreenKeyboardIcon();
                else
                {
                    EnableOnScreenKeyboardIcon();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OnScreenKeyBoardUtil.InputControlGotFocus");
            }
        }

        void InputControlLostFocus(object sender, EventArgs e)
        {
            try
            {
                if (_module.OnScreenKeyboardIconItem == null)
                    HideOnScreenKeyboardIcon();
                else
                {
                    DisableOnScreenKeyboardIcon();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OnScreenKeyboardUtil.InputControlLostFocus");
            }
        }

        private void InputControlNumberKeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Boolean isEnterPressed = e.KeyChar == 13;

                if (isEnterPressed)
                    InvokeTextChangedEvent(true, ((Control)sender).Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OnScreenKeyBoardUtil.InputControlNumberKeyPress");
            }
        }

        private void InputControlTextChanged(object sender, EventArgs e)
        {
            try
            {
                InvokeTextChangedEvent(false, ((Control)sender).Text.Trim());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OnScreenKeyboardUtil.InputControlTextChanged");
            }
        }

        public void DisableOnScreenKeyboard(Control.ControlCollection controlCollection)
        {
            HideOnScreenKeyboardIcon();

            DisableOnScreenKeyboard(controlCollection, 0);
        }



        private void DisableOnScreenKeyboard(Control.ControlCollection controlCollection, int depth)
        {
            try
            {
                // Safeguard against unlimited recusrion if the control-collection somehow is recursive
                if (depth < 10)
                {
                    foreach (Control control in controlCollection)
                    {
                        if (control.IsDisposed == false)
                        {
                            if (IsInputControl(control))
                            {
                                ResetEventsToControl(control);
                            }
                        }

                        DisableOnScreenKeyboard(control.Controls, depth + 1);
                    }
                }
                else
                {
                    Logger.LogEvent(Severity.Error, "Detected deep recursion in OnScreenKeyBoardUtil.DisableOnScreenKeyboard");
                }
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "OnScreenKeyboardUtil.DisableOnScreenKeyboard");
            }
        }
    }

    public class OnScreenKeyboardProperties
    {
        public OnScreenKeyboardProperties(String title, params Keypad[] keyPadTypes)
        {
            Title = title;
            KeyPadTypes = keyPadTypes.Length == 1 ? keyPadTypes : AllKeyPadTypes;
        }

        public OnScreenKeyboardProperties(String title)
        {
            Title = title;
            KeyPadTypes = AllKeyPadTypes;
        }

        private Keypad[] AllKeyPadTypes
        {
            get
            {
                return KeyPadTypes = new[]
                {
                    Utility.KeyPadTypes.Qwerty,
                    Utility.KeyPadTypes.QwertyUpperCase,
                    Utility.KeyPadTypes.Numeric,
                    Utility.KeyPadTypes.Symbol
                };
            }
        }


        public Keypad[] KeyPadTypes { get; private set; }
        public String Title { get; set; }
        public TextEnteredDelegate TextChanged { get; set; }

        //userFinishedTyping means that the onscreenkeyboard ok button is typed or that the hardware enter button is pressed
        public delegate void TextEnteredDelegate(Boolean userFinishedTyping, String textEntered);
    }
}
﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    // Parses a string of format param1name:param1value|....|paramNname, paramNvalue
    public class VasParamParser : Dictionary<string, string>
    {
        public VasParamParser(string parametersString)
        {
            var parameters = parametersString.Split(new [] {'|'});
            foreach (var parameter in parameters)
            {
                var paramComponents = parameter.Split(new [] {':'});
                if(paramComponents.Length != 2)
                    throw new Exception("Detected illegal parameter string: " + parametersString);
                Add(paramComponents[0], paramComponents[1]);
            }
        }

        public string GetEntry(string key)
        {
            string text;
            if (TryGetValue(key, out text) == false)
            {
                text = string.Format(GlobalTexts.VasParamtEntryMissing, key);
            }
            return text;
        }
    }
}

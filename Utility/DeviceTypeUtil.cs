﻿using System.Text;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class determining device
    /// </summary>
    public class DeviceTypeUtil
    {
        private static DevicesTypes _deviceType;
        
        public enum DevicesTypes
        {
            NotSet, MC70, MC75, MC75A, ES400, MC55, MC65,
        }

        public static DevicesTypes GetDeviceType()
        {
            if (_deviceType == DevicesTypes.NotSet)
                _deviceType = FindDeviceType();
            return _deviceType;
        }

        private static DevicesTypes FindDeviceType()
        {
            // Detect the correct device make we are using at the moment
            var strDeviceId = new StringBuilder();
            Win32.SystemParametersInfo(258, 200, strDeviceId, 0);

            //Logger.LogEvent(Severity.Debug, "Detected the following platform: " + strDeviceId);

            switch (strDeviceId.ToString().ToUpper())
            {
                case "SYMBOL MC70":
                case "MOTOROLA MC70":
                    return DevicesTypes.MC70;

                case "MOTOROLA MC75":
                    return DevicesTypes.MC75;

                case "MOTOROLA MC75A":
                    return DevicesTypes.MC75A;

                case "MOTOROLA ES400":
                    return DevicesTypes.ES400;

                case "MOTOROLA MC55":
                    return DevicesTypes.MC55;

                case "MOTOROLA MC65":
                    return DevicesTypes.MC65;

                default:
                    return DevicesTypes.MC65;
            }
        }
    }
}
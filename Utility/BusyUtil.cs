﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class blocking user input and showing waiting cursor while thread is loading.
    /// The general idea is to start when a user has interacted and end it when the work is done.
    /// </summary>
    public class BusyUtil : IDisposable
    {
        private BusyUtil()
        {
            if (_suspendCounter == 0)
                _originalCursorState = Cursor.Current;
            _suspendCounter++;
        }

        private static IntPtr _handleDesktopWindow;
        private static IntPtr HandleDesktopWindow
        {
            get
            {
                if (_handleDesktopWindow == IntPtr.Zero)
                    _handleDesktopWindow = Win32.GetDesktopWindow();
                return _handleDesktopWindow;
            }
        }

        public static bool IsBusy { get; private set;}

        private static Cursor _originalCursorState;
        private static int _suspendCounter;

        private static void BlockUserInput()
        {
            Win32.TouchRegisterWindow(HandleDesktopWindow);
        }

        private static void UnblockUserInput()
        {
            Win32.TouchUnregisterWindow(HandleDesktopWindow);
        }

        public static BusyUtil Suspend()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.BusyUtil))
            {
                var instance = new BusyUtil();

                SetNormal();

                return instance;
            }
        }

        public static void Activate()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.BusyUtil))
            {
                SetBusy();
                _originalCursorState = Cursors.WaitCursor;
            }
        }


        public static void Reset()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.BusyUtil))
            {
                try
                {
                    SetNormal();
                    _originalCursorState = Cursors.Default;
                }
                catch(Exception ex)
                {
                    Debug.WriteLine("Exception while ending busy util: " + ex.Message);
                }
            }
        }

        private static void SetBusy()
        {
            if (IsBusy == false)
            {
                IsBusy = true;
                Cursor.Current = Cursors.WaitCursor;
                Cursor.Show();
                BlockUserInput();
            }
        }

        private static void SetNormal()
        {
            if (IsBusy)
            {
                IsBusy = false;
                Cursor.Current = Cursors.Default;
                UnblockUserInput();
            }
        }

        public void Dispose()
        {
            if (--_suspendCounter == 0)
            {
                if (_originalCursorState == Cursors.WaitCursor)
                    SetBusy();
            }
        }
    }
}

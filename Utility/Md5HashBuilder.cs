using System.Security.Cryptography;
using System.Text;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class Md5HashBuilder
    {
        public string ComputeHash(string data)
        {
            var md5 = MD5.Create();
            var byteArray = md5.ComputeHash(Encoding.ASCII.GetBytes(data));

            var builder = new StringBuilder();
            for (var i = 0; i < byteArray.Length; i++)
            {
                builder.Append(byteArray[i].ToString("X2"));
            }
            return builder.ToString();
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Model;
using PreCom.Core;
using PreCom.Core.Modules;
using LogItem = PreCom.Core.Log.LogItem;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class Logger
    {
        public enum MessageLoggingTypes
        {
            AsyncRequest,
            SyncRequest,
            SyncResponse,
            PushResponse
        }

        private const string SettingDebug = "Debug";
        private const string ValueIgnore = "Ignore";
        private const string ValueLocalFile = "LocalFile";

        private static bool? _logDebugMessages;
        private static readonly AsyncLogWriter AsyncLogWriter;
        private static readonly AsyncServerLogger AsyncServerLogger;

        static Logger()
        {
            if (ModelMain.IsUnitTesting == false)
            {
                AsyncLogWriter = new AsyncLogWriter();
                AsyncServerLogger = new AsyncServerLogger();
            }
        }

        public static bool LogDebugMessages
        {
            get
            {
                try
                {
                    if (_logDebugMessages == null)
                    {
#pragma warning disable 618
                        var log = PreCom.Application.Instance.CoreComponents.Get<ILog>() as PreComBase;

                        if (log != null)
                        {
                            var value = PreCom.Application.Instance.Settings.Read(log, SettingDebug, ValueIgnore, false);
                            _logDebugMessages = value == ValueLocalFile;
                        }
#pragma warning restore 618
                    }

                    return _logDebugMessages == true;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception ocured in exception handler writing loglevel: " + ex.Message);
                }
                return false;
            }

            set
            {
                try
                {
                    _logDebugMessages = value;
#pragma warning disable 618
                    var log = PreCom.Application.Instance.CoreComponents.Get<ILog>() as PreComBase;
#pragma warning restore 618
                    if (log != null)
                    {
                        var settingValue = _logDebugMessages == true ? ValueLocalFile : ValueIgnore;
#pragma warning disable 618
                        PreCom.Application.Instance.Settings.Write(log, SettingDebug, settingValue);
#pragma warning restore 618
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception ocured in exception handler writing loglevel: " + ex.Message);
                }

            }
        }

        public static bool IsAutoTesting { get; set; }
        public static bool ShowMessages { get; set; }


        public static void LogEvent(Severity severity, string text, params object[] args)
        {
            try
            {
                if (ModelMain.IsUnitTesting)
                    return;

                if (!LogDebugMessages && severity == Severity.Debug)
                    return;

                string eventMessage;

                if (args != null && args.Length > 0)
                {
                    eventMessage = string.Format(text, args);
                }
                else
                {
                    eventMessage = text;
                }

                if (severity >= Severity.ExpectedError)
                    AsyncServerLogger.SendAsync(severity, eventMessage, string.Empty, text);

                StringUtil.GetTextForConsignmentEntity(eventMessage, true);

                LogEventAndDump(severity, eventMessage, string.Empty);
            }
            catch (Exception e)
            {
                HandleExceptionWhileLogging(e);
            }
        }

        /// <summary>
        /// Logs data about an exception and raises a fail event in Debug-build to notify developer of exception occurrences
        /// Use this when exceptions are to be expected due to dependencies out of you control
        /// </summary>
        /// <param name="exception">Exception raised</param>
        /// <param name="functionName">Name of function where exception was raised</param>
        public static void LogExpectedException(Exception exception, string functionName)
        {
            LogException(exception, functionName, Severity.ExpectedError);
        }

        /// <summary>
        /// Logs data about an exception and raises a fail event in Debug-build to notify developer of exception occurences
        /// If you have verified the code an exceptions are to be expected due to dependencies out of your control,
        /// use LogExpectedException instead
        /// </summary>
        /// <param name="exception">Exception raised</param>
        /// <param name="functionName">Name of function where exception was raised</param>
        public static void LogException(Exception exception, string functionName)
        {
            // It is extremely important to not throw any exceptions in this method, therefore we add another try-catch here
            try
            {
                LogException(exception, functionName, Severity.Error);
            }
            catch (Exception ex)
            {
                HandleExceptionWhileLogging(ex);
            }
        }

        public static void LogAssert(bool assertTrue)
        {
            if (ModelMain.IsUnitTesting == false)
                LogAssert(assertTrue, "No message");
        }

        public static void LogAssert(bool assertTrue, string msg)
        {
            try
            {
                //Throw an exception to get stacktrace
                if (!assertTrue)
                    throw new ArgumentException();
            }
            catch (ArgumentException ex)
            {
                AsyncServerLogger.SendAsync(Severity.Error, msg, ex.ToString(), msg + ex);
                LogEventAndDump(Severity.Error, msg, ex.StackTrace);
            }
        }

        private static void LogException(Exception exception, string functionName, Severity severity)
        {
            if (functionName.EndsWith("()") == false)
            {
                string.Concat(functionName, "()");
            }

            var logText = string.Format("{0} raised in function {1}: {2}", exception.GetType(), functionName,
                exception.Message);

            if (exception.InnerException != null)
                logText += "\nInner exception: " + exception.InnerException.Message;

            // Do not show message-box if ThreadAbortExceptions occur
            if (exception is ThreadAbortException)
                severity = Severity.ExpectedError;

            AsyncServerLogger.SendAsync(severity, logText, exception.ToString(), functionName + exception);
            LogEventAndDump(severity, logText, exception.ToString());
        }

        private static void LogEventAndDump(Severity severity, string eventMessage, string dump)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Logging))
            {
                try
                {
                    switch (severity)
                    {
                        case Severity.Info:
                            WriteToPrecomLogAsync(eventMessage, dump, LogLevel.Information);
                            break;

                        case Severity.Warning:
                            WriteToPrecomLogAsync(eventMessage, dump, LogLevel.Warning);
                            break;

                        case Severity.Debug:
                            WriteToPrecomLogAsync(eventMessage, dump, LogLevel.Debug);
                            break;

                        case Severity.Error:
                        case Severity.ExpectedError:
                            WriteToPrecomLogAsync(eventMessage, dump, LogLevel.Error);
                            break;
                    }

                    // To avoid this message-box in debug builds log with ExpectedError or ExpectedException
                    if (severity != Severity.Error || IsAutoTesting)
                        return;

                    var result = GuiCommon.ShowModalDialog(severity.ToString(), eventMessage, Severity.NoLog,
                        GlobalTexts.Continue, GlobalTexts.Break, GlobalTexts.Trace);

                    if (result == null) // could be looping error
                    {
                        Application.Exit();
                        return;
                    }

                    if (result == GlobalTexts.Trace)
                    {
                        result = GuiCommon.ShowModalDialog("Stack Trace", dump, Severity.NoLog, GlobalTexts.Continue,
                            GlobalTexts.Break);
                        if (result == null) // looping error ?
                        {
                            Application.Exit();
                            return;
                        }
                    }

                    if (result == GlobalTexts.Break)
                    {
                        if (Debugger.IsAttached)
                            Debugger.Break();
                        else
                            Application.Exit();
                    }
                }
                catch (Exception e)
                {
                    HandleExceptionWhileLogging(e);
                }
            }
        }

        /// <summary>
        /// Writes message as XML to file
        /// </summary>
        public static void LogCommunicationError(MessageLoggingTypes messageType, Object message, string errorText)
        {
            try
            {
                string xmltxt;
                try
                {
                    xmltxt = message.Serialize();
                    xmltxt = xmltxt.Replace("\r\n", "$n");
                }
                catch (Exception)
                {
                    xmltxt = message.GetType().Name;
                }

                LogEvent(Severity.Debug, "Error in communication: {0}, message type (1), message: {2}",
                    errorText, messageType, xmltxt);
            }
            catch (Exception e)
            {
                HandleExceptionWhileLogging(e);
            }
        }

        public static void LogCommunication(MessageLoggingTypes messageType, Object message)
        {
            try
            {
                string xmltxt;
                try
                {
                    xmltxt = message.Serialize();
                    xmltxt = xmltxt.Replace("\r\n", "$n");
                }
                catch (Exception)
                {
                    xmltxt = message.GetType().Name;
                }

                LogEvent(Severity.Debug, "##{0};;{1}",
                     messageType, xmltxt);
            }
            catch
            {
                Debug.WriteLine("Exception ocured in LogCommunication");
            }
        }

        /// <summary>
        /// Do not show any message boxes in this function. Can cause potential problems
        /// </summary>
        /// <param name="e"></param>
        public static void HandleExceptionWhileLogging(Exception e)
        {
            try
            {
                // In theory, non of function calls below throw exception
                BusyUtil.Reset();

#pragma warning disable 618
                var precomLogger = PreCom.Application.Instance.CoreComponents.Get<ILog>();
#pragma warning restore 618
                if (precomLogger != null)
                {
                    var logItem = new LogItem
                    {
                        Body = e.Message,
                        Dump = e.StackTrace,
                        Header = "Exception occured while logging",
                        Level = LogLevel.Error
                    };

                    precomLogger.Write(logItem);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception ocured in exception handler while logging: " + ex.Message);
            }
        }

        static void WriteToPrecomLogAsync(string eventMessage, string dump, LogLevel logLevel)
        {
            AsyncLogWriter.LogAsync(eventMessage, dump, logLevel);
        }
    }
}
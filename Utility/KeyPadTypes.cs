﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using PreCom.Controls;


namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class for getting diffrent keypadtypes based on location setting
    /// "KeyPad" data can be found in the PreCom settings file. (<Keypad_qwerty>)
    /// </summary>
    public class KeyPadTypes
    {
        // ReSharper disable InconsistentNaming
        public static string se_QWERTY;
        public static string se_qwerty;
        public static string se_123;
        public static string se_Sym;
        public static string no_QWERTY;
        public static string no_qwerty;
        public static string no_123;
        public static string no_Sym;
        public static string en_QWERTY;
        public static string en_qwerty;
        public static string en_123;
        public static string en_Sym;

        // ReSharper restore InconsistentNaming
     
        public static Keypad QwertyUpperCase
        {
            get { return KeyPadHelper(GlobalConstants.Keypad_QWERTY); }
        }

        public static Keypad Qwerty
        {
            get { return KeyPadHelper(GlobalConstants.Keypad_qwerty); }
        }

        public static Keypad Numeric
        {
            get { return KeyPadHelper(GlobalConstants.Keypad_123); }
        }

        public static Keypad Symbol
        {
            get { return KeyPadHelper(GlobalConstants.Keypad_Sym); }
        }
        /// <summary>
        /// creates a keypad with given type and language
        /// if precom.settings does not contain the special language 
        /// keypad definitions (in Settings section), the default keypad
        /// (in PreCom section)is used.
        /// </summary>
        /// <param name="keypadType">QWERTY,qwerty,123 or Sym</param>
        /// <returns>keypad matching the language and type or type only</returns>
        private static Keypad KeyPadHelper(string keypadType)
        {

            var currentKeypadLanguage = SettingDataProvider.Instance.GetSetting(GlobalConstants.USER_KEYPAD_LANGUAGE);
            if (currentKeypadLanguage == "") currentKeypadLanguage = "no";
            var keypadData = "";
            switch (currentKeypadLanguage)
            {
                case "se":
                    switch (keypadType)
                    {
                        case GlobalConstants.Keypad_QWERTY:
                            keypadData = se_QWERTY;
                            break;
                        case GlobalConstants.Keypad_qwerty:
                            keypadData = se_qwerty;
                            break;
                        case GlobalConstants.Keypad_123:
                            keypadData = se_123;
                            break;
                        case GlobalConstants.Keypad_Sym:
                            keypadData = se_Sym;
                            break;
                    }
                    break;
                case "no":
                    switch (keypadType)
                    {
                        case GlobalConstants.Keypad_QWERTY:
                            keypadData = no_QWERTY;
                            break;
                        case GlobalConstants.Keypad_qwerty:
                            keypadData = no_qwerty;
                            break;
                        case GlobalConstants.Keypad_123:
                            keypadData = no_123;
                            break;
                        case GlobalConstants.Keypad_Sym:
                            keypadData = no_Sym;
                            break;
                    }
                    break;
                case "en":
                    switch (keypadType)
                    {
                        case GlobalConstants.Keypad_QWERTY:
                            keypadData = en_QWERTY;
                            break;
                        case GlobalConstants.Keypad_qwerty:
                            keypadData = en_qwerty;
                            break;
                        case GlobalConstants.Keypad_123:
                            keypadData = en_123;
                            break;
                        case GlobalConstants.Keypad_Sym:
                            keypadData = en_Sym;
                            break;
                    }
                    break;
               
            }
            return keypadData=="" ? new Keypad(keypadType) : new Keypad(keypadType, keypadData);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using PreCom.Core.Log;
using PreCom.Core.Modules;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    internal class AsyncLogWriter : AsyncQueueWorker<LogQueueItem>
    {
        private readonly string _logFilePath;

        public AsyncLogWriter()
        {
            _logFilePath = Path.Combine(FileUtil.GetApplicationPath(), @"PreCom2.log");
        }
        
        public void LogAsync(string eventMessage, string dump, LogLevel logLevel)
        {
            var logQueueItem = new LogQueueItem { LogLevel = logLevel, EventMessage = eventMessage, Dump = dump };
            ExecuteAsync(logQueueItem);
        }

        protected override void DoWork(Queue<LogQueueItem> logItemQueue)
        {
            try
            {
                using (var writer = new StreamWriter(_logFilePath, true))
                {
                    while (logItemQueue.Count > 0)
                    {
                        var logItem = logItemQueue.Dequeue();
#if !PROFILING
                        writer.WriteLine("{0};{1};0;;{2};{3}", DateTime.Now, logItem.LogLevel, logItem.EventMessage, logItem.Dump);
#else            
                        ProfilingMeasurement.LogEntry(logItem.EventMessage, logItem.LogLevel);    
#endif
                    }
                }
            }
            catch (Exception ex)
            {
                HandleExceptionWhileLogging(ex);
            }
        }

        private void HandleExceptionWhileLogging(Exception e)
        {
            try
            {
                // In theory, non of function calls below throw exception
                BusyUtil.Reset();

                ILog precomLogger = null;
                
#pragma warning disable 618
                var instance = PreCom.Application.Instance;
                if(instance != null)
                    precomLogger = instance.CoreComponents.Get<ILog>();
#pragma warning restore 618

                if (precomLogger == null)
                    return;

                var logItem = new LogItem
                {
                    Body = e.Message,
                    Dump = e.StackTrace,
                    Header = "Exception occured while logging",
                    Level = LogLevel.Error
                };

                precomLogger.Write(logItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Exception ocured in exception handler while logging: " + ex.Message);
            }
        }
    }
}
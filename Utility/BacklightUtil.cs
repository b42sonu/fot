﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Symbol.Display;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class BacklightUtil
    {
        private static BacklightUtil _instance;
        private readonly StandardDisplay _standardDisplay;
        private readonly int _flashes;

        private BacklightUtil()
        {
            try
            {
                var availableDevices = Device.AvailableDevices;

                var standardDevice = availableDevices.FirstOrDefault(availableDevice => availableDevice.DisplayType == DisplayType.StandardDisplay);
                if (standardDevice != null)
                {
                    _standardDisplay = new StandardDisplay(standardDevice);
                    var backlightFlashDuration = SettingDataProvider.Instance.GetCommonSetting(GlobalConstants.BACKLIGHT_FLASH_DURATION, "3");
                    var iFlashDuration = Convert.ToInt32(backlightFlashDuration);
                    _flashes = iFlashDuration;
                }
            }
            catch (Exception ex)
            {

                Logger.LogException(ex, "Failed to create BacklightUtil");
            }

        }

        public static BacklightUtil Instance
        {
            get { return _instance ?? (_instance = new BacklightUtil()); }
        }

        public void FlashBacklight()
        {
            if (_standardDisplay != null)
            {
                var currentState = _standardDisplay.BacklightState;
                var newState = _standardDisplay.BacklightState == BacklightState.OFF
                    ? BacklightState.ON
                    : BacklightState.OFF;

                for (var i = 0; i < _flashes; i++)
                {
                    _standardDisplay.BacklightState = newState;
                    System.Threading.Thread.Sleep(1000);
                    _standardDisplay.BacklightState = currentState;
                    System.Threading.Thread.Sleep(1000);
                }

            }
        }

        public void SetState(BacklightState backlightState)
        {
            if (_standardDisplay != null)
            {
                _standardDisplay.BacklightState = backlightState;
            }
        }
    }
}

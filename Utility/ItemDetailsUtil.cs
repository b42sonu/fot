﻿using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using System.Globalization;
using System;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class ItemDetailsUtil
    {

        /// <summary>
        /// Fills details for consignment 
        /// </summary>
        /// <returns></returns>
        public static List<DetailsPanel.Info> PrepareInfoList(PlannedConsignmentsType consignment, StopType stopInformation)
        {
            var infoList = new List<DetailsPanel.Info>();

            if (consignment != null)
            {
                infoList.Add(new DetailsPanel.Info
                {
                    Heading = GlobalTexts.Consignment,
                    Section = consignment.ConsignmentNumber
                });

                if (consignment.Consignee != null)
                {
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.Consignee,
                        Section = consignment.Consignee.Name1
                    });

                    if (string.IsNullOrEmpty(consignment.Consignee.Name2) == false)
                    {
                        infoList.Add(new DetailsPanel.Info
                        {
                            Heading = GlobalTexts.Consignee + " " + GlobalTexts.Name,
                            Section = consignment.Consignee.Name2
                        });
                    }

                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.ConsigneeAddr,
                        Section = consignment.Consignee.Address1 + " " + consignment.Consignee.PostalCode
                    });

                    infoList.Add(new DetailsPanel.Info
                        {
                            Heading = GlobalTexts.Town,
                            Section = consignment.Consignee.PostalName
                        });

                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.TelephoneNumber,
                        Section = consignment.Consignee.PhoneNumber
                    });
                }

                if (consignment.Consignor != null)
                {
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.Consignor,
                        Section = consignment.Consignor.Name1
                    });
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.ConsignorAddr,
                        Section = consignment.Consignor.Address1 + " " + consignment.Consignor.PostalCode
                    });
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.Town,
                        Section = consignment.Consignor.PostalName
                    });
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.TelephoneNumber,
                        Section = consignment.Consignor.PhoneNumber
                    });
                }


                if (stopInformation != null)
                {
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.DriverInstruction,
                        Section = stopInformation.AdditionalInfo
                    });
                }

                infoList.Add(new DetailsPanel.Info
                {
                    Heading = GlobalTexts.NumberOfItems,
                    Section = consignment.ConsignmentItemCount.ToString(CultureInfo.InvariantCulture)
                });

                if (consignment.Weight != null)
                {
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.Weight,
                        Section = consignment.Weight.Amount + consignment.Weight.MeasureUnit
                    });
                }

                if (consignment.Volume != null)
                {
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.Volume,
                        Section = consignment.Volume.Amount + consignment.Volume.MeasureUnit
                    });
                }

                if (consignment.ProductCategory != null)
                {
                    infoList.Add(new DetailsPanel.Info
                    {
                        Heading = GlobalTexts.Temperature,
                        Section = consignment.ProductCategory
                    });
                }
            }
            return infoList;
        }


        /// <summary>
        /// Fills details for consignment item
        /// </summary>
        /// <param name="consignmentItem"></param>
        /// <returns></returns>
        public static List<DetailsPanel.Info> PrepareInfoList(ConsignmentItem consignmentItem)
        {
            var infoList = new List<DetailsPanel.Info>();
            if (consignmentItem != null)
            {
                //Append last production event detail
                if (consignmentItem.PlannedConsignmentItem != null && consignmentItem.PlannedConsignmentItem.EventTypeList != null && consignmentItem.PlannedConsignmentItem.EventTypeList.Length > 0)
                {

                    var productionEventDetail = FormatLastProductionEvent(consignmentItem);

                    infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.LastProductionEvent, Section = productionEventDetail });
                }

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsignmentItem, Section = consignmentItem.ItemId });

                var isNull = !(consignmentItem.PlannedConsignment != null && consignmentItem.PlannedConsignment.Consignee != null);

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Consignee, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignee.Name1 });
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsigneeAddr, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignee.Address1 + " " + consignmentItem.PlannedConsignment.Consignee.PostalCode });
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignee.PostalName });
                //defect 259 fixes
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsigneeAddr, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignee.Address1 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignee.CountryCode+" - "+ consignmentItem.PlannedConsignment.Consignee.PostalCode +"  "+ consignmentItem.PlannedConsignment.Consignee.PostalName });

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Telephone, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignee.PhoneNumber });


                isNull = !(consignmentItem.PlannedConsignment != null && consignmentItem.PlannedConsignment.Consignor != null);

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Consignor, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignor.Name1 });
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsignorAddr, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignor.Address1 + " " + consignmentItem.PlannedConsignment.Consignor.PostalCode });
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignor.PostalName });

                //Defect 259 fixed
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsignorAddr, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignor.Address1 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignmentItem.PlannedConsignment.Consignor.CountryCode + " - " + consignmentItem.PlannedConsignment.Consignor.PostalCode + "  " + consignmentItem.PlannedConsignment.Consignor.PostalName });

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ProductCode, Section = consignmentItem.ProductCode ?? string.Empty });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Placement, Section = consignmentItem.Shelf ?? string.Empty });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Weight, Section = Convert.ToString(consignmentItem.Weight) ?? string.Empty });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Volume, Section = Convert.ToString(consignmentItem.Volume) ?? string.Empty });

            }
            return infoList;
        }

        public static string FormatLastProductionEvent(ConsignmentItem consignmentItem)
        {
            var lastProductionEvent = consignmentItem.PlannedConsignmentItem.EventTypeList[consignmentItem.PlannedConsignmentItem.EventTypeList.Length - 1];
            var eventDetail = new[] 
            { 
                lastProductionEvent.EventCode, 
                lastProductionEvent.EventTime.ToString(CultureInfo.InvariantCulture), 
                lastProductionEvent.LocationId 
            };

            string result = eventDetail.Where(lastEvent => !string.IsNullOrEmpty(lastEvent)).Aggregate(string.Empty, (current, lastEvent) => string.Format("{0}, {1}", current, lastEvent).TrimStart(',').Trim());
            
            return result;
        }

        /// <summary>
        /// Fills details for consignment
        /// </summary>
        /// <param name="consignment"></param>
        /// <returns></returns>
        public static List<DetailsPanel.Info> PrepareInfoList(Consignment consignment)
        {

            var infoList = new List<DetailsPanel.Info>();
            if (consignment != null)
            {
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Consignment, Section = consignment.ConsignmentId });

                var isNull = !(consignment.PlannedConsignment != null && consignment.PlannedConsignment.Consignee != null);

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Consignee, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignee.Name1 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsigneeAddr, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignee.Address1 + " " + consignment.PlannedConsignment.Consignee.PostalCode });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignee.PostalName });
                
                ////Defect 259 fixed
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsigneeAddr, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignee.Address1 });
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignee.CountryCode + " - " + consignment.PlannedConsignment.Consignee.PostalCode +" "+ consignment.PlannedConsignment.Consignee.PostalName });

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Telephone, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignee.PhoneNumber });

                isNull = !(consignment.PlannedConsignment != null && consignment.PlannedConsignment.Consignor != null);

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Consignor, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignor.Name1 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsignorAddr, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignor.Address1 + " " + consignment.PlannedConsignment.Consignor.PostalCode });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignor.PostalName });

                ////Defect 259 fixed
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ConsignorAddr, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignor.Address1 });
                //infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Town, Section = isNull ? string.Empty : consignment.PlannedConsignment.Consignor.CountryCode + " - " + consignment.PlannedConsignment.Consignor.PostalCode + " " + consignment.PlannedConsignment.Consignor.PostalName });


                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ProductCode, Section = consignment.ProductCode ?? string.Empty });

            }
            return infoList;
        }

        /// <summary>
        /// Fills details for LoadCarrier
        /// </summary>
        /// <param name="loadCarrier"></param>
        /// <returns></returns>
        public static List<DetailsPanel.Info> PrepareInfoList(LoadCarrier loadCarrier)
        {
            var infoList = new List<DetailsPanel.Info>();
            if (loadCarrier != null)
            {
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.LoadCarrierId, Section = loadCarrier.LoadCarrierId });

                var isNull = (loadCarrier.LogicalLoadCarrierDetails == null);

                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.LoadCarrierType, Section = isNull ? string.Empty : loadCarrier.LogicalLoadCarrierDetails[0].LoadCarrierType });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.DestinationName + " " + GlobalTexts.Name, Section = isNull ? string.Empty : loadCarrier.LogicalLoadCarrierDetails[0].DestinationName1 + ' ' + loadCarrier.LogicalLoadCarrierDetails[0].DestinationName2 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.PostgangCode, Section = isNull ? string.Empty : loadCarrier.LogicalLoadCarrierDetails[0].PostgangCode });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ProductionFlowCode, Section = isNull ? string.Empty : loadCarrier.LogicalLoadCarrierDetails[0].ProductionFlowCode });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.TimeCreated, Section = isNull ? string.Empty : loadCarrier.LogicalLoadCarrierDetails[0].TimeCreated.ToShortTimeString() });
            }
            return infoList;
        }

        /// <summary>
        /// Fills details for LoadCarrier
        /// </summary>
        /// <param name="loadCarrier"></param>
        /// <returns></returns>
        public static List<DetailsPanel.Info> PrepareInfoList(GetGoodsListResponseOperationLoadCarrier loadCarrier)
        {
            var infoList = new List<DetailsPanel.Info>();
            if (loadCarrier != null)
            {
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.LoadCarrierId, Section = loadCarrier.loadCarrierId });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.DestinationName + " " + GlobalTexts.Name, Section = loadCarrier.destinationName1 + (string.IsNullOrEmpty(loadCarrier.destinationName2) ? string.Empty : ' ' + loadCarrier.destinationName2) });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.PostgangCode, Section = string.IsNullOrEmpty(loadCarrier.postgangCode) ? string.Empty : loadCarrier.postgangCode });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.CdAddress1, Section = string.IsNullOrEmpty(loadCarrier.cDAddress1) ? string.Empty : loadCarrier.cDAddress1 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.CdAddress2, Section = string.IsNullOrEmpty(loadCarrier.cDAddress2) ? string.Empty : loadCarrier.cDAddress2 });
                infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.CdAddress3, Section = string.IsNullOrEmpty(loadCarrier.cDAddress3) ? string.Empty : loadCarrier.cDAddress3 });
            }
            return infoList;
        }
    }
}

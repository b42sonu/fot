﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Resco.Controls.OutlookControls;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// This calss contains string validations.
    /// </summary>
    public class StringUtil
    {
        /// <summary>
        /// Check if a string only contains letters (upper case and lower case).
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsAlfa(string text)
        {
            var regex = new Regex(@"^[a-zA-Z]+$");

            return regex.IsMatch(text);
        }

        /// <summary>
        /// Check if a string only contains uppercase letters
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool IsAlfaUpper(string data)
        {
            const string alfachars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            return data.All(c => alfachars.IndexOf(c) != -1);
        }

        /// <summary>
        /// Check if a string only contains of a mix of numbers and letters (upper case and lower case).
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsAlfaNumeric(string text)
        {
            var regex = new Regex(@"^[0-9a-zA-Z]+$");

            return regex.IsMatch(text);
        }

        /// <summary>
        /// Check if a string is a non signed, integer number
        /// </summary>
        /// <returns></returns>
        public static bool IsNumeric(string textEntry)
        {
            var result = false;

            if (string.IsNullOrEmpty(textEntry) == false)
            {
                var objNotWholePattern = new Regex("[^0-9]");
                result = objNotWholePattern.IsMatch(textEntry) == false;
            }

            return result;
        }

        /// <summary>
        /// Check if a string is a non signed, non decimal numeric number that may have leading zeros.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsLeadingZerosNumeric(string text)
        {
            var regex = new Regex(@"^[0-9]+$");

            return regex.IsMatch(text);
        }

        public static string[] GetStringArray(Type type)
        {
            //use Reflection to get the fields in our enum
            var info = type.GetFields();

            return info.Select(fInfo => fInfo.Name).ToArray();
        }

        /// <summary>
        /// Return similar length string if string exceeds space
        /// </summary>
        public static string ReturnCommonFormattedText(string text, double maxLimit)
        {
            double weightage = 0;
            string finalString = string.Empty;

            if (string.IsNullOrEmpty(text))
                return string.Empty;

            foreach (char c in text)
            {
                weightage += GetWeightage(c);
                if (weightage < maxLimit)
                    finalString += c;
                else
                {
                    finalString += "...";
                    break;
                }
            }
            return finalString;
        }

        /// <summary>
        /// Assign weightage as per width chars take
        /// </summary>
        private static double GetWeightage(char c)
        {
            if (char.IsNumber(c) || char.IsPunctuation(c))
                return 1;
            
            switch (c)
            {
                case 'w':
                case 'm':
                    return 1.5;
                case 'i':
                    return .45;
                case 'j':
                    return .6;
                case 't':
                    return .84;
                case '=':
                    return 1.4;

            }

            
            if (Char.IsLower(c) == false)
                return 1.8;


            return 1;
        }

        static readonly ToolTip LabelToolTip = new ToolTip();
        public static void SetText(Resco.Controls.CommonControls.TransparentLabel label, string text)
        {
            if (string.IsNullOrEmpty(text) || text.Length < 4||label.Size.Width<10)
            {
                label.Text = text;
                return;
            }
           int width = label.Width;
           bool initialAutoSize = label.AutoSize;
            label.AutoSize = true;
            label.Text = text;
            if (LabelToolTip.Controls.Contains(label))
                LabelToolTip.Controls.Remove(label);
            if (label.Width > width &&text.Length>1)
            {
                int index;
                if (label.Width >= (width * 2))
                {
                    index = 1;
                    label.Text = text.Substring(0, index++);
                    while (label.Width <= width&&index<text.Length)
                    {
                        label.Text = text.Substring(0, index++);
                    }
                    if (label.Text.Length > index - 4)
                    label.Text = label.Text.Substring(0, index - 4) + "...";
                }
                else
                {
                    index = text.Length - 2;
                    label.Text = text.Substring(0, index--);
                    while (label.Width > width && index < text.Length)
                    {
                        label.Text = text.Substring(0, index--);
                    }
                    if (label.Text.Length > index - 1)
                    label.Text = label.Text.Substring(0, index - 1) + "...";
                }
                LabelToolTip.TitleText = "Actual value";
                LabelToolTip.Text = text;
                LabelToolTip.Controls.Add(label);
            }
            label.AutoSize = initialAutoSize;
            label.Width = width;

        }

        public static string ToDisplayEntity(string entityNumber)
        {
            if (entityNumber == null || entityNumber.Length <= 10 || entityNumber == Consignment.SyntheticConsignmentIdWarning)
                return entityNumber;

            entityNumber = entityNumber.Trim();
            var result = entityNumber.Substring(0, 2) + "..." + entityNumber.Substring(entityNumber.Length - 5);
            return result;
        }

        /// <summary>
        /// Change the message for string containing item for logging purposes
        /// </summary>
        public static string GetTextForConsignmentEntity(string entityToChange, bool isLogging)
        {
            //string msgToChange = "Consignment number #ItemDisplayId370...8939|3707193498939| is registered.";

            if (entityToChange.Contains("#ItemDisplayId:"))
            {
                string operationalString = Regex.Split(entityToChange, "#ItemDisplayId:")[1]; // "370...8939|3707193498939| is registered."

                entityToChange = entityToChange.Replace("#ItemDisplayId:", "");
                //   "Consignment number 370...8939|3707193498939| is registered."

                int lastPipe = operationalString.LastIndexOf('|')+ 1;
                string strFirstPipe = operationalString.Substring(0, lastPipe); // "370...8939|3707193498939"
                string strRequired = isLogging == false ? strFirstPipe.Split('|')[0] : strFirstPipe.Split('|')[1];

               entityToChange = entityToChange.Replace(strFirstPipe, strRequired); //   "Consignment number 3707193498939| is registered."

            }
            return entityToChange;
        }
    }
}
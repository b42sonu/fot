﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public struct Memorystatus
    {
        public UInt32 DwLength;
        public UInt32 DwMemoryLoad;
        public UInt32 DwTotalPhys;
        public UInt32 DwAvailPhys;
        public UInt32 DwTotalPageFile;
        public UInt32 DwAvailPageFile;
        public UInt32 DwTotalVirtual;
        public UInt32 DwAvailVirtual;
    }

    public class MemoryMeasurements : IDisposable
    {
        static Timer _timer;

        public MemoryMeasurements()
        {
            _timer = new Timer(LogMemoryUsage, null, TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(10));
        }

        public void Dispose()
        {
            if (_timer != null)
            {
                _timer.Dispose();
                _timer = null;
            }
        }

        public static void GetGlobalMemoryStatus(out UInt32 dwTotal, out UInt32 dwAvail,
                                             out UInt32 dwProcTotal, out UInt32 dwProcAvail)
        {
            var status = new Memorystatus();
            status.DwLength = (UInt32)Marshal.SizeOf(status);
            Win32.GlobalMemoryStatus(out status);

            dwTotal = status.DwTotalPhys / 1024;
            dwAvail = status.DwAvailPhys / 1024;
            dwProcTotal = status.DwTotalVirtual / 1024;
            dwProcAvail = status.DwAvailVirtual / 1024;
        }

        private void LogMemoryUsage(object param)
        {
            try
            {
                uint totalRamMemory;
                uint availRamMemory;
                uint procTotalRamMemory;
                uint procAvailRamMemory;

                GetGlobalMemoryStatus(out totalRamMemory, out availRamMemory,
                                      out procTotalRamMemory, out procAvailRamMemory);
                
                float usedMemory = (totalRamMemory - availRamMemory);
                var ramPercent = (int)((usedMemory / totalRamMemory) * 100.0);

                float procUsedmemory = procTotalRamMemory - procAvailRamMemory;
                var procRamPercent = (int)((procUsedmemory / procTotalRamMemory) * 100.0);


                var usedGcMemory = GC.GetTotalMemory(false) / 1024;

                var measurements = string.Format(
                    "{0} PDA: {1}% Process: {2}%  Available: {3}  GC usage: {4}",
                    DateTime.Now, ramPercent, procRamPercent, procAvailRamMemory, usedGcMemory);

                WriteMeasurements(measurements);
            }
            catch (Exception ex)
            {
#if !PreComLT
                Logger.LogException(ex, "MemoryMeasurements.LogMemoryUsage");
#endif
            }
        }

        private static void WriteMeasurements(string measurements)
        {
            using (var sw = new StreamWriter(Path.Combine(FileUtil.GetApplicationPath(), @"MemoryMeasurements.log"), true))
            {
                sw.WriteLine(measurements);
            }
        }
    }
}






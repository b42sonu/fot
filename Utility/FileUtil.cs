﻿using System;
using System.IO;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class FileUtil
    {
        public static string GetApplicationPath()
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            return path;
        }

        public static bool LoadListExist()
        {
            Logger.LogEvent(Severity.Debug, "Method : SettingsCommands.LoadListExist()");
            bool fileExist = false;
            try
            {
                string applicationPath = GetApplicationPath();
                string filePath = Path.Combine(Path.Combine(applicationPath, "xml"), "LoadList.xml");
                if (File.Exists(filePath))
                    fileExist = true;
            }
            catch (Exception ex)
            {
                fileExist = false;
                Logger.LogExpectedException(ex, "SettingsCommands.LoadListExist()");
            }
            return fileExist;
        }
    }
}

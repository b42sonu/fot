namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public enum LogQueueItemType
    {
        Event,
        Exception
    }
}
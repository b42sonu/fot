﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class Win32
    {
        public const int LmemZeroinit = 0x40;

        #region PInvokes to coredll.dll
        [DllImport("coredll.dll", EntryPoint = "#33", SetLastError = true)]
        public static extern IntPtr LocalAlloc(int flags, int byteCount);

        [DllImport("coredll.dll", EntryPoint = "#36", SetLastError = true)]
        public static extern IntPtr LocalFree(IntPtr hMem);

        [DllImport("coredll.dll")]
        public static extern void GlobalMemoryStatus(out Memorystatus lpBuffer);

        [DllImport("coredll.dll", EntryPoint = "SystemParametersInfo", SetLastError = true)]
        public static extern int SystemParametersInfo(int uiAction, int uiParam, StringBuilder pvParam, int fWinIni);

        [DllImport("coredll.dll", SetLastError = true)]
        public static extern IntPtr GetDesktopWindow();

        [DllImport("coredll.dll", EntryPoint = "FindWindowW", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("coredll.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);

        [DllImport("coredll.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Auto)]
        public static extern IntPtr CreateEvent(IntPtr lpEventAttributes, [In, MarshalAs(UnmanagedType.Bool)] bool bManualReset, [In, MarshalAs(UnmanagedType.Bool)] bool bIntialState, [In, MarshalAs(UnmanagedType.BStr)] string lpName);


        [DllImport("coredll.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EventModify(IntPtr hEvent, [In, MarshalAs(UnmanagedType.U4)] int dEvent);
        public enum EventFlags
        {
            Pulse = 1,
            Reset = 2,
            Set = 3
        }
        public static bool SetEvent(IntPtr hEvent) { return EventModify(hEvent, (int)EventFlags.Set); }

        [DllImport("coredll.dll")]
        public static extern int CloseHandle(IntPtr hObject);

        [DllImport("coredll.dll")]
        public static extern int WaitForMultipleObjects(int nCount, IntPtr lpHandles, int fWaitAll, int dwMilliseconds);

        #endregion



        #region PInvokes to touch.dll
        [DllImport("touch.dll", SetLastError = true)]
        public static extern bool TouchRegisterWindow(IntPtr hwnd);

        [DllImport("touch.dll", SetLastError = true)]
        public static extern void TouchUnregisterWindow(IntPtr hwnd);
        #endregion


        #region PInvokes to gpsapi.dll
        [DllImport("gpsapi.dll")]
        public static extern IntPtr GPSOpenDevice(IntPtr hNewLocationData, IntPtr hDeviceStateChange, string szDeviceName, int dwFlags);

        [DllImport("gpsapi.dll")]
        public static extern int GPSCloseDevice(IntPtr hGPSDevice);

        [DllImport("gpsapi.dll")]
        public static extern int GPSGetPosition(IntPtr hGPSDevice, IntPtr pGPSPosition, int dwMaximumAge, int dwFlags);

        [DllImport("gpsapi.dll")]
        public static extern int GPSGetDeviceState(IntPtr pGPSDevice);
        #endregion
    }
}
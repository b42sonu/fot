﻿using System.Drawing;

namespace Com.Bring.PMP.PreComFW.Shared.Forms
{
    partial class MessageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelTitle = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.multiButtonControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MultiButtonControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.labelTitle)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.labelTitle);
            this.touchPanel.Controls.Add(this.multiButtonControl);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 278, 240, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 240;
            this.touchPanel.HScrollBarProperties.Maximum = 460;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(240, 294);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = false;
            this.labelTitle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelTitle.Location = new System.Drawing.Point(2, 2);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(476, 50);
            this.labelTitle.Text = "Ttile";
            this.labelTitle.BackColor = Color.FromArgb(46, 48, 50);
            this.labelTitle.ForeColor = Color.White;
            this.labelTitle.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // messageControl
            // 
            this.messageControl.Location = new System.Drawing.Point(21, 73);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438, 174);
            this.messageControl.TabIndex = 51;
            // 
            // MultiButtonControl
            // 
            this.multiButtonControl.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                   | System.Windows.Forms.AnchorStyles.Right)));
            this.multiButtonControl.Location = new System.Drawing.Point(3, 433);
            this.multiButtonControl.Name = "multiButtonControl";
            this.multiButtonControl.Size = new System.Drawing.Size(474, 99);
            this.multiButtonControl.TabIndex = 50;
            // 
            // MessageDialog
            // 
            this.Name = "MessageDialog";
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.labelTitle)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Controls.MultiButtonControl multiButtonControl;
        private Resco.Controls.CommonControls.TransparentLabel labelTitle;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
    }
}
﻿using System;
using System.Threading;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;
using PreCom.Core;

namespace Com.Bring.PMP.PreComFW.Shared.Forms
{
    public partial class MessageDialog : PreComDialog
    {
        private string DialogResult { get; set; }
        private bool _alreadyLoggedOut;
        
        private readonly AutoResetEvent _finishedEvent = new AutoResetEvent(false);

        private readonly EventHandler _previousHardwareEnterHandler;

        public static string Show(string title, string text, Severity severity, params string[] buttonTexts)
        {
            string result = null;
            try
            {
                MessageDialog dialog = null;
                if (BaseModule.MainForm.InvokeRequired)
                    BaseModule.MainForm.Invoke((Action)(() => dialog = Launch(title, text, severity, buttonTexts)));
                else
                    dialog = Launch(title, text, severity, buttonTexts);

                result = dialog.DialogResult;
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.Show");
            }

            return result;
        }

        public static void ShowDuringWorkExecution(string title, string text, Severity severity, Action workAction)
        {
            if (BaseModule.MainForm.InvokeRequired)
            {
                BaseModule.MainForm.Invoke((Action)(() => ShowDuringWorkExecution(title, text, severity, workAction)));
                return;
            }

            var dialog = new MessageDialog(title, text, severity);
            using (BusyUtil.Suspend())
            {
                dialog.Show();
                ThreadPool.QueueUserWorkItem(state =>
                    {
                        workAction();
                        BaseModule.MainForm.Invoke((Action)(() => dialog.ClosePopUp(dialog, new EventArgs())));
                    });
                dialog.RunPump();
            }
        }

        private static MessageDialog Launch(string title, string text, Severity severity, string[] buttonTexts)
        {
            using (BusyUtil.Suspend())
            {
                var dialog = new MessageDialog(title, text, severity, buttonTexts);
                

                if (BaseModule.ActiveModule != null && BaseModule.ActiveModule.Form!=null && BaseModule.ActiveModule.CurrentView != null)
                    dialog.Show(BaseModule.MainForm);
                else
                    dialog.Show();

                dialog.RunPump();
                Logger.LogEvent(Severity.Debug, "MessageDialog Launch");
                
                return dialog;
            }
        }


        private MessageDialog(string title, string text, Severity severity, params string[] buttonTexts)
        {
            try
            {
                InitializeComponent();
                var image = GuiCommon.Instance.BackgroundImage;
                if (image != null)
                {
                    touchPanel.BackgroundImage = image;
                }

                LogEvent(StringUtil.GetTextForConsignmentEntity(text,true), severity);

                Type = DialogType.Dialog;

                DialogResult = null;
                _previousHardwareEnterHandler = HardwareKeyboard.OnEnterKeyPressed;

                var i = 0;
                foreach (string buttonText in buttonTexts)
                {
                    var tabButtonType = TabButtonType.Normal;
                    if (i == 0)
                        tabButtonType = TabButtonType.Confirm;
                    else if (i == 1)
                        tabButtonType = TabButtonType.Cancel;

                    var button = new TabButton(buttonText, ClosePopUp, tabButtonType);
                    multiButtonControl.ListButtons.Add(button);
                    i++;
                }

                multiButtonControl.GenerateButtons();
                InitMessage(title, text, severity);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.ShowNewModalDialog");
            }
        }

        private void LogEvent(string text, Severity severity)
        {
            if (severity != Severity.NoLog)
            {
                if (severity == Severity.Error)
                    severity = Severity.ExpectedError;
                Logger.LogEvent(severity, text);
            }
        }


        private void RunPump()
        {
            // Stop processing of new operation lists while showing modal dialog
            bool isOnSensitiveScreenOrModuleOrig = ModelMain.IsOnSensitiveScreenOrModule;

            try
            {
                FotScanner.Instance.Suspend();
                ModelModules.Communication.LoginStatus += LoginStatusEventReceived;

                ModelMain.IsOnSensitiveScreenOrModule = true;

                while (_finishedEvent.WaitOne(10, false) == false)
                {
                    Application.DoEvents();
                    Thread.Sleep(0);
                    if(IsDisposed == false)
                        BringToFront();
                }
            }

            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "GuiCommon.ShowModalDialog");
            }
            finally
            {

                ModelModules.Communication.LoginStatus -= LoginStatusEventReceived;
                FotScanner.Instance.Resume();
                if (isOnSensitiveScreenOrModuleOrig == false)
                    ModelMain.IsOnSensitiveScreenOrModule = false;
            }
        }

        private void LoginStatusEventReceived(LoginEventArgs e)
        {
            try
            {
                if (e.Status == LoginStatusInfo.LogoutOK)
                {
                    if (_alreadyLoggedOut == false)
                    {
                        _alreadyLoggedOut = true;
                        ClosePopUp(null, null);
                        Logger.LogEvent(Severity.Debug, "LoginStatusEventReceived logoutOk received in MessageDialog");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ModuleSettings.LoginStatusEventReceived " + ex.Message);
            }

        }


        private void ClosePopUp(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object, EventArgs>(ClosePopUp), sender, e);
                return;
            }
            try
            {
                var button = sender as TabButton;
                // If 0, it was triggered by hardware enter on default button
                DialogResult = button == null ? multiButtonControl.ListButtons[0].Text : button.Text;
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.ClosePopUp");
            }
            finally
            {
                Hide();
                HardwareKeyboard.SubscribeEnterKeyPressed(_previousHardwareEnterHandler);
                _finishedEvent.Set();
            }
        }

        private void InitMessage(string messageTitle, string messageText, Severity severity)
        {
            try
            {
                labelTitle.Text = messageTitle;
                var messageHolder = new MessageHolder();
                switch (severity)
                {
                    case Severity.Debug:
                    case Severity.Info:
                        messageHolder.Update(messageText, MessageState.Information);
                        break;

                    case Severity.Warning:
                        messageHolder.Update(messageText, MessageState.Warning);
                        SoundUtil.Instance.PlayWarningSound();
                        break;

                    default:
                        messageHolder.Update(messageText, MessageState.Error);
                        SoundUtil.Instance.PlayScanErrorSound();
                        break;
                }
                messageControl.ShowMessage(messageHolder);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.InitMessage");
            }
        }
    }
}
﻿using System;
using System.Drawing;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.OutlookControls;

namespace Com.Bring.PMP.PreComFW.Shared.Forms
{
    public partial class FormModuleMenu : BaseForm
    {
        public readonly MultiButtonControl MultiButtonControl;
        private int CurrentPage { get; set; }
        private int TotalPages { get; set; }
        private const string ButtonBackName = "ButtonBackName";
        private const string ButtonNextName = "ButtonNextName";
        public FormModuleMenu()
        {
            InitializeComponent();
            MultiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(MultiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
                panelButtonContainer.BackgroundImage = image;
            }
            SetStandardControlProperties(labelModuleName, lblOrgUnit, null, null, null, null, null);

            AddButtons();
        }

        private ModuleMenuItem[] _menuItems;
        private int _cancelEvent;

        private void AddButtons()
        {

            try
            {
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBackName });
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Next, ButtonNextClick, ButtonNextName));
                MultiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.AddButtons");
            }
        }

        public override void OnShow(IFormData formData)
        {
            if (formData != null)
            {
                var formDataModuleMenu = (FormDataModuleMenu)formData;

                _cancelEvent = formDataModuleMenu.CancelEvent;
                CurrentPage = formDataModuleMenu.PageNo == 0 ? 1 : formDataModuleMenu.PageNo;
                if (formDataModuleMenu.Info != null)
                {
                    labelInstructions.Visible = true;
                    labelInstructions.Text = formDataModuleMenu.Info;
                }
                else
                {
                    labelInstructions.Visible = false;
                }

                CreateMenu(formDataModuleMenu.ModuleMenu.MenuItems);
                labelModuleName.Text = formData.HeaderText;
                lblOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            }
            var button = MultiButtonControl.GetButton(ButtonBackName);
            if (button != null)
                button.Text = GlobalTexts.Back;
            button = MultiButtonControl.GetButton(ButtonNextName);
            if (button != null)
            {
                if (!string.IsNullOrEmpty(button.Text))
                button.Text = GlobalTexts.Next;
                if (button.Tag!=null&&!string.IsNullOrEmpty(button.Tag.ToString()))
                    button.Tag = GlobalTexts.Next;
            }
        }


        protected void CreateMenu(ModuleMenuItem[] menuItems)
        {
            _menuItems = (from moduleMenuItem in menuItems
                          where moduleMenuItem.Disabled == false && moduleMenuItem.Hidden == false
                          select moduleMenuItem).ToArray(); 
            CreatePageButtons();
            SetNextButton(CurrentPage < TotalPages);
        }

        private void CreatePageButtons()
        {
            try
            {
                var x = 0;
                var y = 0;
                Color backColor = Color.FromArgb(46, 48, 50);
                Color foreColor = Color.White;
                Size buttonSize = ScaleUtil.GetScaledSize(209, 100);
                int buttonSpace = ScaleUtil.GetScaledPosition(20);

                // Upto 6 buttons per page
                const int pageSize = 6;
                TotalPages = _menuItems.Length / pageSize;
                if (_menuItems.Length % pageSize > 0)
                    TotalPages++;

                // Show page number if more than one page
                labelPageNumber.Visible = TotalPages > 1;
                panelButtonContainer.Controls.Clear();
                
                int i = (CurrentPage - 1) * pageSize;
                for (int count = 0; i < _menuItems.Length && count < 6; count++, i++)
                {
                    ModuleMenuItem moduleMenuItem = _menuItems[i];
                    var button = new ImageButton
                    {
                        BackColor = backColor,
                        ForeColor = foreColor,
                        Size = buttonSize,
                        Location = new Point(x, y),
                        Tag = _menuItems[i]
                    };
                    button.Size = buttonSize;
                    button.Text = moduleMenuItem.Text;
                    button.Font = new Font("Arial", 8F, FontStyle.Regular);
                    button.Click += OnMenuButtonClicked;
                    panelButtonContainer.Controls.Add(button);
                    if (button.Text.Length > 19)
                        AdjustFont(button);
                    if (count % 2 == 0)
                    {
                        x = button.Size.Width + buttonSpace;
                    }
                    else
                    {
                        x = 0;
                        y += button.Size.Height + buttonSpace;
                    }
                }
                ShowPageNumber();
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FormModuleMenu.CreatePageButtons");
            }
        }

        void AdjustFont(ImageButton btn)
        {
            if (btn.Text.Contains("\r\n")) return;
   
            string newText = string.Empty;
            string[] words = btn.Text.Split(' ');
            string line = string.Empty; //= _words[0];
            // newText = line;
            foreach (string word in words)
            {
                if ((line.Length + word.Length) > 18)
                {
                    newText += Environment.NewLine + word;
                    line = word;
                }
                else
                {
                    line += " " + word;
                    newText += " " + word;
                }
            }
            newText.Replace('_', ' ');
            btn.Text = newText;
        }


        void SetNextButton(bool status)
        {
            MultiButtonControl.SetButtonVisibleState(ButtonNextName, status);
        }
        void ShowPageNumber()
        {
            labelPageNumber.Text = CurrentPage + "/" + TotalPages;
        }

        private void OnMenuButtonClicked(object sender, EventArgs e)
        {
            try
            {
                var menuItem = (ModuleMenuItem)((ImageButton)sender).Tag;
                ViewEvent.Invoke(menuItem.ActionIndex, CurrentPage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadingMenu.OnMenuButtonClicked");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                if (CurrentPage > 1)
                {
                    CurrentPage--;
                    CreatePageButtons();
                    if (CurrentPage == TotalPages - 1)//if next button is disabled enable it
                        SetNextButton(true);

                }
                else
                {
                    //  ViewEvent.Invoke(_cancelEvent, _pageNo);  
                    ViewEvent.Invoke(_cancelEvent, CurrentPage);
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormModuleMenu.ButtonBackClick");
            }
        }

        private void ButtonNextClick(object sender, EventArgs e)
        {
            try
            {
                if (CurrentPage < TotalPages)
                {
                    CurrentPage++;
                    CreatePageButtons();
                    if (CurrentPage == TotalPages)
                        SetNextButton(false);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormModuleMenu.ButtonNextClick");
            }

        }
    }

    public class FormDataModuleMenu : BaseFormData
    {
        public int PageNo { get; set; }
        public ModuleMenu ModuleMenu { get; set; }
        public int CancelEvent { get; set; }
        public string Info { get; set; }
    }
}
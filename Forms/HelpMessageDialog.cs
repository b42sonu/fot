﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;
using PreCom.Core;
using System.Xml;

namespace Com.Bring.PMP.PreComFW.Shared.Forms
{
    public partial class HelpMessageDialog : PreComDialog
    {
        private string DialogResult { get; set; }
        private bool _alreadyLoggedOut;
        private static string _fileName;

        private readonly AutoResetEvent _finishedEvent = new AutoResetEvent(false);

        private readonly EventHandler _previousHardwareEnterHandler;

        public static string Show(string moduleName, string processName, string formName, string helpText)
        {

            _fileName = moduleName + "_" + processName + "_" + formName + "_" + ModelUser.UserProfile.LanguageCode + ".xml"; 
            string result = null;
            try
            {
                HelpMessageDialog dialog = null;
                if (BaseModule.MainForm.InvokeRequired)
                    BaseModule.MainForm.Invoke((Action)(() => dialog = Launch(helpText)));
                else
                    dialog = Launch(helpText);

                result = dialog.DialogResult;

            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "GuiCommon.ShowModalDialog");
            }

            return result;
        }

        private static HelpMessageDialog Launch(string helpText)
        {

            using (BusyUtil.Suspend())
            {
                var dialog = new HelpMessageDialog(helpText);

                dialog.Show();
                dialog.RunPump();
                return dialog;
            }
        }

        private HelpMessageDialog(string text)
        {
            try
            {
                InitializeComponent();
                var image = GuiCommon.Instance.BackgroundImage;
                if (image != null)
                {
                    touchPanel.BackgroundImage = image;
                }

                Type = DialogType.Dialog;

                DialogResult = null;
                _previousHardwareEnterHandler = HardwareKeyboard.OnEnterKeyPressed;

                multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Cancel, ButtonCancelClick, "cancelButton"));
                multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Save, ButtonSaveClick, "saveButton"));

                multiButtonControl.GenerateButtons();
                InitMessage(text);
                if (HelpCommands.IsEditHelpAllowedByUser)
                {
                    messageControl.Visible = false;
                    txtNewMessage.Visible = true;
                    chkChangeFormat.Checked = false;
                }
                else
                {
                    messageControl.Visible = true;
                    messageControl.ShowMessage(new MessageHolder(text, MessageState.Information));
                    messageControl.Size = new System.Drawing.Size(438, 140);
                    txtNewMessage.Visible = false;
                    chkChangeFormat.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.ShowNewModalDialog");
            }
        }

       

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                ClosePopUp(sender);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonBackClick");
            }
        }

        private void ButtonSaveClick(object sender, EventArgs e)
        {
            try
            {
                //Save to file
                HelpCommands.IsEditHelpAllowedByUser = !chkChangeFormat.Checked;
                SaveFile(_fileName, txtNewMessage.Text);
                ClosePopUp(sender);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonBackClick");
            }
        }

        private void FormatChanged(object sender, EventArgs e)
        {

        }

        private void RunPump()
        {
            // Stop processing of new operation lists while showing modal dialog
            bool isOnSensitiveScreenOrModuleOrig = ModelMain.IsOnSensitiveScreenOrModule;

            try
            {
                FotScanner.Instance.Suspend();
#pragma warning disable 618
                PreCom.Application.Instance.Communication.LoginStatus += LoginStatusEventReceived;
#pragma warning restore 618

                ModelMain.IsOnSensitiveScreenOrModule = true;

                while (_finishedEvent.WaitOne(10, false) == false)
                {
                    Application.DoEvents();
                    Thread.Sleep(0);
                }
            }

            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "GuiCommon.ShowModalDialog");
            }
            finally
            {
#pragma warning disable 618
                PreCom.Application.Instance.Communication.LoginStatus -= LoginStatusEventReceived;
#pragma warning restore 618
                FotScanner.Instance.Resume();
                if (isOnSensitiveScreenOrModuleOrig == false)
                    ModelMain.ResetIsOnSensistiveScreenWithoutSignal();
            }
        }

        private void LoginStatusEventReceived(LoginEventArgs e)
        {
            try
            {
                if (e.Status == LoginStatusInfo.LogoutOK)
                {
                    if (_alreadyLoggedOut == false)
                    {
                        _alreadyLoggedOut = true;
                        ClosePopUp(null);
                        Logger.LogEvent(Severity.Debug, "LoginStatusEventReceived logoutOk received in MessageDialog");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ModuleSettings.LoginStatusEventReceived " + ex.Message);
            }

        }

        private void SaveFile(String fileName, string helpText)
        {
            string path = FileUtil.GetApplicationPath();
            string filePath = path + "\\Help\\" + fileName;

            if (!Directory.Exists(path + "\\Help"))
                Directory.CreateDirectory(path + "\\Help");
            else if (File.Exists(filePath))
            {
                //If file already exists, then delete the same.
                File.Delete(filePath);
            }


            var help = new Help { Language = ModelUser.UserProfile.LanguageCode, Text = helpText };

            using (XmlWriter writer = XmlWriter.Create(filePath))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Data");
                writer.WriteElementString("Language", help.Language);
                writer.WriteElementString("HelpText", help.Text);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            
        }

        private void ClosePopUp(object sender)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<object>(ClosePopUp), sender);
                return;
            }

            try
            {
                var button = sender as TabButton;
                // If 0, it was triggered by hardware enter on default button
                DialogResult = button == null ? multiButtonControl.ListButtons[0].Text : button.Text;
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.ClosePopUp");
            }
            finally
            {
                Hide();
                HardwareKeyboard.SubscribeEnterKeyPressed(_previousHardwareEnterHandler);
                _finishedEvent.Set();
            }
        }

        private void InitMessage(string messageText)
        {
            try
            {
                labelTitle.Text = GlobalTexts.Help;
                lblChkText.Text = GlobalTexts.ShowReadOnlyMode;
                txtNewMessage.Text = messageText;
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "MessageDialog.InitMessage");
            }
        }
    }

    public class Help
    {
        internal string Language;
        internal string Text;
    }
}
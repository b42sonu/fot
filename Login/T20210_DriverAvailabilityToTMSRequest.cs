﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login
{
    public class Header
    {
        public string MessageIdField { get; set; }
        public string MessageTypeField { get; set; }
        public string MessageModeField { get; set; }
        public string ContextReferenceField { get; set; }
        public string ActionField { get; set; }
        public string VersionField { get; set; }
        public System.DateTime FirstProcessedTimestampField { get; set; }
        public bool FirstProcessedTimestampFieldSpecified { get; set; }
        public System.DateTime ProcessedTimestampField { get; set; }
        public bool ProcessedTimestampFieldSpecified { get; set; }
        public System.DateTime SourceSystemTimestampField { get; set; }
        public System.DateTime TargetSystemTimestampField { get; set; }
        public bool TargetSystemTimestampFieldSpecified { get; set; }
        public string SecurityTokenField { get; set; }
        public string SourceCompanyField { get; set; }
        public string SourceSystemField { get; set; }
        public string SourceSystemUserField { get; set; }
        public string SourceSystemRefField { get; set; }
    }

    public class T20210_DriverAvailabilityToTMSRequest : EntityBase
    {
        public Header Header { get; set; }
        public string UserLogonId { get; set; }
        public string UserRoleId { get; set; }
        public string OrgUnitId { get; set; }
        public string CountryCode { get; set; }
        public string CompanyCode { get; set; }
        public string TmsAffiliationId { get; set; }
        public string LoadCarrierId { get; set; }
        public string PowerLoadCarrierId { get; set; }
        public int AgentNumber { get; set; }
        public string ExpeditionId { get; set; }
        public string UserPhoneNumber { get; set; }
        public string HeartbeatIntervalMinutes { get; set; }
        public bool Available { get; set; }
        public string CaptureEquipmentId { get; set; }
        public bool Override { get; set; }
        public string CorrelationId { get; set; }
        public string SessionId { get; set; }
        public string Version { get; set; }
        public string ProcessName { get; set; }
        public string SubProcessName { get; set; }
        public string SubProcessVersion { get; set; }
    }
}

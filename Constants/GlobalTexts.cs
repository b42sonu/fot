﻿#if !PreComLT
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.Constants
{
    public class GlobalTexts
    {
        // Text for buttons

        public static string Signature { get { return Language.Translate("Signature"); } }
        public static string NewDamage { get { return Language.Translate("New Damage"); } }
        public static string TakePhoto { get { return Language.Translate("Take picture"); } }
        public static string Items { get { return Language.Translate("Items"); } }
        public static string Deviation { get { return Language.Translate("Deviation"); } }
        public static string Delete { get { return Language.Translate("Delete"); } }
        public static string DifferentStop { get { return Language.Translate("Different Stop"); } }
        public static string Save { get { return Language.Translate("Save"); } }
        public static string Camera { get { return Language.Translate("Camera"); } }
        public static string Reset { get { return Language.Translate("Reset"); } }
        public static string GpsSettingsHeader { get { return Language.Translate("GpsSettingsHeader"); } }
        public static string GpsSettingsButton { get { return Language.Translate("GpsSettingsButton"); } }
        public static string TmiSettings { get { return Language.Translate("TMI Settings"); } }
        public static string ChangeProfile { get { return Language.Translate("Change Profile"); } }
        public static string ChangeLogLevel { get { return Language.Translate("Change LogLevel"); } }
        public static string Attempt { get { return Language.Translate("Attempt"); } }
        public static string Confirm { get { return Language.Translate("Confirm"); } }
        public static string ConfirmChanges { get { return Language.Translate("Confirm Changes "); } }
        public static string ConfirmNewPassword { get { return Language.Translate("Confirm new password"); } }
        public static string UnplannedPick { get { return Language.Translate("Unpl. pick"); } }
        public static string AttemptedDel { get { return Language.Translate("Attempted Del"); } }
        public static string Departure { get { return Language.Translate("Departure"); } }
        public static string DepartureNotAllowed { get { return Language.Translate("You have pickups on this stop that are started but not completed. Finish them before departing."); } }
        public static string Refresh { get { return Language.Translate("Refresh"); } }
        public static string Details { get { return Language.Translate("Details"); } }
        public static string Back { get { return Language.Translate("Back"); } }
        public static string Scan { get { return Language.Translate("Scan"); } }
        public static string Yes { get { return Language.Translate("Yes"); } }
        public static string No { get { return Language.Translate("No"); } }
        public static string Ok { get { return Language.Translate("Ok"); } }
        public static string Stop { get { return Language.Translate("Stop"); } }
        public static string Locations { get { return Language.Translate("Locations"); } }
        public static string Break { get { return Language.Translate("Break"); } }
        public static string Trace { get { return Language.Translate("Trace"); } }
        public static string On { get { return Language.Translate("On"); } }
        public static string Off { get { return Language.Translate("Off"); } }
        public static string Cancel { get { return Language.Translate("Cancel"); } }
        public static string ChgCarrier { get { return Language.Translate("Chg.Carrier"); } }
        public static string UnloadAll { get { return Language.Translate("Unl. Whole"); } }
        public static string Unload { get { return Language.Translate("Unload"); } }
        public static string Load { get { return Language.Translate("Load"); } }
        public static string Reconcillation { get { return Language.Translate("Reconcilliation"); } }
        public static string OpenCarrier { get { return Language.Translate("Open Carrier"); } }
        public static string CloseCarrier { get { return Language.Translate("Close Carrier"); } }
        public static string Continue { get { return Language.Translate("Continue"); } }
        public static string TryAgain { get { return Language.Translate("Please try again"); } }
        public static string Navigate { get { return Language.Translate("Navigate"); } }
        public static string OpenCarrierAborted { get { return Language.Translate("Opening of carrier was cancelled"); } }
        public static string CloseCarrierAborted { get { return Language.Translate("Closing of carrier was cancelled"); } }



        public static string Erase { get { return Language.Translate("Erase"); } }
        public static string EraseSignature { get { return Language.Translate("Do you want to erase the signature field?"); } }
        public static string Info { get { return Language.Translate("Info"); } }
        public static string Reject { get { return Language.Translate("Reject"); } }
        public static string Accept { get { return Language.Translate("Accept"); } }
        public static string AcceptAll { get { return Language.Translate("Accept all"); } }
        public static string Login { get { return Language.Translate("Login"); } }
        public static string Logout { get { return Language.Translate("Logout"); } }
        public static string Planning { get { return Language.Translate("Planning"); } }
        public static string StartPause { get { return Language.Translate("Activate pause"); } }
        public static string StopPause { get { return Language.Translate("Stop pause"); } }
        public static string Minutes30 { get { return Language.Translate("30 min"); } }
        public static string Hour1 { get { return Language.Translate("1 Hour"); } }
        public static string Hour2 { get { return Language.Translate("2 Hour"); } }
        public static string Hour4 { get { return Language.Translate("4 Hour"); } }
        public static string Hour8 { get { return Language.Translate("8 Hour"); } }
        public static string SortStops { get { return Language.Translate("Sort stops"); } }
        public static string Pickup { get { return Language.Translate("Pickup"); } }
        public static string Up { get { return Language.Translate("Up"); } }
        public static string Down { get { return Language.Translate("Down"); } }
        public static string Predef { get { return Language.Translate("Predef"); } }
        public static string AsLoaded { get { return Language.Translate("As Loaded"); } }
        public static string ManualSort { get { return Language.Translate("Manual Sort"); } }
        public static string Zone { get { return Language.Translate("Zone"); } }
        public static string Sorting { get { return Language.Translate("Sorting"); } }
        public static string ManualSorting { get { return Language.Translate("Manual Sorting"); } }
        public static string NextZone { get { return Language.Translate("Next Zone"); } }
        public static string PrevZone { get { return Language.Translate("Prev Zone"); } }
        public static string ChangeZone { get { return Language.Translate("Change Zone"); } }
        public static string Sort { get { return Language.Translate("Sort"); } }
        public static string InternalScanner { get { return Language.Translate("Internal scanner"); } }
        public static string ExternalScanner { get { return Language.Translate("External scanner"); } }


        // Text for labels
        public static string OfflineCanNotSendRequest { get { return Language.Translate("Offline : Can not send request."); } }
        public static string Delivery { get { return Language.Translate("Delivery"); } }
        public static string ConfirmSignature { get { return Language.Translate("Please confirm with or without signature"); } }
        public static string ConfirmDamageForItem { get { return Language.Translate("Confirm damage for item type and item number"); } }
        public static string Comment { get { return Language.Translate("Comment"); } }
        public static string PackageTypeDamaged { get { return Language.Translate("Package type was totally damaged"); } }
        public static string FellOutOfCar { get { return Language.Translate("Fell out of car"); } }
        public static string Destruert { get { return Language.Translate("Destruert"); } }
        public static string TakePictureConfirmation { get { return Language.Translate("Do you want to take a picture?"); } }
        public static string DamageType { get { return Language.Translate("Damage type"); } }
        public static string APackageDamage { get { return Language.Translate("A Package Damage"); } }
        public static string NrOfConsignments { get { return Language.Translate("Consigment count"); } }
        public static string StopId { get { return Language.Translate("Stop Id"); } }
        public static string DeliveryToPostOffice { get { return Language.Translate("Delivery to Post Office"); } }
        public static string UnplannedPickup { get { return Language.Translate("Unplanned Pickup"); } }
        public static string WithoutStop { get { return Language.Translate("Without stop"); } }
        public static string Operations { get { return Language.Translate("Operations"); } }
        public static string Stops { get { return Language.Translate("Stops"); } }
        public static string InformationConsignmentScanned { get { return Language.Translate("You have scanned a consignment"); } }
        public static string ConsignmentId { get { return Language.Translate("Consignment id"); } }
        public static string ConfirmConsignmentCount { get { return Language.Translate("Confirm consignment item count"); } }
        public static string DamageRegistration { get { return Language.Translate("Damage registration"); } }
        public static string DamageFor { get { return Language.Translate("Damage for"); } }

        public static string LabelThumbnail { get { return Language.Translate("Thumbnail for already captured photos"); } }
        public static string ChangeTmsAvailability { get { return Language.Translate("Change TMS Availability"); } }
        public static string PlanTmsAvailability { get { return Language.Translate("Plan TMS Availability"); } }
        public static string ChangeConfirmInformation { get { return Language.Translate("Change or Confirm Information"); } }
        public static string ScanOrEnterBarcode { get { return Language.Translate("Scan or enter barcode"); } }
        public static string AttemptedPickUp { get { return Language.Translate("Attempted Pickup"); } }
        public static string DepartFromStop { get { return Language.Translate("Are you sure you would like to depart from this stop?"); } }
        public static string OperationDetails { get { return Language.Translate("Operation Details"); } }
        public static string OperationListDetails { get { return Language.Translate("Operation List Details"); } }
        public static string StartOperationList { get { return Language.Translate("Activate Operation list from main menu to begin your work"); } }
        public static string OperationListPresent { get { return Language.Translate("Operation list present"); } }
        public static string Consignee { get { return Language.Translate("Consignee"); } }
        public static string Consignments { get { return Language.Translate("Consignments"); } }
        public static string ConsigneeAddr { get { return Language.Translate("Consignee address"); } }
        public static string Completed { get { return Language.Translate("Completed"); } }
        public static string NotCompleted { get { return Language.Translate("Not Completed"); } }
        public static string All { get { return Language.Translate("All"); } }
        public static string ConsignorAddr { get { return Language.Translate("Consignor address"); } }
        public static string OrderNo { get { return Language.Translate("Order No"); } }
        public static string Cause { get { return Language.Translate("Cause"); } }
        public static string Measure { get { return Language.Translate("Measure"); } }
        public static string GenerateBookingNumber { get { return Language.Translate("Generate booking number"); } }
        public static string ClickToGenrateBookingNumber { get { return Language.Translate("Or click  here to generate"); } }
        public static string BookingNumber { get { return Language.Translate("Booking Number"); } }
        public static string CustomerNumber { get { return Language.Translate("Customer Number"); } }
        public static string UnplannedPickUp { get { return Language.Translate("Unplanned pick up"); } }
        public static string ChangePassword { get { return Language.Translate("Change Password"); } }
        public static string CurrentPassword { get { return Language.Translate("Current Password"); } }
        public static string NewPassword { get { return Language.Translate("New Password"); } }
        public static string ConfirmPassword { get { return Language.Translate("Confirm Password"); } }
        public static string OptionalComment { get { return Language.Translate("Comment (optional)"); } }
        public static string OptionalComments { get { return Language.Translate("Comments (optional)"); } }
        public static string AttemptedDelivery { get { return Language.Translate("Attempted delivery"); } }
        public static string ConsignmentItemNumber { get { return Language.Translate("Consignment/item number"); } }
        public static string TripId { get { return Language.Translate("Trip Id"); } }
        public static string ConfirmInfo { get { return Language.Translate("Are new carrier and select operation type you wish to start"); } }
        public static string NewCarrier { get { return Language.Translate("New Carrier"); } }
        public static string SelectAddCarrier { get { return Language.Translate("Select a carrier from the list and click Continue"); } }
        public static string WorkList { get { return Language.Translate("Work List"); } }
        public static string SignatureInfo { get { return Language.Translate("Your signature will be displayed in Posten/Brings tracking solution online"); } }
        public static string CustomerName { get { return Language.Translate("Customer name"); } }
        public static string VerifyChangePowerUnit { get { return Language.Translate("Verify or change Power Unit ID"); } }
        public static string LoadedByCustomer { get { return Language.Translate("Load by customer"); } }
        public static string PowerUnitId { get { return Language.Translate("Power Unit ID"); } }
        public static string Settings { get { return Language.Translate("Settings"); } }
        public static string Date { get { return Language.Translate("Date"); } }
        public static string VerifyChangeRouteId { get { return Language.Translate("Verify or change Route ID"); } }
        public static string SortZone { get { return Language.Translate("Sort Zone"); } }
        public static string ZoneSortingView { get { return Language.Translate("Zone Sorting View"); } }
        public static string DeliveryMovedToZone { get { return Language.Translate("Delivery moved into Zone"); } }
        public static string AssignZone { get { return Language.Translate("Click on Zone to assign selected row"); } }
        public static string FinishedZoneGrouping { get { return Language.Translate("Finished grouping into zones"); } }
        public static string EnterConsigneeName { get { return Language.Translate("Enter Consignee Name"); } }
        public static string ModuleName { get { return Language.Translate("Module Name"); } }
        public static string ViewName { get { return Language.Translate("View Name"); } }
        public static string ActiveProcess { get { return Language.Translate("Active Process"); } }
        public static string Available { get { return Language.Translate("Available"); } }
        public static string Unavailable { get { return Language.Translate("Unavailable"); } }
        public static string Paused { get { return Language.Translate("Paused"); } }
        public static string ScheduledPause { get { return Language.Translate("Scheduled pause"); } }


        // Short statements and headers
        public static string ConfirmationOperationList { get { return Language.Translate("Operation List Confirmation"); } }
        public static string Attention { get { return Language.Translate("Attention"); } }
        public static string Error { get { return Language.Translate("Error"); } }
        public static string Goods { get { return Language.Translate("Goods"); } }
        public static string Offline { get { return Language.Translate("Offline"); } }
        public static string Summary { get { return Language.Translate("Summary"); } }
        public static string ScanConsignment { get { return Language.Translate("Scan required consignment"); } }
        public static string Temperature { get { return Language.Translate("Temperature"); } }
        public static string ConsignmentItem { get { return Language.Translate("Consignment item"); } }
        public static string ConsignmentItemShort { get { return Language.Translate("Cons. item"); } }
        public static string Consignment { get { return Language.Translate("Consignment"); } }
        public static string OrgUnitId { get { return Language.Translate("OrgUnit Id"); } }
        public static string PowerUnit { get { return Language.Translate("Power unit"); } }
        public static string LoadCarrier { get { return Language.Translate("Load carrier"); } }
        public static string PostalCode { get { return Language.Translate("Postal code"); } }
        public static string TmsAffiliation { get { return Language.Translate("TMS affiliation"); } }
        public static string RouteId { get { return Language.Translate("Route id"); } }
        public static string CompanyCode { get { return Language.Translate("Company code"); } }
        public static string CountryCode { get { return Language.Translate("Country code"); } }
        public static string TelephoneNumber { get { return Language.Translate("Phone no"); } }
        public static string Telephone { get { return Language.Translate("Telephone"); } }
        public static string UserProfile { get { return Language.Translate("User Profile"); } }
        public static string ConfirmUserProfile { get { return Language.Translate("Confirm User Profile"); } }
        public static string OperationFormHeaderText { get { return Language.Translate("Operation list"); } }
        public static string UnfinishedOperationsTitle { get { return Language.Translate("Unfinished stops"); } }
        public static string UnfinishedDeliveriesTitle { get { return Language.Translate("Unfinished deliveries"); } }
        public static string ZoneItemSortHeading1 { get { return Language.Translate("Sort items inside Zone"); } }
        public static string ZoneItemSortHeading2 { get { return Language.Translate("Select a row move it up/down"); } }
        public static string ChangeZoneHeading { get { return Language.Translate("Select zone to move item to, or press cancel"); } }
        public static string Help { get { return Language.Translate("Help"); } }
        public static string BatterySwap { get { return Language.Translate("Battery Swap"); } }
        public static string ContineWithBatterySwap { get { return Language.Translate("Do you want to continue with battery swap?"); } }
        public static string PreparingForBatterySwap { get { return Language.Translate("Prepairing to to perform battery swap"); } }
        public static string PressOkAfterBatterySwap { get { return Language.Translate("Press ok when battery has been changed"); } }

        // Information
        public static string ScanConsignmentDeleted { get { return Language.Translate("Selected scanned Item(s) has been deleted from cache"); } }
        public static string AreYouSureYouWantToDelete { get { return Language.Translate("Are you sure you want to delete"); } }
        public static string ConsignmentAddedToItem { get { return Language.Translate("Consignment {0} added to consignment item {1}"); } }
        public static string ConsignmentItemCount { get { return Language.Translate("Consignment item count"); } }
        public static string ProcessForDeleteItemFailed { get { return Language.Translate("Process for Deletion of Item {0} failed"); } }
        public static string ItemDeleted { get { return Language.Translate("Consignment {0} deleted"); } }
        public static string ConsignmentItemDeleted { get { return Language.Translate("Consignment item {0} deleted"); } }
        public static string LoadingProfile { get { return Language.Translate("Please wait, loading profile from server"); } }
        public static string LoggedInOffline { get { return Language.Translate("You were logged in offline with local profile"); } }
        public static string ProfileRetrived { get { return Language.Translate("Profile retrieved successfully from server"); } }
        public static string RegisteredConsignment { get { return Language.Translate("Registered consignment"); } }
        public static string ConsignmentRegistered { get { return Language.Translate("Consignment {0} registered"); } }
        public static string OfflineConsignmentRegistered { get { return Language.Translate("Offline: Consignment {0} registered"); } }
        public static string ConsignmentItemRegistered { get { return Language.Translate("Consignment item {0} registered"); } }
        public static string OfflineConsignmentItemRegistered { get { return Language.Translate("Offline: Consignment item {0} registered"); } }
        public static string AcceptRejectOrder { get { return Language.Translate("{0} orders accepted and {1} orders rejected"); } }
        public static string NewOperationsReceived { get { return Language.Translate("New operations received"); } }
        public static string UpdatedOperationsReceived { get { return Language.Translate("Updated operations received"); } }
        public static string OperationsCancelled { get { return Language.Translate("Operations in your operation list were cancelled by operator"); } }
        public static string OperationWithEmptyOrderNumber { get { return Language.Translate("Operations with empty PickUp order number or empty stop id found"); } }
        public static string PhotoAdded { get { return Language.Translate("Photo added"); } }
        public static string ArrivedAtWarehouse { get { return Language.Translate("Arrived at warehouse"); } }
        public static string InternalScannerActivated { get { return Language.Translate("Internal scanner activated"); } }
        public static string ExternalScannerActivated { get { return Language.Translate("External scanner activated"); } }
        public static string DeliveryDateRegistered { get { return Language.Translate("{0} has a planned delivery date registered"); } }
        public static string PlannedDeliveryDateNotForToday { get { return Language.Translate("Planned delivery date is not set for today, but for {0}"); } }
        public static string GoodsShouldNotbeUnloaded { get { return Language.Translate("{0} should not be unloaded at this stop"); } }
        public static string GoodsShouldNotbeLoaded { get { return Language.Translate("{0} should not be loaded at this stop"); } }


        //Verifications
        public static string ConfirmOperationListMessage { get { return Language.Translate("Backup operation list found on device. Do you want to load it?"); } }
        public static string VerifyPowerUnitLoadListMessage { get { return Language.Translate("Please provide your current Power Unit ID"); } }
        public static string VerifyRouteIdLoadListMessage { get { return Language.Translate("Please provide your current Route ID"); } }

        public static string VerifyScannedUnplannedConsignment { get { return Language.Translate("Please confirm you want to handle scanned, unplanned consignment"); } }
        public static string UnloadingVerification { get { return Language.Translate("Please verify if unloading for this stop is completed"); } }
        public static string PendingTasksOperationListMessage { get { return Language.Translate("You have pending operations in operation list. Do you want to continue?"); } }
        public static string PendingTasksLoadListMessage { get { return Language.Translate("You have pending tasks in Load list. Do you want to continue?"); } }


        // Confirmation
        public static string PendingTasksMessage { get { return Language.Translate("User has some pending tasks. Do you really want to continue"); } }
        public static string DriverOverridden { get { return Language.Translate("The Asset already has a driver assigned. Do you wish to take over the asset?"); } }
        public static string UnloadingCompleted { get { return Language.Translate("Unloading completed?"); } }
        public static string UnfinishedOperationsDescription { get { return Language.Translate("You have unfinished operations in the operation list. Do you want to log out?"); } }
        public static string UnfinishedMessagesInQueue { get { return Language.Translate("There are unsent messages in the queue. Do you want to log out anyway?"); } }
        public static string UnfinishedDeliveriesDescription { get { return Language.Translate("You have unfinished deliveries in the delivery list"); } }
        public static string AreYouSureYouWantToLogOut { get { return Language.Translate("Are you sure you want to log out?"); } }

        // Abortions
        public static string AttemptedDeliveryAborted { get { return Language.Translate("Attempted delivery aborted"); } }
        public static string AttemptDeliveryAborted { get { return Language.Translate("Attempt. delivery aborted"); } }
        public static string ScanningConsignmentAborted { get { return Language.Translate("Scanning of consignment is aborted"); } }
        public static string HandleScannedUnplannedConsignmentAborted { get { return Language.Translate("Handling of scanned, unplanned consignment aborted"); } }
        public static string CorrectionMenuAborted { get { return Language.Translate("Correction menu aborted"); } }
        public static string ConsignmentItemConfirmationAborted { get { return Language.Translate("Confirmation of consignment item count aborted"); } }
        public static string FlowAborted { get { return Language.Translate("Flow aborted"); } }
        public static string ScanningAborted { get { return Language.Translate("Scanning aborted"); } }
        public static string NotDeliveredAborted { get { return Language.Translate("Not Delivered Aborted"); } }
        public static string CarrierOpened { get { return Language.Translate("Carrier opened successfully"); } }
        public static string CarrierClosed { get { return Language.Translate("Carrier closed successfully"); } }

        // Flows
        public static string NotDelivered { get { return Language.Translate("Not Delivered"); } }
        public static string Unloading { get { return Language.Translate("Unloading"); } }
        public static string Loading { get { return Language.Translate("Loading"); } }
        public static string LoadingLineHaulTruck { get { return Language.Translate("Loading of line haul truck"); } }
        public static string LoadingDistributionTruck { get { return Language.Translate("Loading of distribution truck"); } }
        public static string LoadingCombinationTrip { get { return Language.Translate("Loading of combination trip"); } }
        public static string UnloadingPickupTruck { get { return Language.Translate("Unloading of pickup truck"); } }
        public static string UnloadingLineHaulTruck { get { return Language.Translate("Unloading of line haul truck"); } }
        public static string PickupFromCustomer { get { return Language.Translate("Pickup from Customer"); } }
        public static string BuildingOfLoadCarrierHeader { get { return Language.Translate("BuildingOfLoadCarrierHeader"); } }
        public static string BuildingOfLoadCarrierButton { get { return Language.Translate("BuildingOfLoadCarrierButton"); } }
        public static string RemainingGoodsAtHubHeader { get { return Language.Translate("RemainingGoodsAtHubHeader"); } }
        public static string RemainingGoodsAtHubButton { get { return Language.Translate("RemainingGoodsAtHubButton"); } }
        public static string PrintLoadList { get { return Language.Translate("Print or mail load list"); } }
        public static string ControlScan { get { return Language.Translate("Control Scan"); } }
        public static string OperationListControlScan { get { return Language.Translate("Operation list Control scan"); } }
        public static string IntoHoldingAtHubHeader { get { return Language.Translate("IntoHoldingAtHubHeader"); } }
        public static string IntoHoldingAtHubButton { get { return Language.Translate("IntoHoldingAtHubButton"); } }
        public static string OutOfHoldingAtHubHeader { get { return Language.Translate("OutOfHoldingAtHubHeader"); } }
        public static string OutOfHoldingAtHubButton { get { return Language.Translate("OutOfHoldingAtHubButton"); } }
        public static string RegisterDamage { get { return Language.Translate("Register Damage"); } }
        public static string CorrectWeightAndVolume { get { return Language.Translate("Correct Weight and Volume"); } }
        public static string EnterCorrectValuesInWeightAndVolume { get { return Language.Translate("Please enter numeric values in fields Weight, Length, Width and Height."); } }
        public static string ChangeProductGroup { get { return Language.Translate("Change Product Group"); } }
        public static string GeneralDeviations { get { return Language.Translate("General Deviations"); } }
        public static string ArrivalRegistration { get { return Language.Translate("Arrival Registration"); } }
        public static string LoadList { get { return Language.Translate("Load List"); } }
        public static string ExecuteLinking { get { return Language.Translate("Execute Linking"); } }
        public static string CorrectWeightVolume { get { return Language.Translate("Correct w/v"); } }
        public static string DeliveryToSubContractor { get { return Language.Translate("Delivered to sub-contractor/driver"); } }
        public static string RegisterTemperature { get { return Language.Translate("Register temperature"); } }
        public static string BulkRegistration { get { return Language.Translate("Bulk Registration"); } }
        public static string DeliveryToCustomer { get { return Language.Translate("Delivery to customer"); } }
        public static string DeliveryToTheCustomer { get { return Language.Translate("Delivery to the Customer"); } }
        public static string PickingFromWarehouse { get { return Language.Translate("Picking from warehouse"); } }
        public static string HandedInTerminal { get { return Language.Translate("Handed in Terminal"); } }
        public static string InTerminal { get { return Language.Translate("In Terminal"); } }
        public static string PreparingForDriver { get { return Language.Translate("Out of shelf - preparing for driver"); } }
        public static string SelectScannerButton { get { return Language.Translate("SelectScannerButton"); } }
        public static string SelectScannerHeader { get { return Language.Translate("SelectScannerHeader"); } }
        public static string KeyLockButton { get { return Language.Translate("KeyLockButton"); } }
        public static string KeyLockHeader { get { return Language.Translate("KeyLockHeader"); } }
        public static string KeyLockLegend { get { return Language.Translate("Automatic keylock"); } }

        // Error messages
        public static string IllegalBarcode { get { return Language.Translate("Illegal barcode. Invalid format or checksum"); } }
        public static string ScanningConsignmentNotAuthorized { get { return Language.Translate("User is not authorized for scanning consignments"); } }
        public static string IllegalConsignment { get { return Language.Translate("Illegal consignment {0}: Wrong type or invalid checksum. Please try again!"); } }
        public static string SavePhotoError { get { return Language.Translate("Problem occured when saving photo"); } }
        public static string CameraInitializeError { get { return Language.Translate("Problem occured when Initializing the Camera"); } }
        public static string InvalidLocation { get { return Language.Translate("Invalid Location Id"); } }
        public static string InvalidRole { get { return Language.Translate("Selected role is not valid"); } }
        public static string NotHavingValidProfile { get { return Language.Translate("Not connected and no local profile"); } }
        public static string PickupMultipleConsignmentsUnauthorized { get { return Language.Translate("Pick-up of multiple consignments not authorized"); } }
        public static string InvalidPowerUnitId { get { return Language.Translate("The Power unit/load carrier id is not valid.Please enter a new one"); } }
        public static string InvalidRouteId { get { return Language.Translate("The Route id is not valid.Please enter a new one"); } }

        public static string FailedSavingScanning { get { return Language.Translate("Failed to store scanned scanning"); } }
        public static string IllegalAddress { get { return Language.Translate("Illegal address detected in plan"); } }
        public static string LoadListError { get { return Language.Translate("Load list could not be retrieved"); } }
        public static string PasswordExpired { get { return Language.Translate("Your password is expired"); } }
        public static string ErrorWhileScanning { get { return Language.Translate("Unspecific error situation encountered while scanning barcode"); } }

        public static string MissingConsignmentId { get { return Language.Translate("Could not process consignment item {0} as consignment id is missing"); } }
        public static string NoScannerAvailable { get { return Language.Translate("No scanner object available"); } }
        public static string ScannerActivationFailed { get { return Language.Translate("Failed to activate scanner"); } }
        public static string ScannedItemStopped { get { return Language.Translate("Item {0} has been stopped. Stop code: {1}"); } }
        public static string ActorBlocked { get { return Language.Translate("Item {0} can not be processed as actor {1} is blocked"); } }
        public static string DeliveryOfAlcoholNotAllowedNow { get { return Language.Translate("Delivery of alcohol is not allowed at this time"); } }
        public static string ArrivedAtCustomerWareHouse { get { return Language.Translate("Arrived At Customer WareHouse"); } }
        public static string ValidationFailed { get { return Language.Translate("Validation failed"); } }
        public static string ScheduledPauseIllegalStart { get { return Language.Translate("Start of pause must be before end of pause"); } }
        public static string ScheduledPauseIllegalEnd { get { return Language.Translate("End of pause must be in future"); } }

        public static string IllegalLoadCarrierType { get { return Language.Translate("Can not build Load carriers of type '{0}'"); } }
        public static string NoAvailableProductCategories { get { return Language.Translate("No availabe Product Categories you can change to"); } }
        public static string LogoutFailure { get { return Language.Translate("Failed to log out"); } }

        public static string ConsigmentIdNotValidForLinking { get { return Language.Translate("Consignment id {0} in pdf is not same as consignment id {1} scanned for linking"); } }
        public static string MissingInformationInRequest { get { return Language.Translate("Missing information in request"); } }
        public static string VasParamtEntryMissing { get { return Language.Translate("Config-entry '{0}' is not configured for VAS param-setting"); } }
        public static string UnexpectedDatabaseError { get { return Language.Translate("Unexpected database error"); } }
        public static string InformationNotFound { get { return Language.Translate("Information not found"); } }

        // Driver availability
        public static string AssetCanNotBeOvertaken { get { return Language.Translate("Your unit is already logged in. Contact helpdesk to resolve this"); } }
        public static string DriverAvailability { get { return Language.Translate("Driver availability"); } }
        public static string LoginToTmsFailed { get { return Language.Translate("Failed to login to TMS:"); } }
        public static string LoginToTmsSucceded { get { return Language.Translate("Login to TMS successful"); } }
        public static string NoAssociatedPowerunit { get { return Language.Translate("You are no longer associated to the current Power Unit in the TMS. You will not get updated planning information for the Power Unit"); } }
        public static string UserLoggedOffFromTms { get { return Language.Translate("User logged off TMS"); } }


        //Warning messages
        public static string ConsignmentItemCantZero { get { return Language.Translate("Number of consignment items can not be 0"); } }
        public static string PhotoCancelled { get { return Language.Translate("Photo cancelled"); } }
        public static string NoPhotoAdded { get { return Language.Translate("No photo added"); } }
        public static string PasswordSoonExpired { get { return Language.Translate("Your password will expire in {0} days. Do you want to change now?"); } }
        public static string InactiveUserLoggedOut { get { return Language.Translate("User logged out due to inactivity"); } }
        public static string DoYouWantFotToDelete { get { return Language.Translate("Do you want FOT to remove the items from your list of deliveries?"); } }
        public static string LogDebugEventsWarning { get { return Language.Translate("Logging debug events may slow down the application"); } }
        public static string SyntheticConsignmentIdWarning { get { return Language.Translate("Mangler fakturagrunnlag"); } }


        // Prompts
        public static string EnterTemprature { get { return Language.Translate("Enter temperature"); } }
        public static string ValidateTemprature { get { return Language.Translate("Enter valid temperature"); } }
        public static string EnterBarcode { get { return Language.Translate("Enter barcode"); } }
        public static string SelectTmsAffilationValue { get { return Language.Translate("Please select TMS Affiliation Value"); } }
        public static string EnterCustomerNumber { get { return Language.Translate("Enter customer number"); } }
        public static string EnterBookingNumber { get { return Language.Translate("Enter booking number"); } }

        //New properties for Label 
        public static string PlannedOperation { get { return Language.Translate("Planned Operation"); } }
        public static string SelectPlannedOperation { get { return Language.Translate("Please select planned operation"); } }
        public static string PositionModule { get { return Language.Translate("Position module"); } }
        public static string ErrorPositionModule { get { return Language.Translate("Error while retreiving position module"); } }
        public static string Desktop { get { return Language.Translate("Desktop"); } }
        public static string Lan { get { return Language.Translate("LAN"); } }
        public static string Wlan { get { return Language.Translate("WLAN"); } }
        public static string Address { get { return Language.Translate("Address"); } }
        public static string ConnectionStatus { get { return Language.Translate("Connected to"); } }
        public static string TmiInformation { get { return Language.Translate("TMI Does not accept positions"); } }
        public static string TmiInformationMessage { get { return Language.Translate("The current TMI does not accept positioning, and therefore you cannot change the GPS status"); } }
        public static string SettingModule { get { return Language.Translate("Settings module"); } }
        public static string ErrorInSettingModule { get { return Language.Translate("Error while retreiving settings module"); } }
        public static string IsRegistered { get { return Language.Translate("is registered"); } }
        public static string LoadCarrierIsRegistered { get { return Language.Translate("Load carrier {0} is registered"); } }
        public static string OfflineLoadCarrierIsRegistered { get { return Language.Translate("Offline: Load carrier {0} is registered"); } }
        public static string ConsignmentItemIsRegistered { get { return Language.Translate("Consignment item {0} is registered"); } }
        public static string PendingOperations { get { return Language.Translate("There are operations present in your Operation list"); } }
        public static string GoodsAvailabityError { get { return Language.Translate("No loaded goods available due to timeout/an error"); } }
        public static string GoodsAvailabityErrorPowerunit { get { return Language.Translate("Currently no loaded goods are available for your power unit"); } }

        public static string CauseNotSet { get { return Language.Translate("Cause is not set"); } }
        public static string ItemAlreadyDamage { get { return Language.Translate("Item is already registered as damage"); } }
        public static string Placement { get { return Language.Translate("Placement"); } }
        public static string Weight { get { return Language.Translate("Weight"); } }
        public static string Volume { get { return Language.Translate("Volume"); } }
        public static string OperationList { get { return Language.Translate("OperationList"); } }
        public static string Warning { get { return Language.Translate("Warning"); } }
        public static string PasswordCanNotChange { get { return Language.Translate("Password cannot be changed more than once per day"); } }
        public static string PasswordChange { get { return Language.Translate("Password change"); } }
        public static string PleaseEnter { get { return Language.Translate("Please enter"); } }
        public static string PasswordDoesNotMatch { get { return Language.Translate("New password and confirm password are not equal"); } }
        public static string PasswordDoesNotMatchPolicy { get { return Language.Translate("New password is mismatch with the password policy"); } }
        public static string SelectAtLeastOneItem { get { return Language.Translate("Please select at least one item from list"); } }
        public static string NoItemInList { get { return Language.Translate("No item in list for selection"); } }
        public static string PhotoLimitOver { get { return Language.Translate("Your photo capture limit is over"); } }
        public static string EnterItemCount { get { return Language.Translate("Enter item count"); } }
        public static string ReconcilliationCancelled { get { return Language.Translate("Reconcilliation canceled"); } }
        public static string Consignor { get { return Language.Translate("Consignor"); } }
        public static string DamageCodeNotSet { get { return Language.Translate("Damage code is not set"); } }
        public static string Type { get { return Language.Translate("Type"); } }
        public static string ConfirmDamageFor { get { return Language.Translate("Confirm damage for"); } }
        public static string RegisterTempratureCancelled { get { return Language.Translate("Register temperature cancelled"); } }
        public static string WrongCarrierFormat { get { return Language.Translate("Wrong carrier format. Please enter a valid carrier"); } }
        public static string AddCarrierAndSelectOperation { get { return Language.Translate("Add new carrier and select operation type you wish to start"); } }
        public static string SignatureCancelled { get { return Language.Translate("Signature cancelled"); } }
        public static string ReconcilliationAborted { get { return Language.Translate("Reconcilliation aborted"); } }
        public static string InvalidInput { get { return Language.Translate("Invalid Input"); } }
        public static string InvalidInputMessage { get { return Language.Translate("Please enter valid input"); } }
        public static string ReconcilliationListHeading { get { return Language.Translate("S/P/EDI"); } }
        public static string ConsignmentItems { get { return Language.Translate("Consignment Items"); } }
        public static string ListOfItems { get { return Language.Translate("List of items"); } }
        public static string Status { get { return Language.Translate("Status"); } }
        public static string NoConsignmentCodesSynced { get { return Language.Translate("No consignmentEntity codes synced"); } }
        public static string NoCauseMeasureSynced { get { return Language.Translate("No cause/measure synced"); } }
        public static string DamageInformationNotSynced { get { return Language.Translate("Damage information not synced"); } }
        public static string DriverInstruction { get { return Language.Translate("Driver instruction"); } }
        public static string NumberOfItems { get { return Language.Translate("Number of items"); } }
        public static string LastProductionEvent { get { return Language.Translate("Last production event"); } }
        public static string EarlierEventsFor { get { return Language.Translate("Earlier events for"); } }
        public static string Name { get { return Language.Translate("Name"); } }
        public static string Damage { get { return Language.Translate("Damage"); } }
        public static string RegisterDamageCancelled { get { return Language.Translate("Register damage cancelled"); } }
        public static string DamageRegisteredForItem { get { return Language.Translate("Damage registered for item {0}"); } }
        public static string ZipCode { get { return Language.Translate("Zip Code"); } }

        public static string PackageType { get { return Language.Translate("Package-type"); } }

        public static string TeleNo { get { return Language.Translate("Tele. No"); } }

        public static string UnplannedConsignmentNotVerified { get { return Language.Translate("Unplanned consignment not verified"); } }

        public static string SwitchingGprsToLan { get { return Language.Translate("Switching GPRS to LAN"); } }
        public static string SwitchingWlanToLan { get { return Language.Translate("Switching WLAN to LAN"); } }
        public static string SwitchingGprsToDesktop { get { return Language.Translate("Switching GPRS to Desktop"); } }
        public static string SwitchingLanToDesktop { get { return Language.Translate("Switching LAN to Desktop"); } }
        public static string SwitchingWlanToDesktop { get { return Language.Translate("Switching WLAN to Desktop"); } }
        public static string SwitchingGprsToWlan { get { return Language.Translate("Switching GPRS to WLAN"); } }
        public static string SwitchingLanToWlan { get { return Language.Translate("Switching LAN to WLAN"); } }
        public static string SwitchingDesktopToWlan { get { return Language.Translate("Switching Desktop to WLAN"); } }
        public static string SwitchingLanToGprs { get { return Language.Translate("Switching LAN to GPRS"); } }
        public static string SwitchingDeskopToGprs { get { return Language.Translate("Switching Deskop to GPRS"); } }
        public static string SwitchingWlanTo3G { get { return Language.Translate("Switching WLAN to 3G"); } }
        public static string SwitchingLanTo3G { get { return Language.Translate("Switching LAN to 3G"); } }
        public static string SwitchingWlanToGprs { get { return Language.Translate("Switching WLAN to GPRS"); } }
        public static string SwitchingDesktopTo3G { get { return Language.Translate("Switching Desktop to 3G"); } }
        public static string BookingRef { get { return Language.Translate("Booking ref"); } }

        public static string ConsignmentOrItem { get { return Language.Translate("Consignment(item)"); } }
        public static string ProductCategory { get { return Language.Translate("Product Category"); } }
        public static string TimeFrame { get { return Language.Translate("TimeFrame"); } }
        public static string EnterLoadCarrier { get { return Language.Translate("Enter Load Carrier"); } }
        public static string EnterTripCarrierItemConsignment { get { return Language.Translate("Enter trip, carrier, item or consignment"); } }
        public static string FullName { get { return Language.Translate("Full Name"); } }
        public static string InvalidPowerUnit { get { return Language.Translate("Invalid Power Unit"); } }

        public static string RequestGoodsListResponse { get { return Language.Translate("Goods List Response"); } }
        public static string RequestGoodsListFailed { get { return Language.Translate("Retrieve Goods List failed. Error-code : {0}, {1}"); } }
        public static string GoodsNotReturnedFromLm { get { return Language.Translate("No Stops/Trips available from LM"); } }
        public static string SelectCauseAndMeasure { get { return Language.Translate("Select Cause and Measure"); } }
        public static string OperationId { get { return Language.Translate("OperationId"); } }
        public static string LogDebugInformation { get { return Language.Translate("Log debug information"); } }
        public static string Vas { get { return Language.Translate("Vas"); } }

        public static string ConsignmentItemUnknownInLm { get { return Language.Translate("Consignment item number unknown in LM, scan consignment number"); } }
        public static string LoadCarrierUnknownInLm { get { return Language.Translate("Load carrier {0} is unknown in LM"); } }
        public static string DeleteCapturePhotos { get { return Language.Translate("Delete or Keep pictures?"); } }
        public static string Enabled { get { return Language.Translate("Enable"); } }
        public static string Disabled { get { return Language.Translate("Disabled"); } }
        public static string GpsStatus { get { return Language.Translate("GPS Status"); } }
        public static string Altitude { get { return Language.Translate("Altitude"); } }
        public static string Latitude { get { return Language.Translate("Latitude"); } }
        public static string Position { get { return Language.Translate("Position"); } }
        public static string Satellites { get { return Language.Translate("Satellites"); } }
        public static string EnableDisableGps { get { return Language.Translate("Enable or disable the GPS"); } }


        public static string AlterProductGroup { get { return Language.Translate("Alter Product Group"); } }
        public static string ChooseProductGroup { get { return Language.Translate("Choose Product Group"); } }
        public static string ProductGroupChanged { get { return Language.Translate("Product Group changed from {0} to {1} for consignment {2}"); } }
        public static string ProductGroupChangeRegistered { get { return Language.Translate("Product Group change is registered for {0}"); } }

        public static string AcceptChange { get { return Language.Translate("Accept Change"); } }
        public static string EmptyProductCategory { get { return Language.Translate("Product category is not selected"); } }
        public static string ProductCategoryIsNotChanged { get { return Language.Translate("Selected product category '{0}' is same as existing value"); } }

        public static string Version { get { return Language.Translate("Version"); } }
        public static string ChangeProductGroupAborted { get { return Language.Translate("Change product group aborted"); } }
        public static string Height { get { return Language.Translate("Height"); } }
        public static string Width { get { return Language.Translate("Width"); } }
        public static string Length { get { return Language.Translate("Length"); } }
        public static string Skip { get { return Language.Translate("Skip"); } }
        public static string Print { get { return Language.Translate("Print"); } }
        public static string NewRegistration { get { return Language.Translate("New Registration"); } }
        public static string ScanPdf417 { get { return Language.Translate("Scan PDF417(or consignment number)"); } }
        public static string UnknownConsignmentScanPdf { get { return Language.Translate("Unknown consignment information for item {0}. Scan PDF to register consignment information"); } }

        public static string NewNumberOfItemsWillBeSet { get { return Language.Translate("New number of items will be set to {0}. Click Back if not Ok to accept."); } }
        public static string LoadDistributionTruck { get { return Language.Translate("Load distribution truck"); } }
        public static string LoadLineHaulButton { get { return Language.Translate("LoadLineHaulButton"); } }
        public static string UnloadLineHaulButton { get { return Language.Translate("UnloadLineHaulButton"); } }
        public static string LoadLineHaul { get { return Language.Translate("Load line haul"); } }
        public static string UnloadLineHaul { get { return Language.Translate("Unload line haul"); } }
        public static string UnloadPickUpTruck { get { return Language.Translate("Unload pickup truck"); } }
        public static string LoadOrActivateLoadCarrier { get { return Language.Translate("Do you want to load the carrier or do you want to activate for loading?"); } }
        public static string UnloadOrActivateLoadCarrier { get { return Language.Translate("Do you want to unload the carrier or you want to activate for unloading?"); } }
        public static string Corectwvfor { get { return Language.Translate("Correct w/v for"); } }
        public static string NumberOfItemsInShipment { get { return Language.Translate("Number of items in shipment"); } }
        public static string LoadCarrierScanAborted { get { return Language.Translate("Scanning of carrier aborted"); } }
        public static string CarrierMoveId { get { return Language.Translate("Carrier Move Id"); } }
        public static string ChangeRole { get { return Language.Translate("Change role"); } }
        public static string ActiveRole { get { return Language.Translate("Active role"); } }

        public static string Reason { get { return Language.Translate("Reason"); } }
        public static string Action { get { return Language.Translate("Action"); } }
        public static string ScanOrEnterItemOrCarrier { get { return Language.Translate("Scan/enter consignment item and carrier id"); } }
        public static string ScanOrEnterRbtOrPowerUnit { get { return Language.Translate("Scan or enter RBT or PowerUnit Id"); } }

        public static string FreeText { get { return Language.Translate("Free Text"); } }
        public static string ItemAlreadyRegisterd { get { return Language.Translate("Item is already registered as General Deviation"); } }
        public static string ItemAlreadyRegistered { get { return Language.Translate("Item is already registered"); } }


        public static string InvalidItem { get { return Language.Translate("is an invalid item"); } }
        public static string ItemUnloaded { get { return Language.Translate("Item {0} Unloaded"); } }
        public static string ActiveShelf { get { return Language.Translate("Active Shelf"); } }
        public static string NoInformationToShow { get { return Language.Translate("No information"); } }
        public static string NoOfItems { get { return Language.Translate("No. of Items"); } }
        public static string Complete { get { return Language.Translate("Complete"); } }

        public static string ScanConsignmentItemDeliveryCodeOrLoadcarrier { get { return Language.Translate("Consignment Item, Delivery Code or Load Carrier"); } }
        public static string Recipient { get { return Language.Translate("Recipient"); } }
        public static string Town { get { return Language.Translate("Town"); } }
        public static string ScanPdfOrConsignmentNumberMessage { get { return Language.Translate("Please scan a consignment number or a PDF417 Waybill"); } }
        public static string ConsignmentItemRegisteredWithoutConsignment { get { return Language.Translate("Item {0} is registered without consignment information"); } }
        public static string ScanConsignmentItemMessage { get { return Language.Translate("Please scan a consignment item"); } }
        public static string OfflineOrTimeout { get { return Language.Translate("Offline or Timeout. No response from LM. Try again later"); } }
        public static string ConsignmentItemConnectConsignment { get { return Language.Translate("Consignment item {0} has been connected to consignment {1}"); } }
        public static string ConsignmentActivated { get { return Language.Translate("Consignment {0} is activated"); } }
        public static string ScanPdfOrConsignmentNumber { get { return Language.Translate("Scan Consignment or PDF417 WayBill"); } }

        public static string ScanConsignmentItemOrPdf { get { return Language.Translate("Scan consignment,item or PDF417"); } }
        public static string ItemIsDeleted { get { return Language.Translate("Item {0} deleted"); } }
        public static string ScanEnterLocationId { get { return Language.Translate("Scan/Enter Location Id"); } }
        public static string DeliveredToAnotherLocationHeader { get { return Language.Translate("DeliveredToAnotherLocationHeader"); } }
        public static string DeliveredToAnotherLocationButton { get { return Language.Translate("DeliveredToAnotherLocationButton"); } }
        public static string ConfirmDelWantToDelete { get { return Language.Translate("Confirm del. Want to delete item {0} ?"); } }
        public static string NoInformation { get { return Language.Translate("No Shelf location in LM"); } }
        public static string PleaseScanValidOrgUnit { get { return Language.Translate("Please scan valid OrgUnit Id"); } }
        public static string DeliveredToPoPibByCustomer { get { return Language.Translate("Delivered to PO/PiB by customer"); } }
        public static string Activate { get { return Language.Translate("Activate"); } }
        public static string LoadCarrierAlreadyScanned { get { return Language.Translate("Load carrier already scanned"); } }
        public static string LoadCarrierAlreadyUnloaded { get { return Language.Translate("Load carrier already unloaded"); } }
        public static string LoadCarrierAlreadyLoaded { get { return Language.Translate("Load carrier already loaded"); } }



        public static string GoodsLoadedIntoCarrier { get { return Language.Translate("{0} loaded into carrier {1}"); } }
        public static string OfflineGoodsLoadedIntoCarrier { get { return Language.Translate("Offline : {0} loaded into carrier {1}"); } }
        public static string GoodsLoadedIntoRoute { get { return Language.Translate("{0} loaded into route {1}"); } }

        public static string GoodsUnLoadedFromCarrier { get { return Language.Translate("{0} Unloaded from carrier {1}"); } }

        public static string Above25 { get { return Language.Translate("Above 25"); } }
        public static string Below25 { get { return Language.Translate("Below 25"); } }
        public static string TooYoung { get { return Language.Translate("Too young"); } }
        public static string DeliveryToConsignee { get { return Language.Translate("Delivery to Consignee"); } }
        public static string Next { get { return Language.Translate("Next"); } }
        public static string Previous { get { return Language.Translate("Previous"); } }
        public static string SignOffAppliesTo { get { return Language.Translate("Sign off applies to: consignment item and contract signed with the supplier: Do you want to continue?"); } }
        public static string ConsignmentRefusedByConsignee { get { return Language.Translate("The following consignment items are registered as refused by the consignee. The items shall not be delivered. Do you want to continue?"); } }
        public static string ItemRegisteredOutOfHolding { get { return Language.Translate("Item {0} is registered out of holding"); } }
        public static string LoadCarrierRegisteredOutOfHolding { get { return Language.Translate("load carrier {0} is registered out of holding"); } }
        public static string ScanOrEnterConsignmentItem { get { return Language.Translate("Scan or enter consignment item"); } }
        public static string CanNotOpenDeviationForLoadCarrier { get { return Language.Translate("Can not open deviation menu for load carrier"); } }
        public static string NoItemScanned { get { return Language.Translate("No item scanned!"); } }
        public static string LoadCarrierNotValid { get { return Language.Translate("Load carrier not valid."); } }
        public static string LoadCarrierTerminated { get { return Language.Translate("Loadcarrier {0} was terminated {1}"); } }

        public static string EnterItemCarrierOrNewShelf { get { return Language.Translate("Enter item, carrier or new shelf"); } }

        public static string RegisterTimeConsumption { get { return Language.Translate("Register time consumption"); } }
        public static string NoTimeConsumptionSelected { get { return Language.Translate("Please select time from list"); } }
        public static string ShelfLocationIsDifferent { get { return Language.Translate("The shelf location is different from org unit id {0} - {1}"); } }

        public static string HandedInAtTerminal { get { return Language.Translate("Handed in at Terminal"); } }
        public static string AcceptNoConnection { get { return Language.Translate("Accept no connection"); } }
        public static string ScanConsignmentOrPdf417 { get { return Language.Translate("Scan or enter consignment or PDF417"); } }
        public static string PleaseUsePos { get { return Language.Translate("Offline : Please use POS in Post office"); } }
        public static string Item { get { return Language.Translate("Item"); } }
        public static string CorrectCountOfItems { get { return Language.Translate("Correct Number of items in consignment"); } }
        public static string EnterValidNumberOfItems { get { return Language.Translate("Please enter valid number of items"); } }
        public static string NoOfItemsScanned { get { return Language.Translate("Number of items scanned"); } }
        public static string CorrectNumberOfItems { get { return Language.Translate("Correct Number of Items"); } }
        public static string NumberOfItemsInConsignment { get { return Language.Translate("Number of Items in Consignment"); } }
        public static string ItemAlreadyRegisteredNotDelivered { get { return Language.Translate("Item is already registered as Not Delivered"); } }
        public static string LoadCarrierAlreadyRegistered { get { return Language.Translate("Loadcarrier  {0} already registered"); } }
        public static string ScanRbtOrEnterPowerUnit { get { return Language.Translate("Scan R/B/T label or enter power unit id"); } }
        public static string PleaseEnterPowerunit { get { return Language.Translate("Please enter powerunit"); } }
        public static string PleaseEnterItemOrLoadCarrier { get { return Language.Translate("Please enter consignment item or loadcarrier"); } }
        public static string ChooseOrgUnit { get { return Language.Translate("Choose Org. Unit"); } }
        public static string ConsigneeNameLength { get { return Language.Translate("Consignee Name should be between 2 to 28 characters"); } }
        public static string SelectReasonValue { get { return Language.Translate("Selected reason value"); } }
        public static string SelectedActionValue { get { return Language.Translate("Selected action value"); } }
        public static string CancelTryOrProcessOffline { get { return Language.Translate("You can cancel,try again or proceed in offline mode."); } }
        public static string WishToKeepExistingConnection { get { return Language.Translate("Wish to keep existing connection {0}or create New ?"); } }
        public static string PhysicalCarrierAlreadyConnected { get { return Language.Translate("Physical load carrier is already connected to a logical load carrier"); } }
        public static string Destination { get { return Language.Translate("Destination : {0}"); } }
        public static string DestinationName { get { return Language.Translate("Destination"); } }
        public static string DestinationName1 { get { return Language.Translate("Destination1"); } }
        public static string DestinationName2 { get { return Language.Translate("Destination2"); } }
        public static string Pwr { get { return Language.Translate("Pwr : {0}"); } }
        public static string NewConn { get { return Language.Translate("New Conn."); } }
        public static string NewConnection { get { return Language.Translate("New Connection"); } }
        public static string OldConnection { get { return Language.Translate("Old Connection"); } }
        public static string KeepOld { get { return Language.Translate("Keep Old"); } }
        public static string PrintErrorLoadLineHaul { get { return Language.Translate("Only Carrier with postal service code major client and crossdocking may print"); } }
        public static string PrintError { get { return Language.Translate("Error while sending printer request"); } }
        public static string NoPrinterFor { get { return Language.Translate("No Printers for {0}"); } }
        public static string PrintVoucher { get { return Language.Translate("Print voucher"); } }
        public static string SelectPrinterFromlist { get { return Language.Translate("Select Printer in list"); } }
        public static string ChangePrinter { get { return Language.Translate("Change Printer"); } }
        public static string ActivateCarierForPrinting { get { return Language.Translate("Activate carier for printing"); } }
        public static string Printer { get { return Language.Translate("Printer"); } }
        public static string ScanLoadCarrierAndTripId { get { return Language.Translate("Scan or enter log. load carrier or R/B/T or Power Unit Id"); } }
        public static string ScanTripInfoOrRouteId { get { return Language.Translate("Scan or Enter R/B/T or Route Id or Power Unit Id"); } }

        public static string OrgUnit { get { return Language.Translate("Org. Unit"); } }
        //public static string DeliverToLocation { get { return Language.Translate("Delivered to {0}"); } }

        public static string ItemHandledInPostOffice { get { return Language.Translate("item  {0} was handed in at Post office"); } }

        public static string AlertConsignmentItem { get { return Language.Translate("Please scan/enter consignment item no."); } }
        public static string ShelfIdIsActivated { get { return Language.Translate("Shelf {0} is activated"); } }
        public static string ConsignmentItemWasMovedToShelf { get { return Language.Translate("{0} {1} was moved to new shelf {2}"); } }
        public static string ConsignmentItemWasMovedToNewPlacement { get { return Language.Translate("{0} was moved to new placement {1}"); } }
        public static string ConsignmentItemRegisteredInToShelf { get { return Language.Translate("{0} {1} is registered into shelf {2}"); } }
        public static string UseArrivalRegistrationToRegisterGoods { get { return Language.Translate("Use the Arrival Registration to register goods that has arrived and to register change of shelf location"); } }
        public static string ScanOrEnterShelf { get { return Language.Translate("Scan or enter shelf id"); } }
        public static string NoReplyFromServer { get { return Language.Translate("No reply from server"); } }
        public static string PlaceIsActivated { get { return Language.Translate("Place  {0} is activated"); } }
        public static string ItemIsRegisteredIntoPlace { get { return Language.Translate("Item {0} is registered into place {1}"); } }
        public static string OfflineItemIsRegisteredIntoPlace { get { return Language.Translate("Offline: Item {0} is registered into place {1}"); } }
        public static string LoadCarrierIsRegisteredIntoPlace { get { return Language.Translate("Load carrier {0} is registered into place {1}"); } }
        public static string OfflineLoadCarrierIsRegisteredIntoPlace { get { return Language.Translate("Offline: Load carrier {0} is registered into place {1}"); } }
        //public static string ScanOrEnterGoodsPlacement { get { return Language.Translate("Scan or enter goods placement"); } }
        //public static string CarrierRemovedFromWorkList { get { return Language.Translate("Carrier removed from Work List"); } }
        //public static string ConfirmDelete { get { return Language.Translate("Confirm delete: Want to delete item"); } }
        public static string ChangeOrgUnitAborted { get { return Language.Translate("Change of org. unit aborted."); } }
        //public static string ValidRbtScanned { get { return Language.Translate("Valid rbt scanned!"); } }
        public static string ChangeOrgUnit { get { return Language.Translate("Change Org. Unit"); } }
        public static string LoadCombTrip { get { return Language.Translate("Load Comb. trip"); } }
        public static string RegisterDistribVehicle { get { return Language.Translate("Register distr. vehicle (trip1)"); } }
        public static string RegisterLinehaulTruck { get { return Language.Translate("Register truck (trip2)"); } }

        public static string ScanItemOrNewActiveTrip { get { return Language.Translate("Scan or enter consignment item number"); } }
        //public static string CarrList { get { return Language.Translate("Carr.List"); } }
        public static string NewTrip { get { return Language.Translate("New Trip"); } }
        public static string Carrier { get { return Language.Translate("Carrier"); } }
        //public static string Loaded { get { return Language.Translate("loaded"); } }
        public static string LoadedHeading { get { return Language.Translate("Loaded"); } }
        //public static string Unloaded { get { return Language.Translate("Unloaded"); } }
        public static string LoadCarrierNotConnected { get { return Language.Translate("{0} Carrier not connected in back system, cannot activate for unloading in LineHaul"); } }
        public static string LoadCarrierConnected { get { return Language.Translate("Connected to {0} and cannot be loaded"); } }

        //Messages related to showing registration messages, while not in loading/unloading processes
        public static string CarrierTerminatedButRegistered { get { return Language.Translate("Load Carrier {0} is terminated but registered"); } }
        public static string CarrierUnknownButRegistered { get { return Language.Translate("Load Carrier {0} is unknown but registered"); } }
        public static string CarrierRegisteredOffline { get { return Language.Translate("Offline:Load Carrier {0} is registered"); } }
        public static string CarrierRegistered { get { return Language.Translate("Load Carrier {0} is registered"); } }
        public static string LoadCarrierRegisteredInToShelf { get { return Language.Translate("Load carrier {0} is registered into shelf {1}"); } }
        public static string OfflineLoadCarrierRegisteredInToShelf { get { return Language.Translate("Offline:Load carrier {0} is registered into shelf {1}"); } }
        public static string CarrierTerminatedButRegisteredIntoShelf { get { return Language.Translate("Load Carrier {0} is terminated but registered into shelf {1}"); } }
        public static string CarrierUnknownButRegisteredIntoShelf { get { return Language.Translate("Load Carrier {0} is unknown but registered into shelf {1}"); } }

        //Messages related to showing messages for activation of carrier
        public static string CarrierTerminatedCanNotBeUsedForLoading { get { return Language.Translate("Load Carrier {0} terminated and can't be used for loading"); } }
        public static string CarrierUnknownCanNotBeUsedForLoading { get { return Language.Translate("Load Carrier {0} is unknown and can't be used for loading"); } }
        public static string CarrierTerminatedButActivatedForUnloading { get { return Language.Translate("Load Carrier {0} is terminated but activated for unloading"); } }
        public static string CarrierUnknownButActivatedForUnloading { get { return Language.Translate("Load Carrier {0} is unknown but activated for unloading"); } }
        public static string CarrierTerminatedButActivated { get { return Language.Translate("Load Carrier {0} is terminated but activated"); } }
        public static string CarrierUnknownButActivated { get { return Language.Translate("Load Carrier {0} is unknown but activated"); } }

        //Messages related to showing registration messages, while in loading/Unloading process.
        public static string CarrierTerminatedCanNotLoad { get { return Language.Translate("Load Carrier {0} terminated and can't load"); } }
        public static string CarrierUnknownCanNotLoad { get { return Language.Translate("Load Carrier {0} is unknown and can't load"); } }
        public static string CarrierTerminatedAndLoaded { get { return Language.Translate("Load Carrier {0} terminated but loaded into carrier {1}"); } }
        public static string CarrierTerminatedAndUnLoaded { get { return Language.Translate("Load Carrier {0} terminated but Un loaded from carrier {1}"); } }
        public static string CarrierUnknownAndLoaded { get { return Language.Translate("Load Carrier {0} un known but loaded into carrier {1}"); } }
        public static string CarrierUnKnownAndUnLoaded { get { return Language.Translate("Load Carrier {0} un known but Un loaded from carrier {1}"); } }
        public static string CarrierLoaded { get { return Language.Translate("Load Carrier {0} loaded into carrier {1}"); } }
        public static string CarrierLoadedOffLine { get { return Language.Translate("Offline:Load Carrier {0} loaded into carrier {1}"); } }
        public static string CarrierUnLoaded { get { return Language.Translate("Load Carrier {0} Un loaded from carrier {1}"); } }
        public static string CarrierUnLoadedOffLine { get { return Language.Translate("Offline:Load Carrier {0} Un loaded from carrier {1}"); } }
        public static string CarrierLoadedIntoRoute { get { return Language.Translate("Load Carrier {0} loaded into route {1}"); } }
        public static string CarrierLoadedOffLineIntoRoute { get { return Language.Translate("Offline:Load Carrier {0} loaded into route {1}"); } }
        public static string CarrierTerminatedAndLoadedIntoRoute { get { return Language.Translate("Load Carrier {0} terminated but loaded into route {1}"); } }
        public static string CarrierUnknownAndLoadedIntoRoute { get { return Language.Translate("Load Carrier {0} un known but loaded into route {1}"); } }

        public static string LoadCarrierCanNotBeLoaded { get { return Language.Translate("Rules for loading are broken. Can't load"); } }

        //public static string IsActivated { get { return Language.Translate("is activated."); } }
        public static string Route { get { return Language.Translate("Route"); } }
        public static string ScanningRbtAborted { get { return Language.Translate("Scanning of RBT aborted."); } }
        public static string ScanningLoadCarrierAborted { get { return Language.Translate("Scanning of Load Carrier aborted."); } }
        public static string ScanningPhysicalCarrierAborted { get { return Language.Translate("Scanning of Physical Carrier aborted."); } }

        public static string EnteringManualRbtAborted { get { return Language.Translate("Entering manual R/B/T aborted!"); } }
        public static string TransportLoadCarrierSaved { get { return Language.Translate("Transport/Load carrier saved"); } }
        public static string ItemIsAlreadyLoadedTo { get { return Language.Translate("Item is already loaded on to {0}"); } }
        public static string ScanOrEnterTransportCarrierId { get { return Language.Translate("Scan or enter Transport Carrier Id"); } }
        public static string ScanningLoadCarrierRbtAborted { get { return Language.Translate("Scanning of load carrier/Rbt aborted."); } }
        //public static string ScanOrEnterConsignmentItemTrip { get { return Language.Translate("Scan or enter consignment item number or trip"); } }
        public static string CarrierId { get { return Language.Translate("Carrier id"); } }
        public static string LoadedItems { get { return Language.Translate("Loaded items"); } }
        public static string NotAvailableShortForm { get { return Language.Translate("N/A"); } }
        //public static string AlterWeightVol { get { return Language.Translate("Alter weight/vol"); } }
        public static string ManualInput { get { return Language.Translate("Manual input"); } }
        public static string Cancelled { get { return Language.Translate("Canceled"); } }
        public static string PleaseEnterValidCount { get { return Language.Translate("Please Enter Valid Consignment Item Count"); } }
        public static string PleaseEnterValidScannedCount { get { return Language.Translate("Please Enter Scanned Consignment Item Count"); } }
        public static string DeliveryByCustomer { get { return Language.Translate("Delivery By Customer"); } }
        public static string PlaceId { get { return Language.Translate("Place Id"); } }
        public static string Shelf { get { return Language.Translate("Shelf"); } }
        public static string LoadCarrierType { get { return Language.Translate("Type"); } }
        public static string PostgangCode { get { return Language.Translate("Local/Distr"); } }
        public static string CdAddress1 { get { return Language.Translate("CDAddress1"); } }
        public static string CdAddress2 { get { return Language.Translate("CDAddress2"); } }
        public static string CdAddress3 { get { return Language.Translate("CDAddress3"); } }
        public static string ProductionFlowCode { get { return Language.Translate("Flow"); } }
        public static string TimeCreated { get { return Language.Translate("Created"); } }
        public static string RbtOfLoadLineRequire { get { return Language.Translate("You scanned RBT of load distrib. trip. Please scan RBT of load line trip"); } }
        public static string RbtOfLoadDistribRequire { get { return Language.Translate("You scanned RBT of Line Haul trip. Please scan RBT of load distrib. trip"); } }



        //public static string IsLoadedOn { get { return Language.Translate("is loaded on"); } }
        //public static string IsLoaded { get { return Language.Translate("is loaded"); } }
        // auto public static string IsAlreadyLoaded { get { return Language.Translate("Item is already loaded."); } }
        // auto public static string IsAlreadyLoadedOnTo { get { return Language.Translate("Item is already loaded on to"); } }
        // auto public static string IsUnknown { get { return Language.Translate("is unknown"); } }
        public static string ConsignmentIsUnknown { get { return Language.Translate("Consignment Is Unknown"); } }


        public static string Cons { get { return Language.Translate("Cons"); } }
        public static string LEv { get { return Language.Translate("L.Ev"); } }
        public static string Send { get { return Language.Translate("Send"); } }
        public static string Recp { get { return Language.Translate("Recp"); } }
        public static string WidthVolume { get { return Language.Translate("W/V"); } }
        public static string Blk { get { return Language.Translate("Blk!"); } }
        public static string Plc { get { return Language.Translate("Plc"); } }
        public static string Rtrn { get { return Language.Translate("Rtrn"); } }
        public static string DegrC { get { return Language.Translate("°C"); } }
        public static string Cat { get { return Language.Translate("Cat"); } }
        public static string ItemNotFound { get { return Language.Translate("Item {0} not found"); } }
        public static string ConsignmentNotFound { get { return Language.Translate("Consignment {0} not found"); } }
        public static string NotAvailable { get { return Language.Translate("Not available"); } }
        public static string DateAndTimeFormat { get { return Language.Translate("dd.MM.yyyy HH:mm"); } }

        public static string IdShouldBeOfLength { get { return Language.Translate("Id should be of length between {0} and {1}"); } }
        //public static string ConsignmentNameLength { get { return Language.Translate("Consignee Name should be of length between {0} and {1}"); } }
        public static string PleaseSelectAtLeastOne { get { return Language.Translate("Please check atleast one checkbox"); } }

        //public static string ItemCanNotBeDelivered { get { return Language.Translate("Consignment Item shall not be delivered - Stop Order"); } }
        //public static string NotAllowedToDeliverLogicalLoadCarrier { get { return Language.Translate("Not Allowed to Deliver Logical Load Carrier"); } }
        public static string AlreadyActivated { get { return Language.Translate("already activated."); } }
        public static string InvalidDeliveryCode { get { return Language.Translate("Invalid Delivery Code."); } }


        public static string IsAllItemsAreScanned { get { return Language.Translate("You have scanned {0} consignment items for consignment {1}. The expected number of consignment items is {2}"); } }
        public static string CameraUpdatedInstruction { get { return Language.Translate("Remember! Use Enter button on PDA to capture picture"); } }
        public static string CameraDeleteInstruction { get { return Language.Translate("Delete picture?Click on thumbnail"); } }
        public static string CameraDeletePicture { get { return Language.Translate("Delete this Picture?"); } }


        //After date 06/09/2013
        public static string PlannedDeliveryDateSet { get { return Language.Translate("Planned delivery date is set for {0}"); } }
        public static string PlannedDeliveryDateNotSetOnItem { get { return Language.Translate("No planned delivery date is set on the item {0}"); } }
        public static string LoadCarrierRegistered { get { return Language.Translate("load carrier {0} is registered."); } }
        public static string LoadCarrierUnloaded { get { return Language.Translate("load carrier {0} unloaded."); } }
        public static string OfflineLoadCarrierUnloaded { get { return Language.Translate("Offline: load carrier {0} unloaded."); } }
        public static string PrintSucessful { get { return Language.Translate("Print out is sent to printer"); } }
        public static string MailSucessFul { get { return Language.Translate("List is sent by mail"); } }
        public static string LogLoadCarrierRegistered { get { return Language.Translate("Log. Load carrier registerd"); } }
        public static string ScanOrEnterPhyLoadCarrier { get { return Language.Translate("Scan or enter phys. load carrier"); } }
        public static string EnterTripInformation { get { return Language.Translate("Enter trip information"); } }
        public static string TurnOnOrOffLog { get { return Language.Translate("Turn log level on and off"); } }
        public static string PhyLoadCarrier { get { return Language.Translate("Phys. load carrier"); } }
        public static string PhyCarrierDoesNotExists { get { return Language.Translate("Physical Carrier {0} does not exists"); } }
        public static string DeviationRegistered { get { return Language.Translate("Deviation registered to item"); } }


        public static string OfflineChangePassword { get { return Language.Translate("You are currently offline. Go online to change password"); } }
        public static string Reson24Warning { get { return Language.Translate("This registration require an agreement with the consignee, that allows later delivery"); } }

        public static string IncorrectSignature { get { return Language.Translate("Incorrect signature"); } }
        public static string ItemWithDeliveryCodeRegistered { get { return Language.Translate("Item {0} with delivery code {1} is registered"); } }
        public static string IdConfirmation { get { return Language.Translate("Id Confirmation"); } }
        public static string YouHaveRecieved { get { return Language.Translate("You have recieved"); } }
        public static string NumberOfConsignmentItems { get { return Language.Translate("Number of consignment items"); } }
        public static string NumberOfLoadCarriers { get { return Language.Translate("Number of load carriers"); } }
        public static string BankId { get { return Language.Translate("Bank Id"); } }
        public static string EuropeanId { get { return Language.Translate("European Id"); } }
        public static string PostenId { get { return Language.Translate("Posten-Id Card"); } }
        public static string DriversLicence { get { return Language.Translate("Drivers Licence"); } }
        public static string Passport { get { return Language.Translate("Passport"); } }
        public static string IdDetails { get { return Language.Translate("Id Details"); } }

        public static string SelectRegisterTimeInstruction { get { return Language.Translate("Highlight the row and accept additional work time with Ok"); } }

        public static string RegisterAsDelayed { get { return Language.Translate("Register as delayed"); } }
        public static string LoadCarrierAbbrevation { get { return Language.Translate("LB"); } }
        public static string TotalWeight { get { return Language.Translate("Total Weight"); } }
        public static string ItemIsRegistered { get { return Language.Translate("Item {0} is registered."); } }
        public static string UnloadWholeCarrier { get { return Language.Translate("Unload whole carrier?"); } }
        public static string DelayRegisteredFor { get { return Language.Translate("Delay has been registered for {0}"); } }
        public static string WholeCarrierUnloaded { get { return Language.Translate("Whole carrier unloaded {0} Activate new or cancel"); } }
        public static string UnloadingWholeCarrierAborted { get { return Language.Translate("Unloading of whole carrier {0} aborted"); } }
        public static string Dest { get { return Language.Translate("Dest"); } }

        public static string ChooseAction { get { return Language.Translate("Please choose an action below"); } }
        public static string NoAttempetdDeliveryRegistered { get { return Language.Translate("This consignment has no \"Attempted Delivery\" event registered."); } }
        public static string Not { get { return Language.Translate("Not"); } }
        public static string Delivered { get { return Language.Translate("Delivered"); } }
        public static string LoadCarrierDeleted { get { return Language.Translate("Load Carrier {0} deleted"); } }

        public static string ItemIsStoppedReturnIt { get { return Language.Translate("Item is stopped. Return it."); } }

        public static string CorrectItem { get { return Language.Translate("Correct item"); } }
        public static string TransportCarrier { get { return Language.Translate("Transport carrier"); } }
        public static string ScanConsignmentOrItem { get { return Language.Translate("Scan/Enter Consignment or item"); } }
        public static string UnknownItemRegistered { get { return Language.Translate("Item {0} with unknown consignment info is registered"); } }
        public static string NoStopsFound { get { return Language.Translate("No stops found"); } }
        public static string OrganizationUnitAborted { get { return Language.Translate("Organization unit aborted"); } }

        public static string Deliver { get { return Language.Translate("Deliver"); } }
        public static string Message { get { return Language.Translate("Message"); } }
        public static string Availability { get { return Language.Translate("Availability"); } }
        public static string Detail { get { return Language.Translate("Detail"); } }
        public static string InvalidNumberOrFormatScanOrEnterItemOrShelf { get { return Language.Translate("Invalid number or format. Scan or enter item or shelf"); } }
        public static string NewLocation { get { return Language.Translate("New location"); } }
        public static string NoInformationAboutItemAvailable { get { return Language.Translate("No information about item available"); } }
        public static string NoEarlierDamageAvailable { get { return Language.Translate("No earlier damage available"); } }
        public static string LoadCarrierAlreadyActive { get { return Language.Translate("Load carrier {0} already active"); } }
        public static string OldConnectionToPhysicalPreserved { get { return Language.Translate("Old connection to physical carrier {0} is preserved."); } }
        public static string ScanOrEnterLoadCarrierForAssembly { get { return Language.Translate("Scan or enter log. load carrier for assembly"); } }


        public static string Order { get { return Language.Translate("Order"); } }
        public static string Dev { get { return Language.Translate("DEV"); } }
        public static string Cust { get { return Language.Translate("Cust"); } }
        public static string Ref { get { return Language.Translate("Ref"); } }
        public static string Serivces { get { return Language.Translate("Serivces"); } }
        public static string Start { get { return Language.Translate("Start"); } }
        public static string End { get { return Language.Translate("Stop"); } }
        public static string OrderInfo { get { return Language.Translate("Order Info"); } }
        public static string ItemNofN { get { return Language.Translate("{0} {1} of {2}"); } }
        public static string Time { get { return Language.Translate("Time"); } }
        public static string Number { get { return Language.Translate("Number"); } }
        public static string GoodsInfo { get { return Language.Translate("Goods Info"); } }
        public static string GoodsInformation { get { return Language.Translate("Goods Information"); } }
        public static string Information { get { return Language.Translate("Information"); } }


        public static string PreviousStop { get { return Language.Translate("Previous Stop"); } }
        public static string NextStop { get { return Language.Translate("Next Stop"); } }
        public static string PreviousGood { get { return Language.Translate("Previous Good"); } }
        public static string NextGood { get { return Language.Translate("Next Good"); } }
        public static string AddVas { get { return Language.Translate("Add VAS"); } }
        public static string AddDev { get { return Language.Translate("Add Dev"); } }
        public static string Customer { get { return Language.Translate("Customer"); } }
        public static string NewDeviationIsRegistered { get { return Language.Translate("New deviation is registered"); } }
        public static string DeviationRegistrationAborted { get { return Language.Translate("Deviation registration aborted"); } }
        public static string DeviationProcessAborted { get { return Language.Translate("Deviation process is aborted"); } }
        public static string RegisteredVAS { get { return Language.Translate("Registered VAS"); } }
        public static string NewServiceRegistered { get { return Language.Translate("New service is registered"); } }
        public static string NewServiceRegistrattionAborted { get { return Language.Translate("New service registration aborted"); } }
        public static string StopList { get { return Language.Translate("Stop List"); } }
        public static string OrderList { get { return Language.Translate("Order List"); } }
        public static string AllOrders { get { return Language.Translate("All Orders"); } }
        public static string NewMessage { get { return Language.Translate("New message"); } }
        public static string Reply { get { return Language.Translate("Reply"); } }
        public static string Read { get { return Language.Translate("Read"); } }
        public static string ReadMessage { get { return Language.Translate("Read Message"); } }
        public static string DeleteAll { get { return Language.Translate("Delete All"); } }
        public static string Restore { get { return Language.Translate("Restore"); } }
        public static string Inbox { get { return Language.Translate("Inbox"); } }
        public static string DurationShortCuts { get { return Language.Translate("Duration short cuts"); } }
        public static string ConfirmDeleteMessage { get { return Language.Translate("Are you sure you want to delete this message from Inbox and move it to deleted items folder ?"); } }
        public static string ConfirmDeleteAllMessage { get { return Language.Translate("Are you sure you want to permanently delete all items in the deleted items folder?"); } }
        public static string HeaderLanguage { get { return Language.Translate("HeaderLanguage"); } }
        public static string LoadCarrierId { get { return Language.Translate("Load carrier Id"); } }
        public static string LoadCarrierActivated { get { return Language.Translate("Load carrier {0} activated"); } }
        public static string ScanLoadCarrierOrConsignmentItem { get { return Language.Translate("Scan load carrier or consignment item"); } }

        public static string ServicesAdded { get { return Language.Translate("Services added"); } }
        public static string TheConsignmentItemContainsAlcoholetc { get { return Language.Translate("The consignment item contains alcohol. Remember to check the consignees age and that consignee is sober.Do you want to continue?"); } }
        public static string CheckIdPapersToVerifyAge { get { return Language.Translate("Check Id papers to verify age!"); } }

        public static string ScanOrEnterConsignmentAndItemNumber { get { return Language.Translate("Scan or enter consignment(item) number"); } }
        public static string ErrorUpdatingMessageStatus { get { return Language.Translate("An error occured while updating message status"); } }
        public static string LanguageOptions { get { return Language.Translate("Language Options"); } }
        //Language Options
        public static string SelectVas { get { return Language.Translate("Select Vas"); } }
        public static string ConsignmentDeliveredByAuthorization { get { return Language.Translate("Is the consignment delivered by authorization?"); } }

        public static string Payments { get { return Language.Translate("Payments"); } }
        public static string DeliveryCodeScannedOffline { get { return Language.Translate("Delivery Code cannot be scanned offline"); } }
        public static string NoOfCarriers { get { return Language.Translate("No of carriers"); } }

        public static string Deleted { get { return Language.Translate("Deleted"); } }
        public static string Sent { get { return Language.Translate("Sent"); } }
        public static string PleaseEnterMessageToSend { get { return Language.Translate("Please enter message to send"); } }
        public static string VerifyingNewSortingOrderWithAlystraBex { get { return Language.Translate("Verifying new sorting order with Alystra Bex"); } }
        public static string PleaseWait { get { return Language.Translate("Please wait"); } }
        public static string YourStopListHasBeenSorted { get { return Language.Translate("Your stop list has been sorted"); } }
        public static string TheNewSortingWasNotApprovedByAlysraBex { get { return Language.Translate("The new sorting was not approved by Alysra BEX"); } }
        public static string TimeOut { get { return Language.Translate("Time out"); } }

        public static string DeliveryFromVinmonopoletInOpeningHour { get { return Language.Translate("Delivery of items from Vinmonopolet must be done with in opening hour. Override this rule?"); } }
        public static string Abort { get { return Language.Translate("Abort"); } }
        public static string Override { get { return Language.Translate("Override"); } }
        public static string Code { get { return Language.Translate("Code"); } }
        public static string Call999ToGetAcceptanceCode { get { return Language.Translate("Call 90 51 40 18 to get acceptance code for the delivery"); } }
        public static string OverrideOpeningHours { get { return Language.Translate("Override Opening Hours"); } }

        public static string NameAndSignatureMissing { get { return Language.Translate("NameAndSignatureMissing"); } }
        public static string NameMissing { get { return Language.Translate("NameMissing"); } }
        public static string SignatureMissing { get { return Language.Translate("SignatureMissing"); } }

        public static string InvalidNumberOrFormatScanOrEnterShelf { get { return Language.Translate("Invalid number or format. Scan or enter shelf"); } }
        public static string TotalAmount { get { return Language.Translate("Total Amount"); } }

        public static string SelectTypeInstruction { get { return Language.Translate("Search by code or text. Select type."); } }
        public static string SelectReasonInstruction { get { return Language.Translate("Search by code or text. Select reason."); } }
        public static string SelectActionInstruction { get { return Language.Translate("Search by code or text. Select action."); } }
        public static string InvalidNumber { get { return Language.Translate("Invalid number"); } }
        public static string MenuLanguage { get { return Language.Translate("Menu Language"); } }
        public static string KeypadLanguage { get { return Language.Translate("Keypad Language"); } }
        public static string RegisteredDev { get { return Language.Translate("Registered Dev"); } }
        public static string Notes { get { return Language.Translate("Notes"); } }
        public static string ScanConsignmentOrItemOrLoadcarrier { get { return Language.Translate("Input Consignment, item or carrier"); } }

        public static string ConsignmentNumbers { get { return Language.Translate("Consignment numbers"); } }
        public static string TaskDetails { get { return Language.Translate("Task details"); } }
        public static string ItemDetails { get { return Language.Translate("Item Details"); } }
        public static string ConfirmationLoadList { get { return Language.Translate("Load List Confirmation"); } }
        public static string ConfirmLoadListMessage { get { return Language.Translate("Backup load list found on device. Do you want to load it?"); } }
        public static string ContractedItems { get { return Language.Translate("Contracted items"); } }
        public static string BulkRegistrationNotAllowed { get { return Language.Translate("Bulk Registration is not allowed on this Consignment"); } }
        public static string PdaOfflineDuringRequest { get { return Language.Translate("PDA was offline during validation request"); } }
        public static string UserName { get { return Language.Translate("Username"); } }
        public static string ScanningUnplannedConsignment { get { return Language.Translate("Scanning unplanned consignmentEntity not authorized"); } }
        public static string NoErrors { get { return Language.Translate("No errors"); } }
        public static string TimeFormat { get { return Language.Translate("dd.MM.yy HH.mm.ss"); } }
        public static string ConfirmDeleteItem { get { return Language.Translate("Confirm Delete . Are you sure you want to delete item"); } }
        public static string For { get { return Language.Translate("For"); } }
        public static string Correctwv { get { return Language.Translate("Correct w/v"); } }
        public static string OfflineOperationCanNotPerform { get { return Language.Translate(" Offline :Operation can not perform"); } }
        public static string EmailIsKnown { get { return Language.Translate("e-mail is known"); } }
        public static string EmailIsNotKnown { get { return Language.Translate("e-mail is not known"); } }
        public static string ContinueIfStopsNotAvailable { get { return Language.Translate("Stops not available. Do you want to continue?"); } }
        public static string YouAreOffline { get { return Language.Translate("You are offline."); } }
        public static string ContinueIfOnline { get { return Language.Translate("You are offline. Do you want to continue?"); } }
        public static string DoYouWantToActivateWorklist { get { return Language.Translate("Do you want to activate as a work list item?"); } }
        public static string EventRegisteredWithResponse { get { return Language.Translate("Event is registered without validation response."); } }
        public static string Reg { get { return Language.Translate("Reg"); } }
        public static string Sedi { get { return Language.Translate("S/EDI"); } }
        public static string SelectedVas { get { return Language.Translate("Select VAS"); } }
        public static string ConfirmedBy { get { return Language.Translate("Confirmed by"); } }
        public static string Amount { get { return Language.Translate("Amount"); } }
        public static string EnterValidAmount { get { return Language.Translate("Enter valid amount."); } }
        public static string LoadCarrierAlreadyActivate { get { return Language.Translate("Load carrier already active"); } }
        public static string RouteAlreadyActive { get { return Language.Translate("Route already active"); } }

        public static string StopInfo { get { return Language.Translate("Stop Info"); } }

        public static string ViewChanges { get { return Language.Translate("View Changes"); } }
        public static string ActivateTripsStepNof2 { get { return Language.Translate("Activate 2 trips. Step {0} of 2"); } }
        public static string YouHaveSelecetedLoadLineButScannedLoadDistr { get { return Language.Translate("You have selected load line but scanned load distr."); } }
        public static string YouHaveSelectedLoadingDistrVehButScannedLineTrip { get { return Language.Translate("You have selected loading distr. veh. but scanned line trip"); } }
        public static string SelectedWliIsNotRelatedToCombinationTripPleaseTryAnotherOne { get { return Language.Translate("Selected WLI is not related to combination trip, please try another one"); } }
        public static string CombinationTripHasDifferentPowerUnits { get { return Language.Translate("Combination trips requires identical power units. Power units scanned: {0}/{1}"); } }
        public static string CombinationTripHasDifferentAgentNumbers { get { return Language.Translate("Combination trips requires identical agent numbers. Agent numbers scanned: {0}/{1}"); } }
        public static string CombinationTripAlreadyActive { get { return Language.Translate("combination trip already active"); } }
        public static string ActiveCarrAre { get { return Language.Translate("Active carr. are"); } }
        public static string NewValuesOfChangedFeilds { get { return Language.Translate("New values of the changed fields are"); } }
        public static string ScanOrEnterRbt { get { return Language.Translate("Scan or enter R/B/T id"); } }
        public static string SyntheticConsignment { get { return Language.Translate("Synthetic Consignment"); } }
        public static string ExtraInfoNeededForSyntheticConsignmentNumber { get { return Language.Translate("More information is needed for the consignment. The consignment item should not be loaded"); } }
        public static string CantProcessSyntheticConsignment { get { return Language.Translate("Cannot process synthetic consignments"); } }
        public static string ErrorConnectingPrinter { get { return Language.Translate("Error contacting print module, please try again later"); } }

        public static string ScannedAndTotalItem { get { return Language.Translate("You have entered {0} items in consignment and scanned {1} items Click back if not Ok to accept"); } }
        public static string LoadCarrierActivatedWithRecipent { get { return Language.Translate(" Load carrier {0} activated with {1}"); } }
        public static string LoadCarrierNotValidated { get { return Language.Translate("Load carrier not validated"); } }
        public static string ProductCode { get { return Language.Translate("Product Code"); } }
        public static string DoYouWantToDeleteThisItem { get { return Language.Translate("Do you want to delete this item {0} ?"); } }
        public static string Reversed { get { return Language.Translate("Reversed"); } }
        public static string Default { get { return Language.Translate("Default"); } }
        public static string ConsignmentPaymentHeading { get { return Language.Translate("ConsignmentPaymentHeading"); } }
        public static string AmountPaymentHeading { get { return Language.Translate("AmountPaymentHeading"); } }
        public static string InvalidBookingNumber { get { return Language.Translate("Invalid Booking Number"); } }
        public static string OfflineItemHandledInPostOffice { get { return Language.Translate("Offline:item  {0} was handed in at Post office"); } }
        public static string OfflineLoadCarrierRegistered { get { return Language.Translate("Offline:load carrier {0} is registered."); } }
        public static string AttemptDel { get { return Language.Translate("Attempted Del."); } }
        public static string OfflineActiveCarrAre { get { return Language.Translate("Offline:Active carr. are"); } }
        public static string CarrierActivated { get { return Language.Translate("Carrier {0} is activated."); } }
        public static string OfflineCarrierActivated { get { return Language.Translate("Offline:Carrier {0} is activated."); } }
        public static string TaskDetail { get { return Language.Translate("Task Detail"); } }
        public static string RouteActivated { get { return Language.Translate("Route {0} is activated."); } }
        public static string LoadCarrierloaded { get { return Language.Translate("Load carrier {0} loaded."); } }
        public static string OfflineGoodsLoadedIntoRoute { get { return Language.Translate("Offline:{0} loaded into route {1}"); } }
        public static string OfflineTransportLoadCarrierSaved { get { return Language.Translate("Offline:Transport/Load carrier saved"); } }
        public static string ItemLoaded { get { return Language.Translate("Item {0} is loaded"); } }
        public static string ItemLoadedOn { get { return Language.Translate("Item {0} is loaded on {1}."); } }
        public static string OfflineItemLoaded { get { return Language.Translate("Offline:Item {0} is loaded"); } }
        public static string OfflineItemLoadedOn { get { return Language.Translate("Offline:Item {0} is loaded on {1}."); } }
        public static string OfflineCarrierOpened { get { return Language.Translate("Offline:Carrier opened successfully"); } }
        public static string OfflineCarrierClosed { get { return Language.Translate("Offline:Carrier closed successfully"); } }
        public static string NewActionReason { get { return Language.Translate("New Action Reason"); } }

        public static string BulkRegistrationFlowAborted { get { return Language.Translate("Bulk Registration flow aborted"); } }
        public static string BulkRegistrationCompleted { get { return Language.Translate("Bulk Registration completed"); } }
        public static string OfflineRegisteredConsignment { get { return Language.Translate("Offline:Registered consignment"); } }
        public static string ConsigneeName { get { return Language.Translate("Consignee Name"); } }
        public static string OfflineGoodsUnLoadedFromCarrier { get { return Language.Translate("Offline:{0} Unloaded from carrier {1}"); } }
        public static string NewStop { get { return Language.Translate("New Stop"); } }
        public static string LoadCarrierNotActivated { get { return Language.Translate("Load carrier {0} not activated"); } }
        public static string ShowReadOnlyMode { get { return Language.Translate("Show Read Only Mode"); } }
        public static string ActiveOffline { get { return Language.Translate("active offline"); } }

        public static string WrongVersionAlysraBexSorting { get { return Language.Translate("The new sorting was not approved by Alysra BEX due to wrong Version of Operation list"); } }
        public static string InvalidOrderingAlysraBexSorting { get { return Language.Translate("The new sorting was not approved by Alysra BEX due to invalid ordering of Operation list"); } }

        public static string ConsigmentItemNotSameWithPdfItemNumber { get { return Language.Translate("Consignment item {0} in pdf is not same as consignment item {1} scanned for linking"); } }
        public static string WrongBarCodeType { get { return Language.Translate("Wrong barcode type"); } }
        public static string UnableToRetrieveUserProfile { get { return Language.Translate("Failed to login.Could not retrieve user profile"); } }


        public static string SortingAlystraStopOrderDownMessage { get { return Language.Translate("You cannot put pickup  operation  below the delivery operation for same order"); } }
        public static string SortingAlystraStopOrderUpMessage { get { return Language.Translate("You cannot put delivery operation above the pickup operation for same order"); } }
        public static string RegisteredUnkownItemMessage { get { return Language.Translate("Consignment item {0} registered with unknown consignment number"); } }
        public static string PlaceIsAlreadyActivated { get { return Language.Translate("Place  {0} is  already activated"); } }
        public static string LoadingOfItemsWithoutKnownConsignmentNotAllowed { get { return Language.Translate("Loading of Items without known consignment not allowed"); } }
        public static string DeliveryBeforePickupOrder { get { return Language.Translate("The pickup for this order has not been registered. You cannot deliver before you have picked up the goods."); } }

        public static string StartTime { get { return Language.Translate("Start Time"); } }
        public static string EndTime { get { return Language.Translate("End Time"); } }
    }
}

#endif
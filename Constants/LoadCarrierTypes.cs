﻿namespace Com.Bring.PMP.PreComFW.Shared.Constants
{
    public class LoadCarrierTypes
    {
        public const string LineHaul = "HPG"; //LineHaul
        public const string MajorCustomer = "SK"; // Storkunde
        public const string RegisteredMail = "REK"; // Rekommandert
        public const string LocalMail = "LPG"; // LokalPostgang
    }
}

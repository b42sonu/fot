﻿namespace Com.Bring.PMP.PreComFW.Shared.Constants
{
    public class GlobalConstants
    {
        /// Password policy names.

        // ReSharper disable InconsistentNaming
        public const string PASSWORD_WARNING_DATE_COUNT = "Password Change Warning";
        public const int PASSWORD_WARNING_DAY_COUNT_VALUE = 5;

        /// Password policy default values.
        public const string PASSWORD_EXPIRE_DATE_COUNT = "Password Expired";
        public const int PASSWORD_EXPIRE_DAY_COUNT_VALUE = 60;
        
        public const string PASSWORD_HISTORY_COUNT = "Password history count";
        public const int PASSWORD_HISTORY_COUNT_VALUE = 5;

        public const string PASSWORD_POLICY = "Password Policy";
        public const string PASSWORD_POLICY_VALUE = @"((?=.*\d)(?=.*[a-z]).{6,})";

        /// Recent password changed date
        public const string PASSWORD_CHANGED_DATE = "Password Changed";

        public const string PASSWORD_NEVER_EXPIRED_VALUE = "01/01/1900 00:00:00";

        /// User location
        public const string USER_LOCATION_ID = "User Location Id";

        /// Session id for Driver Availability
        public const string USER_SESSION_ID = "User Session Id";

        /// User keypad setting
        public const string USER_KEYPAD_LANGUAGE = "UserKeypadLanguage";
        public const string Keypad_QWERTY = "QWERTY";
        public const string Keypad_qwerty = "qwerty";
        public const string Keypad_123 = "123";
        public const string Keypad_Sym = "Sym";

        /// User Inactivity Timeout 
        public const string USER_INACTIVITY_LOGOUT = "InactivityTimeoutLogout";
        public const int USER_INACTIVITY_LOGOUT_DEFAULT_TIMEOUT_VALUE = 60;

        /// Inactivity ScreenLock 
        public const string USER_INACTIVITY_SCREENLOCK_ENABLED = "ScreenLockActive";
        public const string USER_INACTIVITY_SCREENLOCK_TIMEOUT = "InactivityTimeoutScreenlock";
        public const int USER_INACTIVITY_SCREENLOCK_DEFAULT_TIMEOUT_VALUE = 5;


        public const string INFO_COLOR_RGB = "Info Color Rgb";
        public const string WARNING_COLOR_RGB = "Warning Color Rgb";
        public const string ERROR_COLOR_RGB = "Error Color Rgb";

        public const string TEXT_COLOR_INFO = "Info Text Color";
        public const string TEXT_COLOR_WARNING = "Warning Text Color";
        public const string TEXT_COLOR_ERROR = "Error Text Color";

        // How long to flash backlight on received message
        public const string BACKLIGHT_FLASH_DURATION = "BacklightFlashDuration";

        
        // ReSharper restore InconsistentNaming         
    }

    public class TmsAffiliation
    {
        public const string NoTms = "NO_TMS";
        public const string Amphora = "AMPHORA";
        public const string Alystra = "ALYST_BEX";
    }
}

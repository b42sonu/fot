﻿using System;

namespace Com.Bring.PMP.PreComFW.Shared.Constants
{
    [Flags]
    public enum OrderInfoValidationType
    {
        NoValidation = 0,
        CustomerId = 1,
        OrderNumber = 2,
        CustomerIdAndBookingNumber = 3,
        AmphoraValidation=4,
    };
}

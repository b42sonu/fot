﻿using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using CommunicationEntity.Loading;
using CommunicationEntity.LogicalLoadCarrierEvent;
using System;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.EventHelpers
{
    static public class LoadCarrierEventHelper
    {
        public static ConnectLoadCarriersRequest CreateConnectLoadCarriersAsyncRequest(string physicalLoadCarrier, string logicalLoadCarrier)
        {

            var eventInfo = new ConnectLoadCarriersRequest
                {
                    PhysicalId = physicalLoadCarrier,
                    LogicalId = logicalLoadCarrier,
                    UserLogonId = ModelUser.UserProfile.UserId,
                    OrgUnitId = ModelUser.UserProfile.OrgUnitId,
                    TMS_Affiliation = ModelUser.UserProfile.TmsAffiliation
                };
            return eventInfo;

        }

        public static T20261_ConnectLogicalToPhysicalLoadCarSyncRequest CreateConnectLoadCarriersSyncRequest(string physicalLoadCarrier, string logicalLoadCarrier)
        {

            var eventInfo = new T20261_ConnectLogicalToPhysicalLoadCarSyncRequest
            {
                PhysicalId = physicalLoadCarrier,
                LogicalId = logicalLoadCarrier,
                UserLogonId = ModelUser.UserProfile.UserId,
                OrgUnitId = ModelUser.UserProfile.OrgUnitId,
                TMS_Affiliation = ModelUser.UserProfile.TmsAffiliation
            };
            return eventInfo;
        }


        public static LogicalLoadCarrierEvent CreateLoadCarrierEvent(string loadCarrierId, string stopId, string tripId, string shelfLoacation, string orgUnitId, string eventCode, Signature signature)
        {

            //Create object for event information
            var eventInformation = new LogicalLoadCarrierEventEventInformation
            {
                LogicalLoadCarrierId = loadCarrierId,
                ShelfLoaction = shelfLoacation,
                EventType = eventCode,
                Picture = signature != null ? Convert.ToBase64String(signature.Sketch) : string.Empty,
                Signature = signature != null ? signature.PrintedName : string.Empty,
                Timestamp = DateTime.Now
            };

            //Create object for load carrier event
            var loadCarrierEvent = new LogicalLoadCarrierEvent
            {
                UserInformation = GetLoadCarrierEventsUserInformation(),
                LocationInformation = GetLoadCarrierEventsLocationInformation(),
                EventInformation = eventInformation,
                LoggingInformation = LoggingInformation.GetLoggingInformationForLoadCarrierEvent("Load Carrier Event", ModelUser.UserProfile)
            };
            return loadCarrierEvent;
        }


        public static List<LogicalLoadCarrierEvent> CreateLoadCarrierEventsFromEntityMap(EntityMap entityMap, Signature signature)
        {
            var loadCarrierEvents = entityMap.Select(entity => entity.Value)
                .OfType<LoadCarrier>()
                .Select(loadCarrier =>
                    CreateLoadCarrierEvent(loadCarrier.ConsignmentId, string.Empty, string.Empty, string.Empty, ModelUser.UserProfile.OrgUnitId, EventCodeUtil.GetEventCodeFromProcessForLoadCarrier(), signature)).ToList();
            return loadCarrierEvents;
        }



        public static LogicalLoadCarrierEventUserInformation GetLoadCarrierEventsUserInformation()
        {
            Logger.LogEvent(Severity.Debug, "Retrieveing load carrier event user information");
            var userProfile = ModelUser.UserProfile;

            LogicalLoadCarrierEventUserInformation userInformation = null;
            if (userProfile != null)
            {

                userInformation = new LogicalLoadCarrierEventUserInformation
                {
                    UserLogonId = userProfile.UserId,
                    FirstName = String.IsNullOrEmpty(userProfile.FirstName) ? "unknown" : userProfile.FirstName,
                    LastName = String.IsNullOrEmpty(userProfile.LastName) ? "unknown" : userProfile.LastName,
                    UserOrgUnitId = userProfile.OrgUnitId,
                    PhoneNumber = userProfile.TelephoneNumber,
                    CompanyCode = userProfile.CompanyCode,
                    TMS_Affiliation = userProfile.TmsAffiliation,
                };
            }

            return userInformation;
        }

        public static LogicalLoadCarrierEventLocationInformation GetLoadCarrierEventsLocationInformation()
        {
            Logger.LogEvent(Severity.Debug, "Retrieving load carrier event location information");
            LogicalLoadCarrierEventLocationInformation loadCarrierEventsLocationInformation = null;

            if (ModelUser.UserProfile != null)
            {
                loadCarrierEventsLocationInformation = new LogicalLoadCarrierEventLocationInformation
                {
                    EventCompanyCode = ModelUser.UserProfile.CompanyCode,
                    EventOrgUnitId = ModelUser.UserProfile.OrgUnitId,
                    LocationId = string.Empty,

                    EventPostalCode = ModelUser.UserProfile.PostalCode,
                    EventCountryCode = ModelUser.UserProfile.CountryCode
                };
            }

            return loadCarrierEventsLocationInformation;
        }

        public static LogicalLoadCarrierEventLoggingInformation GetLoadCarrierEventsLoggingInformation()
        {
            Logger.LogEvent(Severity.Debug, "Retrieving load carrier event location information");

            return new LogicalLoadCarrierEventLoggingInformation();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom;
using PreCom.Core.Communication;
using AddressType = Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList.AddressType;
using Consignment = Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap.Consignment;

namespace Com.Bring.PMP.PreComFW.Shared.EventHelpers
{
    static public class GoodsEventHelper
    {
        public static GoodsEvents CreateGoodsEvent()
        {
            return CreateGoodsEvent(null);
        }

        
        public static GoodsEvents CreateGoodsEvent(ConsignmentEntity consignmentEntity)
        {
            return CreateGoodsEvent(consignmentEntity, null);
        }

        public static GoodsEvents CreateGoodsEvent(ConsignmentEntity consignmentEntity, string eventCode)
        {
            Logger.LogEvent(Severity.Debug, "Executing command GoodsEventHelper.CreateGoodsEvent()");

            var operationProcess = ModelMain.SelectedOperationProcess;

            var operationListStop = CommandsOperationList.GetStop(operationProcess.StopId);

            

            var eventInformation = new GoodsEventsEventInformation();

            if (string.IsNullOrEmpty(eventCode))
                eventCode = EventCodeUtil.GetEventCodeFromProcess();

            if (consignmentEntity is ConsignmentItem)
            {
                eventInformation = GetGoodsEventsEventInformationForConsignmentItem(consignmentEntity.CastToConsignmentItem(), eventCode);
            }
            else if (consignmentEntity is Consignment)
            {
                eventInformation = GetGoodsEventsEventInformationForConsignment(consignmentEntity.CastToConsignment(), operationProcess.StopId, operationProcess.OperationId, eventCode);
            }

            // Send an event to backend that we have scanned this consignment item
            var goodsEvent = new GoodsEvents
            {
                UserInformation = GetGoodsEventsUserInformation(),
                EventInformation = eventInformation,
                LocationInformation = GetGoodsEventsLocationInformation(operationListStop),
                OperationInformation = GetGoodsEventsOperationInformation(operationProcess),
                CaptureEquipment = GetGoodsEventsCaptureEquipment(),
                TMSAffiliationId = ModelUser.UserProfile != null ? ModelUser.UserProfile.TmsAffiliation : null,
                LoggingInformation = GetLoggingInformation()
            };

            return goodsEvent;
        }

        public static GoodsEvents CreateGoodsEventMessage(EntityMap entityMap, OperationProcess operationProcess, Signature signature, string eventCode)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsDelivery.CreateGoodsEventMessage()");

            try
            {
                var eventInformation = GetGoodsEventsEventInformationForConsignments(entityMap, eventCode);


                //If no consignment or consignment items were scanned then return null for goods event message.
                if (eventInformation.Consignment != null && eventInformation.Consignment.Length == 0)
                    return null;

                var stopInfo = operationProcess != null ? CommandsOperationList.GetStop(operationProcess.StopId) : null;

                // Send an event to backend with all scanned consignment items
                var userInformation = GetGoodsEventsUserInformation();
                

                var locationInformation = GetGoodsEventsLocationInformation(stopInfo);
                var captureEquipment = GetGoodsEventsCaptureEquipment();

                GoodsEventsOperationInformation operationInformation = GetGoodsEventsOperationInformation(operationProcess);
                
                var goodsEventMessage = new GoodsEvents
                {
                    UserInformation = userInformation,
                    EventInformation = eventInformation,
                    LocationInformation = locationInformation,
                    CaptureEquipment = captureEquipment,
                    OperationInformation = operationInformation,
                    LoggingInformation = GetLoggingInformation(),
                    TMSAffiliationId = ModelUser.UserProfile.TmsAffiliation
                };

                if (signature != null)
                {
                    var goodseventSignature = new GoodsEventsEventInformationSignature
                    {
                        RecipientName = signature.PrintedName,
                        Signature = signature.Sketch,
                        SignatureId = Guid.NewGuid().ToString(),
                        SignatureTime = DateTime.Now
                    };

                    // Recipient name is mandatory in e-connect
                    if (string.IsNullOrEmpty(goodseventSignature.RecipientName))
                        goodseventSignature.RecipientName = "  ";

                    goodsEventMessage.EventInformation.Signature = goodseventSignature;
                }
                return goodsEventMessage;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsDelivery.CreateGoodsEventMessage");
                return null;
            }
        }
        public static GoodsEvents CreateGoodsEventMessage(EntityMap entityMap, OperationProcess operationProcess, Signature signature)
        {
            return CreateGoodsEventMessage(entityMap, operationProcess, signature, null);
        }

        public static T20200_ValidateGoodsFromFOTReply ValidateGoodsFromFot(ICommunicationClient communicationClient, T20200_ValidateGoodsFromFOTRequest request)
        {
            T20200_ValidateGoodsFromFOTReply reply = null;
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.ValidateGoodsFromFot()");
            try
            {
                string description = request.ConsignmentItemNumber ?? request.ConsignmentNumber;

                var transaction = new Transaction(new Guid(request.LoggingInformation.MessageId), (byte)TransactionCategory.ValidateGoodsFromFot, description);
                reply = communicationClient.Query<T20200_ValidateGoodsFromFOTReply>(request, transaction);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "Validation.ValidateGoodsFromFot");
            }

            return reply;
        }

        public static T20277_ValidateLoadCarrierFromFOTReply ValidateLoadCarrierFromFot(ICommunicationClient communicationClient, T20277_ValidateLoadCarrierFromFOTRequest request)
        {
            T20277_ValidateLoadCarrierFromFOTReply reply = null;
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.ValidateLoadCarrierFromFot()");
            try
            {
                string description = request.LoadCarrierId ?? request.LoadCarrierId;

                var transaction = new Transaction(new Guid(request.LoggingInformation.MessageId), (byte)TransactionCategory.ValidateGoodsFromFot, description);
                reply = communicationClient.Query<T20277_ValidateLoadCarrierFromFOTReply>(request, transaction);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "Validation.ValidateLoadCarrierFromFot");
            }

            return reply;
        }


        public static GoodsEventsEventInformation GetGoodsEventsEventInformationForConsignmentItem(ConsignmentItem consignmentItem, string eventCode)
        {

            Logger.LogEvent(Severity.Debug, "Retrieveing goods event information for consignment item");

            var eventInformation = new GoodsEventsEventInformation
            {
                EventCode = eventCode,
                EventTime = DateTime.Now,
                Consignment = new[] { Create(consignmentItem) }
            };

            return eventInformation;
        }


        
        public static GoodsEventsEventInformation GetGoodsEventsEventInformationForConsignment(Consignment consignment, string stopId, string operationId, string eventCode)
        {
            Logger.LogEvent(Severity.Debug, "Retrieveing goods event information for consignment");

            var eventInformation = new GoodsEventsEventInformation
            {
                EventCode = eventCode,
                EventTime = DateTime.Now,
                Consignment = new[] { Create(consignment) }
            };

            return eventInformation;
        }

        public static GoodsEventsUserInformation GetGoodsEventsUserInformation()
        {
            Logger.LogEvent(Severity.Debug, "Retrieveing goods event user information");
            UserProfile userProfile = ModelUser.UserProfile;

            GoodsEventsUserInformation userInformation = null;
            if (userProfile != null)
            {
                userInformation = new GoodsEventsUserInformation
                {
                    UserLogonId = userProfile.UserId,
                    FirstName = String.IsNullOrEmpty(userProfile.FirstName) ? "unknown" : userProfile.FirstName,
                    LastName = String.IsNullOrEmpty(userProfile.LastName) ? "unknown" : userProfile.LastName,
                    UserOrgUnitId = userProfile.OrgUnitId,
                    PhoneNumber = userProfile.TelephoneNumber
                };
            }

            return userInformation;
        }


        public static GoodsEventsLocationInformation GetGoodsEventsLocationInformation(OperationListStop operationListStop)
        {
            Logger.LogEvent(Severity.Debug, "Retrieving goods event location information from operationlist stop");
            GoodsEventsLocationInformation goodsEventsLocationInformation = null;

            if (ModelUser.UserProfile != null)
            {
                goodsEventsLocationInformation = new GoodsEventsLocationInformation
                {
                    EventCompanyCode = ModelUser.UserProfile.CompanyCode,
                    EventOrgUnitId = ModelUser.UserProfile.OrgUnitId,
                    EventPostalCode = ModelUser.UserProfile.PostalCode,
                    EventCountryCode =  ModelUser.UserProfile.CountryCode
                };
            }

            return goodsEventsLocationInformation;
        }

        public static GoodsEventsCaptureEquipment GetGoodsEventsCaptureEquipment()
        {
            var captureEquipmentId = "unknown";// in testing situation Communication is not instaniated

#pragma warning disable 618
            if (Application.Instance.Communication != null)
                captureEquipmentId = Application.Instance.Platform.Hardware.Tag;
#pragma warning restore 618

            var goodsCaptureEquipment = new GoodsEventsCaptureEquipment
            {
                CaptureEquipmentId = captureEquipmentId,
                CaptureEquipmentType = "PDA"
            };
            return goodsCaptureEquipment;
        }


        /// <summary>
        /// method for get selected opreation event infromation..
        /// </summary>
        /// <param name="operationProcess"> </param>
        /// <returns></returns>
        internal static GoodsEventsOperationInformation GetGoodsEventsOperationInformation(OperationProcess operationProcess)
        {
            string operationOriginSystem = String.Empty;
            
            var goodsEventsOperationInformation = new GoodsEventsOperationInformation
            {
                OperationOriginSystem = operationOriginSystem,
                PlanId = string.Empty,
                ValueAddedService = null,
                PowerUnitId = GetPowerUnit() 
            };

            var operationListId = CommandsOperationList.GetOperationListId();
            if (string.IsNullOrEmpty(operationListId) == false)
                goodsEventsOperationInformation.OperationListId = operationListId;


            if (operationProcess != null)
            {
                if (ModelUser.UserProfile != null && ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora)
                    goodsEventsOperationInformation.StopId = operationProcess.StopId;

                if (string.IsNullOrEmpty(operationProcess.RouteId) == false)
                    goodsEventsOperationInformation.RouteId = operationProcess.RouteId.ToUpper();

                if (!string.IsNullOrEmpty(operationProcess.LogicalLoadCarrierId) && operationProcess.WorkListItemType == WorkListItemType.LoadCarrier)
                {
                    goodsEventsOperationInformation.LoadCarrier = operationProcess.LogicalLoadCarrierId.ToUpper();
                }

                if (!string.IsNullOrEmpty(operationProcess.PhysicalLoadCarrierId))
                {
                    goodsEventsOperationInformation.PhysicalLoadCarrier = operationProcess.PhysicalLoadCarrierId.ToUpper();
                }

                if (!string.IsNullOrEmpty(operationProcess.TripId))
                    goodsEventsOperationInformation.TripId = operationProcess.TripId.ToUpper();

                if (!string.IsNullOrEmpty(operationProcess.ExternalTripId))
                    goodsEventsOperationInformation.ExternalTripId = operationProcess.ExternalTripId.ToUpper();    
            }
            return goodsEventsOperationInformation;
        }

        private static void ValidateAddress(AddressType address)
        {
            if ((string.Compare(address.CountryCode, "no", StringComparison.OrdinalIgnoreCase) == 0 && address.PostalCode.Length != 4) ||
             (string.Compare(address.CountryCode, "se", StringComparison.OrdinalIgnoreCase) == 0 && address.PostalCode.Length != 5))
            {
                string messageText = GlobalTexts.IllegalAddress;

                //SoundUtil.Instance.PlayWarningSound();
                GuiCommon.ShowModalDialog(GlobalTexts.Attention, messageText, Severity.Warning, GlobalTexts.Ok);
            }
        }

        /// <summary>
        /// method for add photo in goods event in consignment and consignment based on the condition which type is scanned...
        /// </summary>
        public static GoodsEvents AddPhotoInGoodsEvent(GoodsEvents goodsEvent, ConsignmentEntity consignmentEntity, string folderName)
        {
            if (goodsEvent != null && goodsEvent.EventInformation != null && goodsEvent.EventInformation.Consignment != null && goodsEvent.EventInformation.Consignment.Length > 0)
            {
                GoodsEventsEventInformationConsignment goodsEventsConsignment = goodsEvent.EventInformation.Consignment[0];
                if (consignmentEntity is ConsignmentItem)
                {
                    if (goodsEventsConsignment != null && goodsEventsConsignment.ConsignmentItem != null && goodsEventsConsignment.ConsignmentItem.Length > 0)
                    {
                        GoodsEventsEventInformationConsignmentConsignmentItem goodsEventsConsignmentItem = goodsEventsConsignment.ConsignmentItem[0];
                        if (goodsEventsConsignmentItem != null)
                            goodsEventsConsignmentItem.Picture = GetPictureArray(folderName);
                    }
                }
                else
                {
                    if (goodsEventsConsignment != null)
                        goodsEventsConsignment.Picture = GetPictureArray(folderName);
                }
            }
            return goodsEvent;
        }
        /// <summary>
        /// method for get picture array....
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        private static PictureType[] GetPictureArray(string folderName)
        {
            PictureType[] arrayPictureType = null;
            string directoryPath =
                Path.Combine(Path.Combine(
                    Path.Combine(FileUtil.GetApplicationPath(), "Photo"),
                    folderName), "thumb");

            if (Directory.Exists(directoryPath))
            {
                string[] photosPaths = Directory.GetFiles(directoryPath, "*.jpg");

                if (photosPaths.Length > 0)
                {
                    arrayPictureType = new PictureType[photosPaths.Length];


                    short pictureCount = 1;
                    short dimention = 0;

                    foreach (var photoPath in photosPaths)
                    {
                        using (var fs = new FileStream(photoPath, FileMode.Open))
                        {
                            var bytes = new byte[fs.Length];
                            fs.Read(bytes, 0, (int)fs.Length);
                            fs.Flush();
                            fs.Close();

                            var picture = new PictureType
                                                      {
                                                          PictureId = folderName,
                                                          Picture = new byte[1][],
                                                          PictureNumber = new int[1]
                                                      };


                            picture.Picture[0] = bytes;
                            picture.PictureNumber[0] = pictureCount;

                            arrayPictureType[dimention] = picture;
                            pictureCount++;
                            dimention++;
                        }
                    }
                }
            }

            return arrayPictureType;
        }
        /// <summary>
        /// delete the folder of captured picture after adding images in goods event and send to server....
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public static bool DeletePicturesFolder(string folderName)
        {
            bool isDeleted = false;
            string directoryPath = Path.Combine(
                    Path.Combine(FileUtil.GetApplicationPath(), "Photo"),
                    folderName);
            if (Directory.Exists(directoryPath))
            {
                Directory.Delete(directoryPath, true);
                isDeleted = true;
            }
            return isDeleted;
        }


        public static GoodsEventsLoggingInformation GetLoggingInformation()
        {
            var loggingInformation = new GoodsEventsLoggingInformation
                                         {
                                             TimeFromClient = DateTime.Now,
                                             TimeFromClientSpecified = true,
                                             ProcessName  = BaseModule.ActiveModule != null ? BaseModule.ActiveModule.Name : string.Empty,
                                             SubProcessName = BaseModule.CurrentFlow != null ? BaseModule.CurrentFlow.CurrentProcess.ToString() : "Unknown process",
                                             SubProcessVersion = DynamicAssemblyInfo.Version 
                                         };
            return loggingInformation;
        }


        /// <summary>
        /// Create a GoodsEventInformation structure based on what consignmentid has been scanned, and what has been planned for this consignment
        /// 
        /// </summary>
        public static GoodsEventsEventInformation GetGoodsEventsEventInformationForConsignments(EntityMap entityMap, string eventCode)
        {
            Logger.LogEvent(Severity.Debug, "Retrieveing goods event information for consignment");

            var eventInformation = new GoodsEventsEventInformation
            {
                EventCode = string.IsNullOrEmpty(eventCode) ? EventCodeUtil.GetEventCodeFromProcess() : eventCode,
                EventTime = DateTime.Now,
                Consignment = GetConsignmentsForGoodsEvent(entityMap),
            };
            return eventInformation;
        }

        public static GoodsEventsEventInformationConsignment[] GetConsignmentsForGoodsEvent(EntityMap entityMap)
        {

            var consignments = new List<GoodsEventsEventInformationConsignment>();

            if (entityMap != null && entityMap.Values != null && entityMap.Count > 0)
            {

                foreach (var consignment in entityMap.Values)
                {
                    if (consignment is LoadCarrier)
                        continue;

                    var consignmentItemHistoryMap = entityMap.GetConsignmentItems(consignment.ConsignmentId);

                    // Retrieve all scanned consignment items
                    var scannedConsignmentItems = consignmentItemHistoryMap.Where(c => (c.Value.EntityStatus & EntityStatus.Scanned) != 0).ToList();

                    // If neither consignment, nor consignment items are scanned, we dont send any event
                    // Only if consignment has scanned items, ConsignmentItemEvent is sent
                    if (scannedConsignmentItems.Count == 0 && consignment.HasStatus(EntityStatus.Scanned) == false)
                        continue;

                    // Convert from valuepairs to array
                    var consignmentItems = new List<GoodsEventsEventInformationConsignmentConsignmentItem>();
                    consignmentItems.AddRange(scannedConsignmentItems.Select(consignmentItem => new GoodsEventsEventInformationConsignmentConsignmentItem
                    {
                        ConsignmentItemNumber = consignmentItem.Value.ItemId
                    }));

                    bool hasPruScanning = scannedConsignmentItems.Any(scannedItem => scannedItem.Value.IsPruScanning);
                    var eventType = hasPruScanning
                                        ? GoodsEventsEventInformationConsignmentEventType.ConsignmentItemEventPRU
                                        : scannedConsignmentItems.Count > 0
                                              ? GoodsEventsEventInformationConsignmentEventType.ConsignmentItemEvent
                                              : GoodsEventsEventInformationConsignmentEventType.ConsignmentEvent;

                    var consignmentInfo = CreateGoodsEventsEventInformationConsignment(consignment, consignmentItems, eventType);

                    consignments.Add(consignmentInfo);
                }
            }

            return consignments.ToArray();
        }

        private static GoodsEventsEventInformationConsignment CreateGoodsEventsEventInformationConsignment(Consignment consignment, List<GoodsEventsEventInformationConsignmentConsignmentItem> consignmentItems,
              GoodsEventsEventInformationConsignmentEventType consignmentItemEventType)
        {
            var result = new GoodsEventsEventInformationConsignment
            {
                ConsignmentNumber = consignment.ConsignmentIdForGoodsEvent,
                OrderNumber = consignment.OrderNumber,
                CustomerNumber = consignment.CustomerId,
                ConsignmentItem = consignmentItems.ToArray(),
                ConsignmentItemsHandled = consignmentItems.Count,
                ConsignmentItemsHandledSpecified = true,
                MassRegistration = false,
                EventType = consignmentItemEventType
            };

            // Use user specified consignmentitem count if existing
            if (consignment.ManuallyEnteredConsignmentItemCount > 0)
            {
                result.ConsignmentItemsHandled = consignment.ManuallyEnteredConsignmentItemCount;
            }

            return result;
        }

        public static GoodsEventsEventInformationConsignment Create(Consignment consignment)
        {
            int itemCount = consignment.ItemCount(EntityStatus.Scanned, false);
            var result = new GoodsEventsEventInformationConsignment
            {
                MassRegistration = true,
                EventType = GetEventType(consignment),
                ConsignmentItemsHandled = itemCount,
                ConsignmentItemsHandledSpecified = itemCount >= 1,
                ConsignmentNumber = consignment.ConsignmentIdForGoodsEvent,
                OrderNumber = consignment.OrderNumber,
                CustomerNumber = consignment.CustomerId,
            };
            return result;
        }

        private static GoodsEventsEventInformationConsignment Create(ConsignmentItem consignmentItem)
        {
            var result = new GoodsEventsEventInformationConsignment
            {
                MassRegistration = false,
                EventType = GetEventType(consignmentItem),
                ConsignmentNumber = consignmentItem.ConsignmentIdForGoodsEvent,
                CustomerNumber = consignmentItem.CustomerId,
                OrderNumber = consignmentItem.OrderNumber,
                ConsignmentItem = new[] { new GoodsEventsEventInformationConsignmentConsignmentItem { ConsignmentItemNumber = consignmentItem.ItemId } }
            };

            return result;
        }

        public static GoodsEventsEventInformationConsignmentEventType GetEventType(ConsignmentEntity consignmentEntity)
        {
            GoodsEventsEventInformationConsignmentEventType eventType;
            if (consignmentEntity is ConsignmentItem)
            {
                eventType = consignmentEntity.IsPruScanning
                                ? GoodsEventsEventInformationConsignmentEventType.ConsignmentItemEventPRU
                                : GoodsEventsEventInformationConsignmentEventType.ConsignmentItemEvent;
            }
            else
            {
                eventType = consignmentEntity.IsPruScanning
                                ? GoodsEventsEventInformationConsignmentEventType.ConsignmentEventPRU
                                : GoodsEventsEventInformationConsignmentEventType.ConsignmentEvent;
            }
            return eventType;
        }

        public static void UpdateEventInformationWithVasInformation(GoodsEvents goodsEvent, VasSubFlowResultInfo vasSubFlowResultInfo)
        {
            if (goodsEvent != null && goodsEvent.EventInformation != null)
            {
                goodsEvent.EventInformation.RecipientId = vasSubFlowResultInfo.RecipientId;
                goodsEvent.EventInformation.IdentificationDocumentType = vasSubFlowResultInfo.IdType;
                goodsEvent.EventInformation.EventComment = vasSubFlowResultInfo.EventComment;
                if (vasSubFlowResultInfo.AuthorizationUsed)
                    goodsEvent.EventInformation.AuthorizationCode = "1";

                if (goodsEvent.EventInformation.Signature != null)
                {
                    if (String.IsNullOrEmpty(goodsEvent.EventInformation.Signature.RecipientName))
                        goodsEvent.EventInformation.Signature.RecipientName = vasSubFlowResultInfo.RecipientName;
                }
            }
        }

        public static string GetPowerUnit()
        {
            var powerUnit = string.Empty;
            switch (ModelUser.UserProfile.RoleName)
            {
                case RoleUtil.Driver1:
                    powerUnit = ModelUser.UserProfile.PowerUnit;
                    break;
            }
            return powerUnit;
        }
    }
}
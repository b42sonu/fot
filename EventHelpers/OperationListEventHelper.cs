﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListEvent;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom;
using System.Linq;

namespace Com.Bring.PMP.PreComFW.Shared.EventHelpers
{
    public class OperationListEventHelper
    {
        #region "OperationListEvent realted methods"
        /// <summary>
        /// method for get capture equipment information..
        /// </summary>
        /// <returns></returns>
        public static OperationListEventCaptureEquipment GetOperationListEventCaptureEquipment()
        {
            OperationListEventCaptureEquipment eventCaptureEquipment = null;
            //class OperationListEventCaptureEquipment
#pragma warning disable 618
            if (Application.Instance.Communication != null)

            {
                eventCaptureEquipment = new OperationListEventCaptureEquipment
                {
                    CaptureEquipmentId = Application.Instance.Platform.Hardware.Tag,
                    CaptureEquipmentType = "PDA"
                };
            }
#pragma warning restore 618

            return eventCaptureEquipment;
        }

        /// <summary>
        /// method for get selected opreation event infromation..
        /// </summary>
        /// <param name="plannedOperation"></param>
        /// <param name="stopId">Specifies stopId for the current stop.</param>
        /// <param name="loadCarrierId">Specifies current load carrier id.</param>
        /// <param name="tripId">Specifies tripId for the current stop.</param>
        /// <returns>Specifies the object for OperationListEventOperationInformation.</returns>
        public static OperationListEventOperationInformation GetOperationListEventOperationInformation(PlannedOperationType plannedOperation, string stopId, string tripId, string loadCarrierId)
        {

            //class OperationListEventOperationInformation
            var eventOperationInformation = new OperationListEventOperationInformation
            {
                ExternalTripId = plannedOperation != null ? plannedOperation.ExternalTripId : string.Empty,
                LoadCarrier = plannedOperation != null ? plannedOperation.LoadCarrierId : loadCarrierId,
                OperationListId = plannedOperation != null ? CommandsOperationList.GetOperationListId() : string.Empty,
                OperationOriginSystem = string.Empty,
                PlanId = string.Empty,
                StopId = ModelUser.UserProfile != null && ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ? string.Empty : stopId,
                TMSAffiliationId = ModelUser.UserProfile != null ? ModelUser.UserProfile.TmsAffiliation : string.Empty,//Alystra  VAS/Dev required 
                TripId = plannedOperation != null ? plannedOperation.TripId : tripId,
                ValueAddedService = null,

                //Amphora 
                PlannedOperationId = plannedOperation != null ? plannedOperation.OperationId : string.Empty,
                OrderId = plannedOperation != null ? plannedOperation.OrderID : string.Empty,

                OperationType = plannedOperation != null ? GetOperationTypeForAmphora(plannedOperation.OperationType) : string.Empty,
                Route = plannedOperation != null ? plannedOperation.Route : string.Empty,
                
                //Alystra
                OperationListVersionNumber = ModelOperationList.OperationList != null ? ModelOperationList.OperationList.VersionNumber : string.Empty,
                SessionId = ModelOperationList.OperationList != null ? ModelOperationList.OperationList.SessionId : string.Empty
            };

            return eventOperationInformation;
        }

        private static string GetOperationTypeForAmphora(PlannedOperationTypeOperationType operationType)
        {
            switch (operationType)
            {
                case PlannedOperationTypeOperationType.LOAD_DISTRIBUTION:
                    return "LoadingOfDistributionTruck";

                case PlannedOperationTypeOperationType.LOAD_LINE_HAUL:
                    return "LoadingOfLineHaulTruck";

                case PlannedOperationTypeOperationType.PICKUP:
                    return "Pickup";

                case PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL:
                    return "UnloadingOfLineHaulTruck";

                case PlannedOperationTypeOperationType.UNLOAD_PICKUP:
                    return "UnloadingOfPickupTruck";

                case PlannedOperationTypeOperationType.DELIVERY:
                    return "DeliveryToCustomer";

                default:
                    return string.Empty;
            }
        }



        /// <summary>
        /// method for get event location information..
        /// </summary>
        /// <param name="operationListStop"></param>
        /// <returns></returns>
        public static OperationListEventLocationInformation GetOperationListEventLocationInformation(OperationListStop operationListStop)
        {
            //class OperationListEventLocationInformation 
            var eventLocationInformation = new OperationListEventLocationInformation();

            if (ModelUser.UserProfile != null)
            {
                eventLocationInformation.EventCompanyCode = ModelUser.UserProfile.CompanyCode;
                eventLocationInformation.EventCountryCode = ModelUser.UserProfile.CountryCode;
                eventLocationInformation.EventOrgUnitId = ModelUser.UserProfile.OrgUnitId;
            }

            if (operationListStop != null && operationListStop.StopInformation != null)
            {
                if (operationListStop.StopInformation.Address != null)
                {
                    eventLocationInformation.EventPostalCode = operationListStop.StopInformation.Address.PostalCode;
                    eventLocationInformation.EventCountryCode = operationListStop.StopInformation.Address.CountryCode;
                }
                eventLocationInformation.LocationId = operationListStop.StopInformation.LocationId;
            }

            return eventLocationInformation;
        }


        /// <summary>
        /// method for get event for user information from profile...
        /// </summary>
        /// <returns></returns>
        public static OperationListEventUserInformation GetOperationListEventUserInformation()
        {
            //class operationlist event user info ....
            OperationListEventUserInformation eventUserInformation = null;
            if (ModelUser.UserProfile != null)
            {
                eventUserInformation = new OperationListEventUserInformation
                                           {
                                               FirstName = ModelUser.UserProfile.FirstName,
                                               LastName = ModelUser.UserProfile.LastName,
                                               UserLogonId = ModelUser.UserProfile.UserId,
                                               UserOrgUnitId = ModelUser.UserProfile.OrgUnitId,
                                               MobilePhone = ModelUser.UserProfile.TelephoneNumber
                                           };
            }
            return eventUserInformation;
        }


        public static OperationListEventEventInformation GetOperationListEventInformation(PlannedOperationType plannedOperation, string operationStatus)
        {
            return GetOperationListEventInformation(null, plannedOperation, null, null, operationStatus);
        }

        ///// <summary>
        ///// method for get operation list event information...
        ///// </summary>
        ///// <returns></returns>
        //public static OperationListEventEventInformation GetOperationListEventInformation(PlannedOperationType plannedOperation, string eventCode)
        //{
        //    return GetOperationListEventInformation(null, plannedOperation, eventCode);
        //}
        public static OperationListEventEventInformation GetOperationListEventInformation(OperationListStop operationListStop, PlannedOperationType plannedOperation, string eventCode, Signature signature, string operationStatus)
        {
            OperationListEventEventInformationSignature sign = null;
            if (signature != null)
            {
                sign = new OperationListEventEventInformationSignature
                {
                    ConsigneeName = signature.PrintedName,
                    GraphicalSignature = signature.Sketch != null ? Convert.ToBase64String(signature.Sketch) : string.Empty
                };
            }
            //class OperationListEventEventInformation 
            var eventInformation = new OperationListEventEventInformation
            {
                ActionCode = string.Empty,//Alystra  Dev required 
                CauseCode = string.Empty,//Alystra  Dev required 
                DamageCode = string.Empty,
                EventCode = string.IsNullOrEmpty(eventCode) ? EventCodeUtil.GetEventCodeFromProcess() : eventCode,//Alystra  VAS/Dev required 
                EventComment = string.Empty,//TODO:For Alystra text entered for amount registered //Alystra required 
                EventStatus = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra ? GetEventStatusForAlystra(operationListStop) : string.Empty,
                //Added for Amphora
                EventStatusType = "New",//Alystra  VAS/Dev required 
                OperationStatus = !string.IsNullOrEmpty(operationStatus) ? operationStatus : string.Empty,//TODO : Required for Alystra VAS Operation List Event 
                ConsignmentItemCount = GetItemCount(plannedOperation, operationStatus),
                ConsignmentItemCountSpecified = true,
                EventTime = DateTime.Now,//Alystra  VAS/Dev required 
                PickUpOrder = plannedOperation != null ? plannedOperation.PickupOrderNumber : String.Empty,
                UtcTime = DateTime.UtcNow,
                UtcTimeSpecified = true,
                Signature = sign, //Alystra VAS not required 
                
            };
            return eventInformation;
        }

        /// <summary>
        /// FOT 84 change order
        /// </summary>
        /// <param name="plannedOperationType"></param>
        /// <param name="operationStatus"></param>
        /// <returns></returns>
        private static int GetItemCount(PlannedOperationType plannedOperationType, string operationStatus)
        {
            if((ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora)&&(operationStatus == StopOperationType.Started))
            {
                return 0;
            }
            if ((ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora) && (operationStatus == StopOperationType.Completed) && plannedOperationType != null)
            {
                return plannedOperationType.ConsignmentItemCount;
            }
            return plannedOperationType != null ? plannedOperationType.ConsignmentItemCount : 0;
        }

        private static string GetEventStatusForAlystra(OperationListStop operationListStop)
        {
            string eventCode = "C";
            if (operationListStop != null && operationListStop.AlystraBexOrderLevel != null && operationListStop.AlystraBexOrderLevel.DeviationItem != null)
            {
                var count = operationListStop.AlystraBexOrderLevel.DeviationItem.Count(n => n.IsAddedDeviation);
                if (count > 0)
                    eventCode = "D";
            }
            return eventCode;
        }
        #endregion
    }
}

﻿using System;
using System.ComponentModel;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListEvent;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using CommunicationEntity.LogicalLoadCarrierEvent;
using PreCom;

namespace Com.Bring.PMP.PreComFW.Shared.EventHelpers
{
    public class LoggingInformation
    {
        public static T GetLoggingInformation<T>(string subProcessName, UserProfile userProfile) where T : new()
        {
            string processName = BaseModule.CurrentFlow != null ? BaseModule.CurrentFlow.CurrentProcess.ToString() : "Unknown process";
            return GetLoggingInformation<T>(processName, subProcessName, userProfile);
        }

        public static T GetLoggingInformation<T>(string processName, string subProcessName, UserProfile userProfile) where T : new()
        {
            Logger.LogAssert(string.IsNullOrEmpty(processName) == false && processName.Length <= 21, "ProcessName length must be 1-21 chars");
            Logger.LogAssert(subProcessName.Length <= 20, "subProcessName max length is 20");
            
            var loggingInformation = new T();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(loggingInformation);
            
            SetPropertyString(loggingInformation, "CompanyCode", userProfile.CompanyCode, properties);
            SetPropertyString(loggingInformation, "InterfaceVersion", "1.0", properties); //TODO: Set real
            SetPropertyString(loggingInformation, "InvocationTime", DateTime.Now, properties);
            SetPropertyString(loggingInformation, "LanguageCode", userProfile.LanguageCode, properties);
            SetPropertyString(loggingInformation, "LogonName", userProfile.UserId, properties);
            SetPropertyString(loggingInformation, "MessageId", Guid.NewGuid().ToString(), properties);
            SetPropertyString(loggingInformation, "OrgUnitId", userProfile.OrgUnitId, properties);
            SetPropertyString(loggingInformation, "ProcessName", processName, properties);
            SetPropertyString(loggingInformation, "SubProcessName", subProcessName, properties);
            SetPropertyString(loggingInformation, "SubProcessVersion", "0.9", properties);//TODO: Set build version
            SetPropertyString(loggingInformation, "SystemCode", "FOT", properties); //TODO: Set real
            SetPropertyString(loggingInformation, "UserRole", userProfile.RoleName, properties);

            SetPropertyString(loggingInformation, "CountryCode", GetCountryCode(userProfile), properties);

            return loggingInformation;
        }


        public static LogicalLoadCarrierEventLoggingInformation GetLoggingInformationForLoadCarrierEvent(string subProcessName, UserProfile userProfile) 
        {
            var processName = BaseModule.CurrentFlow != null ? BaseModule.CurrentFlow.CurrentProcess.ToString() : "Unknown process";
            Logger.LogAssert(string.IsNullOrEmpty(processName) == false && processName.Length <= 21, "ProcessName length must be 1-21 chars");
            Logger.LogAssert(subProcessName.Length <= 20, "subProcessName max length is 20");

            var loggingInformation = new LogicalLoadCarrierEventLoggingInformation();
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(loggingInformation);

            SetPropertyString(loggingInformation, "TimeFromClient", DateTime.Now, properties);
            SetPropertyString(loggingInformation, "TimeToIntegrationServer", DateTime.Now, properties);
            SetPropertyString(loggingInformation, "TimeDeliveredToInterface", DateTime.Now, properties);
            SetPropertyString(loggingInformation, "ConnectionCode", string.Empty, properties);
            SetPropertyString(loggingInformation, "ConnectionState", string.Empty, properties);
            SetPropertyString(loggingInformation, "ProcessName", processName, properties);
            SetPropertyString(loggingInformation, "SubProcessName", subProcessName, properties);
            SetPropertyString(loggingInformation, "SubProcessVersion", "0.9", properties);
            SetPropertyString(loggingInformation, "Version", "FOT", properties);
            SetPropertyString(loggingInformation, "Guid", Guid.NewGuid().ToString(), properties);
            return loggingInformation;
        }
        



        /// <summary>
        /// Retrieve countrycode from plan if we have that, or else from UserProfile
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        private static string GetCountryCode(UserProfile userProfile)
        {
            string countryCode = null;
            try
            {
                OperationListStop stop = CommandsOperationList.GetStop(ModelMain.SelectedOperationProcess.StopId);
                if (stop != null && stop.StopInformation != null && stop.StopInformation.Address != null)
                    countryCode = stop.StopInformation.Address.CountryCode;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "LoggingInformation.GetCountryCode");
            }
            
            finally
            {
                //In case stop info doesn't have country code filled
                if (String.IsNullOrEmpty(countryCode)) 
                    countryCode = userProfile.CountryCode;
            }
            return countryCode;
        }

        static void SetPropertyString(object loggingInformation, string propertyName, object value, PropertyDescriptorCollection properties)
        {
            PropertyDescriptor property = properties.Find(propertyName, true);
            if (property != null)
            {
                property.SetValue(loggingInformation, value);
            }
            else
            {
                Logger.LogEvent(Severity.Error, "Failed to set logging property '{0}'", propertyName);
            }
        }

        /// <summary>
        /// method  for get logging information..
        /// </summary>
        /// <returns></returns>
        public static OperationListEventLoggingInformation GetOperationListEventLoggingInformation()
        {
            OperationListEventLoggingInformation eventLoggingInformation = null;
#pragma warning disable 618
            if (Application.Instance != null && Application.Instance.Platform != null && Application.Instance.Platform.CurrentNetwork != null)

            {
                eventLoggingInformation = new OperationListEventLoggingInformation
                {
                    ConnectionCode = Convert.ToString(Application.Instance.Platform.CurrentNetwork.Type),
                    ConnectionState = Application.Instance.Communication.IsConnected ? "Connected" : "Disconnected",
                    ProcessName = "OperationList",
                    SubProcessName = string.Empty,
                    SubProcessVersion = string.Empty,
                    TimeFromClient = DateTime.Now,
                    TimeFromClientSpecified = true
                };
#pragma warning restore 618
            }
            return eventLoggingInformation;
        }
    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Devices;

namespace Com.Bring.PMP.PreComFW.Shared.EventHelpers
{
    static public class NoteEventHelper
    {
        /// <summary>
        /// Send Async Register Waybill message
        /// </summary>
        /// <returns></returns>
        public static bool CreateAndSendConsignmentNoteEvent(FotScannerOutput scannerOutput)
        {
            bool sent = false;
            var consignmentNote = CreateConsignmentNoteEvent(scannerOutput);
            if (consignmentNote != null)
            {
                CommunicationClient.Instance.SendConsignmentNoteEvent(consignmentNote);
                sent = true;
            }
            return sent;
        }




        /// <summary>
        /// Create object for Consignment note Event
        /// </summary>
        public static CreateConsignmentNote CreateConsignmentNoteEvent(FotScannerOutput scannerOutput)
        {
            CreateConsignmentNote consignmentNote = null;
            if (scannerOutput != null)
            {
                var pdf = BarcodePdf.Create(scannerOutput.Code);

                consignmentNote = new CreateConsignmentNote
                {
                    PDF417Raw = pdf.PdfString,
                    TimestampFromFOT = DateTime.Now,

                };
            }
            return consignmentNote;
        }
    }
}

﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListEvent;
using Com.Bring.PMP.PreComFW.CommunicationEntity.RequestActualsSummary;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.EventHelpers
{
    public class EventReportHelper
    {
        public static void SendDepartureFromStopEvent(OperationProcess operationProcess, string loadCarrierId, bool departingOperationListStop, Signature signature)
        {
            SendDepartureFromStopEvent(operationProcess, loadCarrierId, departingOperationListStop, null, signature);
        }

        /// <summary>
        /// This method, takes StopId and OperationId as parameter, and sends departure from stop event.
        /// </summary>
        /// <param name="operationProcess">Specifies object for operation process.</param>
        /// <param name="loadCarrierId">Specifies the current load carrier id.</param>
        /// <param name="departingOperationListStop">Specifies bool value which says, is user is departing stop from operation list or not.</param>
        /// <param name="eventCode"> </param>
        /// <param name="signature"> </param>
        public static void SendDepartureFromStopEvent(OperationProcess operationProcess, string loadCarrierId, bool departingOperationListStop, string eventCode, Signature signature)
        {
            PlannedOperationType plannedOperation = null;
            OperationListStop operationListStop = null;
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsArriveAtStop.SendOperationListEventAsyncReq()");

            //If user is departing any stop which is from operation list, then get operation and stop
            if (departingOperationListStop)
            {
                if (ModelOperationList.OperationList != null)
                {
                    plannedOperation = CommandsOperationList.GetPlannedOperation(operationProcess.StopId, operationProcess.OperationId);
                }
                Logger.LogAssert(plannedOperation != null, "PlannedOperationType != null");
                operationListStop = CommandsOperationList.GetStop(operationProcess.StopId);
            }

            var operationListEvent = new OperationListEvent
            {
                CaptureEquipment = OperationListEventHelper.GetOperationListEventCaptureEquipment(),
                EventInformation = OperationListEventHelper.GetOperationListEventInformation(null, plannedOperation, eventCode, signature, StopOperationType.Departed),
                LocationInformation = OperationListEventHelper.GetOperationListEventLocationInformation(operationListStop),
                OperationInformation = OperationListEventHelper.GetOperationListEventOperationInformation(plannedOperation, operationProcess.StopId, operationProcess.TripId, loadCarrierId),
                UserInformation = OperationListEventHelper.GetOperationListEventUserInformation(),
                LoggingInformation = LoggingInformation.GetOperationListEventLoggingInformation(),
            };

#pragma warning disable 618
            if (Application.Instance != null && Application.Instance.Communication != null &&
                    Application.Instance.Communication.GetClient().CredentialId > 0)
#pragma warning restore 618
            {
                var objTran = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.OperationListEvent, "Stop Id:" + operationProcess.StopId);

                CommunicationClient.Instance.SendOperationListEvent(operationListEvent, objTran);
            }
        }

        /// <summary>
        /// This method send Operation finished message (OperationListEvent) to LM or TM
        /// </summary>
        public static void SendOperationFinishedEvent(OperationProcess operationProcess, string loadCarrierId, bool departingOperationListStop, Signature signature, EntityMap entityMap)
        {

            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsDepartureFromStop.SendOperationFinished()");
            string eventCode = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra ? "ORDER" : string.Empty;
            PlannedOperationType plannedOperation = null;
            OperationListStop operationListStop = null;

            //If user is departing any stop which is from operation list, then get operation and stop
            if (departingOperationListStop)
            {
                if (ModelOperationList.OperationList != null)
                {
                    plannedOperation = CommandsOperationList.GetPlannedOperation(operationProcess.StopId, operationProcess.OperationId);
                }
                operationListStop = CommandsOperationList.GetStop(operationProcess.StopId);

            }

            var operationListEvent = new OperationListEvent
            {
                CaptureEquipment = OperationListEventHelper.GetOperationListEventCaptureEquipment(),
                EventInformation = OperationListEventHelper.GetOperationListEventInformation(operationListStop, plannedOperation, eventCode, signature, StopOperationType.Completed),
                LocationInformation = OperationListEventHelper.GetOperationListEventLocationInformation(operationListStop),
                OperationInformation = OperationListEventHelper.GetOperationListEventOperationInformation(plannedOperation, operationProcess.StopId, operationProcess.TripId, loadCarrierId),
                UserInformation = OperationListEventHelper.GetOperationListEventUserInformation(),
                LoggingInformation = LoggingInformation.GetOperationListEventLoggingInformation(),
            };
            //Change Order 084 for Amphora item count should be item count 
            if (entityMap != null && operationListEvent.EventInformation != null &&
                ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora)
            {
                operationListEvent.EventInformation.ConsignmentItemCount = entityMap.ScannedConsignmentItemsTotalCount;
                operationListEvent.EventInformation.ConsignmentItemCountSpecified = true;
            }

#pragma warning disable 618
            if (Application.Instance != null && Application.Instance.Communication != null &&
                    Application.Instance.Communication.GetClient().CredentialId > 0)
#pragma warning restore 618
            {
                var objTran = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.OperationListEvent, "Stop Id:" + operationProcess.StopId);

                CommunicationClient.Instance.SendOperationListEvent(operationListEvent, objTran);
            }
        }

        /// <summary>
        /// This method send Operation finished message (OperationListEvent) to LM or TM
        /// </summary>
        public static void SendAlystraRegisterVasOperationEvent(OperationProcess operationProcess, string customerName, PdaVas vasItem)
        {

            Logger.LogEvent(Severity.Debug, "Executing command EventReportHelper.SendAlystraRegisterVASOperationEvent()");
            const string eventCode = "VAS";
            OperationListStop operationListStop = CommandsOperationList.GetStop(operationProcess.StopId);
            string orderId = string.Empty;
            if (operationListStop != null)
            {
                var operation = (from n in operationListStop.PlannedOperation where n.OperationId == operationProcess.OperationId select n).SingleOrDefault<PlannedOperationType>();
                if (operation != null)
                    orderId = operation.OrderID;
            }
            vasItem.OrderId = orderId;
            var eventInformation = new OperationListEventEventInformation { EventStatusType = vasItem.VasTypeId, EventComment = vasItem.Amount, EventCode = eventCode, EventTime = DateTime.Now };
            var operationListEvent = new OperationListEvent
                {
                    CaptureEquipment = OperationListEventHelper.GetOperationListEventCaptureEquipment(),
                    EventInformation = eventInformation,
                    LocationInformation =
                        OperationListEventHelper.GetOperationListEventLocationInformation(operationListStop),
                    OperationInformation =
                        OperationListEventHelper.GetOperationListEventOperationInformation(null, operationProcess.StopId,
                                                                                      operationProcess.TripId, null),
                    UserInformation = OperationListEventHelper.GetOperationListEventUserInformation(),
                    LoggingInformation = LoggingInformation.GetOperationListEventLoggingInformation()
                };
            operationListEvent.OperationInformation.OrderId = vasItem.OrderId;


#pragma warning disable 618
            if (Application.Instance != null && Application.Instance.Communication != null &&
                  Application.Instance.Communication.GetClient().CredentialId > 0)
#pragma warning restore 618
            {
                var objTran = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.OperationListEvent, "Stop Id:" + operationProcess.StopId);

                CommunicationClient.Instance.SendOperationListEvent(operationListEvent, objTran);
            }
        }

        /// <summary>
        /// This method send Operation Event for Alystra for Add Deviation
        /// </summary>
        public static void SendAlystraAddDeviationOperationEvent(OperationProcess operationProcess)
        {

            Logger.LogEvent(Severity.Debug, "Executing command EventReportHelper.SendAlystraAddDeviationOperationEvent()");
            const string eventCode = "DEV";
            OperationListStop operationListStop = CommandsOperationList.GetStop(operationProcess.StopId);

            var operationListEvent = new OperationListEvent
            {
                CaptureEquipment = OperationListEventHelper.GetOperationListEventCaptureEquipment(),
                EventInformation = OperationListEventHelper.GetOperationListEventInformation(operationListStop, null, eventCode, null, null),
                LocationInformation = OperationListEventHelper.GetOperationListEventLocationInformation(operationListStop),
                OperationInformation = OperationListEventHelper.GetOperationListEventOperationInformation(null, operationProcess.StopId, operationProcess.TripId, null),
                UserInformation = OperationListEventHelper.GetOperationListEventUserInformation(),
                LoggingInformation = LoggingInformation.GetOperationListEventLoggingInformation(),
            };
#pragma warning disable 618
            if (Application.Instance != null && Application.Instance.Communication != null &&
                            Application.Instance.Communication.GetClient().CredentialId > 0)
#pragma warning restore 618
            {
                var objTran = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.OperationListEvent, "Stop Id:" + operationProcess.StopId);

                CommunicationClient.Instance.SendOperationListEvent(operationListEvent, objTran);
            }
        }

        /// <summary>
        /// This function send an actual summary message to Application server.
        /// </summary>
        public static void SendActualsSummaryMessage(string stopId, string tripId, bool departingOperationListStop, EntityMap entityMap, FlowDataDepartureFromStop.ActionTypes actionType)
        {
            OperationListStop stop = null;
            Logger.LogEvent(Severity.Debug, "Executing command GoodsEventHelper.SendActualsSummaryMessage()");
            try
            {
                //get current stop from operation list, if user is departing stop which
                //is in operation list
                if (departingOperationListStop)
                    stop = CommandsOperationList.GetStop(stopId);

                //Prepare object for actual summary
                var actualsSummary = new RequestActualsSummary_Type
                {
                    StopId = stopId,
                    TripId = tripId,
                    LocationId = stop != null && stop.StopInformation != null ? stop.StopInformation.LocationId : String.Empty,
                    TMSAffiliationId = ModelUser.UserProfile.TmsAffiliation
                };

                if (entityMap != null)
                {
                    var eventInformation = GoodsEventHelper.GetGoodsEventsEventInformationForConsignments(entityMap, null);

                    var consignments = GoodsEventHelper.GetConsignmentsForGoodsEvent(entityMap);

                    var consignmentEvents =
                        (from c in consignments
                         where c.EventType == GoodsEventsEventInformationConsignmentEventType.ConsignmentEvent
                         select c).ToList();

                    actualsSummary.ConsignmentEvents = new RequestActualsSummary_TypeConsignmentEvents[consignmentEvents.Count];
                    var consignmentCounter = 0;


                    foreach (var goodsEventsInformationConsignment in consignmentEvents)
                    {
                        actualsSummary.ConsignmentEvents[consignmentCounter] = new RequestActualsSummary_TypeConsignmentEvents
                            {
                                ConsignmentNumber = goodsEventsInformationConsignment.ConsignmentNumber,
                                CustomerId = goodsEventsInformationConsignment.CustomerNumber,
                                ActionType = actionType.ToString(),
                                EventCode = eventInformation.EventCode,
                                OrderNumber = goodsEventsInformationConsignment.OrderNumber,
                            };

                        consignmentCounter += 1;
                    }

                    var consignmentItemEvents =
                        (from c in consignments
                         where c.EventType == GoodsEventsEventInformationConsignmentEventType.ConsignmentItemEvent
                         select c).ToList();

                    var numberOfConsignmentItems =
                        consignments.Where(goodsEventsInformationConsignment => goodsEventsInformationConsignment.ConsignmentItem != null)
                        .Sum(goodsEventsInformationConsignment => goodsEventsInformationConsignment.ConsignmentItem.Length);


                    actualsSummary.ConsignmentItemEvents = new RequestActualsSummary_TypeConsignmentItemEvents[numberOfConsignmentItems];
                    var consignmentItemCounter = 0;

                    foreach (var goodsEventsInformationConsignment in consignmentItemEvents)
                    {
                        foreach (var item in goodsEventsInformationConsignment.ConsignmentItem)
                        {
                            actualsSummary.ConsignmentItemEvents[consignmentItemCounter] =
                                new RequestActualsSummary_TypeConsignmentItemEvents
                                {
                                    ConsignmentNumber = goodsEventsInformationConsignment.ConsignmentNumber,
                                    ConsignmentItemNumber = item.ConsignmentItemNumber,
                                    ActionType = actionType.ToString(),
                                    CustomerId = goodsEventsInformationConsignment.CustomerNumber,
                                    EventCode = eventInformation.EventCode,
                                    OrderNumber = goodsEventsInformationConsignment.OrderNumber
                                };
                            consignmentItemCounter += 1;
                        }
                    }
                }

                CommunicationClient.Instance.SendActualsSummaryEvent(stopId, actualsSummary);

                Logger.LogEvent(Severity.Debug, "GoodsEventHelper.SendActualsSummaryMessage(), Summary message sent");

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "GoodsEventHelper.SendActualsSummaryMessage()");
            }

        }

        /*public static void SendItemDeleteEvent(ConsignmentEntity consignmentEntity)
        {
            Logger.LogEvent(Severity.Debug, "Executing command GoodsEventHelper.DeleteEntity()");
            GoodsEventsOperationInformation earlierOperationInfo = null;


            try
            {
                //Prepare object for user information
                var userInformation = new GoodsEventsUserInformation
                    {
                        FirstName = ModelUser.UserProfile.FirstName,
                        LastName = ModelUser.UserProfile.LastName,
                        UserLogonId = ModelUser.UserProfile.UserId,
                        UserOrgUnitId = ModelUser.UserProfile.OrgUnitId
                    };

                //Prepare object for event information
                if (consignmentEntity.GoodsEvent != null)
                {

                    var eventInformation = new GoodsEventsEventInformation
                        {
                            ActionCode = "",
                            CauseCode = "",
                            EventCode = consignmentEntity.GoodsEvent.EventInformation.EventCode,
                            EventStatus = "S",
                            EventTime = consignmentEntity.GoodsEvent.EventInformation.EventTime,
                            Consignment = consignmentEntity.GoodsEvent.EventInformation.Consignment
                        };
                    //Prepare object for location information
                    var locationInformation = new GoodsEventsLocationInformation
                    {
                        EventOrgUnitId = ModelUser.UserProfile != null ? ModelUser.UserProfile.OrgUnitId : null
                    };

                    //Prepare object for capture equipment information
                    var captureEquipmentInformation = new GoodsEventsCaptureEquipment
                    {
                        CaptureEquipmentId = Application.Instance.Platform.Hardware.Tag,
                        CaptureEquipmentType = ""
                    };

                    //Get operation information from earlier goods event.
                    if (consignmentEntity.GoodsEvent.OperationInformation != null)
                        earlierOperationInfo = consignmentEntity.GoodsEvent.OperationInformation;

                    var operationInformation = new GoodsEventsOperationInformation
                    {
                        ExternalTripId = earlierOperationInfo != null ? earlierOperationInfo.ExternalTripId : string.Empty,
                        LoadCarrier = "",
                        OperationListId = CommandsOperationList.GetOperationListId(),
                        OperationOriginSystem = "",
                        PlanId = "",
                        StopId = earlierOperationInfo != null ? earlierOperationInfo.StopId : string.Empty,
                        TripId = earlierOperationInfo != null ? earlierOperationInfo.TripId : string.Empty,
                        ValueAddedService = null,
                    };



                    var goodsEvent = new PreComFW.CommunicationEntity.GoodsEventHelper.GoodsEventHelper
                    {
                        UserInformation = userInformation,
                        EventInformation = eventInformation,
                        LocationInformation = locationInformation,
                        CaptureEquipment = captureEquipmentInformation,
                        OperationInformation = operationInformation,
                        TMSAffiliationId = ModelUser.UserProfile.TmsAffiliation
                    };


                    var transaction = new Transaction(Guid.NewGuid(),
                                                      (byte)TransactionCategory.GoodsEventsEventInformation,
                                                      consignmentEntity.Id);

                    CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);

                    Logger.LogEvent(Severity.Debug, "Item delete event sent for entered consignment/consignment item");
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "GoodsEventHelper.DeleteEntity");
            }
        }*/
        public static void SendItemDeleteEvent(ConsignmentEntity consignmentEntity)
        {
            Logger.LogEvent(Severity.Debug, "Executing command GoodsEventHelper.DeleteEntity()");

            try
            {

                //Prepare object for event information
                if (consignmentEntity.GoodsEvent != null)
                {

                    var eventInformation = new GoodsEventsEventInformation
                        {
                            ActionCode = consignmentEntity.GoodsEvent.EventInformation.ActionCode,
                            CauseCode = consignmentEntity.GoodsEvent.EventInformation.CauseCode,
                            EventCode = consignmentEntity.GoodsEvent.EventInformation.EventCode,
                            EventStatus = "S",
                            EventTime = consignmentEntity.GoodsEvent.EventInformation.EventTime,
                            Consignment = consignmentEntity.GoodsEvent.EventInformation.Consignment,
                            AdditionalTime = consignmentEntity.GoodsEvent.EventInformation.AdditionalTime,
                            AdditionalTimeSpecified =
                                consignmentEntity.GoodsEvent.EventInformation.AdditionalTimeSpecified,
                            AuthorizationCode = consignmentEntity.GoodsEvent.EventInformation.AuthorizationCode,
                            DamageCode = consignmentEntity.GoodsEvent.EventInformation.DamageCode,
                            EventCodeFromUserProcess =
                                consignmentEntity.GoodsEvent.EventInformation.EventCodeFromUserProcess,
                            EventComment = consignmentEntity.GoodsEvent.EventInformation.EventComment,
                            EventCoordinateX = consignmentEntity.GoodsEvent.EventInformation.EventCoordinateX,
                            EventCoordinateY = consignmentEntity.GoodsEvent.EventInformation.EventCoordinateY,
                            EventCoordinatesystem = consignmentEntity.GoodsEvent.EventInformation.EventCoordinatesystem,
                            EventTemperature = consignmentEntity.GoodsEvent.EventInformation.EventTemperature,
                            EventTemperatureSpecified =
                                consignmentEntity.GoodsEvent.EventInformation.EventTemperatureSpecified,
                            IdentificationDocumentType =
                                consignmentEntity.GoodsEvent.EventInformation.IdentificationDocumentType,
                            OrgUnitIdDeliveredTo = consignmentEntity.GoodsEvent.EventInformation.OrgUnitIdDeliveredTo,
                            RecipientId = consignmentEntity.GoodsEvent.EventInformation.RecipientId,
                            ShelfLocation = consignmentEntity.GoodsEvent.EventInformation.ShelfLocation,
                            Signature = consignmentEntity.GoodsEvent.EventInformation.Signature,
                            TemperatureUnit = consignmentEntity.GoodsEvent.EventInformation.TemperatureUnit,
                            TemperatureZone = consignmentEntity.GoodsEvent.EventInformation.TemperatureZone,
                            TimestampCode = consignmentEntity.GoodsEvent.EventInformation.TimestampCode,
                            UtcTime = consignmentEntity.GoodsEvent.EventInformation.UtcTime,
                            UtcTimeSpecified = consignmentEntity.GoodsEvent.EventInformation.UtcTimeSpecified

                        };

                    var userInformation = consignmentEntity.GoodsEvent.UserInformation;
                    var locationInformation = consignmentEntity.GoodsEvent.LocationInformation;
                    var captureEquipmentInformation = consignmentEntity.GoodsEvent.CaptureEquipment;
                    var operationInformation = consignmentEntity.GoodsEvent.OperationInformation;
                    var loggingInformation = GoodsEventHelper.GetLoggingInformation();
                    var goodsEvent = new GoodsEvents
                    {
                        UserInformation = userInformation,
                        EventInformation = eventInformation,
                        LocationInformation = locationInformation,
                        CaptureEquipment = captureEquipmentInformation,
                        OperationInformation = operationInformation,
                        TMSAffiliationId = ModelUser.UserProfile.TmsAffiliation,
                        LoggingInformation = loggingInformation
                    };


                    var transaction = new Transaction(Guid.NewGuid(),
                                                      (byte)TransactionCategory.GoodsEventsEventInformation,
                                                      consignmentEntity.EntityId);

                    CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);

                    Logger.LogEvent(Severity.Debug, "Item delete event sent for entered consignment/consignment item");
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "GoodsEventHelper.DeleteEntity");
            }
        }
    }
}

﻿using PreCom;
using PreCom.Core;

namespace Com.Bring.PMP.PreComFW.Shared.Model
{
#pragma warning disable 618
    
    public static class ModelModules
    {
        public static BaseModule OperationListModule { get; set; }
        
        private static PlatformBase _platform;
        static public PlatformBase Platform
        {
            get { return _platform ?? (_platform = Application.Platform); }
        }

        private static Application _application;
        static public Application Application
        {
            get { return _application ?? (_application = Application.Instance); }
        }

        private static CoreCollection _devices;
        public static CoreCollection Devices
        {
            get { return _devices ?? (_devices = Application.Devices); }
        }

        private static CommunicationBase _communication;
        static public CommunicationBase Communication
        {
            get { return _communication ?? (_communication = Application.Communication); }
        }

        static public T GetModule<T>() 
        {
            return Application.Modules.Get<T>();
        }
    }

#pragma warning restore 618
}

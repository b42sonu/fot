﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.Model
{
    public static class CommunicationModel
    {
        public static UserInformation UserInformation { get; set; }
        public static string UserLoginId { get; set; }
        public static UserProfileReply UserProfile { get; set; }
        public static string UserProfileSessionId { get; set; }



        #region Operation List Related
        
        public static OperationList OperationList { get; set; }
        public static IList<StubOperationListStop> ListClientOperationStops;
        public static string SelectedStopId { get; set; }
        public static string SelectedPlannedOperationId { get; set; }
        public static string SelectedTripId { get; set; }
        public static string SelectedExternalTripId { get; set; }
        public static string SelectedRoute { get; set; }


        public static bool IsOperationListUpdated { get; set; }
        //public string TabTypeFilter { get;set; }
        public static OperationListTab OperationListTabSelected { get; set; }
        //public bool IsFilterReq { get; set; }
        #endregion

        #region Updated Operation List Related

        public static OperationList UpdatedOperationList { get; set; }
        public static int UpdatedSelectedStatus { get; set; }
        public static string UpdatedSelectedStopId { get; set; }
        public static string UpdatedSelectedOperationId { get; set; }
        public static List<string> StopsToAcceptReject { get; set; }
        public static int UpdatedAcceptCount { get; set; }
        public static int UpdatedRejectCount { get; set; }
        public static IList<StubOperationListStop> UpdatedOperationListStubs { get; set; }
        public static bool UpdatedIsStopSelected { get; set; }
        public static List<PlannedOperationType> UpdatedFilteredOperationsForStop { get; set; }
        public static PlannedConsignmentsType[] UpdatedOperationConsignments { get; set; }
        
        public static List<PackageType> PackageTypes { get; set; }
        public static bool IsNewOperationList { get; set; }
        public static bool IsOnSensitiveScreenOrModule { get; set; }
        //property that accounts if user has logged on
        public static bool IsLoggedOn { get; set; }

        public static bool OperationListExists { get; set; }


        //Loading related
        public static List<LoadingOperation> LoadingOperations { get; set; }
        public static GetGoodsListResponsePlannedStop[] PlannedStops { get; set; }
        public static List<LoadingOperation> LoadingStops { get; set; }
        public static bool LoadingDifferentStop { get; set; }
        public static bool LoadingWithOutStop { get; set; }


        #endregion
    }
}

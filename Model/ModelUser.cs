﻿using System;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core;

namespace Com.Bring.PMP.PreComFW.Shared.Model
{
    public enum SettingChangeType
    {
        ProfileUpdated,
        SettingsSynced
    }

    public static class ModelUser
    {
        public static event Action<SettingChangeType> SettingsChanged;

        public static UserProfile UserProfile
        {
            get { return _userProfile; }
            set
            {
                _userProfile = value;
                if(_userProfile != null)
                    AnnounceChange(SettingChangeType.ProfileUpdated);
            }
        }

        public static string SessionId { get; private set; }
        public static DateTime SessionLogonTime { get; private set; }
        public static DateTime LogonTime = DateTime.MaxValue;
        private static UserProfile _userProfile;

        public static void Clear()
        {
            UserProfile = null;
            ClearSessionId();
        }

        public static bool SetSessionId(string sessionId, DateTime sessionLogonTime)
        {
            try
            {
                if (String.IsNullOrEmpty(SessionId))
                {
                    Logger.LogEvent(Severity.Debug, "New session id received: " + sessionId);
                }
                else if (sessionLogonTime > SessionLogonTime)
                {
                    Logger.LogEvent(Severity.Info, "Session id {0} replaced with session id {1}", SessionId, sessionId);
                }
                else
                {
                    Logger.LogEvent(Severity.Info, "Session id {0} rejected as it is older than current session id {1}", sessionId, SessionId);
                    return false;
                }


                SessionLogonTime = sessionLogonTime;
                SessionId = sessionId;
                SettingDataProvider.Instance.SaveSetting(GlobalConstants.USER_SESSION_ID, sessionId);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ModelUser.SetSessionId");
                return false;
            }
        }

        // Clear on logout
        public static void ClearSessionId()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Session id cleared");
                SessionLogonTime = DateTime.MinValue;
                SessionId = String.Empty;

                SettingDataProvider.Instance.SaveSetting(GlobalConstants.USER_SESSION_ID, string.Empty);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "ModelUser.ClearSessionId");
            }
        }

        public static bool IsLoggedInToTms
        {
            get { return String.IsNullOrEmpty(SessionId) == false; }
        }

        public static string GetSavedSessionId()
        {
            try
            {
                return SettingDataProvider.Instance.GetSetting(GlobalConstants.USER_SESSION_ID);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ModelUser.GetSavedSessionId");
            }
            return String.Empty;
        }

        public class UserSettings : PreComBase
        {
            public const string DisableHelp = "DisableHelp";
            public const string EditHelp = "EditHelp";

            public override bool Initialize()
            {
                return true;
            }

            public override bool Dispose(out bool cancel)
            {
                cancel = false;
                return true;
            }

            public override bool IsInitialized
            {
                get { return true; }
            }

        }

        public static void AnnounceChange(SettingChangeType settingChangeType)
        {
            if (SettingsChanged != null)
                SettingsChanged.Invoke(settingChangeType);
        }
    }
    
    
}

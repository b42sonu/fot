﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Microsoft.WindowsCE.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Model
{
    public class ModelMain
    {
        // ReSharper disable InconsistentNaming
        public const int WM_HANDLE_QUEUED_MESSAGES = 0x8000 + 1;
        // ReSharper restore InconsistentNaming

        //TODO: Move this to appropriate place
        //Is used to avoid that a push message is received and take over the screen. This is typically set in views like signature.
        private static bool _isOnSensitiveScreenOrModule;
        public static bool IsOnSensitiveScreenOrModule
        {
            get { return _isOnSensitiveScreenOrModule; }
            set
            {
                // If going from sensitive screen to non-sensitive, we should trigger queued messages
                if (_isOnSensitiveScreenOrModule && value == false)
                {
                    PostHandleQueuedMessages();
                }
                _isOnSensitiveScreenOrModule = value;
            }
        }

        public static void PostHandleQueuedMessages()
        {
            var message = Message.Create(MessageWindowHandle, WM_HANDLE_QUEUED_MESSAGES, (IntPtr) 0xffff, IntPtr.Zero);
            MessageWindow.PostMessage(ref message);
        }

        // Sometimes you dont want to signal system that sensitive screen has ended (no triggering of processing messages)
        public static void ResetIsOnSensistiveScreenWithoutSignal()
        {
            _isOnSensitiveScreenOrModule = false;
        }

        public static bool IsUnitTesting { get; set; }

        public static IntPtr MessageWindowHandle { get; set; }

        public static List<WorkListItem> WorkListItems { get; set; }

        public static Dictionary<FlowType, IFlowFactory> FlowFactories = new Dictionary<FlowType, IFlowFactory>();
        public static Dictionary<ModuleType, BaseModule> Modules = new Dictionary<ModuleType, BaseModule>();

        public static readonly OperationProcess SelectedOperationProcess = new OperationProcess();

        public static event EventHandler LoginCompleted;
        public static void SignalLoginCompleted()
        {
            if (LoginCompleted != null)
                LoginCompleted(null, null);
        }

        
        static public Action LaunchSettingsFormEvent;

        static public void DoLaunchSettingsForm()
        {
            if (LaunchSettingsFormEvent != null)
            {
                LaunchSettingsFormEvent();
            }
        }
    }
}

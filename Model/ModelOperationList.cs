﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.OperationList;

namespace Com.Bring.PMP.PreComFW.Shared.Model
{
    public class ModelOperationList
    {
        static ModelOperationList()
        {
            NewStops = new List<string>();
        }

        #region Operation List Related

        public static OperationListEx OperationList { get; set; }
        public static IList<BindingStopAmphora> ClientOperationStops { get; set; }
        public static OperationListTab SelectedOperationListTab { get; set; }
        
        public static OperationListEx UpdatedOperationListAmphora { get; set; }
        public static bool IsNewOperationListAmphora { get; set; }
        public static List<string> NewStops { get; set; } // New stops received in push message

        public static int AcceptCountAmphora { get; set; }
        public static int RejectCountAmphora { get; set; }
        
        public static IList<BindingStopAmphora> BindingOperationListAmphora { get; set; }
        public static List<PlannedOperationType> UpdatedFilteredOperationsForStop { get; set; }
        #endregion

        #region "Sorted Operation list Status"
        public static ValidateResortedOperationlistResponse ResortedOperationlistResponse { get; set; }
        #endregion
    }
}

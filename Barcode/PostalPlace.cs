﻿using System;

namespace Com.Bring.PMP.PreComFW.Shared.Barcode
{
	/// <summary>
	/// Summary description for PostOffice
	/// </summary>
	public class PostalPlace 
	{
        public PostalPlace(string postalCode, string countryCode)
        {
            PostalCode = postalCode;
            CountryCode = countryCode;
        }


        public String PostalCode
		{
            get; set;
		}
	    
	    public string CountryCode
	    {
            get; set;
	    }
	}
}


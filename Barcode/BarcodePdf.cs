﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Barcode
{
    public enum PdfType
    {
        Undefined,
        WaybillType1,
        WaybillType2,
        RouteCarrierTrip, //Rute/Bærer/Tur - RBT
        TransportCarrier,
        LoadCarrier,
        AgentExpTerm
        //Underleverandor,
    }

    public enum TripType
    {
        Local,
        LineHaul
    };

    public class BarcodeRouteCarrierTrip
    {
        public static BarcodeRouteCarrierTrip Decode(string barcodeString)
        {
            if (barcodeString.Length < 110)
                throw new Exception("Route-carrier-trip data is shorter than required");

            if (barcodeString.StartsWith("RT01") == false)
                throw new Exception("Unextpected Route-carrier-trip identifier: " + barcodeString.Substring(0, 4));

            var result = new BarcodeRouteCarrierTrip
            {

                TripType = barcodeString[53] == 'L' ? TripType.Local : TripType.LineHaul,
                Direction = barcodeString[31].ToString(CultureInfo.InvariantCulture).TrimEnd(),
                RouteId = barcodeString.Substring(9, 6).TrimEnd(),
                PowerUnitId = barcodeString.Substring(15, 10).TrimEnd(),
                LoadCarrierId = barcodeString.Substring(43, 10).TrimEnd(),

                ExpeditionId = barcodeString.Substring(34, 9).TrimEnd(),
                ExpeditionName = barcodeString.Substring(84, 25).TrimEnd(),
                ExpeditionCountryCode = barcodeString.Substring(32, 2).TrimEnd(),

                AgentId = barcodeString.Substring(4, 5).TrimEnd(),
                AgentName = barcodeString.Substring(54, 30).TrimEnd()
            };

            result.TripId = barcodeString.Substring(25, 6);
            result.TripId = result.TripId.TrimStart('0');


            return result;
        }

        public BarcodeRouteCarrierTrip()
        {

        }

        public BarcodeRouteCarrierTrip(string routeId, string tripId, string powerUnitId, string loadCarrierId)
        {
            RouteId = routeId;
            TripId = tripId;
            PowerUnitId = powerUnitId;
            LoadCarrierId = loadCarrierId;
        }

        public string RouteId { get; set; }
        public string TripId { get; set; }
        public TripType TripType { get; set; }
        public string Direction { get; private set; }

        public string PowerUnitId { get; set; }
        public string LoadCarrierId { get; set; }

        public string ExpeditionId { get; private set; }
        public string ExpeditionName { get; private set; }
        public string ExpeditionCountryCode { get; private set; }

        public string AgentId { get; private set; }
        public string AgentName { get; private set; }
    };

    public class BarcodeTransportCarrier
    {
        public static BarcodeTransportCarrier Decode(string barcodeString)
        {
            if (barcodeString.Length < 54)
                throw new Exception("Route-carrier-trip data is shorter than required");

            if (barcodeString.StartsWith("TB01") == false)
                throw new Exception("Unextpected Route-carrier-trip identifier: " + barcodeString.Substring(0, 4));

            var result = new BarcodeTransportCarrier
                {
                    TransportCarrierId = barcodeString.Substring(4, 10).TrimEnd(),
                    HaveLoadCapacity = barcodeString[14] == 'J',
                    DriverId = barcodeString.Substring(15, 9).TrimEnd(),
                    DriverName = barcodeString.Substring(24, 30).TrimEnd(),
                };
            return result;
        }


        public string TransportCarrierId { get; private set; }
        public bool HaveLoadCapacity { get; private set; }
        public string DriverId { get; private set; }

        public string DriverName { get; private set; }
    };

    public class BarcodeLoadCarrier
    {
        public static BarcodeLoadCarrier Decode(string barcodeString)
        {
            // Load carrier from PDF
            if (barcodeString.StartsWith("LB01"))
            {
                if (barcodeString.Length < 14)
                    throw new Exception("Route-carrier-trip data is shorter than required");

                return new BarcodeLoadCarrier { LoadCarrierId = barcodeString.Substring(4, 10).TrimEnd() };
            }

            // Posten load carrier
            return new BarcodeLoadCarrier { LoadCarrierId = barcodeString };
        }


        public string LoadCarrierId { get; set; }
    };

    public class BarcodeAgentExp
    {
        public static BarcodeAgentExp Decode(string barcodeString)
        {
            var result = new BarcodeAgentExp();
            try
            {
                result.AgentNr = barcodeString.Substring(4, 5).TrimEnd();
                result.ExpCountryCode = barcodeString.Substring(9, 2).TrimEnd();
                result.ExpId = barcodeString.Substring(11, 9).TrimEnd();
                result.TerminalNr = barcodeString.Substring(20, 3).TrimEnd();
                result.Placement = barcodeString.Substring(23, 3).TrimEnd();
                result.AgentName = barcodeString.Substring(26, 30).TrimEnd();
                result.ExpName = barcodeString.Substring(56, 25).TrimEnd();
                result.TerminalName = barcodeString.Substring(81, 25).TrimEnd();
                result.PlacementName = barcodeString.Substring(106, 25).TrimEnd();
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BarcodePdf.DecodeAgentExp");
                return null;
            }
        }


        public string AgentNr { get; private set; }
        public string ExpCountryCode { get; private set; }
        public string ExpId { get; private set; }
        public string TerminalNr { get; private set; }
        public string Placement { get; private set; }
        public string AgentName { get; private set; }
        public string ExpName { get; private set; }
        public string TerminalName { get; private set; }
        public string PlacementName { get; private set; }
    };

    public class BarcodePdf
    {

        // TODO: Translate all entries from Norwegian
        private enum WaybillEntry
        {
            // Activate faste feltposisjoner. Startindeks og lengde på felt må brukas.
            Identifier,
            WaybillId,
            VersionNr,
            ConsignmentItemId,
            AgentId,
            RecipientCountryCode,
            RecipientPostalCode,
            NumberOfItems,
            TotalGrossWeight,
            TotalVolume,
            ProductTypeId,
            ProductGroup,
            Payee,
            TransportCode,
            WaybillTransportKodeB,
            CustomsGoods,
            CurrencyCode,
            HazardousGoods,
            HazardCode,
            Local,
            TpConsignorLocation,
            TpConsigneeLocation,
            Insured,
            RegisteredDate,
            SequenceNrRecipientLocation,
            // Activate semikolon separerade felt. Størrelse på felt ikke intressant.
            OrderNr,
            AvsenderKundeNr,
            AvtaleNr,
            AntallEuroPaller,
            GodslisteNr,
            ConsigneeName,
            ConsigneeAdress1,
            ConsigneeAdress2,
            ConsigneeCountryCode,
            ConsigneePostalNr,
            MottakerKundeNrHosSender,
            ConsignorName,
            ConsignorAddress1,
            ConsignorAddress2,
            ConsignorCountryCode,
            ConsignorPostalNr,
            Anmerkning1,
            Anmerkning2,
            Anmerkning3,
            Anmerkning4,
            Anmerkning5,
            Anmerkning6,
            Anmerkning7,
            Anmerkning8,
            MottakerLeveringsAdresse1,
            MottakerLeveringsAdresse2,
            TransportKodeA,
            BelopHvisUtleveringsForbehold,
            BelopHvisSenderEtterkrav,
            ForsikringsVerdi,
            EvMottakersKundeNrHosNc,
            EvMottakersAgentNrHosNc,
            TredjePartBetalersAgentNr,
            TredjePartBetalersKundeNrHosNc,
            MottakersReferanse,
            LeveringsInstruks,
            PoliseNr,
            GiroNr,
            TollNummer,
            FareunNr,
            // -- Linjedata forekommer 1 til 8 ganger. Minst en linje må fylles ut.
            // -- Linjedata 1
            GodsMerking1,
            Emballasje1,
            AntallKolli1,
            Bruttovekt1,
            Volum1,
            Lengde1,
            Bredde1,
            Hoyde1,
            AntallPaller1,
            LasteMeter1,
            FarligtGodsVarelinje1,
            SperreGodsVarelinje1,
            DekksLastVarelinje1,
            AntallLbh1,
            // -- Linjedata 2
            GodsMerking2,
            Emballasje2,
            AntallKolli2,
            Bruttovekt2,
            Volum2,
            Lengde2,
            Bredde2,
            Hoyde2,
            AntallPaller2,
            LasteMeter2,
            FarligtGodsVarelinje2,
            SperreGodsVarelinje2,
            DekksLastVarelinje2,
            AntallLbh2,
            // -- Linjedata 3
            GodsMerking3,
            Emballasje3,
            AntallKolli3,
            Bruttovekt3,
            Volum3,
            Lengde3,
            Bredde3,
            Hoyde3,
            AntallPaller3,
            LasteMeter3,
            FarligtGodsVarelinje3,
            SperreGodsVarelinje3,
            DekksLastVarelinje3,
            AntallLbh3,
            // -- Linjedata 4
            GodsMerking4,
            Emballasje4,
            AntallKolli4,
            Bruttovekt4,
            Volum4,
            Lengde4,
            Bredde4,
            Hoyde4,
            AntallPaller4,
            LasteMeter4,
            FarligtGodsVarelinje4,
            SperreGodsVarelinje4,
            DekksLastVarelinje4,
            AntallLbh4,
            // -- Linjedata 5
            GodsMerking5,
            Emballasje5,
            AntallKolli5,
            Bruttovekt5,
            Volum5,
            Lengde5,
            Bredde5,
            Hoyde5,
            AntallPaller5,
            LasteMeter5,
            FarligtGodsVarelinje5,
            SperreGodsVarelinje5,
            DekksLastVarelinje5,
            AntallLbh5,
            // -- Linjedata 6
            GodsMerking6,
            Emballasje6,
            AntallKolli6,
            Bruttovekt6,
            Volum6,
            Lengde6,
            Bredde6,
            Hoyde6,
            AntallPaller6,
            LasteMeter6,
            FarligtGodsVarelinje6,
            SperreGodsVarelinje6,
            DekksLastVarelinje6,
            AntallLbh6,
            // -- Linjedata 7
            GodsMerking7,
            Emballasje7,
            AntallKolli7,
            Bruttovekt7,
            Volum7,
            Lengde7,
            Bredde7,
            Hoyde7,
            AntallPaller7,
            LasteMeter7,
            FarligtGodsVarelinje7,
            SperreGodsVarelinje7,
            DekksLastVarelinje7,
            AntallLbh7,
            // -- Linjedata 8
            GodsMerking8,
            Emballasje8,
            AntallKolli8,
            Bruttovekt8,
            Volum8,
            Lengde8,
            Bredde8,
            Hoyde8,
            AntallPaller8,
            LasteMeter8,
            FarligtGodsVarelinje8,
            SperreGodsVarelinje8,
            DekksLastVarelinje8,
            AntallLbh8
        }

        public static BarcodePdf Create(string barcodeString)
        {
            var pdf = new BarcodePdf(barcodeString);
            if (pdf.PdfType != PdfType.Undefined)
                return pdf;

            return null;
        }

        // Only create tthrough factory method Decode()
        private BarcodePdf(string barcodeString)
        {
            _data = barcodeString;
            InitPdfType();

            if (PdfType == PdfType.WaybillType1 || PdfType == PdfType.WaybillType2)
                UnpackData();
        }

        private void UnpackData()
        {
            try
            {
                var stringBuf = new StringBuilder(1600);

                // Retireve fixed length data
                var fieldPos = WaybillFieldsLength[WaybillEntry.OrderNr]; // Lst fixed field
                int fixedFieldsEndIndex = fieldPos.Start;
                stringBuf.Append(_data.Substring(0, fixedFieldsEndIndex));


                int dataIndex = fixedFieldsEndIndex + 1; // Skip the semicolon after last fixed entry
                for (var consignmentEntry = WaybillEntry.OrderNr; consignmentEntry <= WaybillEntry.AntallLbh8; consignmentEntry++)
                {
                    stringBuf.Append(GetVariableField(consignmentEntry, ref dataIndex));
                }
                _data = stringBuf.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BarcodePdf.UnpackData");
            }
        }

        private string GetVariableField(WaybillEntry waybillEntry, ref int dataIndex)
        {
            var fieldPos = WaybillFieldsLength[waybillEntry];
            string field;
            if (dataIndex < _data.Length)
            {
                int length = _data.IndexOf(';', dataIndex) - dataIndex;
                field = _data.Substring(dataIndex, length);
                field = field.Length > fieldPos.Length ? field.Substring(0, fieldPos.Length) : field.PadRight(fieldPos.Length);
                dataIndex += length + 1;
            }
            else
            {
                field = string.Empty.PadRight(fieldPos.Length);
            }
            return field;
        }

        public string PdfString
        {
            get { return _data; }
        }

        public PdfType PdfType { get; private set; }

        /// <summary>
        /// Decode BarcodeType of pdf. Throws exception on unexpected data-format and on unknown pdf-BarcodeType
        /// </summary>
        private void InitPdfType()
        {
            if (_data != null && _data.Length >= 8)
            {
                string pdfId = _data.Substring(0, 4);
                PdfType pdfType;
                if (IdentityTable.TryGetValue(pdfId, out pdfType))
                {
                    PdfType = pdfType;
                }
            }
        }

        private string _data;

        private static readonly Dictionary<string, PdfType> IdentityTable = InitIdentityTable();
        private static readonly Dictionary<WaybillEntry, FieldPos> WaybillFieldsLength = InitWaybilFieldsLength();

        private struct FieldPos
        {
            private static int _indexer;
            public readonly int Start;
            public readonly int Length;

            public FieldPos(int length)
            {
                Start = _indexer;
                Length = length;
                _indexer += length;
            }
        }

        private static Dictionary<string, PdfType> InitIdentityTable()
        {
            var identityTable = new Dictionary<string, PdfType>
            {
                {"NC01", PdfType.WaybillType1},
                {"NC02", PdfType.WaybillType2},
                {"RT01", PdfType.RouteCarrierTrip},
                {"TB01", PdfType.TransportCarrier},
                {"LB01", PdfType.LoadCarrier},
                {"AE01", PdfType.AgentExpTerm},
                //{"UL01", PdfType.Underleverandor}
            };
            return identityTable;
        }



        private static Dictionary<WaybillEntry, FieldPos> InitWaybilFieldsLength()
        {
            var waybillFieldsLength = new Dictionary<WaybillEntry, FieldPos>
            {
                {WaybillEntry.Identifier, new FieldPos(4)},
                {WaybillEntry.WaybillId, new FieldPos(17)},
                {WaybillEntry.VersionNr, new FieldPos(2)},
                {WaybillEntry.ConsignmentItemId, new FieldPos(18)},
                {WaybillEntry.AgentId, new FieldPos(5)},
                {WaybillEntry.RecipientCountryCode, new FieldPos(2)},
                {WaybillEntry.RecipientPostalCode, new FieldPos(9)},
                {WaybillEntry.NumberOfItems, new FieldPos(5)},
                {WaybillEntry.TotalGrossWeight, new FieldPos(6)},
                {WaybillEntry.TotalVolume, new FieldPos(6)},
                {WaybillEntry.ProductTypeId, new FieldPos(1)},
                {WaybillEntry.ProductGroup, new FieldPos(4)},
                {WaybillEntry.Payee, new FieldPos(1)},
                {WaybillEntry.TransportCode, new FieldPos(1)},
                {WaybillEntry.WaybillTransportKodeB, new FieldPos(1)},
                {WaybillEntry.CustomsGoods, new FieldPos(1)},
                {WaybillEntry.CurrencyCode, new FieldPos(3)},
                {WaybillEntry.HazardousGoods, new FieldPos(1)},
                {WaybillEntry.HazardCode, new FieldPos(1)},
                {WaybillEntry.Local, new FieldPos(1)},
                {WaybillEntry.TpConsignorLocation, new FieldPos(1)},
                {WaybillEntry.TpConsigneeLocation, new FieldPos(1)},
                {WaybillEntry.Insured, new FieldPos(1)},
                {WaybillEntry.RegisteredDate, new FieldPos(8)},
                {WaybillEntry.SequenceNrRecipientLocation, new FieldPos(1)},
                
                // Variable length fields from PDF
                {WaybillEntry.OrderNr, new FieldPos(15)},
                {WaybillEntry.AvsenderKundeNr, new FieldPos(8)},
                {WaybillEntry.AvtaleNr, new FieldPos(6)},
                {WaybillEntry.AntallEuroPaller, new FieldPos(3)},
                {WaybillEntry.GodslisteNr, new FieldPos(6)},
                {WaybillEntry.ConsigneeName, new FieldPos(30)},
                {WaybillEntry.ConsigneeAdress1, new FieldPos(30)},
                {WaybillEntry.ConsigneeAdress2, new FieldPos(30)},
                {WaybillEntry.ConsigneeCountryCode, new FieldPos(2)},
                {WaybillEntry.ConsigneePostalNr, new FieldPos(9)},
                {WaybillEntry.MottakerKundeNrHosSender, new FieldPos(8)},
                {WaybillEntry.ConsignorName, new FieldPos(30)},
                {WaybillEntry.ConsignorAddress1, new FieldPos(30)},
                {WaybillEntry.ConsignorAddress2, new FieldPos(30)},
                {WaybillEntry.ConsignorCountryCode, new FieldPos(2)},
                {WaybillEntry.ConsignorPostalNr, new FieldPos(9)},
                {WaybillEntry.Anmerkning1, new FieldPos(34)},
                {WaybillEntry.Anmerkning2, new FieldPos(34)},
                {WaybillEntry.Anmerkning3, new FieldPos(34)},
                {WaybillEntry.Anmerkning4, new FieldPos(34)},
                {WaybillEntry.Anmerkning5, new FieldPos(34)},
                {WaybillEntry.Anmerkning6, new FieldPos(34)},
                {WaybillEntry.Anmerkning7, new FieldPos(34)},
                {WaybillEntry.Anmerkning8, new FieldPos(34)},
                {WaybillEntry.MottakerLeveringsAdresse1, new FieldPos(30)},
                {WaybillEntry.MottakerLeveringsAdresse2, new FieldPos(30)},
                {WaybillEntry.TransportKodeA, new FieldPos(3)},
                {WaybillEntry.BelopHvisUtleveringsForbehold, new FieldPos(7)},
                {WaybillEntry.BelopHvisSenderEtterkrav, new FieldPos(7)},
                {WaybillEntry.ForsikringsVerdi, new FieldPos(7)},
                {WaybillEntry.EvMottakersKundeNrHosNc, new FieldPos(8)},
                {WaybillEntry.EvMottakersAgentNrHosNc, new FieldPos(5)},
                {WaybillEntry.TredjePartBetalersAgentNr, new FieldPos(5)},
                {WaybillEntry.TredjePartBetalersKundeNrHosNc, new FieldPos(8)},
                {WaybillEntry.MottakersReferanse, new FieldPos(15)},
                {WaybillEntry.LeveringsInstruks, new FieldPos(30)},
                {WaybillEntry.PoliseNr, new FieldPos(11)},
                {WaybillEntry.GiroNr, new FieldPos(11)},
                {WaybillEntry.TollNummer, new FieldPos(18)},
                {WaybillEntry.FareunNr, new FieldPos(100)},
                // -- Linjedata forekommer 1 til 8 ganger. Minst en linje må fylles ut.
                // -- Linjedata 1
                {WaybillEntry.GodsMerking1, new FieldPos(10)},
                {WaybillEntry.Emballasje1, new FieldPos(25)},
                {WaybillEntry.AntallKolli1, new FieldPos(4)},
                {WaybillEntry.Bruttovekt1, new FieldPos(6)},
                {WaybillEntry.Volum1, new FieldPos(6)},
                {WaybillEntry.Lengde1, new FieldPos(3)},
                {WaybillEntry.Bredde1, new FieldPos(3)},
                {WaybillEntry.Hoyde1, new FieldPos(3)},
                {WaybillEntry.AntallPaller1, new FieldPos(4)},
                {WaybillEntry.LasteMeter1, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje1, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje1, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje1, new FieldPos(1)},
                {WaybillEntry.AntallLbh1, new FieldPos(3)},
                // -- Linjedata 2
                {WaybillEntry.GodsMerking2, new FieldPos(10)},
                {WaybillEntry.Emballasje2, new FieldPos(25)},
                {WaybillEntry.AntallKolli2, new FieldPos(4)},
                {WaybillEntry.Bruttovekt2, new FieldPos(6)},
                {WaybillEntry.Volum2, new FieldPos(6)},
                {WaybillEntry.Lengde2, new FieldPos(3)},
                {WaybillEntry.Bredde2, new FieldPos(3)},
                {WaybillEntry.Hoyde2, new FieldPos(3)},
                {WaybillEntry.AntallPaller2, new FieldPos(4)},
                {WaybillEntry.LasteMeter2, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje2, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje2, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje2, new FieldPos(1)},
                {WaybillEntry.AntallLbh2, new FieldPos(3)},
                // -- Linjedata 3
                {WaybillEntry.GodsMerking3, new FieldPos(10)},
                {WaybillEntry.Emballasje3, new FieldPos(25)},
                {WaybillEntry.AntallKolli3, new FieldPos(4)},
                {WaybillEntry.Bruttovekt3, new FieldPos(6)},
                {WaybillEntry.Volum3, new FieldPos(6)},
                {WaybillEntry.Lengde3, new FieldPos(3)},
                {WaybillEntry.Bredde3, new FieldPos(3)},
                {WaybillEntry.Hoyde3, new FieldPos(3)},
                {WaybillEntry.AntallPaller3, new FieldPos(4)},
                {WaybillEntry.LasteMeter3, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje3, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje3, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje3, new FieldPos(1)},
                {WaybillEntry.AntallLbh3, new FieldPos(3)},
                // -- Linjedata 4
                {WaybillEntry.GodsMerking4, new FieldPos(10)},
                {WaybillEntry.Emballasje4, new FieldPos(25)},
                {WaybillEntry.AntallKolli4, new FieldPos(4)},
                {WaybillEntry.Bruttovekt4, new FieldPos(6)},
                {WaybillEntry.Volum4, new FieldPos(6)},
                {WaybillEntry.Lengde4, new FieldPos(3)},
                {WaybillEntry.Bredde4, new FieldPos(3)},
                {WaybillEntry.Hoyde4, new FieldPos(3)},
                {WaybillEntry.AntallPaller4, new FieldPos(4)},
                {WaybillEntry.LasteMeter4, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje4, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje4, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje4, new FieldPos(1)},
                {WaybillEntry.AntallLbh4, new FieldPos(3)},
                // -- Linjedata 5
                {WaybillEntry.GodsMerking5, new FieldPos(10)},
                {WaybillEntry.Emballasje5, new FieldPos(25)},
                {WaybillEntry.AntallKolli5, new FieldPos(4)},
                {WaybillEntry.Bruttovekt5, new FieldPos(6)},
                {WaybillEntry.Volum5, new FieldPos(6)},
                {WaybillEntry.Lengde5, new FieldPos(3)},
                {WaybillEntry.Bredde5, new FieldPos(3)},
                {WaybillEntry.Hoyde5, new FieldPos(3)},
                {WaybillEntry.AntallPaller5, new FieldPos(4)},
                {WaybillEntry.LasteMeter5, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje5, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje5, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje5, new FieldPos(1)},
                {WaybillEntry.AntallLbh5, new FieldPos(3)},
                // -- Linjedata 6
                {WaybillEntry.GodsMerking6, new FieldPos(10)},
                {WaybillEntry.Emballasje6, new FieldPos(25)},
                {WaybillEntry.AntallKolli6, new FieldPos(4)},
                {WaybillEntry.Bruttovekt6, new FieldPos(6)},
                {WaybillEntry.Volum6, new FieldPos(6)},
                {WaybillEntry.Lengde6, new FieldPos(3)},
                {WaybillEntry.Bredde6, new FieldPos(3)},
                {WaybillEntry.Hoyde6, new FieldPos(3)},
                {WaybillEntry.AntallPaller6, new FieldPos(4)},
                {WaybillEntry.LasteMeter6, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje6, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje6, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje6, new FieldPos(1)},
                {WaybillEntry.AntallLbh6, new FieldPos(3)},
                // -- Linjedata 7
                {WaybillEntry.GodsMerking7, new FieldPos(10)},
                {WaybillEntry.Emballasje7, new FieldPos(25)},
                {WaybillEntry.AntallKolli7, new FieldPos(4)},
                {WaybillEntry.Bruttovekt7, new FieldPos(6)},
                {WaybillEntry.Volum7, new FieldPos(6)},
                {WaybillEntry.Lengde7, new FieldPos(3)},
                {WaybillEntry.Bredde7, new FieldPos(3)},
                {WaybillEntry.Hoyde7, new FieldPos(3)},
                {WaybillEntry.AntallPaller7, new FieldPos(4)},
                {WaybillEntry.LasteMeter7, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje7, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje7, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje7, new FieldPos(1)},
                {WaybillEntry.AntallLbh7, new FieldPos(3)},
                // -- Linjedata 8
                {WaybillEntry.GodsMerking8, new FieldPos(10)},
                {WaybillEntry.Emballasje8, new FieldPos(25)},
                {WaybillEntry.AntallKolli8, new FieldPos(4)},
                {WaybillEntry.Bruttovekt8, new FieldPos(6)},
                {WaybillEntry.Volum8, new FieldPos(6)},
                {WaybillEntry.Lengde8, new FieldPos(3)},
                {WaybillEntry.Bredde8, new FieldPos(3)},
                {WaybillEntry.Hoyde8, new FieldPos(3)},
                {WaybillEntry.AntallPaller8, new FieldPos(4)},
                {WaybillEntry.LasteMeter8, new FieldPos(3)},
                {WaybillEntry.FarligtGodsVarelinje8, new FieldPos(1)},
                {WaybillEntry.SperreGodsVarelinje8, new FieldPos(1)},
                {WaybillEntry.DekksLastVarelinje8, new FieldPos(1)},
                {WaybillEntry.AntallLbh8, new FieldPos(3)}
            };

            return waybillFieldsLength;
        }

        private string GetWaybillField(WaybillEntry field)
        {
            string result = string.Empty;
            FieldPos value;
            if (WaybillFieldsLength.TryGetValue(field, out value))
            {
                result = _data.Substring(value.Start, value.Length).Trim();
            }
            return result;
        }


        public PdfWaybill PdfWaybill
        {
            get
            {
                var pdfWaybill = new PdfWaybill
                {
                    ConsignmentId = GetWaybillField(WaybillEntry.WaybillId),
                    NumberOfItems = Convert.ToInt32(GetWaybillField(WaybillEntry.NumberOfItems)),
                    ConsignmentItemId = GetWaybillField(WaybillEntry.ConsignmentItemId),
                    ConsignmentOrderNr = GetWaybillField(WaybillEntry.OrderNr),
                };

                //pdfWaybill.Consignor.Name = GetWaybillField(WaybillEntry.ConsignorName);
                //pdfWaybill.Consignor.Adress1 = GetWaybillField(WaybillEntry.ConsignorAddress1);
                //pdfWaybill.Consignor.Adress2 = GetWaybillField(WaybillEntry.ConsignorAddress2);
                //pdfWaybill.Consignor.PostalPlace = new PostalPlace(GetWaybillField(WaybillEntry.ConsignorPostalNr), GetWaybillField(WaybillEntry.ConsignorCountryCode));

                //pdfWaybill.Consignee.Name = GetWaybillField(WaybillEntry.ConsigneeName);
                //pdfWaybill.Consignee.Adress1 = GetWaybillField(WaybillEntry.ConsigneeAdress1);
                //pdfWaybill.Consignee.Adress2 = GetWaybillField(WaybillEntry.ConsigneeAdress2);
                //pdfWaybill.Consignee.PostalPlace =
                //    new PostalPlace(GetWaybillField(WaybillEntry.ConsigneePostalNr), GetWaybillField(WaybillEntry.ConsigneeCountryCode));

                //pdfWaybill.Width = GetWaybillField(WaybillEntry.Bredde1);
                //pdfWaybill.Height = GetWaybillField(WaybillEntry.Hoyde1);
                //pdfWaybill.Length = GetWaybillField(WaybillEntry.Lengde1);
                //pdfWaybill.Weight = GetWaybillField(WaybillEntry.Bruttovekt1);
                //pdfWaybill.Volume = GetWaybillField(WaybillEntry.Volum1);

                return pdfWaybill;
            }
        }
    }
}

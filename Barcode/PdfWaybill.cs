﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Barcode
{
    /// <summary>
    /// Represents the PdfWaybill entity.
    /// </summary>
    public class PdfWaybill : EntityBase
    {
        /// <summary>
        /// Gets or sets the consignment id
        /// </summary>
        public string ConsignmentId { get; set; }

        /// <summary>
        /// Gets or sets the number of parcels in consignment
        /// </summary>
        public int NumberOfItems { get; set; }

        /// <summary>
        /// Gets or sets the id of the parcel refereded by this containment label
        /// </summary>
        public string ConsignmentItemId { get; set; }

        /// <summary>
        /// Gets or sets the order number of the parcel refereded by this containment label
        /// </summary>
        public string ConsignmentOrderNr { get; set; }

        /// <summary>
        /// Gets or sets the sender of consignment
        /// </summary>
        private Adress _consignor;
        public Adress Consignor
        {
            get
            {
                _consignor = _consignor ?? new Adress();
                return _consignor;
            }
        }

        /// <summary>
        /// Gets or sets the recipient of consignment
        /// </summary>
        private Adress _consignee;
        public Adress Consignee
        {
            get
            {
                _consignee = _consignee ?? new Adress();
                return _consignee;
            }
        }

        /// <summary>
        /// Gets or sets the width for the containment
        /// </summary>
        public string Width { get; set; }

        /// <summary>
        /// Gets or sets the height for the containment
        /// </summary>
        public string Height { get; set; }

        /// <summary>
        /// Gets or sets the length for the containment
        /// </summary>
        public string Length { get; set; }

        /// <summary>
        /// Gets or sets the volume for the containment
        /// </summary>
        public string Volume { get; set; }

        /// <summary>
        /// Gets or sets the weight for the containment
        /// </summary>
        public string Weight { get; set; }

        /// <summary>
        /// /// Gets or sets the placement
        /// </summary>
        public string Placement { get; set; }

        ///// <summary>
        ///// Gets or sets the value added services.
        ///// </summary>
        ///// <value>The value added services.</value>
        ////public ValueAddedService[] ValueAddedServices { get; set; }
    }
}

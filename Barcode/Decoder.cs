﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Text.RegularExpressions;
using Symbol.Barcode;

namespace Com.Bring.PMP.PreComFW.Shared.Barcode
{
    public class Decoder
    {
        private const string PruConsignmentIdentifier = "07072185";
        private const string PruConsignmentItemIdentifier = "37072185";


        //private const string IdentifierPostalNumber = "5L";
        //private const string IdentifierProductCode = "2W";
        //private const string IdentifierVasCode = "4W";
        //private const string IdentifierCustomerNumber = "9V";

        public static BarcodeData Decode(FotScannerOutput scannerOutput)
        {
            Logger.LogEvent(Severity.Debug, "Decoding barcode '{0}' of type {1}", scannerOutput.Code.Replace(";", "/"), scannerOutput.Type.ToString());
            
            var decoder = new Decoder(scannerOutput);
            if (decoder.Decode())
                return decoder._barcodeData;

            Logger.LogEvent(Severity.Warning, "Failed to decode barcode '{0}' of type {1}", scannerOutput.Code.Replace(";", "/"), scannerOutput.Type.ToString());
            return new BarcodeData();
        }


        // Only create through factory method
        private Decoder(FotScannerOutput scannerOutput)
        {
            _scannerOutput = scannerOutput;
        }

        private BarcodeData _barcodeData;
        private readonly FotScannerOutput _scannerOutput;

        private DecoderTypes ManualInput
        {
            get { return 0; }
        }

        private bool Decode()
        {
            if (_scannerOutput.Code != null)
            {
                try
                {
                    if (_scannerOutput.Type == ManualInput)
                    {
                        _scannerOutput.Code = _scannerOutput.Code.ToUpper();
                    }

                    if (DecodePdf())
                        return true;
                    if (DecodeGs1())
                        return true;
                    if (DecodeCode39())
                        return true;
                    if (DecodeCode128())
                        return true;
                    if (DecodeCargo())
                        return true;
                    if (DecodeSpecialJShipment())
                        return true;
                    if (DecodeConsignment())
                        return true;
                    if (DecodeDeliveryCode())
                        return true;
                    if (DecodeDriverLicence())
                        return true;

                    if (DecodeFrigoConsignment())
                        return true;
                }
                catch (Exception e)
                {
                    Logger.LogException(e, "Decoder.Decode");
                }
            }
            return false;
        }

        /// <summary>
        /// Decodes data if the scanned barcode was a pdf
        /// </summary>
        private bool DecodePdf()
        {
            bool result = false;
            try
            {
                if (_scannerOutput.Type == DecoderTypes.PDF417)
                {
                    BarcodePdf pdf = BarcodePdf.Create(_scannerOutput.Code);
                    if (pdf != null)
                    {
                        _barcodeData = new BarcodeData(pdf);
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodePdf");
            }
            return result;
        }

        /// <summary>
        /// Code 128 application standard GS1 - Spec 3.4
        /// 
        /// Format 01 : 00##################
        /// Length: 20
        /// Prefix length: 2
        /// Indicator length: 0
        /// Prefix must contain 00
        /// Manual input may omit prefix and hence must be added, length is 18 if optional missing) 
        /// </summary>
        private bool DecodeGs1()
        {
            bool result = false;

            try
            {
                var scannerData = _scannerOutput.Code;

                /* Manuell inntasting indikeres med 00*/
                if (_scannerOutput.Type != DecoderTypes.PDF417)
                {
                    if (scannerData.Length == 18)
                        scannerData = "00" + scannerData;
                    if (scannerData.Length == 20)
                    {
                        string indikator = scannerData.Substring(0, 2);
                        if (indikator == "00")
                        {
                            string data = scannerData.Substring(2, 17);
                            char checkSum = scannerData[19];
                            result = Checksum.IsValidCheckSumGs1(data, checkSum);
                            if (result)
                            {
                                string content = scannerData.Substring(2);
                                if (content.StartsWith(PruConsignmentIdentifier))
                                {
                                    _barcodeData = new BarcodeData(content, _scannerOutput.Type, BarcodeType.ConsignmentNumber,
                                        BarcodeSubType.PruTracking);
                                }
                                else if (content.StartsWith(PruConsignmentItemIdentifier))
                                {
                                    _barcodeData = new BarcodeData(content, _scannerOutput.Type, BarcodeType.ConsignmentItemNumber,
                                        BarcodeSubType.PruTracking);
                                    _barcodeData.ConsignmentId = ConsignmentEntity.UndefinedConsignmentId;
                                }
                                else
                                    _barcodeData = new BarcodeData(content, _scannerOutput.Type, BarcodeType.ConsignmentItemNumber);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeGs1");
            }
            return result;
        }


        /// <summary>
        /// Code 39 - Spec 3.2
        /// 
        /// Format 08: @@#########$$
        /// Length: 13
        /// Prefix length: 0
        /// Indicator length: 2 (Alpha only designates a consignment item)
        /// </summary>
        protected bool DecodeCode39()
        {
            bool result = false;
            try
            {
                if (_scannerOutput.Type == DecoderTypes.CODE39 || _scannerOutput.Type == DecoderTypes.CODE128 || _scannerOutput.Type == ManualInput)
                {
                    if (DecodeShelf(_scannerOutput.Code))
                        return true;
                    if (DecodeOrgUnit(_scannerOutput.Code))
                        return true;

                    result = DecodeConsignmentItem(_scannerOutput.Code);
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeCode39");
            }
            return result;
        }

        private bool DecodeConsignmentItem(string barcodeText)
        {
            bool result = false;
            int barcodeLength = barcodeText.Length;

            //Validate consignment item number
            if (barcodeLength == 13)    
            {
                string identifier = barcodeText.Substring(0, 2);
                string content = barcodeText.Substring(2, barcodeLength - 5);
                
                string checkSum = barcodeText.Substring(barcodeLength - 3, 1);
                string origin = barcodeText.Substring(barcodeLength - 2);

                if (StringUtil.IsNumeric(content) && StringUtil.IsNumeric(checkSum) &&
                    StringUtil.IsAlfaUpper(identifier) && StringUtil.IsAlfaUpper(origin))
                {
                    // Calculate checksum only for Norwegian consignments
                    result = origin != "NO" || Checksum.IsValidCheckSumCode39(content, checkSum);

                    if (result)
                    {
                        _barcodeData = new BarcodeData(barcodeText, _scannerOutput.Type,
                                                       BarcodeType.ConsignmentItemNumber);
                    }
                }
            }
            // HACK: This is to support a series of mis-coded consignment item that Posten printed
            // Temporary fix
            else if (barcodeLength == 14)
            {
                _barcodeData = new BarcodeData(barcodeText, _scannerOutput.Type, BarcodeType.ConsignmentItemNumber);
                result = true;
            }

            return result;
        }

        private bool DecodeOrgUnit(string barcodeText)
        {
            bool result = false;
            if (barcodeText.Length == 6)
            {
                if (Regex.IsMatch(barcodeText, @"^\d+$")) // Is numeric
                {
                    _barcodeData = new BarcodeData(barcodeText, _scannerOutput.Type, BarcodeType.OrgUnitId);
                    result = true;
                }
            }
            return result;
        }

        // Validation for shelf id (One alpha followed by alpha or space and then 6 digits)
        private bool DecodeShelf(string barcodeText)
        {
            bool result = false;
            if (barcodeText.Length == 8)
            {
                var regex = new Regex(@"[A-Z][A-Z,' '][0-9]{6}", RegexOptions.IgnorePatternWhitespace);
                if (regex.IsMatch(barcodeText))
                {
                    _barcodeData = new BarcodeData(barcodeText, _scannerOutput.Type, BarcodeType.Shelf);
                    result = true;
                }
            }
            return result;
        }


        /// <summary>
        /// Code 128  - Spec 3.3
        /// 
        /// Format 01 : 00##################
        /// Length: 13
        /// Prefix length: 0
        /// Indicator length: 0
        /// Manual input may omit prefix and hence must be added, 18 if optional missing) 
        /// </summary>
        private bool DecodeCode128()
        {
            bool result = false;

            try
            {
                if (_scannerOutput.Type == DecoderTypes.CODE128 || _scannerOutput.Type == ManualInput)
                {
                    switch (_scannerOutput.Code.Length)
                    {
                        case 4:
                            //validation for route id
                            if (Regex.IsMatch(_scannerOutput.Code, @"[0-9]{4}"))
                            {
                                _barcodeData = new BarcodeData(_scannerOutput.Code, _scannerOutput.Type,
                                                               BarcodeType.RouteId);
                                return true;
                            }
                            break;

                        case 13:

                            string content = _scannerOutput.Code.Substring(0, 11);
                            string country = _scannerOutput.Code.Substring(11, 2);

                            if (StringUtil.IsNumeric(content) && StringUtil.IsAlfaUpper(country))
                            {
                                _barcodeData = new BarcodeData(_scannerOutput.Code, _scannerOutput.Type,
                                                               BarcodeType.ConsignmentItemNumber);
                                result = true;
                            }
                            break;

                        case 22:
                        case 26:
                            result = DecodePostenLoadCarrier(_scannerOutput.Code);
                            break;
                    }
                }
            }
            catch (FormatException ex)
            {
                Logger.LogExpectedException(ex, "Decoder.Decode128");
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.Decode128");
            }

            return result;
        }

        // Barcode formats

        // IDENTFORMAT  NUMCHARPREFIX    FORMATSTRING                         
        //    ---------------------------1234567890123456789012345678901234567
        //    --------------------------+---------+---------+---------+----
        //    01           2             00##################                 
        //    02           1             J@@@@@@@@@@@@??????????????????????? 
        //    03           2             2J@@@@@@@@@@@@???????????????????????
        //    04           2             3J@@@@@@@@@@@@???????????????????????
        //    05           2             4J@@@@@@@@@@@@???????????????????????
        //    06           0             ##################                   
        //    07           0             @@########$$$                        
        //    08           0             @@#########$$     
        //    09           0             ############ 
        //    10           2             ???...?00##################??...?               

        // #	This position in the barcode must contain a digit.
        // $	This position in the barcode must contain a letter.
        // @	This position in the barcode must contain a character.
        // %	If this position is present in the barcode, it must contain a digit.
        // &	If this position is present in the barcode, it must contain a letter.
        // ?	If this position is present in the barcode, it may contain any character.
        private bool DecodeSpecialJShipment()
        {
            bool result = false;
            if (_scannerOutput.Type == DecoderTypes.CODE128 || _scannerOutput.Type == ManualInput)
            {
                if (_scannerOutput.Code.Length >= 13 && _scannerOutput.Code.Length <= 37)
                {
                    string content = null;
                    
                    var barcodeType = BarcodeType.ConsignmentItemNumber;
                    if (_scannerOutput.Code[0] == 'J')
                    {
                        content = _scannerOutput.Code.Substring(1);
                    }
                    else
                    {
                        switch (_scannerOutput.Code.Substring(0, 2))
                        {
                            case "1J":
                            case "3J":
                                content = _scannerOutput.Code.Substring(2);
                                break;

                            case "2J":
                            case "4J":
                                content = _scannerOutput.Code.Substring(2);
                                barcodeType = BarcodeType.ConsignmentNumber;
                                break;
                        }
                    }

                    if (content != null)
                    {
                        _barcodeData = new BarcodeData(content, _scannerOutput.Type, barcodeType, BarcodeSubType.SpecialJ);
                        result = true;
                    }
                }

            }
            return result;
        }


        private bool DecodePostenLoadCarrier(string barcodeString)
        {
            bool result = false;

            do
            {
                if (barcodeString.Length == 22)
                {
                    barcodeString = "8003" + barcodeString;
                }

                string indicator = barcodeString.Substring(0, 4);
                if (indicator != "8003")
                    break;

                string leadingNumber = barcodeString.Substring(4, 1);
                if (leadingNumber != "0")
                    break;

                string countryCode = barcodeString.Substring(5, 2);
                if (countryCode != "70")
                    break;

                string articleNumber = barcodeString.Substring(7, 11);
                if (articleNumber != "30043121014")
                    break;

                string serialnumber = barcodeString.Substring(18, 7);

                string temp = countryCode + articleNumber + serialnumber;
                const string mask = "13131313131313131313";
                int sum = 0;
                for (int i = 0; i < temp.Length; i++)
                    sum += Convert.ToInt32(temp.Substring(i, 1)) * Convert.ToInt32(mask.Substring(i, 1));

                sum = 10 - (sum % 10);
                if (sum == 10)
                    sum = 0;

                string checkSum = barcodeString.Substring(25, 1);
                if (sum == Convert.ToInt32(checkSum))
                {

                    var loadCarrier = barcodeString.Substring(4, 22);

                    _barcodeData = new BarcodeData(loadCarrier, _scannerOutput.Type, BarcodeType.LoadCarrier);
                    result = true;
                }
            }
            while (false);

            return result;
        }


        private bool DecodeCargo()
        {
            bool result = false;
            try
            {
                if (_scannerOutput.Type == DecoderTypes.CODE39 || _scannerOutput.Type == ManualInput)
                {
                    if (_scannerOutput.Code.Length == 10 && StringUtil.IsNumeric(_scannerOutput.Code))
                    {
                        result = Checksum.IsValidCheckSumCargo1(_scannerOutput.Code);
                        if (result)
                        {
                            // Remove checksum digit
                            _scannerOutput.Code.Substring(0, _scannerOutput.Code.Length - 1);
                            _barcodeData = new BarcodeData(_scannerOutput.Code, _scannerOutput.Type, BarcodeType.ConsignmentNumber);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeCargo()");
            }
            return result;
        }


        private bool DecodeConsignment()
        {
            bool result = false;

            try
            {
                if (_scannerOutput.Type != DecoderTypes.PDF417)
                {
                    string code = _scannerOutput.Code;
                    if (code.Length == 17)
                    {
                        code = "401" + _scannerOutput.Code;
                    }

                    if (code.Length == 20)
                    {
                        string indikator = code.Substring(0, 3);
                        string content = code.Substring(3, 16);
                        string checksumDigit = code.Substring(19, 1);

                        if (indikator == "401")
                        {
                            result = Checksum.IsValidCheckSumCargo2(content, checksumDigit);
                            if (result)
                            {
                                _barcodeData = new BarcodeData(code.Substring(3), _scannerOutput.Type,
                                                               BarcodeType.ConsignmentNumber);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeConsignment");
            }
            return result;
        }

        private bool DecodeDeliveryCode()
        {
            bool result = false;
            try
            {
                if (_scannerOutput.Type == ManualInput)
                {
                    int barcodeLength = _scannerOutput.Code.Length;

                    if (barcodeLength == 4)
                    {
                        if (Regex.IsMatch(_scannerOutput.Code, "^[A-Za-z]{2}[0-9]{2}$")) // First two alpha, last two numeric
                        {
                            _barcodeData = new BarcodeData(_scannerOutput.Code, _scannerOutput.Type, BarcodeType.DeliveryCode);
                            result = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeCode39");
            }
            return result;
        }


        private bool DecodeFrigoConsignment()
        {
            bool result = false;

            try
            {
                switch (_scannerOutput.Type)
                {
                    case DecoderTypes.CODE39:
                    case DecoderTypes.UNKNOWN:
                    case DecoderTypes.CODE128:
                    case DecoderTypes.EAN128:
                        break;
                    default:
                        return false;
                }

                string code = _scannerOutput.Code;
                if (code.Length == 13)
                {
                    if (code.StartsWith("401"))
                        code = code.Substring(3);
                }

                if (code.Length == 10)
                {
                    string value = code.Substring(0, 9);
                    string checksumDigit = code.Substring(9, 1);

                    result = Checksum.IsValidCheckSumFrigo(value, checksumDigit);
                    if (result)
                    {
                        _barcodeData = new BarcodeData(code, _scannerOutput.Type, BarcodeType.ConsignmentNumber);
                    }
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeFrigoConsignment");
            }
            return result;
        }

        /// <summary>
        /// Decode Drivers Licence
        /// </summary>
        /// <returns></returns>
        private bool DecodeDriverLicence()
        {
            bool result = false;

            try
            {
                switch (_scannerOutput.Type)
                {
                    case DecoderTypes.I2OF5:
                    case DecoderTypes.DATAMATRIX:
                        break;
                    default:
                        return false;
                }

                _barcodeData = new BarcodeData(_scannerOutput.Code, _scannerOutput.Type,
                                                           BarcodeType.DriversLicence);
                result = true;
            }
            catch (Exception e)
            {
                Logger.LogException(e, "Decoder.DecodeDriverLicence");
            }
            return result;
        }


    }
}
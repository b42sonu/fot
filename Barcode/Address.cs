﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Barcode
{
    public class Adress : EntityBase
    {
        public string Name
        {
            get; set;
        }

        public string Adress1
        {
            get;
            set;
        }

        public string Adress2
        {
            get;
            set;
        }

        public PostalPlace PostalPlace
        {
            get;
            set;
        }
    }
}

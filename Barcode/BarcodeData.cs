﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Symbol.Barcode;

namespace Com.Bring.PMP.PreComFW.Shared.Barcode
{
    [Flags]
    public enum BarcodeType
    {
        Unknown = 0x0,
        Rbt = 0x1,
        ConsignmentItemNumber = 0x2,
        ConsignmentNumber = 0x4,
        ProductCode = 0x8,
        VasCode = 0x10,
        CustomerNumber = 0x20,
        DeliveryCode = 0x40,
        Shelf = 0x80,
        DriversLicence = 0x100,
        TransportCarrier = 0x200,
        LoadCarrier = 0x400,
        PostalNumber = 0x800,
        RouteId = 0x1000,
        OrgUnitId = 0x2000,
        AgentExp = 0x4000,

        Shipment = ConsignmentItemNumber | ConsignmentNumber,
        ConsignmentItemOrLoadCarrier = ConsignmentItemNumber | LoadCarrier,
        ConsignmentItemOrDeliveryCode = ConsignmentItemNumber | DeliveryCode
    }

    [Flags]
    public enum BarcodeSubType
    {
        Standard = 0x0,
        PruTracking = 0x1,
        Pdf = 0x2,
        SpecialJ = 0x4
    }

    /// <summary>
    /// Data stored in a barcode label in decoded format
    /// </summary>
    public class BarcodeData
    {
        internal BarcodeData()
        {
            
        }

        internal BarcodeData(BarcodePdf pdf)
        {
            BarcodeSubType = BarcodeSubType.Pdf;
            switch (pdf.PdfType)
            {
                case PdfType.WaybillType1:
                case PdfType.WaybillType2:
                    EncodingType = DecoderTypes.PDF417;
                    ConsignmentId = pdf.PdfWaybill.ConsignmentId;
                    ConsignmentItemId = pdf.PdfWaybill.ConsignmentItemId;
                    BarcodeType = BarcodeType.ConsignmentItemNumber;
                    break;

                case PdfType.RouteCarrierTrip:
                    BarcodeType = BarcodeType.Rbt;
                    BarcodeRouteCarrierTrip = BarcodeRouteCarrierTrip.Decode(pdf.PdfString);
                    break;

                case PdfType.TransportCarrier:
                    BarcodeType = BarcodeType.TransportCarrier;
                    BarcodeTransportCarrier = BarcodeTransportCarrier.Decode(pdf.PdfString);
                    break;

                case PdfType.LoadCarrier:
                    BarcodeType = BarcodeType.LoadCarrier;
                    BarcodeLoadCarrier = BarcodeLoadCarrier.Decode(pdf.PdfString);
                    break;

                case PdfType.AgentExpTerm:
                    BarcodeAgentExp = BarcodeAgentExp.Decode(pdf.PdfString);
                    if(BarcodeAgentExp != null)
                        BarcodeType = BarcodeType.AgentExp;
                    break;

                default:
                    throw new Exception("Detected unhandled pdf-type: " + pdf.PdfType);
            }
        }

        internal BarcodeData(string barcodeString, DecoderTypes encodingType, BarcodeType barcodeType) :
            this(barcodeString, encodingType, barcodeType, BarcodeSubType.Standard)
        { }

        internal BarcodeData(string barcodeString, DecoderTypes encodingType, BarcodeType barcodeType, BarcodeSubType barcodeSubType)
        {
            BarcodeSubType = barcodeSubType;

            switch (barcodeType)
            {
                case BarcodeType.ConsignmentNumber:
                    ConsignmentId = barcodeString;
                    break;

                case BarcodeType.ConsignmentItemNumber:
                    ConsignmentId = ConsignmentEntity.UndefinedConsignmentId;
                    ConsignmentItemId = barcodeString;
                    break;

                case BarcodeType.LoadCarrier:
                    BarcodeLoadCarrier = BarcodeLoadCarrier.Decode(barcodeString);
                    break;

                case BarcodeType.OrgUnitId:
                    OrgUnitId = barcodeString;
                    break;

                case BarcodeType.Shelf:
                    ShelfId = barcodeString;
                    break;

                case BarcodeType.DeliveryCode:
                    DeliveryCode = barcodeString;
                    break;

                case BarcodeType.RouteId:
                    RouteId = barcodeString;
                    break;

                case BarcodeType.DriversLicence:
                    LicenceNumber = barcodeString;
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled barcode protocol {0} in BarcodeData()", barcodeType);
                    break;
            }

            EncodingType = encodingType;
            BarcodeType = barcodeType;
        }

        public readonly BarcodeSubType BarcodeSubType;
        public readonly DecoderTypes EncodingType;
        public readonly BarcodeType BarcodeType;
        public readonly BarcodeRouteCarrierTrip BarcodeRouteCarrierTrip;
        public readonly BarcodeTransportCarrier BarcodeTransportCarrier;
        public readonly BarcodeLoadCarrier BarcodeLoadCarrier;
        public readonly BarcodeAgentExp BarcodeAgentExp;
        public readonly string OrgUnitId;
        public readonly string LicenceNumber;

        public bool IsLoadCarrier
        {
            get { return BarcodeType == BarcodeType.LoadCarrier; }
        }

        public bool IsTransportCarrier
        {
            get { return BarcodeType == BarcodeType.TransportCarrier; }
        }

        public readonly string ConsignmentItemId;
        public string ConsignmentId;
        public string ShelfId;
        public string DeliveryCode;
        public string RouteId;
    }
}

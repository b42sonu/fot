﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SampleScreenDesigner
{
    internal class ViewState 
    {
        public const int Ok = 1;
    }

    internal delegate void ViewEventDelegate(int viewState);

    enum ViewStates
    {
        Ok,
        Cancel
    }


    internal class BaseView
    {
        public ViewEventDelegate ViewEvent;
    }

    internal class View : BaseView
    {
        internal View()
        {
            ViewEvent.Invoke(ViewState.Ok);
        }
    }




    internal class Flow
    {
        Flow()
        {
            var view = new View();

            view.ViewEvent = (state => EventHandler((ViewStates)state));
        }

        void EventHandler(ViewStates state)
        {
            switch (state)
            {
                case ViewStates.Ok:

                    break;
            }
        }
    }

}

﻿using System;
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.Constants
{
    public class GlobalTexts
    {
        // Text for buttons
        
        public static string Signature { get { return Language.Translate("Signature"); } }
        public static string NewDamage { get { return Language.Translate("New Damage"); } }
        public static string TakePhoto { get { return Language.Translate("Take Photo"); } }
        public static string Items { get { return Language.Translate("Items"); } }
        public static string Deviation { get { return Language.Translate("Deviation"); } }
        public static string Delete { get { return Language.Translate("Delete"); } }
        public static string DifferentStop { get { return Language.Translate("Different Stop"); } }
        public static string Save { get { return Language.Translate("Save"); } }
        public static string Camera { get { return Language.Translate("Camera"); } }
        public static string Reset { get { return Language.Translate("Reset"); } }
        public static string TmsAvailability { get { return Language.Translate("TMS Availability"); } }
        public static string GpsSettings { get { return Language.Translate("GPS Settings"); } }
        public static string TmiSettings { get { return Language.Translate("TMI Settings"); } }
        public static string Delivery { get { return Language.Translate("Del."); } }
        public static string ChangeLocation { get { return Language.Translate("Change Location"); } }
        public static string Attempt { get { return Language.Translate("Attempt"); } }
        public static string OkEnter { get { return Language.Translate("Ok/Enter"); } }
        public static string Confirm { get { return Language.Translate("Confirm"); } }
        public static string UnplannedPick { get { return Language.Translate("Unpl. pick"); } }
        public static string AttemptedDel { get { return Language.Translate("Attempted Del."); } }
        public static string Departure { get { return Language.Translate("Departure"); } }
        public static string Refresh { get { return Language.Translate("Refresh"); } }
        public static string Details { get { return Language.Translate("Details"); } }
        public static string Open { get { return Language.Translate("Open"); } }
        public static string Get { get { return Language.Translate("Get"); } }
        public static string Back { get { return Language.Translate("Back"); } }
        public static string Scan { get { return Language.Translate("Scan"); } }
        public static string Yes { get { return Language.Translate("Yes"); } }
        public static string No { get { return Language.Translate("No"); } }
        public static string Ok { get { return Language.Translate("Ok"); } }
        public static string On { get { return Language.Translate("On"); } }
        public static string Off { get { return Language.Translate("Off"); } }
        public static string Finish { get { return Language.Translate("Finish"); } }
        public static string Cancel { get { return Language.Translate("Cancel"); } }
        public static string ChgCarrier { get { return Language.Translate("Chg.Carrier"); } }
        public static string Unload { get { return Language.Translate("Unload"); } }
        public static string Load { get { return Language.Translate("Load"); } }
        public static string Reconcillation { get { return Language.Translate("Reconcilliation"); } }
        public static string Attempted { get { return Language.Translate("Attempted"); } }
        public static string Continue { get { return Language.Translate("Continue"); } }
        public static string Erase { get { return Language.Translate("Erase"); } }
        public static string AddTransportEquipment { get { return Language.Translate("Add transport equipment"); } }
        public static string AddPackageType { get { return Language.Translate("Add package type"); } }
        public static string Info { get { return Language.Translate("Info"); } }
        public static string Reject { get { return Language.Translate("Reject"); } }
        public static string Accept { get { return Language.Translate("Accept"); } }
        public static string AcceptAll { get { return Language.Translate("Accept all"); } }
      
  
        
        //Text for labels

        public static string ConfirmSignature { get { return Language.Translate("Please confirm with or without signature"); } }
        public static string ConfirmDamageForItem { get { return Language.Translate("Confirm damagae for item type item number"); } }
        public static string Comment { get { return Language.Translate("Comment"); } }
        public static string PackageTypeDamaged { get { return Language.Translate("Package type was totally damaged."); } }
        public static string FellOutOfCar { get { return Language.Translate("Fell out of car"); } }
        public static string Destruert { get { return Language.Translate("Destruert"); } }
        public static string TakePictureConfirmation { get { return Language.Translate("Do you want to take a picture ?"); } }
        public static string LabelDamageForItem { get { return Language.Translate("Damage for Item Text Item Number"); } }
        public static string RegisterDamage { get { return Language.Translate("Register Damage"); } }
        public static string DamageType { get { return Language.Translate("Damage type"); } }
        public static string APackageDamage { get { return Language.Translate("A Package Damage"); } }
        public static string PackageDamage { get { return Language.Translate("Package Damage"); } }
        public static string DamageInformation { get { return Language.Translate("Damage Information"); } }
        public static string Text { get { return Language.Translate("Text"); } }
        public static string EarlierEvents{ get { return Language.Translate("Earlier events for item no"); } }
        public static string NrOfConsignments { get { return Language.Translate("Nr of consigments"); } }
        public static string StopId { get { return Language.Translate("Stop Id"); } }
        public static string Pickup { get { return Language.Translate("Pickup"); } }
        public static string WithoutStop { get { return Language.Translate("WithOut Stop"); } }
        public static string Operations { get { return Language.Translate("Operations"); } }
        public static string InformationConsignmentScanned {get { return Language.Translate("You have scanned a consignment"); } }
        public static string ConsignmentId { get { return Language.Translate("Consignment id"); } }
        public static string ConfirmConsignmentCount { get { return Language.Translate("Confirm consignment item count"); } }
        public static string Discard { get { return Language.Translate("Discard"); } }
        public static string DamageRegistration { get { return Language.Translate("Damage registration"); } }
        public static string DamageFor { get { return Language.Translate("Click on camera  to activate \r\ncamera .User enter button to\r\ncapture a photo."); } }
        public static string CameraInstruction { get { return Language.Translate("Click on camera  to activate \r\ncamera .User enter button to\r\ncapture a photo."); } }
        public static string LabelThumbnail { get { return Language.Translate("Thumbnail for already captured photos"); } }
        public static string ChangeTmsAvailability { get { return Language.Translate("Change TMS Availability"); } }
        public static string ChangeConfirmInformation { get { return Language.Translate("Change or Confirm Information"); } }
        public static string ScanOrEnterConsignment { get { return Language.Translate("Scan or enter consignment number"); } }
        public static string AttemptedPickUp { get { return Language.Translate("Attempted Pickup"); } }
        public static string DepartFromStop { get { return Language.Translate( "Are you sure you would like to depart from this stop?"); } }
        public static string OperationDetails { get { return Language.Translate("Operation Details"); } }
        public static string StartOperationList { get { return Language.Translate("Start Operation list from main menu to start your work"); } }
        public static string OperationListPresent { get { return Language.Translate("Operation list present"); } }
        public static string LoadList { get { return Language.Translate("Load List"); } }
        public static string Consignee { get { return Language.Translate("Consignee"); } }
        public static string Consignments { get { return Language.Translate("Consignments"); } }
        public static string ConsigneeAdr { get { return Language.Translate("Consignee adr"); } }
        public static string Completed { get { return Language.Translate("Completed"); } }
        public static string NotCompleted { get { return Language.Translate("Not Completed"); } }
        public static string All { get { return Language.Translate("All"); } }
        public static string ConsignmentNumber { get { return Language.Translate("Consignment number"); } }
        public static string GrossWeight { get { return Language.Translate("Gross Weight"); } }
        public static string CustomerNr { get { return Language.Translate("Customer nr"); } }
        public static string ConsignorAdr { get { return Language.Translate("Consignor adr"); } }
        public static string OrderNo { get { return Language.Translate("Order No."); } }
        public static string Cause { get { return Language.Translate("Cause"); } }
        public static string Measure { get { return Language.Translate("Measure"); } }
        public static string Description { get { return Language.Translate("Description"); } }
        public static string GenerateBookingNumber { get { return Language.Translate("Generate booking number"); } }
        public static string ClickToGenrateBookingNumber { get { return Language.Translate("Or click  here to generate"); } }
        public static string BookingNumber { get { return Language.Translate("Booking Number"); } }
        public static string CustomerNumber { get { return Language.Translate("Customer Number"); } }
        public static string UnplanedPickUp { get { return Language.Translate("Unplanned pick up"); } }
        public static string ChangePassword { get { return Language.Translate("Change Password"); } }
        public static string CurrentPassword { get { return Language.Translate("Current Password"); } }
        public static string NewPassword { get { return Language.Translate("New Password"); } }
        public static string AttemptDeliveryInfo { get { return Language.Translate("Attempted delivery information"); } }
        public static string OptionalComment { get { return Language.Translate("Comment (optional)"); } }
        public static string OptionalComments { get { return Language.Translate("Comments (optional)"); } }
        public static string AttemptedDelivery { get { return Language.Translate("Attempted delivery"); } }
        public static string ConsignmentItemNumber { get { return Language.Translate("Consignment/item number"); } }
        public static string TripId { get { return Language.Translate("Trip Id"); } }
        public static string RegisterTemperature { get { return Language.Translate("Register Temperature"); } }
        public static string ConfirmInfo { get { return Language.Translate("Are new carrier and select operation type you wish to start."); } }
        public static string NewCarrier { get { return Language.Translate("New Carrier"); } }
        public static string SelectAddCarrier { get { return Language.Translate("Select a carrier from the list and click Continue or New Carrier to add another."); } }
        public static string WorkList { get { return Language.Translate("Work List"); } }
        public static string AddCarrierSelectOperation { get { return Language.Translate("Add new carrier and select operation type you wish to start."); } }
        public static string SignatureInfo { get { return Language.Translate("Your signature will be displayed in Posten/Brings tracking solution online."); } }
        public static string NrOfConsignmentItem { get { return Language.Translate("Nr of consignment items"); } }
        public static string CustomerName { get { return Language.Translate("Customer name"); } }
        public static string VerifyChangePowerUnit { get { return Language.Translate("Verify or change Power Unit ID"); } }
        public static string Loading { get { return Language.Translate("Loading"); } }
        public static string PowerUnitId { get { return Language.Translate("Power Unit ID"); } }
        public static string Quantity { get { return Language.Translate("Quantity"); } }
        public static string PackageTypes { get { return Language.Translate("Package types"); } }
        public static string PackageTypeAndTransportEquipment { get { return Language.Translate("Package type and transport equipment"); } }
        public static string TransportEquipment { get { return Language.Translate("Transport equipment"); } }
        public static string PrivateContactDetail { get { return Language.Translate("Private Contact Detail"); } }
        public static string GroupInformation  { get { return Language.Translate("Group Information"); } }
        public static string Settings  { get { return Language.Translate("Settings"); } }
   
        
        // Short statements and headers
        public static string InvalidSignature { get { return Language.Translate("Invalid signature"); } }
        public static string ConsignmentDeleted { get { return Language.Translate("Consignment deleted"); } }
        public static string ConfirmationOperationList { get { return Language.Translate("Operation List Confirmation"); } }
        public static string Attention { get { return Language.Translate("Attention"); } }
        public static string Error { get { return Language.Translate("Error"); } }
        public static string Goods { get { return Language.Translate("Goods"); } }
        public static string Offline { get { return Language.Translate("Offline"); } }
        public static string Summary { get { return Language.Translate("Summary"); } }
        public static string Unknown { get { return Language.Translate("Unknown"); } }
        public static string HeaderScanConsignment { get { return Language.Translate("Scan required consignment"); } }
        public static string Temperature { get { return Language.Translate("Temperature"); } }
        public static string ConsignmentItem { get { return Language.Translate("Consignment item"); } }
        public static string Consignment { get { return Language.Translate("Consignment"); } }
        public static string LogOnMessage { get { return Language.Translate("Log On"); } }
        public static string OrgUnitId { get { return Language.Translate("OrgUnit Id"); } }
        public static string PowerUnit { get { return Language.Translate("Power Unit"); } }
        public static string ContainmentUnit { get { return Language.Translate("Containment Unit"); } }
        public static string UnitId { get { return Language.Translate("Unit Id"); } }
        public static string PostalCode { get { return Language.Translate("Postal Code"); } }
        public static string TmsAffiliation { get { return Language.Translate("TMS Affi."); } }
        public static string RouteId { get { return Language.Translate("Route Id"); } }
        public static string CompanyCode { get { return Language.Translate("Company Code"); } }
        public static string CountryCode { get { return Language.Translate("Country Code"); } }
        public static string TelephoneNumber { get { return Language.Translate("Tele. No."); } }
        public static string UserProfile { get { return Language.Translate("User Profile"); } }
        public static string OperationFormHeaderText { get { return Language.Translate("Operation List"); } }
        public static string TmsAvailabilityInformation { get { return Language.Translate("Tasks Information"); } }
        public static string DriverAvailability { get { return Language.Translate("Driver Availability"); } }
        public static string UnfinishedOperationsTitle { get { return Language.Translate("Unfinished Operations"); } }
        public static string UnfinishedDeliveriesTitle { get { return Language.Translate("Unfinished Deliveries"); } }

        // Information
        public static string ScanConsignmentDeleted { get { return Language.Translate("Selected scanned consignment/consignment Item(s) has been deleted from cache."); } }
        public static string PackageTypeAndTransportVerified { get { return Language.Translate("Package type and Transport equipment verified."); } }
        public static string ConsignmentAddedToItem { get { return Language.Translate("Consignment {0} added to consignment item {1}"); } }
        public static string NoOfConsignmentItem { get { return Language.Translate("No of consignment items"); } }
        public static string ChooseCorrectionForConsignment { get { return Language.Translate("Consignment scanned earlier. Choose correction for"); } }
        public static string ChooseCorrectionForItem { get { return Language.Translate("Item scanned earlier. Choose correction for"); } }
        public static string ProcessForDeleteItemFailed { get { return Language.Translate("Process for Deletion of Item {0} Failed"); } }
        public static string ItemDeleted { get { return Language.Translate("Item {0} deleted"); } }
        public static string RegisterTempratureForItem { get { return Language.Translate("Process for registering temperature Triggered for item"); } }
        public static string RegisterCorrectionForItem { get { return Language.Translate("Process for registering correction Triggered for item"); } }
        public static string LoadingProfile { get { return Language.Translate("Please wait, loading profile from server."); } }
        public static string UserLoggedFromAssetMesssage { get { return Language.Translate("User is no more associated with the current Asset or already loggedoff by some other user"); } }
        public static string OfflineMessage { get { return Language.Translate("You are not connected to server, and have logged in offline"); } }
        public static string UserProfileFromServer { get { return Language.Translate("Getting profile from server...."); } }
        public static string ConnectedTolocalProfile { get { return Language.Translate("You are currently offline and connected to local profile."); } }
        public static string ProfileRetrived { get { return Language.Translate("Profile retrieved successfully from server."); } }
        public static string InConsignment { get { return Language.Translate("in consignment"); } }
        public static string RegisteredConsignment { get { return Language.Translate("Registered consignment"); } }
        public static string RegisteredConsignmentItem { get { return Language.Translate("Registered consignment item"); } }
        public static string NoAssociatedPowerunit { get { return Language.Translate("You are no longer associated to the current Power Unit in the TMS. You will not get updated planning information for the Power Unit"); } }
        public static string AcceptRejectOrder { get { return Language.Translate("{0} orders accepted and {1} orders rejected"); } }
        public static string NewOperationListRecived { get { return Language.Translate("New Operation List recieved"); } }
        public static string UpdatedOperationListRecived { get { return Language.Translate("Updated Operation List recieved"); } }

        //Verifications
        public static string ConfirmOperationListMessage { get { return Language.Translate("Local operation list exists, do you want to persist it?"); } }
        public static string VerifyPowerUnitLoadingMessage { get { return Language.Translate("You have no loading operations in your operation list. Please verify if the curren Power Unit ID is correct or change it by input the correct one."); } }
        public static string VerifyPowerUnitLoadListMessage { get { return Language.Translate("You have no local load list. Please provide your current Power Unit ID."); } }
        public static string VerifyScannedUnplannedConsignment { get { return Language.Translate("Please confirm you want to handle scanned, unplanned consignment"); } }
        public static string UnloadingVerification { get { return Language.Translate("Please verify if unloading for this stop is completed"); } }
        public static string PendingTasksOperationListMessage { get { return Language.Translate("User has some pending tasks in operation list.Do you really want to continue?"); } }
        public static string PendingTasksLoadListMessage { get { return Language.Translate("User has some pending tasks in Load list. Do you really want to continue?"); } }

        // Confirmation
        public static string NoPendingTasksMessage { get { return Language.Translate("User has no pending tasks in the OperationList so logging off the user from TMS"); } }
        public static string PendingTasksMessage { get { return Language.Translate("User has some pending tasks. Do you really want to continue"); } }
        public static string UserProfileLabel { get { return Language.Translate("Change or confirm Information"); } }
        public static string DriverOverridden { get { return Language.Translate("The Asset already has a driver assigned. Do you wish to take over the asset?"); } }
        public static string UnloadingCompleted { get { return Language.Translate("Unloading completed?"); } }
        public static string UnfinishedOperationsDescription { get { return Language.Translate("You have unfinished operations in the operation list."); } }
        public static string UnfinishedDeliveriesDescription { get { return Language.Translate("You have unfinished deliveries in the delivery list."); } }
        public static string AreYouSureYouWantToLogOut { get { return Language.Translate("Are you sure you want to log out?"); } }
        public static string EnterOptionalConsignment { get { return Language.Translate("Optional consignment number is missing. Do you want to provide it?"); } }

        // Abortions
        public static string AttemptedDeliveryAborted { get { return Language.Translate("Attempted delivery aborted."); } }
        public static string PackageTypeAndTransportAborted { get { return Language.Translate("Verify package type and transport equipment aborted"); } }
        public static string ScanningConnsignmentAborted { get { return Language.Translate("Scanning of consignment id aborted"); } }
        public static string HandleScannedUnplannedConsignmentAborted { get { return Language.Translate("Handling of scanned, unplanned consignment aborted"); } }
        public static string CorrectionMenuAborted { get { return Language.Translate("Correction menu aborted."); } }
        public static string ConsignmentItemConfirmationAborted { get { return Language.Translate("Confirmation of consignment item count aborted"); } }

        // Error messages
        public static string PackageTypeRequired { get { return Language.Translate("At least one package-type item is required"); } }
        public static string ItemSelectedMultipleTimes { get { return Language.Translate(" {0} {1} has been selected multiple times"); } }
        public static string SignatureClarityMessage { get { return Language.Translate("The signature must be large and clear"); } }
        public static string NameStrength { get { return Language.Translate("Name must be min 2, max 28 characters. Please try again."); } }
        public static string NameRequired { get { return Language.Translate("Name must be filled in"); } }
        public static string IllegalBarcode { get { return Language.Translate("Illegal barcode '{0}': Format not valid or invalid checksum"); } }
        public static string NotAuthorizedUser { get { return Language.Translate("Barcode {0} identifies a consignment, but user is not authorized for scanning such items"); } }
        public static string IllegalConsignment { get { return Language.Translate("Illegal consignment '{0}': Wrong type or invalid checksum. Please try again!"); } }
        public static string SavePhotoError { get { return Language.Translate("Problem occured when saving photo."); } }
        public static string CameraInitializeError { get { return Language.Translate("Problem occured when Initializing the Camera."); } }
        public static string InvalidPowerUnit { get { return Language.Translate("User has put in Power Unit {0} which does not exist in {1}."); } }
        public static string InvalidBookingNumber { get { return Language.Translate("Booking number is not valid!"); } }
        public static string InvalidLocation { get { return Language.Translate("Invalid Location Id"); } }
        public static string LocationNotFound { get { return Language.Translate("Location Id could not be found in the unit name inventory. Please wait until the table have been properly synchronized.Current progress"); } }
        public static string NotHavingValidProfile { get { return Language.Translate("Not connected to server and also not having valid local profile."); } }
        public static string ProfileRoleEmpty { get { return Language.Translate("Invalid profile"); } }
        public static string PickupMultipleConsignmentsUnauthorized { get { return Language.Translate("Pick-up of multiple consignments not authorized"); } }
        public static string InvalidPowerUnitId { get { return Language.Translate("The Power unit/load carrier id is not valid.Please enter a new one."); } }
        public static string InvalidUserId { get { return Language.Translate("Invalid user Id.\n User not known in TMS.You will not recive assignment from the TMS.Contact help desk."); } }
        public static string OfflineProfile { get { return Language.Translate("The terminal cannot connect to the TMS.\n The terminal will try to reconnect  with the system automatically."); } }

        //Warning messages
        public static string ConsignmentItemCantZero { get { return Language.Translate("Number of consignment items can not be 0"); } }

        // Prompts
        public static string EnterTemprature { get { return Language.Translate("Enter temperature"); } }
        public static string ValidateTemprature { get { return Language.Translate("Enter valid temperature"); } }
        public static string EnterBarcode { get { return Language.Translate("Enter barcode"); } }
        public static string EnterPowerUnitId { get { return Language.Translate("Please enter Power Unit Id"); } }
        public static string TmsAffilationValue { get { return Language.Translate("Please select TMS Affiliation Value"); } }
        public static string EnterCustomerNumber { get { return Language.Translate("Enter customer number"); } }
        public static string EnterBookingNumber { get { return Language.Translate("Enter booking number"); } }
        public static string LocationId { get { return Language.Translate("Please enter location id."); } }
 
        //New properties for Label 
        public static string GoodsHandled { get { return Language.Translate("goods handled"); } }
        public static string SelectEquipment { get { return Language.Translate("Select equipment"); } }
        public static string PlannedOperation { get { return Language.Translate("Planned Operation"); } }
        public static string SelectPlannedOperation { get { return Language.Translate("Please select planned operation."); } }
        public static string PositionModule { get { return Language.Translate("Position module"); } }
        public static string ErrorPositionModule { get { return Language.Translate("Error while retreiving position module."); } }
        public static string Desktop { get { return Language.Translate("Desktop"); } }
        public static string Lan { get { return Language.Translate("LAN"); } }
        public static string Wlan { get { return Language.Translate("WLAN"); } }
        public static string ConnectionStatus { get { return Language.Translate("Connected to"); } }
        public static string TmiInformation { get { return Language.Translate("TMI Does not accept positions"); } }
        public static string TmiInformationMessage { get { return Language.Translate("The current TMI does not accept positioning, and therefore you cannot change the GPS status"); } }
        public static string SettingModule { get { return Language.Translate("Settings module"); } }
        public static string ErrorInSettingModule { get { return Language.Translate("Error while retreiving settings module."); } }
        public static string To { get { return Language.Translate("to"); } }
        public static string Switching { get { return Language.Translate("Switching"); } }
    }
}
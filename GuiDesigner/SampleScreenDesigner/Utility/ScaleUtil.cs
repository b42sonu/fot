﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.CommonControls;
using Resco.Controls.OutlookControls;
using Resco.Controls.SmartGrid;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    /// <summary>
    /// Util class for getting the right density of pictures
    /// </summary>
    public static class ScaleUtil
    {
        public const String OnscreeKeyboardButton = "OnscreenKeyboardButton";
        public const String Info = "Info";
        public const String HeaderLogo = "HeaderLogo";

        public static Bitmap GetImage(String imageName)
        {
            if (IsHighDensityDevice)
            {
                imageName += "_hdpi";
            }
            else
            {
                imageName += "_ldpi";
            }

            object obj = SampleScreenDesigner.Properties.Resources.ResourceManager.GetObject(imageName, SampleScreenDesigner.Properties.Resources.Culture);
            Bitmap image = ((Bitmap)(obj));

            if (image == null)
            {
                //Logger.LogEvent(Severity.Error, "Could not find the image " + imageName + " in resources");
            }

            return image;
        }

        public static bool IsHighDensityDevice
        {
            get
            {
                /*if (PreCom.Application.Instance.Platform.IsInitialized)
                {
                    return PreCom.Application.Instance.Platform.CurrentScreen.PixelDensity ==
                           PreCom.Core.Screen.PixelDensity.Large;
                }*/
                return true;
            }
        }

        public static int GetScaledPosition(int position)
        {
            if (IsHighDensityDevice)
            {
                return position;
            }

            return position / 2;
        }

        public static Size GetScaledSize(int width,int height)
        {
            return IsHighDensityDevice ? new Size(width,height) : new Size(width / 2, height / 2);
        }
        public static Size GetScaledSize(Size size)
        {
            return IsHighDensityDevice ? size : new Size(size.Width / 2, size.Height / 2);
        }

        public static Point GetScaledPoint(Point location)
        {
            return IsHighDensityDevice ? location : new Point(location.X / 2, location.Y / 2);
        }
        public static Point GetScaledPoint(int x,int y)
        {
            return IsHighDensityDevice ? new Point(x,y) : new Point(x / 2, y / 2);
        }

        public static Bitmap ResizeImage(Bitmap image)
        {
            if (IsHighDensityDevice)
            {
                return image;
            }

            var result = new Bitmap(image.Width / 2, image.Height / 2);

            using (Graphics graphics = Graphics.FromImage(result))
            {
                graphics.DrawImage(image, new Rectangle(0, 0, image.Width / 2, image.Height / 2), 0, 0, image.Width,
                                   image.Height, GraphicsUnit.Pixel, new ImageAttributes());
            }

            return result;
        }

        public static void ScaleControlsInForm(Control form)
        {
            if (IsHighDensityDevice) return;

            //Logger.LogEvent(Severity.Debug, "ScaleUtil.ScaleControlsInForm;" + form.Name);

            foreach (Control control1 in form.Controls)
            {
                Resizecontrol(control1);
                foreach (Control control2 in control1.Controls)
                {
                    Resizecontrol(control2);
                    foreach (Control control3 in control2.Controls)
                    {
                        Resizecontrol(control3);
                        foreach (Control control4 in control3.Controls)
                        {
                            Resizecontrol(control4);
                            foreach (Control control5 in control4.Controls)
                            {
                                Resizecontrol(control5);
                            }
                        }
                    }
                }

            }
        }
        private static void Resizecontrol(Control ctrl)
        {
            if (ctrl is MessageControl)
            {
                var lbl = (MessageControl)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            } if (ctrl is TextBox)
            {
                var lbl = (TextBox)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is TransparentLabel)
            {
                var lbl = (TransparentLabel)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is ImageButton)
            {
                var lbl = (ImageButton)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is PictureBox)
            {
                var lbl = (PictureBox)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is Panel)
            {
                var lbl = (Panel)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is TouchPanel)
            {
                var lbl = (TouchPanel)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is SmartGrid)
            {
                var lbl = (SmartGrid)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is RadioButton)
            {
                var lbl = (RadioButton)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else if (ctrl is ListBox)
            {
                var lbl = (ListBox)ctrl;
                lbl.Width = lbl.Width / 2;
                lbl.Height = lbl.Height / 2;
                lbl.Top = lbl.Top / 2;
                lbl.Left = lbl.Left / 2;
            }
            else
            {
                //Logger.LogEvent(Severity.Debug, "ScaleUtil.Resizecontrol did not handle;" + ctrl.GetType().Name);
            }

        }

    }
}
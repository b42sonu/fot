﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace SampleScreenDesigner.Utility
{
    public class DeviceInfo
    {
        #region SystemParametersInfoActions enum

        public enum SystemParametersInfoActions : uint
        {
            SPI_GETPLATFORMTYPE = 257, // this is used elsewhere for Smartphone/PocketPC detection
            SPI_GETOEMINFO = 258,
        }

        #endregion

        [DllImport("Coredll.dll", EntryPoint = "SystemParametersInfoW", CharSet = CharSet.Unicode)]
        private static extern int SystemParametersInfo4Strings(uint uiAction, uint uiParam, StringBuilder pvParam,
                                                               uint fWinIni);

        public static string GetOemInfo(string onError)
        {
            var oemInfo = new StringBuilder(50);
            if (SystemParametersInfo4Strings((uint) SystemParametersInfoActions.SPI_GETOEMINFO,
                                             (uint) oemInfo.Capacity, oemInfo, 0) == 0)
                return onError;
            return oemInfo.ToString();
        }

        public static string GetOemInfo()
        {
            string ret = GetOemInfo(null);
            if (ret == null)
                throw new Exception("Error getting OEM info.");
            return ret;
        }

        public static string GetPlatformType(string onError)
        {
            var platformType = new StringBuilder(50);
            if (SystemParametersInfo4Strings((uint) SystemParametersInfoActions.SPI_GETPLATFORMTYPE,
                                             (uint) platformType.Capacity, platformType, 0) == 0)
                return onError;
            return platformType.ToString();
        }

        public static string GetPlatformType()
        {
            string ret = GetPlatformType(null);
            if (ret == null)
                throw new Exception("Error getting platform info.");
            return ret;
        }
    }
}
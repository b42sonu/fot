﻿//using System;
//using System.Linq;
//using System.Collections.Generic;
//using System.Text;
//using System.Windows.Forms;
//using System.Collections;
//using System.ComponentModel;
//using System.Threading;
//using System.IO;
//using System.Drawing;
//using System.Drawing.Imaging;

//namespace SampleScreenDesigner.Utility
//{
//    class ShowCamera
//    {
//        private DirectShowNETCF.Camera.Camera _camera;
//        private Hashtable _currentCameraResolution;
//        private bool _isFlashOn;

//        public enum CameraStateEnum
//        {
//            Disposed,
//            Stopped,
//            Started
//        }

//        private ShowCamera()
//        {

//        }

//        private static ShowCamera instance = null;
//        public static ShowCamera Instance
//        {
//            get
//            {
//                if (instance == null)
//                {
//                    instance = new ShowCamera();
//                }
//                return instance;
//            }
//            set
//            {
//                instance = value;
//            }
//        }

//        private CameraStateEnum _cameraState = CameraStateEnum.Disposed;
//        public CameraStateEnum CameraState
//        {
//            get
//            {
//                return _cameraState;
//            }
//        }

//        public void InitializeCamera(string resolution, Panel cameraContainer)
//        {
//            try
//            {
//                if (_camera != null)
//                {
//                    _camera.release();
//                    _camera = null;
//                }

//                _camera = new DirectShowNETCF.Camera.Camera
//                               {
//                                   CapType = DirectShowNETCF.Camera.CaptureType.PreviewStill
//                               };
//                if (resolution != null)
//                {
//                    PopulateCameraResolutionBasedOnDevice(DeviceInfo.GetOemInfo());
//                    SetResolution(resolution, _camera);
//                }

//                _camera.init();
//                _camera.autoFocusOn();

//                StartCamera(cameraContainer);
//            }
//            catch (Win32Exception win32Ex)
//            {
//                DisposeCamera();
//                return;
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }
//        }

//        public void DisposeCamera()
//        {
//            if (_camera == null) return;
//            _camera.flashOff();
//            _isFlashOn = false;
//            _camera.stop();
//            _camera.release();
//            _cameraState = CameraStateEnum.Disposed;
//            _camera = null;
//            GC.Collect();
//            GC.WaitForPendingFinalizers();
//        }

//        public void StopCamera()
//        {
//            if (_camera != null)
//            {
//                _camera.flashOff();
//                _isFlashOn = false;
//                _camera.stop();
//                _cameraState = CameraStateEnum.Stopped;

//            }
//        }

//        public void StartCamera(Panel cameraContainer)
//        {
//            if (_camera != null)
//            {
//                _camera.run(cameraContainer.Handle);
//                _cameraState = CameraStateEnum.Started;
//            }
//        }

//        public void Resize(Panel cameraContainer, int width, int height)
//        {
//            try
//            {
//                _camera.resize(cameraContainer.Handle, width, height);
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show(ex.Message);
//            }

//        }

//        public void ToggleLight()
//        {
//            if (_camera != null)
//            {
//                if (_isFlashOn)
//                {
//                    _camera.flashOff();
//                    _isFlashOn = false;
//                }
//                else
//                {
//                    _camera.flashOn();
//                    _isFlashOn = true;
//                }
//            }
//        }

//        public string TakeSnapshot(string directory)
//        {
//            DateTime timestamp = DateTime.Now;
//            string fileName = timestamp.ToString("yyyyMMdd-HHmmss") + ".jpg";
//            string filePath = directory + "\\" + fileName;
//            string thumbFilePath = directory + "\\thumb\\" + fileName;

//            _camera.stillImage(filePath);

//            //Ensure that the file is created
//            while (!File.Exists(filePath))
//                Thread.Sleep(100);

//            //Ensure that the file is released
//            GC.Collect();
//            GC.WaitForPendingFinalizers();

//            //Dispose camera to free memory
//            DisposeCamera();
//            Bitmap bmp;
//            try
//            {
//                bmp = new Bitmap(filePath);
//            }
//            catch
//            {
//                // Generally, you should try to catch an explicit
//                // exception, but I know that this can throw 2 different
//                // types of exceptions on an OOM.
//                GC.Collect();
//                // An exception will be thrown if still OOM
//                bmp = new Bitmap(filePath);
//            }

//            CreateThumbnail(bmp, thumbFilePath);
//            DrawTimestampOnBitmap(bmp, timestamp);
//            bmp.Save(filePath, ImageFormat.Jpeg);
//            Thread.Sleep(2000);
//            bmp.Dispose();
//            return thumbFilePath;//fileName;
//        }

//        private void CreateThumbnail(Image originalImage, string destination)
//        {
//            Bitmap thumbnail = new Bitmap(128, 95);//40, 40);
//            Graphics g = Graphics.FromImage(thumbnail);
//            Rectangle rectDestination = new Rectangle(0, 0, 128, 95);//40, 40);
//            g.DrawImage(originalImage, rectDestination, new Rectangle(0, 0, originalImage.Width, originalImage.Height), GraphicsUnit.Pixel);

//            thumbnail.Save(destination, ImageFormat.Jpeg);
//            thumbnail.Dispose();
//            g.Dispose();

//        }

//        private void DrawTimestampOnBitmap(Image originalImage, DateTime timestamp)
//        {
//            Graphics g = Graphics.FromImage(originalImage);
//            string timestampText = timestamp.ToString("dd MMM yyyy - HH:mm:ss");
//            Font timeStampFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Regular);
//            SizeF size = g.MeasureString(timestampText, timeStampFont);
//            g.DrawString(timestampText, timeStampFont, new SolidBrush(Color.White), new RectangleF(originalImage.Width - size.Width, originalImage.Height - size.Height, size.Width, size.Height));
//            g.DrawImage(originalImage, 0, 0);
//            g.Dispose();
//        }


//        private void PopulateCameraResolutionBasedOnDevice(string device)
//        {
//            if (device.Equals("MOTOROLA MC75A") || device.Equals("Motorola MC65"))
//            {
//                _currentCameraResolution = new Hashtable(3)
//                                               {
//                                                   {"small", "320X240"},
//                                                   {"medium", "640X480"},
//                                                   {"large", "800X600"}
//                                               };
//            }
//            else if (device.Equals("Motorola ES400"))
//            {
//                _currentCameraResolution = new Hashtable(3)
//                                               {
//                                                   {"small", "320X240"},
//                                                   {"medium", "640X480"},
//                                                   {"large", "800X600"}
//                                               };
//            }
//            else
//            {
//                _currentCameraResolution = new Hashtable(3)
//                                               {
//                                                   {"small", "176x144"},
//                                                   {"medium", "320x240"},
//                                                   {"large", "480x640"}
//                                               };
//            }
//        }

//        private void SetResolution(string resolution, DirectShowNETCF.Camera.Camera camera)
//        {
//            if (camera == null) return;
//            List<string> supportedResolutions = camera.getMediaTypes();
//            if (resolution.Equals("small"))
//            {
//                int resolutionId = supportedResolutions.FindIndex(s => s == (string)_currentCameraResolution["small"]);
//                if (resolutionId != -1)
//                {
//                    camera.setMediaType(resolutionId);
//                }
//                else
//                {
//                    camera.setMediaType(0);
//                }
//            }
//            else if (resolution.Equals("medium"))
//            {
//                int resolutionId = supportedResolutions.FindIndex(s => s == (string)_currentCameraResolution["medium"]);
//                if (resolutionId != -1)
//                {
//                    camera.setMediaType(resolutionId);
//                }
//                else
//                {
//                    camera.setMediaType(1);
//                }
//            }
//            else if (resolution.Equals("large"))
//            {
//                int resolutionId = supportedResolutions.FindIndex(s => s == (string)_currentCameraResolution["large"]);
//                if (resolutionId != -1)
//                {
//                    camera.setMediaType(resolutionId);
//                }
//                else
//                {
//                    camera.setMediaType(2);
//                }
//            }
//        }
//    }
//}

﻿using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormDisplayStopList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDisplayStopList));
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationStatus = new System.Windows.Forms.ImageList();
            this.imageListOperationType = new System.Windows.Forms.ImageList();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.listOperations = new Resco.Controls.AdvancedList.AdvancedList();
            this.lblTripId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblStopId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblTripIdHead = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblStopIdHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCell1 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.imageCell2 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellOperationOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.TextLocation = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate2 = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCell3 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.imageCell4 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCell2 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellAlternateOperationOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.textCell3 = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate3 = new Resco.Controls.AdvancedList.RowTemplate();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblTripId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStopId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTripIdHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStopIdHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            this.imageListOperationStatus.Images.Clear();
            this.imageListOperationStatus.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
            this.imageListOperationStatus.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            this.imageListOperationStatus.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.imageListOperationType.Images.Clear();
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource4"))));
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.listOperations);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblTripId);
            this.touchPanel.Controls.Add(this.lblStopId);
            this.touchPanel.Controls.Add(this.lblTripIdHead);
            this.touchPanel.Controls.Add(this.lblStopIdHeading);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // listOperations
            // 
            this.listOperations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listOperations.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listOperations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listOperations.DataRows.Clear();
            this.listOperations.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            resources.GetString("listOperations.HeaderRow")});
            this.listOperations.Location = new System.Drawing.Point(0, 40);
            this.listOperations.MultiSelect = true;
            this.listOperations.Name = "listOperations";
            this.listOperations.ScrollbarSmallChange = 32;
            this.listOperations.ScrollbarWidth = 26;
            this.listOperations.SelectedTemplateIndex = 2;
            this.listOperations.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listOperations.ShowHeader = true;
            this.listOperations.Size = new System.Drawing.Size(480, 340);
            this.listOperations.TabIndex = 2;
            this.listOperations.TemplateIndex = 1;
            this.listOperations.Templates.Add(this.rowTemplate1);
            this.listOperations.Templates.Add(this.rowTemplate2);
            this.listOperations.Templates.Add(this.rowTemplate3);
            this.listOperations.TouchScrolling = true;
            this.listOperations.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // lblTripId
            // 
            this.lblTripId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblTripId.Location = new System.Drawing.Point(100, 380);
            this.lblTripId.Name = "lblTripId";
            this.lblTripId.Size = new System.Drawing.Size(21, 14);
            this.lblTripId.Text = "Abc";
            // 
            // lblStopId
            // 
            this.lblStopId.Location = new System.Drawing.Point(100, 420);
            this.lblStopId.Name = "lblStopId";
            this.lblStopId.Size = new System.Drawing.Size(20, 15);
            this.lblStopId.Text = "Abc";
            // 
            // lblTripIdHead
            // 
            this.lblTripIdHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblTripIdHead.Location = new System.Drawing.Point(3, 380);
            this.lblTripIdHead.Name = "lblTripIdHead";
            this.lblTripIdHead.Size = new System.Drawing.Size(0, 0);
            // 
            // lblStopIdHeading
            // 
            this.lblStopIdHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblStopIdHeading.Location = new System.Drawing.Point(3, 420);
            this.lblStopIdHeading.Name = "lblStopIdHeading";
            this.lblStopIdHeading.Size = new System.Drawing.Size(0, 0);
            // 
            // textCell1
            // 
            this.textCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell1.CellSource.ConstantData = "Operation List";
            this.textCell1.DesignName = "textCell1";
            this.textCell1.Location = new System.Drawing.Point(0, 0);
            this.textCell1.Size = new System.Drawing.Size(-1, 32);
            this.textCell1.Visible = false;
            // 
            // rowTemplate1
            // 
            this.rowTemplate1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rowTemplate1.CellTemplates.Add(this.textCell1);
            this.rowTemplate1.Height = 0;
            this.rowTemplate1.Name = "rowTemplate1";
            // 
            // imageCell1
            // 
            this.imageCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCell1.AutoResize = true;
            this.imageCell1.CellSource.ColumnName = "StopType";
            this.imageCell1.DesignName = "imageCell1";
            this.imageCell1.ImageList = this.imageListOperationType;
            this.imageCell1.Location = new System.Drawing.Point(15, 10);
            this.imageCell1.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // imageCell2
            // 
            this.imageCell2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCell2.CellSource.ColumnName = "Status";
            this.imageCell2.DesignName = "imageCell2";
            this.imageCell2.ImageList = this.imageListOperationStatus;
            this.imageCell2.Location = new System.Drawing.Point(430, 0);
            this.imageCell2.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationStopId.Visible = false;
            // 
            // TextCellOperationOrderNumber
            // 
            this.TextCellOperationOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellOperationOrderNumber.CellSource.ColumnName = "TripId";
            this.TextCellOperationOrderNumber.DesignName = "TextCellOperationOrderNumber";
            this.TextCellOperationOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellOperationOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellOperationOrderNumber.Visible = false;
            // 
            // TextLocation
            // 
            this.TextLocation.CellSource.ColumnName = "Location";
            this.TextLocation.DesignName = "TextLocation";
            this.TextLocation.Location = new System.Drawing.Point(0, 0);
            this.TextLocation.Size = new System.Drawing.Size(-1, 64);
            this.TextLocation.Visible = false;
            // 
            // rowTemplate2
            // 
            this.rowTemplate2.CellTemplates.Add(this.imageCell1);
            this.rowTemplate2.CellTemplates.Add(this.textCellOperationDetail);
            this.rowTemplate2.CellTemplates.Add(this.imageCell2);
            this.rowTemplate2.CellTemplates.Add(this.textCellOperationOperationId);
            this.rowTemplate2.CellTemplates.Add(this.textCellOperationStopId);
            this.rowTemplate2.CellTemplates.Add(this.TextCellOperationOrderNumber);
            this.rowTemplate2.CellTemplates.Add(this.TextLocation);
            this.rowTemplate2.Height = 64;
            this.rowTemplate2.Name = "rowTemplate2";
            // 
            // imageCell3
            // 
            this.imageCell3.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCell3.AutoResize = true;
            this.imageCell3.CellSource.ColumnName = "StopType";
            this.imageCell3.DesignName = "imageCell3";
            this.imageCell3.ImageList = this.imageListOperationType;
            this.imageCell3.Location = new System.Drawing.Point(15, 10);
            this.imageCell3.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // imageCell4
            // 
            this.imageCell4.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCell4.CellSource.ColumnName = "Status";
            this.imageCell4.DesignName = "imageCell4";
            this.imageCell4.ImageList = this.imageListOperationStatus;
            this.imageCell4.Location = new System.Drawing.Point(430, 0);
            this.imageCell4.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;
            // 
            // textCell2
            // 
            this.textCell2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell2.CellSource.ColumnName = "StopId";
            this.textCell2.DesignName = "textCell2";
            this.textCell2.Location = new System.Drawing.Point(0, 0);
            this.textCell2.Size = new System.Drawing.Size(0, 64);
            this.textCell2.Visible = false;
            // 
            // TextCellAlternateOperationOrderNumber
            // 
            this.TextCellAlternateOperationOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellAlternateOperationOrderNumber.CellSource.ColumnName = "OrderNumber";
            this.TextCellAlternateOperationOrderNumber.DesignName = "TextCellAlternateOperationOrderNumber";
            this.TextCellAlternateOperationOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellAlternateOperationOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellAlternateOperationOrderNumber.Visible = false;
            // 
            // textCell3
            // 
            this.textCell3.CellSource.ColumnName = "Location";
            this.textCell3.DesignName = "textCell3";
            this.textCell3.Location = new System.Drawing.Point(0, 0);
            this.textCell3.Size = new System.Drawing.Size(-1, 64);
            this.textCell3.Visible = false;
            // 
            // rowTemplate3
            // 
            this.rowTemplate3.BackColor = System.Drawing.Color.LightGray;
            this.rowTemplate3.CellTemplates.Add(this.imageCell3);
            this.rowTemplate3.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.rowTemplate3.CellTemplates.Add(this.imageCell4);
            this.rowTemplate3.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.rowTemplate3.CellTemplates.Add(this.textCell2);
            this.rowTemplate3.CellTemplates.Add(this.TextCellAlternateOperationOrderNumber);
            this.rowTemplate3.CellTemplates.Add(this.textCell3);
            this.rowTemplate3.Height = 64;
            this.rowTemplate3.Name = "rowTemplate3";
            // 
            // FormDisplayStopList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormDisplayStopList";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblTripId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStopId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTripIdHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStopIdHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList imageListOperationStatus;
        private System.Windows.Forms.ImageList imageListOperationType;


        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listOperations;
        private Resco.Controls.CommonControls.TransparentLabel lblTripId;
        private Resco.Controls.CommonControls.TransparentLabel lblStopId;
        private Resco.Controls.CommonControls.TransparentLabel lblTripIdHead;
        private Resco.Controls.CommonControls.TransparentLabel lblStopIdHeading;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell textCellHeaderTemplate;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell imageCellOperationType;
        private Resco.Controls.AdvancedList.ImageCell imageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationTripId;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell imageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.ImageCell imageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationTripId;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell1;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate2;
        private Resco.Controls.AdvancedList.ImageCell imageCell1;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.ImageCell imageCell2;
        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedList.TextCell TextCellOperationOrderNumber;
        private Resco.Controls.AdvancedList.TextCell TextLocation;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate3;
        private Resco.Controls.AdvancedList.ImageCell imageCell3;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.ImageCell imageCell4;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell textCell2;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateOperationOrderNumber;
        private Resco.Controls.AdvancedList.TextCell textCell3;
    }
}
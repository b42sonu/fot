﻿using System.Windows.Forms;
using PreCom.Controls;
using Resco.Controls.CommonControls;

namespace SampleScreenDesigner.Forms
{
    partial class FormManualRbt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblStop = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtBoxTrip = new PreComInput2();
            this.txtBoxRoute = new PreComInput2();
            this.txtBoxLoadCarrierId = new PreComInput2();
            this.txtBoxPowerUnit = new PreComInput2();
            this.lblTrip = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblRoute = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblLoadCarrierId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPowerUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblEnterTripInfo = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblModuleName = new TransparentLabel();

            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtBoxStop = new PreComInput2();
            this.touchPanel.SuspendLayout();

            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.txtBoxStop);
            this.touchPanel.Controls.Add(this.lblStop);
            this.touchPanel.Controls.Add(this.txtBoxTrip);
            this.touchPanel.Controls.Add(this.txtBoxRoute);
            this.touchPanel.Controls.Add(this.txtBoxLoadCarrierId);
            this.touchPanel.Controls.Add(this.txtBoxPowerUnit);
            this.touchPanel.Controls.Add(this.lblTrip);
            this.touchPanel.Controls.Add(this.lblRoute);
            this.touchPanel.Controls.Add(this.lblLoadCarrierId);
            this.touchPanel.Controls.Add(this.lblPowerUnit);
            this.touchPanel.Controls.Add(this.lblEnterTripInfo);
            this.touchPanel.Controls.Add(this.lblModuleName);

            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;

            // 
            // lblStop
            // 
            this.lblStop.Location = new System.Drawing.Point(15, 325);
            this.lblStop.Name = "lblStop";
            this.lblStop.Size = new System.Drawing.Size(261, 42);
            this.lblStop.Text = "Stop";

            // 
            // txtBoxTrip
            // 
            this.txtBoxTrip.Location = new System.Drawing.Point(176, 275);
            this.txtBoxTrip.Name = "txtBoxTrip";
            this.txtBoxTrip.Size = new System.Drawing.Size(216, 21);
            this.txtBoxTrip.TabIndex = 4;
            this.txtBoxTrip.KeyUp += new KeyEventHandler(this.TxtBoxTripKeyUp);
            // 
            // txtBoxRoute
            // 
            this.txtBoxRoute.Location = new System.Drawing.Point(176, 225);
            this.txtBoxRoute.Name = "txtBoxRoute";
            this.txtBoxRoute.Size = new System.Drawing.Size(216, 21);
            this.txtBoxRoute.TabIndex = 3;
            this.txtBoxRoute.KeyUp += new KeyEventHandler(this.TxtBoxRouteKeyUp);
            // 
            // txtBoxLoadCarrierId
            // 
            this.txtBoxLoadCarrierId.Location = new System.Drawing.Point(176, 175);
            this.txtBoxLoadCarrierId.Name = "txtBoxLoadCarrierId";
            this.txtBoxLoadCarrierId.Size = new System.Drawing.Size(216, 21);
            this.txtBoxLoadCarrierId.TabIndex = 20;
            this.txtBoxLoadCarrierId.TabIndex = 2;
            this.txtBoxLoadCarrierId.KeyUp += new KeyEventHandler(this.TxtBoxLoadCarrierIdKeyUp);
            // 
            // txtBoxPowerUnit
            // 
            this.txtBoxPowerUnit.Location = new System.Drawing.Point(176, 125);
            this.txtBoxPowerUnit.Name = "txtBoxPoerUnit";
            this.txtBoxPowerUnit.Size = new System.Drawing.Size(216, 21);
            this.txtBoxPowerUnit.TabIndex = 1;
            this.txtBoxPowerUnit.KeyUp += new KeyEventHandler(this.TxtBoxPowerUnitKeyUp);
            // 
            // lblTrip
            // 
            this.lblTrip.Location = new System.Drawing.Point(15, 275);
            this.lblTrip.Name = "lblTrip";
            this.lblTrip.Size = new System.Drawing.Size(150, 40);
            this.lblTrip.Text = "Trip";

            // 
            // lblRoute
            // 
            this.lblRoute.Location = new System.Drawing.Point(15, 225);
            this.lblRoute.Name = "lblRoute";
            this.lblRoute.Size = new System.Drawing.Size(150, 40);
            this.lblRoute.Text = "Route";
            // 
            // lblLoadCarrierId
            // 
            this.lblLoadCarrierId.Location = new System.Drawing.Point(15, 175);
            this.lblLoadCarrierId.Name = "lblLoadCarrierId";
            this.lblLoadCarrierId.Size = new System.Drawing.Size(150, 40);
            this.lblLoadCarrierId.Text = "LoadCarrierId";
            // 
            // lblPowerUnit
            // 
            this.lblPowerUnit.Location = new System.Drawing.Point(15, 125);
            this.lblPowerUnit.Name = "lblPowerUnit";
            this.lblPowerUnit.Size = new System.Drawing.Size(130, 40);
            this.lblPowerUnit.Text = "PowerUnit";
            // 
            // lblEnterTripInfo
            // 
            this.lblEnterTripInfo.Location = new System.Drawing.Point(12, 70);
            this.lblEnterTripInfo.Name = "lblEnterTripInfo";
            this.lblEnterTripInfo.Size = new System.Drawing.Size(250, 48);
            this.lblEnterTripInfo.Text = "Enter trip information";
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.Text = "Delivery to PO/PiB by customer";
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;



            // 
            // txtBoxStop
            // 
            this.txtBoxStop.Location = new System.Drawing.Point(176, 325);
            this.txtBoxStop.Name = "txtBoxStop";
            this.txtBoxStop.Size = new System.Drawing.Size(216, 21);
            this.txtBoxStop.TabIndex = 5;
            this.txtBoxStop.KeyUp += new KeyEventHandler(this.TxtBoxStopKeyUp);

            // 
            // FormCorrectWeightAndVolume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormManualRbt";
            this.Text = "FormManualRbt";
            this.touchPanel.ResumeLayout(false);

            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }



        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblTrip;
        private Resco.Controls.CommonControls.TransparentLabel lblRoute;
        private Resco.Controls.CommonControls.TransparentLabel lblLoadCarrierId;
        private Resco.Controls.CommonControls.TransparentLabel lblPowerUnit;
        private Resco.Controls.CommonControls.TransparentLabel lblEnterTripInfo;
        private PreCom.Controls.PreComInput2 txtBoxPowerUnit;
        private PreCom.Controls.PreComInput2 txtBoxTrip;
        private PreCom.Controls.PreComInput2 txtBoxRoute;
        private PreCom.Controls.PreComInput2 txtBoxLoadCarrierId;
        private Resco.Controls.CommonControls.TransparentLabel lblStop;
        private PreCom.Controls.PreComInput2 txtBoxStop;
    }
}
﻿namespace SampleScreenDesigner.Forms
{
    partial class FormScanLoadCarier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.lblActivateCarier = new System.Windows.Forms.Label();
            this.lblPrinterNameValue = new System.Windows.Forms.Label();
            this.lblDestValue = new System.Windows.Forms.Label();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.preComInputScanInput = new PreCom.Controls.PreComInput2();
            this.lblScan = new System.Windows.Forms.Label();
            this.lblLoadCarierValue = new System.Windows.Forms.Label();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblPrinterNameText = new System.Windows.Forms.Label();
            this.lblDestText = new System.Windows.Forms.Label();
            this.lblLoadCarierText = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.lblModuleName = new System.Windows.Forms.Label();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOkEnter = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBlank = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOkEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).BeginInit();
            this.SuspendLayout();
            // 
            // lblActivateCarier
            // 
            this.lblActivateCarier.Location = new System.Drawing.Point(15, 107);
            this.lblActivateCarier.Name = "lblActivateCarier";
            this.lblActivateCarier.Size = new System.Drawing.Size(452, 40);
            this.lblActivateCarier.Text = "Activate carier for printing";
            // 
            // lblPrinterNameValue
            // 
            this.lblPrinterNameValue.Location = new System.Drawing.Point(111, 427);
            this.lblPrinterNameValue.Name = "lblPrinterNameValue";
            this.lblPrinterNameValue.Size = new System.Drawing.Size(356, 40);
            this.lblPrinterNameValue.Text = "N2808_LAS   e-mail is unknown";
            // 
            // lblDestValue
            // 
            this.lblDestValue.Location = new System.Drawing.Point(111, 387);
            this.lblDestValue.Name = "lblDestValue";
            this.lblDestValue.Size = new System.Drawing.Size(366, 40);
            this.lblDestValue.Text = "3045 Bakkeryd og Sonn";
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.preComInputScanInput);
            this.touchPanel.Controls.Add(this.lblScan);
            this.touchPanel.Controls.Add(this.lblActivateCarier);
            this.touchPanel.Controls.Add(this.lblPrinterNameValue);
            this.touchPanel.Controls.Add(this.lblDestValue);
            this.touchPanel.Controls.Add(this.lblLoadCarierValue);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.lblPrinterNameText);
            this.touchPanel.Controls.Add(this.lblDestText);
            this.touchPanel.Controls.Add(this.lblLoadCarierText);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnOkEnter);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // preComInputScanInput
            // 
            this.preComInputScanInput.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.preComInputScanInput.Location = new System.Drawing.Point(15, 193);
            this.preComInputScanInput.Name = "preComInputScanInput";
            this.preComInputScanInput.Size = new System.Drawing.Size(436, 48);
            this.preComInputScanInput.TabIndex = 24;
            this.preComInputScanInput.TextTranslation = false;
            // 
            // lblScan
            // 
            this.lblScan.Location = new System.Drawing.Point(15, 150);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(452, 40);
            this.lblScan.Text = "Scan or enter log. load carrier";
            // 
            // lblLoadCarierValue
            // 
            this.lblLoadCarierValue.Location = new System.Drawing.Point(111, 344);
            this.lblLoadCarierValue.Name = "lblLoadCarierValue";
            this.lblLoadCarierValue.Size = new System.Drawing.Size(366, 40);
            this.lblLoadCarierValue.Text = "76543232321";
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(15, 244);
            this.MsgMessage.MessageText = "";
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(436, 97);
            this.MsgMessage.TabIndex = 14;
            // 
            // lblPrinterNameText
            // 
            this.lblPrinterNameText.Location = new System.Drawing.Point(15, 430);
            this.lblPrinterNameText.Name = "lblPrinterNameText";
            this.lblPrinterNameText.Size = new System.Drawing.Size(90, 42);
            this.lblPrinterNameText.Text = "Printer:";
            // 
            // lblDestText
            // 
            this.lblDestText.Location = new System.Drawing.Point(15, 387);
            this.lblDestText.Name = "lblDestText";
            this.lblDestText.Size = new System.Drawing.Size(90, 40);
            this.lblDestText.Text = "DEST:";
            // 
            // lblLoadCarierText
            // 
            this.lblLoadCarierText.Location = new System.Drawing.Point(15, 344);
            this.lblLoadCarierText.Name = "lblLoadCarierText";
            this.lblLoadCarierText.Size = new System.Drawing.Size(90, 40);
            this.lblLoadCarierText.Text = "LB:";
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(15, 56);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(462, 48);
            this.lblHeading.Text = "OP OSLO OG AKERSHUS VAREBIL1";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(142, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(217, 50);
            this.lblModuleName.Text = "Print voucher";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Back";
            // 
            // btnOkEnter
            // 
            this.btnOkEnter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOkEnter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOkEnter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOkEnter.ForeColor = System.Drawing.Color.White;
            this.btnOkEnter.Location = new System.Drawing.Point(322, 483);
            this.btnOkEnter.Name = "btnOkEnter";
            this.btnOkEnter.Size = new System.Drawing.Size(158, 50);
            this.btnOkEnter.TabIndex = 7;
            this.btnOkEnter.Text = "OK/Enter";
            this.btnOkEnter.Click += new System.EventHandler(this.btnOkEnter_Click);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // btnBlank
            // 
            this.btnBlank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBlank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBlank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBlank.ForeColor = System.Drawing.Color.White;
            this.btnBlank.Location = new System.Drawing.Point(161, 483);
            this.btnBlank.Name = "btnBlank";
            this.btnBlank.Size = new System.Drawing.Size(158, 50);
            this.btnBlank.TabIndex = 8;
            // 
            // FormScanLoadCarier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Controls.Add(this.btnBlank);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormScanLoadCarier";
            this.Text = "FormPrinterInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOkEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).EndInit();
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.Label lblActivateCarier;
        private System.Windows.Forms.Label lblPrinterNameValue;
        private System.Windows.Forms.Label lblDestValue;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 preComInputScanInput;
        private System.Windows.Forms.Label lblScan;
        private System.Windows.Forms.Label lblLoadCarierValue;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl MsgMessage;
        private System.Windows.Forms.Label lblPrinterNameText;
        private System.Windows.Forms.Label lblDestText;
        private System.Windows.Forms.Label lblLoadCarierText;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOkEnter;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnBlank;
    }
}
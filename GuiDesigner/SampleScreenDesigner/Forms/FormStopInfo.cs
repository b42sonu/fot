﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Windows.Forms;
using SampleScreenDesigner.Controls;
using System.Text;

namespace SampleScreenDesigner.Forms
{



    /// <summary>
    /// This class, shows the list of Loading/Unloading operations, depending on for
    /// which operation type screen is opened.
    /// </summary>
    public partial class FormStopInfo : Form
    {

        private TabMultipleButtons _tabMultipleButtons;

        #region Methods and Functions

        /// <summary>
        /// Default constructor for class.
        /// </summary>
        public FormStopInfo()
        {
            InitializeComponent();
            SetTextToGui();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            SetHtml();
        }
        void SetHtml()
        {
            var html = new StringBuilder();
            html.Append("<table border='1px'>");
            html.Append(CreateRow("Customer", ""));
            html.Append(CreateRow("Services", ""));
            html.Append(CreateRow("Name", ""));
            html.Append(CreateRow("Address", ""));
            html.Append(CreateRow("Goods", ""));
            html.Append(CreateRow("W /Vol", ""));
            html.Append(CreateRow("Goods info", ""));
            html.Append(CreateRow("Order info", ""));
            html.Append(CreateRow("Stop info", ""));
            html.Append(CreateRow("Customer", ""));
            html.Append("</table>");
            webBrowserStopDetails.DocumentText = html.ToString();
        }
        private string CreateRow(string columnName, string columnValue)
        {
            return string.Format("<tr><td width='50%'><b>{0}:</b></td><td width='50%'>{1}</td></tr>", columnName, columnValue);
        }

        private void SetTextToGui()
        {
            _tabMultipleButtons = new TabMultipleButtons();
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonUpClick) { ButtonType = TabButtonType.Cancel });
            _tabMultipleButtons.ListButtons.Add(new TabButton("Mesage", BtnOkClick));
            _tabMultipleButtons.ListButtons.Add(new TabButton("Deliver", ButtonDownClick) { ButtonType = TabButtonType.Confirm });
            _tabMultipleButtons.ListButtons.Add(new TabButton("Details", ButtonUpClick));
            _tabMultipleButtons.ListButtons.Add(new TabButton("Scan", ButtonUpClick));

            touchPanel.Controls.Add(_tabMultipleButtons);
            _tabMultipleButtons.GenerateButtons();
            labelModuleName.Text = GlobalTexts.Operations;

            labelOperationType.Text = GlobalTexts.Operations;
            labelOperationTime.Text = GlobalTexts.Operations;
        }









        #endregion

        #region Events






        private void BtnOkClick(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// This event runs when user clicks on back button and send message to flow that
        /// user clicked back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonUpClick(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// This event runs when user clicks on scan button and send message to flow that user clicked on
        /// scan button and passes parameters that are required by flow to handle the click of scan button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDownClick(object sender, EventArgs e)
        {

        }







        #endregion

        





    }





}
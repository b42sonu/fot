﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.PickUp.Views
{
    partial class FormReconcilliationScannedPickup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonDetails = new Resco.Controls.OutlookControls.ImageButton();
            this.ListConsignments = new Resco.Controls.AdvancedList.AdvancedList();
            this.templateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellHeaderCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellSelectedTextCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedTextCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowUnselected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellUnselectedTextCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedTextCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowItems = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellItemUnselectedTextCol = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemUnselectedlmageCol = new Resco.Controls.AdvancedList.ImageCell();
            this.imageListStatus = new System.Windows.Forms.ImageList();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.buttonBack);
            this.touchPanel.Controls.Add(this.buttonDetails);
            this.touchPanel.Controls.Add(this.ListConsignments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(0, 483);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(158, 50);
            this.buttonBack.TabIndex = 29;
            this.buttonBack.Text = "Back";
            this.buttonBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonDetails.ForeColor = System.Drawing.Color.White;
            this.buttonDetails.Location = new System.Drawing.Point(161, 483);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(158, 50);
            this.buttonDetails.TabIndex = 28;
            this.buttonDetails.Text = "Details";
            this.buttonDetails.Click += new System.EventHandler(this.ButtonDetailsClick);
            // 
            // ListConsignments
            // 
            this.ListConsignments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.ListConsignments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListConsignments.DataRows.Clear();
            this.ListConsignments.GridColor = System.Drawing.Color.Black;
            this.ListConsignments.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.ListConsignments.Location = new System.Drawing.Point(11, 54);
            this.ListConsignments.Name = "ListConsignments";
            this.ListConsignments.ScrollbarSmallChange = 32;
            this.ListConsignments.ScrollbarWidth = 26;
            this.ListConsignments.ShowHeader = true;
            this.ListConsignments.Size = new System.Drawing.Size(459, 323);
            this.ListConsignments.TabIndex = 3;
            this.ListConsignments.Templates.Add(this.templateHeader);
            this.ListConsignments.Templates.Add(this.templateRowSelected);
            this.ListConsignments.Templates.Add(this.templateRowUnselected);
            this.ListConsignments.Templates.Add(this.templateRowItems);
            this.ListConsignments.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            // 
            // templateHeader
            // 
            this.templateHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol1);
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol2);
            this.templateHeader.Height = 28;
            this.templateHeader.Name = "templateHeader";
            // 
            // cellHeaderCol1
            // 
            this.cellHeaderCol1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellHeaderCol1.CellSource.ConstantData = "Consignments";
            this.cellHeaderCol1.DesignName = "cellHeaderCol1";
            this.cellHeaderCol1.Location = new System.Drawing.Point(10, 0);
            this.cellHeaderCol1.Name = "cellHeaderCol1";
            this.cellHeaderCol1.Size = new System.Drawing.Size(280, 28);
            this.cellHeaderCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellHeaderCol2
            // 
            this.cellHeaderCol2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderCol2.CellSource.ConstantData = "Actual/Planned";
            this.cellHeaderCol2.DesignName = "cellHeaderCol2";
            this.cellHeaderCol2.Location = new System.Drawing.Point(280, 0);
            this.cellHeaderCol2.Name = "cellHeaderCol2";
            this.cellHeaderCol2.Size = new System.Drawing.Size(-1, 28);
            this.cellHeaderCol2.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowSelected
            // 
            this.templateRowSelected.BackColor = System.Drawing.Color.DarkGray;
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol1);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol2);
            this.templateRowSelected.Height = 22;
            this.templateRowSelected.Name = "templateRowSelected";
            // 
            // cellSelectedTextCol1
            // 
            this.cellSelectedTextCol1.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellSelectedTextCol1.CellSource.ColumnIndex = 0;
            this.cellSelectedTextCol1.DesignName = "cellSelectedTextCol1";
            this.cellSelectedTextCol1.Location = new System.Drawing.Point(10, 0);
            this.cellSelectedTextCol1.Size = new System.Drawing.Size(280, 22);
            this.cellSelectedTextCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellSelectedTextCol2
            // 
            this.cellSelectedTextCol2.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellSelectedTextCol2.CellSource.ColumnIndex = 1;
            this.cellSelectedTextCol2.DesignName = "cellSelectedTextCol2";
            this.cellSelectedTextCol2.Location = new System.Drawing.Point(280, 0);
            this.cellSelectedTextCol2.Size = new System.Drawing.Size(-1, 22);
            this.cellSelectedTextCol2.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowUnselected
            // 
            this.templateRowUnselected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol1);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol2);
            this.templateRowUnselected.Height = 22;
            this.templateRowUnselected.Name = "templateRowUnselected";
            // 
            // cellUnselectedTextCol1
            // 
            this.cellUnselectedTextCol1.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellUnselectedTextCol1.CellSource.ColumnIndex = 0;
            this.cellUnselectedTextCol1.DesignName = "cellUnselectedTextCol1";
            this.cellUnselectedTextCol1.Location = new System.Drawing.Point(10, 0);
            this.cellUnselectedTextCol1.Size = new System.Drawing.Size(280, 22);
            this.cellUnselectedTextCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellUnselectedTextCol2
            // 
            this.cellUnselectedTextCol2.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellUnselectedTextCol2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cellUnselectedTextCol2.DesignName = "cellUnselectedTextCol2";
            this.cellUnselectedTextCol2.Location = new System.Drawing.Point(280, 0);
            this.cellUnselectedTextCol2.Size = new System.Drawing.Size(-1, 22);
            this.cellUnselectedTextCol2.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowItems
            // 
            this.templateRowItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowItems.CellTemplates.Add(this.cellItemUnselectedTextCol);
            this.templateRowItems.CellTemplates.Add(this.cellItemUnselectedlmageCol);
            this.templateRowItems.Height = 22;
            this.templateRowItems.Name = "templateRowItems";
            // 
            // cellItemUnselectedTextCol
            // 
            this.cellItemUnselectedTextCol.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellItemUnselectedTextCol.DesignName = "cellItemUnselectedTextCol";
            this.cellItemUnselectedTextCol.Location = new System.Drawing.Point(10, 0);
            this.cellItemUnselectedTextCol.Size = new System.Drawing.Size(280, 22);
            // 
            // cellItemUnselectedlmageCol
            // 
            this.cellItemUnselectedlmageCol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellItemUnselectedlmageCol.AutoTransparent = true;
            this.cellItemUnselectedlmageCol.CellSource.ColumnIndex = 1;
            this.cellItemUnselectedlmageCol.DesignName = "cellItemUnselectedlmageCol";
            this.cellItemUnselectedlmageCol.ImageList = this.imageListStatus;
            this.cellItemUnselectedlmageCol.Location = new System.Drawing.Point(280, 0);
            this.cellItemUnselectedlmageCol.Size = new System.Drawing.Size(-1, 22);
            // 
            // imageListStatus
            // 
            this.imageListStatus.ImageSize = new System.Drawing.Size(20, 20);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Pickup";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(322, 483);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(158, 50);
            this.buttonOk.TabIndex = 31;
            this.buttonOk.Text = "Ok";
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // FormReconcilliationScannedPickup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormReconcilliationScannedPickup";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.AdvancedList.AdvancedList ListConsignments;
        private Resco.Controls.AdvancedList.RowTemplate templateHeader;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol1;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelected;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol1;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol2;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselected;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol1;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol2;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol2;
        private System.Windows.Forms.ImageList imageListStatus;
        private Resco.Controls.AdvancedList.RowTemplate templateRowItems;
        private Resco.Controls.AdvancedList.TextCell cellItemUnselectedTextCol;
        private Resco.Controls.AdvancedList.ImageCell cellItemUnselectedlmageCol;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
        private Resco.Controls.OutlookControls.ImageButton buttonDetails;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
    }
}
﻿using System.Windows.Forms;
namespace SampleScreenDesigner.Forms
{
    partial class FormTakePhotoNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.pictureBoxMC70 = new System.Windows.Forms.PictureBox();
            this.panelPopUp = new System.Windows.Forms.Panel();
            this.labelDeletePicture = new System.Windows.Forms.Label();
            this.pictureBoxPopUp = new System.Windows.Forms.PictureBox();
            this.btnDiscard = new Resco.Controls.OutlookControls.ImageButton();
            this.btnSave = new Resco.Controls.OutlookControls.ImageButton();
            this.lableThumbnail = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanelPhotos = new Resco.Controls.CommonControls.TouchPanel();
            this.lableCameraInstruction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lableDeleteInstruction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lableDamageFor = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            this.panelPopUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDiscard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableThumbnail)).BeginInit();
            this.touchPanelPhotos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lableCameraInstruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDeleteInstruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDamageFor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.pictureBoxMC70);
            this.touchPanel.Controls.Add(this.panelPopUp);
            this.touchPanel.Controls.Add(this.lableThumbnail);
            this.touchPanel.Controls.Add(this.touchPanelPhotos);
            this.touchPanel.Controls.Add(this.lableCameraInstruction);
            this.touchPanel.Controls.Add(this.lableDeleteInstruction);
            this.touchPanel.Controls.Add(this.lableDamageFor);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 588);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // pictureBoxMC70
            // 
            this.pictureBoxMC70.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxMC70.Name = "pictureBoxMC70";
            this.pictureBoxMC70.Size = new System.Drawing.Size(480, 482);
            // 
            // panelPopUp
            // 
            this.panelPopUp.Controls.Add(this.labelDeletePicture);
            this.panelPopUp.Controls.Add(this.pictureBoxPopUp);
            this.panelPopUp.Controls.Add(this.btnDiscard);
            this.panelPopUp.Controls.Add(this.btnSave);
            this.panelPopUp.Location = new System.Drawing.Point(4, 79);
            this.panelPopUp.Name = "panelPopUp";
            this.panelPopUp.Size = new System.Drawing.Size(473, 367);
            this.panelPopUp.Visible = false;
            // 
            // labelDeletePicture
            // 
            this.labelDeletePicture.Location = new System.Drawing.Point(133, 260);
            this.labelDeletePicture.Name = "labelDeletePicture";
            this.labelDeletePicture.Size = new System.Drawing.Size(221, 27);
            this.labelDeletePicture.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBoxPopUp
            // 
            this.pictureBoxPopUp.Location = new System.Drawing.Point(79, 13);
            this.pictureBoxPopUp.Name = "pictureBoxPopUp";
            this.pictureBoxPopUp.Size = new System.Drawing.Size(320, 240);
            this.pictureBoxPopUp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // btnDiscard
            // 
            this.btnDiscard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDiscard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDiscard.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDiscard.ForeColor = System.Drawing.Color.White;
            this.btnDiscard.Location = new System.Drawing.Point(22, 305);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(147, 50);
            this.btnDiscard.TabIndex = 33;
            this.btnDiscard.Click += new System.EventHandler(this.BtnDiscardClick);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnSave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(304, 305);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(147, 50);
            this.btnSave.TabIndex = 32;
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // lableThumbnail
            // 
            this.lableThumbnail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lableThumbnail.Location = new System.Drawing.Point(4, 131);
            this.lableThumbnail.Name = "lableThumbnail";
            this.lableThumbnail.Size = new System.Drawing.Size(0, 0);
            // 
            // touchPanelPhotos
            // 
            this.touchPanelPhotos.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanelPhotos.Location = new System.Drawing.Point(0, 160);
            this.touchPanelPhotos.Name = "touchPanelPhotos";
            this.touchPanelPhotos.Size = new System.Drawing.Size(480, 230);
            // 
            // lableCameraInstruction
            // 
            this.lableCameraInstruction.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lableCameraInstruction.Location = new System.Drawing.Point(4, 390);
            this.lableCameraInstruction.Name = "lableCameraInstruction";
            this.lableCameraInstruction.Size = new System.Drawing.Size(0, 0);
            // 
            // lableDeleteInstruction
            // 
            this.lableDeleteInstruction.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lableDeleteInstruction.Location = new System.Drawing.Point(4, 440);
            this.lableDeleteInstruction.Name = "lableDeleteInstruction";
            this.lableDeleteInstruction.Size = new System.Drawing.Size(0, 0);
            // 
            // lableDamageFor
            // 
            this.lableDamageFor.AutoSize = false;
            this.lableDamageFor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lableDamageFor.Location = new System.Drawing.Point(4, 49);
            this.lableDamageFor.Name = "lableDamageFor";
            this.lableDamageFor.Size = new System.Drawing.Size(473, 84);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(115, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormTakePhotoNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormTakePhotoNew";
            this.touchPanel.ResumeLayout(false);
            this.panelPopUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDiscard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableThumbnail)).EndInit();
            this.touchPanelPhotos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lableCameraInstruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDeleteInstruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDamageFor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;


        //private Resco.Controls.CommonControls.TransparentLabel lableItemDetail;
        private Resco.Controls.CommonControls.TransparentLabel lableDamageFor;
        private Resco.Controls.CommonControls.TransparentLabel lableCameraInstruction;
        private Resco.Controls.CommonControls.TransparentLabel lableThumbnail;
        private Resco.Controls.CommonControls.TouchPanel touchPanelPhotos;
        private System.Windows.Forms.Panel panelPopUp;
        private Resco.Controls.OutlookControls.ImageButton btnDiscard;
        private Resco.Controls.OutlookControls.ImageButton btnSave;
        private System.Windows.Forms.PictureBox pictureBoxPopUp;

        private Resco.Controls.CommonControls.TransparentLabel lableDeleteInstruction;
        private Label labelDeletePicture;
        private PictureBox pictureBoxMC70;


    }
}
﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    partial class FormAttemptedPickup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelDescription = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.cbMeasure = new System.Windows.Forms.ComboBox();
            this.cbCause = new System.Windows.Forms.ComboBox();
            this.buttonCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.editConsignmentNumber = new PreCom.Controls.PreComInput2();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.AutoScroll = false;
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelDescription);
            this.touchPanel.Controls.Add(this.labelMeasure);
            this.touchPanel.Controls.Add(this.labelCause);
            this.touchPanel.Controls.Add(this.cbMeasure);
            this.touchPanel.Controls.Add(this.cbCause);
            this.touchPanel.Controls.Add(this.buttonCancel);
            this.touchPanel.Controls.Add(this.editConsignmentNumber);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelDescription
            // 
            this.labelDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelDescription.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelDescription.Location = new System.Drawing.Point(22, 290);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(107, 24);
            this.labelDescription.Text = "Description";
            // 
            // labelMeasure
            // 
            this.labelMeasure.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMeasure.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelMeasure.Location = new System.Drawing.Point(22, 170);
            this.labelMeasure.Name = "labelMeasure";
            this.labelMeasure.Size = new System.Drawing.Size(83, 24);
            this.labelMeasure.Text = "Measure";
            this.labelMeasure.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // labelCause
            // 
            this.labelCause.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelCause.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelCause.Location = new System.Drawing.Point(22, 82);
            this.labelCause.Name = "labelCause";
            this.labelCause.Size = new System.Drawing.Size(62, 24);
            this.labelCause.Text = "Cause";
            this.labelCause.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // cbMeasure
            // 
            this.cbMeasure.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.cbMeasure.Location = new System.Drawing.Point(22, 196);
            this.cbMeasure.Name = "cbMeasure";
            this.cbMeasure.Size = new System.Drawing.Size(448, 37);
            this.cbMeasure.TabIndex = 30;
            // 
            // cbCause
            // 
            this.cbCause.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.cbCause.Location = new System.Drawing.Point(22, 108);
            this.cbCause.Name = "cbCause";
            this.cbCause.Size = new System.Drawing.Size(448, 37);
            this.cbCause.TabIndex = 29;
            this.cbCause.SelectedIndexChanged += new System.EventHandler(this.OnCauseChanged);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 483);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(238, 50);
            this.buttonCancel.TabIndex = 27;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancelClick);
            // 
            // editConsignmentNumber
            // 
            this.editConsignmentNumber.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.editConsignmentNumber.Location = new System.Drawing.Point(22, 316);
            this.editConsignmentNumber.Multiline = true;
            this.editConsignmentNumber.Name = "editConsignmentNumber";
            this.editConsignmentNumber.Size = new System.Drawing.Size(436, 147);
            this.editConsignmentNumber.TabIndex = 23;
            this.editConsignmentNumber.TextTranslation = false;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Attempted Pickup";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(242, 483);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(238, 50);
            this.buttonOk.TabIndex = 26;
            this.buttonOk.Text = "Ok/Enter";
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // labelTask
            // 
            this.labelTask.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelTask.Location = new System.Drawing.Point(22, 53);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(334, 24);
            this.labelTask.Text = "Scan or enter consignment number";
            // 
            // FormAttemptedPickup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormAttemptedPickup";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private PreCom.Controls.PreComInput2 editConsignmentNumber;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private Resco.Controls.OutlookControls.ImageButton buttonCancel;
        private System.Windows.Forms.ComboBox cbMeasure;
        private System.Windows.Forms.ComboBox cbCause;
        private Resco.Controls.CommonControls.TransparentLabel labelMeasure;
        private Resco.Controls.CommonControls.TransparentLabel labelCause;
        private Resco.Controls.CommonControls.TransparentLabel labelDescription;
    }
}
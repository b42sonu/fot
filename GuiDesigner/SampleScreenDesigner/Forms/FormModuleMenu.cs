﻿using System;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.OutlookControls;
using SampleScreenDesigner.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Forms
{
    public partial class FormModuleMenu : Form
    {
        public FormModuleMenu()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            _currentPage = 0;
            ModuleMenu moduleMenu = ModuleMenu.Create();
            CreateMenu(moduleMenu);
            AddTabButtons();
        }

        void AddTabButtons()
        {
            tabMultipleButtons.ListButtons.Add(new TabButton("Back", ButtonBackClick, TabButtonType.Cancel));
            tabMultipleButtons.ListButtons.Add(new TabButton("Next", ButtonNextClick, ButtonNextName));
            tabMultipleButtons.GenerateButtons();
        }
        private ModuleMenuItem[] _menuItems;
        private Button[] _buttons;
        private int _currentPage { get; set; }
        private int _pageSize { get; set; }
        private const string ButtonNextName = "ButtonNextName";
        TabMultipleButtons tabMultipleButtons=new TabMultipleButtons();
        private void CreateMenu(ModuleMenu moduleMenu)
        {
            const int ctrlHeight = 40; //Specifies the height for each control.
            const int ctrlSpace = 20; // Specifies the vertical space between rows of controls.
            int yPos = 65; // Specifies the initial vertical point from where drawing of controls will start
            SuspendLayout();

            // var size = new Size(400, 50);
            _menuItems = moduleMenu.GetItems("Driver2").ToArray();

            // int max = _menuItems.Count();
            // //TODO: assert max < 12

            // if (max > 6)
            // {
            //     max = 6;
            //     _buttons = new Button[6];
            // }

            // for (int i = 0; i < max; i++ )
            // {
            //     var menuItem = _menuItems[i];

            //     var btnMenuItem = CreateButton(menuItem, size, yPos);
            //     if (_buttons != null)
            //         _buttons[i] = btnMenuItem;

            //     btnMenuItem.Tag = menuItem;
            //     touchPanel.Controls.Add(btnMenuItem);

            //     //Update vertical point by adding control space and control height.
            //     //yPos = yPos + ctrlHeight + ctrlSpace;
            // }

            // if (_menuItems.Count() > 6)
            // {
            //     touchPanel.Controls.Add(CreatePageButton(size, yPos + 15)); 
            // }

            CreatePageButtons();
            ResumeLayout(false);
        }

        private Button CreatePageButton(Size size, int yPos)
        {
            Button button = new Button
            {
                Location = new Point(45, yPos),
                ClientSize = size,
                Text = "Next page",
                ForeColor = Color.White,
                BackColor = Color.FromArgb(46, 48, 50),
                Font = new Font("Arial", 9F, FontStyle.Regular),
                Tag = true,
            };
            button.Click += OnPageButtonClicked;
            return button;
        }

        private void CreatePageButtons()
        {
            int x = 0, y = 0;
            Color backolor = Color.FromArgb(46, 48, 50), foreColor = Color.White;
            Size buttonSize = new Size(209, 100);
            int buttonSpace = 20;
            int pageSize = 6;
            _pageSize = _menuItems.Length / 6;
            if (_menuItems.Length % 6 > 0)
                _pageSize++;

            panelButtonContainer.Controls.Clear();
            int i = (_currentPage * pageSize);
            for (int count = 0; i < _menuItems.Length && count < 6; count++, i++)
            {
                ModuleMenuItem moduleMenuItem = _menuItems[i];
                var button = new ImageButton();
                button.BackColor = backolor;
                button.ForeColor = foreColor;
                button.Size = buttonSize;
                button.Location = new Point(x, y);
                button.Tag = _menuItems[count];
                button.ClientSize = buttonSize;
                button.Text = moduleMenuItem.Text;
                Font = new Font("Arial", 9F, FontStyle.Regular);
                //button.Click += OnMenuItemClicked;
                panelButtonContainer.Controls.Add(button);
                if (count % 2 == 0)
                {
                    x = buttonSize.Width + buttonSpace;
                }
                else
                {
                    x = 0;
                    y += buttonSize.Height + buttonSpace;
                }
            }
            ShowPageNumber();
        }

        private void OnPageButtonClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            if ((bool)button.Tag)
            {
                ShowPage2();
                button.Tag = false;
                button.Text = "Next page";
            }
            else
            {
                ShowPage1();
                button.Tag = true;
                button.Text = "Previous page";
            }
        }

        private void ShowPage1()
        {
            for (int i = 0; i < 6; i++)
            {
                var button = _buttons[i];
                button.Tag = _menuItems[i];
                button.Text = _menuItems[i].Text;
                button.Visible = true;
            }
        }

        private void ShowPage2()
        {
            int max = _menuItems.Count();
            for (int i = 6; i < 12; i++)
            {
                var button = _buttons[i - 6];
                if (i < max)
                {
                    button.Tag = _menuItems[i];
                    button.Text = _menuItems[i].Text;
                }
                else
                {
                    button.Visible = false;
                }
            }
        }

        Button CreateButton(ModuleMenuItem moduleMenuItem, Size size, int yPos)
        {
            return new Button
            {
                Location = new Point(45, yPos),
                ClientSize = size,
                Text = moduleMenuItem.Text,
                ForeColor = Color.White,
                BackColor = Color.FromArgb(46, 48, 50),
                Font = new Font("Arial", 9F, FontStyle.Regular),
                Tag = moduleMenuItem
            };
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
            if (_currentPage > 0)
            {
                _currentPage--;
                CreatePageButtons();
                if (_currentPage == _pageSize - 2)//if next button is disabled enable it
                    SetNextBuuton(true);

            }
            else
            {
                this.Close();
            }
        }

        private void ButtonNextClick(object sender, EventArgs e)
        {
            if (_currentPage < _pageSize - 1)
            {
                _currentPage++;
                CreatePageButtons();
                if (_currentPage == _pageSize - 1)
                    SetNextBuuton(false);
            }

        }
        void SetNextBuuton(bool status)
        {
            tabMultipleButtons.HideButton(ButtonNextName, status);
        }
        void ShowPageNumber()
        {
            labelPageNumber.Text = (_currentPage + 1) + "/" + _pageSize;
        }
    }

    public class ModuleMenuItem
    {
        public string Text { get; set; }
        public String[] Roles;
    }

    public class ModuleMenu
    {
        public string ModuleName { get; set; }
        public ModuleMenuItem[] MenuItems;

        public IEnumerable<ModuleMenuItem> GetItems(string roleName)
        {
            var result = new List<ModuleMenuItem>();

            foreach (var moduleMenuItem in MenuItems)
            {
                if (moduleMenuItem.Roles.Contains(roleName))
                    result.Add(moduleMenuItem);
            }
            return result;
        }


        public static ModuleMenu Create()
        {
            ModuleMenu moduleMenu = new ModuleMenu();

            moduleMenu.MenuItems = new[]
                                       {
                                           new ModuleMenuItem
                                               {
                                                   Text = "Change product group",
                                                   Roles = new[] {"Customer", "Driver2"}
                                               },
                                           new ModuleMenuItem
                                               {
                                                   Text = "Item2",
                                                   Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                               },
                                           new ModuleMenuItem
                                               {
                                                   Text = "Item3",
                                                   Roles = new[] {"Driver2", "PostOfficeAndPostInStore", "Driver1"}
                                               },
                                           new ModuleMenuItem
                                           {
                                               Text = "Item4",
                                               Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                           },
                                           new ModuleMenuItem
                                           {
                                               Text = "Item5",
                                               Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                           },
                                           new ModuleMenuItem
                                           {
                                               Text = "Item6",
                                               Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                           },
                                           new ModuleMenuItem
                                               {
                                                   Text = "Item7",
                                                   Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                               },

                                           //new ModuleMenuItem
                                           //    {
                                           //        Text = "Item8",
                                           //        Roles = new[] {"Customer", "Driver2"}
                                           //    },
                                           //new ModuleMenuItem
                                           //    {
                                           //        Text = "Item9",
                                           //        Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                           //    },
                                           //new ModuleMenuItem
                                           //    {
                                           //        Text = "Item10",
                                           //        Roles = new[] {"Driver2", "PostOfficeAndPostInStore", "Driver1"}
                                           //    },
                                           //new ModuleMenuItem
                                           //{
                                           //    Text = "Item11",
                                           //    Roles = new[] {"Driver2", "PostOfficeAndPostInStore"}
                                           //}
                                       };

            return moduleMenu;
        }
    }
}
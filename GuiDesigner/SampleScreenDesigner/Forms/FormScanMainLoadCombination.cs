﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormScanMainLoadCombination : Form
    {
        public FormScanMainLoadCombination()
        {
            InitializeComponent();
            SetTextToGui();
        }
        private void SetTextToGui()
        {

        }
   
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            
        }

        private void btnOkEnter_Click(object sender, EventArgs e)
        {
            MessageHolder msg = new MessageHolder();
            msg.Text = "Printout is sent to printer N2808_LAS";
            msg.State = MessageState.Information;
            MsgMessage.ShowMessage(msg);
        }
    }
}
﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace SampleScreenDesigner.Forms
{
    partial class FormProfileSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtOrgUnitId = new PreCom.Controls.PreComInput2();
            this.lblOrgUnitId = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtPowerUnit = new PreCom.Controls.PreComInput2();
            this.lblPowerUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtContainmentUnit = new PreCom.Controls.PreComInput2();
            this.lblContainmentUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtUnitId = new PreCom.Controls.PreComInput2();
            this.lblUnitId = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtPostalCode = new PreCom.Controls.PreComInput2();
            this.lblPostalCode = new Resco.Controls.CommonControls.TransparentLabel();
            this.CmbTMSAffiliation = new System.Windows.Forms.ComboBox();
            this.lblTMSAffiliation = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtRouteId = new PreCom.Controls.PreComInput2();
            this.lblRouteId = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtCompanyCode = new PreCom.Controls.PreComInput2();
            this.lblCompanyCode = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtCountryCode = new PreCom.Controls.PreComInput2();
            this.lblCountryCode = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtTelephoneNumber = new PreCom.Controls.PreComInput2();
            this.lblTelephoneNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this._messageControlBox = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.touchPanelControls = new Resco.Controls.CommonControls.TouchPanel();
            this.btnCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOK = new Resco.Controls.OutlookControls.ImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnitId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPowerUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContainmentUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnitId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPostalCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTMSAffiliation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRouteId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCountryCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelephoneNumber)).BeginInit();
            this.touchPanel.SuspendLayout();
            this.touchPanelControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOK)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHeader.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(171, 0);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(135, 27);
            this.lblHeader.Text = "User Profile";
            this.lblHeader.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.Location = new System.Drawing.Point(6, 51);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(464, 34);
            this.lblMessage.Text = "Change or Confirm Information";
            // 
            // txtOrgUnitId
            // 
            this.txtOrgUnitId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtOrgUnitId.Location = new System.Drawing.Point(0, 0);
            this.txtOrgUnitId.Name = "txtOrgUnitId";
            this.txtOrgUnitId.Size = new System.Drawing.Size(100, 41);
            this.txtOrgUnitId.TabIndex = 14;
            // 
            // lblOrgUnitId
            // 
            this.lblOrgUnitId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblOrgUnitId.Location = new System.Drawing.Point(0, 0);
            this.lblOrgUnitId.Name = "lblOrgUnitId";
            this.lblOrgUnitId.Size = new System.Drawing.Size(100, 20);
            this.lblOrgUnitId.Text = "OrgUnit Id";
            // 
            // txtPowerUnit
            // 
            this.txtPowerUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtPowerUnit.Location = new System.Drawing.Point(0, 0);
            this.txtPowerUnit.Name = "txtPowerUnit";
            this.txtPowerUnit.Size = new System.Drawing.Size(100, 41);
            this.txtPowerUnit.TabIndex = 14;
            // 
            // lblPowerUnit
            // 
            this.lblPowerUnit.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPowerUnit.Location = new System.Drawing.Point(0, 0);
            this.lblPowerUnit.Name = "lblPowerUnit";
            this.lblPowerUnit.Size = new System.Drawing.Size(100, 20);
            this.lblPowerUnit.Text = "Power Unit";
            // 
            // txtContainmentUnit
            // 
            this.txtContainmentUnit.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtContainmentUnit.Location = new System.Drawing.Point(0, 0);
            this.txtContainmentUnit.Name = "txtContainmentUnit";
            this.txtContainmentUnit.Size = new System.Drawing.Size(100, 41);
            this.txtContainmentUnit.TabIndex = 14;
            // 
            // lblContainmentUnit
            // 
            this.lblContainmentUnit.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblContainmentUnit.Location = new System.Drawing.Point(0, 0);
            this.lblContainmentUnit.Name = "lblContainmentUnit";
            this.lblContainmentUnit.Size = new System.Drawing.Size(100, 20);
            this.lblContainmentUnit.Text = "Containment Unit";
            // 
            // txtUnitId
            // 
            this.txtUnitId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtUnitId.Location = new System.Drawing.Point(0, 0);
            this.txtUnitId.Name = "txtUnitId";
            this.txtUnitId.Size = new System.Drawing.Size(100, 41);
            this.txtUnitId.TabIndex = 14;
            // 
            // lblUnitId
            // 
            this.lblUnitId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblUnitId.Location = new System.Drawing.Point(0, 0);
            this.lblUnitId.Name = "lblUnitId";
            this.lblUnitId.Size = new System.Drawing.Size(100, 20);
            this.lblUnitId.Text = "Unit Id";
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtPostalCode.Location = new System.Drawing.Point(0, 0);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(100, 41);
            this.txtPostalCode.TabIndex = 14;
            // 
            // lblPostalCode
            // 
            this.lblPostalCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPostalCode.Location = new System.Drawing.Point(0, 0);
            this.lblPostalCode.Name = "lblPostalCode";
            this.lblPostalCode.Size = new System.Drawing.Size(100, 20);
            this.lblPostalCode.Text = "Postal Code";
            // 
            // CmbTMSAffiliation
            // 
            this.CmbTMSAffiliation.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.CmbTMSAffiliation.Location = new System.Drawing.Point(0, 0);
            this.CmbTMSAffiliation.Name = "CmbTMSAffiliation";
            this.CmbTMSAffiliation.Size = new System.Drawing.Size(100, 41);
            this.CmbTMSAffiliation.TabIndex = 14;
            this.CmbTMSAffiliation.SelectedIndexChanged += new System.EventHandler(this.CmbTmsAffiliationSelectedIndexChanged);
            this.CmbTMSAffiliation.Items.Add("TM_OTC");
            this.CmbTMSAffiliation.Items.Add("Alystra");
            // 
            // lblTMSAffiliation
            // 
            this.lblTMSAffiliation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTMSAffiliation.Location = new System.Drawing.Point(0, 0);
            this.lblTMSAffiliation.Name = "lblTMSAffiliation";
            this.lblTMSAffiliation.Size = new System.Drawing.Size(100, 20);
            this.lblTMSAffiliation.Text = "TMS Affi.";
            // 
            // txtRouteId
            // 
            this.txtRouteId.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtRouteId.Location = new System.Drawing.Point(0, 0);
            this.txtRouteId.Name = "txtRouteId";
            this.txtRouteId.Size = new System.Drawing.Size(100, 41);
            this.txtRouteId.TabIndex = 14;
            // 
            // lblRouteId
            // 
            this.lblRouteId.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRouteId.Location = new System.Drawing.Point(0, 0);
            this.lblRouteId.Name = "lblRouteId";
            this.lblRouteId.Size = new System.Drawing.Size(100, 20);
            this.lblRouteId.Text = "Route Id";
            // 
            // txtCompanyCode
            // 
            this.txtCompanyCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtCompanyCode.Location = new System.Drawing.Point(0, 0);
            this.txtCompanyCode.Name = "txtCompanyCode";
            this.txtCompanyCode.Size = new System.Drawing.Size(100, 41);
            this.txtCompanyCode.TabIndex = 14;
            // 
            // lblCompanyCode
            // 
            this.lblCompanyCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCompanyCode.Location = new System.Drawing.Point(0, 0);
            this.lblCompanyCode.Name = "lblCompanyCode";
            this.lblCompanyCode.Size = new System.Drawing.Size(100, 20);
            this.lblCompanyCode.Text = "Company Code";
            // 
            // txtCountryCode
            // 
            this.txtCountryCode.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtCountryCode.Location = new System.Drawing.Point(0, 0);
            this.txtCountryCode.Name = "txtCountryCode";
            this.txtCountryCode.Size = new System.Drawing.Size(100, 41);
            this.txtCountryCode.TabIndex = 14;
            // 
            // lblCountryCode
            // 
            this.lblCountryCode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCountryCode.Location = new System.Drawing.Point(0, 0);
            this.lblCountryCode.Name = "lblCountryCode";
            this.lblCountryCode.Size = new System.Drawing.Size(100, 20);
            this.lblCountryCode.Text = "Country Code";
            // 
            // txtTelephoneNumber
            // 
            this.txtTelephoneNumber.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.txtTelephoneNumber.Location = new System.Drawing.Point(0, 0);
            this.txtTelephoneNumber.Name = "txtTelephoneNumber";
            this.txtTelephoneNumber.Size = new System.Drawing.Size(100, 41);
            this.txtTelephoneNumber.TabIndex = 14;
            // 
            // lblTelephoneNumber
            // 
            this.lblTelephoneNumber.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTelephoneNumber.Location = new System.Drawing.Point(0, 0);
            this.lblTelephoneNumber.Name = "lblTelephoneNumber";
            this.lblTelephoneNumber.Size = new System.Drawing.Size(100, 20);
            this.lblTelephoneNumber.Text = "Tele. No.";
            // 
            // _messageControlBox
            // 
            this._messageControlBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._messageControlBox.Location = new System.Drawing.Point(6, 327);
            this._messageControlBox.Name = "_messageControlBox";
            this._messageControlBox.Size = new System.Drawing.Size(468, 123);
            this._messageControlBox.TabIndex = 24;
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.touchPanelControls);
            this.touchPanel.Controls.Add(this.btnCancel);
            this.touchPanel.Controls.Add(this.btnOK);
            this.touchPanel.Controls.Add(this.lblHeader);
            this.touchPanel.Controls.Add(this._messageControlBox);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // touchPanelControls
            // 
            this.touchPanelControls.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.touchPanelControls.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanelControls.Location = new System.Drawing.Point(0, 35);
            this.touchPanelControls.Name = "touchPanelControls";
            this.touchPanelControls.Size = new System.Drawing.Size(480, 275);
            this.touchPanelControls.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(0, 454);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(238, 80);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "Back";
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOK.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOK.ForeColor = System.Drawing.Color.White;
            this.btnOK.Location = new System.Drawing.Point(242, 454);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(238, 80);
            this.btnOK.TabIndex = 30;
            this.btnOK.Text = "Ok";
            this.btnOK.Click += new System.EventHandler(this.BtnOkClick);
            // 
            // FormProfileSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormProfileSettings";
            this.Text = "User Profile";
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnitId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPowerUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblContainmentUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnitId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPostalCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTMSAffiliation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRouteId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCountryCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTelephoneNumber)).EndInit();
            this.touchPanel.ResumeLayout(false);
            this.touchPanelControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnCancel;
        private Resco.Controls.OutlookControls.ImageButton btnOK;

        private Resco.Controls.CommonControls.TransparentLabel lblHeader;
        private Resco.Controls.CommonControls.TransparentLabel lblMessage;
        private PreCom.Controls.PreComInput2 txtOrgUnitId;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgUnitId;
        private PreCom.Controls.PreComInput2 txtPowerUnit;
        private Resco.Controls.CommonControls.TransparentLabel lblPowerUnit;
        private PreCom.Controls.PreComInput2 txtContainmentUnit;
        private Resco.Controls.CommonControls.TransparentLabel lblContainmentUnit;
        private PreCom.Controls.PreComInput2 txtUnitId;
        private Resco.Controls.CommonControls.TransparentLabel lblUnitId;
        private PreCom.Controls.PreComInput2 txtPostalCode;
        private Resco.Controls.CommonControls.TransparentLabel lblPostalCode;
        private System.Windows.Forms.ComboBox CmbTMSAffiliation;
        private Resco.Controls.CommonControls.TransparentLabel lblTMSAffiliation;
        private PreCom.Controls.PreComInput2 txtRouteId;
        private Resco.Controls.CommonControls.TransparentLabel lblRouteId;
        private PreCom.Controls.PreComInput2 txtCompanyCode;
        private Resco.Controls.CommonControls.TransparentLabel lblCompanyCode;
        private PreCom.Controls.PreComInput2 txtCountryCode;
        private Resco.Controls.CommonControls.TransparentLabel lblCountryCode;
        private PreCom.Controls.PreComInput2 txtTelephoneNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblTelephoneNumber;
        private MessageControl _messageControlBox;
        private Resco.Controls.CommonControls.TouchPanel touchPanelControls;
    }
}
﻿namespace SampleScreenDesigner.Forms
{
    partial class FormConnectionSatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.buttonBlank = new Resco.Controls.OutlookControls.ImageButton();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPhysicalLoadConnected = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnKeepOld = new Resco.Controls.OutlookControls.ImageButton();
            this.btnNewConnection = new Resco.Controls.OutlookControls.ImageButton();
            this.lblOldConnection = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMessageDisconnected = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOldDestination = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOldPower = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNewConnection = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNewPwr = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNewDestination = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhysicalLoadConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnKeepOld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewConnection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldConnection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessageDisconnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewConnection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewPwr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewDestination)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblNewPwr);
            this.touchPanel.Controls.Add(this.lblNewDestination);
            this.touchPanel.Controls.Add(this.lblNewConnection);
            this.touchPanel.Controls.Add(this.lblOldPower);
            this.touchPanel.Controls.Add(this.lblOldDestination);
            this.touchPanel.Controls.Add(this.lblMessageDisconnected);
            this.touchPanel.Controls.Add(this.lblMessage);
            this.touchPanel.Controls.Add(this.lblOldConnection);
            this.touchPanel.Controls.Add(this.buttonBlank);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblPhysicalLoadConnected);
            this.touchPanel.Controls.Add(this.btnKeepOld);
            this.touchPanel.Controls.Add(this.btnNewConnection);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // buttonBlank
            // 
            this.buttonBlank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBlank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBlank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBlank.ForeColor = System.Drawing.Color.White;
            this.buttonBlank.Location = new System.Drawing.Point(161, 483);
            this.buttonBlank.Name = "buttonBlank";
            this.buttonBlank.Size = new System.Drawing.Size(158, 50);
            this.buttonBlank.TabIndex = 54;
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(150, 10);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(98, 17);
            
            // 
            // lblPhysicalLoadConnected
            // 
            this.lblPhysicalLoadConnected.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.lblPhysicalLoadConnected.Location = new System.Drawing.Point(22, 88);
            this.lblPhysicalLoadConnected.Name = "lblPhysicalLoadConnected";
            this.lblPhysicalLoadConnected.Size = new System.Drawing.Size(221, 15);
    
            // 
            // btnKeepOld
            // 
            this.btnKeepOld.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKeepOld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnKeepOld.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnKeepOld.ForeColor = System.Drawing.Color.White;
            this.btnKeepOld.Location = new System.Drawing.Point(322, 483);
            this.btnKeepOld.Name = "btnKeepOld";
            this.btnKeepOld.Size = new System.Drawing.Size(158, 50);
            this.btnKeepOld.TabIndex = 31;
            
            // 
            // btnNewConnection
            // 
            this.btnNewConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewConnection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnNewConnection.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnNewConnection.ForeColor = System.Drawing.Color.White;
            this.btnNewConnection.Location = new System.Drawing.Point(0, 483);
            this.btnNewConnection.Name = "btnNewConnection";
            this.btnNewConnection.Size = new System.Drawing.Size(158, 50);
            this.btnNewConnection.TabIndex = 30;
         
            // 
            // transparentLabel1
            // 
            this.lblOldConnection.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOldConnection.Location = new System.Drawing.Point(22, 279);
            this.lblOldConnection.Name = "transparentLabel1";
            this.lblOldConnection.Size = new System.Drawing.Size(90, 14);
       
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.lblMessage.Location = new System.Drawing.Point(23, 176);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(380,75);
            this.lblMessage.AutoSize = false;
      
            // 
            // lblMessageDisconnected
            // 
            this.lblMessageDisconnected.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblMessageDisconnected.Location = new System.Drawing.Point(22, 127);
            this.lblMessageDisconnected.Name = "lblMessageDisconnected";
            this.lblMessageDisconnected.Size = new System.Drawing.Size(199, 14);
            
            // 
            // lblOldDestination
            // 
            this.lblOldDestination.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.lblOldDestination.Location = new System.Drawing.Point(22, 309);
            this.lblOldDestination.Name = "lblOldDestination";
            this.lblOldDestination.Size = new System.Drawing.Size(101, 15);
            
            // 
            // lblOldPower
            // 
            this.lblOldPower.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblOldPower.Location = new System.Drawing.Point(22, 339);
            this.lblOldPower.Name = "lblOldPower";
            this.lblOldPower.Size = new System.Drawing.Size(39, 14);
           
            // 
            // lblNewConnection
            // 
            this.lblNewConnection.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblNewConnection.Location = new System.Drawing.Point(22, 379);
            this.lblNewConnection.Name = "lblNewConnection";
            this.lblNewConnection.Size = new System.Drawing.Size(95, 14);
          
            // 
            // lblNewPwr
            // 
            this.lblNewPwr.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNewPwr.Location = new System.Drawing.Point(23, 441);
            this.lblNewPwr.Name = "lblNewPwr";
            this.lblNewPwr.Size = new System.Drawing.Size(39, 14);
          
            // 
            // lblNewDestination
            // 
            this.lblNewDestination.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.lblNewDestination.Location = new System.Drawing.Point(23, 411);
            this.lblNewDestination.Name = "lblNewDestination";
            this.lblNewDestination.Size = new System.Drawing.Size(101, 15);
            
            // 
            // FormConnectionSatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F,192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormConnectionSatus";
            this.Text = "FormConnectionStatus";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhysicalLoadConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnKeepOld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewConnection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldConnection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessageDisconnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewConnection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewPwr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewDestination)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnKeepOld;
        private Resco.Controls.OutlookControls.ImageButton btnNewConnection;
        private Resco.Controls.CommonControls.TransparentLabel lblPhysicalLoadConnected;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Resco.Controls.OutlookControls.ImageButton buttonBlank;
        private Resco.Controls.CommonControls.TransparentLabel lblOldPower;
        private Resco.Controls.CommonControls.TransparentLabel lblOldDestination;
        private Resco.Controls.CommonControls.TransparentLabel lblMessageDisconnected;
        private Resco.Controls.CommonControls.TransparentLabel lblMessage;
        private Resco.Controls.CommonControls.TransparentLabel lblOldConnection;
        private Resco.Controls.CommonControls.TransparentLabel lblNewPwr;
        private Resco.Controls.CommonControls.TransparentLabel lblNewDestination;
        private Resco.Controls.CommonControls.TransparentLabel lblNewConnection;

    }
}
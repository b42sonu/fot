﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    public partial class FormDisplayStopList : Form
    {
        public FormDisplayStopList()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            BindOperations();
        }

        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            switch (e.DataRow.CurrentTemplateIndex)
            {
                    
                case 1: // The master row has to be collapsed
                                   
                    break;
                case 2: // The master row has to be expanded  
                    UnSelectOtherRows();
                    e.DataRow.Selected = true;
                    ShowOperationDetail(e);
                    break;
            }
        }


        private void ShowOperationDetail(RowEventArgs e)
        {
            lblTripId.Text = Convert.ToString(e.DataRow["PlannedStop.TripId"]);
            lblStopId.Text = Convert.ToString(e.DataRow["PlannedStop.StopId"]);
        }


        private void UnSelectOtherRows()
        {
            for (var i = 0; i <= listOperations.DataRows.Count - 1;i++ )
            {

                var row = listOperations.DataRows[i];
                row.Selected = false;
            }

        }

        private void BindOperations()
        {
            var stopInfo1 = new GetGoodsListResponsePlannedStop
                                {
                                   StopId = "StopId1",
                                   TripId                                   = "TripId1",
                                   EarliestStartTime                                   = DateTime.Now,
                                   LatestEndTime                                   = DateTime.Now,
                                   LocationAddress                                   = "Address",
                                   LocationPostalCode                                   = "12345"
                               };
            var stopInfo2 = new GetGoodsListResponsePlannedStop
            {
                StopId = "StopId2",
                TripId = "TripId2",
                EarliestStartTime = DateTime.Now,
                LatestEndTime = DateTime.Now,
                LocationAddress = "Address",
                LocationPostalCode = "12345"
            };


            var loadingStop1 = new LoadingStop
                                  {
                                      PlannedStop = stopInfo1,
                                      Status=1,
                                      StopType = 0
                                  };
            var loadingStop2 = new LoadingStop
            {
                PlannedStop = stopInfo2,
                Status = 1,
                StopType = 0
            };



            var listOfObjects = new List<LoadingStop> {loadingStop1, loadingStop2};


            listOperations.BeginUpdate();
            listOperations.DataSource = listOfObjects;
            listOperations.EndUpdate();
        }

        private void ButtonLoadWithOutStopClick(object sender, EventArgs e)
        {
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
        }

        private void ButtonScanClick(object sender, EventArgs e)
        {
        }

    }

    public class LoadingStop
    {
        public GetGoodsListResponsePlannedStop PlannedStop { get; set; }
        public int Status { get; set; }
        public int StopType { get; set; }
    }

    public class GetGoodsListResponsePlannedStop
    {
        public string TripId { get; set; }
        public string StopId { get; set; }
        public string LocationName { get; set; }
        public string LocationAddress { get; set; }
        public string LocationPostalCode { get; set; }
        public string StopSequence { get; set; }
        public DateTime LatestEndTime { get; set; }
        public bool LatestEndTimeSpecified { get; set; }
        public DateTime EarliestStartTime { get; set; }
        public bool EarliestStartTimeSpecified { get; set; }
    }




}
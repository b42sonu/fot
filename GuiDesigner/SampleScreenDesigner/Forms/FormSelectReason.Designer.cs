﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using PreCom.Controls;
using System.Windows.Forms;

namespace SampleScreenDesigner.Forms
{

    partial class FormSelectReason
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageControlBox = new MessageControl();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReason = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtReason = new PreComInput2();
            this.listReasons = new ListBox();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.lblReason);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtReason);
            this.touchPanel.Controls.Add(this.listReasons);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.Controls.Add(this.messageControlBox);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            //var image = GuiCommon.Instance.BackgroundImage;
            //if (image != null)
            //{
            //    touchPanel.BackgroundImage = image;
            //}

            // 
            // _messageControlBox
            // 
            this.messageControlBox.Location = new System.Drawing.Point(6, 350);
            this.messageControlBox.Name = "messageControlBox";
            this.messageControlBox.Size = new System.Drawing.Size(468, 123);
            this.messageControlBox.TabIndex = 1;

            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Location = new System.Drawing.Point(11, 410);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(446, 60);
            this.lblHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // lblPowerUnit
            // 
            this.lblReason.Location = new System.Drawing.Point(11, 60);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(157, 29);
            this.lblReason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);


            this.txtReason.Location = new System.Drawing.Point(11, 90);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(436, 70);
            this.txtReason.TabIndex = 0;
            this.txtReason.TextTranslation = false;
            this.txtReason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtReason.KeyUp += new KeyEventHandler(TxtReasonKeyUp);

            this.listReasons.Location = new System.Drawing.Point(11, 130);
            this.listReasons.Name = "lstReasons";
            this.listReasons.Size = new System.Drawing.Size(460, 280);
            this.listReasons.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.listReasons.SelectedIndexChanged += new System.EventHandler(ListReasonsSelectedIndexChanged);


            // 
            // FormSelectReason
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormSelectReason";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;

        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblReason;
        private ListBox listReasons;
        private PreCom.Controls.PreComInput2 txtReason;
        private MessageControl messageControlBox;
    }
}
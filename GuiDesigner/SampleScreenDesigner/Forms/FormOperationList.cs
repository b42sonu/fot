﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using System.Drawing.Imaging;


namespace Com.Bring.PMP.PreComFW.OperationList.Views
{
    public partial class FormOperationList : Form
    {
        public FormOperationList()
        {
            InitializeComponent();
            

            GetStopsInfo();

            advancedListOperationList.BeginUpdate();
            advancedListOperationList.DataSource = ListClientOperationStops;
            advancedListOperationList.EndUpdate();
        }


        private void FormOperationList_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Up))
            {
                // Up
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down))
            {
                // Down
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Left))
            {
                // Left
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right))
            {
                // Right
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Enter))
            {
                // Enter
            }

        }

        private void BtnDetailsClick(object sender, EventArgs e)
        {

        }

        private void BtnUnplannedPickUpClick(object sender, EventArgs e)
        {

        }

        private void BtnOptionsClick(object sender, EventArgs e)
        {

        }

        private void BtnBackClick(object sender, EventArgs e)
        {

        }

        public void PnlConfirmMessagePaint(object sender, PaintEventArgs e)
        {
            var pen = new Pen(Color.FromArgb(113, 112, 116), 2);
            var graphics = e.Graphics;
            graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlConfirmMessage.Width - 3, pnlConfirmMessage.Height - 3));
        }

        public void PnlMessagePaint(object sender, PaintEventArgs e)
        {
            var pen = new Pen(Color.FromArgb(113, 112, 116), 2);
            var graphics = e.Graphics;
            graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlMessage.Width - 3, pnlMessage.Height - 3));

            //// Create a red and black bitmap to demonstrate transparency.
            //var bmp = new Bitmap(75, 75);
            //var g = Graphics.FromImage(bmp);

            //g.FillEllipse(new SolidBrush(Color.Red), 0, 0, bmp.Width, bmp.Width);
            //g.DrawLine(new Pen(Color.Black), 0, 0, bmp.Width, bmp.Width);
            //g.DrawLine(new Pen(Color.Black), bmp.Width, 0, 0, bmp.Width);
            //g.Dispose();

            //var attr = new ImageAttributes();

            //// Set the transparency color key based on the upper-left pixel 
            //// of the image.
            //// Uncomment the following line to make all black pixels transparent:
            //// attr.SetColorKey(bmp.GetPixel(0, 0), bmp.GetPixel(0, 0));

            //// Set the transparency color key based on a specified value.
            //// Uncomment the following line to make all red pixels transparent:
            // attr.SetColorKey(Color.White, Color.White);

            //// Draw the image using the image attributes.
            //Rectangle dstRect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //e.Graphics.DrawImage(bmp, dstRect, 0, 0, bmp.Width, bmp.Height,
            //    GraphicsUnit.Pixel, attr);







        }


        private void BtnDepartureClick(object sender, EventArgs e)
        {
            //this.pnlConfirmMessage.Visible = true;
            //this.pnlConfirmMessage.BringToFront();

            this.pnlMessage.Visible = true;
            this.pnlMessage.BringToFront();
        }

        private void BtnScanClick(object sender, EventArgs e)
        {

        }

        private void AdvancedListOperationListRowSelect(object sender, Resco.Controls.AdvancedList.RowEventArgs e)
        {

        }

        private void TabControlOperationSelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        private void GetStopsInfo()
        {
            StubOperationListStop stubOperationListStop = new StubOperationListStop();

            stubOperationListStop.AlternateStopSign = "8";
            stubOperationListStop.StopAddress = "Skoyen,34564 Bring";
            stubOperationListStop.StopId = "StopId1";
            stubOperationListStop.StopSign = "3";
            stubOperationListStop.StopStatus = 1;
            stubOperationListStop.StopTimeInfo = "09:30-09:35 Skoyen";

            ListClientOperationStops.Add(stubOperationListStop);   
 
            StubOperationListStop stubOperationListStop2 = new StubOperationListStop();

            stubOperationListStop2.AlternateStopSign = "8";
            stubOperationListStop2.StopAddress = "Storo,34564 FRIGO";
            stubOperationListStop2.StopId = "StopId2";
            stubOperationListStop2.StopSign = "3";
            stubOperationListStop2.StopStatus = 1;
            stubOperationListStop2.StopTimeInfo = "09:30-09:35 Bergen";

            ListClientOperationStops.Add(stubOperationListStop2);
        }

        
        private IList<StubOperationListStop> ListClientOperationStops = new List<StubOperationListStop>();

        private void BtnAttemptedDeliveryClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnConfirmClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnAttemptClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }

    public class StubOperationListStop
    {

        public string StopId { get; set; }


        public int StopStatus { get; set; }

        public String StopHeader
        {
            get { return StopTimeInfo + Environment.NewLine + StopAddress; }

        }

        public string StopAddress { get; set; }
        public string StopSign { get; set; }
        public string AlternateStopSign { get; set; }
        public String StopTimeInfo { get; set; }

        public string OperationId { get; set; }
        public string OrderNumber { get; set; }

    }
}
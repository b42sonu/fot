﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    partial class FormMessages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ImageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.tabControlOperationList = new Resco.Controls.CommonControls.TabControl();
            this.tabInbox = new Resco.Controls.CommonControls.TabPage();
            this.advancedListOperationList = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelected = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellSelectedImage = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCellSelectedMessageText = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellSelectedDate = new Resco.Controls.AdvancedList.TextCell();
            this.textCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowUnselected = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCellStopInformation = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.tabRead = new Resco.Controls.CommonControls.TabPage();
            this.tabDeleted = new Resco.Controls.CommonControls.TabPage();
            this.pnlNewMessage = new System.Windows.Forms.Panel();
            this.txtNewMessage = new PreCom.Controls.PreComInput2();
            this.lblDetailValue3 = new System.Windows.Forms.Label();
            this.lblDetailValue1 = new System.Windows.Forms.Label();
            this.lblDetailValue2 = new System.Windows.Forms.Label();
            this.lblDetailType3 = new System.Windows.Forms.Label();
            this.lblDetailType1 = new System.Windows.Forms.Label();
            this.lblDetailType2 = new System.Windows.Forms.Label();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOperationList)).BeginInit();
            this.tabControlOperationList.SuspendLayout();
            this.tabInbox.SuspendLayout();
            this.pnlNewMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ImageListOperationListIcons
            // 
            this.ImageListOperationListIcons.ImageSize = new System.Drawing.Size(40, 40);
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.tabControlOperationList);
            this.touchPanel.Controls.Add(this.pnlNewMessage);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(2, 2);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.Text = "Messages";
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // tabControlOperationList
            // 
            this.tabControlOperationList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlOperationList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlOperationList.Location = new System.Drawing.Point(0, 44);
            this.tabControlOperationList.Name = "tabControlOperationList";
            this.tabControlOperationList.SelectedIndex = 2;
            this.tabControlOperationList.Size = new System.Drawing.Size(480, 380);
            this.tabControlOperationList.TabIndex = 43;
            this.tabControlOperationList.TabPages.Add(this.tabInbox);
            this.tabControlOperationList.TabPages.Add(this.tabRead);
            this.tabControlOperationList.TabPages.Add(this.tabDeleted);
            this.tabControlOperationList.Text = "tabControlOperationList";
            this.tabControlOperationList.ToolbarSize = new System.Drawing.Size(480, 51);
            this.tabControlOperationList.SelectedIndexChanged += new System.EventHandler(this.TabControlOperationSelectedIndexChanged);
            // 
            // tabInbox
            // 
            this.tabInbox.Controls.Add(this.advancedListOperationList);
            this.tabInbox.Location = new System.Drawing.Point(0, 0);
            this.tabInbox.Name = "tabInbox";
            this.tabInbox.Size = new System.Drawing.Size(480, 329);
            // 
            // 
            // 
            this.tabInbox.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabInbox.TabItem.CustomSize = new System.Drawing.Size(120, 50);
            this.tabInbox.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabInbox.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabInbox.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabInbox.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabInbox.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabInbox.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabInbox.TabItem.Name = "";
            this.tabInbox.TabItem.Text = "Inbox";
            // 
            // advancedListOperationList
            // 
            this.advancedListOperationList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListOperationList.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedListOperationList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListOperationList.DataRows.Clear();
            this.advancedListOperationList.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.advancedListOperationList.KeyNavigation = true;
            this.advancedListOperationList.Location = new System.Drawing.Point(0, 0);
            this.advancedListOperationList.Name = "advancedListOperationList";
            this.advancedListOperationList.ScrollbarSmallChange = 32;
            this.advancedListOperationList.ScrollbarWidth = 26;
            this.advancedListOperationList.SelectedTemplateIndex = 1;
            this.advancedListOperationList.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.advancedListOperationList.ShowHeader = true;
            this.advancedListOperationList.Size = new System.Drawing.Size(480, 326);
            this.advancedListOperationList.TabIndex = 2;
            this.advancedListOperationList.TemplateIndex = 2;
            this.advancedListOperationList.Templates.Add(this.RowTemplateHeader);
            this.advancedListOperationList.Templates.Add(this.templateRowSelected);
            this.advancedListOperationList.Templates.Add(this.templateRowUnselected);
            this.advancedListOperationList.TouchScrolling = true;
            this.advancedListOperationList.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.AdvancedListOperationListRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // templateRowSelected
            // 
            this.templateRowSelected.BackColor = System.Drawing.Color.DarkGray;
            this.templateRowSelected.CellTemplates.Add(this.ImageCellSelectedImage);
            this.templateRowSelected.CellTemplates.Add(this.TextCellSelectedMessageText);
            this.templateRowSelected.CellTemplates.Add(this.TextCellSelectedDate);
            this.templateRowSelected.CellTemplates.Add(this.textCell1);
            this.templateRowSelected.Height = 64;
            this.templateRowSelected.Name = "templateRowSelected";
            // 
            // ImageCellSelectedImage
            // 
            this.ImageCellSelectedImage.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellSelectedImage.AutoResize = true;
            this.ImageCellSelectedImage.Border = Resco.Controls.AdvancedList.BorderType.Flat;
            this.ImageCellSelectedImage.CellSource.ConstantData = "0";
            this.ImageCellSelectedImage.DesignName = "ImageCellSelectedImage";
            this.ImageCellSelectedImage.ImageIndex = 0;
            this.ImageCellSelectedImage.ImageList = this.ImageListOperationListIcons;
            this.ImageCellSelectedImage.Location = new System.Drawing.Point(3, 0);
            this.ImageCellSelectedImage.Size = new System.Drawing.Size(64, 64);
            // 
            // TextCellSelectedMessageText
            // 
            this.TextCellSelectedMessageText.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.TextCellSelectedMessageText.Border = Resco.Controls.AdvancedList.BorderType.Flat;
            this.TextCellSelectedMessageText.CellSource.ColumnName = "Messagetext";
            this.TextCellSelectedMessageText.DesignName = "TextCellSelectedMessageText";
            this.TextCellSelectedMessageText.Location = new System.Drawing.Point(67, 0);
            this.TextCellSelectedMessageText.Size = new System.Drawing.Size(334, 64);
            this.TextCellSelectedMessageText.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // TextCellSelectedDate
            // 
            this.TextCellSelectedDate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellSelectedDate.Border = Resco.Controls.AdvancedList.BorderType.Flat;
            this.TextCellSelectedDate.CellSource.ColumnName = "FormattedDate";
            this.TextCellSelectedDate.DesignName = "TextCellSelectedDate";
            this.TextCellSelectedDate.Location = new System.Drawing.Point(400, 0);
            this.TextCellSelectedDate.Size = new System.Drawing.Size(75, 64);
            // 
            // textCell1
            // 
            this.textCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell1.CellSource.ColumnName = "MessageId";
            this.textCell1.DesignName = "textCell1";
            this.textCell1.Location = new System.Drawing.Point(0, 0);
            this.textCell1.Size = new System.Drawing.Size(0, 64);
            // 
            // templateRowUnselected
            // 
            this.templateRowUnselected.CellTemplates.Add(this.ImageCellStopSign);
            this.templateRowUnselected.CellTemplates.Add(this.TextCellStopInformation);
            this.templateRowUnselected.CellTemplates.Add(this.TextCellStopId);
            this.templateRowUnselected.CellTemplates.Add(this.textCellOperationId);
            this.templateRowUnselected.Height = 60;
            this.templateRowUnselected.Name = "templateRowUnselected";
            // 
            // ImageCellStopSign
            // 
            this.ImageCellStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellStopSign.AutoResize = true;
            this.ImageCellStopSign.Border = Resco.Controls.AdvancedList.BorderType.Flat;
            this.ImageCellStopSign.CellSource.ConstantData = "0";
            this.ImageCellStopSign.DesignName = "ImageCellStopSign";
            this.ImageCellStopSign.ImageIndex = 0;
            this.ImageCellStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellStopSign.Location = new System.Drawing.Point(3, 0);
            this.ImageCellStopSign.Size = new System.Drawing.Size(64, 64);
            // 
            // TextCellStopInformation
            // 
            this.TextCellStopInformation.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.TextCellStopInformation.Border = Resco.Controls.AdvancedList.BorderType.Flat;
            this.TextCellStopInformation.CellSource.ColumnName = "Messagetext";
            this.TextCellStopInformation.DesignName = "TextCellStopInformation";
            this.TextCellStopInformation.Location = new System.Drawing.Point(67, 0);
            this.TextCellStopInformation.Size = new System.Drawing.Size(334, 64);
            this.TextCellStopInformation.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // TextCellStopId
            // 
            this.TextCellStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellStopId.Border = Resco.Controls.AdvancedList.BorderType.Flat;
            this.TextCellStopId.CellSource.ColumnName = "FormattedDate";
            this.TextCellStopId.DesignName = "TextCellStopId";
            this.TextCellStopId.Location = new System.Drawing.Point(400, 0);
            this.TextCellStopId.Size = new System.Drawing.Size(75, 64);
            // 
            // textCellOperationId
            // 
            this.textCellOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationId.CellSource.ColumnName = "MessageId";
            this.textCellOperationId.DesignName = "textCellOperationId";
            this.textCellOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationId.Size = new System.Drawing.Size(0, 64);
            // 
            // tabRead
            // 
            this.tabRead.Location = new System.Drawing.Point(0, 0);
            this.tabRead.Name = "tabRead";
            this.tabRead.Size = new System.Drawing.Size(480, 329);
            // 
            // 
            // 
            this.tabRead.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabRead.TabItem.CustomSize = new System.Drawing.Size(150, 50);
            this.tabRead.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabRead.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabRead.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabRead.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabRead.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabRead.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabRead.TabItem.Name = "";
            this.tabRead.TabItem.Text = "Sent";
            // 
            // tabDeleted
            // 
            this.tabDeleted.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tabDeleted.Location = new System.Drawing.Point(0, 0);
            this.tabDeleted.Name = "tabDeleted";
            this.tabDeleted.Size = new System.Drawing.Size(480, 329);
            // 
            // 
            // 
            this.tabDeleted.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabDeleted.TabItem.CustomSize = new System.Drawing.Size(150, 50);
            this.tabDeleted.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabDeleted.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabDeleted.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabDeleted.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabDeleted.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabDeleted.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabDeleted.TabItem.Name = "";
            this.tabDeleted.TabItem.Text = "Deleted";
            // 
            // pnlNewMessage
            // 
            this.pnlNewMessage.Controls.Add(this.txtNewMessage);
            this.pnlNewMessage.Location = new System.Drawing.Point(0, 47);
            this.pnlNewMessage.Name = "pnlNewMessage";
            this.pnlNewMessage.Size = new System.Drawing.Size(480, 307);
            // 
            // txtNewMessage
            // 
            this.txtNewMessage.Location = new System.Drawing.Point(0, 0);
            this.txtNewMessage.MaxLength = 100;
            this.txtNewMessage.Multiline = true;
            this.txtNewMessage.Name = "txtNewMessage";
            this.txtNewMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNewMessage.Size = new System.Drawing.Size(480, 307);
            this.txtNewMessage.TabIndex = 28;
            this.txtNewMessage.TextTranslation = false;
            // 
            // lblDetailValue3
            // 
            this.lblDetailValue3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailValue3.Location = new System.Drawing.Point(230, 60);
            this.lblDetailValue3.Name = "lblDetailValue3";
            this.lblDetailValue3.Size = new System.Drawing.Size(245, 70);
            // 
            // lblDetailValue1
            // 
            this.lblDetailValue1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailValue1.Location = new System.Drawing.Point(230, 0);
            this.lblDetailValue1.Name = "lblDetailValue1";
            this.lblDetailValue1.Size = new System.Drawing.Size(245, 35);
            // 
            // lblDetailValue2
            // 
            this.lblDetailValue2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailValue2.Location = new System.Drawing.Point(230, 30);
            this.lblDetailValue2.Name = "lblDetailValue2";
            this.lblDetailValue2.Size = new System.Drawing.Size(245, 30);
            // 
            // lblDetailType3
            // 
            this.lblDetailType3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType3.Location = new System.Drawing.Point(3, 60);
            this.lblDetailType3.Name = "lblDetailType3";
            this.lblDetailType3.Size = new System.Drawing.Size(172, 90);
            // 
            // lblDetailType1
            // 
            this.lblDetailType1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType1.Location = new System.Drawing.Point(3, 0);
            this.lblDetailType1.Name = "lblDetailType1";
            this.lblDetailType1.Size = new System.Drawing.Size(220, 30);
            // 
            // lblDetailType2
            // 
            this.lblDetailType2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType2.Location = new System.Drawing.Point(3, 30);
            this.lblDetailType2.Name = "lblDetailType2";
            this.lblDetailType2.Size = new System.Drawing.Size(148, 40);
            // 
            // FormMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormMessages";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOperationList)).EndInit();
            this.tabControlOperationList.ResumeLayout(false);
            this.tabInbox.ResumeLayout(false);
            this.pnlNewMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList ImageListOperationListIcons;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TabControl tabControlOperationList;
        private Resco.Controls.CommonControls.TabPage tabInbox;
        private Resco.Controls.AdvancedList.AdvancedList advancedListOperationList;
        private Resco.Controls.CommonControls.TabPage tabRead;
        private Resco.Controls.CommonControls.TabPage tabDeleted;
        // private System.Windows.Forms.Panel pnlButtons;
        // private System.Windows.Forms.Panel pnlOptionalButtons;
        private System.Windows.Forms.Label lblDetailValue3;
        private System.Windows.Forms.Label lblDetailValue1;
        private System.Windows.Forms.Label lblDetailValue2;
        private System.Windows.Forms.Label lblDetailType3;
        private System.Windows.Forms.Label lblDetailType1;
        private System.Windows.Forms.Label lblDetailType2;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Panel pnlNewMessage;
        private PreCom.Controls.PreComInput2 txtNewMessage;
        private RowTemplate RowTemplateHeader;
        private TextCell TextCellHeaderTemplate;
        private RowTemplate templateRowSelected;
        private ImageCell ImageCellSelectedImage;
        private TextCell TextCellSelectedMessageText;
        private TextCell TextCellSelectedDate;
        private TextCell textCell1;
        private RowTemplate templateRowUnselected;
        private ImageCell ImageCellStopSign;
        private TextCell TextCellStopInformation;
        private TextCell TextCellStopId;
        private TextCell textCellOperationId;



    }
}
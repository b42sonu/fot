﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormDamageInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblEarlierEvents = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.lblCommentHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblType = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.cmbAction = new System.Windows.Forms.ComboBox();
            this.cmbReason = new System.Windows.Forms.ComboBox();
            this.cmbEvent = new System.Windows.Forms.ComboBox();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCommentHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblEarlierEvents);
            this.touchPanel.Controls.Add(this.buttonCancel);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.lblCommentHeading);
            this.touchPanel.Controls.Add(this.lblMeasure);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblType);
            this.touchPanel.Controls.Add(this.txtComments);
            this.touchPanel.Controls.Add(this.cmbAction);
            this.touchPanel.Controls.Add(this.cmbReason);
            this.touchPanel.Controls.Add(this.cmbEvent);
            this.touchPanel.Controls.Add(this.labelTask);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblEarlierEvents
            // 
            this.lblEarlierEvents.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblEarlierEvents.Location = new System.Drawing.Point(3, 13);
            this.lblEarlierEvents.Name = "lblEarlierEvents";
            this.lblEarlierEvents.Size = new System.Drawing.Size(302, 27);
            this.lblEarlierEvents.Text = "Earlier events for <item no>:";
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 472);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(238, 60);
            this.buttonCancel.TabIndex = 29;
            this.buttonCancel.Text = Language.Translate("Cancel");
            this.buttonCancel.Click += new System.EventHandler(ButtonCancelClick);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(242, 472);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(238, 60);
            this.buttonOk.TabIndex = 28;
            this.buttonOk.Text = Language.Translate("Ok/Enter");
            this.buttonOk.Click += new System.EventHandler(ButtonOkClick);
            // 
            // lblCommentHeading
            // 
            this.lblCommentHeading.Location = new System.Drawing.Point(3, 310);
            this.lblCommentHeading.Name = "lblCommentHeading";
            this.lblCommentHeading.Size = new System.Drawing.Size(223, 29);
            this.lblCommentHeading.Text = Language.Translate("Comments (optional)");
            // 
            // lblMeasure
            // 
            this.lblMeasure.Location = new System.Drawing.Point(3, 252);
            this.lblMeasure.Name = "lblMeasure";
            this.lblMeasure.Size = new System.Drawing.Size(90, 29);
            this.lblMeasure.Text = Language.Translate("Measure");
            // 
            // lblCause
            // 
            this.lblCause.Location = new System.Drawing.Point(3, 204);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(64, 29);
            this.lblCause.Text = Language.Translate("Cause");
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(3, 155);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(47, 29);
            this.lblType.Text = Language.Translate("Text");
            // 
            // txtComments
            // 
            this.txtComments.Location = new System.Drawing.Point(3, 345);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(472, 121);
            this.txtComments.TabIndex = 9;
            // 
            // cmbAction
            // 
            this.cmbAction.Location = new System.Drawing.Point(139, 247);
            this.cmbAction.Name = "cmbAction";
            this.cmbAction.Size = new System.Drawing.Size(336, 41);
            this.cmbAction.TabIndex = 8;
            // 
            // cmbReason
            // 
            this.cmbReason.Location = new System.Drawing.Point(139, 199);
            this.cmbReason.Name = "cmbReason";
            this.cmbReason.Size = new System.Drawing.Size(336, 41);
            this.cmbReason.TabIndex = 6;
            this.cmbReason.SelectedIndexChanged += new System.EventHandler(CmbReasonSelectedIndexChanged);
            // 
            // cmbEvent
            // 
            this.cmbEvent.Location = new System.Drawing.Point(139, 151);
            this.cmbEvent.Name = "cmbEvent";
            this.cmbEvent.Size = new System.Drawing.Size(336, 41);
            this.cmbEvent.TabIndex = 4;
            // 
            // labelTask
            // 
            this.labelTask.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelTask.Location = new System.Drawing.Point(0, 108);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(207, 24);
            this.labelTask.Text = Language.Translate("Damage Information");
            // 
            // FormDamageInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDamageInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCommentHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private TextBox txtComments;
        private ComboBox cmbAction;
        private ComboBox cmbReason;
        private ComboBox cmbEvent;
        private Resco.Controls.CommonControls.TransparentLabel lblMeasure;
        private Resco.Controls.CommonControls.TransparentLabel lblCause;
        private Resco.Controls.CommonControls.TransparentLabel lblType;
        private Resco.Controls.CommonControls.TransparentLabel lblCommentHeading;
        private Resco.Controls.OutlookControls.ImageButton buttonCancel;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.CommonControls.TransparentLabel lblEarlierEvents;
    }
}
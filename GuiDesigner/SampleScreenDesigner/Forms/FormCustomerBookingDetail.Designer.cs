﻿using PreCom.Controls;
using Resco.Controls.CommonControls;

namespace SampleScreenDesigner.Forms
{
    partial class FormCustomerBookingDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.btnGenerateBookingNnumber = new Resco.Controls.OutlookControls.ImageButton();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();

            this.labelGenerateBookingNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtBookingNumber = new PreCom.Controls.PreComInput2();
            this.labelBookingNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtCustomerNumber = new PreCom.Controls.PreComInput2();
            this.labelCustomerNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtCarrierId = new PreCom.Controls.PreComInput2();
            this.labelCarrierId = new Resco.Controls.CommonControls.TransparentLabel();

            this.txtStopId = new PreCom.Controls.PreComInput2();
            this.labelStopId = new Resco.Controls.CommonControls.TransparentLabel();

            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnGenerateBookingNnumber)).BeginInit();

            ((System.ComponentModel.ISupportInitialize)(this.labelGenerateBookingNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelBookingNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomerNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCarrierId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStopId)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            //this.touchPanel.Controls.Add(this.btnGenerateBookingNnumber);
            this.touchPanel.Controls.Add(this.messageControl);

            //this.touchPanel.Controls.Add(this.labelGenerateBookingNumber);
            //this.touchPanel.Controls.Add(this.txtBookingNumber);
            //this.touchPanel.Controls.Add(this.labelBookingNumber);
            //this.touchPanel.Controls.Add(this.txtCustomerNumber);
            //this.touchPanel.Controls.Add(this.labelCustomerNumber);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // btnGenerateBookingNnumber
            // 
            this.btnGenerateBookingNnumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnGenerateBookingNnumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnGenerateBookingNnumber.ForeColor = System.Drawing.Color.White;
            this.btnGenerateBookingNnumber.Location = new System.Drawing.Point(21, 250);
            this.btnGenerateBookingNnumber.Name = "btnGenerateBookingNnumber";
            this.btnGenerateBookingNnumber.Size = new System.Drawing.Size(438, 50);
            this.btnGenerateBookingNnumber.TabIndex = 33;

            this.btnGenerateBookingNnumber.Click += new System.EventHandler(this.ButtonGenerateBookingNumberClick);
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(21, 341);
            this.messageControl.MessageText = "";
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438, 120);
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 42);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // labelGenerateBookingNumber
            // 
            this.labelGenerateBookingNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelGenerateBookingNumber.Location = new System.Drawing.Point(21, 222);
            this.labelGenerateBookingNumber.Name = "labelGenerateBookingNumber";
            this.labelGenerateBookingNumber.Size = new System.Drawing.Size(246, 30);

            // 
            // txtBookingNumber
            // 
            this.txtBookingNumber.Location = new System.Drawing.Point(21, 173);
            this.txtBookingNumber.Name = "txtBookingNumber";
            this.txtBookingNumber.Size = new System.Drawing.Size(438, 41);
            this.txtBookingNumber.TabIndex = 1;
            this.txtBookingNumber.TextTranslation = false;
            //this.txtBookingNumber.Click += new System.EventHandler(this.TextBoxesClick);
            this.txtBookingNumber.Tag = "Booking Number";
            this.txtBookingNumber.MaxLength = 35;
            this.txtBookingNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtBookingNumberKeyUp);
            // 
            // labelBookingNumber
            // 
            this.labelBookingNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelBookingNumber.Location = new System.Drawing.Point(21, 145);
            this.labelBookingNumber.Name = "labelBookingNumber";
            this.labelBookingNumber.Size = new System.Drawing.Size(165, 30);

            // 
            // txtCustomerNumber
            // 
            this.txtCustomerNumber.Location = new System.Drawing.Point(21, 105);
            this.txtCustomerNumber.Name = "txtCustomerNumber";
            this.txtCustomerNumber.Size = new System.Drawing.Size(438, 40);
            this.txtCustomerNumber.TabIndex = 0;
            this.txtCustomerNumber.TextTranslation = false;
            //this.txtCustomerNumber.Click += new System.EventHandler(this.TextBoxesClick);
            this.txtCustomerNumber.Tag = "Customer Number";
            this.txtCustomerNumber.MaxLength = 11;
            this.txtCustomerNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtCustomerNumberKeyUp);
            // 
            // labelCustomerNumber
            // 
            this.labelCustomerNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelCustomerNumber.Location = new System.Drawing.Point(21, 71);
            this.labelCustomerNumber.Name = "labelCustomerNumber";
            this.labelCustomerNumber.Size = new System.Drawing.Size(181, 30);


            /// new addeed
            // 
            // txtCarrierId
            // 
            this.txtCarrierId.Location = new System.Drawing.Point(21, 173);
            this.txtCarrierId.Name = "txtCarrierId";
            this.txtCarrierId.Size = new System.Drawing.Size(438, 40);
            this.txtCarrierId.TabIndex = 0;
            this.txtCarrierId.TextTranslation = false;
            this.txtCarrierId.Tag = "Customer Number";
            this.txtCarrierId.MaxLength = 11;
            this.txtCarrierId.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtCustomerNumberKeyUp);
            // 
            // labelCarrierId
            // 
            this.labelCarrierId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelCarrierId.Location = new System.Drawing.Point(21, 145);
            this.labelCarrierId.Name = "labelCarrierId";
            this.labelCarrierId.Size = new System.Drawing.Size(181, 30);


            // 
            // txtStopId
            // 
            this.txtStopId.Location = new System.Drawing.Point(21, 250);
            this.txtStopId.Name = "txtStopId";
            this.txtStopId.Size = new System.Drawing.Size(438, 40);
            this.txtStopId.TabIndex = 0;
            this.txtStopId.TextTranslation = false;
            this.txtStopId.Tag = "StopId Id";
            this.txtStopId.MaxLength = 11;
            this.txtStopId.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtCustomerNumberKeyUp);
            // 
            // labelStopId
            // 
            this.labelStopId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelStopId.Location = new System.Drawing.Point(21, 222);
            this.labelStopId.Name = "labelStopId";
            this.labelStopId.Size = new System.Drawing.Size(181, 30);

            // end




            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(115, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(215, 27);

            // 
            // FormCustomerBookingDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormCustomerBookingDetail";
            this.Text = "FormCustomerBookingDetail";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnGenerateBookingNnumber)).EndInit();

            ((System.ComponentModel.ISupportInitialize)(this.labelGenerateBookingNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelBookingNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomerNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCarrierId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStopId)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private PreCom.Controls.PreComInput2 txtCustomerNumber;
        private Resco.Controls.CommonControls.TransparentLabel labelCustomerNumber;
        private PreCom.Controls.PreComInput2 txtBookingNumber;
        private Resco.Controls.CommonControls.TransparentLabel labelBookingNumber;
        private Resco.Controls.CommonControls.TransparentLabel labelGenerateBookingNumber;

        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private Resco.Controls.OutlookControls.ImageButton btnGenerateBookingNnumber;

        private PreCom.Controls.PreComInput2 txtCarrierId;
        private Resco.Controls.CommonControls.TransparentLabel labelCarrierId;

        private PreCom.Controls.PreComInput2 txtStopId;
        private Resco.Controls.CommonControls.TransparentLabel labelStopId;

    }
}
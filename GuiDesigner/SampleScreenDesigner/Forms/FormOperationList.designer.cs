﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.OperationList.Views
{
    partial class FormOperationList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOperationList));

            this.pnlMessage = new Panel();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblInfoHeading = new System.Windows.Forms.Label();
            this.picInfo = new System.Windows.Forms.PictureBox();

            this.pnlConfirmMessage = new Panel();
            this.btnConfirm = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.btnAttempt = new Resco.Controls.OutlookControls.ImageButton();
            this.lblConfirmInfo = new System.Windows.Forms.Label();
            this.lblConfirmInfoHeading = new System.Windows.Forms.Label();
            this.picConfirmInfo = new System.Windows.Forms.PictureBox();

            this.ImageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.btnAttemptedDelivery = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDeparture = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnScan = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDetails = new Resco.Controls.OutlookControls.ImageButton();
            this.btnUnplannedPickUp = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.tabControlOperationList = new Resco.Controls.CommonControls.TabControl();
            this.tabNotStarted = new Resco.Controls.CommonControls.TabPage();
            this.advancedListOperationList = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplateStopPlus = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCellStopInformation = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellStopStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCellStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplateStopMinus = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCellAlternateStopInfo = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellAlternateStopStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateStopId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellAlternateOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellAlternateOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellOperationOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellAlternameOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellAlternateOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellAlternateOperationOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplateForSingleOperation = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellSingleStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellSingleStopDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellSingleStopStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellSingleStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellSingleOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellSingleOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.tabCompleted = new Resco.Controls.CommonControls.TabPage();
            this.tabAll = new Resco.Controls.CommonControls.TabPage();
            this.panelBottomInfo = new System.Windows.Forms.Panel();
            this.lblConsignorAdr = new System.Windows.Forms.Label();
            this.lblCosignment = new System.Windows.Forms.Label();
            this.lblBookingRef = new System.Windows.Forms.Label();
            this.lblConsAdr = new System.Windows.Forms.Label();
            this.lblConsiHead = new System.Windows.Forms.Label();
            this.lblBookingHeading = new System.Windows.Forms.Label();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.btnAttemptedDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeparture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUnplannedPickUp)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOperationList)).BeginInit();
            this.tabControlOperationList.SuspendLayout();
            this.tabNotStarted.SuspendLayout();
            this.panelBottomInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            this.ImageListOperationListIcons.Images.Clear();

            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Finished());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.New());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Started());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.PlusLarge());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Unloading());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Loading());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Pickup());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Delivery());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.MinusLarge());
            // 
            // btnOptions
            // 
            this.btnAttemptedDelivery.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(79)))), ((int)(((byte)(83)))));
            this.btnAttemptedDelivery.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnAttemptedDelivery.ForeColor = System.Drawing.Color.White;
            this.btnAttemptedDelivery.Location = new System.Drawing.Point(322, 429);
            this.btnAttemptedDelivery.Name = "btnAttemptedDelivery";
            this.btnAttemptedDelivery.Size = new System.Drawing.Size(158, 50);
            this.btnAttemptedDelivery.TabIndex = 29;
            this.btnAttemptedDelivery.Text = "AttemptedDel";
            this.btnAttemptedDelivery.Click += new System.EventHandler(this.BtnAttemptedDeliveryClick);
            // 
            // btnDeparture
            // 
            this.btnDeparture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDeparture.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDeparture.ForeColor = System.Drawing.Color.White;
            this.btnDeparture.Location = new System.Drawing.Point(161, 483);
            this.btnDeparture.Name = "btnDeparture";
            this.btnDeparture.Size = new System.Drawing.Size(158, 50);
            this.btnDeparture.TabIndex = 30;
            this.btnDeparture.Text = "Departure";
            this.btnDeparture.Click += new System.EventHandler(this.BtnDepartureClick);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.BtnBackClick);
            // 
            // btnScan
            // 
            this.btnScan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnScan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnScan.ForeColor = System.Drawing.Color.White;
            this.btnScan.Location = new System.Drawing.Point(322, 483);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(158, 50);
            this.btnScan.TabIndex = 32;
            this.btnScan.Text = "Scan";
            this.btnScan.Click += new System.EventHandler(this.BtnScanClick);
            // 
            // btnDetails
            // 
            this.btnDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(77)))), ((int)(((byte)(79)))), ((int)(((byte)(83)))));
            this.btnDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDetails.ForeColor = System.Drawing.Color.White;
            this.btnDetails.Location = new System.Drawing.Point(0, 429);
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.Size = new System.Drawing.Size(158, 50);
            this.btnDetails.TabIndex = 33;
            this.btnDetails.Text = "Details";
            this.btnDetails.Click += new System.EventHandler(this.BtnDetailsClick);
            // 
            // btnUnpannedPickUp
            // 
            this.btnUnplannedPickUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnUnplannedPickUp.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnUnplannedPickUp.ForeColor = System.Drawing.Color.White;
            this.btnUnplannedPickUp.Location = new System.Drawing.Point(161, 429);
            this.btnUnplannedPickUp.Name = "btnUnplannedPickUp";
            this.btnUnplannedPickUp.Size = new System.Drawing.Size(158, 50);
            this.btnUnplannedPickUp.TabIndex = 34;
            this.btnUnplannedPickUp.Text = "UnplannedPick";
            this.btnUnplannedPickUp.Click += new System.EventHandler(this.BtnUnplannedPickUpClick);
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.tabControlOperationList);
            this.touchPanel.Controls.Add(this.btnUnplannedPickUp);
            this.touchPanel.Controls.Add(this.btnDetails);
            this.touchPanel.Controls.Add(this.btnScan);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnDeparture);
            this.touchPanel.Controls.Add(this.btnAttemptedDelivery);
            this.touchPanel.Controls.Add(this.panelBottomInfo);
            this.touchPanel.Controls.Add(this.pnlConfirmMessage);
            this.touchPanel.Controls.Add(this.pnlMessage);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            // 
            // tabControlOperationList
            // 
            this.tabControlOperationList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlOperationList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlOperationList.Location = new System.Drawing.Point(0, 0);
            this.tabControlOperationList.Name = "tabControlOperationList";
            this.tabControlOperationList.SelectedIndex = 0;
            this.tabControlOperationList.Size = new System.Drawing.Size(480, 305);
            this.tabControlOperationList.TabIndex = 43;
            this.tabControlOperationList.TabPages.Add(this.tabNotStarted);
            this.tabControlOperationList.TabPages.Add(this.tabCompleted);
            this.tabControlOperationList.TabPages.Add(this.tabAll);
            this.tabControlOperationList.Text = "tabControlOperationList";
            this.tabControlOperationList.ToolbarSize = new System.Drawing.Size(480, 51);
            this.tabControlOperationList.SelectedIndexChanged += new System.EventHandler(this.TabControlOperationSelectedIndexChanged);
            // 
            // tabNotStarted
            // 
            this.tabNotStarted.Controls.Add(this.advancedListOperationList);
            this.tabNotStarted.Location = new System.Drawing.Point(0, 0);
            this.tabNotStarted.Name = "tabNotStarted";
            this.tabNotStarted.Size = new System.Drawing.Size(480, 221);
            // 
            // 
            // 
            this.tabNotStarted.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabNotStarted.TabItem.CustomSize = new System.Drawing.Size(190, 50);
            this.tabNotStarted.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabNotStarted.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabNotStarted.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabNotStarted.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabNotStarted.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabNotStarted.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabNotStarted.TabItem.Name = "";
            this.tabNotStarted.TabItem.Text = "NotCompleted";
            // 
            // advancedListOperationList
            // 
            this.advancedListOperationList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListOperationList.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedListOperationList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListOperationList.DataRows.Clear();
            this.advancedListOperationList.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            resources.GetString("advancedListOperationList.HeaderRow")});
            this.advancedListOperationList.Location = new System.Drawing.Point(0, 0);
            this.advancedListOperationList.MultiSelect = true;
            this.advancedListOperationList.Name = "advancedListOperationList";
            this.advancedListOperationList.ScrollbarSmallChange = 32;
            this.advancedListOperationList.ScrollbarWidth = 26;
            this.advancedListOperationList.SelectedTemplateIndex = 2;
            this.advancedListOperationList.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.advancedListOperationList.ShowHeader = true;
            this.advancedListOperationList.Size = new System.Drawing.Size(480, 255);
            this.advancedListOperationList.TabIndex = 2;
            this.advancedListOperationList.TemplateIndex = 1;
            this.advancedListOperationList.Templates.Add(this.RowTemplateHeader);
            this.advancedListOperationList.Templates.Add(this.RowTemplateStopPlus);
            this.advancedListOperationList.Templates.Add(this.RowTemplateStopMinus);
            this.advancedListOperationList.Templates.Add(this.RowTemplatePlannedOp);
            this.advancedListOperationList.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.advancedListOperationList.Templates.Add(this.RowTemplateForSingleOperation);
            this.advancedListOperationList.TouchScrolling = true;
            this.advancedListOperationList.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.AdvancedListOperationListRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplateStopPlus
            // 
            this.RowTemplateStopPlus.CellTemplates.Add(this.ImageCellStopSign);
            this.RowTemplateStopPlus.CellTemplates.Add(this.TextCellStopInformation);
            this.RowTemplateStopPlus.CellTemplates.Add(this.ImageCellStopStatus);
            this.RowTemplateStopPlus.CellTemplates.Add(this.TextCellStopId);
            this.RowTemplateStopPlus.CellTemplates.Add(this.textCellOperationId);
            this.RowTemplateStopPlus.CellTemplates.Add(this.textCellOrderNumber);
            this.RowTemplateStopPlus.Height = 64;
            this.RowTemplateStopPlus.Name = "RowTemplateStopPlus";
            // 
            // ImageCellStopSign
            // 
            this.ImageCellStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellStopSign.AutoResize = true;
            this.ImageCellStopSign.CellSource.ColumnName = "StopSign";
            this.ImageCellStopSign.DesignName = "ImageCellStopSign";
            this.ImageCellStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellStopSign.Location = new System.Drawing.Point(3, 10);
            this.ImageCellStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // TextCellStopInformation
            // 
            this.TextCellStopInformation.CellSource.ColumnName = "StopHeader";
            this.TextCellStopInformation.DesignName = "TextCellStopInformation";
            this.TextCellStopInformation.Location = new System.Drawing.Point(45, 0);
            this.TextCellStopInformation.Size = new System.Drawing.Size(388, 64);
            this.TextCellStopInformation.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);

            // 
            // ImageCellStopStatus
            // 
            this.ImageCellStopStatus.AutoResize = true;
            this.ImageCellStopStatus.AutoTransparent = true;
            this.ImageCellStopStatus.CellSource.ColumnName = "StopStatus";
            this.ImageCellStopStatus.DesignName = "ImageCellStopStatus";
            this.ImageCellStopStatus.ImageList = this.ImageListOperationListIcons;
            this.ImageCellStopStatus.Location = new System.Drawing.Point(433, 25);
            this.ImageCellStopStatus.Size = new System.Drawing.Size(16, 16);
            // 
            // TextCellStopId
            // 
            this.TextCellStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellStopId.CellSource.ColumnName = "StopId";
            this.TextCellStopId.DesignName = "TextCellStopId";
            this.TextCellStopId.Location = new System.Drawing.Point(0, 0);
            this.TextCellStopId.Size = new System.Drawing.Size(0, 64);
            this.TextCellStopId.Visible = false;
            // 
            // textCellOperationId
            // 
            this.textCellOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationId.CellSource.ColumnName = "OperationId";
            this.textCellOperationId.DesignName = "textCellOperationId";
            this.textCellOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationId.Visible = false;
            // 
            // textCellOrderNumber
            // 
            this.textCellOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOrderNumber.CellSource.ColumnName = "OrderId";
            this.textCellOrderNumber.DesignName = "textCellOrderNumber";
            this.textCellOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.textCellOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.textCellOrderNumber.Visible = false;
            // 
            // RowTemplateStopMinus
            // 
            this.RowTemplateStopMinus.CellTemplates.Add(this.ImageCellAlternateStopSign);
            this.RowTemplateStopMinus.CellTemplates.Add(this.TextCellAlternateStopInfo);
            this.RowTemplateStopMinus.CellTemplates.Add(this.ImageCellAlternateStopStatus);
            this.RowTemplateStopMinus.CellTemplates.Add(this.textCellAlternateStopId);
            this.RowTemplateStopMinus.CellTemplates.Add(this.TextCellAlternateOperationId);
            this.RowTemplateStopMinus.CellTemplates.Add(this.TextCellAlternateOrderNumber);
            this.RowTemplateStopMinus.Height = 64;
            this.RowTemplateStopMinus.Name = "RowTemplateStopMinus";
            // 
            // ImageCellAlternateStopSign
            // 
            this.ImageCellAlternateStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateStopSign.AutoResize = true;
            this.ImageCellAlternateStopSign.CellSource.ColumnName = "AlternateStopSign";
            this.ImageCellAlternateStopSign.DesignName = "ImageCellAlternateStopSign";
            this.ImageCellAlternateStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellAlternateStopSign.Location = new System.Drawing.Point(3, 10);
            this.ImageCellAlternateStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // TextCellAlternateStopInfo
            // 
            this.TextCellAlternateStopInfo.CellSource.ColumnName = "StopHeader";
            this.TextCellAlternateStopInfo.DesignName = "TextCellAlternateStopInfo";
            this.TextCellAlternateStopInfo.Location = new System.Drawing.Point(45, 0);
            this.TextCellAlternateStopInfo.Size = new System.Drawing.Size(388, 64);
            this.TextCellAlternateStopInfo.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCellAlternateStopStatus
            // 
            this.ImageCellAlternateStopStatus.AutoResize = true;
            this.ImageCellAlternateStopStatus.AutoTransparent = true;
            this.ImageCellAlternateStopStatus.CellSource.ColumnName = "StopStatus";
            this.ImageCellAlternateStopStatus.DesignName = "ImageCellAlternateStopStatus";
            this.ImageCellAlternateStopStatus.ImageList = this.ImageListOperationListIcons;
            this.ImageCellAlternateStopStatus.Location = new System.Drawing.Point(433, 25);
            this.ImageCellAlternateStopStatus.Size = new System.Drawing.Size(16, 16);
            // 
            // textCellAlternateStopId
            // 
            this.textCellAlternateStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateStopId.CellSource.ColumnName = "StopId";
            this.textCellAlternateStopId.DesignName = "textCellAlternateStopId";
            this.textCellAlternateStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateStopId.Visible = false;
            // 
            // TextCellAlternateOperationId
            // 
            this.TextCellAlternateOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellAlternateOperationId.CellSource.ColumnName = "OperationId";
            this.TextCellAlternateOperationId.DesignName = "TextCellAlternateOperationId";
            this.TextCellAlternateOperationId.Location = new System.Drawing.Point(0, 0);
            this.TextCellAlternateOperationId.Size = new System.Drawing.Size(0, 64);
            this.TextCellAlternateOperationId.Visible = false;
            // 
            // TextCellAlternateOrderNumber
            // 
            this.TextCellAlternateOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellAlternateOrderNumber.CellSource.ColumnName = "OrderId";
            this.TextCellAlternateOrderNumber.DesignName = "TextCellAlternateOrderNumber";
            this.TextCellAlternateOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellAlternateOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellAlternateOrderNumber.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationType);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationDetail);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationStatus);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationOperationId);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationStopId);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.TextCellOperationOrderNumber);
            this.RowTemplatePlannedOp.Height = 64;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.ImageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationType.AutoResize = true;
            this.ImageCellOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellOperationType.DesignName = "ImageCellOperationType";
            this.ImageCellOperationType.ImageList = this.ImageListOperationListIcons;
            this.ImageCellOperationType.Location = new System.Drawing.Point(30, 10);
            this.ImageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(72, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(370, 64);
            this.textCellOperationDetail.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCellOperationStatus
            // 
            this.ImageCellOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationStatus.AutoResize = true;
            this.ImageCellOperationStatus.AutoTransparent = true;
            this.ImageCellOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellOperationStatus.DesignName = "ImageCellOperationStatus";
            this.ImageCellOperationStatus.ImageList = this.ImageListOperationListIcons;
            this.ImageCellOperationStatus.Location = new System.Drawing.Point(433, 25);
            this.ImageCellOperationStatus.Size = new System.Drawing.Size(16, 16);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationStopId.Visible = false;
            // 
            // TextCellOperationOrderNumber
            // 
            this.TextCellOperationOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellOperationOrderNumber.CellSource.ColumnName = "OrderId";
            this.TextCellOperationOrderNumber.DesignName = "TextCellOperationOrderNumber";
            this.TextCellOperationOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellOperationOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellOperationOrderNumber.Visible = false;
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternateOperationType);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternameOperationStatus);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.TextCellAlternateOperationStopId);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.TextCellAlternateOperationOrderNumber);
            this.RowTemplatePlannedOpAlternateTemp.Height = 64;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.ImageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateOperationType.AutoResize = true;
            this.ImageCellAlternateOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.ImageCellAlternateOperationType.ImageList = this.ImageListOperationListIcons;
            this.ImageCellAlternateOperationType.Location = new System.Drawing.Point(30, 10);
            this.ImageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(72, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(370, 64);
            this.textCellAlternateOperationDetail.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCellAlternameOperationStatus
            // 
            this.ImageCellAlternameOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternameOperationStatus.AutoResize = true;
            this.ImageCellAlternameOperationStatus.AutoTransparent = true;
            this.ImageCellAlternameOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellAlternameOperationStatus.DesignName = "ImageCellAlternameOperationStatus";
            this.ImageCellAlternameOperationStatus.ImageList = this.ImageListOperationListIcons;
            this.ImageCellAlternameOperationStatus.Location = new System.Drawing.Point(433, 25);
            this.ImageCellAlternameOperationStatus.Size = new System.Drawing.Size(16, 16);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;
            // 
            // TextCellAlternateOperationStopId
            // 
            this.TextCellAlternateOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellAlternateOperationStopId.CellSource.ColumnName = "StopId";
            this.TextCellAlternateOperationStopId.DesignName = "TextCellAlternateOperationStopId";
            this.TextCellAlternateOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.TextCellAlternateOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.TextCellAlternateOperationStopId.Visible = false;
            // 
            // TextCellAlternateOperationOrderNumber
            // 
            this.TextCellAlternateOperationOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellAlternateOperationOrderNumber.CellSource.ColumnName = "OrderId";
            this.TextCellAlternateOperationOrderNumber.DesignName = "TextCellAlternateOperationOrderNumber";
            this.TextCellAlternateOperationOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellAlternateOperationOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellAlternateOperationOrderNumber.Visible = false;
            // 
            // RowTemplateForSingleOperation
            // 
            this.RowTemplateForSingleOperation.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.ImageCellSingleStopSign);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCellSingleStopDetail);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.ImageCellSingleStopStatus);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCellSingleStopId);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCellSingleOperationId);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCellSingleOrderNumber);
            this.RowTemplateForSingleOperation.Height = 64;
            this.RowTemplateForSingleOperation.Name = "RowTemplateForSingleOperation";
            // 
            // ImageCellSingleStopSign
            // 
            this.ImageCellSingleStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellSingleStopSign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImageCellSingleStopSign.AutoResize = true;
            this.ImageCellSingleStopSign.CellSource.ColumnName = "StopSign";
            this.ImageCellSingleStopSign.DesignName = "ImageCellSingleStopSign";
            this.ImageCellSingleStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellSingleStopSign.Location = new System.Drawing.Point(3, 10);
            this.ImageCellSingleStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellSingleStopDetail
            // 
            this.textCellSingleStopDetail.CellSource.ColumnName = "StopHeader";
            this.textCellSingleStopDetail.DesignName = "textCellSingleStopDetail";
            this.textCellSingleStopDetail.Location = new System.Drawing.Point(45, 0);
            this.textCellSingleStopDetail.Size = new System.Drawing.Size(388, 64);
            this.textCellSingleStopDetail.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCellSingleStopStatus
            // 
            this.ImageCellSingleStopStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellSingleStopStatus.AutoResize = true;
            this.ImageCellSingleStopStatus.AutoTransparent = true;
            this.ImageCellSingleStopStatus.CellSource.ColumnName = "StopStatus";
            this.ImageCellSingleStopStatus.DesignName = "ImageCellSingleStopStatus";
            this.ImageCellSingleStopStatus.ImageList = this.ImageListOperationListIcons;
            this.ImageCellSingleStopStatus.Location = new System.Drawing.Point(433, 25);
            this.ImageCellSingleStopStatus.Size = new System.Drawing.Size(16, 16);
            // 
            // textCellSingleStopId
            // 
            this.textCellSingleStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellSingleStopId.CellSource.ColumnName = "StopId";
            this.textCellSingleStopId.DesignName = "textCellSingleStopId";
            this.textCellSingleStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellSingleStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellSingleStopId.Visible = false;
            // 
            // textCellSingleOperationId
            // 
            this.textCellSingleOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellSingleOperationId.CellSource.ColumnName = "OperationId";
            this.textCellSingleOperationId.DesignName = "textCellSingleOperationId";
            this.textCellSingleOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellSingleOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellSingleOperationId.Visible = false;
            // 
            // textCellSingleOrderNumber
            // 
            this.textCellSingleOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellSingleOrderNumber.CellSource.ColumnName = "OrderId";
            this.textCellSingleOrderNumber.DesignName = "textCellSingleOrderNumber";
            this.textCellSingleOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.textCellSingleOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.textCellSingleOrderNumber.Visible = false;
            // 
            // tabCompleted
            // 
            this.tabCompleted.Location = new System.Drawing.Point(0, 0);
            this.tabCompleted.Name = "tabCompleted";
            this.tabCompleted.Size = new System.Drawing.Size(480, 221);
            // 
            // 
            // 
            this.tabCompleted.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabCompleted.TabItem.CustomSize = new System.Drawing.Size(150, 50);
            this.tabCompleted.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabCompleted.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabCompleted.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabCompleted.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabCompleted.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabCompleted.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabCompleted.TabItem.Name = "";
            this.tabCompleted.TabItem.Text = "Completed";
            // 
            // tabAll
            // 
            this.tabAll.Location = new System.Drawing.Point(0, 0);
            this.tabAll.Name = "tabAll";
            this.tabAll.Size = new System.Drawing.Size(480, 221);
            // 
            // 
            // 
            this.tabAll.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabAll.TabItem.CustomSize = new System.Drawing.Size(100, 50);
            this.tabAll.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabAll.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabAll.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabAll.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabAll.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabAll.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabAll.TabItem.Name = "";
            this.tabAll.TabItem.Text = "All";
            // 
            // panelBottomInfo
            // 
            this.panelBottomInfo.Controls.Add(this.lblConsignorAdr);
            this.panelBottomInfo.Controls.Add(this.lblCosignment);
            this.panelBottomInfo.Controls.Add(this.lblBookingRef);
            this.panelBottomInfo.Controls.Add(this.lblConsAdr);
            this.panelBottomInfo.Controls.Add(this.lblConsiHead);
            this.panelBottomInfo.Controls.Add(this.lblBookingHeading);
            this.panelBottomInfo.Location = new System.Drawing.Point(0, 273);
            this.panelBottomInfo.Name = "panelBottomInfo";
            this.panelBottomInfo.Size = new System.Drawing.Size(480, 152);
            // 
            // lblConsignorAdr
            // 
            this.lblConsignorAdr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignorAdr.Location = new System.Drawing.Point(170, 90);
            this.lblConsignorAdr.Name = "lblConsignorAdr";
            this.lblConsignorAdr.Size = new System.Drawing.Size(305, 70);
            // 
            // lblCosignment
            // 
            this.lblCosignment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCosignment.Location = new System.Drawing.Point(170, 30);
            this.lblCosignment.Name = "lblCosignment";
            this.lblCosignment.Size = new System.Drawing.Size(305, 35);
            // 
            // lblBookingRef
            // 
            this.lblBookingRef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBookingRef.Location = new System.Drawing.Point(170, 60);
            this.lblBookingRef.Name = "lblBookingRef";
            this.lblBookingRef.Size = new System.Drawing.Size(305, 30);
            // 
            // lblConsAdr
            // 
            this.lblConsAdr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsAdr.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsAdr.Location = new System.Drawing.Point(3, 90);
            this.lblConsAdr.Name = "lblConsAdr";
            this.lblConsAdr.Size = new System.Drawing.Size(172, 90);
            this.lblConsAdr.Text = "ConsignorAdress:";
            // 
            // lblConsiHead
            // 
            this.lblConsiHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsiHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsiHead.Location = new System.Drawing.Point(3, 30);
            this.lblConsiHead.Name = "lblConsiHead";
            this.lblConsiHead.Size = new System.Drawing.Size(160, 30);
            this.lblConsiHead.Text = "Consignments: ";
            // 
            // lblBookingHeading
            // 
            this.lblBookingHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBookingHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblBookingHeading.Location = new System.Drawing.Point(3, 60);
            this.lblBookingHeading.Name = "lblBookingHeading";
            this.lblBookingHeading.Size = new System.Drawing.Size(148, 40);
            this.lblBookingHeading.Text = "OrderNumber";

            #region Departure from stop Confirmation Box

            // 
            // pnlConfirmMessage
            // 
            this.pnlConfirmMessage.BackColor = System.Drawing.Color.White;
            this.pnlConfirmMessage.Location = new System.Drawing.Point(20, 20);
            this.pnlConfirmMessage.Name = "pnlButtons";
            this.pnlConfirmMessage.Controls.Add(this.btnConfirm);
            this.pnlConfirmMessage.Controls.Add(this.btnCancel);
            this.pnlConfirmMessage.Controls.Add(this.btnAttempt);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfoHeading);
            this.pnlConfirmMessage.Controls.Add(this.picConfirmInfo);
            this.pnlConfirmMessage.Size = new System.Drawing.Size(440, 380);
            this.pnlConfirmMessage.Visible = false;
            this.pnlConfirmMessage.Paint += new PaintEventHandler(PnlConfirmMessagePaint);


            //btnConfirm
            this.btnConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnConfirm.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnConfirm.ForeColor = System.Drawing.Color.White;
            this.btnConfirm.Location = new System.Drawing.Point(300, 300);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(120, 60);
            this.btnConfirm.TabIndex = 30;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.Click += new System.EventHandler(BtnConfirmClick);

            //btnCancel
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(20, 300);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 60);
            this.btnCancel.TabIndex = 30;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(BtnCancelClick);

            //btnAttempt
            this.btnAttempt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnAttempt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnAttempt.ForeColor = System.Drawing.Color.White;
            this.btnAttempt.Location = new System.Drawing.Point(160, 300);
            this.btnAttempt.Name = "btnAttempt";
            this.btnAttempt.Size = new System.Drawing.Size(120, 60);
            this.btnAttempt.TabIndex = 30;
            this.btnAttempt.Text = "Attempted Delivery";
            this.btnAttempt.Click += new System.EventHandler(BtnAttemptClick);

            // 
            // lblConfirmInfo
            // 
            this.lblConfirmInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblConfirmInfo.Location = new System.Drawing.Point(25, 130);
            this.lblConfirmInfo.Name = "lblConfirmInfo";
            this.lblConfirmInfo.Size = new System.Drawing.Size(370, 60);
            this.lblConfirmInfo.Text = "DepartFromStop";
            this.lblConfirmInfo.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblConfirmInfoHeading
            // 
            this.lblConfirmInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblConfirmInfoHeading.Location = new System.Drawing.Point(25, 100);
            this.lblConfirmInfoHeading.Name = "lblConfirmInfoHeading";
            this.lblConfirmInfoHeading.Size = new System.Drawing.Size(236, 30);
            this.lblConfirmInfoHeading.Text = "Departure";
            this.lblConfirmInfoHeading.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //  
            // picConfirmInfo
            // 
            this.picConfirmInfo.Image = GuiCommon.Instance.Attention();
            this.picConfirmInfo.Location = new System.Drawing.Point(195, 20);
            this.picConfirmInfo.Name = "picConfirmInfo";
            this.picConfirmInfo.Size = new System.Drawing.Size(50, 50);
            this.picConfirmInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            #endregion

            #region Departure not allowed Information Box

            // 
            // pnlMessage
            // 
            this.pnlMessage.BackColor = System.Drawing.Color.White;
            this.pnlMessage.Location = new System.Drawing.Point(20, 20);
            this.pnlMessage.Name = "pnlMessage";
            this.pnlMessage.Controls.Add(this.btnOk);
            this.pnlMessage.Controls.Add(this.lblInfo);
            this.pnlMessage.Controls.Add(this.lblInfoHeading);
            this.pnlMessage.Controls.Add(this.picInfo);
            this.pnlMessage.Size = new System.Drawing.Size(440, 380);
            this.pnlMessage.Visible = false;
            this.pnlMessage.Paint += new PaintEventHandler(PnlMessagePaint);


            //btnOk
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(310, 300);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(120, 60);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(BtnOkClick);

            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblInfo.Location = new System.Drawing.Point(25, 130);
            this.lblInfo.Name = "lblConfirmInfo";
            this.lblInfo.Size = new System.Drawing.Size(370, 120);
            this.lblInfo.Text = "You have pickups on this stop that are started but not completed. Finish them before departing.";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblInfoHeading
            // 
            this.lblInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblInfoHeading.Location = new System.Drawing.Point(25, 100);
            this.lblInfoHeading.Name = "lblConfirmInfoHeading";
            this.lblInfoHeading.Size = new System.Drawing.Size(400, 30);
            this.lblInfoHeading.Text = "Departure not allowed";
            this.lblInfoHeading.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //  
            // picInfo
            // 
            this.picInfo.Image = GuiCommon.Instance.Attention();
            this.picInfo.Location = new System.Drawing.Point(195, 20);
            this.picInfo.Name = "picConfirmInfo";
            this.picInfo.Size = new System.Drawing.Size(50, 50);
            this.picInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            #endregion



            // 
            // FormOperationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormOperationList";
            ((System.ComponentModel.ISupportInitialize)(this.btnAttemptedDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeparture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUnplannedPickUp)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOperationList)).EndInit();
            this.tabControlOperationList.ResumeLayout(false);
            this.tabNotStarted.ResumeLayout(false);
            this.panelBottomInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList ImageListOperationListIcons;
        private Resco.Controls.OutlookControls.ImageButton btnAttemptedDelivery;
        private Resco.Controls.OutlookControls.ImageButton btnDeparture;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnScan;
        private Resco.Controls.OutlookControls.ImageButton btnDetails;
        private Resco.Controls.OutlookControls.ImageButton btnUnplannedPickUp;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TabControl tabControlOperationList;
        private Resco.Controls.CommonControls.TabPage tabNotStarted;
        private Resco.Controls.AdvancedList.AdvancedList advancedListOperationList;
        private Resco.Controls.CommonControls.TabPage tabCompleted;
        private Resco.Controls.CommonControls.TabPage tabAll;
        private System.Windows.Forms.Panel panelBottomInfo;
        private System.Windows.Forms.Label lblConsignorAdr;
        private System.Windows.Forms.Label lblCosignment;
        private System.Windows.Forms.Label lblBookingRef;
        private System.Windows.Forms.Label lblConsAdr;
        private System.Windows.Forms.Label lblConsiHead;
        private System.Windows.Forms.Label lblBookingHeading;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateStopPlus;
        private Resco.Controls.AdvancedList.ImageCell ImageCellStopSign;
        private Resco.Controls.AdvancedList.TextCell TextCellStopInformation;
        private Resco.Controls.AdvancedList.ImageCell ImageCellStopStatus;
        private Resco.Controls.AdvancedList.TextCell TextCellStopId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellOrderNumber;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateStopMinus;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateStopSign;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateStopInfo;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateStopStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateStopId;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateOperationId;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateOrderNumber;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedList.TextCell TextCellOperationOrderNumber;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateOperationStopId;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateOperationOrderNumber;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateForSingleOperation;
        private Resco.Controls.AdvancedList.ImageCell ImageCellSingleStopSign;
        private Resco.Controls.AdvancedList.TextCell textCellSingleStopDetail;
        private Resco.Controls.AdvancedList.ImageCell ImageCellSingleStopStatus;
        private Resco.Controls.AdvancedList.TextCell textCellSingleStopId;
        private Resco.Controls.AdvancedList.TextCell textCellSingleOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellSingleOrderNumber;

        private Panel pnlConfirmMessage;
        private PictureBox picConfirmInfo;
        private Label lblConfirmInfo;
        private Label lblConfirmInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnConfirm;
        private Resco.Controls.OutlookControls.ImageButton btnCancel;
        private Resco.Controls.OutlookControls.ImageButton btnAttempt;



        private Panel pnlMessage;
        private PictureBox picInfo;
        private Label lblInfo;
        private Label lblInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        




    }
}
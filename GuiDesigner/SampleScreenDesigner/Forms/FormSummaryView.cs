﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using SampleScreenDesigner.Properties;

namespace Com.Bring.PMP.PreComFW.PickUp.Views
{
    public partial class FormSummaryView : Form
    {
        readonly string[] _columnNamesConsignments, _columnNamesConsignmentItems;
        private bool _showingConsignments;

        public FormSummaryView()
        {
            _columnNamesConsignments = new[] { "Consignments", "Actual/Planned" };
            _columnNamesConsignmentItems = new[] { "Consignment items", "Image" };

            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            imageListStatus.Images.Add(Resources.Ok);
            imageListStatus.Images.Add(Resources.Plus);
            imageListStatus.Images.Add(Resources.Minus);
            OnShow();
        }

        private void OnShow()
        {
            ShowConsignments();
        }
        private void ShowConsignments()
        {
            _showingConsignments = true;

            labelModuleName.Text = "Pick up - goods loaded";
            
            ListConsignments.BeginUpdate();
            ListConsignments.DataRows.Clear();
            cellHeaderCol1.CellSource.ConstantData = _columnNamesConsignments[0];
            cellHeaderCol2.CellSource.ConstantData = _columnNamesConsignments[1];

            var colTextValues = new string[2];
            colTextValues[0] = "Consignments1";
            colTextValues[1] = "Consignments2";

            var insertRow = new Row(2, 1, colTextValues);
            ListConsignments.DataRows.Add(insertRow);

            ListConsignments.EndUpdate();
        }

        private void ShowConsignmentItems()
        {
            _showingConsignments = false;
            labelModuleName.Text = "Consignment xxx";

            ListConsignments.BeginUpdate();
            ListConsignments.DataRows.Clear();
            cellHeaderCol1.CellSource.ConstantData = _columnNamesConsignmentItems[0];
            cellHeaderCol2.CellSource.ConstantData = "Status";

            var colTextValues = new string[2];
            colTextValues[0] = "Items0";
            colTextValues[1] = "0";
            var insertRow = new Row(3, 3, colTextValues);
            ListConsignments.DataRows.Add(insertRow);

            colTextValues[0] = "Items1";
            colTextValues[1] = "1";
            insertRow = new Row(3, 3, colTextValues);
            ListConsignments.DataRows.Add(insertRow);

            colTextValues[0] = "Items2";
            colTextValues[1] = "2";
            insertRow = new Row(3, 3, colTextValues);
            ListConsignments.DataRows.Add(insertRow);

            colTextValues[0] = "Items3";
            colTextValues[1] = "-1";
            insertRow = new Row(3, 3, colTextValues);
            ListConsignments.DataRows.Add(insertRow);
            ListConsignments.EndUpdate();
        }



        private void ButtonDetailsClick(object sender, EventArgs e)
        {
            ShowConsignmentItems();
        }


        private void OnActiveRowChanged(object sender, EventArgs e)
        {
            buttonDetails.Enabled = ListConsignments.ActiveRowIndex != -1;
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
            if(_showingConsignments == false)
                ShowConsignments();
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {

        }
    }
}
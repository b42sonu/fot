﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    partial class FormReconcillationDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonDelete = new Resco.Controls.OutlookControls.ImageButton();
            this.listConsignments = new Resco.Controls.AdvancedList.AdvancedList();
            this.templateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.templateRowSelected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellSelectedTextCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedTextCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowUnselected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellUnselectedTextCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedTextCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowItems = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellItemUnselectedTextCol = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemUnselectedlmageCol = new Resco.Controls.AdvancedList.ImageCell();
            this.imageListStatus = new System.Windows.Forms.ImageList();
            this.labelHeader = new Resco.Controls.CommonControls.TransparentLabel();
            this.cellHeaderCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.lblNrOfItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrOfItems)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.lblNrOfItems);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.buttonBack);
            this.touchPanel.Controls.Add(this.buttonDelete);
            this.touchPanel.Controls.Add(this.listConsignments);
            this.touchPanel.Controls.Add(this.labelHeader);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(322, 482);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(158, 50);
            this.buttonOk.TabIndex = 31;
            this.buttonOk.Text = "Ok";
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(0, 482);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(158, 50);
            this.buttonBack.TabIndex = 29;
            this.buttonBack.Text = "Back";
            this.buttonBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonDelete.ForeColor = System.Drawing.Color.White;
            this.buttonDelete.Location = new System.Drawing.Point(161, 482);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(158, 50);
            this.buttonDelete.TabIndex = 28;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.Click += new System.EventHandler(this.ButtonDetailsClick);
            // 
            // listConsignments
            // 
            this.listConsignments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.listConsignments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listConsignments.DataRows.Clear();
            this.listConsignments.GridColor = System.Drawing.Color.Black;
            this.listConsignments.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listConsignments.Location = new System.Drawing.Point(11, 89);
            this.listConsignments.Name = "listConsignments";
            this.listConsignments.ScrollbarSmallChange = 32;
            this.listConsignments.ScrollbarWidth = 26;
            this.listConsignments.ShowHeader = true;
            this.listConsignments.Size = new System.Drawing.Size(459, 323);
            this.listConsignments.TabIndex = 3;
            this.listConsignments.Templates.Add(this.templateHeader);
            this.listConsignments.Templates.Add(this.templateRowSelected);
            this.listConsignments.Templates.Add(this.templateRowUnselected);
            this.listConsignments.Templates.Add(this.templateRowItems);
            this.listConsignments.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            // 
            // templateHeader
            // 
            this.templateHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.templateHeader.Height = 0;
            this.templateHeader.Name = "templateHeader";
            // 
            // templateRowSelected
            // 
            this.templateRowSelected.BackColor = System.Drawing.Color.DarkGray;
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol1);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol2);
            this.templateRowSelected.Height = 22;
            this.templateRowSelected.Name = "templateRowSelected";
            // 
            // cellSelectedTextCol1
            // 
            this.cellSelectedTextCol1.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellSelectedTextCol1.CellSource.ColumnIndex = 0;
            this.cellSelectedTextCol1.DesignName = "cellSelectedTextCol1";
            this.cellSelectedTextCol1.Location = new System.Drawing.Point(10, 0);
            this.cellSelectedTextCol1.Size = new System.Drawing.Size(280, 22);
            this.cellSelectedTextCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellSelectedTextCol2
            // 
            this.cellSelectedTextCol2.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellSelectedTextCol2.CellSource.ColumnIndex = 1;
            this.cellSelectedTextCol2.DesignName = "cellSelectedTextCol2";
            this.cellSelectedTextCol2.Location = new System.Drawing.Point(280, 0);
            this.cellSelectedTextCol2.Size = new System.Drawing.Size(-1, 22);
            this.cellSelectedTextCol2.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowUnselected
            // 
            this.templateRowUnselected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol1);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol2);
            this.templateRowUnselected.Height = 22;
            this.templateRowUnselected.Name = "templateRowUnselected";
            // 
            // cellUnselectedTextCol1
            // 
            this.cellUnselectedTextCol1.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellUnselectedTextCol1.CellSource.ColumnIndex = 0;
            this.cellUnselectedTextCol1.DesignName = "cellUnselectedTextCol1";
            this.cellUnselectedTextCol1.Location = new System.Drawing.Point(10, 0);
            this.cellUnselectedTextCol1.Size = new System.Drawing.Size(280, 22);
            this.cellUnselectedTextCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellUnselectedTextCol2
            // 
            this.cellUnselectedTextCol2.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellUnselectedTextCol2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cellUnselectedTextCol2.DesignName = "cellUnselectedTextCol2";
            this.cellUnselectedTextCol2.Location = new System.Drawing.Point(280, 0);
            this.cellUnselectedTextCol2.Size = new System.Drawing.Size(-1, 22);
            this.cellUnselectedTextCol2.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowItems
            // 
            this.templateRowItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowItems.CellTemplates.Add(this.cellItemUnselectedTextCol);
            this.templateRowItems.CellTemplates.Add(this.cellItemUnselectedlmageCol);
            this.templateRowItems.Height = 22;
            this.templateRowItems.Name = "templateRowItems";
            // 
            // cellItemUnselectedTextCol
            // 
            this.cellItemUnselectedTextCol.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellItemUnselectedTextCol.DesignName = "cellItemUnselectedTextCol";
            this.cellItemUnselectedTextCol.Location = new System.Drawing.Point(10, 0);
            this.cellItemUnselectedTextCol.Size = new System.Drawing.Size(280, 22);
            // 
            // cellItemUnselectedlmageCol
            // 
            this.cellItemUnselectedlmageCol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellItemUnselectedlmageCol.AutoTransparent = true;
            this.cellItemUnselectedlmageCol.CellSource.ColumnIndex = 1;
            this.cellItemUnselectedlmageCol.DesignName = "cellItemUnselectedlmageCol";
            this.cellItemUnselectedlmageCol.ImageList = this.imageListStatus;
            this.cellItemUnselectedlmageCol.Location = new System.Drawing.Point(280, 0);
            this.cellItemUnselectedlmageCol.Size = new System.Drawing.Size(-1, 22);
            // 
            // imageListStatus
            // 
            this.imageListStatus.ImageSize = new System.Drawing.Size(20, 20);
            // 
            // labelHeader
            // 
            this.labelHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelHeader.AutoSize = false;
            this.labelHeader.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeader.Location = new System.Drawing.Point(11, 3);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(459, 27);
            this.labelHeader.Text = "Pickup";
            this.labelHeader.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // cellHeaderCol1
            // 
            this.cellHeaderCol1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellHeaderCol1.CellSource.ConstantData = "Consignments";
            this.cellHeaderCol1.DesignName = "cellHeaderCol1";
            this.cellHeaderCol1.Location = new System.Drawing.Point(10, 0);
            this.cellHeaderCol1.Name = "cellHeaderCol1";
            this.cellHeaderCol1.Size = new System.Drawing.Size(280, 28);
            this.cellHeaderCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // lblNrOfItems
            // 
            this.lblNrOfItems.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNrOfItems.AutoSize = false;
            this.lblNrOfItems.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblNrOfItems.Location = new System.Drawing.Point(11, 56);
            this.lblNrOfItems.Name = "lblNrOfItems";
            this.lblNrOfItems.Size = new System.Drawing.Size(459, 27);
            this.lblNrOfItems.Text = "Nr of consigments/...";
            this.lblNrOfItems.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomLeft;
            // 
            // FormReconcillationDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Name = "FormReconcillationDelete";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNrOfItems)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelHeader;
        private Resco.Controls.AdvancedList.AdvancedList listConsignments;
        private Resco.Controls.AdvancedList.RowTemplate templateHeader;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol1;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelected;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol1;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol2;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselected;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol1;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol2;
        private System.Windows.Forms.ImageList imageListStatus;
        private Resco.Controls.AdvancedList.RowTemplate templateRowItems;
        private Resco.Controls.AdvancedList.TextCell cellItemUnselectedTextCol;
        private Resco.Controls.AdvancedList.ImageCell cellItemUnselectedlmageCol;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
        private Resco.Controls.OutlookControls.ImageButton buttonDelete;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.CommonControls.TransparentLabel lblNrOfItems;
    }
}
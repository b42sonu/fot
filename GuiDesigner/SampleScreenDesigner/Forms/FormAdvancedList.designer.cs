﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    partial class FormAdvancedList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdvancedList));
            this.ImageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.advancedListOperationList = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell2 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplate2 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell3 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell4 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplate3 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell5 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedList.TextCell();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAmount = new Resco.Controls.CommonControls.TransparentLabel();
            this.lbTotalAmount = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalAmount)).BeginInit();
            this.SuspendLayout();
            this.ImageListOperationListIcons.Images.Clear();
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblAmount);
            this.touchPanel.Controls.Add(this.lbTotalAmount);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.advancedListOperationList);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // advancedListOperationList
            // 
            this.advancedListOperationList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListOperationList.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedListOperationList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListOperationList.DataRows.Clear();
            this.advancedListOperationList.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            resources.GetString("advancedListOperationList.HeaderRow")});
            this.advancedListOperationList.KeyNavigation = true;
            this.advancedListOperationList.Location = new System.Drawing.Point(9, 105);
            this.advancedListOperationList.Name = "advancedListOperationList";
            this.advancedListOperationList.ScrollbarSmallChange = 32;
            this.advancedListOperationList.ScrollbarWidth = 26;
            this.advancedListOperationList.ShowHeader = true;
            this.advancedListOperationList.Size = new System.Drawing.Size(464, 224);
            this.advancedListOperationList.TabIndex = 45;
            this.advancedListOperationList.Templates.Add(this.RowTemplate1);
            this.advancedListOperationList.Templates.Add(this.RowTemplate2);
            this.advancedListOperationList.Templates.Add(this.RowTemplate3);
            this.advancedListOperationList.TouchScrolling = true;
            // 
            // RowTemplate1
            // 
            this.RowTemplate1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.RowTemplate1.CellTemplates.Add(this.textCell1);
            this.RowTemplate1.CellTemplates.Add(this.textCell2);
            this.RowTemplate1.Height = 30;
            this.RowTemplate1.Name = "RowTemplate1";
            // 
            // textCell1
            // 
            this.textCell1.CellSource.ConstantData = "Consignments";
            this.textCell1.DesignName = "textCell1";
            this.textCell1.Location = new System.Drawing.Point(0, 0);
            this.textCell1.Size = new System.Drawing.Size(300, 16);
            // 
            // textCell2
            // 
            this.textCell2.CellSource.ConstantData = "Amount";
            this.textCell2.DesignName = "textCell2";
            this.textCell2.Location = new System.Drawing.Point(300, 0);
            this.textCell2.Size = new System.Drawing.Size(-1, 16);
            // 
            // RowTemplate2
            // 
            this.RowTemplate2.CellTemplates.Add(this.textCell3);
            this.RowTemplate2.CellTemplates.Add(this.textCell4);
            this.RowTemplate2.Height = 32;
            this.RowTemplate2.Name = "RowTemplate2";
            // 
            // textCell3
            // 
            this.textCell3.DesignName = "textCell3";
            this.textCell3.Location = new System.Drawing.Point(0, 0);
            this.textCell3.Size = new System.Drawing.Size(300, 16);
            // 
            // textCell4
            // 
            this.textCell4.DesignName = "textCell4";
            this.textCell4.Location = new System.Drawing.Point(300, 0);
            this.textCell4.Size = new System.Drawing.Size(-1, 16);
            // 
            // RowTemplate3
            // 
            this.RowTemplate3.BackColor = System.Drawing.Color.DarkGray;
            this.RowTemplate3.CellTemplates.Add(this.textCell5);
            this.RowTemplate3.CellTemplates.Add(this.textCell6);
            this.RowTemplate3.Height = 32;
            this.RowTemplate3.Name = "RowTemplate3";
            // 
            // textCell5
            // 
            this.textCell5.DesignName = "textCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(300, 16);
            // 
            // textCell6
            // 
            this.textCell6.DesignName = "textCell6";
            this.textCell6.Location = new System.Drawing.Point(300, 0);
            this.textCell6.Size = new System.Drawing.Size(-1, 16);
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.Text = "Payments";
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 42);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblAmount
            // 
            this.lblAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAmount.Location = new System.Drawing.Point(167, 415);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(14, 29);
            this.lblAmount.Text = "0";
            // 
            // lbTotalAmount
            // 
            this.lbTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalAmount.Location = new System.Drawing.Point(9, 415);
            this.lbTotalAmount.Name = "lbTotalAmount";
            this.lbTotalAmount.Size = new System.Drawing.Size(144, 29);
            this.lbTotalAmount.Text = "Total Amount";
            // 
            // FormAdvancedList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormAdvancedList";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbTotalAmount)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList ImageListOperationListIcons;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        // private System.Windows.Forms.Panel pnlButtons;
        // private System.Windows.Forms.Panel pnlOptionalButtons;
        private AdvancedList advancedListOperationList;
        private RowTemplate RowTemplate1;
        private TextCell textCell1;
        private TextCell textCell2;
        private RowTemplate RowTemplate2;
        private TextCell textCell3;
        private TextCell textCell4;
        private RowTemplate RowTemplate3;
        private TextCell textCell5;
        private TextCell textCell6;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblAmount;
        private Resco.Controls.CommonControls.TransparentLabel lbTotalAmount;
        

    }
}
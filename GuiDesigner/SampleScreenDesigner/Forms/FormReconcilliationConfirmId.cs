﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using System.Collections.Generic;

namespace SampleScreenDesigner.Forms
{
    public partial class FormReconcilliationConfirmId : Form
    {
        //readonly string[] _columnNamesConsignments, _columnNamesConsignmentItems;
        public FormReconcilliationConfirmId()
        {
            //_columnNamesConsignments = new[] { "Consignments", "Actual/Planned" };
            //_columnNamesConsignmentItems = new[] { "Consignment items", "Image" };

            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            OnShow();
        }

        private void OnShow()
        {
            ShowConsignments();
        }
        private void ShowConsignments()
        {
            var consignment = new List<string> { "1213123", "345454", "5655476" };

            labelModuleName.Text = "Pick up - goods loaded";

            ListConsignments.BeginUpdate();
            ListConsignments.DataRows.Clear();
            //cellHeaderCol1.CellSource.ConstantData = _columnNamesConsignments[0];
            //cellHeaderCol2.CellSource.ConstantData = _columnNamesConsignments[1];
            int i = 1;
            foreach (string st in consignment)
            {


                var colTextValues = new string[2];
                colTextValues[0] = st;
                colTextValues[1] = (i + "/" + i);

                var insertRow = new Row(2, 1, colTextValues);
                ListConsignments.DataRows.Add(insertRow);
                i++;
            }
            ListConsignments.EndUpdate();
        }





        private void ButtonDetailsClick(object sender, EventArgs e)
        {

        }


        private void OnActiveRowChanged(object sender, EventArgs e)
        {
            buttonDetails.Enabled = ListConsignments.ActiveRowIndex != -1;
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {

        }
    }
}
﻿namespace SampleScreenDesigner.Forms
{
    partial class FormScanLoadCarierOrConsignmentItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.preComInputScanInput = new PreCom.Controls.PreComInput2();
            this.lblScan = new System.Windows.Forms.Label();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblHeading = new System.Windows.Forms.Label();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBlank = new Resco.Controls.OutlookControls.ImageButton();
            this.lblActivateCarrier = new System.Windows.Forms.Label();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblActivateCarrier);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.preComInputScanInput);
            this.touchPanel.Controls.Add(this.lblScan);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(0, 0);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.Text = "Building Load Carrier";
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // preComInputScanInput
            // 
            this.preComInputScanInput.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.preComInputScanInput.Location = new System.Drawing.Point(8, 184);
            this.preComInputScanInput.Name = "preComInputScanInput";
            this.preComInputScanInput.Size = new System.Drawing.Size(446, 48);
            this.preComInputScanInput.TabIndex = 24;
            this.preComInputScanInput.TextTranslation = false;
            // 
            // lblScan
            // 
            this.lblScan.Location = new System.Drawing.Point(0, 147);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(477, 33);
            this.lblScan.Text = "Scan or enter log. load carrier for assembly";
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(9, 236);
            this.MsgMessage.MessageText = "";
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(446, 97);
            this.MsgMessage.TabIndex = 14;
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(0, 40);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.Text = "OP OSLO OG AKERSHUS VAREBIL1";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // btnBlank
            // 
            this.btnBlank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBlank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBlank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBlank.ForeColor = System.Drawing.Color.White;
            this.btnBlank.Location = new System.Drawing.Point(161, 483);
            this.btnBlank.Name = "btnBlank";
            this.btnBlank.Size = new System.Drawing.Size(158, 50);
            this.btnBlank.TabIndex = 8;
            // 
            // lblActivateCarrier
            // 
            this.lblActivateCarrier.Location = new System.Drawing.Point(2, 75);
            this.lblActivateCarrier.Name = "lblActivateCarrier";
            this.lblActivateCarrier.Size = new System.Drawing.Size(477, 33);
            this.lblActivateCarrier.Text = "Activate Load Carrier For Assembly";
            // 
            // FormScanLoadCarierOrConsignmentItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Controls.Add(this.btnBlank);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormScanLoadCarierOrConsignmentItem";
            this.Text = "FormPrinterInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 preComInputScanInput;
        private System.Windows.Forms.Label lblScan;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl MsgMessage;
        private System.Windows.Forms.Label lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnBlank;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private System.Windows.Forms.Label lblActivateCarrier;
    }
}
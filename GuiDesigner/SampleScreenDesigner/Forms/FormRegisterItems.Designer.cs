﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormDamageInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblEarlierEvents = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignmentId = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignmentItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.consignmentItemCount = new PreCom.Controls.PreComInput2();
            this.labelItemsScanned = new Resco.Controls.CommonControls.TransparentLabel();
            this.scannedConsignmentItemCount = new PreCom.Controls.PreComInput2();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelItemsScanned)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.scannedConsignmentItemCount);
            this.touchPanel.Controls.Add(this.labelItemsScanned);
            this.touchPanel.Controls.Add(this.consignmentItemCount);
            this.touchPanel.Controls.Add(this.labelConsignmentItems);
            this.touchPanel.Controls.Add(this.labelConsignmentId);
            this.touchPanel.Controls.Add(this.labelConsignment);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblEarlierEvents);
            this.touchPanel.Controls.Add(this.buttonBack);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblEarlierEvents
            // 
            this.lblEarlierEvents.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblEarlierEvents.Location = new System.Drawing.Point(3, 13);
            this.lblEarlierEvents.Name = "lblEarlierEvents";
            this.lblEarlierEvents.Size = new System.Drawing.Size(0, 0);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(0, 472);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(238, 60);
            this.buttonBack.TabIndex = 29;
            this.buttonBack.Text = "Back";
            this.buttonBack.Click += new System.EventHandler(this.ButtonCancelClick);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(242, 472);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(238, 60);
            this.buttonOk.TabIndex = 28;
            this.buttonOk.Text = "Ok/Enter";
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Pickup";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // labelConsignment
            // 
            this.labelConsignment.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignment.Location = new System.Drawing.Point(14, 87);
            this.labelConsignment.Name = "labelConsignment";
            this.labelConsignment.Size = new System.Drawing.Size(132, 24);
            this.labelConsignment.Text = "Consignment:";
            // 
            // labelConsignmentId
            // 
            this.labelConsignmentId.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignmentId.Location = new System.Drawing.Point(202, 87);
            this.labelConsignmentId.Name = "labelConsignmentId";
            this.labelConsignmentId.Size = new System.Drawing.Size(36, 24);
            this.labelConsignmentId.Text = "nnn";
            // 
            // labelConsignmentItems
            // 
            this.labelConsignmentItems.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignmentItems.Location = new System.Drawing.Point(9, 224);
            this.labelConsignmentItems.Name = "labelConsignmentItems";
            this.labelConsignmentItems.Size = new System.Drawing.Size(235, 24);
            this.labelConsignmentItems.Text = "Correct number of items:";
            // 
            // consignmentItemCount
            // 
            this.consignmentItemCount.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.consignmentItemCount.Location = new System.Drawing.Point(274, 207);
            this.consignmentItemCount.Name = "consignmentItemCount";
            this.consignmentItemCount.Size = new System.Drawing.Size(140, 41);
            this.consignmentItemCount.TabIndex = 44;
            this.consignmentItemCount.TextTranslation = false;
            // 
            // labelItemsScanned
            // 
            this.labelItemsScanned.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelItemsScanned.Location = new System.Drawing.Point(6, 294);
            this.labelItemsScanned.Name = "labelItemsScanned";
            this.labelItemsScanned.Size = new System.Drawing.Size(249, 24);
            this.labelItemsScanned.Text = "Number of items scanned:";
            // 
            // scannedConsignmentItemCount
            // 
            this.scannedConsignmentItemCount.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.scannedConsignmentItemCount.Location = new System.Drawing.Point(274, 277);
            this.scannedConsignmentItemCount.Name = "scannedConsignmentItemCount";
            this.scannedConsignmentItemCount.Size = new System.Drawing.Size(140, 41);
            this.scannedConsignmentItemCount.TabIndex = 46;
            this.scannedConsignmentItemCount.TextTranslation = false;
            // 
            // FormDamageInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDamageInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelItemsScanned)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.CommonControls.TransparentLabel lblEarlierEvents;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignment;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentId;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentItems;
        private PreCom.Controls.PreComInput2 consignmentItemCount;
        private PreCom.Controls.PreComInput2 scannedConsignmentItemCount;
        private Resco.Controls.CommonControls.TransparentLabel labelItemsScanned;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormOperationListNew : Form
    {
        public FormOperationListNew()
        {
            InitializeComponent();
           

            GetStopsInfo();

            /*advancedListOperationList.BeginUpdate();
            advancedListOperationList.DataSource = _listClientOperationStops;
            advancedListOperationList.EndUpdate();*/
            AddButtonToPanel();
            
        }

        void AddButtonToPanel()
        {
            tabMultipleButtons1.ListButtons.Add(new TabButton("Details",BtnDetailsClick));
            tabMultipleButtons1.ListButtons.Add(new TabButton("Unpl pick", BtnUnplannedPickUpClick));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BtnBackClick,true,true));
            tabMultipleButtons1.ListButtons.Add(new TabButton("Departure", BtnDepartureClick));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Scan", BtnScanClick, true));
            tabMultipleButtons1.ListButtons.Add(new TabButton("Not Delivered", null));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Attempted Del.", BtnDetailsClick,new Point(320,0)));
            tabMultipleButtons1.GenerateButtons();
        }


        private void BtnDetailsClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnDetails Clicked");
        }

        private void BtnUnplannedPickUpClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnUnplannedPickUp Clicked");
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnBack Clicked");
        }

        public void PnlConfirmMessagePaint(object sender, PaintEventArgs e)
        {
            var pen = new Pen(Color.FromArgb(113, 112, 116), 2);
            var graphics = e.Graphics;
            graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlConfirmMessage.Width - 3, pnlConfirmMessage.Height - 3));
        }

        public void PnlMessagePaint(object sender, PaintEventArgs e)
        {
            var pen = new Pen(Color.FromArgb(113, 112, 116), 2);
            var graphics = e.Graphics;
            graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlMessage.Width - 3, pnlMessage.Height - 3));

            //// Create a red and black bitmap to demonstrate transparency.
            //var bmp = new Bitmap(75, 75);
            //var g = Graphics.FromImage(bmp);

            //g.FillEllipse(new SolidBrush(Color.Red), 0, 0, bmp.Width, bmp.Width);
            //g.DrawLine(new Pen(Color.Black), 0, 0, bmp.Width, bmp.Width);
            //g.DrawLine(new Pen(Color.Black), bmp.Width, 0, 0, bmp.Width);
            //g.Dispose();

            //var attr = new ImageAttributes();

            //// Set the transparency color key based on the upper-left pixel 
            //// of the image.
            //// Uncomment the following line to make all black pixels transparent:
            //// attr.SetColorKey(bmp.GetPixel(0, 0), bmp.GetPixel(0, 0));

            //// Set the transparency color key based on a specified value.
            //// Uncomment the following line to make all red pixels transparent:
            // attr.SetColorKey(Color.White, Color.White);

            //// Draw the image using the image attributes.
            //Rectangle dstRect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //e.Graphics.DrawImage(bmp, dstRect, 0, 0, bmp.Width, bmp.Height,
            //    GraphicsUnit.Pixel, attr);







        }


        private void BtnDepartureClick(object sender, EventArgs e)
        {
            //this.pnlConfirmMessage.Visible = true;
            //this.pnlConfirmMessage.BringToFront();
            MessageBox.Show("BtnDeparture Clicked");
            pnlMessage.Visible = true;
            pnlMessage.BringToFront();
        }

        private void BtnScanClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnScan Clicked");
        }

        private void AdvancedListOperationListRowSelect(object sender, Resco.Controls.AdvancedList.RowEventArgs e)
        {

        }

        private void TabControlOperationSelectedIndexChanged(object sender, EventArgs e)
        {

        }

        

        private void GetStopsInfo()
        {
            var stubOperationListStop = new StubOperationListStop
                                            {
                                                AlternateStopSign = "8",
                                                StopAddress = "Skoyen,34564 Bring",
                                                StopId = "StopId1",
                                                StopSign = "3",
                                                StopStatus = 1,
                                                StopTimeInfo = "09:30-09:35 Skoyen"
                                            };

            _listClientOperationStops.Add(stubOperationListStop);   
 
            var stubOperationListStop2 = new StubOperationListStop
                                             {
                                                 AlternateStopSign = "8",
                                                 StopAddress = "Storo,34564 FRIGO",
                                                 StopId = "StopId2",
                                                 StopSign = "3",
                                                 StopStatus = 1,
                                                 StopTimeInfo = "09:30-09:35 Bergen"
                                             };

            _listClientOperationStops.Add(stubOperationListStop2);
        }

       
        private readonly IList<StubOperationListStop> _listClientOperationStops = new List<StubOperationListStop>();

        private void BtnAttemptedDeliveryClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnAttemptedDelevery Clicked");
          
        }

        private void BtnConfirmClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnConfirm Clicked");
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnCancel Clicked");
        }

        private void BtnAttemptClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnAttempt Clicked");
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnOk Clicked");
        }
    }
}
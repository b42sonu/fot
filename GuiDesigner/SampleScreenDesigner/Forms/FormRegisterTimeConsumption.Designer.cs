﻿using Resco.Controls.SmartGrid;

namespace SampleScreenDesigner.Forms
{
    partial class FormRegisterTimeConsumption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.advancedListTime = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell5 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate2 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell7 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell8 = new Resco.Controls.AdvancedList.TextCell();
            this.labelInstructions = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.advancedListTime);
            this.touchPanel.Controls.Add(this.labelInstructions);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // advancedListTime
            // 
            this.advancedListTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListTime.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedListTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListTime.DataRows.Clear();
            this.advancedListTime.Location = new System.Drawing.Point(22, 101);
            this.advancedListTime.Name = "advancedListTime";
            this.advancedListTime.ScrollbarSmallChange = 32;
            this.advancedListTime.ScrollbarWidth = 26;
            this.advancedListTime.SelectedTemplateIndex = 1;
            this.advancedListTime.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.advancedListTime.Size = new System.Drawing.Size(436, 377);
            this.advancedListTime.TabIndex = 38;
            this.advancedListTime.Templates.Add(this.rowTemplate1);
            this.advancedListTime.Templates.Add(this.rowTemplate2);
            this.advancedListTime.TouchScrolling = true;
            // 
            // rowTemplate1
            // 
            this.rowTemplate1.BackColor = System.Drawing.Color.LightGray;
            this.rowTemplate1.CellTemplates.Add(this.textCell5);
            this.rowTemplate1.CellTemplates.Add(this.textCell6);
            this.rowTemplate1.Height = 64;
            this.rowTemplate1.Name = "rowTemplate1";
            // 
            // textCell5
            // 
            this.textCell5.CellSource.ColumnName = "MeasureCode";
            this.textCell5.DesignName = "textCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(65, 64);
            this.textCell5.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // textCell6
            // 
            this.textCell6.CellSource.ColumnName = "MeasureDescription";
            this.textCell6.DesignName = "textCell6";
            this.textCell6.Location = new System.Drawing.Point(66, 0);
            this.textCell6.Size = new System.Drawing.Size(-1, 64);
            this.textCell6.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // rowTemplate2
            // 
            this.rowTemplate2.BackColor = System.Drawing.Color.White;
            this.rowTemplate2.CellTemplates.Add(this.textCell7);
            this.rowTemplate2.CellTemplates.Add(this.textCell8);
            this.rowTemplate2.Height = 64;
            this.rowTemplate2.Name = "rowTemplate2";
            // 
            // textCell7
            // 
            this.textCell7.CellSource.ColumnName = "MeasureCode";
            this.textCell7.DesignName = "textCell7";
            this.textCell7.Location = new System.Drawing.Point(0, 0);
            this.textCell7.Size = new System.Drawing.Size(65, 64);
            this.textCell7.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // textCell8
            // 
            this.textCell8.CellSource.ColumnName = "MeasureDescription";
            this.textCell8.DesignName = "textCell8";
            this.textCell8.Location = new System.Drawing.Point(66, 0);
            this.textCell8.Size = new System.Drawing.Size(-1, 64);
            this.textCell8.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // labelInstructions
            // 
            this.labelInstructions.AutoSize = false;
            this.labelInstructions.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelInstructions.Location = new System.Drawing.Point(22, 46);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(436, 57);
            this.labelInstructions.Text = "Highlight the row and accept additional work time with Ok/Enter";
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(84, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(310, 27);
            this.labelModuleName.Text = "Register consumption time";
            // 
            // FormRegisterTimeConsumption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormRegisterTimeConsumption";
            this.Text = "FormRegisterTimeConsumption";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelInstructions;
        private Resco.Controls.AdvancedList.AdvancedList advancedListTime;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell5;
        private Resco.Controls.AdvancedList.TextCell textCell6;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate2;
        private Resco.Controls.AdvancedList.TextCell textCell7;
        private Resco.Controls.AdvancedList.TextCell textCell8;
    }
}
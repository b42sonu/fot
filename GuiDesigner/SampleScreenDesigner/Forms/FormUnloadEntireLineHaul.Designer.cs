﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using PreCom.Controls;

namespace SampleScreenDesigner.Forms
{
    partial class FormUnloadingOfEntireLineHaul
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelDestinationValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelLoadCarrierValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelDestination = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelLoadCarrier = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelDestinationValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLoadCarrierValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLoadCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.labelOrgUnit);
            this.touchPanel.Controls.Add(this.labelDestinationValue);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.labelLoadCarrierValue);
            this.touchPanel.Controls.Add(this.labelDestination);
            this.touchPanel.Controls.Add(this.labelLoadCarrier);
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // labelDestinationValue
            // 
            this.labelDestinationValue.AutoSize = false;
            this.labelDestinationValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelDestinationValue.Location = new System.Drawing.Point(157, 377);
            this.labelDestinationValue.Name = "labelDestinationValue";
            this.labelDestinationValue.Size = new System.Drawing.Size(313, 82);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // labelLoadCarrierValue
            // 
            this.labelLoadCarrierValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelLoadCarrierValue.Location = new System.Drawing.Point(157, 321);
            this.labelLoadCarrierValue.Name = "labelLoadCarrierValue";
            this.labelLoadCarrierValue.Size = new System.Drawing.Size(239, 27);
            // 
            // labelDestination
            // 
            this.labelDestination.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDestination.Location = new System.Drawing.Point(11, 377);
            this.labelDestination.Name = "labelDestination";
            this.labelDestination.Size = new System.Drawing.Size(131, 27);
            this.labelDestination.Visible = false;
            // 
            // labelLoadCarrier
            // 
            this.labelLoadCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelLoadCarrier.Location = new System.Drawing.Point(11, 321);
            this.labelLoadCarrier.Name = "labelLoadCarrier";
            this.labelLoadCarrier.Size = new System.Drawing.Size(32, 27);
            // 
            // labelOrgUnit
            // 
            this.labelOrgUnit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelOrgUnit.AutoSize = false;
            this.labelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelOrgUnit.Location = new System.Drawing.Point(11, 59);
            this.labelOrgUnit.Name = "labelOrgUnit";
            this.labelOrgUnit.Size = new System.Drawing.Size(459, 27);
            this.labelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // messageControl
            // 
            this.messageControl.Location = new System.Drawing.Point(11, 111);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(459, 152);
            this.messageControl.TabIndex = 6;
            // 
            // FormUnloadingOfEntireLineHaul
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormUnloadingOfEntireLineHaul";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelDestinationValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLoadCarrierValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelLoadCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelDestinationValue;
        private Resco.Controls.CommonControls.TransparentLabel labelLoadCarrier;
        private Resco.Controls.CommonControls.TransparentLabel labelLoadCarrierValue;
        private Resco.Controls.CommonControls.TransparentLabel labelDestination;
        private Resco.Controls.CommonControls.TransparentLabel labelOrgUnit;
        private MessageControl messageControl;
    }
}
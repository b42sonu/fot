﻿using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormOperationsListSorting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOperationsListSorting));
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationType = new System.Windows.Forms.ImageList();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.panelShowSortingState = new System.Windows.Forms.Panel();
            this.btnPopUpOk = new Resco.Controls.OutlookControls.ImageButton();
            this.pictureBoxConsignment = new System.Windows.Forms.PictureBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblWait = new System.Windows.Forms.Label();
            this.listOperations = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.StopTemplate = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellSortOrder = new Resco.Controls.AdvancedList.TextCell();
            this.imageCell1 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellOperationOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.SelectedStopTemplate = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell3 = new Resco.Controls.AdvancedList.TextCell();
            this.imageCell3 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.textCell2 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCellAlternateOperationOrderNumber = new Resco.Controls.AdvancedList.TextCell();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.touchPanel.SuspendLayout();
            this.panelShowSortingState.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPopUpOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            this.imageListOperationType.Images.Clear();
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource4"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource5"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource6"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource7"))));
            this.imageListOperationType.Images.Add(((System.Drawing.Image)(resources.GetObject("resource8"))));
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.panelShowSortingState);
            this.touchPanel.Controls.Add(this.listOperations);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // panelShowSortingState
            // 
            this.panelShowSortingState.Controls.Add(this.btnPopUpOk);
            this.panelShowSortingState.Controls.Add(this.pictureBoxConsignment);
            this.panelShowSortingState.Controls.Add(this.lblInfo);
            this.panelShowSortingState.Controls.Add(this.lblWait);
            this.panelShowSortingState.Location = new System.Drawing.Point(8, 118);
            this.panelShowSortingState.Name = "panelShowSortingState";
            this.panelShowSortingState.Size = new System.Drawing.Size(464, 217);
            // 
            // btnOk
            // 
            this.btnPopUpOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnPopUpOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnPopUpOk.ForeColor = System.Drawing.Color.White;
            this.btnPopUpOk.Location = new System.Drawing.Point(172, 152);
            this.btnPopUpOk.Name = "btnPopUpOk";
            this.btnPopUpOk.Size = new System.Drawing.Size(120, 60);
            this.btnPopUpOk.TabIndex = 32;
            this.btnPopUpOk.Text = "Ok";
            this.btnPopUpOk.Click += new System.EventHandler(this.BtnPopUpOkClick);
            // 
            // pictureBoxConsignment
            // 
            this.pictureBoxConsignment.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxConsignment.Image")));
            this.pictureBoxConsignment.Location = new System.Drawing.Point(204, 8);
            this.pictureBoxConsignment.Name = "pictureBoxConsignment";
            this.pictureBoxConsignment.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxConsignment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(3, 69);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(447, 72);
            this.lblInfo.Text = "Verifying new sorting order with Alystra Bex";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblWait
            // 
            this.lblWait.Location = new System.Drawing.Point(3, 165);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(447, 35);
            this.lblWait.Text = "Please wait...";
            this.lblWait.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // listOperations
            // 
            this.listOperations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listOperations.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listOperations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listOperations.DataRows.Clear();
            this.listOperations.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            resources.GetString("listOperations.HeaderRow")});
            this.listOperations.Location = new System.Drawing.Point(0, 40);
            this.listOperations.Name = "listOperations";
            this.listOperations.ScrollbarSmallChange = 32;
            this.listOperations.ScrollbarWidth = 26;
            this.listOperations.SelectedTemplateIndex = 2;
            this.listOperations.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listOperations.ShowHeader = true;
            this.listOperations.Size = new System.Drawing.Size(480, 395);
            this.listOperations.TabIndex = 0;
            this.listOperations.TemplateIndex = 1;
            this.listOperations.Templates.Add(this.rowTemplate1);
            this.listOperations.Templates.Add(this.StopTemplate);
            this.listOperations.Templates.Add(this.SelectedStopTemplate);
            this.listOperations.TouchScrolling = true;
            this.listOperations.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // rowTemplate1
            // 
            this.rowTemplate1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rowTemplate1.CellTemplates.Add(this.textCell1);
            this.rowTemplate1.Height = 0;
            this.rowTemplate1.Name = "rowTemplate1";
            // 
            // textCell1
            // 
            this.textCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell1.CellSource.ConstantData = "Operation List";
            this.textCell1.DesignName = "textCell1";
            this.textCell1.Location = new System.Drawing.Point(0, 0);
            this.textCell1.Size = new System.Drawing.Size(-1, 32);
            this.textCell1.Visible = false;
            // 
            // StopTemplate
            // 
            this.StopTemplate.CellTemplates.Add(this.textCellSortOrder);
            this.StopTemplate.CellTemplates.Add(this.imageCell1);
            this.StopTemplate.CellTemplates.Add(this.textCellOperationDetail);
            this.StopTemplate.CellTemplates.Add(this.textCellOperationStopId);
            this.StopTemplate.CellTemplates.Add(this.TextCellOperationOrderNumber);
            this.StopTemplate.Height = 64;
            this.StopTemplate.Name = "StopTemplate";
            // 
            // textCellSortOrder
            // 
            this.textCellSortOrder.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellSortOrder.CellSource.ColumnName = "DisplaySortOrder";
            this.textCellSortOrder.DesignName = "textCellSortOrder";
            this.textCellSortOrder.Location = new System.Drawing.Point(0, 0);
            this.textCellSortOrder.Size = new System.Drawing.Size(49, 64);
            // 
            // imageCell1
            // 
            this.imageCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCell1.AutoResize = true;
            this.imageCell1.CellSource.ColumnName = "StopSign";
            this.imageCell1.DesignName = "imageCell1";
            this.imageCell1.ImageList = this.imageListOperationType;
            this.imageCell1.Location = new System.Drawing.Point(57, 9);
            this.imageCell1.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "StopAddress";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(107, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(371, 64);
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationStopId.Visible = false;
            // 
            // TextCellOperationOrderNumber
            // 
            this.TextCellOperationOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellOperationOrderNumber.CellSource.ColumnName = "OrignalSortOrder";
            this.TextCellOperationOrderNumber.DesignName = "TextCellOperationOrderNumber";
            this.TextCellOperationOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellOperationOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellOperationOrderNumber.Visible = false;
            // 
            // SelectedStopTemplate
            // 
            this.SelectedStopTemplate.BackColor = System.Drawing.Color.LightGray;
            this.SelectedStopTemplate.CellTemplates.Add(this.textCell3);
            this.SelectedStopTemplate.CellTemplates.Add(this.imageCell3);
            this.SelectedStopTemplate.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.SelectedStopTemplate.CellTemplates.Add(this.textCell2);
            this.SelectedStopTemplate.CellTemplates.Add(this.TextCellAlternateOperationOrderNumber);
            this.SelectedStopTemplate.Height = 64;
            this.SelectedStopTemplate.Name = "SelectedStopTemplate";
            // 
            // textCell3
            // 
            this.textCell3.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell3.CellSource.ColumnName = "DisplaySortOrder";
            this.textCell3.DesignName = "textCell3";
            this.textCell3.Location = new System.Drawing.Point(0, 0);
            this.textCell3.Size = new System.Drawing.Size(49, 64);
            // 
            // imageCell3
            // 
            this.imageCell3.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCell3.AutoResize = true;
            this.imageCell3.CellSource.ColumnName = "StopSign";
            this.imageCell3.DesignName = "imageCell3";
            this.imageCell3.ImageList = this.imageListOperationType;
            this.imageCell3.Location = new System.Drawing.Point(57, 9);
            this.imageCell3.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "StopAddress";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(107, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(371, 64);
            // 
            // textCell2
            // 
            this.textCell2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell2.CellSource.ColumnName = "StopId";
            this.textCell2.DesignName = "textCell2";
            this.textCell2.Location = new System.Drawing.Point(0, 0);
            this.textCell2.Size = new System.Drawing.Size(0, 64);
            this.textCell2.Visible = false;
            // 
            // TextCellAlternateOperationOrderNumber
            // 
            this.TextCellAlternateOperationOrderNumber.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellAlternateOperationOrderNumber.CellSource.ColumnName = "OrignalSortOrder";
            this.TextCellAlternateOperationOrderNumber.DesignName = "TextCellAlternateOperationOrderNumber";
            this.TextCellAlternateOperationOrderNumber.Location = new System.Drawing.Point(0, 0);
            this.TextCellAlternateOperationOrderNumber.Size = new System.Drawing.Size(0, 64);
            this.TextCellAlternateOperationOrderNumber.Visible = false;
            // 
            // FormOperationsListSorting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormOperationsListSorting";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.touchPanel.ResumeLayout(false);
            this.panelShowSortingState.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnPopUpOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;
        private System.Windows.Forms.ImageList imageListOperationType;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listOperations;



        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell1;
        private Resco.Controls.AdvancedList.RowTemplate StopTemplate;
        private Resco.Controls.AdvancedList.TextCell textCellSortOrder;
        private Resco.Controls.AdvancedList.ImageCell imageCell1;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedList.TextCell TextCellOperationOrderNumber;
        private Resco.Controls.AdvancedList.RowTemplate SelectedStopTemplate;
        private Resco.Controls.AdvancedList.TextCell textCell3;
        private Resco.Controls.AdvancedList.ImageCell imageCell3;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCell2;
        private Resco.Controls.AdvancedList.TextCell TextCellAlternateOperationOrderNumber;
        private System.Windows.Forms.Panel panelShowSortingState;
        private Resco.Controls.OutlookControls.ImageButton btnPopUpOk;
        private System.Windows.Forms.PictureBox pictureBoxConsignment;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label lblWait;


    }
}
﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormManualSorting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoadList));
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.btnPredef = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsigneeAdr = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeAdrHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnDown = new Resco.Controls.OutlookControls.ImageButton();
            this.btnUp = new Resco.Controls.OutlookControls.ImageButton();
            this.listDeliveries = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellAlternameOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellSortNo = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateSortNo = new Resco.Controls.AdvancedList.TextCell();
            this.lblConsignee = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeHead = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentItemsHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.tabControlDeliveries = new Resco.Controls.CommonControls.TabControl();
            this.tabNotStarted = new Resco.Controls.CommonControls.TabPage();
            this.tabCompleted = new Resco.Controls.CommonControls.TabPage();
            this.tabAll = new Resco.Controls.CommonControls.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPredef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdrHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItemsHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).BeginInit();
            this.tabControlDeliveries.SuspendLayout();
            this.tabNotStarted.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Load List";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            this.imageListOperationListIcons.Images.Clear();
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("Delivery"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.lblConsigneeAdr);
            this.touchPanel.Controls.Add(this.lblConsigneeAdrHeading);
            this.touchPanel.Controls.Add(this.btnDown);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnPredef);
            this.touchPanel.Controls.Add(this.btnUp);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblConsignee);
            this.touchPanel.Controls.Add(this.lblConsignmentItems);
            this.touchPanel.Controls.Add(this.lblConsigneeHead);
            this.touchPanel.Controls.Add(this.lblConsignmentItemsHeading);
            this.touchPanel.Controls.Add(listDeliveries);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);


            // 
            // btnPredef
            // 
            this.btnPredef.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnPredef.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnPredef.ForeColor = System.Drawing.Color.White;
            this.btnPredef.Location = new System.Drawing.Point(161, 483);
            this.btnPredef.Name = "btnPredef";
            this.btnPredef.Size = new System.Drawing.Size(158, 50);
            this.btnPredef.TabIndex = 30;
            this.btnPredef.Text = "Predef";
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(322, 483);
            this.btnOk.Name = "btnScan";
            this.btnOk.Size = new System.Drawing.Size(158, 50);
            this.btnOk.TabIndex = 32;
            this.btnOk.Text = "Ok";

            // 
            // btnDown
            // 
            this.btnDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDown.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDown.ForeColor = System.Drawing.Color.White;
            this.btnDown.Location = new System.Drawing.Point(1, 428);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(158, 50);
            this.btnDown.TabIndex = 38;
            this.btnDown.Text = "Down";
            this.btnDown.Click += btnDown_Click;

            // 
            // btnUp
            // 
            this.btnUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnUp.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnUp.ForeColor = System.Drawing.Color.White;
            this.btnUp.Location = new System.Drawing.Point(322, 428);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(158, 50);
            this.btnUp.TabIndex = 38;
            this.btnUp.Text = "Up";
            this.btnUp.Click += new System.EventHandler(btnUp_Click);


            // 
            // listDeliveries
            // 
            this.listDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listDeliveries.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listDeliveries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listDeliveries.DataRows.Clear();
            this.listDeliveries.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listDeliveries.Location = new System.Drawing.Point(0, 40);
            this.listDeliveries.MultiSelect = true;
            this.listDeliveries.Name = "listDeliveries";
            this.listDeliveries.ScrollbarSmallChange = 32;
            this.listDeliveries.ScrollbarWidth = 26;
            this.listDeliveries.SelectedTemplateIndex = 2;
            this.listDeliveries.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listDeliveries.ShowHeader = true;
            this.listDeliveries.Size = new System.Drawing.Size(480, 380);
            this.listDeliveries.TabIndex = 2;
            this.listDeliveries.TemplateIndex = 1;
            this.listDeliveries.Templates.Add(this.RowTemplateHeader);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOp);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.listDeliveries.TouchScrolling = true;
            this.listDeliveries.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationType);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationDetail);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationStatus);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationOperationId);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellSortNo);
            this.RowTemplatePlannedOp.Height = 64;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";

            // 
            // ImageCellOperationType
            // 
            this.ImageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationType.AutoResize = true;
            this.ImageCellOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellOperationType.DesignName = "ImageCellOperationType";
            this.ImageCellOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationType.Location = new System.Drawing.Point(57, 10);
            this.ImageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(100, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(327, 64);
            // 
            // ImageCellOperationStatus
            // 
            this.ImageCellOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellOperationStatus.DesignName = "ImageCellOperationStatus";
            this.ImageCellOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "DeliveryOperation.Consignment.ConsignmentNumber";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;

            // 
            // textCellSortNo
            // 
            this.textCellSortNo.CellSource.ColumnName = "DefaultSortNo";
            this.textCellSortNo.DesignName = "textCellSortNo";
            this.textCellSortNo.Location = new System.Drawing.Point(15, 0);
            this.textCellSortNo.Size = new System.Drawing.Size(40, 40);


            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternateOperationType);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternameOperationStatus);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateSortNo);
            this.RowTemplatePlannedOpAlternateTemp.Height = 64;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";

            // 
            // textCellAlternateSortNo
            // 
            this.textCellAlternateSortNo.CellSource.ColumnName = "DefaultSortNo";
            this.textCellAlternateSortNo.DesignName = "textCellSortNo";
            this.textCellAlternateSortNo.Location = new System.Drawing.Point(15, 0);
            this.textCellAlternateSortNo.Size = new System.Drawing.Size(40, 40);

            // 
            // ImageCellAlternateOperationType
            // 
            this.ImageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateOperationType.AutoResize = true;
            this.ImageCellAlternateOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.ImageCellAlternateOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternateOperationType.Location = new System.Drawing.Point(57, 10);
            this.ImageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(100, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(327, 64);
            // 
            // ImageCellAlternameOperationStatus
            // 
            this.ImageCellAlternameOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternameOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellAlternameOperationStatus.DesignName = "ImageCellAlternameOperationStatus";
            this.ImageCellAlternameOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternameOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellAlternameOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;
           

            // 
            // lblConsigneeHead
            // 
            this.lblConsigneeHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeHead.Location = new System.Drawing.Point(3, 337);
            this.lblConsigneeHead.Name = "lblConsigneeHead";
            this.lblConsigneeHead.Size = new System.Drawing.Size(61, 14);
            this.lblConsigneeHead.Text = "Consignee:";
            // 
            // lblConsignee
            // 
            this.lblConsignee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignee.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignee.Location = new System.Drawing.Point(170, 337);
            this.lblConsignee.Name = "lblConsignee";
            this.lblConsignee.Size = new System.Drawing.Size(21, 14);
            this.lblConsignee.Text = "Abc";

            // 
            // lblConsignmentItemsHeading
            // 
            this.lblConsignmentItemsHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItemsHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignmentItemsHeading.Location = new System.Drawing.Point(3, 369);
            this.lblConsignmentItemsHeading.Name = "lblConsignmentItemsHeading";
            this.lblConsignmentItemsHeading.Size = new System.Drawing.Size(81, 14);
            this.lblConsignmentItemsHeading.Text = "Consignments:";
           
            // 
            // lblConsignmentItems
            // 
            this.lblConsignmentItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItems.Location = new System.Drawing.Point(170, 368);
            this.lblConsignmentItems.Name = "lblConsignmentItems";
            this.lblConsignmentItems.Size = new System.Drawing.Size(20, 15);
            this.lblConsignmentItems.Text = "Abc";
           

            // 
            // lblConsigneeAdrHeading
            // 
            this.lblConsigneeAdrHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeAdrHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeAdrHeading.Location = new System.Drawing.Point(3, 398);
            this.lblConsigneeAdrHeading.Name = "lblConsigneeAdrHeading";
            this.lblConsigneeAdrHeading.Size = new System.Drawing.Size(82, 14);
            this.lblConsigneeAdrHeading.Text = "Consignee adr:";
           
            
            // 
            // lblConsigneeAdr
            // 
            this.lblConsigneeAdr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeAdr.Location = new System.Drawing.Point(170, 397);
            this.lblConsigneeAdr.Name = "lblConsigneeAdr";
            this.lblConsigneeAdr.Size = new System.Drawing.Size(20, 15);
            this.lblConsigneeAdr.Text = "Abc";
            
            


            // 
            // FormLoadList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormLoadList";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPredef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdrHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItemsHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).EndInit();
            this.tabControlDeliveries.ResumeLayout(false);
            this.tabNotStarted.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        #endregion
        private Resco.Controls.CommonControls.TabControl tabControlDeliveries;
        private Resco.Controls.CommonControls.TabPage tabNotStarted;
        private Resco.Controls.CommonControls.TabPage tabCompleted;
        private Resco.Controls.CommonControls.TabPage tabAll;

        private System.Windows.Forms.ImageList imageListOperationListIcons;
       
        private Resco.Controls.OutlookControls.ImageButton btnPredef;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.OutlookControls.ImageButton btnDown;
        private Resco.Controls.OutlookControls.ImageButton btnUp;



        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listDeliveries;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignee;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItems;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeHead;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItemsHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeAdr;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeAdrHeading;
        
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;
        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellSortNo;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateSortNo;

        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;

        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;

       




    }
}
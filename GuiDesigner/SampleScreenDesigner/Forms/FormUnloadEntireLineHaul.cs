﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormUnloadingOfEntireLineHaul : Form
    {
        private TabMultipleButtons _tabMultipleButtons;
        public FormUnloadingOfEntireLineHaul()
        {
            InitializeComponent();
            SetTextToGui();
        }
        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.UnloadPickUpTruck;
            labelModuleName.Text = GlobalTexts.UnloadPickUpTruck;
            labelDestination.Text = GlobalTexts.Destination + ":";

            _tabMultipleButtons = new TabMultipleButtons();
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = "ButtonBack" });
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.RegisterCorrectionForItem, ButtonBackClick, "ButtonRegisterDelayed"));
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonBackClick, TabButtonType.Confirm) { Name = "ButtonOk" });
            touchPanel.Controls.Add(_tabMultipleButtons);
            _tabMultipleButtons.GenerateButtons();
        }
        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        private void OnCauseChanged(object sender, EventArgs e)
        {
           
        }
    }
}
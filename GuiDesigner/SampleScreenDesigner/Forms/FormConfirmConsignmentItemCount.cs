﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount
{
    public partial class FormConfirmConsignmentItemCount : Form
    {
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private TabMultipleButtons _tabButtons;
        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public FormConfirmConsignmentItemCount()
        {
            InitializeComponent();
            SetTextToGui();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            //InitOnScreenKeyBoardProperties();
        }
        private void SetTextToGui()
        {
            _tabButtons = new TabMultipleButtons();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonConfirmClick, TabButtonOkName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();



          
            labelModuleName.Text = GlobalTexts.Pickup;
            transparentLabel3.Text = GlobalTexts.ConfirmConsignmentCount;
            transparentLabel2.Text = GlobalTexts.ConsignmentId + ":";
            transparentLabel1.Text = GlobalTexts.InformationConsignmentScanned;
            
        }
        
        private void ButtonBackClick(object sender, EventArgs e)
        {
        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {
        }

        
        private void OnConsignmentItemsEditClicked(object sender, EventArgs e)
        {
        }
        private void EditConsignmentItemCountKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                
            }

        }

    }
}
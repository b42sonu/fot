﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    [Flags]
    public enum OrderInfoValidationType
    {
        NoValidation = 0,
        CustomerId = 1,
        OrderNumber = 2,
        CustomerIdAndBookingNumber = 3,
        AmphoraValidation = 4,
    };
    public partial class FormCustomerBookingDetail : Form
    {
        #region "Variables"
        private TabMultipleButtons _tabButtons;
       
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        #endregion
        public FormCustomerBookingDetail()
        {
            InitializeComponent();
            SetTextToGui();
            DesignViewAsPerCustomerInfo(OrderInfoValidationType.CustomerIdAndBookingNumber, false);
        }
        private void SetTextToGui()
        {
            _tabButtons = new TabMultipleButtons();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonOkName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();



            labelGenerateBookingNumber.Text = GlobalTexts.ClickToGenrateBookingNumber + ":";
            labelBookingNumber.Text = GlobalTexts.BookingNumber + ":";
            labelCustomerNumber.Text = GlobalTexts.CustomerNumber + ":";
            labelModuleName.Text = "UnplannedPickUp";
            btnGenerateBookingNnumber.Text = GlobalTexts.GenerateBookingNumber;


            labelStopId.Text = GlobalTexts.StopId + ":";
            labelCarrierId.Text = GlobalTexts.CarrierMoveId + ":";
            lblHeading.Text = "OrgUnitName";
        }
        private void TxtConsignmentNumberClick(object sender, EventArgs e)
        {

        }

        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }
        private void ButtonGenerateBookingNumberClick(object sender, EventArgs e)
        {

        }
        private void TxtBookingNumberKeyUp(object sender, KeyEventArgs e)
        {
        }
        private void TxtCustomerNumberKeyUp(object sender, KeyEventArgs e)
        {
        }

        /// <summary>
        /// method for show control on the basis of OrderInfoValidationType..
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isGenerateBookingNumberRequired"> </param>
        private void DesignViewAsPerCustomerInfo(OrderInfoValidationType type, bool isGenerateBookingNumberRequired)//OrderInfoValidationType type)
        {

            switch (type)
            {
                case OrderInfoValidationType.CustomerId:  //only customer
                    touchPanel.Controls.Add(txtCustomerNumber);
                    touchPanel.Controls.Add(labelCustomerNumber);
                    break;

                case OrderInfoValidationType.OrderNumber://only booking
                    labelGenerateBookingNumber.Location = new Point(ScaleUtil.GetScaledPosition(labelBookingNumber.Location.X),
                                                                    ScaleUtil.GetScaledPosition(labelBookingNumber.Location.Y)); //labelBookingNumber.Location;
                    btnGenerateBookingNnumber.Location = new Point(ScaleUtil.GetScaledPosition(txtBookingNumber.Location.X),
                                                                    ScaleUtil.GetScaledPosition(txtBookingNumber.Location.Y));//txtBookingNumber.Location;

                    labelBookingNumber.Location = new Point(ScaleUtil.GetScaledPosition(labelCustomerNumber.Location.X),
                                                                    ScaleUtil.GetScaledPosition(labelCustomerNumber.Location.Y));//labelCustomerNumber.Location;
                    txtBookingNumber.Location = new Point(ScaleUtil.GetScaledPosition(txtCustomerNumber.Location.X),
                                                                    ScaleUtil.GetScaledPosition(txtCustomerNumber.Location.Y));//txtCustomerNumber.Location;

                    touchPanel.Controls.Add(txtBookingNumber);
                    touchPanel.Controls.Add(labelBookingNumber);

                    if (isGenerateBookingNumberRequired)
                    {
                        touchPanel.Controls.Add(btnGenerateBookingNnumber);
                        touchPanel.Controls.Add(labelGenerateBookingNumber);
                    }
                    //_tabButtons.ShowButton(TabButtonOkName, false);
                    break;

                case OrderInfoValidationType.CustomerIdAndBookingNumber://both (customer and booking)
                    if (isGenerateBookingNumberRequired)
                    {
                        touchPanel.Controls.Add(btnGenerateBookingNnumber);
                        touchPanel.Controls.Add(labelGenerateBookingNumber);
                    }

                    touchPanel.Controls.Add(txtCustomerNumber);
                    touchPanel.Controls.Add(labelCustomerNumber);

                    touchPanel.Controls.Add(txtBookingNumber);
                    touchPanel.Controls.Add(labelBookingNumber);


                    //_tabButtons.ShowButton(TabButtonOkName, false);

                    break;

                case OrderInfoValidationType.NoValidation:
                    DesignViewAsPerCustomerInfo(OrderInfoValidationType.OrderNumber, isGenerateBookingNumberRequired);
                    break;

                case OrderInfoValidationType.AmphoraValidation:
                    labelBookingNumber.Location = new Point(ScaleUtil.GetScaledPosition(labelCustomerNumber.Location.X),
                                                                    ScaleUtil.GetScaledPosition(labelCustomerNumber.Location.Y));//labelCustomerNumber.Location;
                    txtBookingNumber.Location = new Point(ScaleUtil.GetScaledPosition(txtCustomerNumber.Location.X),
                                                                    ScaleUtil.GetScaledPosition(txtCustomerNumber.Location.Y));//txtCustomerNumber.Location;

                    touchPanel.Controls.Add(txtBookingNumber);
                    touchPanel.Controls.Add(labelBookingNumber);

                    touchPanel.Controls.Add(txtCarrierId);
                    touchPanel.Controls.Add(labelCarrierId);

                    touchPanel.Controls.Add(txtStopId);
                    touchPanel.Controls.Add(labelStopId);
                    break;
            }



        }

    }
}
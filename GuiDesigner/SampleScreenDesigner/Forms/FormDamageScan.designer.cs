﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls;
using Resco.Controls.CommonControls;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using System.Windows.Forms;
using System.Drawing;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormDamageScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsignmentEntityLabel = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentEntityNoLabel = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblScanHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtScannedNumber = new System.Windows.Forms.TextBox();
            this.lblCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblType = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblComments = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentEntityLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentEntityNoLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblConsignmentEntityLabel);
            this.touchPanel.Controls.Add(this.lblConsignmentEntityNoLabel);
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.lblScanHeading);
            this.touchPanel.Controls.Add(this.lblMeasure);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblType);
            this.touchPanel.Controls.Add(this.lblComments);
            this.touchPanel.Controls.Add(this.txtComments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // lblConsignmentEntityLabel
            // 
            this.lblConsignmentEntityLabel.AutoSize = false;
            this.lblConsignmentEntityLabel.Location = new System.Drawing.Point(8, 31);
            this.lblConsignmentEntityLabel.Name = "lblConsignmentEntityLabel";
            this.lblConsignmentEntityLabel.Size = new System.Drawing.Size(209, 30);
           
            // 
            // lblConsignmentEntityNoLabel
            // 
            this.lblConsignmentEntityNoLabel.AutoSize = false;
            this.lblConsignmentEntityNoLabel.Location = new System.Drawing.Point(223, 34);
            this.lblConsignmentEntityNoLabel.Name = "lblConsignmentEntityNoLabel";
            this.lblConsignmentEntityNoLabel.Size = new System.Drawing.Size(251, 25);
          
            // 
            // lblScanHeading
            // 
            this.lblScanHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblScanHeading.Location = new System.Drawing.Point(29, 237);
            this.lblScanHeading.Name = "lblScanHeading";
            this.lblScanHeading.Size = new System.Drawing.Size(0, 0);
            // 
            // lblMeasure
            // 
            this.lblMeasure.Location = new System.Drawing.Point(14, 114);
            this.lblMeasure.Name = "lblMeasure";
            this.lblMeasure.Size = new System.Drawing.Size(0, 0);
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(24, 272);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(424, 40);
            this.txtScannedNumber.TabIndex = 37;
            // 
            // lblCause
            // 
            this.lblCause.Location = new System.Drawing.Point(14, 85);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(0, 0);
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(14, 56);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 0);
            // 
            // lblComments
            // 
            this.lblComments.AutoSize = false;
            this.lblComments.Location = new System.Drawing.Point(14, 140);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(461, 26);
            // 
            // txtComments
            // 
            this.txtComments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(217)))), ((int)(((byte)(218)))));
            this.txtComments.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtComments.Location = new System.Drawing.Point(14, 168);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.ReadOnly = true;
            this.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComments.Size = new System.Drawing.Size(460, 70);
            this.txtComments.TabIndex = 62;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(131, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // messageControlValidateScan
            // 
            this.messageControlValidateScan.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControlValidateScan.Location = new System.Drawing.Point(24, 316);
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(424, 110);
            this.messageControlValidateScan.TabIndex = 56;
            // 
            // FormDamageScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDamageScan";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentEntityLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentEntityNoLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private TextBox txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblScanHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblMeasure;
        private Resco.Controls.CommonControls.TransparentLabel lblCause;
        private Resco.Controls.CommonControls.TransparentLabel lblType;
        private Resco.Controls.CommonControls.TransparentLabel lblComments;
        private MessageControl messageControlValidateScan;
        private TransparentLabel lblConsignmentEntityLabel;
        private TransparentLabel lblConsignmentEntityNoLabel;
        private System.Windows.Forms.TextBox txtComments;
    }
}
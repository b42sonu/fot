﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    public partial class FormProfileSettings : Form
    {
        public FormProfileSettings()
        {
            InitializeComponent();
            AddFieldsForUserProfile();
           // _messageControlBox.ShowMessage("dfsaf",MessageControl.State.Information);
        }
        /// <summary>
        /// This method displays the user profile field on the screen. THis method currently work for MC70. For other
        /// </summary>
        private void AddFieldsForUserProfile()
        {
            Hashtable _fieldRights = new Hashtable();

            _fieldRights.Add("OrgUnitId",true);
            _fieldRights.Add("PowerUnit", true);
            _fieldRights.Add("ContainmentUnit", true);
            _fieldRights.Add("UnitId", true);
            _fieldRights.Add("PostalCode", true);
            _fieldRights.Add("TMSAffiliation", true);
            _fieldRights.Add("RouteId", true);
            _fieldRights.Add("CompanyCode", true);
            _fieldRights.Add("TelephoneNumber", true);

           // _fieldRights.Add("msg", true);
            //Get rights for fields on the basis of current role of user.
            //GetRightsForFieldsToDisplay();
            int ctrlHeight = ScaleUtil.GetScaledPosition(21); //Specifies the height for each control.
            int ctrlSpace = ScaleUtil.GetScaledPosition(30); // Specifies the vertical space between rows of controls.
            int yCord = ScaleUtil.GetScaledPosition(20);//57; // Specifies the initial vertical point from where drawing of controls will start

            SuspendLayout();
            touchPanelControls.SuspendLayout();
            //Define scaling of dimentions
            //AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            //AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            //Define sizes for text field and labels
            var lblSize = new Size(ScaleUtil.GetScaledPosition(130), ScaleUtil.GetScaledPosition(20));
            var txtSize = new Size(ScaleUtil.GetScaledPosition(250), ScaleUtil.GetScaledPosition(21));
            //Controls.Add(lblHeader);
            //Controls.Add(lblMessage);

            //Iterate for each right
            foreach (var obj in _fieldRights.Keys)
            {
                //Set location for label and textfield
                var txtLocation = new Point(ScaleUtil.GetScaledPosition(200), yCord);
                var lblLocation = new Point(ScaleUtil.GetScaledPosition(2), yCord);
                // Get right value it will be either x or (x)
                var fieldRightValue = Convert.ToString(_fieldRights[obj]);
                //For each right value check in switch statement and display right control
                //according to that.
                switch (obj.ToString())
                {
                    case "OrgUnitId": //Comments from first case will be same for all other cases.
                        //Set attributes for textfield and label control
                        txtOrgUnitId.Location = txtLocation;
                        txtOrgUnitId.Enabled = true;//fieldRightValue != "(x)";
                        txtOrgUnitId.Size = txtSize;
                        lblOrgUnitId.Location = lblLocation;
                        lblOrgUnitId.Size = lblSize;
                        //Add text field and label control on form
                        touchPanelControls.Controls.Add(txtOrgUnitId);
                        touchPanelControls.Controls.Add(lblOrgUnitId);
                        //Update vertical point by adding control space and control height.
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "PowerUnit":
                        txtPowerUnit.Location = txtLocation;
                        //If the right value is (x) ( which means field value should not be editable if screen opens at login
                        //time) and _isopenedFromSettings==false (which means screen is opened at login time) then
                        //Field should be disabled. This is same for all other cases in this switch statement.
                        txtPowerUnit.Enabled = true;//fieldRightValue != "(x)";
                        txtPowerUnit.Size = txtSize;
                        lblPowerUnit.Location = lblLocation;
                        lblPowerUnit.Size = lblSize;
                        touchPanelControls.Controls.Add(txtPowerUnit);
                        touchPanelControls.Controls.Add(lblPowerUnit);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "ContainmentUnit":
                        txtContainmentUnit.Location = txtLocation;
                        txtContainmentUnit.Enabled = true;//fieldRightValue != "(x)";
                        txtContainmentUnit.Size = txtSize;
                        lblContainmentUnit.Location = lblLocation;
                        lblContainmentUnit.Size = lblSize;
                        touchPanelControls.Controls.Add(txtContainmentUnit);
                        touchPanelControls.Controls.Add(lblContainmentUnit);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "UnitId":
                        txtUnitId.Location = txtLocation;
                        txtUnitId.Enabled = fieldRightValue != "(x)";
                        txtUnitId.Size = txtSize;
                        lblUnitId.Location = lblLocation;
                        lblUnitId.Size = lblSize;
                        touchPanelControls.Controls.Add(txtUnitId);
                        touchPanelControls.Controls.Add(lblUnitId);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "PostalCode":
                        txtPostalCode.Location = txtLocation;
                        txtPostalCode.Enabled = true;//fieldRightValue != "(x)";
                        txtPostalCode.Size = txtSize;
                        lblPostalCode.Location = lblLocation;
                        lblPostalCode.Size = lblSize;
                        touchPanelControls.Controls.Add(txtPostalCode);
                        touchPanelControls.Controls.Add(lblPostalCode);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "TMSAffiliation":
                        CmbTMSAffiliation.Items.Add("TM_OTC");
                        CmbTMSAffiliation.Items.Add("Alystra");
                        //CmbTMSAffiliation.DataSource = Module.TMSList;
                        //CmbTMSAffiliation.DisplayMember = "TMSId";
                        //CmbTMSAffiliation.ValueMember = "TMSId";
                        CmbTMSAffiliation.Location = txtLocation;
                        CmbTMSAffiliation.Size = txtSize;
                        lblTMSAffiliation.Location = lblLocation;
                        lblTMSAffiliation.Size = lblSize;
                        touchPanelControls.Controls.Add(CmbTMSAffiliation);
                        touchPanelControls.Controls.Add(lblTMSAffiliation);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "RouteId":
                        txtRouteId.Location = txtLocation;
                        txtRouteId.Enabled = true;//fieldRightValue != "(x)";
                        txtRouteId.Size = txtSize;
                        lblRouteId.Location = lblLocation;
                        lblRouteId.Size = lblSize;
                        touchPanelControls.Controls.Add(txtRouteId);
                        touchPanelControls.Controls.Add(lblRouteId);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "CompanyCode":
                        txtCompanyCode.Location = txtLocation;
                        txtCompanyCode.Enabled = true;//fieldRightValue != "(x)";
                        txtCompanyCode.Size = txtSize;
                        lblCompanyCode.Location = lblLocation;
                        lblCompanyCode.Size = lblSize;
                        touchPanelControls.Controls.Add(txtCompanyCode);
                        touchPanelControls.Controls.Add(lblCompanyCode);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "CountryCode":
                        txtCountryCode.Location = txtLocation;
                        txtCountryCode.Enabled = true;//fieldRightValue != "(x)";
                        txtCountryCode.Size = txtSize;
                        lblCountryCode.Location = lblLocation;
                        lblCountryCode.Size = lblSize;
                        touchPanelControls.Controls.Add(txtCountryCode);
                        touchPanelControls.Controls.Add(lblCountryCode);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;
                    case "TelephoneNumber":
                        txtTelephoneNumber.Location = txtLocation;
                        txtTelephoneNumber.Enabled = true;//fieldRightValue != "(x)";
                        txtTelephoneNumber.Size = txtSize;
                        lblTelephoneNumber.Location = lblLocation;
                        lblTelephoneNumber.Size = lblSize;
                        touchPanelControls.Controls.Add(txtTelephoneNumber);
                        touchPanelControls.Controls.Add(lblTelephoneNumber);
                        yCord = yCord + ctrlHeight + ctrlSpace;
                        break;

                  
                }
            }
            touchPanelControls.ResumeLayout();
           
            ResumeLayout(false);
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {

        }

        private void BtnOkClick(object sender, EventArgs e)
        {

        }



        /// <summary>
        /// Selected Index change event of combobox for TMSAffiliation.
        /// </summary>
        /// <param name="sender">Object who sends that event.</param>
        /// <param name="e">object of type EventArgs, send by sender object.</param>
        private void CmbTmsAffiliationSelectedIndexChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

    }
}
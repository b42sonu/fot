﻿using System.Windows.Forms;
namespace SampleScreenDesigner.Forms
{
    partial class FormAttemptedDelivery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblProcessHeader = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblInfo = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNoOfConsigItm = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNoOfLoadCarriers = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNoOfServices = new Resco.Controls.CommonControls.TransparentLabel();
            this.inkbSignature = new Resco.Controls.OutlookControls.InkBox();
            this.lblSignature = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.lblFullName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblProcessHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfConsigItm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfServices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblProcessHeader);
            this.touchPanel.Controls.Add(this.lblInfo);
            this.touchPanel.Controls.Add(this.lblNoOfConsigItm);
            this.touchPanel.Controls.Add(this.lblNoOfLoadCarriers);
            this.touchPanel.Controls.Add(this.lblNoOfServices);
            this.touchPanel.Controls.Add(this.inkbSignature);
            this.touchPanel.Controls.Add(this.lblSignature);
            this.touchPanel.Controls.Add(this.txtFullName);
            this.touchPanel.Controls.Add(this.lblFullName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblProcessHeader
            // 
            this.lblProcessHeader.AutoSize = false;
            this.lblProcessHeader.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblProcessHeader.Location = new System.Drawing.Point(0, 2);
            this.lblProcessHeader.Name = "lblProcessHeader";
            this.lblProcessHeader.Size = new System.Drawing.Size(480, 30);
            this.lblProcessHeader.Text = "OP Region Oslo Og Producksio";
            this.lblProcessHeader.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = false;
            this.lblInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblInfo.Location = new System.Drawing.Point(27, 431);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(438, 62);
            this.lblInfo.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // lblNoOfConsigItm
            // 
            this.lblNoOfConsigItm.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNoOfConsigItm.Location = new System.Drawing.Point(14, 95);
            this.lblNoOfConsigItm.Name = "lblNoOfConsigItm";
            this.lblNoOfConsigItm.Size = new System.Drawing.Size(169, 27);
            this.lblNoOfConsigItm.Text = "No of items : 10";
            // 
            // lblNoOfLoadCarriers
            // 
            this.lblNoOfLoadCarriers.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNoOfLoadCarriers.Location = new System.Drawing.Point(14, 68);
            this.lblNoOfLoadCarriers.Name = "lblNoOfLoadCarriers";
            this.lblNoOfLoadCarriers.Size = new System.Drawing.Size(243, 27);
            this.lblNoOfLoadCarriers.Text = "No of load carriers : 10";
            // 
            // lblNoOfServices
            // 
            this.lblNoOfServices.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNoOfServices.Location = new System.Drawing.Point(14, 122);
            this.lblNoOfServices.Name = "lblNoOfServices";
            this.lblNoOfServices.Size = new System.Drawing.Size(122, 27);
            this.lblNoOfServices.Text = "services : 4";
            // 
            // inkbSignature
            // 
            this.inkbSignature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.inkbSignature.Location = new System.Drawing.Point(14, 264);
            this.inkbSignature.Name = "inkbSignature";
            this.inkbSignature.Size = new System.Drawing.Size(451, 156);
            this.inkbSignature.TabIndex = 2;
            // 
            // lblSignature
            // 
            this.lblSignature.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblSignature.Location = new System.Drawing.Point(14, 231);
            this.lblSignature.Name = "lblSignature";
            this.lblSignature.Size = new System.Drawing.Size(110, 27);
            this.lblSignature.Text = "Signature:";
            // 
            // txtFullName
            // 
            this.txtFullName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtFullName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtFullName.Location = new System.Drawing.Point(14, 185);
            this.txtFullName.MaxLength = 28;
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(451, 40);
            this.txtFullName.TabIndex = 1;
            // 
            // lblFullName
            // 
            this.lblFullName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblFullName.Location = new System.Drawing.Point(14, 150);
            this.lblFullName.Name = "lblFullName";
            this.lblFullName.Size = new System.Drawing.Size(175, 27);
            this.lblFullName.Text = "Customer Name";
            // 
            // FormAttemptedDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormAttemptedDelivery";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblProcessHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfConsigItm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfServices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFullName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.InkBox inkbSignature;
        private TextBox txtFullName;
        private Resco.Controls.CommonControls.TransparentLabel lblNoOfConsigItm;
        private Resco.Controls.CommonControls.TransparentLabel lblNoOfLoadCarriers;
        private Resco.Controls.CommonControls.TransparentLabel lblNoOfServices;
        private Resco.Controls.CommonControls.TransparentLabel lblSignature;
        private Resco.Controls.CommonControls.TransparentLabel lblFullName;
        private Resco.Controls.CommonControls.TransparentLabel lblInfo;
        private Resco.Controls.CommonControls.TransparentLabel lblProcessHeader;
    }
}
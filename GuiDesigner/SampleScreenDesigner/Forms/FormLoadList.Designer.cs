﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormLoadList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoadList));
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.btnDetails = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnScan = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsigneeAdr = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeAdrHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnRefresh = new Resco.Controls.OutlookControls.ImageButton();
            this.listDeliveries = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellAlternameOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.lblConsignee = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeHead = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentItemsHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.tabControlDeliveries = new Resco.Controls.CommonControls.TabControl();
            this.tabNotStarted = new Resco.Controls.CommonControls.TabPage();
            this.tabCompleted = new Resco.Controls.CommonControls.TabPage();
            this.tabAll = new Resco.Controls.CommonControls.TabPage();
            this._messageControlBox = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdrHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItemsHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).BeginInit();
            this.tabControlDeliveries.SuspendLayout();
            this.tabNotStarted.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Load List";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            this.imageListOperationListIcons.Images.Clear();
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("Delivery"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            // 
            // btnDetails
            // 
            this.btnDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDetails.ForeColor = System.Drawing.Color.White;
            this.btnDetails.Location = new System.Drawing.Point(161, 483);
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.Size = new System.Drawing.Size(158, 50);
            this.btnDetails.TabIndex = 30;
            this.btnDetails.Text = "Details";
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            // 
            // btnScan
            // 
            this.btnScan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnScan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnScan.ForeColor = System.Drawing.Color.White;
            this.btnScan.Location = new System.Drawing.Point(322, 483);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(158, 50);
            this.btnScan.TabIndex = 32;
            this.btnScan.Text = "Scan";
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.lblConsigneeAdr);
            this.touchPanel.Controls.Add(this.lblConsigneeAdrHeading);
            this.touchPanel.Controls.Add(this.btnRefresh);
            this.touchPanel.Controls.Add(this.btnScan);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnDetails);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblConsignee);
            this.touchPanel.Controls.Add(this.lblConsignmentItems);
            this.touchPanel.Controls.Add(this.lblConsigneeHead);
            this.touchPanel.Controls.Add(this.lblConsignmentItemsHeading);
            this.touchPanel.Controls.Add(this.tabControlDeliveries);
            this.touchPanel.Controls.Add(this._messageControlBox);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnRefresh.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnRefresh.ForeColor = System.Drawing.Color.White;
            this.btnRefresh.Location = new System.Drawing.Point(1, 428);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(158, 50);
            this.btnRefresh.TabIndex = 38;
            this.btnRefresh.Text = "Refresh";
            // 
            // listDeliveries
            // 
            this.listDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listDeliveries.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listDeliveries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listDeliveries.DataRows.Clear();
            this.listDeliveries.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listDeliveries.Location = new System.Drawing.Point(0, 0);
            this.listDeliveries.MultiSelect = true;
            this.listDeliveries.Name = "listDeliveries";
            this.listDeliveries.ScrollbarSmallChange = 32;
            this.listDeliveries.ScrollbarWidth = 26;
            this.listDeliveries.SelectedTemplateIndex = 2;
            this.listDeliveries.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listDeliveries.ShowHeader = true;
            this.listDeliveries.Size = new System.Drawing.Size(480, 221);
            this.listDeliveries.TabIndex = 2;
            this.listDeliveries.TemplateIndex = 1;
            this.listDeliveries.Templates.Add(this.RowTemplateHeader);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOp);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.listDeliveries.TouchScrolling = true;
            this.listDeliveries.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationType);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationDetail);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationStatus);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationOperationId);
            this.RowTemplatePlannedOp.Height = 64;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.ImageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationType.AutoResize = true;
            this.ImageCellOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellOperationType.DesignName = "ImageCellOperationType";
            this.ImageCellOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationType.Location = new System.Drawing.Point(15, 10);
            this.ImageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // ImageCellOperationStatus
            // 
            this.ImageCellOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellOperationStatus.DesignName = "ImageCellOperationStatus";
            this.ImageCellOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "DeliveryOperation.Consignment.ConsignmentNumber";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;
            
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternateOperationType);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternameOperationStatus);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.RowTemplatePlannedOpAlternateTemp.Height = 64;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.ImageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateOperationType.AutoResize = true;
            this.ImageCellAlternateOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.ImageCellAlternateOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternateOperationType.Location = new System.Drawing.Point(15, 10);
            this.ImageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // ImageCellAlternameOperationStatus
            // 
            this.ImageCellAlternameOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternameOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellAlternameOperationStatus.DesignName = "ImageCellAlternameOperationStatus";
            this.ImageCellAlternameOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternameOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellAlternameOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;
           

            // 
            // lblConsigneeHead
            // 
            this.lblConsigneeHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeHead.Location = new System.Drawing.Point(3, 337);
            this.lblConsigneeHead.Name = "lblConsigneeHead";
            this.lblConsigneeHead.Size = new System.Drawing.Size(61, 14);
            this.lblConsigneeHead.Text = "Consignee:";
            // 
            // lblConsignee
            // 
            this.lblConsignee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignee.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignee.Location = new System.Drawing.Point(170, 337);
            this.lblConsignee.Name = "lblConsignee";
            this.lblConsignee.Size = new System.Drawing.Size(21, 14);
            this.lblConsignee.Text = "Abc";

            // 
            // lblConsignmentItemsHeading
            // 
            this.lblConsignmentItemsHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItemsHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignmentItemsHeading.Location = new System.Drawing.Point(3, 369);
            this.lblConsignmentItemsHeading.Name = "lblConsignmentItemsHeading";
            this.lblConsignmentItemsHeading.Size = new System.Drawing.Size(81, 14);
            this.lblConsignmentItemsHeading.Text = "Consignments:";
           
            // 
            // lblConsignmentItems
            // 
            this.lblConsignmentItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItems.Location = new System.Drawing.Point(170, 368);
            this.lblConsignmentItems.Name = "lblConsignmentItems";
            this.lblConsignmentItems.Size = new System.Drawing.Size(20, 15);
            this.lblConsignmentItems.Text = "Abc";
           

            // 
            // lblConsigneeAdrHeading
            // 
            this.lblConsigneeAdrHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeAdrHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeAdrHeading.Location = new System.Drawing.Point(3, 398);
            this.lblConsigneeAdrHeading.Name = "lblConsigneeAdrHeading";
            this.lblConsigneeAdrHeading.Size = new System.Drawing.Size(82, 14);
            this.lblConsigneeAdrHeading.Text = "Consignee adr:";
           
            
            // 
            // lblConsigneeAdr
            // 
            this.lblConsigneeAdr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeAdr.Location = new System.Drawing.Point(170, 397);
            this.lblConsigneeAdr.Name = "lblConsigneeAdr";
            this.lblConsigneeAdr.Size = new System.Drawing.Size(20, 15);
            this.lblConsigneeAdr.Text = "Abc";
           
            
            
            // 
            // tabControlDeliveries
            // 
            this.tabControlDeliveries.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlDeliveries.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlDeliveries.Location = new System.Drawing.Point(0, 40);
            this.tabControlDeliveries.Name = "tabControlDeliveries";
            this.tabControlDeliveries.SelectedIndex = 0;
            this.tabControlDeliveries.Size = new System.Drawing.Size(480, 272);
            this.tabControlDeliveries.TabIndex = 43;
            this.tabControlDeliveries.TabPages.Add(this.tabNotStarted);
            this.tabControlDeliveries.TabPages.Add(this.tabCompleted);
            this.tabControlDeliveries.TabPages.Add(this.tabAll);
            this.tabControlDeliveries.Text = "tabControlDeliveries";
            this.tabControlDeliveries.ToolbarSize = new System.Drawing.Size(480, 51);
            this.tabControlDeliveries.SelectedIndexChanged += TabControlDeliveriesSelectedIndexChanged;
            // 
            // tabNotStarted
            // 
            this.tabNotStarted.Controls.Add(this.listDeliveries);
            this.tabNotStarted.Location = new System.Drawing.Point(0, 0);
            this.tabNotStarted.Name = "tabNotStarted";
            this.tabNotStarted.Size = new System.Drawing.Size(480, 221);
            // 
            // 
            // 
            this.tabNotStarted.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabNotStarted.TabItem.CustomSize = new System.Drawing.Size(190, 50);
            this.tabNotStarted.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabNotStarted.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabNotStarted.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabNotStarted.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabNotStarted.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabNotStarted.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabNotStarted.TabItem.Name = "";
            this.tabNotStarted.TabItem.Text = "Not Complete";
            // 
            // tabCompleted
            // 
            this.tabCompleted.Location = new System.Drawing.Point(0, 0);
            this.tabCompleted.Name = "tabCompleted";
            this.tabCompleted.Size = new System.Drawing.Size(480, 221);
            // 
            // 
            // 
            this.tabCompleted.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabCompleted.TabItem.CustomSize = new System.Drawing.Size(150, 50);
            this.tabCompleted.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabCompleted.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabCompleted.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabCompleted.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabCompleted.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabCompleted.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabCompleted.TabItem.Name = "";
            this.tabCompleted.TabItem.Text = "Complete";
            // 
            // tabAll
            // 
            this.tabAll.Location = new System.Drawing.Point(0, 0);
            this.tabAll.Name = "tabAll";
            this.tabAll.Size = new System.Drawing.Size(480, 221);
            // 
            // 
            // 
            this.tabAll.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabAll.TabItem.CustomSize = new System.Drawing.Size(100, 50);
            this.tabAll.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabAll.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabAll.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabAll.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabAll.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabAll.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabAll.TabItem.Name = "";
            this.tabAll.TabItem.Text = "All";

            // 
            // _messageControlBox
            // 
            this._messageControlBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._messageControlBox.Location = new System.Drawing.Point(0, 320);
            this._messageControlBox.Name = "_messageControlBox";
            this._messageControlBox.Size = new System.Drawing.Size(480, 100);
            this._messageControlBox.TabIndex = 24;


            // 
            // FormLoadList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormLoadList";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdrHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItemsHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).EndInit();
            this.tabControlDeliveries.ResumeLayout(false);
            this.tabNotStarted.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MessageControl _messageControlBox;
        private Resco.Controls.CommonControls.TabControl tabControlDeliveries;
        private Resco.Controls.CommonControls.TabPage tabNotStarted;
        private Resco.Controls.CommonControls.TabPage tabCompleted;
        private Resco.Controls.CommonControls.TabPage tabAll;

        private System.Windows.Forms.ImageList imageListOperationListIcons;
       
        private Resco.Controls.OutlookControls.ImageButton btnDetails;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnScan;

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listDeliveries;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignee;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItems;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeHead;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItemsHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeAdr;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeAdrHeading;
        
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnRefresh;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;
        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;

        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;

       




    }
}
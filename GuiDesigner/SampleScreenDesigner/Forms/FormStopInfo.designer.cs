﻿using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormStopInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStopInfo));
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
          
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelOperationTime = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelOperationType = new Resco.Controls.CommonControls.TransparentLabel();
            this.webBrowserStopDetails = new System.Windows.Forms.WebBrowser();
           
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelOperationTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOperationType)).BeginInit();
           
            this.SuspendLayout();
            // 
            // labelModuleName
            // 

            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 3);
            this.labelModuleName.Name = "lblModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(477, 33);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.labelOperationTime);
            this.touchPanel.Controls.Add(this.labelOperationType);
            this.touchPanel.Controls.Add(this.webBrowserStopDetails);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 588);
            // 
            // labelOperationTime
            // 
            this.labelOperationTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelOperationTime.AutoSize = false;
            this.labelOperationTime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelOperationTime.Location = new System.Drawing.Point(189, 59);
            this.labelOperationTime.Name = "labelOperationTime";
            this.labelOperationTime.Size = new System.Drawing.Size(221, 27);
            this.labelOperationTime.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // labelOperationType
            // 
            this.labelOperationType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelOperationType.AutoSize = false;
            this.labelOperationType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelOperationType.Location = new System.Drawing.Point(11, 59);
            this.labelOperationType.Name = "labelOperationType";
            this.labelOperationType.Size = new System.Drawing.Size(163, 27);
            this.labelOperationType.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // webBrowserStopDetails
            // 
            this.webBrowserStopDetails.Location = new System.Drawing.Point(11, 92);
            this.webBrowserStopDetails.Name = "webBrowserStopDetails";
            this.webBrowserStopDetails.Size = new System.Drawing.Size(459, 348);
            // 
            // FormStopInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormStopInfo";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelOperationTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOperationType)).EndInit();
            
            this.ResumeLayout(false);

        }


        #endregion
      
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private System.Windows.Forms.WebBrowser webBrowserStopDetails;
        private Resco.Controls.CommonControls.TransparentLabel labelOperationTime;
        private Resco.Controls.CommonControls.TransparentLabel labelOperationType;


    }
}
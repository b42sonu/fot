﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SampleScreenDesigner.Forms
{
    
    public partial class FormRegisterTimeConsumption : Form
    {
        private List<ConsumptionTime> _consumptionTimes=new List<ConsumptionTime>();

        public FormRegisterTimeConsumption()
        {
            InitializeComponent();
            
        }

        void Bind()
        {
            for (int i = 0; i < 8; i++)
            {
                _consumptionTimes.Add(new ConsumptionTime("A0" + i + 1, i + 15 + " min additional time"));
            }
            //advancedListTime.BeginUpdate();
            advancedListTime.DataSource = _consumptionTimes;
            //advancedListTime.EndUpdate();
        }
        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        public class ConsumptionTime
        {
            public string TimeCode { get; set; }
            public string Time { get; set; }

            public ConsumptionTime(string timeCode, string time)
            {
                TimeCode = timeCode;
                Time = time;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bind();
        }

        private void AdvancedListOperationListRowSelect(object sender, Resco.Controls.AdvancedList.RowEventArgs e)
        {

        }


    }

  
}
﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormEventVerification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConfirmMessage = new System.Windows.Forms.Label();
            this.btnConfirm = new Resco.Controls.OutlookControls.ImageButton();
            this.lblConfirmItem = new System.Windows.Forms.Label();
            this.lblMeasure = new System.Windows.Forms.Label();
            this.lblCause = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblComments = new System.Windows.Forms.Label();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonSignature = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSignature)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblConfirmMessage);
            this.touchPanel.Controls.Add(this.btnConfirm);
            this.touchPanel.Controls.Add(this.lblConfirmItem);
            this.touchPanel.Controls.Add(this.lblMeasure);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblType);
            this.touchPanel.Controls.Add(this.lblComments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.buttonBack);
            this.touchPanel.Controls.Add(this.buttonSignature);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblConfirmMessage
            // 
            this.lblConfirmMessage.Location = new System.Drawing.Point(13, 370);
            this.lblConfirmMessage.Name = "lblConfirmMessage";
            this.lblConfirmMessage.Size = new System.Drawing.Size(452, 65);
            this.lblConfirmMessage.Text = "Please confirm with or without signature";
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnConfirm.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnConfirm.ForeColor = System.Drawing.Color.White;
            this.btnConfirm.Location = new System.Drawing.Point(160, 472);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(169, 60);
            this.btnConfirm.TabIndex = 63;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.Click += new System.EventHandler(this.ButtonConfirmClick);
            // 
            // lblConfirmItem
            // 
            this.lblConfirmItem.Location = new System.Drawing.Point(13, 62);
            this.lblConfirmItem.Name = "lblConfirmItem";
            this.lblConfirmItem.Size = new System.Drawing.Size(436, 65);
            this.lblConfirmItem.Text = "Confirm damagae for <item type> <item number>";
            // 
            // lblMeasure
            // 
            this.lblMeasure.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblMeasure.Location = new System.Drawing.Point(4, 243);
            this.lblMeasure.Name = "lblMeasure";
            this.lblMeasure.Size = new System.Drawing.Size(474, 27);
            this.lblMeasure.Text = "Measure : 22 - Destruert";
            // 
            // lblCause
            // 
            this.lblCause.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblCause.Location = new System.Drawing.Point(3, 192);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(474, 30);
            this.lblCause.Text = "Cause : 64 - Fell out of car";
            // 
            // lblType
            // 
            this.lblType.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblType.Location = new System.Drawing.Point(3, 145);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(474, 26);
            this.lblType.Text = "Damage type : A Package Damage";
            // 
            // lblComments
            // 
            this.lblComments.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblComments.Location = new System.Drawing.Point(4, 289);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(476, 81);
            this.lblComments.Text = "Comment: Package type was totally damaged.";
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(132, 17);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(197, 27);
            this.labelModuleName.Text = "Register Damage";
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(0, 472);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(162, 60);
            this.buttonBack.TabIndex = 29;
            this.buttonBack.Text = "Back";
            this.buttonBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // buttonSignature
            // 
            this.buttonSignature.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonSignature.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonSignature.ForeColor = System.Drawing.Color.White;
            this.buttonSignature.Location = new System.Drawing.Point(320, 472);
            this.buttonSignature.Name = "buttonSignature";
            this.buttonSignature.Size = new System.Drawing.Size(160, 60);
            this.buttonSignature.TabIndex = 28;
            this.buttonSignature.Text = "Signature";
            this.buttonSignature.Click += new System.EventHandler(this.ButtonSignatureClick);
            // 
            // FormEventVerification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormEventVerification";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnConfirm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSignature)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
        private Resco.Controls.OutlookControls.ImageButton buttonSignature;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Label lblConfirmItem;
        private Label lblMeasure;
        private Label lblCause;
        private Label lblType;
        private Label lblComments;
        private Label lblConfirmMessage;
        private Resco.Controls.OutlookControls.ImageButton btnConfirm;
    }
}
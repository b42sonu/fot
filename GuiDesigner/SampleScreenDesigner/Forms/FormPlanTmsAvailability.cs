﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace Com.Bring.PMP.PreComFW.Setting.Views
{
    public partial class FormPlanTmsAvailability : Form
    {
        public FormPlanTmsAvailability()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            TabMultipleButtons tabMultipleButtons=new TabMultipleButtons();
            tabMultipleButtons.ListButtons.Add(new TabButton("Back",null,TabButtonType.Cancel));
            tabMultipleButtons.ListButtons.Add(new TabButton("Ok",null,TabButtonType.Confirm));
            tabMultipleButtons.GenerateButtons();
            touchPanel.Controls.Add(tabMultipleButtons);
        }

        
        private void Button30MinClick(object sender, EventArgs e)
        {
            SetDuration(30);
        }

        private void Button1HourClick(object sender, EventArgs e)
        {
            SetDuration(60);
        }

        private void Button2HourClick(object sender, EventArgs e)
        {
            SetDuration(120);
        }

        private void Button4HourClick(object sender, EventArgs e)
        {
            SetDuration(240);
        }

        private void Button8HourClick(object sender, EventArgs e)
        {
            SetDuration(480);
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {

        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {

        }

        private void SetDuration(int minutes)
        {
            DateTime startTime = timePickerStart.Value;

            DateTime endTime = startTime.AddMinutes(minutes);

            timePickerStop.Value = endTime;
        }

        private void touchPanel_GotFocus(object sender, EventArgs e)
        {

        }
    }
}
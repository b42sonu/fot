﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    public partial class FormLoadingOperations : Form
    {
        public FormLoadingOperations()
        {
            InitializeComponent();
            BindOperations();
        }

        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            switch (e.DataRow.CurrentTemplateIndex)
            {
                    
                case 1: // The master row has to be collapsed
                                   
                    break;
                case 2: // The master row has to be expanded  
                    UnSelectOtherRows();
                    e.DataRow.Selected = true;
                    ShowOperationDetail(e);
                    break;
            }
        }


        private void ShowOperationDetail(RowEventArgs e)
        {
            lblTripId.Text = Convert.ToString(e.DataRow["TripId"]);
            lblStopId.Text = Convert.ToString(e.DataRow["StopId"]);
        }


        private void UnSelectOtherRows()
        {
            for (var i = 0; i <= listOperations.DataRows.Count - 1;i++ )
            {

                var row = listOperations.DataRows[i];
                row.Selected = false;
            }

        }

        private void BindOperations()
        {

            var firstObject = new LoadingOperation()
                                  {
                                      OperationId = "OperationID1",
                                      //Address = string.Format("{0:t}", plannedOperation.EarliestStartTime) + "-" + string.Format("{0:t}", plannedOperation.LatestStartTime) + Environment.NewLine + plannedConsignmentsType.OrderNumber;"#1462 Sector 39/b Chandigarh 160036",
                                      OperationDetail = "00:00" + " - " + "00:00" + " LocationName1" + Environment.NewLine + "#1462 Sector 39/b Chandigarh 160036",
                                      StopId="StopId1",
                                      TripId="TripId1"  ,
                                      Status = 1
                                      
                                  };
            var SecondObject = new LoadingOperation()
            {
                OperationId = "OperationID2",
                //Address = string.Format("{0:t}", plannedOperation.EarliestStartTime) + "-" + string.Format("{0:t}", plannedOperation.LatestStartTime) + Environment.NewLine + plannedConsignmentsType.OrderNumber;"#1462 Sector 39/b Chandigarh 160036",
                OperationDetail = "00:00" + " - " + "00:00" + " LocationName2" + Environment.NewLine + "#1462 Sector 39/b Chandigarh 160036",
                StopId = "StopId2",
                TripId = "TripId2",
                Status = 1

            };



            var listOfObjects = new List<LoadingOperation>();
            listOfObjects.Add(firstObject);
            listOfObjects.Add(SecondObject);

           


            listOperations.BeginUpdate();
            listOperations.DataSource = listOfObjects;
            listOperations.EndUpdate();
        }

    }

    public class LoadingOperation
    {
        public int OperationType { get; set; }
        public string OperationId { get; set; }
        public string StopId { get; set; }
        public string TripId { get; set; }
        public string LocationName { get; set; }
        public string OperationDetail { get; set; }
        public int Status { get; set; }
    }




}
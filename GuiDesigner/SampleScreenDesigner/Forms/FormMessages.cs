﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormMessages : Form
    {
        private readonly TabMultipleButtons _tabButtons;
        private const string BackButton = "backButton";
        private const string NewMessageButton = "newMessageButton";
        private const string ReadButton = "readButton";
        private const string DeleteButton = "deleteButton";
        private const string ReplyButton = "replyButton";
        private const string DeleteAllButton = "deleteAllButton";
        private const string RestoreButton = "restoreButton";
        private const string SendButton = "sendButton";
        public FormMessages()
        {
            InitializeComponent();
            MockOperationList();
            _tabButtons = new TabMultipleButtons();
            GetStopsInfo();
            tabControlOperationList.Visible = true;
            pnlNewMessage.Visible = true;
            advancedListOperationList.BeginUpdate();
            advancedListOperationList.DataSource = _listClientOperationStops;
            advancedListOperationList.EndUpdate();
            AddButtonToPanel();

        }

        void AddButtonToPanel()
        {
            _tabButtons.ListButtons.Add(new TabButton("Back", ButtonBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton("Delete", ButtonDeleteClick, DeleteButton));
            _tabButtons.ListButtons.Add(new TabButton("New Message", NewMessageClick, NewMessageButton));
            _tabButtons.ListButtons.Add(new TabButton("Read", ButtonReadClick, ReadButton));
            _tabButtons.ListButtons.Add(new TabButton("Reply", ButtonReplyClick, ReplyButton));
            _tabButtons.ListButtons.Add(new TabButton("Send", ButtonSendClick, SendButton));
            _tabButtons.ListButtons.Add(new TabButton("Delete All", ButtonDeleteAllClick, DeleteAllButton));
            _tabButtons.ListButtons.Add(new TabButton("Restore", ButtonRestoreClick, RestoreButton));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
          
        }

        private void ButtonReadClick(object sender, EventArgs e)
        {
           
        }

        private void NewMessageClick(object sender, EventArgs e)
        {
            pnlNewMessage.Visible = true;
            tabControlOperationList.Visible = false;
        }

        private void ButtonSendClick(object sender, EventArgs e)
        {
           
        }



        private void ButtonDeleteClick(object sender, EventArgs e)
        {
           
        }

        private void ButtonDeleteAllClick(object sender, EventArgs e)
        {
           
        }

        private void ButtonReplyClick(object sender, EventArgs e)
        {
            
        }

        private void ButtonRestoreClick(object sender, EventArgs e)
        {
          
        }

        private void BtnDetailsClick(object sender, EventArgs e)
        {
            pnlNewMessage.Visible = true;
            tabControlOperationList.Visible = false;
        }

        private void BtnUnplannedPickUpClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnUnplannedPickUp Clicked");
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnBack Clicked");
        }

      
        private void BtnScanClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnScan Clicked");
        }

        private void AdvancedListOperationListRowSelect(object sender, Resco.Controls.AdvancedList.RowEventArgs e)
        {

        }

        private void TabControlOperationSelectedIndexChanged(object sender, EventArgs e)
        {
            var tabControl = (Resco.Controls.CommonControls.TabControl)sender;
            switch (tabControl.SelectedIndex)
            {
                case 0:
                    tabInbox.Controls.Add(advancedListOperationList);
                    advancedListOperationList.BeginUpdate();
                    advancedListOperationList.DataSource = _listClientOperationStops;
                    advancedListOperationList.EndUpdate();
                    break;
                case 1:
                    tabRead.Controls.Add(advancedListOperationList);
                    advancedListOperationList.BeginUpdate();
                    advancedListOperationList.DataSource = _listClientOperationStops;
                    advancedListOperationList.EndUpdate();
                    break;
                case 2:
                    tabDeleted.Controls.Add(advancedListOperationList);
                    advancedListOperationList.BeginUpdate();
                    advancedListOperationList.DataSource = _listClientOperationStops;
                    advancedListOperationList.EndUpdate();
                    break;
            }
        }

        private void MockOperationList()
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filePath = path + "\\Resources\\SampleOperationList.xml";
            if (File.Exists(filePath))
            {
                XmlReader xmlReader = XmlReader.Create(filePath);
                var xmlSerializer = new XmlSerializer(typeof(OperationList));
                OperationList = (OperationList)xmlSerializer.Deserialize(xmlReader);
            }
        }

        private void GetStopsInfo()
        {
            var stubOperationListStop = new Message
                                            {
                                                MessageId="1",
                                                Messagetext = "sunil",
                                                Time = "10:20"
                                            };

            _listClientOperationStops.Add(stubOperationListStop);

            var stubOperationListStop2 = new Message
                                             {
                                                 MessageId = "2",
                                                 Messagetext = "MAhesh",
                                                 Time = "11:20"
                                             };

            _listClientOperationStops.Add(stubOperationListStop2);
        }

        private OperationList OperationList { get; set; }
        private readonly IList<Message> _listClientOperationStops = new List<Message>();

        private void BtnAttemptedDeliveryClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnAttemptedDelevery Clicked");

        }

        private void BtnConfirmClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnConfirm Clicked");
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnCancel Clicked");
        }

        private void BtnAttemptClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnAttempt Clicked");
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            MessageBox.Show("BtnOk Clicked");
        }

        private void btnNewMessage_Click(object sender, EventArgs e)
        {
            this.pnlNewMessage.Visible = true;
            this.tabControlOperationList.Visible = false;
        }
    }

    public class Message
    {
        public string MessageId { get; set; }
        public string Messagetext { get; set; }
        public string Time { get; set; }
    }
}
﻿namespace Com.Bring.PMP.PreComFW.Shared.Forms
{
    partial class FormModuleMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.panelButtonContainer = new Resco.Controls.CommonControls.TouchPanel();
            this.labelPageNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelInstructions = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            this.panelButtonContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelPageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.panelButtonContainer);
            this.touchPanel.Controls.Add(this.labelPageNumber);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.labelInstructions);
            this.touchPanel.Controls.Add(this.lblOrgUnit);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // panelButtonContainer
            // 
            this.panelButtonContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.panelButtonContainer.Location = new System.Drawing.Point(22, 123);
            this.panelButtonContainer.Name = "panelButtonContainer";
            this.panelButtonContainer.Size = new System.Drawing.Size(438, 345);
            // 
            // labelPageNumber
            // 
            this.labelPageNumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelPageNumber.AutoSize = false;
            this.labelPageNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelPageNumber.Location = new System.Drawing.Point(370, 465);
            this.labelPageNumber.Name = "labelPageNumber";
            this.labelPageNumber.Size = new System.Drawing.Size(50, 34);
            this.labelPageNumber.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleRight;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(40, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(369, 34);
            this.labelModuleName.Text = "Heading";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // labelInstructions
            // 
            this.labelInstructions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelInstructions.AutoSize = false;
            this.labelInstructions.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelInstructions.Location = new System.Drawing.Point(22, 82);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(369, 34);
            this.labelInstructions.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomLeft;
            // 
            // lblOrgUnit
            // 
            this.lblOrgUnit.AutoSize = false;
            this.lblOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgUnit.Location = new System.Drawing.Point(40, 42);
            this.lblOrgUnit.Name = "lblOrgUnit";
            this.lblOrgUnit.Size = new System.Drawing.Size(369, 33);
            this.lblOrgUnit.Text = "OrgUnit";
            this.lblOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormModuleMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormModuleMenu";
            this.panelButtonContainer.ResumeLayout(false);
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelPageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        protected Resco.Controls.CommonControls.TransparentLabel labelInstructions;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgUnit;
        protected Resco.Controls.CommonControls.TransparentLabel labelPageNumber;
        private Resco.Controls.CommonControls.TouchPanel panelButtonContainer;
    }
}
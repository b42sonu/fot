﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormManualRbt : Form
    {
        private TabMultipleButtons _tabButtons;
       
        public FormManualRbt()
        {
            InitializeComponent();
            SetTextToGui();
        }
        private void SetTextToGui()
        {
            _tabButtons = new TabMultipleButtons();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton("", null, "Blank"));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonConfirmClick));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            OperationType operationType = OperationType.UnloadPickUpTruck;
            switch (operationType)
            {
                case OperationType.LoadLineHaulTruck:
                    lblModuleName.Text = GlobalTexts.LoadingLineHaulTruck;
                    break;
                case OperationType.LoadingDistributionTruck:
                    lblModuleName.Text = GlobalTexts.LoadDistributionTruck;
                    break;
                case OperationType.UnloadPickUpTruck:
                    lblModuleName.Text = GlobalTexts.LoadDistributionTruck;
                    TabButton tabButton = _tabButtons.GetButton("Blank");
                    if (tabButton != null)
                    {
                        tabButton.Click += ButtonSkip;
                        tabButton.Text = GlobalTexts.Skip;
                    }
                    break;
            }

            ResetControl(operationType);
        }


       


        private void ResetControl(OperationType operationType)
        {
            switch (operationType)
            {
                case OperationType.UnloadPickUpTruck:
                    lblLoadCarrierId.Visible = false;
                    lblPowerUnit.Visible = false;
                    lblRoute.Visible = false;

                    txtBoxLoadCarrierId.Visible = false;
                    txtBoxPowerUnit.Visible = false;
                    txtBoxRoute.Visible = false;

                    lblTrip.Location = lblPowerUnit.Location;
                    lblStop.Location = lblLoadCarrierId.Location;

                    txtBoxTrip.Location = txtBoxPowerUnit.Location;
                    txtBoxStop.Location = txtBoxLoadCarrierId.Location;
                    break;
                default:
                    lblLoadCarrierId.Visible = true;
                    lblPowerUnit.Visible = true;
                    lblRoute.Visible = true;
                    lblStop.Visible = true;
                    lblTrip.Visible = true;

                    txtBoxLoadCarrierId.Visible = true;
                    txtBoxPowerUnit.Visible = true;
                    txtBoxRoute.Visible = true;
                    txtBoxStop.Visible = true;
                    txtBoxTrip.Visible = true;

                    lblTrip.Location = new System.Drawing.Point(15, 275);
                    lblStop.Location = new System.Drawing.Point(15, 325);

                    txtBoxTrip.Location = new System.Drawing.Point(176, 275);
                    txtBoxStop.Location = new System.Drawing.Point(176, 325);
                    break;
            }
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
            
        }


        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            
        }
        private void ButtonSkip(object sender, EventArgs e)
        {
           
        }
        private void TxtBoxPowerUnitKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtBoxLoadCarrierId.Focus();
        }

        private void TxtBoxLoadCarrierIdKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtBoxRoute.Focus();
        }


        private void TxtBoxRouteKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtBoxTrip.Focus();
        }


        private void TxtBoxTripKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtBoxStop.Focus();
        }


        private void TxtBoxStopKeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtBoxPowerUnit.Focus();
        }
    }
}
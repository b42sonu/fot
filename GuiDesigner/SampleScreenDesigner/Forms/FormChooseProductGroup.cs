﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SampleScreenDesigner.Forms
{
    public partial class FormChooseProductGroup : Form
    {
        public FormChooseProductGroup()
        {
            InitializeComponent();
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        public IList<ConsignmentDetail> ConsignmentDetails
        {
            get
            {
                List<ConsignmentDetail> consignmentDetailList = new List<ConsignmentDetail>();

                consignmentDetailList.Add(new ConsignmentDetail { IsChecked = "false", ConsignmentNumber = "12345" });
                consignmentDetailList.Add(new ConsignmentDetail { IsChecked = "true", ConsignmentNumber = "546546456" });
                consignmentDetailList.Add(new ConsignmentDetail { IsChecked = "false", ConsignmentNumber = "4563656" });

                return consignmentDetailList;
            }
        }

        private void touchPanel_GotFocus(object sender, EventArgs e)
        {

        }

        private void labelConsignmentItem_ParentChanged(object sender, EventArgs e)
        {

        }


    }

}
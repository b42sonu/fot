﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
   

    public partial class FormSelectAction : Form
    {
       
       
        TabMultipleButtons _tabButtons = new TabMultipleButtons();
        private const string ButtonOk = "buttonOk";

        #region Methods and Functions

        public FormSelectAction()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            lblReason.Text = GlobalTexts.Reason;
            lblAction.Text = GlobalTexts.Action;
            lblHeading.Text = GlobalTexts.SearchTextAction + Environment.NewLine + GlobalTexts.PressOkEnter;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonOkClick, ButtonOk));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitOnScreenKeyBoardProperties()
        {
          
        }


       

       
        /// <summary>
        /// 
        /// </summary>
        private void EnableDisableButtons()
        {
            _tabButtons.Enable(ButtonOk, (listActions.Items.Count != 0) && listActions.SelectedIndex >= 0);
           // buttonOk.Enabled = (listActions.Items.Count != 0) && listActions.SelectedIndex >= 0 ;
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFinishedTyping"></param>
        /// <param name="textentered"></param>
        private void TextActionTextChanged(bool userFinishedTyping, string textentered)
        {
          
        }
        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtActionKeyUp(object sender, KeyEventArgs e)
        {
          
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListReasonsSelectedIndexChanged(object sender, EventArgs e)
        {
          
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
           
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
           
        }

        #endregion
    }

    
}
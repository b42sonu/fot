﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Setting.Views
{
    partial class FormTmiSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.smartGridTmiSettings = new Resco.Controls.SmartGrid.SmartGrid();
            this.columnTmiValueBool = new Resco.Controls.SmartGrid.Column();
            this.columnTmiValueString = new Resco.Controls.SmartGrid.Column();
            this.columnTmiId = new Resco.Controls.SmartGrid.Column();
            this.imageButton1 = new Resco.Controls.OutlookControls.ImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // smartGridTmiSettings
            // 
            this.smartGridTmiSettings.BorderSize = 2;
            this.smartGridTmiSettings.ColumnHeaderHeight = 40;
            this.smartGridTmiSettings.ColumnResizeSensitivity = 4;
            this.smartGridTmiSettings.Columns.Add(this.columnTmiValueBool);
            this.smartGridTmiSettings.Columns.Add(this.columnTmiValueString);
            this.smartGridTmiSettings.Columns.Add(this.columnTmiId);
            this.smartGridTmiSettings.Location = new System.Drawing.Point(0, 0);
            this.smartGridTmiSettings.Name = "smartGridTmiSettings";
            this.smartGridTmiSettings.RowHeaderWidth = 24;
            this.smartGridTmiSettings.RowHeight = 32;
            this.smartGridTmiSettings.ScrollHeight = 24;
            this.smartGridTmiSettings.ScrollWidth = 24;
            this.smartGridTmiSettings.Size = new System.Drawing.Size(480, 461);
            this.smartGridTmiSettings.TabIndex = 216;
            this.smartGridTmiSettings.Text = "smartGridTmiSettings";
            this.smartGridTmiSettings.TouchScrolling = true;
            // 
            // columnTmiValueBool
            // 
            this.columnTmiValueBool.CellEdit = Resco.Controls.SmartGrid.CellEditType.CheckBox;
            this.columnTmiValueBool.DataMember = "Value";
            this.columnTmiValueBool.EditMode = Resco.Controls.SmartGrid.EditMode.OnEnter;
            this.columnTmiValueBool.GridLine = false;
            this.columnTmiValueBool.MinimumWidth = 10;
            this.columnTmiValueBool.Name = "columnTmiValueBool";
            this.columnTmiValueBool.Width = 40;
            // 
            // columnTmiValueString
            // 
            this.columnTmiValueString.CellEdit = Resco.Controls.SmartGrid.CellEditType.TextBox;
            this.columnTmiValueString.DataMember = "ValueAsString";
            this.columnTmiValueString.EditMode = Resco.Controls.SmartGrid.EditMode.OnEnter;
            this.columnTmiValueString.HeaderText = "Value";
            this.columnTmiValueString.MinimumWidth = 10;
            this.columnTmiValueString.Name = "columnTmiValueString";
            this.columnTmiValueString.Width = 150;
            // 
            // columnTmiId
            // 
            this.columnTmiId.DataMember = "TmiId";
            this.columnTmiId.HeaderText = "Id";
            this.columnTmiId.MinimumWidth = 10;
            this.columnTmiId.Name = "columnTmiId";
            this.columnTmiId.Width = 500;
            // 
            // imageButton1
            // 
            this.imageButton1.Location = new System.Drawing.Point(300, 467);
            this.imageButton1.Name = "imageButton1";
            this.imageButton1.Size = new System.Drawing.Size(177, 42);
            this.imageButton1.TabIndex = 217;
            this.imageButton1.Text = "imageButton1";
            // 
            // FormTmiSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.imageButton1);
            this.Controls.Add(this.smartGridTmiSettings);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormTmiSettings";
            ((System.ComponentModel.ISupportInitialize)(this.imageButton1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.SmartGrid.SmartGrid smartGridTmiSettings;
        private Resco.Controls.SmartGrid.Column columnTmiValueBool;
        private Resco.Controls.SmartGrid.Column columnTmiId;
        private Resco.Controls.SmartGrid.Column columnTmiValueString;
        private Resco.Controls.OutlookControls.ImageButton imageButton1;



    }
}
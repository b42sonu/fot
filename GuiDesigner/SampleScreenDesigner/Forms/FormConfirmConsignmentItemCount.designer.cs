﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount
{
    partial class FormConfirmConsignmentItemCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._messageControlControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.editConsignmentItemCount = new PreCom.Controls.PreComInput2();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.transparentLabel3 = new Resco.Controls.CommonControls.TransparentLabel();
            this.transparentLabel2 = new Resco.Controls.CommonControls.TransparentLabel();
            this.transparentLabel1 = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // _messageControlControl
            // 
            this._messageControlControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._messageControlControl.Location = new System.Drawing.Point(8, 198);
            this._messageControlControl.MessageText = "Number of consignment items can not be 0";
            this._messageControlControl.Name = "_messageControlControl";
            this._messageControlControl.Size = new System.Drawing.Size(464, 120);
            this._messageControlControl.TabIndex = 3;
            // 
            // editConsignmentItemCount
            // 
            this.editConsignmentItemCount.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.editConsignmentItemCount.Location = new System.Drawing.Point(375, 120);
            this.editConsignmentItemCount.Name = "editConsignmentItemCount";
            this.editConsignmentItemCount.Size = new System.Drawing.Size(81, 41);
            this.editConsignmentItemCount.TabIndex = 43;
            this.editConsignmentItemCount.TextTranslation = false;
            this.editConsignmentItemCount.MaxLength = 5;
            this.editConsignmentItemCount.KeyUp += new System.Windows.Forms.KeyEventHandler(this.EditConsignmentItemCountKeyUp);
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.transparentLabel3);
            this.touchPanel.Controls.Add(this.transparentLabel2);
            this.touchPanel.Controls.Add(this.transparentLabel1);
            this.touchPanel.Controls.Add(this.editConsignmentItemCount);
            this.touchPanel.Controls.Add(this._messageControlControl);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // transparentLabel3
            // 
            this.transparentLabel3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.transparentLabel3.Location = new System.Drawing.Point(18, 128);
            this.transparentLabel3.Name = "transparentLabel3";
            this.transparentLabel3.Size = new System.Drawing.Size(312, 24);


            // 
            // transparentLabel2
            // 
            this.transparentLabel2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.transparentLabel2.Location = new System.Drawing.Point(18, 80);
            this.transparentLabel2.Name = "transparentLabel2";
            this.transparentLabel2.Size = new System.Drawing.Size(255, 24);

            // 
            // transparentLabel1
            // 
            this.transparentLabel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.transparentLabel1.Location = new System.Drawing.Point(18, 50);
            this.transparentLabel1.Name = "transparentLabel1";
            this.transparentLabel1.Size = new System.Drawing.Size(322, 24);

            // 
            // lblModuleName
            // 
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(477, 33);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormConfirmConsignmentItemCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormConfirmConsignmentItemCount";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MessageControl _messageControlControl;
        private PreCom.Controls.PreComInput2 editConsignmentItemCount;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel transparentLabel1;
        private Resco.Controls.CommonControls.TransparentLabel transparentLabel2;
        private Resco.Controls.CommonControls.TransparentLabel transparentLabel3;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
    }
}
﻿using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Goods.Flows.RemainingGoodsAtHub
{
    partial class FormRemainingGoodsAtHubScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelItemCounter = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtScannedNumber = new System.Windows.Forms.TextBox();
            this.lblCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPlacement = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblComments = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelItemCounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPlacement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelTask);
            this.touchPanel.Controls.Add(this.labelItemCounter);
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.lblMeasure);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblPlacement);
            this.touchPanel.Controls.Add(this.lblComments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.buttonCancel);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelTask
            // 
            this.labelTask.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelTask.Location = new System.Drawing.Point(30, 228);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(0, 0);
            // 
            // labelItemCounter
            // 
            this.labelItemCounter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelItemCounter.Location = new System.Drawing.Point(420, 228);
            this.labelItemCounter.Name = "labelItemCounter";
            this.labelItemCounter.Size = new System.Drawing.Size(0, 0);
            this.labelItemCounter.Visible = false;
            // 
            // messageControlValidateScan
            // 
            this.messageControlValidateScan.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControlValidateScan.Location = new System.Drawing.Point(30, 308);
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(424, 120);
            this.messageControlValidateScan.TabIndex = 56;
           // this.messageControlValidateScan.Click += new System.EventHandler(this.messageControlValidateScan_Click);
            // 
            // lblMeasure
            // 
            this.lblMeasure.AutoSize = false;
            this.lblMeasure.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblMeasure.Location = new System.Drawing.Point(6, 102);
            this.lblMeasure.Name = "lblMeasure";
            this.lblMeasure.Size = new System.Drawing.Size(471, 27);
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Location = new System.Drawing.Point(30, 261);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(424, 41);
            this.txtScannedNumber.TabIndex = 37;
            //this.txtScannedNumber.TextChanged += new System.EventHandler(this.txtScannedNumber_TextChanged);
            // 
            // lblCause
            // 
            this.lblCause.AutoSize = false;
            this.lblCause.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblCause.Location = new System.Drawing.Point(6, 69);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(468, 27);
            // 
            // lblPlacement
            // 
            this.lblPlacement.AutoSize = false;
            this.lblPlacement.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblPlacement.Location = new System.Drawing.Point(6, 36);
            this.lblPlacement.Name = "lblPlacement";
            this.lblPlacement.Size = new System.Drawing.Size(468, 27);
            // 
            // lblComments
            // 
            this.lblComments.AutoSize = false;
            this.lblComments.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblComments.Location = new System.Drawing.Point(6, 135);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(468, 87);
            // 
            // labelModuleName
            // 
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(480, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 483);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(238, 50);
            this.buttonCancel.TabIndex = 29;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(242, 483);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(238, 50);
            this.buttonOk.TabIndex = 28;
            this.buttonOk.Click += new System.EventHandler(this.ButtonConfirmClick);
            // 
            // FormRemainingGoodsAtHubScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormRemainingGoodsAtHubScan";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelItemCounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPlacement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControlValidateScan;
        private System.Windows.Forms.TextBox txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblMeasure;
        private Resco.Controls.CommonControls.TransparentLabel lblCause;
        private Resco.Controls.CommonControls.TransparentLabel lblPlacement;
        private Resco.Controls.CommonControls.TransparentLabel lblComments;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton buttonCancel;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private Resco.Controls.CommonControls.TransparentLabel labelItemCounter;
    }
}
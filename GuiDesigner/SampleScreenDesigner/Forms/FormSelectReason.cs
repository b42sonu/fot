﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{

    
    public partial class FormSelectReason : Form
    {
      
        TabMultipleButtons _tabButtons = new TabMultipleButtons();
        private const string ButtonOk = "buttonOk";
        #region Methods and Functions

        public FormSelectReason()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            lblReason.Text = GlobalTexts.Reason + ":";
            lblHeading.Text = GlobalTexts.SearchTextReason + Environment.NewLine + GlobalTexts.PressOkEnter;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonOkClick, ButtonOk));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }


        private void TxtReasonKeyUp(object sender, KeyEventArgs e)
        {

        }

        private void ListReasonsSelectedIndexChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// 
        /// </summary>
        private void EnableDisableButtons()
        {
            _tabButtons.Enable(ButtonOk, (listReasons.Items.Count != 0) && listReasons.SelectedIndex >= 0);
            //buttonOk.Enabled = (listReasons.Items.Count != 0) && listReasons.SelectedIndex >= 0 ;
        }

        #endregion


        #region Events

        

  


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            
        }

        #endregion
    }

   
}
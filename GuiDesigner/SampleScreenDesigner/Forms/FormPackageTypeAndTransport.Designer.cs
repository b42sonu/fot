﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.PickUp.Views
{
    partial class FormPackageTypeAndTransport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.btnReject = new Resco.Controls.OutlookControls.ImageButton();
            this.btnAccept = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.gridTransportEquipment = new Resco.Controls.SmartGrid.SmartGrid();
            this.gridPackageType = new Resco.Controls.SmartGrid.SmartGrid();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.columnTransportType = new Resco.Controls.SmartGrid.Column();
            this.columnTransportQuantity = new Resco.Controls.SmartGrid.Column();
            this.columnPackageType = new Resco.Controls.SmartGrid.Column();
            this.columnPackageQuantity = new Resco.Controls.SmartGrid.Column();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnReject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.BackgroundImageLayout = Resco.Controls.CommonControls.ImageLayout.Center;
            this.touchPanel.BackgroundStyle = Resco.Controls.CommonControls.BackgroundStyle.DisplaySize;
            this.touchPanel.Controls.Add(this.btnReject);
            this.touchPanel.Controls.Add(this.btnAccept);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.gridTransportEquipment);
            this.touchPanel.Controls.Add(this.gridPackageType);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // btnReject
            // 
            this.btnReject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnReject.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Regular);
            this.btnReject.ForeColor = System.Drawing.Color.White;
            this.btnReject.Location = new System.Drawing.Point(0, 429);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(238, 50);
            this.btnReject.TabIndex = 31;
            this.btnReject.Text = "Add transport equipment";
            this.btnReject.WordWrap = true;
            this.btnReject.Click += new System.EventHandler(this.OnAddTransportEquipmentToGrid);
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnAccept.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Regular);
            this.btnAccept.ForeColor = System.Drawing.Color.White;
            this.btnAccept.Location = new System.Drawing.Point(242, 429);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(238, 50);
            this.btnAccept.TabIndex = 30;
            this.btnAccept.Text = "Add package type";
            this.btnAccept.WordWrap = true;
            this.btnAccept.Click += new System.EventHandler(this.OnAddPackageTypeToGrid);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(238, 50);
            this.btnBack.TabIndex = 29;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.ButtonCancelClick);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(242, 483);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(238, 50);
            this.btnOk.TabIndex = 28;
            this.btnOk.Text = "Ok/Enter";
            this.btnOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // gridTransportEquipment
            // 
            this.gridTransportEquipment.AllowResize = false;
            this.gridTransportEquipment.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gridTransportEquipment.BorderSize = 2;
            this.gridTransportEquipment.ColumnHeaderHeight = 28;
            this.gridTransportEquipment.ColumnResizeSensitivity = 4;
            this.gridTransportEquipment.Columns.Add(this.columnTransportType);
            this.gridTransportEquipment.Columns.Add(this.columnTransportQuantity);
            this.gridTransportEquipment.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.gridTransportEquipment.HeaderFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.gridTransportEquipment.HeaderVistaStyle = true;
            this.gridTransportEquipment.Location = new System.Drawing.Point(10, 62);
            this.gridTransportEquipment.Name = "gridTransportEquipment";
            this.gridTransportEquipment.RowHeaderWidth = 24;
            this.gridTransportEquipment.RowHeight = 22;
            this.gridTransportEquipment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridTransportEquipment.ScrollHeight = 24;
            this.gridTransportEquipment.ScrollWidth = 24;
            this.gridTransportEquipment.Size = new System.Drawing.Size(460, 96);
            this.gridTransportEquipment.TabIndex = 10;
            this.gridTransportEquipment.Text = "gridPackageTypes";
            this.gridTransportEquipment.SelectionChanged += new System.EventHandler(this.OnSelectionChanged);
            // 
            // gridPackageType
            // 
            this.gridPackageType.AllowResize = false;
            this.gridPackageType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gridPackageType.BorderSize = 2;
            this.gridPackageType.ColumnHeaderHeight = 28;
            this.gridPackageType.ColumnResizeSensitivity = 4;
            this.gridPackageType.Columns.Add(this.columnPackageType);
            this.gridPackageType.Columns.Add(this.columnPackageQuantity);
            this.gridPackageType.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.gridPackageType.HeaderFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.gridPackageType.HeaderVistaStyle = true;
            this.gridPackageType.Location = new System.Drawing.Point(10, 185);
            this.gridPackageType.Name = "gridPackageType";
            this.gridPackageType.RowHeaderWidth = 24;
            this.gridPackageType.RowHeight = 22;
            this.gridPackageType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gridPackageType.ScrollHeight = 24;
            this.gridPackageType.ScrollWidth = 24;
            this.gridPackageType.Size = new System.Drawing.Size(460, 192);
            this.gridPackageType.TabIndex = 7;
            this.gridPackageType.Text = "gridPackageTypes";
            this.gridPackageType.SelectionChanged += new System.EventHandler(this.OnSelectionChanged);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Package type and transport equipment";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // columnTransportType
            // 
            this.columnTransportType.AutoSizeColumnMode = Resco.Controls.SmartGrid.AutoSizeColumnMode.None;
            this.columnTransportType.CustomizeCells = true;
            this.columnTransportType.EditMode = Resco.Controls.SmartGrid.EditMode.Auto;
            this.columnTransportType.HeaderText = "Transport equipment";
            this.columnTransportType.MinimumWidth = 10;
            this.columnTransportType.Name = "columnTransportType";
            this.columnTransportType.Width = 354;
            // 
            // columnTransportQuantity
            // 
            this.columnTransportQuantity.Alignment = Resco.Controls.SmartGrid.Alignment.TopCenter;
            this.columnTransportQuantity.EditMode = Resco.Controls.SmartGrid.EditMode.Auto;
            this.columnTransportQuantity.HeaderText = "Quantity";
            this.columnTransportQuantity.MinimumWidth = 10;
            this.columnTransportQuantity.Name = "columnTransportQuantity";
            this.columnTransportQuantity.Width = 106;
            // 
            // columnPackageType
            // 
            this.columnPackageType.AutoSizeColumnMode = Resco.Controls.SmartGrid.AutoSizeColumnMode.None;
            this.columnPackageType.CustomizeCells = true;
            this.columnPackageType.EditMode = Resco.Controls.SmartGrid.EditMode.Auto;
            this.columnPackageType.HeaderText = "Package types";
            this.columnPackageType.MinimumWidth = 10;
            this.columnPackageType.Name = "columnPackageType";
            this.columnPackageType.Width = 354;
            // 
            // columnPackageQuantity
            // 
            this.columnPackageQuantity.Alignment = Resco.Controls.SmartGrid.Alignment.TopCenter;
            this.columnPackageQuantity.EditMode = Resco.Controls.SmartGrid.EditMode.Auto;
            this.columnPackageQuantity.HeaderText = "Quantity ";
            this.columnPackageQuantity.MinimumWidth = 10;
            this.columnPackageQuantity.Name = "columnPackageQuantity";
            this.columnPackageQuantity.Width = 106;
            // 
            // FormPackageTypeAndTransport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormPackageTypeAndTransport";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnReject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.SmartGrid.SmartGrid gridPackageType;
        private Resco.Controls.SmartGrid.SmartGrid gridTransportEquipment;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.OutlookControls.ImageButton btnReject;
        private Resco.Controls.OutlookControls.ImageButton btnAccept;
        private Resco.Controls.SmartGrid.Column columnTransportType;
        private Resco.Controls.SmartGrid.Column columnTransportQuantity;
        private Resco.Controls.SmartGrid.Column columnPackageType;
        private Resco.Controls.SmartGrid.Column columnPackageQuantity;
    }
}
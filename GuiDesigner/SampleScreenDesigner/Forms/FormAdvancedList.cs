﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    public partial class FormAdvancedList : Form
    {
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private TabMultipleButtons _tabButtons;
        public FormAdvancedList()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            BindList();


        }
        private void SetTextToControls()
        {
            _tabButtons = new TabMultipleButtons();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonOkName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            lblModuleName.Text = "Payments";
            
        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void BindList()
        {
            var colTextValues = new string[2];
            colTextValues[0] = "534525345";
            colTextValues[1] ="34.45";
            var insertRow1 = new Row(1, 2, colTextValues);

            var colTextValues1 = new string[2];
            colTextValues1[0] = "534525345";
            colTextValues1[1] = "34.45";
            var insertRow2 = new Row(1, 2, colTextValues1);

            var colTextValues2 = new string[2];
            colTextValues2[0] = "534525345";
            colTextValues2[1] = "34.45";
            var insertRow3 = new Row(1, 2, colTextValues2);


            advancedListOperationList.DataRows.Add(insertRow1);
            advancedListOperationList.DataRows.Add(insertRow2);
            advancedListOperationList.DataRows.Add(insertRow3);
        }

        void AddButtonToPanel()
        {
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Details",BtnDetailsClick));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Unpl pick", BtnUnplannedPickUpClick));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BtnBackClick,true,true));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Departure", BtnDepartureClick));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Scan", BtnScanClick, true));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Not Delivered", null));
            //tabMultipleButtons1.ListButtons.Add(new TabButton("Attempted Del.", BtnDetailsClick,new Point(320,0)));
            //tabMultipleButtons1.GenerateButtons();
        }


       
    }


    

   
}
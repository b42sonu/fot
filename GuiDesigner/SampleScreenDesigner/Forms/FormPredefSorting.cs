﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    public partial class FormPredefSorting : Form
    {
        private IEnumerable<DeliveryOperation> _operations = null;
        public FormPredefSorting()
        {
            InitializeComponent();
            _operations = GetMockedLoadList();
            BindOperations("tabNotStarted");
            HideOperationDetails();
            
        }

        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            switch (e.DataRow.CurrentTemplateIndex)
            {
                    
                case 1: // The master row has to be collapsed
                    break;
                case 2: // The master row has to be expanded  
                    UnSelectOtherRows();
                    e.DataRow.Selected = true;
                    ShowOperationDetail(e);
                    break;
            }
        }

        private void ShowOperationDetail(RowEventArgs e)
        {
            var consignment = (PlannedConsignmentsType)e.DataRow["Consignment"];
            if (consignment == null || consignment.Consignee==null) return;
            lblConsignee.Text = consignment.Consignee.Name1;
            lblConsigneeAdr.Text = consignment.Consignee.DeliveryAddress1;
            if (consignment.ConsignmentItem == null) return;
            lblConsignmentItems.Text = Convert.ToString(consignment.ConsignmentItem.Length);
        }

        private void HideOperationDetails()
        {
            lblConsigneeHead.Visible = false;
            lblConsignee.Visible = false;
            lblConsigneeAdrHeading.Visible = false;
            lblConsigneeAdr.Visible = false;
            lblConsignmentItemsHeading.Visible = false;
            lblConsignmentItems.Visible = false;

        }



        private void UnSelectOtherRows()
        {
            for (var i = 0; i <= listDeliveries.DataRows.Count - 1;i++ )
            {
                    
                var row = listDeliveries.DataRows[i];
                row.Selected = false;
            }

        }

        private List<DeliveryOperation> GetFilteredOperations(IEnumerable<DeliveryOperation> operations, string selctedTabName)
        {
            if (operations!=null)
            switch (selctedTabName)
            {
                case "tabNotStarted":
                    return operations.Where(c => c.Status == DeliveryStatus.New || c.Status == DeliveryStatus.Started).ToList();
                case "tabCompleted":
                    return operations.Where(c => c.Status == DeliveryStatus.Finished).ToList();
                case "tabAll":
                    return operations.Where(c => c.Status == DeliveryStatus.New || c.Status == DeliveryStatus.Started || c.Status == DeliveryStatus.Finished).ToList();
            }
            return null;
        }

        private void BindOperations(string selctedTabName)
        {
            listDeliveries.BeginUpdate();
            var filteredoperations = GetFilteredOperations(_operations, selctedTabName);
            listDeliveries.DataSource = filteredoperations;
            listDeliveries.EndUpdate();
        }

        private IEnumerable<DeliveryOperation> GetMockedLoadList()
        {
            /////////////////// Operation1 //////////////////////

            var consignment1 = new PlannedConsignmentsType
            {
                ConsignmentNumber = "70713750024118488",
                ConsignmentItemCount = 2
            };


            var consignee1 = new ConsigneeType
            {
                Name1 = "Rakesh Aggarwal",
                DeliveryAddress1 = "#3462, Sector 39/b",
                DeliveryPostalCode = "1234",
                DeliveryPostalName = "ABC Postal"
            };

            var consignor1 = new ConsignorType
            {
                Name1 = "Aggarwal Rakesh",
                Address1 = "#3462, Sector b/39",
                PostalCode = "4321",
                PostalName = "CBA Postal"
            };

            consignment1.Consignee = consignee1;
            consignment1.Consignor = consignor1;


            var consignmentItem1 = new ConsignmentItemType { ConsignmentItemNumber = "370713750043854986" };

            var consignmentItem2 = new ConsignmentItemType();
            consignmentItem1.ConsignmentItemNumber = "370713750043854993";

            consignment1.ConsignmentItem = new[] { consignmentItem1, consignmentItem2 };

            var deliveryOperation1 = new DeliveryOperation
            {
                Consignment = consignment1,
                Status = DeliveryStatus.New,
                OperationType = 0,
                OperationDetail = GetOperationDetail(consignment1),
                DefaultSortNo = 1

            };

            /////////////////// Operation2 /////////////////////

            var consignment2 = new PlannedConsignmentsType { ConsignmentNumber = "70713750024118648" };

            var consignee2 = new ConsigneeType
            {
                Name1 = "Amit Singla",
                DeliveryAddress1 = "#1462, Sector 41/b",
                DeliveryPostalCode = "5678",
                DeliveryPostalName = "DEF Postal"
            };

            var consignor2 = new ConsignorType
            {
                Name1 = "Singla Amit",
                Address1 = "#1462, Sector 41/b",
                PostalCode = "8765",
                PostalName = "FED Postal"
            };

            consignment2.Consignee = consignee2;
            consignment2.Consignor = consignor2;


            var consignmentItem3 = new ConsignmentItemType { ConsignmentItemNumber = "370713750043855143" };

            var consignmentItem4 = new ConsignmentItemType { ConsignmentItemNumber = "370713750043855150" };

            consignment2.ConsignmentItem = new[] { consignmentItem3, consignmentItem4 };

            var deliveryOperation2 = new DeliveryOperation
            {
                Consignment = consignment2,
                Status = DeliveryStatus.Started,
                OperationType = 0,
                OperationDetail = GetOperationDetail(consignment2),
                DefaultSortNo = 2
            };

            //////////////////// Operation3 ////////////////////////

            var consignment3 = new PlannedConsignmentsType { ConsignmentNumber = "70713750024118631" };

            var consignee3 = new ConsigneeType
            {
                Name1 = "Aman Jain",
                DeliveryAddress1 = "#2462, Sector 42/b",
                DeliveryPostalCode = "9101",
                DeliveryPostalName = "GHI Postal"
            };

            var consignor3 = new ConsignorType
            {
                Name1 = "Jain Aman",
                Address1 = "#2462, Sector b/42",
                PostalCode = "1019",
                PostalName = "IHG Postal"
            };

            consignment3.Consignee = consignee3;
            consignment3.Consignor = consignor3;

            var consignmentItem5 = new ConsignmentItemType { ConsignmentItemNumber = "370713750043855082" };

            var consignmentItem6 = new ConsignmentItemType { ConsignmentItemNumber = "370713750043855099" };

            consignment3.ConsignmentItem = new[] { consignmentItem5, consignmentItem6 };

            var deliveryOperation3 = new DeliveryOperation
            {
                Consignment = consignment3,
                Status = DeliveryStatus.New,
                OperationType = 0,
                OperationDetail = GetOperationDetail(consignment3),
                DefaultSortNo = 3
            };

            var listOfObjects = new List<DeliveryOperation> { deliveryOperation1, deliveryOperation2, deliveryOperation3 };
            return listOfObjects;
        }


        private string GetOperationDetail(PlannedConsignmentsType consignment)
        {
            var details = new StringBuilder();

            if (!string.IsNullOrEmpty(consignment.ConsignmentNumber))
                details.Append(consignment.ConsignmentNumber);

            var consignee = consignment.Consignee;
            if (consignee == null) return details.ToString();

            if (consignment.Consignee == null)
                return details.ToString();

            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1))
                details.Append(consignment.Consignee.DeliveryAddress1);

            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalCode))
            {
                details.Append(Environment.NewLine + consignment.Consignee.DeliveryPostalCode);

                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
                    details.Append("," + consignment.Consignee.DeliveryPostalName);
            }
            else
            {
                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
                    details.Append(Environment.NewLine + consignment.Consignee.DeliveryPostalName);
            }
            return details.ToString();
        }
        private void btnReversed_Click(object sender, System.EventArgs e)
        {
            Reversed();
        }

        private void btnZipCode_Click(object sender, System.EventArgs e)
        {
            SortByZiCode();
        }

        private void btnAsLoaded_Click(object sender, System.EventArgs e)
        {
            AsLoaded();
        }

        



        private void SortByZiCode()
        {
            _operations = from c in _operations
                          orderby
                              c.Consignment.Consignee.DeliveryPostalCode ,c.Consignment.Consignee.DeliveryAddress1,
                              c.Consignment.Consignee.DeliveryPostalName select c;

            BindOperations("tabNotStarted");
        }


        private void Reversed()
        {
            var operationsArr = _operations.ToArray();
            _operations = operationsArr.Reverse().ToList();
    
            BindOperations("tabNotStarted");
        
        }

        private void AsLoaded()
        {
_operations = from c in _operations
                          orderby
                              c.DefaultSortNo
                          select c;

            BindOperations("tabNotStarted");
        }

    }


        




}
﻿namespace Com.Bring.PMP.PreComFW.OperationList.Views
{
    partial class FormConsignmentDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.webBrowserDetail = new System.Windows.Forms.WebBrowser();
            this.lblConsignmentHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnScan = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDeparture = new Resco.Controls.OutlookControls.ImageButton();
            this.lstBoxConsignmentNo = new System.Windows.Forms.ListBox();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeparture)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.webBrowserDetail);
            this.touchPanel.Controls.Add(this.lblConsignmentHeading);
            this.touchPanel.Controls.Add(this.btnScan);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnDeparture);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // webBrowserDetail
            // 
            this.webBrowserDetail.Location = new System.Drawing.Point(3, 103);
            this.webBrowserDetail.Name = "webBrowserDetail";
            this.webBrowserDetail.Size = new System.Drawing.Size(234, 131);
            // 
            // lblConsignmentHeading
            // 
            this.lblConsignmentHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsignmentHeading.Location = new System.Drawing.Point(2, 5);
            this.lblConsignmentHeading.Name = "lblConsignmentHeading";
            this.lblConsignmentHeading.Size = new System.Drawing.Size(0, 0);
            // 
            // btnScan
            // 
            this.btnScan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnScan.ForeColor = System.Drawing.Color.White;
            this.btnScan.Location = new System.Drawing.Point(168, 241);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(80, 24);
            this.btnScan.TabIndex = 4;
            this.btnScan.Click += new System.EventHandler(this.BtnScanClick);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 241);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(80, 24);
            this.btnBack.TabIndex = 5;
            this.btnBack.Click += new System.EventHandler(this.BtnBackClick);
            // 
            // btnDeparture
            // 
            this.btnDeparture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDeparture.ForeColor = System.Drawing.Color.White;
            this.btnDeparture.Location = new System.Drawing.Point(84, 241);
            this.btnDeparture.Name = "btnDeparture";
            this.btnDeparture.Size = new System.Drawing.Size(80, 24);
            this.btnDeparture.TabIndex = 3;
            this.btnDeparture.Click += new System.EventHandler(this.BtnDepartureClick);
            // 
            // lstBoxConsignmentNo
            // 
            this.lstBoxConsignmentNo.Location = new System.Drawing.Point(2, 25);
            this.lstBoxConsignmentNo.Name = "lstBoxConsignmentNo";
            this.lstBoxConsignmentNo.Size = new System.Drawing.Size(237, 72);
            this.lstBoxConsignmentNo.TabIndex = 2;
            // 
            // FormConsignmentDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.lstBoxConsignmentNo);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormConsignmentDetail";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeparture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstBoxConsignmentNo;

        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentHeading;

        private Resco.Controls.OutlookControls.ImageButton btnDeparture;
        private Resco.Controls.OutlookControls.ImageButton btnScan;
        private Resco.Controls.OutlookControls.ImageButton btnBack;

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private System.Windows.Forms.WebBrowser webBrowserDetail;
    }
}
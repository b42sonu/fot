﻿using System.Drawing;
using System.Windows.Forms;
using System;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    public partial class FormReconcillation : Form
    {
        const int RowHeight = 40;
        const int HeaderHeight = 28;

        #region Private Variables and Properties
        
        public FormReconcillation()
        {
            InitializeComponent();
            OnShow(null);

        }


        #endregion


        #region Methods and Funcions

        public void OnShow(object data)
        {
            try
            {
                ListConsignments.Templates.Clear();
                ListConsignments.DataRows.Clear();

                string[] columnNames = { "Consignments"};

                var textCellSelectedCol1 = new TextCell
                {
                    Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft,
                    CellSource = { ColumnName = columnNames[0] },
                    Bounds = new Rectangle(10, 0, 280, RowHeight),
                };

                var rowSelectedTemplate = new RowTemplate
                {
                    BackColor = Color.DarkGray,
                    Height = RowHeight,
                    Name = "Selected",
                    ForeColor = Color.Black
                };
                rowSelectedTemplate.CellTemplates.Add(textCellSelectedCol1);
                

                var textCellUnselectedCol1 = new TextCell
                {
                    Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft,
                    CellSource = { ColumnName = columnNames[0] },
                    Bounds = new Rectangle(10, 0, 280, RowHeight),
                };

                var rowUnselectedTemplate = new RowTemplate
                {
                    BackColor = Color.White,
                    Height = RowHeight,
                    Name = "Unselected",
                    ForeColor = Color.Black
                };
                rowUnselectedTemplate.CellTemplates.Add(textCellUnselectedCol1);
                
                ListConsignments.Templates.Add(rowUnselectedTemplate);
                ListConsignments.Templates.Add(rowSelectedTemplate);
                
                ListConsignments.ShowHeader = false;
                
                var columnMapping = new Mapping(columnNames);

                var dataValues = new string[1];

                //Add dummy data
                var dummyConsignments = new[] 
                { "823456543212345658","723456543212345657","623456543212345656","523456543212345655","423456543212345654","323456543212345653","223456543212345652", "423456543444445654", "323456543212345653" };
                ListConsignments.BeginUpdate();
                foreach (var consignmentId in dummyConsignments)
                {
                    dataValues[0] = consignmentId;
                    var insertRow = new Row(0, 1, dataValues, columnMapping);
                    ListConsignments.DataRows.Add(insertRow);
                }
                ListConsignments.EndUpdate();

            }
            catch
            {
            }
        }


        #endregion

        #region Events

        private void OnActiveRowChanged(object sender, EventArgs e)
        {
          
        }

        private void ButtonDetailsClick(object sender, EventArgs e)
        {

        }


        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
 
        }

        #endregion

        private void ButtonDeviationClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ButtonItemsClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ButtonDetailClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void lblCustomerNo_ParentChanged(object sender, EventArgs e)
        {

        }
    }
}
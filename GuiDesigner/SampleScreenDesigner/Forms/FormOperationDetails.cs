﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using SampleScreenDesigner.Controls;
using System.Text;

namespace SampleScreenDesigner.Forms
{
    public partial class FormOperationDetails : Form
    {
        TabMultipleButtons tabMultipleButtons1;
        OperationDetail od;
        int StopCount = 0;
        int GoodCount = 0;
        int VASCount = 0;
        int DEVCount = 0;
        public string OperationType = "Pickup";
        public FormOperationDetails()
        {
            InitializeComponent();
            tabMultipleButtons1 = new TabMultipleButtons();
            touchPanel.Controls.Add(tabMultipleButtons1);
            MokData();
        }

        void AddRecords()
        {
            od = new OperationDetail();
            od.Order = "<table><tr><td><b>Cust:</b></td><td>Forsmann &amp; Bodenfors AB</td></tr>" +
        "<tr><td>&nbsp;</td><td> &nbsp;</td></tr>" +
        "<tr><td><b>Ref:</b></b></td><td>Test</td></tr>" +
        "<tr><td><b>Order</b></td><td>25343</td></tr>" +
        "<tr><td><b>Services:</b></td><td>1HFETD</td></tr>" +
        "<tr><td><b>Start</b></td><td>8:44</td></tr>" +
        "<tr><td><b>End</b></td><td>9:34</td></tr>" +
        "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>" +
        "<tr><td><b>Order Info:</b></td><td> &nbsp;</td></tr></table>";

            od.Stops.Add("<table><tr><td><b>Stop 1 0f  3</b></b></td><td>Pickup</tr>" +
         "<tr><td><b>Name:</b></td><td>Forsmann &amp; Bodenfors AB</td></tr>" +
        "<tr><td><b>Address:</b></b></td><td>Kyrkogatan 48, 42298 asefh</td></tr>" +
        "<tr><td><b>Time</b></td><td>08:59-23:59</td></tr></table>");

            od.Stops.Add("<table><tr><tr><td><b>Stop 2 0f  3</b></b></td><td>Pickup</tr>" +
        "<td><b>Name:</b></td><td>Stop 2</td></tr>" +
        "<tr><td><b>Address:</b></b></td><td>Mohali India</td></tr>" +
        "<tr><td><b>Time</b></td><td>02:59-23:59</td></tr></table>");

            od.Stops.Add("<table><tr><tr><td><b>Stop 3 0f  3</b></b></td><td>Pickup</tr>" +
        "<tr><td><b>Name:</b></td><td>Stop 3</td></tr>" +
        "<tr><td><b>Address:</b></b></td><td>Chandigarh India</td></tr>" +
        "<tr><td><b>Time</b></td><td>01:59-08:59</td></tr></table>");



            od.Goods.Add("<table><tr><td><b>Gooods Information</b></b></td><td>1 of 3</tr>" +
       "<tr><td><b>Type:</b></td><td>Good Type 1</td></tr>" +
       "<tr><td><b>Number :</b></b></td><td>34</td></tr>" +
       "<tr><td><b>Weight</b></td><td>54</td></tr>" +
       "<tr><td><b>Volume:</b></td><td>12L</td></tr>" +
       "<tr><td><b>Goods Info:</b></td><td> &nbsp;</td></tr></table>");

            od.Goods.Add("<table><tr><td><b>Gooods Information</b></b></td><td>2 of 3</tr>" +
            "<tr><td><b>Type:</b></td><td>Good Type 2</td></tr>" +
     "<tr><td><b>Number :</b></b></td><td>34</td></tr>" +
     "<tr><td><b>Weight</b></td><td>23</td></tr>" +
     "<tr><td><b>Volume:</b></td><td>56L</td></tr>" +
     "<tr><td><b>Goods Info:</b></td><td> Temporary Information</td></tr></table>");

            od.Goods.Add("<table><tr><td><b>Gooods Information</b></b></td><td>3 of 3</tr>" +
            "<tr><td><b>Type:</b></td><td>Good Type 3</td></tr>" +
    "<tr><td><b>Number :</b></b></td><td>34</td></tr>" +
    "<tr><td><b>Weight</b></td><td>23</td></tr>" +
    "<tr><td><b>Volume:</b></td><td>56L</td></tr>" +
    "<tr><td><b>Goods Info:</b></td><td> Temporary Information</td></tr></table>");

            od.VAS.Add("<table><tr><td><b>VAS 1 0f 3</b></b></td><td></tr>" +
            "<tr><td><b>Type:</b></td><td>VAS Type 1</td></tr>" +
    "<tr><td><b>Number :</b></b></td><td>34</td></tr>" +
    "<tr><td><b>Weight</b></td><td>23</td></tr>" +
    "<tr><td><b>Volume:</b></td><td>56L</td></tr>" +
    "<tr><td><b>Goods Info:</b></td><td> Temporary Information</td></tr></table>");

            od.VAS.Add("<table><tr><td><b>VAS 2 0f 3</b></b></td><td></tr>" +
            "<tr><td><b>Type:</b></td><td>VAS Type 2</td></tr>" +
   "<tr><td><b>Number :</b></b></td><td>23</td></tr>" +
   "<tr><td><b>Weight</b></td><td>23</td></tr>" +
   "<tr><td><b>Volume:</b></td><td>56L</td></tr>" +
   "<tr><td><b>VAS Info:</b></td><td> Temporary Information</td></tr></table>");

            od.VAS.Add("<table><tr><td><b>VAS 3 0f 3</b></b></td><td></tr>" +
            "<tr><td><b>Type:</b></td><td>VAS Type 3</td></tr>" +
   "<tr><td><b>Number :</b></b></td><td>34</td></tr>" +
   "<tr><td><b>Weight</b></td><td>23</td></tr>" +
   "<tr><td><b>Volume:</b></td><td>56L</td></tr>" +
   "<tr><td><b>VAS Info:</b></td><td> Temporary Information</td></tr></table>");

            od.DEV = ShowDEV();



        }
        string ShowDEV()
        {
            List<PdaDeviation> DeviationItem = new List<PdaDeviation>();
            DeviationItem.Add(new PdaDeviation("Cause 1","Action 1","Comment 1"));
            DeviationItem.Add(new PdaDeviation("Cause 1","Action 2","Comment 2"));
            DeviationItem.Add(new PdaDeviation("Cause 1","Action 3","Comment 3"));
            var html = new StringBuilder();
            //add styles
            html = html.Append("<style>div{background-color:#00FFFF;width:100%height:200px;overflow:scroll;border:1px solid; margin-top:5px;}table{width:100%;border-collapse:collapse;border-bottom:1px solid;background-color:yellow;}td{vertical-align:text-top;}</style>");
            html.Append("<tablestyle='border-bottom:none;'><tr><td width='70px'><b>Customer</b></td><td width='10px'>:</td><td>Customer Name</td></tr></table>");
            html.Append("<div>");
            foreach (PdaDeviation item in DeviationItem)
            {
                html.Append(CreateDeviationItem(item.Cause, item.Action, item.ExtraInfo));
            }
            webBrowserOperationDetails.DocumentText = html.ToString();
            html = html.Append("</div>");
            return html.ToString();
        }

        private string CreateDeviationItem(string cause, string action, string comment)
        {
            var itemHtml = new StringBuilder();
            itemHtml.Append("<table><tr><td width='70px'>Reason</td><td width='10px'>:</td><td>" + cause + "</td></tr>");
            itemHtml.Append("<tr><td>Action </td><td>:</td><td>" + action + "</td></tr>");
            itemHtml.Append("<tr><td>Comment</td><td>:</td><td>" + comment + "</td></tr></table>");
            return itemHtml.ToString();
        }
        void MokData()
        {
            AddRecords();
            tabControlOperationDetail.TabPages[0].Text = "Order";
            if (od.Stops != null)
            {
                tabControlOperationDetail.TabPages[1].Text = "Stops(" + od.Stops.Count + ")";
            }
            if (od.Goods != null)
            {
                tabControlOperationDetail.TabPages[2].Text = "Goods(" + od.Goods.Count + ")";
            }
            if (od.VAS != null)
            { tabControlOperationDetail.TabPages[3].Text = "VAS(" + od.VAS.Count + ")"; }
          

            webBrowserOperationDetails.DocumentText = od.Order;
            webBrowserOperationDetails.Parent.Controls.Remove(webBrowserOperationDetails);
            tabControlOperationDetail.TabPages[tabControlOperationDetail.SelectedIndex].Controls.Add(webBrowserOperationDetails);
            SetButtons(0);
        }

        void SetButtons(int tabIndex)
        {
            switch (tabIndex)
            {

                case 0:
                    webBrowserOperationDetails.DocumentText = od.Order;
                    tabMultipleButtons1.Clear();
                    tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BackButtonClick));
                    tabMultipleButtons1.GenerateButtons();
                    break;
                case 1:
                    webBrowserOperationDetails.DocumentText = od.Stops[StopCount];
                    tabMultipleButtons1.Clear();
                    tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BackButtonClick));
                    if (od.Stops != null && od.Stops.Count > 1)
                    {
                        tabMultipleButtons1.ListButtons.Add(new TabButton("Previous Stop", ButtonPreviousStopClick, "ButtonPreviousStop") { Enabled = false });
                        tabMultipleButtons1.ListButtons.Add(new TabButton("Next Stop", ButtonNextStopClick, "ButtonNextStop"));
                    }
                    tabMultipleButtons1.GenerateButtons();
                    break;
                case 2:
                    webBrowserOperationDetails.DocumentText = od.Goods[GoodCount];
                    tabMultipleButtons1.Clear();
                    tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BackButtonClick));
                    if (od.Goods != null && od.Goods.Count > 1)
                    {
                        tabMultipleButtons1.ListButtons.Add(new TabButton("Previous Good", ButtonPreviousGoodClick, "ButtonPreviousGood") { Enabled = false });
                        tabMultipleButtons1.ListButtons.Add(new TabButton("Next Good", ButtonNextGoodClick, "ButtonNextGood"));
                    } tabMultipleButtons1.GenerateButtons();
                    break;
                case 3:
                    webBrowserOperationDetails.DocumentText = od.VAS[VASCount];
                    tabMultipleButtons1.Clear();
                    tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BackButtonClick));
                    if (od.VAS != null && od.VAS.Count > 1)
                    {
                        tabMultipleButtons1.ListButtons.Add(new TabButton("Previous VAS", ButtonPreviousVASClick, "ButtonPreviousVAS") { Enabled = false });
                        tabMultipleButtons1.ListButtons.Add(new TabButton("Next VAS", ButtonNextVASClick, "ButtonNextVAS"));
                    } tabMultipleButtons1.GenerateButtons();
                    break;
                case 4:
                    webBrowserOperationDetails.DocumentText = od.DEV;
                    tabMultipleButtons1.Clear();
                    tabMultipleButtons1.ListButtons.Add(new TabButton("Back", BackButtonClick));                   
                    break;
            }
        }
        private void TabControlOperationSelectedIndexChanged(object sender, EventArgs e)
        {
            webBrowserOperationDetails.Parent.Controls.Remove(webBrowserOperationDetails);
            tabControlOperationDetail.TabPages[tabControlOperationDetail.SelectedIndex].Controls.Add(webBrowserOperationDetails);
            SetButtons(tabControlOperationDetail.SelectedIndex);
        }
        private void OnActiveRowChanged(object sender, EventArgs e)
        {
        }
        protected void BackButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("Go to previous menu!");
        }
        protected void ButtonNextStopClick(object sender, EventArgs e)
        {
            tabMultipleButtons1.Enable("ButtonPreviousStop", true);
            webBrowserOperationDetails.DocumentText = od.Stops[++StopCount];
            tabMultipleButtons1.Enable("ButtonNextStop", StopCount != od.Stops.Count - 1);
        }
        protected void ButtonPreviousStopClick(object sender, EventArgs e)
        {
            tabMultipleButtons1.Enable("ButtonNextStop", true);            
            webBrowserOperationDetails.DocumentText = od.Stops[--StopCount];
            tabMultipleButtons1.Enable("ButtonPreviousStop", StopCount != 0);
        }
        protected void ButtonNextGoodClick(object sender, EventArgs e)
        {
            tabMultipleButtons1.Enable("ButtonPreviousGood", true);
            webBrowserOperationDetails.DocumentText = od.Goods[++GoodCount];
            tabMultipleButtons1.Enable("ButtonNextGood", GoodCount != od.Goods.Count - 1);            
        }
        protected void ButtonPreviousGoodClick(object sender, EventArgs e)
        {
            tabMultipleButtons1.Enable("ButtonNextGood", true);           
            webBrowserOperationDetails.DocumentText = od.Goods[--GoodCount];
            tabMultipleButtons1.Enable("ButtonPreviousGood", GoodCount != 0);
        }
        protected void ButtonNextVASClick(object sender, EventArgs e)
        {
                    tabMultipleButtons1.Enable("ButtonPreviousVAS", true);
            webBrowserOperationDetails.DocumentText = od.VAS[++VASCount];
            tabMultipleButtons1.Enable("ButtonNextVAS", VASCount != od.VAS.Count - 1);
        }
        protected void ButtonPreviousVASClick(object sender, EventArgs e)
        {
            tabMultipleButtons1.Enable("ButtonNextVAS", true);
            webBrowserOperationDetails.DocumentText = od.VAS[--VASCount]; 
            tabMultipleButtons1.Enable("ButtonPreviousVAS", VASCount != 0);

        }
        protected void ButtonNextDEVClick(object sender, EventArgs e)
        {  
        }
        protected void ButtonPreviousDEVClick(object sender, EventArgs e)
        {
            tabMultipleButtons1.Enable("ButtonNextDEV", true);         
            tabMultipleButtons1.Enable("ButtonPreviousDEV", DEVCount != 0);
        }

    }
    public class PdaDeviation
    {
        public string Cause { get; set; }
        public string Action { get; set; }
        public string ExtraInfo { get; set; }
        public PdaDeviation(string cause, string action, string extraInfo)
        {
            Cause = cause;
            Action = action;
            ExtraInfo = extraInfo;
        }
    }
    public class OperationDetail
    {
        public string Order { get; set; }
        public List<string> Stops = new List<string>();
        public List<string> Goods = new List<string>();
        public List<string> VAS = new List<string>();
        public string DEV { get; set; }

    }
}
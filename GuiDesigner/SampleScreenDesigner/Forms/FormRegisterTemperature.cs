﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    public partial class FormRegisterTemperature : Form
    {
        public FormRegisterTemperature()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
        }
        private void SetTextToGui()
        {
            btnPhoto.Text = GlobalTexts.Camera;
            btnBack.Text = GlobalTexts.Back;
            btnOk.Text = GlobalTexts.Ok;
            labelPicture.Text = GlobalTexts.TakePictureConfirmation;
            labelComment.Text = GlobalTexts.OptionalComment;
            labelTemperature.Text = GlobalTexts.Temperature + "(+/-)";
            labelModuleName.Text = GlobalTexts.RegisterTemperature;
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        private void ButtonPhotoClick(object sender, EventArgs e)
        {

        }

        
        

      

    }
}
﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace SampleScreenDesigner.Forms
{
    public partial class FormAttemptedDeliveryList : Form
    {
        public FormAttemptedDeliveryList()
        {
            InitializeComponent();
            smartGridConsignment.DataSource = ConsignmentDetails;
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        public IList<ConsignmentDetail> ConsignmentDetails
        {
            get
            {
                List<ConsignmentDetail> consignmentDetailList = new List<ConsignmentDetail>();

                consignmentDetailList.Add(new ConsignmentDetail { IsChecked = "false", ConsignmentNumber = "12345" });
                consignmentDetailList.Add(new ConsignmentDetail { IsChecked = "true", ConsignmentNumber = "546546456" });
                consignmentDetailList.Add(new ConsignmentDetail { IsChecked = "false", ConsignmentNumber = "4563656" });

                return consignmentDetailList;
            }
        }


    }

    public class ConsignmentDetail
    {
        public string IsChecked { get; set; }
        public string ConsignmentNumber { get; set; }
    }
}
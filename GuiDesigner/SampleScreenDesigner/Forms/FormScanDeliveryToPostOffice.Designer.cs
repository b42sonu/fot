﻿
using System;
using Resco.Controls.CommonControls;

namespace SampleScreenDesigner.Forms
{
    partial class FormScanDeliveryToPostOffice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.txtScannedNumber = new PreCom.Controls.PreComInput2();
            this.lblScan = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblItem = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblItemCount = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCount)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblItemCount);
            this.touchPanel.Controls.Add(this.lblItem);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.lblScan);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            //this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(15, 122);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(452, 48);
            this.txtScannedNumber.TabIndex = 24;
            this.txtScannedNumber.TextTranslation = false;
            // 
            // lblScan
            // 
            this.lblScan.Location = new System.Drawing.Point(15, 90);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(342, 29);
            this.lblScan.Text = "Scan or enter Consignment item";
            // 
            // MsgMessage
            // 
            this.messageControlValidateScan.Location = new System.Drawing.Point(15, 179);
            this.messageControlValidateScan.MessageText = "";
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(452, 97);
            this.messageControlValidateScan.TabIndex = 14;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 42);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.Text = "Delivery to PO/PiB by customer";
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblItem
            // 
            this.lblItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblItem.Location = new System.Drawing.Point(3, 428);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(133, 29);
            this.lblItem.Text = "No. of Items";
            // 
            // lblItemCount
            // 
            this.lblItemCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblItemCount.Location = new System.Drawing.Point(161, 428);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(14, 29);
            this.lblItemCount.Text = "0";
            // 
            // FormScanDeliveryToPoByCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);

            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormScanDeliveryToPoByCustomer";
            this.Text = "FormScanDeliveryToPoByCustomer";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCount)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblScan;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControlValidateScan;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private TransparentLabel lblItemCount;
        private TransparentLabel lblItem;
    }
}
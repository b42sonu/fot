﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{



    /// <summary>
    /// This class, shows the list of Loading/Unloading operations, depending on for
    /// which operation type screen is opened.
    /// </summary>
    public partial class FormOperationsListSorting : Form
    {
        private IEnumerable<SortStops> _listOperation;
        private string _selectedStopId = string.Empty;
        private TabMultipleButtons _tabMultipleButtons;
        private const string TabButtonEmpty = "Empty";
        private const string TabButtonInvisible = "Invisible";
        #region Methods and Functions

        /// <summary>
        /// Default constructor for class.
        /// </summary>
        public FormOperationsListSorting()
        {
            InitializeComponent();
            SetTextToGui();
            SetControlStatus(false);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            CreateDummyData();
        }

        private void SetTextToGui()
        {
            _tabMultipleButtons = new TabMultipleButtons();
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonUpClick) { ButtonType = TabButtonType.Cancel });
            _tabMultipleButtons.ListButtons.Add(new TabButton("", null, TabButtonEmpty));
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick) { ButtonType = TabButtonType.Confirm });
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Up, ButtonUpClick));
            _tabMultipleButtons.ListButtons.Add(new TabButton("", null, TabButtonInvisible));
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Down, ButtonDownClick));
            touchPanel.Controls.Add(_tabMultipleButtons);
            _tabMultipleButtons.GenerateButtons();

            _tabMultipleButtons.HideButton(TabButtonEmpty, false);
            _tabMultipleButtons.GetButton(TabButtonInvisible).Visible = false;
            labelModuleName.Text = GlobalTexts.Operations;
        }






        /// <summary>
        /// This method binds the specified list of operations to advance list
        /// </summary>
        private void BindOperations()
        {
            if (_listOperation != null)
            {
                _listOperation = from c in _listOperation orderby c.OrignalSortOrder select c;
                listOperations.BeginUpdate();
                listOperations.DataSource = _listOperation;
                listOperations.EndUpdate();
            }
            else
            {
                listOperations.BeginUpdate();
                listOperations.DataSource = null;
                listOperations.EndUpdate();
            }
        }

        /// <summary>
        /// This method runs when user clicks any operation in list of operations. it deselects
        /// all rows in list of operations.
        /// </summary>
        private void DeSelectOtherRows()
        {
            for (var i = 0; i <= listOperations.DataRows.Count - 1; i++)
            {
                var row = listOperations.DataRows[i];
                row.Selected = false;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// This event runs every time user selects any row in list of operations and processes the
        /// selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            switch (e.DataRow.CurrentTemplateIndex)
            {
                case 1: // The master row has to be collapsed
                    //btnScan.Enabled = false;
                    break;
                case 2: // The master row has to be expanded  
                    ProcessSelectedRow(e);
                    //btnScan.Enabled = true;
                    break;
            }
        }

        /// <summary>
        /// This method does the following:
        /// 1) Deselect all other rows.
        /// 2) Selects the clicked row.
        /// 3) Get the stopId, TripId and OperationId from selected row.
        /// 4) Shows the operation detail at the bottom.
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSelectedRow(RowEventArgs e)
        {
            DeSelectOtherRows();
            e.DataRow.Selected = true;

            _selectedStopId = Convert.ToString(e.DataRow["StopId"]);
            


        }




        private void SetValuesOnBetweenOperation(SortStops operationListOperation1, SortStops operationListOperation2, SortStops loadListOperation, double divideOperationCount, SortButtonType sortButtonType)
        {

            List<SortStops> betweenOperations = null;
            double tempSortOrder = 0.0;
            int betweenOperationCount = 1;
            bool isLoadlistOperationSetFirst = false;
            switch (sortButtonType)
            {
                case SortButtonType.Up:
                    if (operationListOperation1 != null && operationListOperation2 != null)
                    {
                        betweenOperations = _listOperation.Where(
                            n =>
                            n.OrignalSortOrder < operationListOperation2.OrignalSortOrder &&
                            n.OrignalSortOrder > operationListOperation1.OrignalSortOrder).ToList();


                        tempSortOrder = operationListOperation1.OrignalSortOrder;
                    }
                    else if (operationListOperation1 == null && operationListOperation2 != null)
                    {
                        betweenOperations = _listOperation.Where(
                                    n => n.OrignalSortOrder < operationListOperation2.OrignalSortOrder).ToList();
                    }
                    else if (operationListOperation1 != null)
                    {
                        betweenOperations = _listOperation.Where(
                                    n => n.OrignalSortOrder > operationListOperation1.OrignalSortOrder).ToList();
                        tempSortOrder = operationListOperation1.OrignalSortOrder;
                        isLoadlistOperationSetFirst = true;
                    }
                    break;
                case SortButtonType.Down:
                    if (operationListOperation1 != null && operationListOperation2 != null)
                    {
                        betweenOperations = _listOperation.Where(
                            n =>
                            n.OrignalSortOrder < operationListOperation2.OrignalSortOrder &&
                            n.OrignalSortOrder > operationListOperation1.OrignalSortOrder).ToList();


                        tempSortOrder = operationListOperation1.OrignalSortOrder;
                    }
                    else if (operationListOperation1 == null && operationListOperation2 != null)
                    {
                        betweenOperations = _listOperation.Where(
                                    n => n.OrignalSortOrder < operationListOperation2.OrignalSortOrder).ToList();
                    }
                    else if (operationListOperation1 != null)
                    {
                        betweenOperations = _listOperation.Where(
                                    n => n.OrignalSortOrder > operationListOperation1.OrignalSortOrder).ToList();
                        tempSortOrder = operationListOperation1.OrignalSortOrder;
                        isLoadlistOperationSetFirst = true;
                    }
                    break;
            }

            if (betweenOperations != null)
                betweenOperationCount = betweenOperations.Count;

            double diff = GetDifferenceBetweenOPerationListOperation(null, null);
            double startIndex = diff / (betweenOperationCount + divideOperationCount);
            double startSortOrder = tempSortOrder + startIndex;

            if (loadListOperation != null && isLoadlistOperationSetFirst)
            {
                loadListOperation.OrignalSortOrder = startSortOrder;
                startSortOrder = startSortOrder + startIndex;
            }

            if (betweenOperations != null)
            {
                foreach (SortStops operation in betweenOperations)
                {
                    operation.OrignalSortOrder = startSortOrder;
                    startSortOrder = startSortOrder + startIndex;
                }
                if (loadListOperation != null && isLoadlistOperationSetFirst == false) loadListOperation.OrignalSortOrder = startSortOrder;
            }
            else
            {
                if (loadListOperation != null) loadListOperation.OrignalSortOrder = startSortOrder;
            }


            //return startSortOrder;
        }


        private SortStops GetOperationListNextOperation(double sortOrder)
        {
            var operations = _listOperation.Where(
                n => n.OrignalSortOrder > sortOrder && n.StopType == StopType.OperationListOperation).ToList();

            return operations.Any() ? operations.FirstOrDefault() : null;
        }
        private SortStops GetOperationListPreviousOperation(double sortOrder)
        {
            var operations = _listOperation.Where(
                                     n => n.OrignalSortOrder < sortOrder && n.StopType == StopType.OperationListOperation).ToList().OrderByDescending(c => c.OrignalSortOrder);

            return operations.Any() ? operations.FirstOrDefault() : null;
        }

        private double GetDifferenceBetweenOPerationListOperation(SortStops operation1, SortStops operation2)
        {
            double diff = 1.0;
            if (operation1 != null && operation2 != null)
                diff = operation1.OrignalSortOrder - operation2.OrignalSortOrder;
            return diff;

        }
        private void BtnOkClick(object sender, EventArgs e)
        {
            SetControlStatus(true);
        }

        /// <summary>
        /// This event runs when user clicks on back button and send message to flow that
        /// user clicked back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonUpClick(object sender, EventArgs e)
        {
            if (listOperations.SelectedRow != null)
            {
                Row row = listOperations.SelectedRow;
                //MessageBox.Show(row.Index.ToString());

                if (row.Index > 0)
                {
                    SortStops(row, SortButtonType.Up);

                    /*_selectedStopId = Convert.ToString(row["StopId"]);
                    _selectedOperationId = Convert.ToString(row["OperationId"]);
                    double selectedSortOrder = Convert.ToDouble(row["OrignalSortOrder"]);


                    int upRowIndex = row.Index - 1;
                    Row rowUp = listOperations.DataRows[upRowIndex];
                    double upRowSortOrder = Convert.ToDouble(rowUp["OrignalSortOrder"]);
                    string upRowStopId = Convert.ToString(rowUp["StopId"]);
                    string upRowOperationId = Convert.ToString(rowUp["OperationId"]);



                    StubOperationListStop selectedOperation =
                        _listOperation.SingleOrDefault(
                            x =>
                            x.OperationId == _selectedOperationId && x.StopId == _selectedStopId);
                    StubOperationListStop upOperation =
                        _listOperation.SingleOrDefault(
                            x =>
                            x.OperationId == upRowOperationId && x.StopId == upRowStopId);

                    //rowUp["SortOrder"] = row["SortOrder"];
                    //row["SortOrder"] = rowUpSortOrder;


                    if (selectedOperation != null && upOperation != null)
                    {
                        if (selectedOperation.StopType == StopType.OperationListOperation && upOperation.StopType == StopType.OperationListOperation)
                        {
                            selectedOperation.OrignalSortOrder = upRowSortOrder;
                            upOperation.OrignalSortOrder = selectedSortOrder;
                        }
                        else if (selectedOperation.StopType == StopType.LoadListOperation && upOperation.StopType == StopType.OperationListOperation)
                        {
                            //set above..
                            StubOperationListStop previousUpOperationListOperation =
                                GetOperationListPreviousOperation(upOperation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(previousUpOperationListOperation, upOperation, selectedOperation,
                                                           2.0, SortButtonType.Up);

                            // below operation of moved operation...
                            StubOperationListStop nextUpOperationListOperation =
                               GetOperationListNextOperation(upOperation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(upOperation, nextUpOperationListOperation, null, 1.0, SortButtonType.Up);


                        }
                        else if (selectedOperation.StopType == StopType.OperationListOperation && upOperation.StopType == StopType.LoadListOperation)
                        {
                            //set below..
                            StubOperationListStop nextUpOperationListOperation =
                                GetOperationListNextOperation(selectedOperation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(selectedOperation, nextUpOperationListOperation, upOperation,
                                                           2.0, SortButtonType.Up);

                            //set above operation...
                            StubOperationListStop previousUpOperationListOperation =
                                GetOperationListPreviousOperation(selectedOperation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(previousUpOperationListOperation, selectedOperation, null,
                                                           1.0, SortButtonType.Up);


                        }
                        else
                        {
                            selectedOperation.OrignalSortOrder = upRowSortOrder;
                            upOperation.OrignalSortOrder = selectedSortOrder;
                        }

                    }

                    BindOperations(_listOperation);*/
                }
            }
        }

        /// <summary>
        /// This event runs when user clicks on scan button and send message to flow that user clicked on
        /// scan button and passes parameters that are required by flow to handle the click of scan button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDownClick(object sender, EventArgs e)
        {
            if (listOperations.SelectedRow != null)
            {
                Row row = listOperations.SelectedRow;
                //MessageBox.Show(row.Index.ToString());

                if (row.Index < listOperations.DataRows.Count - 1)
                {
                    SortStops(row, SortButtonType.Down);
                }
            }
        }

        private void SortStops(Row row, SortButtonType sortButtonType)
        {
            // below operation of moved operation...

            _selectedStopId = Convert.ToString(row["StopId"]);
            double selectedSortOrder = Convert.ToDouble(row["OrignalSortOrder"]);
            int selectedDisplaySortOrder = Convert.ToInt32(row["DisplaySortOrder"]);
            
            int rowIndex = sortButtonType == SortButtonType.Down ? row.Index + 1 : row.Index - 1;
            Row rowDown = listOperations.DataRows[rowIndex];
            double rowSortOrder = Convert.ToDouble(rowDown["OrignalSortOrder"]);
            string rowStopId = Convert.ToString(rowDown["StopId"]);
            int displaySortOrder = Convert.ToInt32(rowDown["DisplaySortOrder"]);

            SortStops selectedOperation =
                _listOperation.SingleOrDefault(
                    x => x.StopId == _selectedStopId);
            SortStops operation =
                _listOperation.SingleOrDefault(
                    x => x.StopId == rowStopId);

            //rowUp["SortOrder"] = row["SortOrder"];
            //row["SortOrder"] = rowUpSortOrder;


            if (selectedOperation != null && operation != null)
            {
                selectedOperation.DisplaySortOrder = displaySortOrder;
                operation.DisplaySortOrder = selectedDisplaySortOrder;

                if (selectedOperation.StopType == StopType.OperationListOperation &&
                    operation.StopType == StopType.OperationListOperation)
                {
                    selectedOperation.OrignalSortOrder = rowSortOrder;
                    operation.OrignalSortOrder = selectedSortOrder;

                }
                else
                {
                    SortStops otherOperationListOperation;
                    SortStops otherOperationListOperation1;
                    if (selectedOperation.StopType == StopType.LoadListOperation &&
                        operation.StopType == StopType.OperationListOperation)
                    {
                        if (sortButtonType == SortButtonType.Down)
                        {
                            otherOperationListOperation =
                                GetOperationListNextOperation(operation.OrignalSortOrder);
                            SetValuesOnBetweenOperation(operation, otherOperationListOperation, selectedOperation, 2.0,
                                                        sortButtonType);

                            otherOperationListOperation1 =
                               GetOperationListPreviousOperation(operation.OrignalSortOrder);
                            SetValuesOnBetweenOperation(otherOperationListOperation1, operation,
                                                        null,
                                                        1.0, sortButtonType);
                        }
                        else
                        {
                            //set above..
                            otherOperationListOperation =
                                GetOperationListPreviousOperation(operation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(otherOperationListOperation, operation, selectedOperation,
                                                           2.0, SortButtonType.Up);

                            // below operation of moved operation...
                            otherOperationListOperation1 =
                               GetOperationListNextOperation(operation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(operation, otherOperationListOperation1, null, 1.0, SortButtonType.Up);
                        }
                    }
                    else if (selectedOperation.StopType == StopType.OperationListOperation &&
                             operation.StopType == StopType.LoadListOperation)
                    {


                        if (sortButtonType == SortButtonType.Down)
                        {
                            otherOperationListOperation =
                                GetOperationListPreviousOperation(selectedOperation.OrignalSortOrder);
                            SetValuesOnBetweenOperation(otherOperationListOperation, selectedOperation,
                                                        operation,
                                                        2.0, sortButtonType);

                            otherOperationListOperation1 =
                               GetOperationListNextOperation(selectedOperation.OrignalSortOrder);
                            SetValuesOnBetweenOperation(selectedOperation, otherOperationListOperation1,
                                                        null,
                                                        1.0, sortButtonType);

                        }
                        else
                        {
                            //set below..
                            otherOperationListOperation =
                                GetOperationListNextOperation(selectedOperation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(selectedOperation, otherOperationListOperation, operation,
                                                           2.0, sortButtonType);

                            //set above operation...
                            otherOperationListOperation1 =
                                GetOperationListPreviousOperation(selectedOperation.OrignalSortOrder);


                            SetValuesOnBetweenOperation(otherOperationListOperation1, selectedOperation, null,
                                                           1.0, sortButtonType);

                        }
                    }
                    else
                    {
                        selectedOperation.OrignalSortOrder = rowSortOrder;
                        operation.OrignalSortOrder = selectedSortOrder;
                    }
                }
            }

            BindOperations();
            //ResumeStopSelection();
        }

        private void ResumeStopSelection()
        {
            if (string.IsNullOrEmpty(_selectedStopId) == false)
                foreach (Row row in listOperations.DataRows)
                {
                    if (Convert.ToString(row["StopId"]) == _selectedStopId)
                    {
                        row.Selected = true;
                        break;
                    }
                }
        }



        #endregion

        private void CreateDummyData()
        {
            const int tempOrder = 1;
            var op1 = new SortStops
                          {

                              StopSign = Convert.ToString((int)(OperationType.Delivery)),
                              StopAddress = "OperationId1 Stop1 trip1",
                              StopId = "Stop1",
                              DisplaySortOrder = tempOrder,
                              OrignalSortOrder = 1,
                              StopType = StopType.OperationListOperation
                          };

            var op2 = new SortStops
            {
                StopSign = Convert.ToString((int)(OperationType.Delivery)),
                StopAddress = "OperationId2 Stop2 trip2",
                StopId = "Stop2",
                DisplaySortOrder = tempOrder + 1,
                OrignalSortOrder = 2,
                StopType = StopType.OperationListOperation
            };

            var op3 = new SortStops
            {
                StopSign = Convert.ToString((int)(OperationType.Delivery)),
                StopAddress = "OperationId3 Stop3 trip3",
                StopId = "Stop3",
                DisplaySortOrder = tempOrder + 2,
                OrignalSortOrder = 3,
                StopType = StopType.OperationListOperation
            };

            var op4 = new SortStops
            {

                StopSign = Convert.ToString((int)(OperationType.Delivery)),
                StopAddress = "OperationId4 Stop4 trip4",
                StopId = "Stop4",
                DisplaySortOrder = tempOrder + 3,
                OrignalSortOrder = 999,
                StopType = StopType.LoadListOperation
            };
            var op5 = new SortStops
            {

                StopSign = Convert.ToString((int)(OperationType.Delivery)),
                StopAddress = "OperationId5 Stop5 trip5",
                StopId = "Stop5",
                DisplaySortOrder = tempOrder + 4,
                OrignalSortOrder = 999,
                StopType = StopType.LoadListOperation
            };
            _listOperation = new List<SortStops> { op1, op2, op3, op4, op5 };
            BindOperations();
        }

        private void pnlInfoCheckId12_GotFocus(object sender, EventArgs e)
        {

        }

        private void BtnPopUpOkClick(object sender, EventArgs e)
        {
            SetControlStatus(false);
        }
        private void SetControlStatus(bool isShowPopup)
        {
            //_tabMultipleButtons.Enabled = !isShowPopup;
            panelShowSortingState.Visible = isShowPopup;
            //listOperations.Enabled = !isShowPopup;
            if (isShowPopup)
                panelShowSortingState.BringToFront();
            else
                panelShowSortingState.SendToBack();
        }
       
    }
    public enum SortButtonType
    {
        Up,
        Down
    }

    public enum OperationType
    {
        Loading = 0,
        Unloading = 1,
        Delivery = 2,
        LoadLineHaulTruck = 3,
        LoadingDistributionTruck = 4,
        UnloadLineHaulTruck = 5,
        UnloadPickUpTruck = 6,
        Unknown = 7
    }

    public enum OperationStatus
    {
        New = 0,
        Started = 1,
        Finished = 2
    }

    public enum StopType
    {
        OperationListOperation,
        LoadListOperation,
    }
    public class SortStops
    {
        public string StopId { get; set; }
        public string StopAddress { get; set; }
        public string StopSign { get; set; }
        public bool IsLoadListStop { get; set; }
        public int DisplaySortOrder { get; set; }
        public double OrignalSortOrder { get; set; }
        public StopType StopType { get; set; }
    }


}
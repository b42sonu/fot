﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using PreCom.Controls;
using System.Windows.Forms;

namespace SampleScreenDesigner.Forms
{

    partial class FormSelectAction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageControlBox = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblReason = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReasonValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtAction = new PreCom.Controls.PreComInput2();
            this.listActions = new System.Windows.Forms.ListBox();
            this.lblAction = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).BeginInit();
            this.SuspendLayout();
            // 
            // messageControlBox
            // 
            this.messageControlBox.Location = new System.Drawing.Point(6, 350);
            this.messageControlBox.Name = "messageControlBox";
            this.messageControlBox.Size = new System.Drawing.Size(468, 123);
            this.messageControlBox.TabIndex = 1;
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.lblReason);
            this.touchPanel.Controls.Add(this.lblReasonValue);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtAction);
            this.touchPanel.Controls.Add(this.listActions);
            this.touchPanel.Controls.Add(this.lblAction);
            this.touchPanel.Controls.Add(this.messageControlBox);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblReason
            // 
            this.lblReason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblReason.Location = new System.Drawing.Point(11, 60);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(0, 0);
            // 
            // lblReasonValue
            // 
            this.lblReasonValue.AutoSize = false;
            this.lblReasonValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblReasonValue.Location = new System.Drawing.Point(11, 90);
            this.lblReasonValue.Name = "lblReasonValue";
            this.lblReasonValue.Size = new System.Drawing.Size(445, 60);
            // 
            // lblHeading
            // 
            this.lblHeading.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblHeading.Location = new System.Drawing.Point(11, 422);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(446, 60);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // txtAction
            // 
            this.txtAction.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtAction.Location = new System.Drawing.Point(11, 190);
            this.txtAction.Name = "txtAction";
            this.txtAction.Size = new System.Drawing.Size(436, 70);
            this.txtAction.TabIndex = 0;
            this.txtAction.TextTranslation = false;
            // 
            // listActions
            // 
            this.listActions.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.listActions.Location = new System.Drawing.Point(11, 230);
            this.listActions.Name = "listActions";
            this.listActions.Size = new System.Drawing.Size(460, 198);
            this.listActions.TabIndex = 4;
            // 
            // lblAction
            // 
            this.lblAction.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblAction.Location = new System.Drawing.Point(11, 155);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(0, 0);
            // 
            // FormSelectAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormSelectAction";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblReason;
        private Resco.Controls.CommonControls.TransparentLabel lblReasonValue;

        private Resco.Controls.CommonControls.TransparentLabel lblAction;
        private ListBox listActions;
        private PreCom.Controls.PreComInput2 txtAction;
        private MessageControl messageControlBox;
    }
}
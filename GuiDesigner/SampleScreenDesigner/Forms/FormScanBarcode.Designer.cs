﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    partial class FormScanBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editConsignmentNumber = new PreCom.Controls.PreComInput2();
            this.pnlConfirmMessage = new System.Windows.Forms.Panel();
            this.btnUnload = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.btnLoad = new Resco.Controls.OutlookControls.ImageButton();
            this.lblConfirmInfo = new System.Windows.Forms.Label();
            this.lblConfirmInfoHeading = new System.Windows.Forms.Label();
            this.picConfirmInfo = new System.Windows.Forms.PictureBox();
            this.lblNewCarrier = new System.Windows.Forms.Label();
            this.txtNewCarrier = new System.Windows.Forms.TextBox();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelItemCounter = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonBlankTopRight = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonReconcilliation = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonAttemptDelivery = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonAttemptPickup = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonBlankTopLeft = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonBlankBottomMiddle = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonChangeCarrier = new Resco.Controls.OutlookControls.ImageButton();
            this._messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.pnlConfirmMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnUnload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelItemCounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlankTopRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonReconcilliation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAttemptDelivery)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAttemptPickup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlankTopLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlankBottomMiddle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonChangeCarrier)).BeginInit();
            this.SuspendLayout();
            // 
            // editConsignmentNumber
            // 
            this.editConsignmentNumber.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.editConsignmentNumber.Location = new System.Drawing.Point(22, 78);
            this.editConsignmentNumber.Name = "editConsignmentNumber";
            this.editConsignmentNumber.Size = new System.Drawing.Size(436, 48);
            this.editConsignmentNumber.TabIndex = 23;
            this.editConsignmentNumber.TextTranslation = false;
            // 
            // pnlConfirmMessage
            // 
            this.pnlConfirmMessage.BackColor = System.Drawing.Color.White;
            this.pnlConfirmMessage.Controls.Add(this.btnUnload);
            this.pnlConfirmMessage.Controls.Add(this.btnCancel);
            this.pnlConfirmMessage.Controls.Add(this.btnLoad);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfoHeading);
            this.pnlConfirmMessage.Controls.Add(this.picConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblNewCarrier);
            this.pnlConfirmMessage.Controls.Add(this.txtNewCarrier);
            this.pnlConfirmMessage.Location = new System.Drawing.Point(20, 20);
            this.pnlConfirmMessage.Name = "pnlConfirmMessage";
            this.pnlConfirmMessage.Size = new System.Drawing.Size(440, 380);
            this.pnlConfirmMessage.Visible = false;
            this.pnlConfirmMessage.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlConfirmMessagePaint);
            // 
            // btnUnload
            // 
            this.btnUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnUnload.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnUnload.ForeColor = System.Drawing.Color.White;
            this.btnUnload.Location = new System.Drawing.Point(310, 300);
            this.btnUnload.Name = "btnUnload";
            this.btnUnload.Size = new System.Drawing.Size(100, 50);
            this.btnUnload.TabIndex = 30;
            this.btnUnload.Text = "Unload";
            this.btnUnload.Click += new System.EventHandler(this.BtnUnloadClick);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(20, 300);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 50);
            this.btnCancel.TabIndex = 30;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // btnLoad
            // 
            this.btnLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnLoad.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnLoad.ForeColor = System.Drawing.Color.White;
            this.btnLoad.Location = new System.Drawing.Point(140, 300);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(150, 50);
            this.btnLoad.TabIndex = 30;
            this.btnLoad.Text = "Load";
            this.btnLoad.Click += new System.EventHandler(this.BtnLoadClick);
            // 
            // lblConfirmInfo
            // 
            this.lblConfirmInfo.Location = new System.Drawing.Point(25, 130);
            this.lblConfirmInfo.Name = "lblConfirmInfo";
            this.lblConfirmInfo.Size = new System.Drawing.Size(370, 60);
            this.lblConfirmInfo.Text = "Are new carrier and select operation type you wish to start.";
            // 
            // lblConfirmInfoHeading
            // 
            this.lblConfirmInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblConfirmInfoHeading.Location = new System.Drawing.Point(25, 100);
            this.lblConfirmInfoHeading.Name = "lblConfirmInfoHeading";
            this.lblConfirmInfoHeading.Size = new System.Drawing.Size(236, 30);
            this.lblConfirmInfoHeading.Text = "New Carrier";
            // 
            // picConfirmInfo
            // 
            this.picConfirmInfo.Location = new System.Drawing.Point(195, 20);
            this.picConfirmInfo.Name = "picConfirmInfo";
            this.picConfirmInfo.Size = new System.Drawing.Size(50, 50);
            this.picConfirmInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblNewCarrier
            // 
            this.lblNewCarrier.Location = new System.Drawing.Point(25, 220);
            this.lblNewCarrier.Name = "lblNewCarrier";
            this.lblNewCarrier.Size = new System.Drawing.Size(150, 30);
            this.lblNewCarrier.Text = "New Carrier:";
            // 
            // txtNewCarrier
            // 
            this.txtNewCarrier.Location = new System.Drawing.Point(200, 210);
            this.txtNewCarrier.Name = "txtNewCarrier";
            this.txtNewCarrier.Size = new System.Drawing.Size(200, 41);
            this.txtNewCarrier.TabIndex = 35;
            // 
            // labelTask
            // 
            this.labelTask.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelTask.Location = new System.Drawing.Point(22, 46);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(375, 27);
            this.labelTask.Text = "Scan or enter consignment number";
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelTask);
            this.touchPanel.Controls.Add(this.labelItemCounter);
            this.touchPanel.Controls.Add(this.buttonBlankTopRight);
            this.touchPanel.Controls.Add(this.buttonReconcilliation);
            this.touchPanel.Controls.Add(this.buttonAttemptDelivery);
            this.touchPanel.Controls.Add(this.buttonAttemptPickup);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.buttonCancel);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.buttonBlankTopLeft);
            this.touchPanel.Controls.Add(this.buttonBlankBottomMiddle);
            this.touchPanel.Controls.Add(this.buttonChangeCarrier);
            this.touchPanel.Controls.Add(this.pnlConfirmMessage);
            this.touchPanel.Controls.Add(this.editConsignmentNumber);
            this.touchPanel.Controls.Add(this._messageControl);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelItemCounter
            // 
            this.labelItemCounter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelItemCounter.Location = new System.Drawing.Point(410, 46);
            this.labelItemCounter.Name = "labelItemCounter";
            this.labelItemCounter.Size = new System.Drawing.Size(0, 0);
            this.labelItemCounter.Visible = false;
            // 
            // buttonBlankTopRight
            // 
            this.buttonBlankTopRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBlankTopRight.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBlankTopRight.ForeColor = System.Drawing.Color.White;
            this.buttonBlankTopRight.Location = new System.Drawing.Point(319, 429);
            this.buttonBlankTopRight.Name = "buttonBlankTopRight";
            this.buttonBlankTopRight.Size = new System.Drawing.Size(158, 50);
            this.buttonBlankTopRight.TabIndex = 0;
            this.buttonBlankTopRight.Visible = false;
            // 
            // buttonReconcilliation
            // 
            this.buttonReconcilliation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonReconcilliation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonReconcilliation.ForeColor = System.Drawing.Color.White;
            this.buttonReconcilliation.Location = new System.Drawing.Point(160, 429);
            this.buttonReconcilliation.Name = "buttonReconcilliation";
            this.buttonReconcilliation.Size = new System.Drawing.Size(158, 50);
            this.buttonReconcilliation.TabIndex = 45;
            this.buttonReconcilliation.Text = "Reconcilliation";
            this.buttonReconcilliation.Click += new System.EventHandler(this.ButtonReconcilliation);
            // 
            // buttonAttemptDelivery
            // 
            this.buttonAttemptDelivery.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonAttemptDelivery.Enabled = false;
            this.buttonAttemptDelivery.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.buttonAttemptDelivery.ForeColor = System.Drawing.Color.White;
            this.buttonAttemptDelivery.Location = new System.Drawing.Point(1, 429);
            this.buttonAttemptDelivery.Name = "buttonAttemptDelivery";
            this.buttonAttemptDelivery.Size = new System.Drawing.Size(158, 50);
            this.buttonAttemptDelivery.TabIndex = 41;
            this.buttonAttemptDelivery.Text = "Attempted\r\nDel.";
            this.buttonAttemptDelivery.Visible = false;
            this.buttonAttemptDelivery.Click += new System.EventHandler(this.ButtonAttemptedDeliveryClick);
            // 
            // buttonAttemptPickup
            // 
            this.buttonAttemptPickup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonAttemptPickup.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.buttonAttemptPickup.ForeColor = System.Drawing.Color.White;
            this.buttonAttemptPickup.Location = new System.Drawing.Point(1, 429);
            this.buttonAttemptPickup.Name = "buttonAttemptPickup";
            this.buttonAttemptPickup.Size = new System.Drawing.Size(158, 50);
            this.buttonAttemptPickup.TabIndex = 41;
            this.buttonAttemptPickup.Text = "Attempted \r\npickup";
            this.buttonAttemptPickup.Click += new System.EventHandler(this.ButtonAttemptedPickupClick);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Pickup";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(1, 483);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(158, 50);
            this.buttonCancel.TabIndex = 27;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancelClick);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(319, 483);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(158, 50);
            this.buttonOk.TabIndex = 26;
            this.buttonOk.Text = "Ok/Enter";
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // buttonBlankTopLeft
            // 
            this.buttonBlankTopLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBlankTopLeft.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBlankTopLeft.ForeColor = System.Drawing.Color.White;
            this.buttonBlankTopLeft.Location = new System.Drawing.Point(1, 429);
            this.buttonBlankTopLeft.Name = "buttonBlankTopLeft";
            this.buttonBlankTopLeft.Size = new System.Drawing.Size(158, 50);
            this.buttonBlankTopLeft.TabIndex = 49;
            this.buttonBlankTopLeft.Visible = false;
            // 
            // buttonBlankBottomMiddle
            // 
            this.buttonBlankBottomMiddle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBlankBottomMiddle.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBlankBottomMiddle.ForeColor = System.Drawing.Color.White;
            this.buttonBlankBottomMiddle.Location = new System.Drawing.Point(160, 483);
            this.buttonBlankBottomMiddle.Name = "buttonBlankBottomMiddle";
            this.buttonBlankBottomMiddle.Size = new System.Drawing.Size(158, 50);
            this.buttonBlankBottomMiddle.TabIndex = 50;
            // 
            // buttonChangeCarrier
            // 
            this.buttonChangeCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonChangeCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonChangeCarrier.ForeColor = System.Drawing.Color.White;
            this.buttonChangeCarrier.Location = new System.Drawing.Point(160, 483);
            this.buttonChangeCarrier.Name = "buttonChangeCarrier";
            this.buttonChangeCarrier.Size = new System.Drawing.Size(158, 50);
            this.buttonChangeCarrier.TabIndex = 27;
            this.buttonChangeCarrier.Text = "Chg.Carrier";
            this.buttonChangeCarrier.Click += new System.EventHandler(this.ButtonChangeCarrierClick);
            // 
            // _messageControl
            // 
            this._messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._messageControl.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this._messageControl.Location = new System.Drawing.Point(22, 165);
            this._messageControl.Name = "_messageControl";
            this._messageControl.Size = new System.Drawing.Size(436, 120);
            this._messageControl.TabIndex = 3;
            // 
            // FormScanBarcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormScanBarcode";
            this.pnlConfirmMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnUnload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelItemCounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlankTopRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonReconcilliation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAttemptDelivery)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonAttemptPickup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlankTopLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBlankBottomMiddle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonChangeCarrier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MessageControl _messageControl;
        private PreCom.Controls.PreComInput2 editConsignmentNumber;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private Resco.Controls.CommonControls.TransparentLabel labelItemCounter;
        private Resco.Controls.OutlookControls.ImageButton buttonCancel;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.OutlookControls.ImageButton buttonBlankTopRight;
        private Resco.Controls.OutlookControls.ImageButton buttonBlankTopLeft;
        private Resco.Controls.OutlookControls.ImageButton buttonBlankBottomMiddle;
        private Resco.Controls.OutlookControls.ImageButton buttonChangeCarrier;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton buttonAttemptPickup;
        private Resco.Controls.OutlookControls.ImageButton buttonAttemptDelivery;
        private Resco.Controls.OutlookControls.ImageButton buttonReconcilliation;

        #region Work List Pop related

        private Panel pnlConfirmMessage;
        private PictureBox picConfirmInfo;
        private Label lblConfirmInfo;
        private Label lblConfirmInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnUnload;
        private Resco.Controls.OutlookControls.ImageButton btnCancel;
        private Resco.Controls.OutlookControls.ImageButton btnLoad;
        private Label lblNewCarrier;
        private TextBox txtNewCarrier;

        #endregion


    }
}
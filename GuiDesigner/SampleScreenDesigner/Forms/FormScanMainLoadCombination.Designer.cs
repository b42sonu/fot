﻿
using System;
namespace SampleScreenDesigner.Forms
{
    partial class FormScanMainLoadCombination
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPrinterNameValue = new System.Windows.Forms.Label();
            this.lblPrinterNameText = new System.Windows.Forms.Label();
            this.btnNewTrip = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCarrierList = new Resco.Controls.OutlookControls.ImageButton();
            this.btnReconcillation = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDeviation = new Resco.Controls.OutlookControls.ImageButton();
            this.txtScannedNumber = new PreCom.Controls.PreComInput2();
            this.lblScan = new System.Windows.Forms.Label();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblHeading = new System.Windows.Forms.Label();
            this.lblModuleName = new System.Windows.Forms.Label();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOkEnter = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBlank = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewTrip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCarrierList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReconcillation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeviation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOkEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.label1);
            this.touchPanel.Controls.Add(this.label2);
            this.touchPanel.Controls.Add(this.lblPrinterNameValue);
            this.touchPanel.Controls.Add(this.lblPrinterNameText);
            this.touchPanel.Controls.Add(this.btnNewTrip);
            this.touchPanel.Controls.Add(this.btnCarrierList);
            this.touchPanel.Controls.Add(this.btnReconcillation);
            this.touchPanel.Controls.Add(this.btnDeviation);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.lblScan);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnOkEnter);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Location = new System.Drawing.Point(265, 395);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 31);
            this.label1.Text = "0 kg";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Location = new System.Drawing.Point(178, 396);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 32);
            this.label2.Text = "Weight:";
            // 
            // lblPrinterNameValue
            // 
            this.lblPrinterNameValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPrinterNameValue.Location = new System.Drawing.Point(91, 397);
            this.lblPrinterNameValue.Name = "lblPrinterNameValue";
            this.lblPrinterNameValue.Size = new System.Drawing.Size(81, 26);
            this.lblPrinterNameValue.Text = "0 Item";
            // 
            // lblPrinterNameText
            // 
            this.lblPrinterNameText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblPrinterNameText.Location = new System.Drawing.Point(1, 396);
            this.lblPrinterNameText.Name = "lblPrinterNameText";
            this.lblPrinterNameText.Size = new System.Drawing.Size(98, 28);
            this.lblPrinterNameText.Text = "Loaded:";
            // 
            // btnNewTrip
            // 
            this.btnNewTrip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewTrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnNewTrip.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnNewTrip.ForeColor = System.Drawing.Color.White;
            this.btnNewTrip.Location = new System.Drawing.Point(161, 429);
            this.btnNewTrip.Name = "btnNewTrip";
            this.btnNewTrip.Size = new System.Drawing.Size(158, 50);
            this.btnNewTrip.TabIndex = 43;
            this.btnNewTrip.Text = "New Trip";
            // 
            // btnCarrierList
            // 
            this.btnCarrierList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCarrierList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCarrierList.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCarrierList.ForeColor = System.Drawing.Color.White;
            this.btnCarrierList.Location = new System.Drawing.Point(0, 429);
            this.btnCarrierList.Name = "btnCarrierList";
            this.btnCarrierList.Size = new System.Drawing.Size(158, 50);
            this.btnCarrierList.TabIndex = 41;
            this.btnCarrierList.Text = "Carr.List";
            // 
            // btnReconcillation
            // 
            this.btnReconcillation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReconcillation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnReconcillation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnReconcillation.ForeColor = System.Drawing.Color.White;
            this.btnReconcillation.Location = new System.Drawing.Point(322, 429);
            this.btnReconcillation.Name = "btnReconcillation";
            this.btnReconcillation.Size = new System.Drawing.Size(158, 50);
            this.btnReconcillation.TabIndex = 42;
            this.btnReconcillation.Text = "Reconcillation";
            // 
            // btnDeviation
            // 
            this.btnDeviation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDeviation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDeviation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDeviation.ForeColor = System.Drawing.Color.White;
            this.btnDeviation.Location = new System.Drawing.Point(161, 483);
            this.btnDeviation.Name = "btnDeviation";
            this.btnDeviation.Size = new System.Drawing.Size(158, 50);
            this.btnDeviation.TabIndex = 36;
            this.btnDeviation.Text = "Deviation";
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(15, 122);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(452, 48);
            this.txtScannedNumber.TabIndex = 24;
            this.txtScannedNumber.TextTranslation = false;
            // 
            // lblScan
            // 
            this.lblScan.Location = new System.Drawing.Point(15, 90);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(452, 30);
            this.lblScan.Text = "Scan item or new active trip";
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(15, 179);
            this.MsgMessage.MessageText = "";
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(452, 97);
            this.MsgMessage.TabIndex = 14;
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(15, 56);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(462, 34);
            this.lblHeading.Text = "OP DRAMMEN TERMINAL GODS";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(127, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(232, 50);
            this.lblModuleName.Text = "Load Comb. trip";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 6;
            this.btnBack.Text = "Back";
            // 
            // btnOkEnter
            // 
            this.btnOkEnter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOkEnter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOkEnter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOkEnter.ForeColor = System.Drawing.Color.White;
            this.btnOkEnter.Location = new System.Drawing.Point(322, 483);
            this.btnOkEnter.Name = "btnOkEnter";
            this.btnOkEnter.Size = new System.Drawing.Size(158, 50);
            this.btnOkEnter.TabIndex = 7;
            this.btnOkEnter.Text = "OK/Enter";
            this.btnOkEnter.Click += new System.EventHandler(this.btnOkEnter_Click);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // btnBlank
            // 
            this.btnBlank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBlank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBlank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBlank.ForeColor = System.Drawing.Color.White;
            this.btnBlank.Location = new System.Drawing.Point(161, 483);
            this.btnBlank.Name = "btnBlank";
            this.btnBlank.Size = new System.Drawing.Size(158, 50);
            this.btnBlank.TabIndex = 8;
            // 
            // FormScanMainLoadCombination
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Controls.Add(this.btnBlank);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormScanMainLoadCombination";
            this.Text = "FormPrinterInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnNewTrip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCarrierList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReconcillation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeviation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOkEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 txtScannedNumber;
        private System.Windows.Forms.Label lblScan;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl MsgMessage;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOkEnter;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnBlank;
        private Resco.Controls.OutlookControls.ImageButton btnDeviation;
        private Resco.Controls.OutlookControls.ImageButton btnNewTrip;
        private Resco.Controls.OutlookControls.ImageButton btnCarrierList;
        private Resco.Controls.OutlookControls.ImageButton btnReconcillation;
        private System.Windows.Forms.Label lblPrinterNameValue;
        private System.Windows.Forms.Label lblPrinterNameText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}
﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace SampleScreenDesigner.Forms
{
    partial class FormValidationError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.buttonTryAgain = new Resco.Controls.OutlookControls.ImageButton();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelDeliveryInformation = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.labelComment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonTryAgain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDeliveryInformation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.buttonTryAgain);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.labelDeliveryInformation);
            this.touchPanel.Controls.Add(this.labelCause);
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.labelComment);
            this.touchPanel.Controls.Add(this.labelMeasure);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // buttonTryAgain
            // 
            this.buttonTryAgain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonTryAgain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonTryAgain.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonTryAgain.ForeColor = System.Drawing.Color.White;
            this.buttonTryAgain.Location = new System.Drawing.Point(161, 483);
            this.buttonTryAgain.Name = "buttonTryAgain";
            this.buttonTryAgain.Size = new System.Drawing.Size(158, 50);
            this.buttonTryAgain.TabIndex = 54;
            this.buttonTryAgain.Text = "Try Again";
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(150, 10);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(98, 17);
            this.lblModuleName.Text = "Load Line haul";
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.lblHeading.Location = new System.Drawing.Point(50, 58);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(203, 15);
            this.lblHeading.Text = "OP DROMMEN TERMINAL GOD ";
            this.lblHeading.ParentChanged += new System.EventHandler(this.lblHeading_ParentChanged);
            // 
            // labelCause
            // 
            this.labelCause.Location = new System.Drawing.Point(0, 0);
            this.labelCause.Name = "labelCause";
            this.labelCause.Size = new System.Drawing.Size(0, 0);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(322, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(0, 483);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(158, 50);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "Ok";
            // 
            // labelComment
            // 
            this.labelComment.Location = new System.Drawing.Point(0, 0);
            this.labelComment.Name = "labelComment";
            this.labelComment.Size = new System.Drawing.Size(0, 0);
            // 
            // labelMeasure
            // 
            this.labelMeasure.Location = new System.Drawing.Point(0, 0);
            this.labelMeasure.Name = "labelMeasure";
            this.labelMeasure.Size = new System.Drawing.Size(0, 0);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(22, 96);
            this.messageControl.MessageText = "";
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(436, 120);
            this.messageControl.TabIndex = 32;
            // 
            // FormValidationError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormValidationError";
            this.Text = "FormValidationError";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonTryAgain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDeliveryInformation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelMeasure;
        private Resco.Controls.CommonControls.TransparentLabel labelComment;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private Resco.Controls.CommonControls.TransparentLabel labelDeliveryInformation;
        private Resco.Controls.CommonControls.TransparentLabel labelCause;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Resco.Controls.OutlookControls.ImageButton buttonTryAgain;
    }
}
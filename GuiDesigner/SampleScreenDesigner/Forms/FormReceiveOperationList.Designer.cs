﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    partial class FormReceiveOperationList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReceiveOperationList));
            this.advancedListOperationList = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplateStopPlus = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCell9 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell4 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell5 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedList.TextCell();
            this.ImageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.RowTemplateStopMinus = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.TextCell10 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell8 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell15 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell23 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCell12 = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCell1 = new Resco.Controls.AdvancedList.ImageCell();
            this.ImageList1 = new System.Windows.Forms.ImageList();
            this.textCell13 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell14 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell24 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCell17 = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCell4 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCell18 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell21 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell25 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplateForSingleOperation = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellSingleStopSign = new Resco.Controls.AdvancedList.ImageCell();
            this.textCell20 = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCell5 = new Resco.Controls.AdvancedList.ImageCell();
            this.textCell22 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell26 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell27 = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCell2 = new Resco.Controls.AdvancedList.ImageCell();
            this.ImageCell3 = new Resco.Controls.AdvancedList.ImageCell();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.btnInfo = new Resco.Controls.OutlookControls.ImageButton();
            this.btnReject = new Resco.Controls.OutlookControls.ImageButton();
            this.btnAccept = new Resco.Controls.OutlookControls.ImageButton();
            this.btnAcceptAll = new Resco.Controls.OutlookControls.ImageButton();
            this.panelBottomInfo = new System.Windows.Forms.Panel();
            this.lblDetailValue3 = new System.Windows.Forms.Label();
            this.lblDetailValue1 = new System.Windows.Forms.Label();
            this.lblDetailValue2 = new System.Windows.Forms.Label();
            this.lblDetailType3 = new System.Windows.Forms.Label();
            this.lblDetailType1 = new System.Windows.Forms.Label();
            this.lblDetailType2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAccept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAcceptAll)).BeginInit();
            this.panelBottomInfo.SuspendLayout();
            this.SuspendLayout();
            this.ImageListOperationListIcons.Images.Clear();

            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Finished());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.New());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Started());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.PlusLarge());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Unloading());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Loading());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Pickup());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.Delivery());
            this.ImageListOperationListIcons.Images.Add(GuiCommon.Instance.MinusLarge());

            // 
            // advancedListOperationList
            // 
            this.advancedListOperationList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListOperationList.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedListOperationList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListOperationList.DataRows.Clear();
            this.advancedListOperationList.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            resources.GetString("advancedListOperationList.HeaderRow")});
            this.advancedListOperationList.Location = new System.Drawing.Point(0, 0);
            this.advancedListOperationList.MultiSelect = true;
            this.advancedListOperationList.Name = "advancedListOperationList";
            this.advancedListOperationList.ScrollbarSmallChange = 32;
            this.advancedListOperationList.ScrollbarWidth = 26;
            this.advancedListOperationList.SelectedTemplateIndex = 2;
            this.advancedListOperationList.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.advancedListOperationList.ShowHeader = true;
            this.advancedListOperationList.Size = new System.Drawing.Size(480, 258);
            this.advancedListOperationList.TabIndex = 0;
            this.advancedListOperationList.TemplateIndex = 1;
            this.advancedListOperationList.Templates.Add(this.RowTemplateHeader);
            this.advancedListOperationList.Templates.Add(this.RowTemplateStopPlus);
            this.advancedListOperationList.Templates.Add(this.RowTemplateStopMinus);
            this.advancedListOperationList.Templates.Add(this.RowTemplatePlannedOp);
            this.advancedListOperationList.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.advancedListOperationList.Templates.Add(this.RowTemplateForSingleOperation);
            this.advancedListOperationList.TouchScrolling = true;
            this.advancedListOperationList.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.AdvancedListOperationListRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCell1);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCell1
            // 
            this.TextCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell1.CellSource.ConstantData = "Operation List";
            this.TextCell1.DesignName = "TextCell1";
            this.TextCell1.Location = new System.Drawing.Point(0, 0);
            this.TextCell1.Size = new System.Drawing.Size(-1, 32);
            this.TextCell1.Visible = false;
            // 
            // RowTemplateStopPlus
            // 
            this.RowTemplateStopPlus.CellTemplates.Add(this.ImageCellStopSign);
            this.RowTemplateStopPlus.CellTemplates.Add(this.TextCell9);
            this.RowTemplateStopPlus.CellTemplates.Add(this.TextCell4);
            this.RowTemplateStopPlus.CellTemplates.Add(this.textCell5);
            this.RowTemplateStopPlus.CellTemplates.Add(this.textCell6);
            this.RowTemplateStopPlus.Height = 64;
            this.RowTemplateStopPlus.Name = "RowTemplateStopPlus";
            // 
            // ImageCellStopSign
            // 
            this.ImageCellStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellStopSign.AutoResize = true;
            this.ImageCellStopSign.CellSource.ColumnName = "StopSign";
            this.ImageCellStopSign.DesignName = "ImageCellStopSign";
            this.ImageCellStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellStopSign.Location = new System.Drawing.Point(0, 0);
            this.ImageCellStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // TextCell9
            // 
            this.TextCell9.CellSource.ColumnName = "StopHeader";
            this.TextCell9.DesignName = "TextCell9";
            this.TextCell9.Location = new System.Drawing.Point(30, 0);
            this.TextCell9.Size = new System.Drawing.Size(400, 64);
            this.TextCell9.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // TextCell4
            // 
            this.TextCell4.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell4.CellSource.ColumnName = "StopId";
            this.TextCell4.DesignName = "TextCell4";
            this.TextCell4.Location = new System.Drawing.Point(0, 0);
            this.TextCell4.Size = new System.Drawing.Size(0, 64);
            this.TextCell4.Visible = false;
            // 
            // textCell5
            // 
            this.textCell5.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell5.CellSource.ColumnName = "OperationId";
            this.textCell5.DesignName = "textCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(0, 64);
            this.textCell5.Visible = false;
            // 
            // textCell6
            // 
            this.textCell6.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell6.CellSource.ColumnName = "OrderNumber";
            this.textCell6.DesignName = "textCell6";
            this.textCell6.Location = new System.Drawing.Point(0, 0);
            this.textCell6.Size = new System.Drawing.Size(0, 64);
            this.textCell6.Visible = false;
            // 
            // RowTemplateStopMinus
            // 
            this.RowTemplateStopMinus.BackColor = System.Drawing.Color.Gray;
            this.RowTemplateStopMinus.CellTemplates.Add(this.ImageCellAlternateStopSign);
            this.RowTemplateStopMinus.CellTemplates.Add(this.TextCell10);
            this.RowTemplateStopMinus.CellTemplates.Add(this.textCell8);
            this.RowTemplateStopMinus.CellTemplates.Add(this.TextCell15);
            this.RowTemplateStopMinus.CellTemplates.Add(this.TextCell23);
            this.RowTemplateStopMinus.Height = 64;
            this.RowTemplateStopMinus.Name = "RowTemplateStopMinus";
            // 
            // ImageCellAlternateStopSign
            // 
            this.ImageCellAlternateStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateStopSign.AutoResize = true;
            this.ImageCellAlternateStopSign.CellSource.ColumnName = "AlternateStopSign";
            this.ImageCellAlternateStopSign.DesignName = "ImageCellAlternateStopSign";
            this.ImageCellAlternateStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellAlternateStopSign.Location = new System.Drawing.Point(0, 0);
            this.ImageCellAlternateStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // TextCell10
            // 
            this.TextCell10.CellSource.ColumnName = "StopHeader";
            this.TextCell10.DesignName = "TextCell10";
            this.TextCell10.Location = new System.Drawing.Point(30, 0);
            this.TextCell10.Size = new System.Drawing.Size(400, 64);
            this.TextCell10.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // textCell8
            // 
            this.textCell8.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell8.CellSource.ColumnName = "StopId";
            this.textCell8.DesignName = "textCell8";
            this.textCell8.Location = new System.Drawing.Point(0, 0);
            this.textCell8.Size = new System.Drawing.Size(0, 64);
            this.textCell8.Visible = false;
            // 
            // TextCell15
            // 
            this.TextCell15.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell15.CellSource.ColumnName = "OperationId";
            this.TextCell15.DesignName = "TextCell15";
            this.TextCell15.Location = new System.Drawing.Point(0, 0);
            this.TextCell15.Size = new System.Drawing.Size(0, 64);
            this.TextCell15.Visible = false;
            // 
            // TextCell23
            // 
            this.TextCell23.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell23.CellSource.ColumnName = "OrderNumber";
            this.TextCell23.DesignName = "TextCell23";
            this.TextCell23.Location = new System.Drawing.Point(0, 0);
            this.TextCell23.Size = new System.Drawing.Size(0, 64);
            this.TextCell23.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationType);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCell12);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCell1);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCell13);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCell14);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.TextCell24);
            this.RowTemplatePlannedOp.Height = 64;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.ImageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationType.AutoResize = true;
            this.ImageCellOperationType.CellSource.ColumnName = "StopType";
            this.ImageCellOperationType.DesignName = "ImageCellOperationType";
            this.ImageCellOperationType.ImageList = this.ImageListOperationListIcons;
            this.ImageCellOperationType.Location = new System.Drawing.Point(30, 0);
            this.ImageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell12
            // 
            this.textCell12.CellSource.ColumnName = "OperationDetail";
            this.textCell12.DesignName = "textCell12";
            this.textCell12.Location = new System.Drawing.Point(60, 0);
            this.textCell12.Size = new System.Drawing.Size(400, 64);
            this.textCell12.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCell1
            // 
            this.ImageCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCell1.CellSource.ColumnName = "Status";
            this.ImageCell1.DesignName = "ImageCell1";
            this.ImageCell1.ImageList = this.ImageList1;
            this.ImageCell1.Location = new System.Drawing.Point(430, 0);
            this.ImageCell1.Size = new System.Drawing.Size(-1, 64);
            this.ImageList1.Images.Clear();
            //this.ImageList1.Images.Add(global::Com.Bring.PMP.PreComFW.Shared.Properties.Resources.FOT_icon_verified_ok);
            //this.ImageList1.Images.Add(global::Com.Bring.PMP.PreComFW.Shared.Properties.Resources.FOT_icon_error);
            //this.ImageList1.Images.Add(global::Com.Bring.PMP.PreComFW.Shared.Properties.Resources.FOT_icon_attention);
            // 
            // textCell13
            // 
            this.textCell13.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell13.CellSource.ColumnName = "OperationId";
            this.textCell13.DesignName = "textCell13";
            this.textCell13.Location = new System.Drawing.Point(0, 0);
            this.textCell13.Size = new System.Drawing.Size(0, 64);
            this.textCell13.Visible = false;
            // 
            // textCell14
            // 
            this.textCell14.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell14.CellSource.ColumnName = "StopId";
            this.textCell14.DesignName = "textCell14";
            this.textCell14.Location = new System.Drawing.Point(0, 0);
            this.textCell14.Size = new System.Drawing.Size(0, 64);
            this.textCell14.Visible = false;
            // 
            // TextCell24
            // 
            this.TextCell24.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell24.CellSource.ColumnName = "OrderNumber";
            this.TextCell24.DesignName = "TextCell24";
            this.TextCell24.Location = new System.Drawing.Point(0, 0);
            this.TextCell24.Size = new System.Drawing.Size(0, 64);
            this.TextCell24.Visible = false;
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternateOperationType);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCell17);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCell4);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCell18);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.TextCell21);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.TextCell25);
            this.RowTemplatePlannedOpAlternateTemp.Height = 64;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.ImageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateOperationType.AutoResize = true;
            this.ImageCellAlternateOperationType.CellSource.ColumnName = "StopType";
            this.ImageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.ImageCellAlternateOperationType.ImageList = this.ImageListOperationListIcons;
            this.ImageCellAlternateOperationType.Location = new System.Drawing.Point(30, 0);
            this.ImageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell17
            // 
            this.textCell17.CellSource.ColumnName = "OperationDetail";
            this.textCell17.DesignName = "textCell17";
            this.textCell17.Location = new System.Drawing.Point(60, 0);
            this.textCell17.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCell17.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCell17.Size = new System.Drawing.Size(400, 64);
            this.textCell17.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCell4
            // 
            this.ImageCell4.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCell4.CellSource.ColumnName = "Status";
            this.ImageCell4.DesignName = "ImageCell4";
            this.ImageCell4.ImageList = this.ImageList1;
            this.ImageCell4.Location = new System.Drawing.Point(430, 0);
            this.ImageCell4.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCell18
            // 
            this.textCell18.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell18.CellSource.ColumnName = "OperationId";
            this.textCell18.DesignName = "textCell18";
            this.textCell18.Location = new System.Drawing.Point(0, 0);
            this.textCell18.Size = new System.Drawing.Size(0, 64);
            this.textCell18.Visible = false;
            // 
            // TextCell21
            // 
            this.TextCell21.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell21.CellSource.ColumnName = "StopId";
            this.TextCell21.DesignName = "TextCell21";
            this.TextCell21.Location = new System.Drawing.Point(0, 0);
            this.TextCell21.Size = new System.Drawing.Size(0, 64);
            this.TextCell21.Visible = false;
            // 
            // TextCell25
            // 
            this.TextCell25.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCell25.CellSource.ColumnName = "OrderNumber";
            this.TextCell25.DesignName = "TextCell25";
            this.TextCell25.Location = new System.Drawing.Point(0, 0);
            this.TextCell25.Size = new System.Drawing.Size(0, 64);
            this.TextCell25.Visible = false;
            // 
            // RowTemplateForSingleOperation
            // 
            this.RowTemplateForSingleOperation.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.ImageCellSingleStopSign);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCell20);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.ImageCell5);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCell22);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCell26);
            this.RowTemplateForSingleOperation.CellTemplates.Add(this.textCell27);
            this.RowTemplateForSingleOperation.Height = 64;
            this.RowTemplateForSingleOperation.Name = "RowTemplateForSingleOperation";
            // 
            // ImageCellSingleStopSign
            // 
            this.ImageCellSingleStopSign.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellSingleStopSign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImageCellSingleStopSign.AutoResize = true;
            this.ImageCellSingleStopSign.CellSource.ColumnName = "StopSign";
            this.ImageCellSingleStopSign.DesignName = "ImageCellSingleStopSign";
            this.ImageCellSingleStopSign.ImageList = this.ImageListOperationListIcons;
            this.ImageCellSingleStopSign.Location = new System.Drawing.Point(0, 0);
            this.ImageCellSingleStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell20
            // 
            this.textCell20.CellSource.ColumnName = "StopHeader";
            this.textCell20.DesignName = "textCell20";
            this.textCell20.Location = new System.Drawing.Point(30, 0);
            this.textCell20.Size = new System.Drawing.Size(400, 64);
            this.textCell20.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // ImageCell5
            // 
            this.ImageCell5.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCell5.CellSource.ColumnName = "StopStatus";
            this.ImageCell5.DesignName = "ImageCell5";
            this.ImageCell5.ImageList = this.ImageList1;
            this.ImageCell5.Location = new System.Drawing.Point(430, 0);
            this.ImageCell5.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCell22
            // 
            this.textCell22.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell22.CellSource.ColumnName = "StopId";
            this.textCell22.DesignName = "textCell22";
            this.textCell22.Location = new System.Drawing.Point(0, 0);
            this.textCell22.Size = new System.Drawing.Size(0, 64);
            this.textCell22.Visible = false;
            // 
            // textCell26
            // 
            this.textCell26.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell26.CellSource.ColumnName = "OperationId";
            this.textCell26.DesignName = "textCell26";
            this.textCell26.Location = new System.Drawing.Point(0, 0);
            this.textCell26.Size = new System.Drawing.Size(0, 64);
            this.textCell26.Visible = false;
            // 
            // textCell27
            // 
            this.textCell27.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell27.CellSource.ColumnName = "OrderNumber";
            this.textCell27.DesignName = "textCell27";
            this.textCell27.Location = new System.Drawing.Point(0, 0);
            this.textCell27.Size = new System.Drawing.Size(0, 64);
            this.textCell27.Visible = false;
            // 
            // ImageCell2
            // 
            this.ImageCell2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCell2.CellSource.ColumnName = "StopStatus";
            this.ImageCell2.DesignName = "ImageCell2";
            this.ImageCell2.ImageList = this.ImageList1;
            this.ImageCell2.Location = new System.Drawing.Point(215, 0);
            this.ImageCell2.Size = new System.Drawing.Size(-1, 32);
            // 
            // ImageCell3
            // 
            this.ImageCell3.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCell3.CellSource.ColumnName = "StopStatus";
            this.ImageCell3.DesignName = "ImageCell3";
            this.ImageCell3.ImageList = this.ImageList1;
            this.ImageCell3.Location = new System.Drawing.Point(215, 0);
            this.ImageCell3.Size = new System.Drawing.Size(-1, 32);
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.panelBottomInfo);
            this.touchPanel.Controls.Add(this.btnInfo);
            this.touchPanel.Controls.Add(this.btnReject);
            this.touchPanel.Controls.Add(this.btnAccept);
            this.touchPanel.Controls.Add(this.btnAcceptAll);
            this.touchPanel.Controls.Add(this.advancedListOperationList);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);

            // 
            // panelBottomInfo
            // 
            this.panelBottomInfo.Controls.Add(this.lblDetailValue3);
            this.panelBottomInfo.Controls.Add(this.lblDetailValue1);
            this.panelBottomInfo.Controls.Add(this.lblDetailValue2);
            this.panelBottomInfo.Controls.Add(this.lblDetailType3);
            this.panelBottomInfo.Controls.Add(this.lblDetailType1);
            this.panelBottomInfo.Controls.Add(this.lblDetailType2);
            this.panelBottomInfo.Location = new System.Drawing.Point(0, 273);
            this.panelBottomInfo.Name = "panelBottomInfo";
            this.panelBottomInfo.Size = new System.Drawing.Size(480, 152);

            // 
            // lblDetailValue3
            // 
            this.lblDetailValue3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue3.Location = new System.Drawing.Point(170, 100);
            this.lblDetailValue3.Name = "lblDetailValue3";
            this.lblDetailValue3.Size = new System.Drawing.Size(305, 70);

            // 
            // lblDetailValue1
            // 
            this.lblDetailValue1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue1.Location = new System.Drawing.Point(170, 40);
            this.lblDetailValue1.Name = "lblDetailValue1";
            this.lblDetailValue1.Size = new System.Drawing.Size(305, 35);
            // 
            // lblDetailValue2
            // 
            this.lblDetailValue2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue2.Location = new System.Drawing.Point(170, 70);
            this.lblDetailValue2.Name = "lblDetailValue2";
            this.lblDetailValue2.Size = new System.Drawing.Size(305, 30);
            // 
            // lblDetailType3
            // 
            this.lblDetailType3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType3.Location = new System.Drawing.Point(3, 100);
            this.lblDetailType3.Name = "lblDetailType3";
            this.lblDetailType3.Size = new System.Drawing.Size(172, 90);
            this.lblDetailType3.Text = "ConsignorAdress:";
            // 
            // lblDetailType1
            // 
            this.lblDetailType1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType1.Location = new System.Drawing.Point(3, 40);
            this.lblDetailType1.Name = "lblDetailType1";
            this.lblDetailType1.Size = new System.Drawing.Size(160, 30);
            this.lblDetailType1.Text = "Consignments: ";
            // 
            // lblDetailType2
            // 
            this.lblDetailType2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType2.Location = new System.Drawing.Point(3, 70);
            this.lblDetailType2.Name = "lblDetailType2";
            this.lblDetailType2.Size = new System.Drawing.Size(148, 40);
            this.lblDetailType2.Text = "OrderNumber";

            // 
            // btnInfo
            // 
            this.btnInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnInfo.ForeColor = System.Drawing.Color.White;
            this.btnInfo.Location = new System.Drawing.Point(0, 483);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(238, 50);
            this.btnInfo.TabIndex = 3;
            this.btnInfo.Click += new System.EventHandler(this.BtnInfoClick);
            // 
            // btnReject
            // 
            this.btnReject.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnReject.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnReject.ForeColor = System.Drawing.Color.White;
            this.btnReject.Location = new System.Drawing.Point(0, 429);
            this.btnReject.Name = "btnReject";
            this.btnReject.Size = new System.Drawing.Size(238, 50);
            this.btnReject.TabIndex = 1;
            this.btnReject.Click += new System.EventHandler(this.BtnRejectClick);
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnAccept.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnAccept.ForeColor = System.Drawing.Color.White;
            this.btnAccept.Location = new System.Drawing.Point(242, 429);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(238, 50);
            this.btnAccept.TabIndex = 2;

            this.btnAccept.Click += new System.EventHandler(this.BtnAcceptClick);
            // 
            // btnAcceptAll
            // 
            this.btnAcceptAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnAcceptAll.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnAcceptAll.ForeColor = System.Drawing.Color.White;
            this.btnAcceptAll.Location = new System.Drawing.Point(242, 483);
            this.btnAcceptAll.Name = "btnAcceptAll";
            this.btnAcceptAll.Size = new System.Drawing.Size(238, 50);
            this.btnAcceptAll.TabIndex = 4;

            this.btnAcceptAll.Click += new System.EventHandler(this.BtnAcceptAllClick);
            // 
            // FormReceiveOperationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.Controls.Add(this.touchPanel);
            this.Name = "FormReceiveOperationList";
            this.Size = new System.Drawing.Size(480, 536);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnReject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAccept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAcceptAll)).EndInit();
            this.panelBottomInfo.ResumeLayout(false);
            this.ResumeLayout(false);


        }

        #endregion
        private System.Windows.Forms.ImageList ImageListOperationListIcons;
        private System.Windows.Forms.BindingSource testBindingSource;
        private Resco.Controls.AdvancedList.AdvancedList advancedListOperationList;
        private System.Windows.Forms.ImageList ImageList1;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCell1;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateStopPlus;
        private Resco.Controls.AdvancedList.ImageCell ImageCellStopSign;
        private Resco.Controls.AdvancedList.TextCell TextCell9;
        private Resco.Controls.AdvancedList.ImageCell ImageCell2;
        private Resco.Controls.AdvancedList.TextCell TextCell4;
        private Resco.Controls.AdvancedList.TextCell textCell5;
        private Resco.Controls.AdvancedList.TextCell textCell6;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateStopMinus;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateStopSign;
        private Resco.Controls.AdvancedList.TextCell TextCell10;
        private Resco.Controls.AdvancedList.ImageCell ImageCell3;
        private Resco.Controls.AdvancedList.TextCell textCell8;
        private Resco.Controls.AdvancedList.TextCell TextCell15;
        private Resco.Controls.AdvancedList.TextCell TextCell23;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationType;
        private Resco.Controls.AdvancedList.TextCell textCell12;
        private Resco.Controls.AdvancedList.ImageCell ImageCell1;
        private Resco.Controls.AdvancedList.TextCell textCell13;
        private Resco.Controls.AdvancedList.TextCell textCell14;
        private Resco.Controls.AdvancedList.TextCell TextCell24;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.TextCell textCell17;
        private Resco.Controls.AdvancedList.ImageCell ImageCell4;
        private Resco.Controls.AdvancedList.TextCell textCell18;
        private Resco.Controls.AdvancedList.TextCell TextCell21;
        private Resco.Controls.AdvancedList.TextCell TextCell25;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateForSingleOperation;
        private Resco.Controls.AdvancedList.ImageCell ImageCellSingleStopSign;
        private Resco.Controls.AdvancedList.TextCell textCell20;
        private Resco.Controls.AdvancedList.ImageCell ImageCell5;
        private Resco.Controls.AdvancedList.TextCell textCell22;
        private Resco.Controls.AdvancedList.TextCell textCell26;
        private Resco.Controls.AdvancedList.TextCell textCell27;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnInfo;
        private Resco.Controls.OutlookControls.ImageButton btnReject;
        private Resco.Controls.OutlookControls.ImageButton btnAccept;
        private Resco.Controls.OutlookControls.ImageButton btnAcceptAll;
        private System.Windows.Forms.Panel panelBottomInfo;
        private System.Windows.Forms.Label lblDetailValue3;
        private System.Windows.Forms.Label lblDetailValue1;
        private System.Windows.Forms.Label lblDetailValue2;
        private System.Windows.Forms.Label lblDetailType3;
        private System.Windows.Forms.Label lblDetailType1;
        private System.Windows.Forms.Label lblDetailType2;





    }
}
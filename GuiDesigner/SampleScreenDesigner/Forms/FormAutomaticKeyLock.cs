﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormAutomaticKeyLock : Form
    {
        TabMultipleButtons tabMultipleButtons;  
        public FormAutomaticKeyLock()
        {
            InitializeComponent();
            
            tabMultipleButtons = new TabMultipleButtons();
            touchPanel.Controls.Add(tabMultipleButtons);
            tabMultipleButtons.ListButtons.Add(new TabButton("Open", ButtonOpenClick,TabButtonType.Confirm));
            tabMultipleButtons.GenerateButtons();
        }
        protected void ButtonOpenClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OnCauseChanged(object sender, EventArgs e)
        {

        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {

        }

        private void ButtonOkClick(object sender, EventArgs e)
        {

        }


    }
}
﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    public partial class FormSignature: Form
    {
    
        private readonly TabMultipleButtons _tabMultipleButtons;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        private const string ButtonErase = "ButtonErase";
        //FormSignatureView _formMain;
        public FormSignature()
        {
            
            InitializeComponent();
            _tabMultipleButtons = new TabMultipleButtons();
            touchPanel.Controls.Add(_tabMultipleButtons);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            
            SetTextToGui();
            
            
            AddButtons();
            _tabMultipleButtons.GetButton(ButtonErase).BringToFront();
        }

        private void AddButtons()
        {
           
                

                _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Erase, BtnEraseClick, ButtonErase));
                _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                _tabMultipleButtons.GenerateButtons();
           
            
        }

        private void SetTextToGui()
        {
            lblInfo.Text = GlobalTexts.SignatureInfo;
            lblNoOfConsigItm.Text = GlobalTexts.NrOfConsignments;
            lblNoOfServices.Text = "Services added";
            lblFullName.Text = GlobalTexts.CustomerName+":";
            lblSignature.Text = GlobalTexts.Signature+":";
        }


        private void BtnOkClick(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Handles the Click event of the btnErase control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event iformData.</param>
        private void BtnEraseClick(object sender, EventArgs e)
        {
           
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
        }
   



      

        
        
    }
}
﻿
using System;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.CommonControls;

namespace SampleScreenDesigner.Forms
{
    partial class FormDeliveryToConsignee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDeliveryToConsignee));
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.pnlInfoConsignmentAlcohol = new System.Windows.Forms.Panel();
            this.btnYes = new Resco.Controls.OutlookControls.ImageButton();
            this.btnNo = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.pictureBoxConsignment = new System.Windows.Forms.PictureBox();
            this.lblInfoAlcohol = new System.Windows.Forms.Label();
            this.pnlInfoCheckId = new System.Windows.Forms.Panel();
            this.btnAbove24 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBelow25 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnTooYoung = new Resco.Controls.OutlookControls.ImageButton();
            this.pictureBoxCheckId = new System.Windows.Forms.PictureBox();
            this.lblInfoCheckId = new System.Windows.Forms.Label();
            this.btnBlank = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            this.pnlInfoConsignmentAlcohol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            this.pnlInfoCheckId.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAbove24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBelow25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTooYoung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.pnlInfoConsignmentAlcohol);
            this.touchPanel.Controls.Add(this.pnlInfoCheckId);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // pnlInfoConsignmentAlcohol
            // 
            this.pnlInfoConsignmentAlcohol.Controls.Add(this.btnYes);
            this.pnlInfoConsignmentAlcohol.Controls.Add(this.btnNo);
            this.pnlInfoConsignmentAlcohol.Controls.Add(this.btnCancel);
            this.pnlInfoConsignmentAlcohol.Controls.Add(this.pictureBoxConsignment);
            this.pnlInfoConsignmentAlcohol.Controls.Add(this.lblInfoAlcohol);
            this.pnlInfoConsignmentAlcohol.Location = new System.Drawing.Point(9, 128);
            this.pnlInfoConsignmentAlcohol.Name = "pnlInfoConsignmentAlcohol";
            this.pnlInfoConsignmentAlcohol.Size = new System.Drawing.Size(464, 317);
            this.pnlInfoConsignmentAlcohol.GotFocus += new System.EventHandler(this.pnlInfoCheckId12_GotFocus);
            // 
            // btnYes
            // 
            this.btnYes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnYes.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnYes.ForeColor = System.Drawing.Color.White;
            this.btnYes.Location = new System.Drawing.Point(322, 237);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(120, 60);
            this.btnYes.TabIndex = 33;
            this.btnYes.Text = "Yes";
            // 
            // btnNo
            // 
            this.btnNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnNo.ForeColor = System.Drawing.Color.White;
            this.btnNo.Location = new System.Drawing.Point(169, 237);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(120, 60);
            this.btnNo.TabIndex = 32;
            this.btnNo.Text = "No";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(29, 237);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(120, 60);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "Cancel";
            // 
            // pictureBoxConsignment
            // 
            this.pictureBoxConsignment.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxConsignment.Image")));
            this.pictureBoxConsignment.Location = new System.Drawing.Point(185, 8);
            this.pictureBoxConsignment.Name = "pictureBoxConsignment";
            this.pictureBoxConsignment.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxConsignment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblInfoAlcohol
            // 
            this.lblInfoAlcohol.Location = new System.Drawing.Point(3, 69);
            this.lblInfoAlcohol.Name = "lblInfoAlcohol";
            this.lblInfoAlcohol.Size = new System.Drawing.Size(447, 124);
            this.lblInfoAlcohol.Text = "The consignment item contains alcohol. Remember to check the consignee’s age and " +
                "that consignee is sober.\r\nDo you want to continue?";
            // 
            // pnlInfoCheckId
            // 
            this.pnlInfoCheckId.Controls.Add(this.btnAbove24);
            this.pnlInfoCheckId.Controls.Add(this.btnBelow25);
            this.pnlInfoCheckId.Controls.Add(this.btnTooYoung);
            this.pnlInfoCheckId.Controls.Add(this.pictureBoxCheckId);
            this.pnlInfoCheckId.Controls.Add(this.lblInfoCheckId);
            this.pnlInfoCheckId.Location = new System.Drawing.Point(9, 129);
            this.pnlInfoCheckId.Name = "pnlInfoCheckId";
            this.pnlInfoCheckId.Size = new System.Drawing.Size(464, 259);
            // 
            // btnAbove24
            // 
            this.btnAbove24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnAbove24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnAbove24.ForeColor = System.Drawing.Color.White;
            this.btnAbove24.Location = new System.Drawing.Point(322, 171);
            this.btnAbove24.Name = "btnAbove24";
            this.btnAbove24.Size = new System.Drawing.Size(120, 60);
            this.btnAbove24.TabIndex = 33;
            this.btnAbove24.Text = "Above 24";
            // 
            // btnBelow25
            // 
            this.btnBelow25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBelow25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBelow25.ForeColor = System.Drawing.Color.White;
            this.btnBelow25.Location = new System.Drawing.Point(173, 171);
            this.btnBelow25.Name = "btnBelow25";
            this.btnBelow25.Size = new System.Drawing.Size(120, 60);
            this.btnBelow25.TabIndex = 32;
            this.btnBelow25.Text = "Below 25";
            // 
            // btnTooYoung
            // 
            this.btnTooYoung.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnTooYoung.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnTooYoung.ForeColor = System.Drawing.Color.White;
            this.btnTooYoung.Location = new System.Drawing.Point(25, 171);
            this.btnTooYoung.Name = "btnTooYoung";
            this.btnTooYoung.Size = new System.Drawing.Size(120, 60);
            this.btnTooYoung.TabIndex = 31;
            this.btnTooYoung.Text = "Too young";
            // 
            // pictureBoxCheckId
            // 
            this.pictureBoxCheckId.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCheckId.Image")));
            this.pictureBoxCheckId.Location = new System.Drawing.Point(185, 8);
            this.pictureBoxCheckId.Name = "pictureBoxCheckId";
            this.pictureBoxCheckId.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxCheckId.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblInfoCheckId
            // 
            this.lblInfoCheckId.Location = new System.Drawing.Point(3, 103);
            this.lblInfoCheckId.Name = "lblInfoCheckId";
            this.lblInfoCheckId.Size = new System.Drawing.Size(447, 44);
            this.lblInfoCheckId.Text = "Check Id papers to verify age!";
            // 
            // btnBlank
            // 
            this.btnBlank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBlank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBlank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBlank.ForeColor = System.Drawing.Color.White;
            this.btnBlank.Location = new System.Drawing.Point(161, 501);
            this.btnBlank.Name = "btnBlank";
            this.btnBlank.Size = new System.Drawing.Size(158, 50);
            this.btnBlank.TabIndex = 8;
            // 
            // FormDeliveryToConsignee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Controls.Add(this.btnBlank);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDeliveryToConsignee";
            this.Text = "FormDeliveryToConsignee";
            this.touchPanel.ResumeLayout(false);
            this.pnlInfoConsignmentAlcohol.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            this.pnlInfoCheckId.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnAbove24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBelow25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTooYoung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnBlank;
        private System.Windows.Forms.Panel pnlInfoConsignmentAlcohol;
        private System.Windows.Forms.Label lblInfoAlcohol;
        private System.Windows.Forms.PictureBox pictureBoxConsignment;
        private Resco.Controls.OutlookControls.ImageButton btnYes;
        private Resco.Controls.OutlookControls.ImageButton btnNo;
        private Resco.Controls.OutlookControls.ImageButton btnCancel;
        private System.Windows.Forms.Panel pnlInfoCheckId;
        private Resco.Controls.OutlookControls.ImageButton btnAbove24;
        private Resco.Controls.OutlookControls.ImageButton btnBelow25;
        private Resco.Controls.OutlookControls.ImageButton btnTooYoung;
        private System.Windows.Forms.PictureBox pictureBoxCheckId;
        private System.Windows.Forms.Label lblInfoCheckId;
    }
}
﻿using System.Collections.Generic;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    public partial class FormWorkList : Form
    {
        public FormWorkList()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            BindOperations();
        }

        private void btnNewCarrier_Click(object sender, System.EventArgs e)
        {
            pnlConfirmMessage.Visible = true;
            pnlConfirmMessage.BringToFront();
        }

        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            switch (e.DataRow.CurrentTemplateIndex)
            {
                    
                case 1: // The master row has to be collapsed
                                   
                    break;
                case 2: // The master row has to be expanded  
                    UnSelectOtherRows();
                    e.DataRow.Selected = true;
                    break;
            }
        }


        


        private void UnSelectOtherRows()
        {
            for (var i = 0; i <= listOperations.DataRows.Count - 1;i++ )
            {
                var row = listOperations.DataRows[i];
                row.Selected = false;
            }

        }

        private void BindOperations()
        {

            var firstObject = new WorkListItem()
                                  {
                                      OperationType = LoadingOperationType.Loading,
                                      LoadCarrierId = "ABC123",
                                      StopId="StopId1",
                                      TripId="TripId1"  ,
                                  };
            var secondObject = new WorkListItem()
            {
                OperationType = LoadingOperationType.Loading,
                LoadCarrierId = "DEF345",
                StopId = "StopId1",
                TripId = "TripId1",
            };



            var listOfObjects = new List<WorkListItem>();
            listOfObjects.Add(firstObject);
            listOfObjects.Add(secondObject);

            listOperations.BeginUpdate();
            listOperations.DataSource = listOfObjects;
            listOperations.EndUpdate();
        }

    }

    public class WorkListItem
    {
        public string LoadCarrierId { get; set; }
        public string StopId { get; set; }
        public string TripId { get; set; }
        public LoadingOperationType OperationType { get; set; }
    }

    public enum LoadingOperationType
    {
        Loading = 0,
        Unloading = 1
    }
}
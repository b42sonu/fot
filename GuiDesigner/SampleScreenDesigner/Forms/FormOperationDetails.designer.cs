﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.OperationList.Views;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormOperationDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.advancedListTime = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell5 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate2 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell7 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell8 = new Resco.Controls.AdvancedList.TextCell();
            this.labelCustomer = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelStop = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConfirmedBy = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelAmount = new Resco.Controls.CommonControls.TransparentLabel();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.textBoxConfirmedBy = new System.Windows.Forms.TextBox();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConfirmedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.textBoxConfirmedBy);
            this.touchPanel.Controls.Add(this.textBoxAmount);
            this.touchPanel.Controls.Add(this.labelConfirmedBy);
            this.touchPanel.Controls.Add(this.labelAmount);
            this.touchPanel.Controls.Add(this.labelStop);
            this.touchPanel.Controls.Add(this.advancedListTime);
            this.touchPanel.Controls.Add(this.labelCustomer);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // advancedListTime
            // 
            this.advancedListTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListTime.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedListTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListTime.DataRows.Clear();
            this.advancedListTime.Location = new System.Drawing.Point(22, 126);
            this.advancedListTime.Name = "advancedListTime";
            this.advancedListTime.ScrollbarSmallChange = 32;
            this.advancedListTime.ScrollbarWidth = 26;
            this.advancedListTime.SelectedTemplateIndex = 1;
            this.advancedListTime.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.advancedListTime.Size = new System.Drawing.Size(436, 221);
            this.advancedListTime.TabIndex = 38;
            this.advancedListTime.Templates.Add(this.rowTemplate1);
            this.advancedListTime.Templates.Add(this.rowTemplate2);
            this.advancedListTime.TouchScrolling = true;
            this.advancedListTime.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            // 
            // rowTemplate1
            // 
            this.rowTemplate1.BackColor = System.Drawing.Color.LightGray;
            this.rowTemplate1.CellTemplates.Add(this.textCell5);
            this.rowTemplate1.CellTemplates.Add(this.textCell6);
            this.rowTemplate1.Height = 64;
            this.rowTemplate1.Name = "rowTemplate1";
            // 
            // textCell5
            // 
            this.textCell5.CellSource.ColumnName = "VasId";
            this.textCell5.DesignName = "textCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(65, 64);
            this.textCell5.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // textCell6
            // 
            this.textCell6.CellSource.ColumnName = "VasType";
            this.textCell6.DesignName = "textCell6";
            this.textCell6.Location = new System.Drawing.Point(66, 0);
            this.textCell6.Size = new System.Drawing.Size(-1, 64);
            this.textCell6.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // rowTemplate2
            // 
            this.rowTemplate2.BackColor = System.Drawing.Color.White;
            this.rowTemplate2.CellTemplates.Add(this.textCell7);
            this.rowTemplate2.CellTemplates.Add(this.textCell8);
            this.rowTemplate2.Height = 64;
            this.rowTemplate2.Name = "rowTemplate2";
            // 
            // textCell7
            // 
            this.textCell7.CellSource.ColumnName = "VasId";
            this.textCell7.DesignName = "textCell7";
            this.textCell7.Location = new System.Drawing.Point(0, 0);
            this.textCell7.Size = new System.Drawing.Size(65, 64);
            this.textCell7.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // textCell8
            // 
            this.textCell8.CellSource.ColumnName = "VasType";
            this.textCell8.DesignName = "textCell8";
            this.textCell8.Location = new System.Drawing.Point(66, 0);
            this.textCell8.Size = new System.Drawing.Size(-1, 64);
            this.textCell8.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // labelCustomer
            // 
            this.labelCustomer.AutoSize = false;
            this.labelCustomer.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelCustomer.Location = new System.Drawing.Point(22, 46);
            this.labelCustomer.Name = "labelCustomer";
            this.labelCustomer.Size = new System.Drawing.Size(436, 28);
            this.labelCustomer.Text = "Customer";
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(162, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(127, 27);
            this.labelModuleName.Text = "Select VAS";
            // 
            // labelStop
            // 
            this.labelStop.AutoSize = false;
            this.labelStop.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelStop.Location = new System.Drawing.Point(22, 86);
            this.labelStop.Name = "labelStop";
            this.labelStop.Size = new System.Drawing.Size(436, 28);
            this.labelStop.Text = "Stop";
            // 
            // labelConfirmedBy
            // 
            this.labelConfirmedBy.AutoSize = false;
            this.labelConfirmedBy.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConfirmedBy.Location = new System.Drawing.Point(22, 417);
            this.labelConfirmedBy.Name = "labelConfirmedBy";
            this.labelConfirmedBy.Size = new System.Drawing.Size(137, 28);
            this.labelConfirmedBy.Text = "Confirmed by";
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = false;
            this.labelAmount.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelAmount.Location = new System.Drawing.Point(22, 377);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(170, 28);
            this.labelAmount.Text = "Amount";
            // 
            // textBoxAmount
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(203, 370);
            this.textBoxAmount.Name = "textBoxAmount";
            this.textBoxAmount.Size = new System.Drawing.Size(200, 41);
            this.textBoxAmount.TabIndex = 46;
            // 
            // textBoxConfirmedBy
            // 
            this.textBoxConfirmedBy.Location = new System.Drawing.Point(203, 417);
            this.textBoxConfirmedBy.Name = "textBoxConfirmedBy";
            this.textBoxConfirmedBy.Size = new System.Drawing.Size(255, 41);
            this.textBoxConfirmedBy.TabIndex = 47;
            // 
            // FormRegisterTimeConsumption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormRegisterVas";
            this.Text = "FormRegisterVas";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConfirmedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelAmount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelCustomer;
        private Resco.Controls.AdvancedList.AdvancedList advancedListTime;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell5;
        private Resco.Controls.AdvancedList.TextCell textCell6;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate2;
        private Resco.Controls.AdvancedList.TextCell textCell7;
        private Resco.Controls.AdvancedList.TextCell textCell8;
        private Resco.Controls.CommonControls.TransparentLabel labelStop;
        private System.Windows.Forms.TextBox textBoxConfirmedBy;
        private System.Windows.Forms.TextBox textBoxAmount;
        private Resco.Controls.CommonControls.TransparentLabel labelConfirmedBy;
        private Resco.Controls.CommonControls.TransparentLabel labelAmount;   
    }
}
﻿//namespace SampleScreenDesigner.Forms
//{
//    partial class FormTakePhoto
//    {
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.IContainer components = null;

//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && (components != null))
//            {
//                components.Dispose();
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
//            this.panelPopUp = new System.Windows.Forms.Panel();
//            this.panelCamera = new System.Windows.Forms.Panel();
//            this.btnDiscard = new Resco.Controls.OutlookControls.ImageButton();
//            this.btnSave = new Resco.Controls.OutlookControls.ImageButton();
//            this.lableThumbnail = new Resco.Controls.CommonControls.TransparentLabel();
//            this.touchPanelPhotos = new Resco.Controls.CommonControls.TouchPanel();
//            this.lableCameraInstruction = new Resco.Controls.CommonControls.TransparentLabel();
//            this.lableItemDetail = new Resco.Controls.CommonControls.TransparentLabel();
//            this.lableDamageFor = new Resco.Controls.CommonControls.TransparentLabel();
//            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
//            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
//            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
//            this.touchPanel.SuspendLayout();
//            this.panelPopUp.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.btnDiscard)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lableThumbnail)).BeginInit();
//            this.touchPanelPhotos.SuspendLayout();
//            ((System.ComponentModel.ISupportInitialize)(this.lableCameraInstruction)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lableItemDetail)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lableDamageFor)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
//            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // touchPanel
//            // 
//            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
//            this.touchPanel.Controls.Add(this.panelPopUp);
//            this.touchPanel.Controls.Add(this.lableThumbnail);
//            this.touchPanel.Controls.Add(this.touchPanelPhotos);
//            this.touchPanel.Controls.Add(this.lableCameraInstruction);
//            this.touchPanel.Controls.Add(this.lableItemDetail);
//            this.touchPanel.Controls.Add(this.lableDamageFor);
//            this.touchPanel.Controls.Add(this.btnBack);
//            this.touchPanel.Controls.Add(this.btnOk);
//            this.touchPanel.Controls.Add(this.labelModuleName);
//            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.touchPanel.Location = new System.Drawing.Point(0, 0);
//            this.touchPanel.Name = "touchPanel";
//            this.touchPanel.Size = new System.Drawing.Size(480, 535);
//            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
//            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
//            // 
//            // panelPopUp
//            // 
//            this.panelPopUp.Controls.Add(this.panelCamera);
//            this.panelPopUp.Controls.Add(this.btnDiscard);
//            this.panelPopUp.Controls.Add(this.btnSave);
//            this.panelPopUp.Location = new System.Drawing.Point(4, 79);
//            this.panelPopUp.Name = "panelPopUp";
//            this.panelPopUp.Size = new System.Drawing.Size(473, 333);
//            this.panelPopUp.Visible = false;
//            // 
//            // panelCamera
//            // 
//            this.panelCamera.Location = new System.Drawing.Point(138, 10);
//            this.panelCamera.Name = "panelCamera";
//            this.panelCamera.Size = new System.Drawing.Size(200, 200);
//            // 
//            // btnDiscard
//            // 
//            this.btnDiscard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.btnDiscard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
//            this.btnDiscard.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
//            this.btnDiscard.ForeColor = System.Drawing.Color.White;
//            this.btnDiscard.Location = new System.Drawing.Point(22, 271);
//            this.btnDiscard.Name = "btnDiscard";
//            this.btnDiscard.Size = new System.Drawing.Size(147, 50);
//            this.btnDiscard.TabIndex = 33;
//            this.btnDiscard.Text = "Discard";
//            this.btnDiscard.Click += new System.EventHandler(this.BtnDiscardClick);
//            // 
//            // btnSave
//            // 
//            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
//            this.btnSave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
//            this.btnSave.ForeColor = System.Drawing.Color.White;
//            this.btnSave.Location = new System.Drawing.Point(304, 271);
//            this.btnSave.Name = "btnSave";
//            this.btnSave.Size = new System.Drawing.Size(147, 50);
//            this.btnSave.TabIndex = 32;
//            this.btnSave.Text = "Save";
//            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
//            // 
//            // lableThumbnail
//            // 
//            this.lableThumbnail.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
//            this.lableThumbnail.Location = new System.Drawing.Point(4, 222);
//            this.lableThumbnail.Name = "lableThumbnail";
//            this.lableThumbnail.Size = new System.Drawing.Size(366, 24);
//            this.lableThumbnail.Text = "Thumbnail for already captured photos";
//            // 
//            // touchPanelPhotos
//            // 
//            this.touchPanelPhotos.BackColor = System.Drawing.SystemColors.InactiveCaption;
//            this.touchPanelPhotos.Location = new System.Drawing.Point(0, 248);
//            this.touchPanelPhotos.Name = "touchPanelPhotos";
//            this.touchPanelPhotos.Size = new System.Drawing.Size(480, 230);
//            // 
//            // lableCameraInstruction
//            // 
//            this.lableCameraInstruction.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Regular);
//            this.lableCameraInstruction.Location = new System.Drawing.Point(203, 101);
//            this.lableCameraInstruction.Name = "lableCameraInstruction";
//            this.lableCameraInstruction.Size = new System.Drawing.Size(238, 65);
//            this.lableCameraInstruction.Text = "Click on camera  to activate \r\ncamera .User enter button to\r\ncapture a photo.";
//            // 
//            // lableItemDetail
//            // 
//            this.lableItemDetail.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
//            this.lableItemDetail.Location = new System.Drawing.Point(142, 49);
//            this.lableItemDetail.Name = "lableItemDetail";
//            this.lableItemDetail.Size = new System.Drawing.Size(255, 24);
//            this.lableItemDetail.Text = "<item type><item number>";
//            // 
//            // lableDamageFor
//            // 
//            this.lableDamageFor.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
//            this.lableDamageFor.Location = new System.Drawing.Point(4, 49);
//            this.lableDamageFor.Name = "lableDamageFor";
//            this.lableDamageFor.Size = new System.Drawing.Size(119, 24);
//            this.lableDamageFor.Text = "Damage For";
//            // 
//            // btnBack
//            // 
//            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
//            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
//            this.btnBack.ForeColor = System.Drawing.Color.White;
//            this.btnBack.Location = new System.Drawing.Point(0, 484);
//            this.btnBack.Name = "btnBack";
//            this.btnBack.Size = new System.Drawing.Size(238, 50);
//            this.btnBack.TabIndex = 31;
//            this.btnBack.Text = "Back";
//            this.btnBack.Click += new System.EventHandler(this.ButtonBackClick);
//            // 
//            // btnOk
//            // 
//            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
//            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
//            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
//            this.btnOk.ForeColor = System.Drawing.Color.White;
//            this.btnOk.Location = new System.Drawing.Point(242, 484);
//            this.btnOk.Name = "btnOk";
//            this.btnOk.Size = new System.Drawing.Size(238, 50);
//            this.btnOk.TabIndex = 30;
//            this.btnOk.Text = "Ok/Enter";
//            this.btnOk.Click += new System.EventHandler(this.ButtonConfirmClick);
//            // 
//            // labelModuleName
//            // 
//            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
//            this.labelModuleName.Location = new System.Drawing.Point(142, 3);
//            this.labelModuleName.Name = "labelModuleName";
//            this.labelModuleName.Size = new System.Drawing.Size(233, 27);
//            this.labelModuleName.Text = "Damage registration";
//            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
//            // 
//            // FormTakePhoto
//            // 
//            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
//            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
//            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
//            this.ClientSize = new System.Drawing.Size(480, 535);
//            this.Controls.Add(this.touchPanel);
//            this.Location = new System.Drawing.Point(0, 52);
//            this.Name = "FormTakePhoto";
//            this.Text = "FormTakePhoto";
//            this.touchPanel.ResumeLayout(false);
//            this.panelPopUp.ResumeLayout(false);
//            ((System.ComponentModel.ISupportInitialize)(this.btnDiscard)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lableThumbnail)).EndInit();
//            this.touchPanelPhotos.ResumeLayout(false);
//            ((System.ComponentModel.ISupportInitialize)(this.lableCameraInstruction)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lableItemDetail)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.lableDamageFor)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
//            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private Resco.Controls.CommonControls.TouchPanel touchPanel;
//        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
//        private Resco.Controls.OutlookControls.ImageButton btnBack;
//        private Resco.Controls.OutlookControls.ImageButton btnOk;
//        private Resco.Controls.CommonControls.TransparentLabel lableItemDetail;
//        private Resco.Controls.CommonControls.TransparentLabel lableDamageFor;
//        private Resco.Controls.CommonControls.TransparentLabel lableCameraInstruction;
//        private Resco.Controls.CommonControls.TransparentLabel lableThumbnail;
//        private Resco.Controls.CommonControls.TouchPanel touchPanelPhotos;
//        private System.Windows.Forms.Panel panelPopUp;
//        private Resco.Controls.OutlookControls.ImageButton btnDiscard;
//        private Resco.Controls.OutlookControls.ImageButton btnSave;
//        private System.Windows.Forms.Panel panelCamera;
      
//    }
//}
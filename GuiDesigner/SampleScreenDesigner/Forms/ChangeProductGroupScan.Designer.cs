﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace SampleScreenDesigner.Forms
{
    partial class ChangeProductGroupScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.buttonCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.txtScannedNumber = new System.Windows.Forms.TextBox();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblScanHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblScanHeading);
            this.touchPanel.Controls.Add(this.buttonCancel);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(242, 482);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(238, 50);
            this.buttonCancel.TabIndex = 29;
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(0, 482);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(238, 50);
            this.buttonOk.TabIndex = 28;
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(24, 122);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(424, 20);
            this.txtScannedNumber.TabIndex = 37;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(131, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // messageControlValidateScan
            // 
            this.messageControlValidateScan.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControlValidateScan.Location = new System.Drawing.Point(24, 186);
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(424, 120);
            this.messageControlValidateScan.TabIndex = 56;
            // 
            // lblScanHeading
            // 
            this.lblScanHeading.Location = new System.Drawing.Point(29, 101);
            this.lblScanHeading.Name = "lblScanHeading";
            this.lblScanHeading.Size = new System.Drawing.Size(122, 15);
            this.lblScanHeading.Text = "Scan of Enter Barcode ";
            // 
            // ChangeProductGroupScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Name = "ChangeProductGroupScan";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private MessageControl messageControlValidateScan;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton buttonCancel;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private TextBox txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblScanHeading;
    }
}
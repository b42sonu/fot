﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    public partial class FormScanDeliveryToPostOffice : Form
    {
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private TabMultipleButtons _tabButtons;
        public FormScanDeliveryToPostOffice()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            //InitOnScreenKeyBoardProperties();
            //SetBottomLabelVisibility(false);
        }
        private void SetTextToControls()
        {
            _tabButtons = new TabMultipleButtons();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonConfirmClick, TabButtonOkName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            lblModuleName.Text = GlobalTexts.DeliveredToPoPibByCustomer;
            lblScan.Text = GlobalTexts.ScanOrEnterConsignmentItem;
            lblItem.Text = GlobalTexts.NoOfItems;
        }

   
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
           
        }
    }
}
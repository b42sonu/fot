﻿namespace SampleScreenDesigner.Forms
{
    partial class FormPrinterInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblPrinterNameValue = new System.Windows.Forms.Label();
            this.lblDestValue = new System.Windows.Forms.Label();
            this.lblLoadCarierValue = new System.Windows.Forms.Label();
            this.lblPrinterNameText = new System.Windows.Forms.Label();
            this.lblDestText = new System.Windows.Forms.Label();
            this.lblLoadCarierText = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.lblModuleName = new System.Windows.Forms.Label();
            this.btnChangePrinter = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOkEnter = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBlank = new Resco.Controls.OutlookControls.ImageButton();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnChangePrinter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOkEnter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblPrinterNameValue);
            this.touchPanel.Controls.Add(this.lblDestValue);
            this.touchPanel.Controls.Add(this.lblLoadCarierValue);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.lblPrinterNameText);
            this.touchPanel.Controls.Add(this.lblDestText);
            this.touchPanel.Controls.Add(this.lblLoadCarierText);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.btnChangePrinter);
            this.touchPanel.Controls.Add(this.btnOkEnter);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 519, 480, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // lblPrinterNameValue
            // 
            this.lblPrinterNameValue.Location = new System.Drawing.Point(111, 410);
            this.lblPrinterNameValue.Name = "lblPrinterNameValue";
            this.lblPrinterNameValue.Size = new System.Drawing.Size(356, 40);
            this.lblPrinterNameValue.Text = "N2808_LAS   e-mail is unknown";
            // 
            // lblDestValue
            // 
            this.lblDestValue.Location = new System.Drawing.Point(111, 360);
            this.lblDestValue.Name = "lblDestValue";
            this.lblDestValue.Size = new System.Drawing.Size(366, 40);
            this.lblDestValue.Text = "3045 Bakkeryd og Sonn";
            // 
            // lblLoadCarierValue
            // 
            this.lblLoadCarierValue.Location = new System.Drawing.Point(111, 310);
            this.lblLoadCarierValue.Name = "lblLoadCarierValue";
            this.lblLoadCarierValue.Size = new System.Drawing.Size(366, 40);
            this.lblLoadCarierValue.Text = "76543232321";
            // 
            // lblPrinterNameText
            // 
            this.lblPrinterNameText.Location = new System.Drawing.Point(15, 410);
            this.lblPrinterNameText.Name = "lblPrinterNameText";
            this.lblPrinterNameText.Size = new System.Drawing.Size(90, 42);
            this.lblPrinterNameText.Text = "Printer:";
            // 
            // lblDestText
            // 
            this.lblDestText.Location = new System.Drawing.Point(15, 360);
            this.lblDestText.Name = "lblDestText";
            this.lblDestText.Size = new System.Drawing.Size(90, 40);
            this.lblDestText.Text = "DEST:";
            // 
            // lblLoadCarierText
            // 
            this.lblLoadCarierText.Location = new System.Drawing.Point(15, 310);
            this.lblLoadCarierText.Name = "lblLoadCarierText";
            this.lblLoadCarierText.Size = new System.Drawing.Size(90, 40);
            this.lblLoadCarierText.Text = "LB:";
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(15, 73);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(462, 48);
            this.lblHeading.Text = "OP OSLO OG AKERSHUS VAREBIL1";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(139, 18);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(217, 50);
            this.lblModuleName.Text = "Print voucher";
            // 
            // btnChangePrinter
            // 
            this.btnChangePrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChangePrinter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnChangePrinter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnChangePrinter.ForeColor = System.Drawing.Color.White;
            this.btnChangePrinter.Location = new System.Drawing.Point(0, 483);
            this.btnChangePrinter.Name = "btnChangePrinter";
            this.btnChangePrinter.Size = new System.Drawing.Size(158, 50);
            this.btnChangePrinter.TabIndex = 6;
            this.btnChangePrinter.Text = "Change Printer";
            this.btnChangePrinter.Click += new System.EventHandler(this.ButtonConfirmClick);
            // 
            // btnOkEnter
            // 
            this.btnOkEnter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOkEnter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOkEnter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOkEnter.ForeColor = System.Drawing.Color.White;
            this.btnOkEnter.Location = new System.Drawing.Point(322, 483);
            this.btnOkEnter.Name = "btnOkEnter";
            this.btnOkEnter.Size = new System.Drawing.Size(158, 50);
            this.btnOkEnter.TabIndex = 7;
            this.btnOkEnter.Text = "OK/Enter";
            this.btnOkEnter.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // btnBlank
            // 
            this.btnBlank.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBlank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBlank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBlank.ForeColor = System.Drawing.Color.White;
            this.btnBlank.Location = new System.Drawing.Point(161, 483);
            this.btnBlank.Name = "btnBlank";
            this.btnBlank.Size = new System.Drawing.Size(158, 50);
            this.btnBlank.TabIndex = 6;
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(19, 130);
            this.MsgMessage.MessageText = "";
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(424, 131);
            this.MsgMessage.TabIndex = 14;
            // 
            // FormPrinterInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormPrinterInformation";
            this.Text = "FormPrinterInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnChangePrinter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOkEnter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBlank)).EndInit();
            this.ResumeLayout(false);

        }



        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnOkEnter;
        private Resco.Controls.OutlookControls.ImageButton btnChangePrinter;
        private System.Windows.Forms.Label lblModuleName;
        private System.Windows.Forms.Label lblDestText;
        private System.Windows.Forms.Label lblLoadCarierText;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Label lblPrinterNameText;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl MsgMessage;
        private System.Windows.Forms.Label lblPrinterNameValue;
        private System.Windows.Forms.Label lblDestValue;
        private System.Windows.Forms.Label lblLoadCarierValue;
        private Resco.Controls.OutlookControls.ImageButton btnBlank;
    }
}
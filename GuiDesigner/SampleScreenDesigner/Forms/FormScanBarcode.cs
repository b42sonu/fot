﻿using System;
using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    public partial class FormScanBarcode : Form
    {

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public FormScanBarcode()
        {
            InitializeComponent();
        }

        private void ConsignmentNumberTextChanged(bool userFinishedTyping, string textentered)
        {
        }


        public void PnlConfirmMessagePaint(object sender, PaintEventArgs e)
        {

        }

        private void BtnUnloadClick(object sender, EventArgs e)
        {
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
        }

        private void BtnLoadClick(object sender, EventArgs e)
        {
        }

        private void ButtonReconcilliation(object sender, EventArgs e)
        {
        }

        private void ButtonAttemptedDeliveryClick(object sender, EventArgs e)
        {
        }

        private void ButtonAttemptedPickupClick(object sender, EventArgs e)
        {
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {

        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
        }


        private void ButtonExitClick(object sender, EventArgs e)
        {
        }

        private void ButtonAttemptDeliveryClick(object sender, EventArgs e)
        {

        }

        private void ButtonChangeCarrierClick(object sender, EventArgs e)
        {
        }
        
    }



}
﻿
using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;
using System.Drawing;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormDamageScan : Form
    {

        private const string ButtonTakePhotoName = "btnTakePhoto";
        private const string ButtonOkName = "btnOk";
        private const string ButtonBackName = "btnBack";
        private const string ButtonNewDamageName = "btnNewDamage";
        private readonly TabMultipleButtons _tabMultipleButtons;
        public FormDamageScan()
        {
            InitializeComponent();
             _tabMultipleButtons = new TabMultipleButtons();
             //touchPanel.Controls.Add(_tabMultipleButtons);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetGuiBasedOnWhereTheUserComeFrom(true);

        }
        private void SetTextToGui()
        {
            lblScanHeading.Text = GlobalTexts.ScanOrEnterBarcode;
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBackName });
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.TakePhoto, ButtonTakePhoto, ButtonTakePhotoName));
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonConfirmClick, TabButtonType.Confirm) { Name = ButtonOkName });
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.NewDamage, ButtonNewDamageClick, ButtonNewDamageName));
            _tabMultipleButtons.GenerateButtons();


            lblType.Text = String.Format("{0} {1}: {2}", GlobalTexts.Damage, GlobalTexts.Type.ToLower(), "2344");

            lblCause.Text = String.Format("{0}: {1}", GlobalTexts.Cause, "gdfgsg dfgdsgfg");
            lblMeasure.Text = String.Format("{0}: {1}", GlobalTexts.Measure, "cxvvzvc dfgdfgds");

            lblComments.Text = GlobalTexts.Comment + ":";
            txtComments.Text = "dsfjadsjds fhgkjsj jghhj hgjhdf sunil";
        }
        private void SetGuiBasedOnWhereTheUserComeFrom(bool fromCorrectionMenu)
        {
            txtScannedNumber.Visible = !fromCorrectionMenu;
            //messageControlValidateScan.ShowMessage("dgdsgdfdfgdgd", MessageState.Error);
            //_tabMultipleButtons.Enable(ButtonNewDamageName, !fromCorrectionMenu);
            if (fromCorrectionMenu)
            {

                //Read only view when consignment entity comes from correction menu
                //TODO:: Confirm this comment
                // buttonTakePhoto.Location = new System.Drawing.Point(56, 288); 
                messageControlValidateScan.Location = new Point(ScaleUtil.GetScaledPosition(25), ScaleUtil.GetScaledPosition(316));// new Point(25, 330);
                //messageControlValidateScan.ShowMessage("dgdsgdfdfgdgd", MessageState.Error);
                if (true)
                {
                    String type = GlobalTexts.ConsignmentItem;
                    lblScanHeading.Text = GlobalTexts.TakePictureConfirmation;
                    //Design Adjustments
                    /*if (type == GlobalTexts.ConsignmentItem)
                    {
                        lblConsignmentEntityLabel.Size = new Size(ScaleUtil.GetScaledPosition(250), ScaleUtil.GetScaledPosition(25));
                        lblConsignmentEntityNoLabel.Location = new Point(ScaleUtil.GetScaledPosition(262), ScaleUtil.GetScaledPosition(34));
                    }
                    else
                    {
                        lblConsignmentEntityLabel.Size = new Size(ScaleUtil.GetScaledPosition(265), ScaleUtil.GetScaledPosition(25));
                        lblConsignmentEntityNoLabel.Location = new Point(ScaleUtil.GetScaledPosition(277), ScaleUtil.GetScaledPosition(34));
                    }*/
                    lblConsignmentEntityLabel.Text = type + ":";//String.Format("{0} {1}", GlobalTexts.DamageFor,type.ToLower());
                    lblConsignmentEntityNoLabel.Text = "37071375004385434";

                }


                lblScanHeading.Font = new Font("Tahoma", 9F, FontStyle.Regular);
                //StopScan();
            }

            else
            {
                //TODO:: Confirm this comment
                //buttonTakePhoto.Location = new System.Drawing.Point(242, 447);
                //messageControlValidateScan.Location = new System.Drawing.Point(25, 277);
                lblConsignmentEntityLabel.Text = "";
                lblConsignmentEntityNoLabel.Text = "";
                lblScanHeading.Text = GlobalTexts.ScanOrEnterBarcode;
                lblScanHeading.Font = new Font("Tahoma", 9F, FontStyle.Regular);
            }


            if (fromCorrectionMenu)
            {
                //_tabMultipleButtons.Enable(ButtonOkName, true);
                //_tabMultipleButtons.Enable(ButtonTakePhotoName, true);
            }
        }
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
        }

        private void ButtonNewDamageClick(object sender, EventArgs e)
        {
        }

        private void ButtonTakePhoto(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

    }
}
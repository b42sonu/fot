﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;
using Resco.Controls.AdvancedTree;

namespace SampleScreenDesigner.Forms
{
    public partial class FormAlystraPushOperationList : Form
    {
        enum TemplateIndex
        {
            StopMultiOperation,
            StopMultiOperationSelected,
            StopSingleOperation,
            StopSingleOperationSelected,
            Operation,
            OperationSelected,
            OrderStops,
            OrderStopsSelected,
        }


        private readonly IList<CustomOperationList> _customOperationLists;
        private const string AlystraOperationListFileName = "OperationList-from-Alystra-fixed.xml";
        //private const string AmphoraOperationListFileName = "OperationList-from-Amphora.xml";

        private OperationList _operationList;
        private const string TabButtonMessage = "Message";
        private const string TabButtonAvailability = "Availability";
        private const string TabButtonSort = "Sort";
        private const string TabButtonBack = "Back";
        private const string TabButtonDetail = "Detail";
        private const string TabButtonDeliver = "Deliver";
        private TabMultipleButtons _tabMultipleButtons;

        public FormAlystraPushOperationList()
        {
            InitializeComponent();
            RetrieveLocalOperationList(AlystraOperationListFileName);

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            _customOperationLists = CreateCustomOperationsList();
            BindTreeView();

        }

        private void SetTextToGui()
        {
            _tabMultipleButtons = new TabMultipleButtons();
            _tabMultipleButtons.ListButtons.Add(new TabButton("Messsage", BtnBackClick, TabButtonMessage));
            _tabMultipleButtons.ListButtons.Add(new TabButton("Availability", BtnScanClick, TabButtonAvailability) { ButtonType = TabButtonType.Confirm });
            _tabMultipleButtons.ListButtons.Add(new TabButton("Sort", BtnDetailsClick, TabButtonSort));
            _tabMultipleButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, TabButtonBack) { ButtonType = TabButtonType.Cancel });
            _tabMultipleButtons.ListButtons.Add(new TabButton("Detail", BtnDetailsClick, TabButtonDetail));
            _tabMultipleButtons.ListButtons.Add(new TabButton("Deliver", BtnDetailsClick, TabButtonDeliver));
            touchPanel.Controls.Add(_tabMultipleButtons);
            _tabMultipleButtons.GenerateButtons();
        }

        #region "Events buttons"

        private void BtnDetailsClick(object sender, EventArgs e)
        {

        }


        private void BtnBackClick(object sender, EventArgs e)
        {

        }

        private void BtnScanClick(object sender, EventArgs e)
        {

        }
        #endregion


        private void TabControlOperationSelectedIndexChanged(object sender, EventArgs e)
        {
        }

        public void RetrieveLocalOperationList(string fileName)
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            string filePath = path + "\\xml\\" + fileName;
            if (File.Exists(filePath))
            {
                using (XmlReader xmlReader = XmlReader.Create(filePath))
                {
                    var xmlSerializer = new XmlSerializer(typeof(OperationList));
                    _operationList = (OperationList)xmlSerializer.Deserialize(xmlReader);
                }
            }
        }


        private void BindTreeView()
        {
            Type operationListType = typeof(CustomOperationList);
            var operationListMapping = new PropertyMapping(operationListType);

            Type operationDetailsType = typeof(OperationDetails);
            var operationDetailsMapping = new PropertyMapping(operationDetailsType);

            var listOfOperationLists = _customOperationLists ?? TestData();
            foreach (var operationList in listOfOperationLists)
            {
                // create a new stop-node
                var templateIndex = TemplateIndex.StopMultiOperation;
                var templateIndexSelected = TemplateIndex.StopMultiOperationSelected;
                if (operationList.Operations == null && operationList.IsMultipleStopOrder == false)
                {
                    templateIndex = TemplateIndex.StopSingleOperation;
                    templateIndexSelected = TemplateIndex.StopSingleOperationSelected;
                }
                else if (operationList.IsMultipleStopOrder)
                {
                    templateIndex = TemplateIndex.OrderStops;
                    templateIndexSelected = TemplateIndex.OrderStopsSelected;
                }

                var stopNode = new BoundNode((int)templateIndex, (int)templateIndexSelected, operationList, operationListMapping)
                {
                    HidePlusMinus = operationList.Operations == null
                };

                // add it to advancedtree
                treeList.Nodes.Add(stopNode);

                // Add leaves (operations) to stop
                if (operationList.Operations != null)
                {
                    foreach (var operation in operationList.Operations)
                    {
                        // create a node for the operation
                        var operationNode = new BoundNode((int)TemplateIndex.Operation, (int)TemplateIndex.OperationSelected, operation, operationDetailsMapping)
                        {
                            // hide its subnodes
                            HidePlusMinus = true
                        };

                        stopNode.Nodes.Add(operationNode); // add it to AdvancedTree
                    }
                }
            }
        }



        private IList<CustomOperationList> CreateCustomOperationsList()
        {
            IList<CustomOperationList> list = null;

            if (_operationList != null)
            {
                list = new List<CustomOperationList>();
                //list = new List<CustomOperationList> {AddTestDataForOrderTab()};
                //added for order list..
                foreach (OperationListStop operationListStop in _operationList.Stop)
                {
                    if (operationListStop.StopInformation != null)
                    {
                        var customOperationList = new CustomOperationList
                        {
                            StopId = operationListStop.StopInformation.StopId,
                            StopAddress = GetStopTimeAddress(operationListStop.StopInformation.StopId),
                            StopStatus = "0"
                        };

                        if (operationListStop.PlannedOperation != null && operationListStop.PlannedOperation.Length > 0)
                        {
                            if (operationListStop.PlannedOperation.Length > 1)
                            {
                                IList<OperationDetails> listOperation = operationListStop.PlannedOperation.Select(plannedOperation => new OperationDetails
                                {
                                    StopId = operationListStop.StopInformation.StopId,
                                    OperationAddress = GetStopTimeAddress(operationListStop.StopInformation.StopId, plannedOperation.OperationId),
                                    OperationId = plannedOperation.OperationId,
                                    OperationStatus = "1",
                                    OperationSign = "3"
                                }).ToList();
                                customOperationList.Operations = listOperation;

                            }
                            else
                            {
                                //customOperationList.IsLoadListOperation = true;
                                customOperationList.Operations = null;
                                customOperationList.OperationId = operationListStop.PlannedOperation[0].OperationId;
                                customOperationList.StopSign = "3";
                            }
                        }
                        list.Add(customOperationList);
                    }
                }
            }
            return list;

        }

        private CustomOperationList AddTestDataForOrderTab()
        {
            var customOperationList = new CustomOperationList
            {
                //StopAddress = GetStopTimeAddress(operationListStop.StopInformation.StopId),
                StopStatus = "1",
                //StopSequence = GetOperationListOperationStopSquence(operationListStop.StopInformation.StopSequence),
                Operations = null,
                StopSign = "3",
                OrderId = "1223",
                IsMultipleStopOrder = true,
                IsLoadListOperation = false,
                OrderSecondStopSign = "3",
                OrderDetail = " gvzvgxhhjgvhjgvhxvvv ds gvz" + Environment.NewLine + "dfdsfdsfafdsfa",
            };
            return customOperationList;
        }


        public OperationListStop GetStop(String stopId)
        {
            OperationListStop result = null;
            if (_operationList != null && _operationList.Stop != null)
            {
                result = (from n in _operationList.Stop
                          where n.StopInformation != null && n.StopInformation.StopId == stopId
                          select n).SingleOrDefault<OperationListStop>();
            }

            return result;
        }

        public PlannedOperationType GetPlannedOperation(String stopId, string operationId)
        {
            var operationListForStop = GetStop(stopId);
            if (operationListForStop == null)
            {
                return null;
            }

            var operation = (from n in operationListForStop.PlannedOperation
                             where n.OperationId == operationId
                             select n).SingleOrDefault<PlannedOperationType>();

            return operation;
        }


        private string GetStopTimeAddress(string stopId)
        {
            string stopAddressTime = string.Empty;
            if (_operationList != null && _operationList.Stop != null)
            {
                OperationListStop operationListStop = GetStop(stopId);

                if (operationListStop != null)
                {
                    if (operationListStop.StopInformation != null)
                    {
                        stopAddressTime = String.Format("{0:t}-{1:t}\n", operationListStop.StopInformation.EarliestStopTime,
                                                        operationListStop.StopInformation.LatestStopTime);

                        if (operationListStop.StopInformation.Address != null)
                        {
                            stopAddressTime = string.Format("{0}{1},{2} {3}", stopAddressTime, operationListStop.StopInformation.Address.StreetName,
                                operationListStop.StopInformation.Address.PostalCode, operationListStop.StopInformation.Address.PostalName);
                        }
                    }
                }
            }
            return stopAddressTime;
        }

        private string GetStopTimeAddress(string stopId, string operationId)
        {
            string operationAddressTime = string.Empty;
            if (_operationList != null && _operationList.Stop != null)
            {
                OperationListStop operationListStop = GetStop(stopId);

                if (operationListStop != null)
                {
                    PlannedOperationType plannedOperationType = GetPlannedOperation(stopId, operationId);
                    if (plannedOperationType != null)
                    {
                        operationAddressTime = String.Format("{0:t}-{1:t}\n", plannedOperationType.EarliestStartTime,
                                          plannedOperationType.LatestStartTime);
                    }
                }
            }
            return operationAddressTime;
        }

        private IEnumerable<CustomOperationList> TestData()
        {
            var operations1 = new OperationDetails { OperationAddress = "central station1", OperationStatus = "0", OperationId = "OperationId1", OperationSign = "3", StopId = "StopId1" };
            var operations2 = new OperationDetails { OperationAddress = "central station2", OperationStatus = "1", OperationId = "OperationId2", OperationSign = "3", StopId = "StopId2" };
            var operations3 = new OperationDetails { OperationAddress = "central station3", OperationStatus = "2", OperationId = "OperationId3", OperationSign = "3", StopId = "StopId3" };


            IList<OperationDetails> listOperation = new List<OperationDetails>();
            listOperation.Add(operations1);
            listOperation.Add(operations2);
            listOperation.Add(operations3);




            var operationList1 = new CustomOperationList { StopAddress = "oslo1", Operations = listOperation, StopStatus = "0", StopId = "StopId1" };
            var operationList2 = new CustomOperationList { StopAddress = "oslo2", Operations = listOperation, StopStatus = "1", StopId = "StopId2" };
            var operationList3 = new CustomOperationList { StopAddress = "oslo3", Operations = listOperation, StopStatus = "2", StopId = "StopId3" };

            var operationList4 = new CustomOperationList { StopAddress = "oslo4", Operations = null, StopSign = "3", StopStatus = "2", StopId = "StopId4", OperationId = "OperationId4" };
            var operationList5 = new CustomOperationList { StopAddress = "oslo5", Operations = null, StopSign = "3", StopStatus = "2", StopId = "StopId5", OperationId = "OperationId5" };
            var operationList6 = new CustomOperationList { StopAddress = "oslo6", Operations = null, StopSign = "3", StopStatus = "2", StopId = "StopId6", OperationId = "OperationId6" };
            IList<CustomOperationList> list = new List<CustomOperationList>();
            list.Add(operationList1);
            list.Add(operationList2);
            list.Add(operationList3);
            list.Add(operationList4);
            list.Add(operationList5);
            list.Add(operationList6);
            return list;
        }


        /// <summary>
        /// Happens whenever we click on a field in the tree control
        /// </summary>
        private void CellClick(object sender, CellEventArgs e)
        {
            treeList.SuspendRedraw();
            switch ((TemplateIndex)e.Node.CurrentTemplateIndex)
            {
                // The root node for a multi operation stop
                case TemplateIndex.StopMultiOperation:
                    if (treeList.SelectedNode != null)
                    {
                        CollapsePrevious(e.Node);
                    }

                    if (e.Node.IsExpanded == false)
                    {
                        e.Node.Expand();
                        // Select first leaf node if none already selected

                        Node leafNode = e.Node.Nodes.Cast<Node>().FirstOrDefault(childNode => childNode.Selected);
                        if (leafNode == null)
                        {
                            e.Node.Nodes[0].Selected = true;
                            // Ensure visibility of at least second node 
                            leafNode = e.Node.Nodes[1];
                        }
                        treeList.EnsureVisible(leafNode);
                    }
                    else
                        e.Node.Collapse();

                    break;

                // This will never happen as a multi operation stop is never selected
                // The first operation inside stop gets programtically selected
                case TemplateIndex.StopMultiOperationSelected:
                    break;

                // Operation in multi stop
                case TemplateIndex.Operation:
                    e.Node.Selected = true;
                    // Ensure visibility of next node if have one
                    treeList.EnsureVisible(e.NodeIndex < e.Node.Parent.Nodes.Count - 1
                                               ? e.Node.Parent.Nodes[e.NodeIndex + 1] : e.Node);
                    break;

                case TemplateIndex.OperationSelected:
                    //e.Node.Parent["LastSelected"] = e.NodeIndex;
                    break;

                case TemplateIndex.StopSingleOperation:
                    CollapsePrevious(e.Node);
                    e.Node.Selected = true;

                    EnsureSingleOperationVisible(e);

                    break;

                // Not much to do, as we dont let user deselect a node
                case TemplateIndex.StopSingleOperationSelected:
                    EnsureSingleOperationVisible(e);
                    break;
            }

            treeList.ResumeRedraw();
        }

        private void EnsureSingleOperationVisible(CellEventArgs e)
        {
            if (treeList.Nodes.Count > e.NodeIndex + 1)
                treeList.EnsureVisible(treeList.Nodes[e.NodeIndex + 1]);
            else
                treeList.EnsureVisible(e.Node);
        }

        // Collapse multi operation stop, if another branch was expanded
        private void CollapsePrevious(Node clickedNode)
        {
            // Check if we clicked on parent of selected
            if (treeList.SelectedNode != null && treeList.SelectedNode.Parent != clickedNode)
            {
                if (treeList.SelectedNode.Parent != null)
                    treeList.SelectedNode.Parent.Collapse();
            }
        }

        private void BtnPopUpOkClick(object sender, EventArgs e)
        {

        }
    }

    




}
﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Setting.Views
{
    public partial class FormChangeRole : Form
    {
        public FormChangeRole()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            TranslateUi();

            AddRoles();
            SelectActiveRole("Kunden");
        }

        // TODO: Set activerole and button enabled on shown

        private void AddRoles()
        {
            List<string> userRoles = new List<string>();

            userRoles.Add("Landpostbud");
            userRoles.Add("Terminalarbeider");
            userRoles.Add("Sjåfør");
            userRoles.Add("Kunde");

            userRoles.Sort();

            foreach (var userRole in userRoles)
            {
                comboBoxRole.Items.Add(userRole);
            }
        }

        private void SelectActiveRole(string role)
        {
            if (string.IsNullOrEmpty(role) == false)
            {
                comboBoxRole.SelectedIndex = comboBoxRole.Items.IndexOf(role);
            }
        }


        private void TranslateUi()
        {
            buttonOk.Text = GlobalTexts.Ok;
            buttonCancel.Text = GlobalTexts.Cancel;
            labelModuleName.Text = GlobalTexts.ChangeRole;
            labelActiveRole.Text = GlobalTexts.ActiveRole;

            if (labelModuleName.Parent != null)
                labelModuleName.Left = ((labelModuleName.Parent.Right - labelModuleName.Parent.Left) / 2) - (labelModuleName.Right - labelModuleName.Left) / 2;
        }
        
        private void OnButtonCancelClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OnButtonOkClick(object sender, EventArgs e)
        {
            Close();
        }

        private void OnChangedRole(object sender, EventArgs e)
        {
            buttonOk.Enabled = true;
        }

        

    }
}
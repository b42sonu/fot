﻿using Resco.Controls.SmartGrid;

namespace SampleScreenDesigner.Forms
{
    partial class FormChooseProductGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.cmbProductGroup = new System.Windows.Forms.ComboBox();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignmentItemValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignmentValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsigment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignmentItem = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItemValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsigment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.cmbProductGroup);
            this.touchPanel.Controls.Add(this.labelTask);
            this.touchPanel.Controls.Add(this.labelConsignmentItemValue);
            this.touchPanel.Controls.Add(this.labelConsignmentValue);
            this.touchPanel.Controls.Add(this.labelConsigment);
            this.touchPanel.Controls.Add(this.labelConsignmentItem);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.GotFocus += new System.EventHandler(this.touchPanel_GotFocus);
            // 
            // cmbProductGroup
            // 
            this.cmbProductGroup.Location = new System.Drawing.Point(132, 120);
            this.cmbProductGroup.Name = "cmbProductGroup";
            this.cmbProductGroup.Size = new System.Drawing.Size(251, 41);
            this.cmbProductGroup.TabIndex = 49;
            // 
            // labelTask
            // 
            this.labelTask.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelTask.Location = new System.Drawing.Point(129, 95);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(271, 24);
            this.labelTask.Text = "Choose Product Category:";
            // 
            // labelConsignmentItemValue
            // 
            this.labelConsignmentItemValue.AutoSize = false;
            this.labelConsignmentItemValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignmentItemValue.Location = new System.Drawing.Point(197, 261);
            this.labelConsignmentItemValue.Name = "labelConsignmentItemValue";
            this.labelConsignmentItemValue.Size = new System.Drawing.Size(271, 42);
            // 
            // labelConsignmentValue
            // 
            this.labelConsignmentValue.AutoSize = false;
            this.labelConsignmentValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignmentValue.Location = new System.Drawing.Point(160, 309);
            this.labelConsignmentValue.Name = "labelConsignmentValue";
            this.labelConsignmentValue.Size = new System.Drawing.Size(308, 38);
            // 
            // labelConsigment
            // 
            this.labelConsigment.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelConsigment.Location = new System.Drawing.Point(10, 314);
            this.labelConsigment.Name = "labelConsigment";
            this.labelConsigment.Size = new System.Drawing.Size(144, 24);
            this.labelConsigment.Text = "Consignment:";
            // 
            // labelConsignmentItem
            // 
            this.labelConsignmentItem.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelConsignmentItem.Location = new System.Drawing.Point(10, 274);
            this.labelConsignmentItem.Name = "labelConsignmentItem";
            this.labelConsignmentItem.Size = new System.Drawing.Size(181, 24);
            this.labelConsignmentItem.Text = "ConsignmentItem";
            this.labelConsignmentItem.ParentChanged += new System.EventHandler(this.labelConsignmentItem_ParentChanged);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 484);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(238, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(242, 484);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(238, 50);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.ButtonConfirmClick);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(106, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(297, 27);
            this.labelModuleName.Text = "Change Product Category";
            // 
            // labelConsignment
            // 
            this.labelConsignment.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignment.Location = new System.Drawing.Point(22, 125);
            this.labelConsignment.Name = "labelConsignment";
            this.labelConsignment.Size = new System.Drawing.Size(251, 24);
            this.labelConsignment.Text = "Consignment/item number";
            // 
            // labelMeasure
            // 
            this.labelMeasure.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelMeasure.Location = new System.Drawing.Point(22, 88);
            this.labelMeasure.Name = "labelMeasure";
            this.labelMeasure.Size = new System.Drawing.Size(94, 24);
            this.labelMeasure.Text = "Measure:";
            // 
            // labelCause
            // 
            this.labelCause.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelCause.Location = new System.Drawing.Point(22, 51);
            this.labelCause.Name = "labelCause";
            this.labelCause.Size = new System.Drawing.Size(72, 24);
            this.labelCause.Text = "Cause:";
            // 
            // FormChooseProductGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormChooseProductGroup";
            this.Text = "FormAttemptedDeliveryList";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItemValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsigment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignment;
        private Resco.Controls.CommonControls.TransparentLabel labelMeasure;
        private Resco.Controls.CommonControls.TransparentLabel labelCause;
        private System.Windows.Forms.ComboBox cmbProductGroup;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentItemValue;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentValue;
        private Resco.Controls.CommonControls.TransparentLabel labelConsigment;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentItem;
    }
}
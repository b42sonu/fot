﻿//using System;
//using System.Drawing;
//using System.Windows.Forms;
//using SampleScreenDesigner.Utility;
//using Resco.Controls.ImageBox;
//using System.IO;

//namespace SampleScreenDesigner.Forms
//{
//    public partial class FormTakePhoto : Form
//    {
//        public FormTakePhoto()
//        {
//            InitializeComponent();
//            //string path = @"\My Documents\thumb\20121220-165513.jpg";
//            //AddImage(path);
//        }

//        private void ButtonBackClick(object sender, EventArgs e)
//        {
//            SendBackPannel();
//        }

//        private void SendBackPannel()
//        {
//            panelPopUp.Visible = false;
//            panelPopUp.SendToBack();
//            btnOk.Enabled = true;
//            btnBack.Enabled = true;
//        }



//        private void ButtonConfirmClick(object sender, EventArgs e)
//        {
//            ShowCamera.Instance.InitializeCamera(null, panelCamera);
//            BringToFrontPannel();
//        }

//        private void BringToFrontPannel()
//        {
//            panelPopUp.Visible = true;
//            panelPopUp.BringToFront();
//            btnOk.Enabled = false;
//            btnBack.Enabled = false;
//        }

//        private void BtnSaveClick(object sender, EventArgs e)
//        {
//            if (!Directory.Exists(@"\My Documents\thumb"))
//                Directory.CreateDirectory(@"\My Documents\thumb");
//            string imagePath = ShowCamera.Instance.TakeSnapshot(@"\My Documents");
//            AddImage(imagePath);
//            SendBackPannel();
//        }

//        private void AddImage(string imagePath)
//        {
//            var compactImage = new CompactImage();
//            if (File.Exists(imagePath))
//            {
//                compactImage.Load(imagePath);
//                int quotient = touchPanelPhotos.Controls.Count / 3;
//                int remainder = touchPanelPhotos.Controls.Count % 3;
               
//                var pointImageBox = new Point { X = remainder == 0 ? 8 : 146 * remainder, Y = quotient == 0 ? 4 : 105 * quotient };

//                var imageBox = new ImageBox
//                                   {
//                                       Location = pointImageBox,//new Point(8, 4),
//                                       Name = "imageBox1",
//                                       Size = new Size(128, 95),
//                                       TabIndex = 0,
//                                       CompactImage = compactImage
//                                   };


//                touchPanelPhotos.Controls.Add(imageBox);
//            }
//        }

//        private void BtnDiscardClick(object sender, EventArgs e)
//        {
//            SendBackPannel();
//        }

//    }
//}
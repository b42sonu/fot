﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using SampleScreenDesigner.Properties;

namespace Com.Bring.PMP.PreComFW.PickUp.Views
{
    public partial class FormVerifyPowerUnitId : Form
    {
        public FormVerifyPowerUnitId()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
        }

        private void OnShow()
        {
            
        }
     
        private void ButtonBackClick(object sender, EventArgs e)
        {
            
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {

        }
    }
}
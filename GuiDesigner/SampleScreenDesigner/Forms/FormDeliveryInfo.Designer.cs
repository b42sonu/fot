﻿using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Forms
{
    partial class FormDeliveryInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnDeparture = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnScan = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsignmentItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignorDetails = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignorHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeDetails = new Resco.Controls.CommonControls.TransparentLabel();
            this.listConsignmentItems = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellConsignmentNumber = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellAlternateConsignmentNumber = new Resco.Controls.AdvancedList.TextCell();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeparture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignorDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignorHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Operation Details";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // btnDeparture
            // 
            this.btnDeparture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDeparture.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDeparture.ForeColor = System.Drawing.Color.White;
            this.btnDeparture.Location = new System.Drawing.Point(161, 483);
            this.btnDeparture.Name = "btnDeparture";
            this.btnDeparture.Size = new System.Drawing.Size(158, 50);
            this.btnDeparture.TabIndex = 30;
            this.btnDeparture.Text = "Departure";
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            // 
            // btnScan
            // 
            this.btnScan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnScan.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnScan.ForeColor = System.Drawing.Color.White;
            this.btnScan.Location = new System.Drawing.Point(322, 483);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(158, 50);
            this.btnScan.TabIndex = 32;
            this.btnScan.Text = "Scan";
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.lblConsignmentItems);
            this.touchPanel.Controls.Add(this.lblConsignorDetails);
            this.touchPanel.Controls.Add(this.lblConsignorHeading);
            this.touchPanel.Controls.Add(this.btnScan);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnDeparture);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblConsigneeHeading);
            this.touchPanel.Controls.Add(this.lblConsigneeDetails);
            this.touchPanel.Controls.Add(this.listConsignmentItems);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            // 
            // lblConsignmentItems
            // 
            this.lblConsignmentItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItems.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsignmentItems.Location = new System.Drawing.Point(4, 60);
            this.lblConsignmentItems.Name = "lblConsignmentItems";
            this.lblConsignmentItems.Size = new System.Drawing.Size(112, 14);
            this.lblConsignmentItems.Text = "Consignment Items";

            // 
            // lblConsignorHeading
            // 
            this.lblConsignorHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignorHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsignorHeading.Location = new System.Drawing.Point(4, 380);
            this.lblConsignorHeading.Name = "lblConsignorHeading";
            this.lblConsignorHeading.Size = new System.Drawing.Size(64, 15);
            this.lblConsignorHeading.Text = "Consignor:";

            // 
            // lblConsignorDetails
            // 
            this.lblConsignorDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignorDetails.Location = new System.Drawing.Point(4, 400);
            this.lblConsignorDetails.Name = "lblConsignorDetails";
            this.lblConsigneeDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignorDetails.Size = new System.Drawing.Size(0, 45);
            this.lblConsignorDetails.Text = "Rakesh";
           
            // 
            // lblConsigneeHeading
            // 
            this.lblConsigneeHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsigneeHeading.Location = new System.Drawing.Point(4, 290);
            this.lblConsigneeHeading.Name = "lblConsigneeHeading";
            this.lblConsigneeHeading.Size = new System.Drawing.Size(66, 15);
            this.lblConsigneeHeading.Text = "Consignee:";
            // 
            // lblConsigneeDetails
            // 
            this.lblConsigneeDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeDetails.Location = new System.Drawing.Point(4, 310);
            this.lblConsigneeDetails.Name = "lblConsigneeDetails";
            this.lblConsigneeDetails.Size = new System.Drawing.Size(0, 45);
            this.lblConsigneeDetails.Text = "Aggarwal";
            // 
            // listConsignmentItems
            // 
            this.listConsignmentItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listConsignmentItems.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listConsignmentItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listConsignmentItems.DataRows.Clear();
            this.listConsignmentItems.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listConsignmentItems.Location = new System.Drawing.Point(0, 87);
            this.listConsignmentItems.MultiSelect = true;
            this.listConsignmentItems.Name = "listConsignmentItems";
            this.listConsignmentItems.ScrollbarSmallChange = 32;
            this.listConsignmentItems.ScrollbarWidth = 26;
            this.listConsignmentItems.SelectedTemplateIndex = 2;
            this.listConsignmentItems.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listConsignmentItems.ShowHeader = true;
            this.listConsignmentItems.Size = new System.Drawing.Size(480, 180);
            this.listConsignmentItems.TabIndex = 2;
            this.listConsignmentItems.TemplateIndex = 1;
            this.listConsignmentItems.Templates.Add(this.RowTemplateHeader);
            this.listConsignmentItems.Templates.Add(this.RowTemplatePlannedOp);
            this.listConsignmentItems.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.listConsignmentItems.TouchScrolling = true;
            this.listConsignmentItems.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation Details";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellConsignmentNumber);
            this.RowTemplatePlannedOp.Height = 40;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // textCellConsignmentNumber
            // 
            this.textCellConsignmentNumber.CellSource.ColumnName = "ConsignmentItemNumber";
            this.textCellConsignmentNumber.DesignName = "textCellConsignmentNumber";
            this.textCellConsignmentNumber.Location = new System.Drawing.Point(15, 0);
            this.textCellConsignmentNumber.Size = new System.Drawing.Size(370, 40);
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateConsignmentNumber);
            this.RowTemplatePlannedOpAlternateTemp.Height = 40;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // textCellAlternateConsignmentNumber
            // 
            this.textCellAlternateConsignmentNumber.CellSource.ColumnName = "ConsignmentItemNumber";
            this.textCellAlternateConsignmentNumber.DesignName = "textCellAlternateConsignmentNumber";
            this.textCellAlternateConsignmentNumber.Location = new System.Drawing.Point(15, 0);
            this.textCellAlternateConsignmentNumber.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateConsignmentNumber.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateConsignmentNumber.Size = new System.Drawing.Size(370, 40);
            // 
            // FormDeliveryInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormDeliveryInfo";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDeparture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnScan)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignorDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignorHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeDetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        
        private Resco.Controls.OutlookControls.ImageButton btnDeparture;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnScan;

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listConsignmentItems;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeDetails;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignorDetails;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignorHeading;
        
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;
        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.TextCell textCellConsignmentNumber;
      
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateConsignmentNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItems;
    }
}
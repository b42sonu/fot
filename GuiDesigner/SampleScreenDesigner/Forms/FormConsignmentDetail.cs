﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.OperationList.Views
{
    public partial class FormConsignmentDetail : Form
    {
        #region "Private Variables"
        
        System.Drawing.Point _packagePoints = new System.Drawing.Point(0, 0);       
        const int ControlHeight = 44;
       

        #endregion

        #region "Constructor"
        public FormConsignmentDetail()
        {
            
            InitializeComponent();
            //SetHeaderLabels("Task Details");
           
            BindConsignmentsToControl();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
        }

        #endregion

        #region "Methods"
        /// <summary>
        /// Binding the Consignments No's to the ListView Control
        /// </summary>
        private void BindConsignmentsToControl()
        {
            lstBoxConsignmentNo.Items.Add("Test 1");
            lstBoxConsignmentNo.Items.Add("Test 2");
            lstBoxConsignmentNo.Items.Add("Test 3");
            AddControlForDeliveryInstructions();
        }

        /// <summary>
        /// To add the Delivery instructions field that shows the detail of the description of the selected stop.
        /// </summary>
        private void AddControlForDeliveryInstructions()
        {           
            Label lblDeliveryInstructions = new Label();
            lblDeliveryInstructions.Text = "stop info";
            lblDeliveryInstructions.Size = new System.Drawing.Size(445, 80);
            lblDeliveryInstructions.Location = _packagePoints;
            lblDeliveryInstructions.Tag = "Deleted";
            lblDeliveryInstructions.Dock = System.Windows.Forms.DockStyle.Bottom;
            //pnlConsignmentDetails.Controls.Add(lblDeliveryInstructions);
        }
        #endregion

        #region "Events"
        /// <summary>
        /// This event will get fired whenever we will click on the back button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBackClick(object sender, EventArgs e)
        {
          
        }

        /// <summary>
        /// This event will get fired whenever we will click on the departure button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDepartureClick(object sender, EventArgs e)
        {
         
        }

        /// <summary>
        /// This event will get fired whenever we will click on the scan button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnScanClick(object sender, EventArgs e)
        {

        }

        #endregion

        private void BtnBackClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ListBoxConsignmentSelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
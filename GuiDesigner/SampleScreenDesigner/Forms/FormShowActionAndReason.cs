﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Windows.Forms;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{

   

    public partial class FormShowActionAndReason : Form
    {
        
        TabMultipleButtons _tabButtons = new TabMultipleButtons();
        #region Methods and Functions

        /// <summary>
        /// 
        /// </summary>
        public FormShowActionAndReason()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            lblReason.Text = GlobalTexts.Reason + ":";
            lblReasonValue.Text = GlobalTexts.SelectReasonValue;
            lblAction.Text = GlobalTexts.Action+":";
            lblActionValue.Text = GlobalTexts.SelectedActionValue;
            lblFreeText.Text = GlobalTexts.FreeText+":";
           _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

      
        #endregion

        public void ClearText()
        {
            lblReasonValue.Text = string.Empty;
            lblActionValue.Text = string.Empty;
            txtFreeText.Text = string.Empty;
        }

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ClearText();
                
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        #endregion

    }

    
}
﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Setting.Views
{
    public partial class FormTmiSettings : Form
    {
        public FormTmiSettings()
        {
            InitializeComponent();

            smartGridTmiSettings.DataSource = TmiSettings;

        }
        
        public IList<TmiSetting> TmiSettings
        {
            get
            {
                List<TmiSetting> tmiSettings = new List<TmiSetting>();

                tmiSettings.Add(new TmiSetting { TmiId = "IsPickupOfUnplannedConsignmentsAllowed", Value = "false"});
                tmiSettings.Add(new TmiSetting { TmiId = "IsMultipleConsignmentsAllowed", Value = "true" });
                tmiSettings.Add(new TmiSetting { TmiId = "UpdatePackageTypeEventCode", Value = "A" });

                return tmiSettings;
            }
        }
    }

    public class TmiSetting
    {
        public string TmiId { get; set; }
        public string Value { get; set; }
        public string ValueAsString
        {
            get
            {
                if (Value.ToUpper() == "FALSE" || Value.ToUpper() == "TRUE")
                    return Value.ToLower();

                return Value;
            }
            set { Value = value; }
        }
    }
}
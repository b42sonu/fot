﻿using System.Windows.Forms;
namespace SampleScreenDesigner.Forms
{
    partial class FormOperationChanges
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.lblProcessHeader = new Resco.Controls.CommonControls.TransparentLabel();
            this.tabMultipleButtons = new SampleScreenDesigner.Controls.TabMultipleButtons();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblProcessHeader)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.tabMultipleButtons);
            this.touchPanel.Controls.Add(this.webBrowser);
            this.touchPanel.Controls.Add(this.lblProcessHeader);
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 536, 480, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(464, 0, 16, 536);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(22, 47);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(438, 446);
            // 
            // lblProcessHeader
            // 
            this.lblProcessHeader.AutoSize = false;
            this.lblProcessHeader.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblProcessHeader.Location = new System.Drawing.Point(0, 2);
            this.lblProcessHeader.Name = "lblProcessHeader";
            this.lblProcessHeader.Size = new System.Drawing.Size(480, 30);
            this.lblProcessHeader.Text = "OP Region Oslo Og Producksio";
            this.lblProcessHeader.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // tabMultipleButtons
            // 
            this.tabMultipleButtons.Location = new System.Drawing.Point(0, 499);
            this.tabMultipleButtons.Name = "tabMultipleButtons";
            this.tabMultipleButtons.Size = new System.Drawing.Size(480, 50);
            this.tabMultipleButtons.TabIndex = 2;
            // 
            // FormOperationChanges
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormOperationChanges";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblProcessHeader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblProcessHeader;
        private WebBrowser webBrowser;
        private SampleScreenDesigner.Controls.TabMultipleButtons tabMultipleButtons;
    }
}
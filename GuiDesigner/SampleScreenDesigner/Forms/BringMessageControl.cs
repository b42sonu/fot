﻿using System.Drawing;
using System.Windows.Forms;
using MobileGui.Properties;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public partial class BringMessageControl : UserControl
    {
        public class MessageHolder
        {
            public MessageHolder()
            {
                Text = string.Empty;
                State = State.Empty;
            }

            public MessageHolder(string text, State state)
            {
                Text = text;
                State = state;
            }

            public string Text;
            public State State;
        }

        public enum State
        {
            Empty,
            Error,
            Information,
            Warning,
            ModalMessage
        }


        public BringMessageControl()
        {
            InitializeComponent();
            Visible = false;
        }

        /// <summary>
        /// Specifies the text for message as set by user.
        /// </summary>
        public string MessageText
        {
            get { return labelMessage.Text.Trim(); }
            set { labelMessage.Text = value; }
        }

        /// <summary>
        /// Specifies the type of message as set by user.
        /// </summary>
        public State CurrentState { get; set; }

        
        public MessageHolder Message
        {
            get { return new MessageHolder(MessageText, CurrentState); }
            set { MessageText = value.Text; CurrentState = value.State; }
        }


        public Color SetLableFontColor
        {
            get { return labelMessage.ForeColor; }
            set { labelMessage.ForeColor = value; }
        }


        public void ShowMessage(string messageText, State state)
        {
            MessageText = messageText;
            CurrentState = state;

            switch (state)
            {
                case State.Empty:
                    Visible = false;
                    break;
                case State.Error:
                    pictureBoxIcon.Image = Resources.FOT_icon_error;

                    Visible = true;
                    break;
                case State.Warning:
                    pictureBoxIcon.Image = Resources.FOT_icon_attention;
                    Visible = true;
                    break;
                case State.Information:
                    pictureBoxIcon.Image = Resources.FOT_icon_verified_ok;
                    Visible = true;
                    break;
                default:
                    System.Diagnostics.Debug.Fail("Unhandled messagetype detected: " + state.ToString());
                    break;

            }
        }

        private void BringMessageControl_Click(object sender, System.EventArgs e)
        {

        }

    }
}


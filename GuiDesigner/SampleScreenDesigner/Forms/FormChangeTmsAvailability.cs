﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Setting.Views
{
    public partial class FormChangeTmsAvailability : Form
    {
        public FormChangeTmsAvailability()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            messageControl.ShowMessage("test", MessageState.Error);
        }

        
        void CleanUp()
        {}

    }
}
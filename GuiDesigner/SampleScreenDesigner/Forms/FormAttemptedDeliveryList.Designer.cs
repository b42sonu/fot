﻿using Resco.Controls.SmartGrid;

namespace SampleScreenDesigner.Forms
{
    partial class FormAttemptedDeliveryList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.smartGridConsignment = new Resco.Controls.SmartGrid.SmartGrid();
            this.columnConsignmentBool = new Resco.Controls.SmartGrid.Column();
            this.columnConsignmentValueString = new Resco.Controls.SmartGrid.Column();
            this.labelMeasureValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCauseValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.labelConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasureValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCauseValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.smartGridConsignment);
            this.touchPanel.Controls.Add(this.labelMeasureValue);
            this.touchPanel.Controls.Add(this.labelCauseValue);
            this.touchPanel.Controls.Add(this.labelCause);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.labelConsignment);
            this.touchPanel.Controls.Add(this.labelMeasure);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // smartGridConsignment
            // 
            this.smartGridConsignment.BorderSize = 2;
            this.smartGridConsignment.ColumnHeaderHeight = 0;
            this.smartGridConsignment.Columns.Add(this.columnConsignmentBool);
            this.smartGridConsignment.Columns.Add(this.columnConsignmentValueString);
            this.smartGridConsignment.Location = new System.Drawing.Point(22, 152);
            this.smartGridConsignment.Name = "smartGridConsignment";
            this.smartGridConsignment.RowHeaderWidth = 24;
            this.smartGridConsignment.RowHeight = 32;
            this.smartGridConsignment.ScrollHeight = 24;
            this.smartGridConsignment.ScrollWidth = 24;
            this.smartGridConsignment.Size = new System.Drawing.Size(436, 294);
            this.smartGridConsignment.TabIndex = 36;
            this.smartGridConsignment.Text = "smartGrid1";
            // 
            // columnConsignmentBool
            // 
            this.columnConsignmentBool.CellEdit = Resco.Controls.SmartGrid.CellEditType.CheckBox;
            this.columnConsignmentBool.DataMember = "IsChecked";
            this.columnConsignmentBool.GridLine = false;
            this.columnConsignmentBool.MinimumWidth = 10;
            this.columnConsignmentBool.Name = "columnConsignmentBool";
            this.columnConsignmentBool.Width = 30;
            // 
            // columnConsignmentValueString
            // 
            this.columnConsignmentValueString.DataMember = "ConsignmentOrConsignmentItemNumber";
            this.columnConsignmentValueString.MinimumWidth = 10;
            this.columnConsignmentValueString.Name = "columnConsignmentValueString";
            this.columnConsignmentValueString.Width = 400;
            // 
            // labelMeasureValue
            // 
            this.labelMeasureValue.AutoSize = false;
            this.labelMeasureValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelMeasureValue.Location = new System.Drawing.Point(122, 88);
            this.labelMeasureValue.Name = "labelMeasureValue";
            this.labelMeasureValue.Size = new System.Drawing.Size(355, 42);
            // 
            // labelCauseValue
            // 
            this.labelCauseValue.AutoSize = false;
            this.labelCauseValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelCauseValue.Location = new System.Drawing.Point(122, 51);
            this.labelCauseValue.Name = "labelCauseValue";
            this.labelCauseValue.Size = new System.Drawing.Size(355, 38);
            // 
            // labelCause
            // 
            this.labelCause.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelCause.Location = new System.Drawing.Point(22, 51);
            this.labelCause.Name = "labelCause";
            this.labelCause.Size = new System.Drawing.Size(72, 24);
            this.labelCause.Text = "Cause:";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 484);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(238, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(242, 484);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(238, 50);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.ButtonConfirmClick);
            // 
            // labelConsignment
            // 
            this.labelConsignment.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelConsignment.Location = new System.Drawing.Point(22, 125);
            this.labelConsignment.Name = "labelConsignment";
            this.labelConsignment.Size = new System.Drawing.Size(251, 24);
            this.labelConsignment.Text = "Consignment/item number";
            // 
            // labelMeasure
            // 
            this.labelMeasure.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelMeasure.Location = new System.Drawing.Point(22, 88);
            this.labelMeasure.Name = "labelMeasure";
            this.labelMeasure.Size = new System.Drawing.Size(94, 24);
            this.labelMeasure.Text = "Measure:";
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(142, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(217, 27);
            this.labelModuleName.Text = "Attempted delivery";
            // 
            // FormAttemptedDeliveryList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormAttemptedDeliveryList";
            this.Text = "FormAttemptedDeliveryList";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasureValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCauseValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelMeasure;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignment;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.CommonControls.TransparentLabel labelCause;
        private Resco.Controls.CommonControls.TransparentLabel labelMeasureValue;
        private Resco.Controls.CommonControls.TransparentLabel labelCauseValue;
        private Resco.Controls.SmartGrid.SmartGrid smartGridConsignment;
        private Resco.Controls.SmartGrid.Column columnConsignmentBool;
        private Resco.Controls.SmartGrid.Column columnConsignmentValueString;
    }
}
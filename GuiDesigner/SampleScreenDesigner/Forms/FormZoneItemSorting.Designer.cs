﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.OutlookControls;
using PreCom.Utils;

namespace SampleScreenDesigner.Forms
{
    partial class FormZoneItemSorting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoadList));
            this.pnlMessage = new Panel();
            this.lblInfoHeading = new System.Windows.Forms.Label();
            this.picMessage = new System.Windows.Forms.PictureBox();
            this.btnZone1Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone2Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone3Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone4Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone5Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone6Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone7Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone8Details = new Resco.Controls.OutlookControls.ImageButton();


            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.btnNextZone = new Resco.Controls.OutlookControls.ImageButton();
            this.btnPrevZone = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDown = new Resco.Controls.OutlookControls.ImageButton();
            this.btnUp = new Resco.Controls.OutlookControls.ImageButton();
            this.btnChangeZone = new Resco.Controls.OutlookControls.ImageButton();
       
            this.btnCancelChangeZone = new ImageButton();
            
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.listDeliveries = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellSortNo = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateSortNo = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellAlternameOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.tabControlDeliveries = new Resco.Controls.CommonControls.TabControl();
            this.tabNotStarted = new Resco.Controls.CommonControls.TabPage();
            this.tabCompleted = new Resco.Controls.CommonControls.TabPage();
            this.tabAll = new Resco.Controls.CommonControls.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnNextZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).BeginInit();
            this.tabControlDeliveries.SuspendLayout();
            this.tabNotStarted.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Load List";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            this.labelHeading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelHeading.AutoSize = false;
            this.labelHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeading.Location = new System.Drawing.Point(11, 30);
            this.labelHeading.Name = "labelModuleName";
            this.labelHeading.Size = new System.Drawing.Size(459, 27);
            this.labelHeading.Text = "Click on Zone to assign selected row";
            this.labelHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;

            


            this.imageListOperationListIcons.Images.Clear();
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("Delivery"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            
            // 
            // touchPanel
            //
            this.touchPanel.Controls.Add(this.pnlMessage);
            this.touchPanel.Controls.Add(this.btnDown);
            this.touchPanel.Controls.Add(this.btnUp);
            this.touchPanel.Controls.Add(this.btnChangeZone);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.btnPrevZone);
            this.touchPanel.Controls.Add(this.btnNextZone);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.labelHeading);
            this.touchPanel.Controls.Add(listDeliveries);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);

            // 
            // pnlMessage
            // 
            this.pnlMessage.BackColor = System.Drawing.Color.White;
            this.pnlMessage.Location = new System.Drawing.Point(15, 90);
            this.pnlMessage.Name = "pnlButtons";
            this.pnlMessage.Controls.Add(this.lblInfoHeading);
            this.pnlMessage.Controls.Add(this.picMessage);
            this.pnlMessage.Controls.Add(this.btnZone1Details);
            this.pnlMessage.Controls.Add(this.btnZone2Details);
            this.pnlMessage.Controls.Add(this.btnZone3Details);
            this.pnlMessage.Controls.Add(this.btnZone4Details);
            this.pnlMessage.Controls.Add(this.btnZone5Details);
            this.pnlMessage.Controls.Add(this.btnZone6Details);
            this.pnlMessage.Controls.Add(this.btnZone7Details);
            this.pnlMessage.Controls.Add(this.btnZone8Details);
            this.pnlMessage.Controls.Add(this.btnCancelChangeZone);
            this.pnlMessage.Size = new System.Drawing.Size(450, 380);
            this.pnlMessage.Paint += PnlConfirmMessagePaint;

            

            // 
            // lblInfoHeading
            // 
            this.lblInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblInfoHeading.Location = new System.Drawing.Point(10, 90);
            this.lblInfoHeading.Name = "lblAccepted";
            this.lblInfoHeading.Size = new System.Drawing.Size(430, 30);
            this.lblInfoHeading.Text = "Select a row. Move it up/down";
            this.lblInfoHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;

            //  
            // picMessage
            // 
            this.picMessage.Image = SampleScreenDesigner.Properties.Resources.FOT_icon_attention;
            this.picMessage.Location = new System.Drawing.Point(200, 20);
            this.picMessage.Name = "picMessage";
            this.picMessage.Size = new System.Drawing.Size(50, 50);

            // 
            // btnZone1Details
            // 
            this.btnZone1Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone1Details.Location = new System.Drawing.Point(10, 140);
            this.btnZone1Details.Name = "btnZone1Details";
            this.btnZone1Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone1Details.TabIndex = 3;
            this.btnZone1Details.Text =  "1";
            this.btnZone1Details.Paint += PnlBtnZoneDetailsPaint;
            this.btnZone1Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone1Details.Click += new System.EventHandler(btnZone1Details_Click);
            // 
            // btnZone2Details
            //
            this.btnZone2Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone2Details.Location = new System.Drawing.Point(120, 140);
            this.btnZone2Details.Name = "btnZone1Details";
            this.btnZone2Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone2Details.TabIndex = 3;
            this.btnZone2Details.Text = "2";
            this.btnZone2Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone2Details.Paint += PnlBtnZoneDetailsPaint;

            // 
            // btnZone3Details
            // 
            this.btnZone3Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone3Details.Location = new System.Drawing.Point(230, 140);
            this.btnZone3Details.Name = "btnZone1Details";
            this.btnZone3Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone3Details.TabIndex = 3;
            this.btnZone3Details.Text = "3";
            this.btnZone3Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone3Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone4Details
            // 
            this.btnZone4Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone4Details.Location = new System.Drawing.Point(340, 140);
            this.btnZone4Details.Name = "btnZone1Details";
            this.btnZone4Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone4Details.TabIndex = 3;
            this.btnZone4Details.Text = "4";
            this.btnZone4Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone4Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone5Details
            // 
            this.btnZone5Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone5Details.Location = new System.Drawing.Point(10, 230);
            this.btnZone5Details.Name = "btnZone1Details";
            this.btnZone5Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone5Details.TabIndex = 3;
            this.btnZone5Details.Text = "5";
            this.btnZone5Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone5Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone6Details
            // 
            this.btnZone6Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone6Details.Location = new System.Drawing.Point(120, 230);
            this.btnZone6Details.Name = "btnZone1Details";
            this.btnZone6Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone6Details.TabIndex = 3;
            this.btnZone6Details.Text = "6";
            this.btnZone6Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone6Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone7Details
            // 
            this.btnZone7Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone7Details.Location = new System.Drawing.Point(230, 230);
            this.btnZone7Details.Name = "btnZone1Details";
            this.btnZone7Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone7Details.TabIndex = 3;
            this.btnZone7Details.Text = "7";
            this.btnZone7Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone7Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone8Details
            // 
            this.btnZone8Details.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnZone8Details.Location = new System.Drawing.Point(340, 230);
            this.btnZone8Details.Name = "btnZone1Details";
            this.btnZone8Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone8Details.TabIndex = 3;
            this.btnZone8Details.Text = "8";
            this.btnZone8Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone8Details.Paint += PnlBtnZoneDetailsPaint;


            // 
            // btnOk
            // 
            this.btnCancelChangeZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancelChangeZone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancelChangeZone.ForeColor = System.Drawing.Color.White;
            this.btnCancelChangeZone.Location = new System.Drawing.Point(146, 315);
            this.btnCancelChangeZone.Name = "btnCancelZoneItemSort";
            this.btnCancelChangeZone.Size = new System.Drawing.Size(158, 50);
            this.btnCancelChangeZone.TabIndex = 32;
            this.btnCancelChangeZone.Text = "Cancel";
            this.btnCancelChangeZone.Click += new System.EventHandler(btnCancelChangeZone_Click);


            // 
            // btnPredef
            // 
            this.btnNextZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnNextZone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnNextZone.ForeColor = System.Drawing.Color.White;
            this.btnNextZone.Location = new System.Drawing.Point(161, 483);
            this.btnNextZone.Name = "btnSortZone";
            this.btnNextZone.Size = new System.Drawing.Size(158, 50);
            this.btnNextZone.TabIndex = 30;
            this.btnNextZone.Text = "Next Zone";
            
            // 
            // btnBack
            // 
            this.btnPrevZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnPrevZone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnPrevZone.ForeColor = System.Drawing.Color.White;
            this.btnPrevZone.Location = new System.Drawing.Point(0, 483);
            this.btnPrevZone.Name = "btnBack";
            this.btnPrevZone.Size = new System.Drawing.Size(158, 50);
            this.btnPrevZone.TabIndex = 31;
            this.btnPrevZone.Text = "Prev Zone";
            
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(322, 483);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(158, 50);
            this.btnOk.TabIndex = 32;
            this.btnOk.Text = "Ok";
            

            // 
            // btnAsLoaded
            // 
            this.btnDown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDown.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDown.ForeColor = System.Drawing.Color.White;
            this.btnDown.Location = new System.Drawing.Point(1, 428);
            this.btnDown.Name = "btnAsLoaded";
            this.btnDown.Size = new System.Drawing.Size(158, 50);
            this.btnDown.TabIndex = 38;
            this.btnDown.Text = "Down";
            

            // 
            // btnZipCode
            // 
            this.btnUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnUp.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnUp.ForeColor = System.Drawing.Color.White;
            this.btnUp.Location = new System.Drawing.Point(322, 428);
            this.btnUp.Name = "btnZipCode";
            this.btnUp.Size = new System.Drawing.Size(158, 50);
            this.btnUp.TabIndex = 38;
            this.btnUp.Text = "Up";
            

            // 
            // btnReversed
            // 
            this.btnChangeZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnChangeZone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnChangeZone.ForeColor = System.Drawing.Color.White;
            this.btnChangeZone.Location = new System.Drawing.Point(161, 428);
            this.btnChangeZone.Name = "btnReversed";
            this.btnChangeZone.Size = new System.Drawing.Size(158, 50);
            this.btnChangeZone.TabIndex = 38;
            this.btnChangeZone.Text = "Change Zone";
            this.btnChangeZone.Click += new System.EventHandler(btnChangeZone_Click);
            
            
            // 
            // listDeliveries
            // 
            this.listDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listDeliveries.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listDeliveries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listDeliveries.DataRows.Clear();
            this.listDeliveries.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listDeliveries.Location = new System.Drawing.Point(0, 70);
            this.listDeliveries.MultiSelect = true;
            this.listDeliveries.Name = "listDeliveries";
            this.listDeliveries.ScrollbarSmallChange = 32;
            this.listDeliveries.ScrollbarWidth = 26;
            this.listDeliveries.SelectedTemplateIndex = 2;
            this.listDeliveries.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listDeliveries.ShowHeader = true;
            this.listDeliveries.Size = new System.Drawing.Size(480, 320);
            this.listDeliveries.TabIndex = 2;
            this.listDeliveries.TemplateIndex = 1;
            this.listDeliveries.Templates.Add(this.RowTemplateHeader);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOp);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.listDeliveries.TouchScrolling = true;
            this.listDeliveries.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationType);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationDetail);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationStatus);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationOperationId);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellSortNo);
            this.RowTemplatePlannedOp.Height = 64;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.ImageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationType.AutoResize = true;
            this.ImageCellOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellOperationType.DesignName = "ImageCellOperationType";
            this.ImageCellOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationType.Location = new System.Drawing.Point(57, 10);
            this.ImageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(100, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(327, 64);
            // 
            // ImageCellOperationStatus
            // 
            this.ImageCellOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellOperationStatus.DesignName = "ImageCellOperationStatus";
            this.ImageCellOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "DeliveryOperation.Consignment.ConsignmentNumber";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;

            // 
            // textCellSortNo
            // 
            this.textCellSortNo.CellSource.ColumnName = "DefaultSortNo";
            this.textCellSortNo.DesignName = "textCellSortNo";
            this.textCellSortNo.Location = new System.Drawing.Point(15, 15);
            this.textCellSortNo.Size = new System.Drawing.Size(40, 40);
            
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternateOperationType);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternameOperationStatus);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateSortNo);
            this.RowTemplatePlannedOpAlternateTemp.Height = 64;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.ImageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateOperationType.AutoResize = true;
            this.ImageCellAlternateOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.ImageCellAlternateOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternateOperationType.Location = new System.Drawing.Point(57, 10);
            this.ImageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(100, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(327, 64);
            // 
            // ImageCellAlternameOperationStatus
            // 
            this.ImageCellAlternameOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternameOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellAlternameOperationStatus.DesignName = "ImageCellAlternameOperationStatus";
            this.ImageCellAlternameOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternameOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellAlternameOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;


            // 
            // textCellAlternateSortNo
            // 
            this.textCellAlternateSortNo.CellSource.ColumnName = "DefaultSortNo";
            this.textCellAlternateSortNo.DesignName = "textCellSortNo";
            this.textCellAlternateSortNo.Location = new System.Drawing.Point(15, 15);
            this.textCellAlternateSortNo.Size = new System.Drawing.Size(40, 40);
            
            


            // 
            // FormLoadList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormLoadList";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeading)).EndInit();
            
            ((System.ComponentModel.ISupportInitialize)(this.btnNextZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrevZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnChangeZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).EndInit();
            this.tabControlDeliveries.ResumeLayout(false);
            this.tabNotStarted.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        
        

        

        
        

        #endregion
        private Resco.Controls.CommonControls.TabControl tabControlDeliveries;
        private Resco.Controls.CommonControls.TabPage tabNotStarted;
        private Resco.Controls.CommonControls.TabPage tabCompleted;
        private Resco.Controls.CommonControls.TabPage tabAll;

        private System.Windows.Forms.ImageList imageListOperationListIcons;
       
        private Resco.Controls.OutlookControls.ImageButton btnNextZone;
        private Resco.Controls.OutlookControls.ImageButton btnPrevZone;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.OutlookControls.ImageButton btnDown;
        private Resco.Controls.OutlookControls.ImageButton btnUp;
        private Resco.Controls.OutlookControls.ImageButton btnChangeZone;
       


        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listDeliveries;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelHeading;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellSortNo;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateSortNo;

        private Panel pnlMessage;
        private PictureBox picMessage;
        private Label lblInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnZone1Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone2Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone3Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone4Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone5Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone6Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone7Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone8Details;
        private Resco.Controls.OutlookControls.ImageButton btnCancelChangeZone;


       




    }
}
﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using SampleScreenDesigner.Properties;

namespace Com.Bring.PMP.PreComFW.PickUp.Views
{
    public partial class FormErrorMessage : Form
    {
        public FormErrorMessage()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            _messageControlBox.ShowMessage("There are operations present in your Operation list", MessageState.Error);
        }

        private void OnShow()
        {
            
        }
    
        private void ButtonOkClick(object sender, EventArgs e)
        {

        }
    }
}
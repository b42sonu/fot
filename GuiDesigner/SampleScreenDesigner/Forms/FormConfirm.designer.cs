﻿using System.Drawing;
using System.Windows.Forms;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    partial class FormConfirm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.linkLabelBack = new System.Windows.Forms.LinkLabel();
            this.linkLabelOk = new System.Windows.Forms.LinkLabel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelModuleName = new TransparentLabel();
            this.pnlMessage=new Panel();
            this.btnYes = new System.Windows.Forms.Button();
            this.btnNo = new System.Windows.Forms.Button();
            this.lblInfo = new System.Windows.Forms.Label();
            this.lblInfoHeading = new System.Windows.Forms.Label();
            this.picMessage = new System.Windows.Forms.PictureBox();
            this.pnlButtons.SuspendLayout();
            this.touchPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlButtons
            // 
            this.pnlButtons.BackColor = System.Drawing.Color.Black;
            this.pnlButtons.Controls.Add(this.linkLabelBack);
            this.pnlButtons.Controls.Add(this.linkLabelOk);
            this.pnlButtons.Location = new System.Drawing.Point(0, 475);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(480, 60);



            // 
            // pnlMessage
            // 
            this.pnlMessage.BackColor = System.Drawing.Color.White;
            this.pnlMessage.Location = new System.Drawing.Point(40, 50);
            this.pnlMessage.Name = "pnlButtons";
            this.pnlMessage.Controls.Add(this.btnYes);
            this.pnlMessage.Controls.Add(this.btnNo);
            this.pnlMessage.Controls.Add(this.lblInfo);
            this.pnlMessage.Controls.Add(this.lblInfoHeading);
            this.pnlMessage.Controls.Add(this.picMessage);
            this.pnlMessage.Size = new System.Drawing.Size(400, 380);


            // 
            // btnYes
            // 
            this.btnYes.Location = new System.Drawing.Point(70, 300);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(72, 34);
            this.btnYes.TabIndex = 3;
            this.btnYes.Text = "Yes";

            // 
            // btnNo
            // 
            this.btnNo.Location = new System.Drawing.Point(220, 300);
            this.btnNo.Name = "btnNo";
            this.btnNo.Size = new System.Drawing.Size(72, 34);
            this.btnNo.TabIndex = 3;
            this.btnNo.Text = "No";
            
            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblInfo.Location = new System.Drawing.Point(25, 150);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(300, 60);
            this.lblInfo.Text = "Do you wish to delete item 123334445454545545";
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblInfoHeading
            // 
            this.lblInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblInfoHeading.Location = new System.Drawing.Point(25, 100);
            this.lblInfoHeading.Name = "lblAccepted";
            this.lblInfoHeading.Size = new System.Drawing.Size(236, 30);
            this.lblInfoHeading.Text = "Confirm Delete";
            this.lblInfoHeading.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //  
            // picMessage
            // 
            this.picMessage.Image = SampleScreenDesigner.Properties.Resources.FOT_icon_attention;
            this.picMessage.Location = new System.Drawing.Point(170, 20);
            this.picMessage.Name = "picMessage";
            this.picMessage.Size = new System.Drawing.Size(50, 50);


            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(150, 5);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(79, 27);
            this.labelModuleName.Text = "Register Damage";

            // 
            // linkLabelBack
            // 
            this.linkLabelBack.BackColor = System.Drawing.Color.Black;
            this.linkLabelBack.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.linkLabelBack.ForeColor = System.Drawing.Color.White;
            this.linkLabelBack.Location = new System.Drawing.Point(22, 15);
            this.linkLabelBack.Name = "linkLabelBack";
            this.linkLabelBack.Size = new System.Drawing.Size(69, 41);
            this.linkLabelBack.TabIndex = 0;
            this.linkLabelBack.Text = "Back";
            // 
            // linkLabelOk
            // 
            this.linkLabelOk.BackColor = System.Drawing.Color.Black;
            this.linkLabelOk.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.linkLabelOk.ForeColor = System.Drawing.Color.White;
            this.linkLabelOk.Location = new System.Drawing.Point(362, 15);
            this.linkLabelOk.Name = "linkLabelOk";
            this.linkLabelOk.Size = new System.Drawing.Size(96, 41);
            this.linkLabelOk.TabIndex = 1;
            this.linkLabelOk.Text = "Ok/Enter";
            this.linkLabelOk.Click += new System.EventHandler(this.ButtonConfirmClick);
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.pnlButtons);
            this.touchPanel.Controls.Add(this.pnlMessage);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);

            // 
            // FormDamageScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormSignature";
            this.Size = new System.Drawing.Size(480, 535);
            this.pnlButtons.ResumeLayout(false);
            this.touchPanel.ResumeLayout(false);
            
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.LinkLabel linkLabelOk;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.LinkLabel linkLabelBack;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Panel pnlMessage;
        private PictureBox picMessage;
        private Label lblInfo;
        private Label lblInfoHeading;
        private Button btnYes;
        private Button btnNo;

    }
}
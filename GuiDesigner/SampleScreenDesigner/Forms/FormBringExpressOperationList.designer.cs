﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;

namespace SampleScreenDesigner.Forms
{
    partial class FormBringExpressOperationList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBringExpressOperationList));
            this.treeList = new Resco.Controls.AdvancedTree.AdvancedTree();
            this.Stop = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.textCellStopAddress = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCellStopStatus = new Resco.Controls.AdvancedTree.ImageCell();
            this.ImageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.textCellStopId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCellOperationId = new Resco.Controls.AdvancedTree.TextCell();
            this.cellIsLoadListOperation = new Resco.Controls.AdvancedTree.Cell();
            this.textCell1 = new Resco.Controls.AdvancedTree.TextCell();
            this.SelectedStop = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.textCellSelectedStopStopAddress = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCellSelectedStopStopStatus = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellSelectedStopStopId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCellSelectedStopOperationId = new Resco.Controls.AdvancedTree.TextCell();
            this.cellSelectedStopIsLoadListOperation = new Resco.Controls.AdvancedTree.Cell();
            this.textCell2 = new Resco.Controls.AdvancedTree.TextCell();
            this.StopSingleOperation = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.imageCellSingleOperationStopSign = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellSingleOperationStopAddress = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCellSingleOperationStopStatus = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellSingleOperationStopId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCellSingleOperationOperationId = new Resco.Controls.AdvancedTree.TextCell();
            this.cellSingleOperationIsLoadListOperation = new Resco.Controls.AdvancedTree.Cell();
            this.textCell3 = new Resco.Controls.AdvancedTree.TextCell();
            this.SelectedStopSingleOperation = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.imageCellSelectedSingleOperationStopSign = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellSelectedSingleOperationStopAddress = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCellSelectedSingleOperationStopStatus = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellSelectedSingleOperationStopId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCellSelectedSingleOperationOperationId = new Resco.Controls.AdvancedTree.TextCell();
            this.cellSelectedSingleOperationIsLoadListOperation = new Resco.Controls.AdvancedTree.Cell();
            this.textCell4 = new Resco.Controls.AdvancedTree.TextCell();
            this.Operation = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.imageCellOperationOperationSign = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellOperationOperationAddress = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCellOperationOperationStatus = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCell5 = new Resco.Controls.AdvancedTree.TextCell();
            this.OperationSelected = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.imageCellOperationSelectedOperationSign = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellOperationSelectedOperationAddress = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCellOperationSelectedOperationStatus = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCellOperationSelectedStopId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCellOperationSelectedOperationId = new Resco.Controls.AdvancedTree.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedTree.TextCell();
            this.OrderStops = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.imageCell1 = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCell7 = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCell2 = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCell8 = new Resco.Controls.AdvancedTree.TextCell();
            this.textCell9 = new Resco.Controls.AdvancedTree.TextCell();
            this.cell1 = new Resco.Controls.AdvancedTree.Cell();
            this.imageCell3 = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCell10 = new Resco.Controls.AdvancedTree.TextCell();
            this.SelectedOrderStops = new Resco.Controls.AdvancedTree.NodeTemplate();
            this.imageCell4 = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCell11 = new Resco.Controls.AdvancedTree.TextCell();
            this.imageCell5 = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCell12 = new Resco.Controls.AdvancedTree.TextCell();
            this.textCell13 = new Resco.Controls.AdvancedTree.TextCell();
            this.cell2 = new Resco.Controls.AdvancedTree.Cell();
            this.imageCell6 = new Resco.Controls.AdvancedTree.ImageCell();
            this.textCell14 = new Resco.Controls.AdvancedTree.TextCell();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.tabControlOperationList = new Resco.Controls.CommonControls.TabControl();
            this.tabStopList = new Resco.Controls.CommonControls.TabPage();
            this.tabOrderList = new Resco.Controls.CommonControls.TabPage();
            this.tabAllOrder = new Resco.Controls.CommonControls.TabPage();
            this.panelBottomInfo = new System.Windows.Forms.Panel();
            this.lblDetailValue3 = new System.Windows.Forms.TextBox();
            this.lblDetailValue1 = new System.Windows.Forms.Label();
            this.lblDetailValue2 = new System.Windows.Forms.Label();
            this.lblDetailType3 = new System.Windows.Forms.Label();
            this.lblDetailType1 = new System.Windows.Forms.Label();
            this.lblDetailType2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelCustomerInfo = new System.Windows.Forms.Label();
            this.labelRecientName = new System.Windows.Forms.Label();
            this.labelGoodAndOrderNumber = new System.Windows.Forms.Label();
            this.labelStopInfo = new System.Windows.Forms.Label();
            this.textBoxStopInfo = new System.Windows.Forms.TextBox();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOperationList)).BeginInit();
            this.tabControlOperationList.SuspendLayout();
            this.tabStopList.SuspendLayout();
            this.panelBottomInfo.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // treeList
            // 
            this.treeList.AutoScroll = true;
            this.treeList.Location = new System.Drawing.Point(0, 0);
            this.treeList.Name = "treeList";
            this.treeList.PlusMinusSize = new System.Drawing.Size(30, 30);
            this.treeList.ScrollbarWidth = 20;
            this.treeList.SelectionMode = Resco.Controls.AdvancedTree.SelectionMode.NoSelect;
            this.treeList.Size = new System.Drawing.Size(480, 254);
            this.treeList.TabIndex = 0;
            this.treeList.Templates.Add(this.Stop);
            this.treeList.Templates.Add(this.SelectedStop);
            this.treeList.Templates.Add(this.StopSingleOperation);
            this.treeList.Templates.Add(this.SelectedStopSingleOperation);
            this.treeList.Templates.Add(this.Operation);
            this.treeList.Templates.Add(this.OperationSelected);
            this.treeList.Templates.Add(this.OrderStops);
            this.treeList.Templates.Add(this.SelectedOrderStops);
            this.treeList.TouchScrolling = true;
            this.treeList.CellClick += new Resco.Controls.AdvancedTree.CellEventHandler(this.CellClick);
            // 
            // Stop
            // 
            this.Stop.CellTemplates.Add(this.textCellStopAddress);
            this.Stop.CellTemplates.Add(this.imageCellStopStatus);
            this.Stop.CellTemplates.Add(this.textCellStopId);
            this.Stop.CellTemplates.Add(this.textCellOperationId);
            this.Stop.CellTemplates.Add(this.cellIsLoadListOperation);
            this.Stop.CellTemplates.Add(this.textCell1);
            this.Stop.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.Stop.Height = 60;
            this.Stop.Name = "Stop";
            // 
            // textCellStopAddress
            // 
            this.textCellStopAddress.CellSource.ColumnName = "StopAddress";
            this.textCellStopAddress.DesignName = "textCellStopAddress";
            this.textCellStopAddress.Location = new System.Drawing.Point(5, 0);
            this.textCellStopAddress.Size = new System.Drawing.Size(388, 60);
            // 
            // imageCellStopStatus
            // 
            this.imageCellStopStatus.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCellStopStatus.CellSource.ColumnName = "StopStatus";
            this.imageCellStopStatus.DesignName = "imageCellStopStatus";
            this.imageCellStopStatus.ImageList = this.ImageListOperationListIcons;
            this.imageCellStopStatus.Location = new System.Drawing.Point(391, 0);
            this.imageCellStopStatus.Size = new System.Drawing.Size(47, 60);
            this.ImageListOperationListIcons.Images.Clear();
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource"))));
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource4"))));
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource5"))));
            this.ImageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource6"))));
            // 
            // textCellStopId
            // 
            this.textCellStopId.CellSource.ColumnName = "StopId";
            this.textCellStopId.DesignName = "textCellStopId";
            this.textCellStopId.Location = new System.Drawing.Point(1, 0);
            this.textCellStopId.Size = new System.Drawing.Size(54, 60);
            this.textCellStopId.Visible = false;
            // 
            // textCellOperationId
            // 
            this.textCellOperationId.CellSource.ColumnName = "OperationId ";
            this.textCellOperationId.DesignName = "textCellOperationId";
            this.textCellOperationId.Location = new System.Drawing.Point(1, 0);
            this.textCellOperationId.Size = new System.Drawing.Size(54, 60);
            this.textCellOperationId.Visible = false;
            // 
            // cellIsLoadListOperation
            // 
            this.cellIsLoadListOperation.CellSource.ColumnName = "IsLoadListOperation";
            this.cellIsLoadListOperation.DesignName = "cellIsLoadListOperation";
            this.cellIsLoadListOperation.Location = new System.Drawing.Point(1, 0);
            this.cellIsLoadListOperation.Size = new System.Drawing.Size(54, 60);
            this.cellIsLoadListOperation.Visible = false;
            // 
            // textCell1
            // 
            this.textCell1.CellSource.ColumnName = "OrderId";
            this.textCell1.DesignName = "textCell1";
            this.textCell1.Location = new System.Drawing.Point(1, 0);
            this.textCell1.Size = new System.Drawing.Size(54, 60);
            this.textCell1.Visible = false;
            // 
            // SelectedStop
            // 
            this.SelectedStop.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.SelectedStop.CellTemplates.Add(this.textCellSelectedStopStopAddress);
            this.SelectedStop.CellTemplates.Add(this.imageCellSelectedStopStopStatus);
            this.SelectedStop.CellTemplates.Add(this.textCellSelectedStopStopId);
            this.SelectedStop.CellTemplates.Add(this.textCellSelectedStopOperationId);
            this.SelectedStop.CellTemplates.Add(this.cellSelectedStopIsLoadListOperation);
            this.SelectedStop.CellTemplates.Add(this.textCell2);
            this.SelectedStop.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.SelectedStop.Height = 60;
            this.SelectedStop.Name = "SelectedStop";
            // 
            // textCellSelectedStopStopAddress
            // 
            this.textCellSelectedStopStopAddress.CellSource.ColumnName = "StopAddress";
            this.textCellSelectedStopStopAddress.DesignName = "textCellSelectedStopStopAddress";
            this.textCellSelectedStopStopAddress.Location = new System.Drawing.Point(3, 0);
            this.textCellSelectedStopStopAddress.Size = new System.Drawing.Size(390, 60);
            // 
            // imageCellSelectedStopStopStatus
            // 
            this.imageCellSelectedStopStopStatus.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCellSelectedStopStopStatus.CellSource.ColumnName = "StopStatus";
            this.imageCellSelectedStopStopStatus.DesignName = "imageCellSelectedStopStopStatus";
            this.imageCellSelectedStopStopStatus.ImageList = this.ImageListOperationListIcons;
            this.imageCellSelectedStopStopStatus.Location = new System.Drawing.Point(391, 0);
            this.imageCellSelectedStopStopStatus.Size = new System.Drawing.Size(47, 60);
            // 
            // textCellSelectedStopStopId
            // 
            this.textCellSelectedStopStopId.CellSource.ColumnName = "StopId";
            this.textCellSelectedStopStopId.DesignName = "textCellSelectedStopStopId";
            this.textCellSelectedStopStopId.Location = new System.Drawing.Point(1, 0);
            this.textCellSelectedStopStopId.Size = new System.Drawing.Size(54, 60);
            this.textCellSelectedStopStopId.Visible = false;
            // 
            // textCellSelectedStopOperationId
            // 
            this.textCellSelectedStopOperationId.CellSource.ColumnName = "OperationId ";
            this.textCellSelectedStopOperationId.DesignName = "textCellSelectedStopOperationId";
            this.textCellSelectedStopOperationId.Location = new System.Drawing.Point(1, 0);
            this.textCellSelectedStopOperationId.Size = new System.Drawing.Size(54, 60);
            this.textCellSelectedStopOperationId.Visible = false;
            // 
            // cellSelectedStopIsLoadListOperation
            // 
            this.cellSelectedStopIsLoadListOperation.CellSource.ColumnName = "IsLoadListOperation";
            this.cellSelectedStopIsLoadListOperation.DesignName = "cellSelectedStopIsLoadListOperation";
            this.cellSelectedStopIsLoadListOperation.Location = new System.Drawing.Point(1, 0);
            this.cellSelectedStopIsLoadListOperation.Size = new System.Drawing.Size(54, 60);
            this.cellSelectedStopIsLoadListOperation.Visible = false;
            // 
            // textCell2
            // 
            this.textCell2.CellSource.ColumnName = "OrderId";
            this.textCell2.DesignName = "textCell2";
            this.textCell2.Location = new System.Drawing.Point(1, 0);
            this.textCell2.Size = new System.Drawing.Size(54, 60);
            this.textCell2.Visible = false;
            // 
            // StopSingleOperation
            // 
            this.StopSingleOperation.CellTemplates.Add(this.imageCellSingleOperationStopSign);
            this.StopSingleOperation.CellTemplates.Add(this.textCellSingleOperationStopAddress);
            this.StopSingleOperation.CellTemplates.Add(this.imageCellSingleOperationStopStatus);
            this.StopSingleOperation.CellTemplates.Add(this.textCellSingleOperationStopId);
            this.StopSingleOperation.CellTemplates.Add(this.textCellSingleOperationOperationId);
            this.StopSingleOperation.CellTemplates.Add(this.cellSingleOperationIsLoadListOperation);
            this.StopSingleOperation.CellTemplates.Add(this.textCell3);
            this.StopSingleOperation.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.StopSingleOperation.Height = 60;
            this.StopSingleOperation.Name = "StopSingleOperation";
            // 
            // imageCellSingleOperationStopSign
            // 
            this.imageCellSingleOperationStopSign.AutoHeight = true;
            this.imageCellSingleOperationStopSign.AutoResize = true;
            this.imageCellSingleOperationStopSign.CellSource.ColumnName = "StopSign";
            this.imageCellSingleOperationStopSign.DesignName = "imageCellSingleOperationStopSign";
            this.imageCellSingleOperationStopSign.ImageList = this.ImageListOperationListIcons;
            this.imageCellSingleOperationStopSign.Location = new System.Drawing.Point(1, 10);
            this.imageCellSingleOperationStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellSingleOperationStopAddress
            // 
            this.textCellSingleOperationStopAddress.CellSource.ColumnName = "StopAddress";
            this.textCellSingleOperationStopAddress.DesignName = "textCellSingleOperationStopAddress";
            this.textCellSingleOperationStopAddress.Location = new System.Drawing.Point(61, 0);
            this.textCellSingleOperationStopAddress.Size = new System.Drawing.Size(332, 60);
            // 
            // imageCellSingleOperationStopStatus
            // 
            this.imageCellSingleOperationStopStatus.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCellSingleOperationStopStatus.CellSource.ColumnName = "StopStatus";
            this.imageCellSingleOperationStopStatus.DesignName = "imageCellSingleOperationStopStatus";
            this.imageCellSingleOperationStopStatus.ImageList = this.ImageListOperationListIcons;
            this.imageCellSingleOperationStopStatus.Location = new System.Drawing.Point(391, 0);
            this.imageCellSingleOperationStopStatus.Size = new System.Drawing.Size(47, 60);
            // 
            // textCellSingleOperationStopId
            // 
            this.textCellSingleOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellSingleOperationStopId.DesignName = "textCellSingleOperationStopId";
            this.textCellSingleOperationStopId.Location = new System.Drawing.Point(1, 0);
            this.textCellSingleOperationStopId.Size = new System.Drawing.Size(54, 60);
            this.textCellSingleOperationStopId.Visible = false;
            // 
            // textCellSingleOperationOperationId
            // 
            this.textCellSingleOperationOperationId.CellSource.ColumnName = "OperationId ";
            this.textCellSingleOperationOperationId.DesignName = "textCellSingleOperationOperationId";
            this.textCellSingleOperationOperationId.Location = new System.Drawing.Point(1, 0);
            this.textCellSingleOperationOperationId.Size = new System.Drawing.Size(54, 60);
            this.textCellSingleOperationOperationId.Visible = false;
            // 
            // cellSingleOperationIsLoadListOperation
            // 
            this.cellSingleOperationIsLoadListOperation.CellSource.ColumnName = "IsLoadListOperation";
            this.cellSingleOperationIsLoadListOperation.DesignName = "cellSingleOperationIsLoadListOperation";
            this.cellSingleOperationIsLoadListOperation.Location = new System.Drawing.Point(1, 0);
            this.cellSingleOperationIsLoadListOperation.Size = new System.Drawing.Size(54, 60);
            this.cellSingleOperationIsLoadListOperation.Visible = false;
            // 
            // textCell3
            // 
            this.textCell3.CellSource.ColumnName = "OrderId";
            this.textCell3.DesignName = "textCell3";
            this.textCell3.Location = new System.Drawing.Point(1, 0);
            this.textCell3.Size = new System.Drawing.Size(54, 60);
            this.textCell3.Visible = false;
            // 
            // SelectedStopSingleOperation
            // 
            this.SelectedStopSingleOperation.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.SelectedStopSingleOperation.CellTemplates.Add(this.imageCellSelectedSingleOperationStopSign);
            this.SelectedStopSingleOperation.CellTemplates.Add(this.textCellSelectedSingleOperationStopAddress);
            this.SelectedStopSingleOperation.CellTemplates.Add(this.imageCellSelectedSingleOperationStopStatus);
            this.SelectedStopSingleOperation.CellTemplates.Add(this.textCellSelectedSingleOperationStopId);
            this.SelectedStopSingleOperation.CellTemplates.Add(this.textCellSelectedSingleOperationOperationId);
            this.SelectedStopSingleOperation.CellTemplates.Add(this.cellSelectedSingleOperationIsLoadListOperation);
            this.SelectedStopSingleOperation.CellTemplates.Add(this.textCell4);
            this.SelectedStopSingleOperation.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.SelectedStopSingleOperation.Height = 60;
            this.SelectedStopSingleOperation.Name = "SelectedStopSingleOperation";
            // 
            // imageCellSelectedSingleOperationStopSign
            // 
            this.imageCellSelectedSingleOperationStopSign.AutoHeight = true;
            this.imageCellSelectedSingleOperationStopSign.AutoResize = true;
            this.imageCellSelectedSingleOperationStopSign.CellSource.ColumnName = "StopSign";
            this.imageCellSelectedSingleOperationStopSign.DesignName = "imageCellSelectedSingleOperationStopSign";
            this.imageCellSelectedSingleOperationStopSign.ImageList = this.ImageListOperationListIcons;
            this.imageCellSelectedSingleOperationStopSign.Location = new System.Drawing.Point(1, 10);
            this.imageCellSelectedSingleOperationStopSign.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellSelectedSingleOperationStopAddress
            // 
            this.textCellSelectedSingleOperationStopAddress.CellSource.ColumnName = "StopAddress";
            this.textCellSelectedSingleOperationStopAddress.DesignName = "textCellSelectedSingleOperationStopAddress";
            this.textCellSelectedSingleOperationStopAddress.Location = new System.Drawing.Point(61, 0);
            this.textCellSelectedSingleOperationStopAddress.Size = new System.Drawing.Size(332, 60);
            // 
            // imageCellSelectedSingleOperationStopStatus
            // 
            this.imageCellSelectedSingleOperationStopStatus.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCellSelectedSingleOperationStopStatus.CellSource.ColumnName = "StopStatus";
            this.imageCellSelectedSingleOperationStopStatus.DesignName = "imageCellSelectedSingleOperationStopStatus";
            this.imageCellSelectedSingleOperationStopStatus.ImageList = this.ImageListOperationListIcons;
            this.imageCellSelectedSingleOperationStopStatus.Location = new System.Drawing.Point(391, 0);
            this.imageCellSelectedSingleOperationStopStatus.Size = new System.Drawing.Size(47, 60);
            // 
            // textCellSelectedSingleOperationStopId
            // 
            this.textCellSelectedSingleOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellSelectedSingleOperationStopId.DesignName = "textCellSelectedSingleOperationStopId";
            this.textCellSelectedSingleOperationStopId.Location = new System.Drawing.Point(1, 0);
            this.textCellSelectedSingleOperationStopId.Size = new System.Drawing.Size(54, 60);
            this.textCellSelectedSingleOperationStopId.Visible = false;
            // 
            // textCellSelectedSingleOperationOperationId
            // 
            this.textCellSelectedSingleOperationOperationId.CellSource.ColumnName = "OperationId ";
            this.textCellSelectedSingleOperationOperationId.DesignName = "textCellSelectedSingleOperationOperationId";
            this.textCellSelectedSingleOperationOperationId.Location = new System.Drawing.Point(1, 0);
            this.textCellSelectedSingleOperationOperationId.Size = new System.Drawing.Size(54, 60);
            this.textCellSelectedSingleOperationOperationId.Visible = false;
            // 
            // cellSelectedSingleOperationIsLoadListOperation
            // 
            this.cellSelectedSingleOperationIsLoadListOperation.CellSource.ColumnName = "IsLoadListOperation";
            this.cellSelectedSingleOperationIsLoadListOperation.DesignName = "cellSelectedSingleOperationIsLoadListOperation";
            this.cellSelectedSingleOperationIsLoadListOperation.Location = new System.Drawing.Point(1, 0);
            this.cellSelectedSingleOperationIsLoadListOperation.Size = new System.Drawing.Size(54, 60);
            this.cellSelectedSingleOperationIsLoadListOperation.Visible = false;
            // 
            // textCell4
            // 
            this.textCell4.CellSource.ColumnName = "OrderId";
            this.textCell4.DesignName = "textCell4";
            this.textCell4.Location = new System.Drawing.Point(1, 0);
            this.textCell4.Size = new System.Drawing.Size(54, 60);
            this.textCell4.Visible = false;
            // 
            // Operation
            // 
            this.Operation.CellTemplates.Add(this.imageCellOperationOperationSign);
            this.Operation.CellTemplates.Add(this.textCellOperationOperationAddress);
            this.Operation.CellTemplates.Add(this.imageCellOperationOperationStatus);
            this.Operation.CellTemplates.Add(this.textCellOperationStopId);
            this.Operation.CellTemplates.Add(this.textCellOperationOperationId);
            this.Operation.CellTemplates.Add(this.textCell5);
            this.Operation.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.Operation.Height = 60;
            this.Operation.Name = "Operation";
            // 
            // imageCellOperationOperationSign
            // 
            this.imageCellOperationOperationSign.AutoHeight = true;
            this.imageCellOperationOperationSign.AutoResize = true;
            this.imageCellOperationOperationSign.CellSource.ColumnName = "OperationSign";
            this.imageCellOperationOperationSign.DesignName = "imageCellOperationOperationSign";
            this.imageCellOperationOperationSign.ImageList = this.ImageListOperationListIcons;
            this.imageCellOperationOperationSign.Location = new System.Drawing.Point(1, 10);
            this.imageCellOperationOperationSign.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationOperationAddress
            // 
            this.textCellOperationOperationAddress.CellSource.ColumnName = "OperationAddress";
            this.textCellOperationOperationAddress.DesignName = "textCellOperationOperationAddress";
            this.textCellOperationOperationAddress.Location = new System.Drawing.Point(68, 0);
            this.textCellOperationOperationAddress.Size = new System.Drawing.Size(314, 60);
            // 
            // imageCellOperationOperationStatus
            // 
            this.imageCellOperationOperationStatus.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCellOperationOperationStatus.CellSource.ColumnName = "OperationStatus";
            this.imageCellOperationOperationStatus.DesignName = "imageCellOperationOperationStatus";
            this.imageCellOperationOperationStatus.ImageList = this.ImageListOperationListIcons;
            this.imageCellOperationOperationStatus.Location = new System.Drawing.Point(362, 0);
            this.imageCellOperationOperationStatus.Size = new System.Drawing.Size(66, 60);
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(76, 58);
            this.textCellOperationStopId.Visible = false;
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.CellSource.ColumnName = "OperationId ";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(1, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(54, 60);
            this.textCellOperationOperationId.Visible = false;
            // 
            // textCell5
            // 
            this.textCell5.CellSource.ColumnName = "OrderId";
            this.textCell5.DesignName = "textCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(76, 58);
            this.textCell5.Visible = false;
            // 
            // OperationSelected
            // 
            this.OperationSelected.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.OperationSelected.CellTemplates.Add(this.imageCellOperationSelectedOperationSign);
            this.OperationSelected.CellTemplates.Add(this.textCellOperationSelectedOperationAddress);
            this.OperationSelected.CellTemplates.Add(this.imageCellOperationSelectedOperationStatus);
            this.OperationSelected.CellTemplates.Add(this.textCellOperationSelectedStopId);
            this.OperationSelected.CellTemplates.Add(this.textCellOperationSelectedOperationId);
            this.OperationSelected.CellTemplates.Add(this.textCell6);
            this.OperationSelected.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.OperationSelected.Height = 60;
            this.OperationSelected.Name = "OperationSelected";
            // 
            // imageCellOperationSelectedOperationSign
            // 
            this.imageCellOperationSelectedOperationSign.AutoHeight = true;
            this.imageCellOperationSelectedOperationSign.AutoResize = true;
            this.imageCellOperationSelectedOperationSign.CellSource.ColumnName = "OperationSign";
            this.imageCellOperationSelectedOperationSign.DesignName = "imageCellOperationSelectedOperationSign";
            this.imageCellOperationSelectedOperationSign.ImageList = this.ImageListOperationListIcons;
            this.imageCellOperationSelectedOperationSign.Location = new System.Drawing.Point(1, 10);
            this.imageCellOperationSelectedOperationSign.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationSelectedOperationAddress
            // 
            this.textCellOperationSelectedOperationAddress.CellSource.ColumnName = "OperationAddress";
            this.textCellOperationSelectedOperationAddress.DesignName = "textCellOperationSelectedOperationAddress";
            this.textCellOperationSelectedOperationAddress.Location = new System.Drawing.Point(64, 0);
            this.textCellOperationSelectedOperationAddress.Size = new System.Drawing.Size(312, 60);
            // 
            // imageCellOperationSelectedOperationStatus
            // 
            this.imageCellOperationSelectedOperationStatus.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCellOperationSelectedOperationStatus.CellSource.ColumnName = "OperationStatus";
            this.imageCellOperationSelectedOperationStatus.DesignName = "imageCellOperationSelectedOperationStatus";
            this.imageCellOperationSelectedOperationStatus.ImageList = this.ImageListOperationListIcons;
            this.imageCellOperationSelectedOperationStatus.Location = new System.Drawing.Point(358, 0);
            this.imageCellOperationSelectedOperationStatus.Size = new System.Drawing.Size(64, 60);
            // 
            // textCellOperationSelectedStopId
            // 
            this.textCellOperationSelectedStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationSelectedStopId.DesignName = "textCellOperationSelectedStopId";
            this.textCellOperationSelectedStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationSelectedStopId.Size = new System.Drawing.Size(75, 60);
            this.textCellOperationSelectedStopId.Visible = false;
            // 
            // textCellOperationSelectedOperationId
            // 
            this.textCellOperationSelectedOperationId.CellSource.ColumnName = "OperationId ";
            this.textCellOperationSelectedOperationId.DesignName = "textCellOperationSelectedOperationId";
            this.textCellOperationSelectedOperationId.Location = new System.Drawing.Point(1, 0);
            this.textCellOperationSelectedOperationId.Size = new System.Drawing.Size(54, 60);
            this.textCellOperationSelectedOperationId.Visible = false;
            // 
            // textCell6
            // 
            this.textCell6.CellSource.ColumnName = "OrderId";
            this.textCell6.DesignName = "textCell6";
            this.textCell6.Location = new System.Drawing.Point(0, 0);
            this.textCell6.Size = new System.Drawing.Size(75, 60);
            this.textCell6.Visible = false;
            // 
            // OrderStops
            // 
            this.OrderStops.CellTemplates.Add(this.imageCell1);
            this.OrderStops.CellTemplates.Add(this.textCell7);
            this.OrderStops.CellTemplates.Add(this.imageCell2);
            this.OrderStops.CellTemplates.Add(this.textCell8);
            this.OrderStops.CellTemplates.Add(this.textCell9);
            this.OrderStops.CellTemplates.Add(this.cell1);
            this.OrderStops.CellTemplates.Add(this.imageCell3);
            this.OrderStops.CellTemplates.Add(this.textCell10);
            this.OrderStops.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.OrderStops.Height = 110;
            this.OrderStops.Name = "OrderStops";
            // 
            // imageCell1
            // 
            this.imageCell1.AutoHeight = true;
            this.imageCell1.AutoResize = true;
            this.imageCell1.CellSource.ColumnName = "OrderFirstStopSign";
            this.imageCell1.DesignName = "imageCell1";
            this.imageCell1.ImageList = this.ImageListOperationListIcons;
            this.imageCell1.Location = new System.Drawing.Point(1, 15);
            this.imageCell1.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell7
            // 
            this.textCell7.CellSource.ColumnName = "OrderDetail";
            this.textCell7.DesignName = "textCell7";
            this.textCell7.Location = new System.Drawing.Point(61, 0);
            this.textCell7.Size = new System.Drawing.Size(332, 107);
            // 
            // imageCell2
            // 
            this.imageCell2.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCell2.CellSource.ColumnName = "StopStatus";
            this.imageCell2.DesignName = "imageCell2";
            this.imageCell2.ImageList = this.ImageListOperationListIcons;
            this.imageCell2.Location = new System.Drawing.Point(391, 0);
            this.imageCell2.Size = new System.Drawing.Size(47, 109);
            // 
            // textCell8
            // 
            this.textCell8.CellSource.ColumnName = "StopId";
            this.textCell8.DesignName = "textCell8";
            this.textCell8.Location = new System.Drawing.Point(1, 0);
            this.textCell8.Size = new System.Drawing.Size(54, 60);
            this.textCell8.Visible = false;
            // 
            // textCell9
            // 
            this.textCell9.CellSource.ColumnName = "OperationId ";
            this.textCell9.DesignName = "textCell9";
            this.textCell9.Location = new System.Drawing.Point(1, 0);
            this.textCell9.Size = new System.Drawing.Size(54, 60);
            this.textCell9.Visible = false;
            // 
            // cell1
            // 
            this.cell1.CellSource.ColumnName = "IsLoadListOperation";
            this.cell1.DesignName = "cell1";
            this.cell1.Location = new System.Drawing.Point(1, 0);
            this.cell1.Size = new System.Drawing.Size(54, 60);
            this.cell1.Visible = false;
            // 
            // imageCell3
            // 
            this.imageCell3.AutoHeight = true;
            this.imageCell3.AutoResize = true;
            this.imageCell3.CellSource.ColumnName = "OrderSecondStopSign";
            this.imageCell3.DesignName = "imageCell3";
            this.imageCell3.ImageList = this.ImageListOperationListIcons;
            this.imageCell3.Location = new System.Drawing.Point(1, 60);
            this.imageCell3.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell10
            // 
            this.textCell10.CellSource.ColumnName = "OrderId";
            this.textCell10.DesignName = "textCell10";
            this.textCell10.Location = new System.Drawing.Point(1, 0);
            this.textCell10.Size = new System.Drawing.Size(54, 60);
            this.textCell10.Visible = false;
            // 
            // SelectedOrderStops
            // 
            this.SelectedOrderStops.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.SelectedOrderStops.CellTemplates.Add(this.imageCell4);
            this.SelectedOrderStops.CellTemplates.Add(this.textCell11);
            this.SelectedOrderStops.CellTemplates.Add(this.imageCell5);
            this.SelectedOrderStops.CellTemplates.Add(this.textCell12);
            this.SelectedOrderStops.CellTemplates.Add(this.textCell13);
            this.SelectedOrderStops.CellTemplates.Add(this.cell2);
            this.SelectedOrderStops.CellTemplates.Add(this.imageCell6);
            this.SelectedOrderStops.CellTemplates.Add(this.textCell14);
            this.SelectedOrderStops.GradientBackColor = new Resco.Controls.AdvancedTree.GradientColor(System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Transparent, System.Drawing.Color.Transparent, System.Drawing.SystemColors.ControlLightLight, Resco.Controls.AdvancedTree.FillDirection.Vertical);
            this.SelectedOrderStops.Height = 110;
            this.SelectedOrderStops.Name = "SelectedOrderStops";
            // 
            // imageCell4
            // 
            this.imageCell4.AutoHeight = true;
            this.imageCell4.AutoResize = true;
            this.imageCell4.CellSource.ColumnName = "OrderFirstStopSign";
            this.imageCell4.DesignName = "imageCell4";
            this.imageCell4.ImageList = this.ImageListOperationListIcons;
            this.imageCell4.Location = new System.Drawing.Point(1, 15);
            this.imageCell4.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell11
            // 
            this.textCell11.CellSource.ColumnName = "OrderDetail";
            this.textCell11.DesignName = "textCell11";
            this.textCell11.Location = new System.Drawing.Point(61, 0);
            this.textCell11.Size = new System.Drawing.Size(332, 110);
            // 
            // imageCell5
            // 
            this.imageCell5.Alignment = Resco.Controls.AdvancedTree.Alignment.MiddleCenter;
            this.imageCell5.CellSource.ColumnName = "StopStatus";
            this.imageCell5.DesignName = "imageCell5";
            this.imageCell5.ImageList = this.ImageListOperationListIcons;
            this.imageCell5.Location = new System.Drawing.Point(391, 0);
            this.imageCell5.Size = new System.Drawing.Size(47, 109);
            // 
            // textCell12
            // 
            this.textCell12.CellSource.ColumnName = "StopId";
            this.textCell12.DesignName = "textCell12";
            this.textCell12.Location = new System.Drawing.Point(1, 0);
            this.textCell12.Size = new System.Drawing.Size(54, 60);
            this.textCell12.Visible = false;
            // 
            // textCell13
            // 
            this.textCell13.CellSource.ColumnName = "OperationId ";
            this.textCell13.DesignName = "textCell13";
            this.textCell13.Location = new System.Drawing.Point(1, 0);
            this.textCell13.Size = new System.Drawing.Size(54, 60);
            this.textCell13.Visible = false;
            // 
            // cell2
            // 
            this.cell2.CellSource.ColumnName = "IsLoadListOperation";
            this.cell2.DesignName = "cell2";
            this.cell2.Location = new System.Drawing.Point(1, 0);
            this.cell2.Size = new System.Drawing.Size(54, 60);
            this.cell2.Visible = false;
            // 
            // imageCell6
            // 
            this.imageCell6.AutoHeight = true;
            this.imageCell6.AutoResize = true;
            this.imageCell6.CellSource.ColumnName = "OrderSecondStopSign";
            this.imageCell6.DesignName = "imageCell6";
            this.imageCell6.ImageList = this.ImageListOperationListIcons;
            this.imageCell6.Location = new System.Drawing.Point(1, 60);
            this.imageCell6.Size = new System.Drawing.Size(40, 40);
            // 
            // textCell14
            // 
            this.textCell14.CellSource.ColumnName = "OrderId";
            this.textCell14.DesignName = "textCell14";
            this.textCell14.Location = new System.Drawing.Point(1, 0);
            this.textCell14.Size = new System.Drawing.Size(54, 60);
            this.textCell14.Visible = false;
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.tabControlOperationList);
            this.touchPanel.Controls.Add(this.panel1);
            this.touchPanel.Controls.Add(this.panelBottomInfo);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // tabControlOperationList
            // 
            this.tabControlOperationList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlOperationList.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabControlOperationList.Location = new System.Drawing.Point(0, 0);
            this.tabControlOperationList.Name = "tabControlOperationList";
            this.tabControlOperationList.SelectedIndex = 0;
            this.tabControlOperationList.Size = new System.Drawing.Size(480, 305);
            this.tabControlOperationList.TabIndex = 43;
            this.tabControlOperationList.TabPages.Add(this.tabStopList);
            this.tabControlOperationList.TabPages.Add(this.tabOrderList);
            this.tabControlOperationList.TabPages.Add(this.tabAllOrder);
            this.tabControlOperationList.Text = "tabControlOperationList";
            this.tabControlOperationList.ToolbarSize = new System.Drawing.Size(480, 51);
            this.tabControlOperationList.SelectedIndexChanged += new System.EventHandler(this.TabControlOperationSelectedIndexChanged);
            // 
            // tabStopList
            // 
            this.tabStopList.Controls.Add(this.treeList);
            this.tabStopList.Location = new System.Drawing.Point(0, 0);
            this.tabStopList.Name = "tabStopList";
            this.tabStopList.Size = new System.Drawing.Size(480, 254);
            // 
            // 
            // 
            this.tabStopList.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabStopList.TabItem.CustomSize = new System.Drawing.Size(160, 50);
            this.tabStopList.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabStopList.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabStopList.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabStopList.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabStopList.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabStopList.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabStopList.TabItem.Name = "";
            // 
            // tabOrderList
            // 
            this.tabOrderList.Location = new System.Drawing.Point(0, 0);
            this.tabOrderList.Name = "tabOrderList";
            this.tabOrderList.Size = new System.Drawing.Size(480, 254);
            // 
            // 
            // 
            this.tabOrderList.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabOrderList.TabItem.CustomSize = new System.Drawing.Size(150, 50);
            this.tabOrderList.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabOrderList.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabOrderList.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabOrderList.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabOrderList.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabOrderList.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabOrderList.TabItem.Name = "";
            // 
            // tabAllOrder
            // 
            this.tabAllOrder.Location = new System.Drawing.Point(0, 0);
            this.tabAllOrder.Name = "tabAllOrder";
            this.tabAllOrder.Size = new System.Drawing.Size(480, 254);
            // 
            // 
            // 
            this.tabAllOrder.TabItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabAllOrder.TabItem.CustomSize = new System.Drawing.Size(130, 50);
            this.tabAllOrder.TabItem.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(240)))));
            this.tabAllOrder.TabItem.FocusedFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabAllOrder.TabItem.FocusedForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.tabAllOrder.TabItem.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.tabAllOrder.TabItem.ForeColor = System.Drawing.Color.White;
            this.tabAllOrder.TabItem.ItemSizeType = Resco.Controls.CommonControls.ToolbarItemSizeType.ByCustomSize;
            this.tabAllOrder.TabItem.Name = "";
            // 
            // panelBottomInfo
            // 
            this.panelBottomInfo.Controls.Add(this.lblDetailValue3);
            this.panelBottomInfo.Controls.Add(this.lblDetailValue1);
            this.panelBottomInfo.Controls.Add(this.lblDetailValue2);
            this.panelBottomInfo.Controls.Add(this.lblDetailType3);
            this.panelBottomInfo.Controls.Add(this.lblDetailType1);
            this.panelBottomInfo.Controls.Add(this.lblDetailType2);
            this.panelBottomInfo.Location = new System.Drawing.Point(0, 305);
            this.panelBottomInfo.Name = "panelBottomInfo";
            this.panelBottomInfo.Size = new System.Drawing.Size(480, 140);
            // 
            // lblDetailValue3
            // 
            this.lblDetailValue3.BackColor = System.Drawing.Color.White;
            this.lblDetailValue3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblDetailValue3.Location = new System.Drawing.Point(230, 60);
            this.lblDetailValue3.Multiline = true;
            this.lblDetailValue3.Name = "lblDetailValue3";
            this.lblDetailValue3.ReadOnly = true;
            this.lblDetailValue3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lblDetailValue3.Size = new System.Drawing.Size(245, 75);
            this.lblDetailValue3.TabIndex = 0;
            // 
            // lblDetailValue1
            // 
            this.lblDetailValue1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailValue1.Location = new System.Drawing.Point(230, 0);
            this.lblDetailValue1.Name = "lblDetailValue1";
            this.lblDetailValue1.Size = new System.Drawing.Size(245, 35);
            // 
            // lblDetailValue2
            // 
            this.lblDetailValue2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailValue2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailValue2.Location = new System.Drawing.Point(230, 30);
            this.lblDetailValue2.Name = "lblDetailValue2";
            this.lblDetailValue2.Size = new System.Drawing.Size(245, 30);
            // 
            // lblDetailType3
            // 
            this.lblDetailType3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType3.Location = new System.Drawing.Point(3, 60);
            this.lblDetailType3.Name = "lblDetailType3";
            this.lblDetailType3.Size = new System.Drawing.Size(172, 78);
            // 
            // lblDetailType1
            // 
            this.lblDetailType1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType1.Location = new System.Drawing.Point(3, 0);
            this.lblDetailType1.Name = "lblDetailType1";
            this.lblDetailType1.Size = new System.Drawing.Size(220, 30);
            // 
            // lblDetailType2
            // 
            this.lblDetailType2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDetailType2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDetailType2.Location = new System.Drawing.Point(3, 30);
            this.lblDetailType2.Name = "lblDetailType2";
            this.lblDetailType2.Size = new System.Drawing.Size(148, 40);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBoxStopInfo);
            this.panel1.Controls.Add(this.labelStopInfo);
            this.panel1.Controls.Add(this.labelGoodAndOrderNumber);
            this.panel1.Controls.Add(this.labelRecientName);
            this.panel1.Controls.Add(this.labelCustomerInfo);
            this.panel1.Location = new System.Drawing.Point(0, 305);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(480, 140);
            // 
            // labelCustomerInfo
            // 
            this.labelCustomerInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCustomerInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelCustomerInfo.Location = new System.Drawing.Point(1, 1);
            this.labelCustomerInfo.Name = "labelCustomerInfo";
            this.labelCustomerInfo.Size = new System.Drawing.Size(475, 30);
            // 
            // labelRecientName
            // 
            this.labelRecientName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelRecientName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelRecientName.Location = new System.Drawing.Point(1, 35);
            this.labelRecientName.Name = "labelRecientName";
            this.labelRecientName.Size = new System.Drawing.Size(475, 30);
            // 
            // labelGoodAndOrderNumber
            // 
            this.labelGoodAndOrderNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelGoodAndOrderNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelGoodAndOrderNumber.Location = new System.Drawing.Point(2, 65);
            this.labelGoodAndOrderNumber.Name = "labelGoodAndOrderNumber";
            this.labelGoodAndOrderNumber.Size = new System.Drawing.Size(475, 30);
            // 
            // labelStopInfo
            // 
            this.labelStopInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStopInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelStopInfo.Location = new System.Drawing.Point(3, 100);
            this.labelStopInfo.Name = "labelStopInfo";
            this.labelStopInfo.Size = new System.Drawing.Size(112, 30);
            this.labelStopInfo.Text = "Stop Info";
            // 
            // textBoxStopInfo
            // 
            this.textBoxStopInfo.BackColor = System.Drawing.Color.White;
            this.textBoxStopInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStopInfo.Location = new System.Drawing.Point(121, 98);
            this.textBoxStopInfo.Multiline = true;
            this.textBoxStopInfo.Name = "textBoxStopInfo";
            this.textBoxStopInfo.ReadOnly = true;
            this.textBoxStopInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxStopInfo.Size = new System.Drawing.Size(354, 39);
            this.textBoxStopInfo.TabIndex = 8;
            // 
            // FormBringExpressOperationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormBringExpressOperationList";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlOperationList)).EndInit();
            this.tabControlOperationList.ResumeLayout(false);
            this.tabStopList.ResumeLayout(false);
            this.panelBottomInfo.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList ImageListOperationListIcons;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TabControl tabControlOperationList;
        private Resco.Controls.CommonControls.TabPage tabStopList;
        private Resco.Controls.CommonControls.TabPage tabOrderList;
        private Resco.Controls.CommonControls.TabPage tabAllOrder;
        private System.Windows.Forms.Panel panelBottomInfo;
        // private System.Windows.Forms.Panel pnlButtons;
        // private System.Windows.Forms.Panel pnlOptionalButtons;
        private System.Windows.Forms.TextBox lblDetailValue3;
        private System.Windows.Forms.Label lblDetailValue1;
        private System.Windows.Forms.Label lblDetailValue2;
        private System.Windows.Forms.Label lblDetailType3;
        private System.Windows.Forms.Label lblDetailType1;
        private System.Windows.Forms.Label lblDetailType2;

        private Resco.Controls.AdvancedTree.AdvancedTree treeList;
        private Resco.Controls.AdvancedTree.NodeTemplate Stop;
        private Resco.Controls.AdvancedTree.TextCell textCellStopAddress;
        private Resco.Controls.AdvancedTree.ImageCell imageCellStopStatus;
        private Resco.Controls.AdvancedTree.TextCell textCellStopId;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationId;
        private Resco.Controls.AdvancedTree.Cell cellIsLoadListOperation;
        private Resco.Controls.AdvancedTree.TextCell textCell1;
        private Resco.Controls.AdvancedTree.NodeTemplate SelectedStop;
        private Resco.Controls.AdvancedTree.TextCell textCellSelectedStopStopAddress;
        private Resco.Controls.AdvancedTree.ImageCell imageCellSelectedStopStopStatus;
        private Resco.Controls.AdvancedTree.TextCell textCellSelectedStopStopId;
        private Resco.Controls.AdvancedTree.TextCell textCellSelectedStopOperationId;
        private Resco.Controls.AdvancedTree.Cell cellSelectedStopIsLoadListOperation;
        private Resco.Controls.AdvancedTree.TextCell textCell2;
        private Resco.Controls.AdvancedTree.NodeTemplate StopSingleOperation;
        private Resco.Controls.AdvancedTree.ImageCell imageCellSingleOperationStopSign;
        private Resco.Controls.AdvancedTree.TextCell textCellSingleOperationStopAddress;
        private Resco.Controls.AdvancedTree.ImageCell imageCellSingleOperationStopStatus;
        private Resco.Controls.AdvancedTree.TextCell textCellSingleOperationStopId;
        private Resco.Controls.AdvancedTree.TextCell textCellSingleOperationOperationId;
        private Resco.Controls.AdvancedTree.Cell cellSingleOperationIsLoadListOperation;
        private Resco.Controls.AdvancedTree.TextCell textCell3;
        private Resco.Controls.AdvancedTree.NodeTemplate SelectedStopSingleOperation;
        private Resco.Controls.AdvancedTree.ImageCell imageCellSelectedSingleOperationStopSign;
        private Resco.Controls.AdvancedTree.TextCell textCellSelectedSingleOperationStopAddress;
        private Resco.Controls.AdvancedTree.ImageCell imageCellSelectedSingleOperationStopStatus;
        private Resco.Controls.AdvancedTree.TextCell textCellSelectedSingleOperationStopId;
        private Resco.Controls.AdvancedTree.TextCell textCellSelectedSingleOperationOperationId;
        private Resco.Controls.AdvancedTree.Cell cellSelectedSingleOperationIsLoadListOperation;
        private Resco.Controls.AdvancedTree.TextCell textCell4;
        private Resco.Controls.AdvancedTree.NodeTemplate Operation;
        private Resco.Controls.AdvancedTree.ImageCell imageCellOperationOperationSign;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationOperationAddress;
        private Resco.Controls.AdvancedTree.ImageCell imageCellOperationOperationStatus;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationOperationId;
        private Resco.Controls.AdvancedTree.TextCell textCell5;
        private Resco.Controls.AdvancedTree.NodeTemplate OperationSelected;
        private Resco.Controls.AdvancedTree.ImageCell imageCellOperationSelectedOperationSign;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationSelectedOperationAddress;
        private Resco.Controls.AdvancedTree.ImageCell imageCellOperationSelectedOperationStatus;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationSelectedStopId;
        private Resco.Controls.AdvancedTree.TextCell textCellOperationSelectedOperationId;
        private Resco.Controls.AdvancedTree.TextCell textCell6;
        private Resco.Controls.AdvancedTree.NodeTemplate OrderStops;
        private Resco.Controls.AdvancedTree.ImageCell imageCell1;
        private Resco.Controls.AdvancedTree.TextCell textCell7;
        private Resco.Controls.AdvancedTree.ImageCell imageCell2;
        private Resco.Controls.AdvancedTree.TextCell textCell8;
        private Resco.Controls.AdvancedTree.TextCell textCell9;
        private Resco.Controls.AdvancedTree.Cell cell1;
        private Resco.Controls.AdvancedTree.ImageCell imageCell3;
        private Resco.Controls.AdvancedTree.TextCell textCell10;
        private Resco.Controls.AdvancedTree.NodeTemplate SelectedOrderStops;
        private Resco.Controls.AdvancedTree.ImageCell imageCell4;
        private Resco.Controls.AdvancedTree.TextCell textCell11;
        private Resco.Controls.AdvancedTree.ImageCell imageCell5;
        private Resco.Controls.AdvancedTree.TextCell textCell12;
        private Resco.Controls.AdvancedTree.TextCell textCell13;
        private Resco.Controls.AdvancedTree.Cell cell2;
        private Resco.Controls.AdvancedTree.ImageCell imageCell6;
        private Resco.Controls.AdvancedTree.TextCell textCell14;
        private Panel panel1;
        private Label labelGoodAndOrderNumber;
        private Label labelRecientName;
        private Label labelCustomerInfo;
        private Label labelStopInfo;
        private TextBox textBoxStopInfo;

    }
}
﻿using System;
using System.Globalization;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;
using Resco.Controls.NumericUpDown;
using Resco.Controls.SmartGrid;
using RescoNumericUpDown = Resco.Controls.NumericUpDown.NumericUpDown;
using System.Diagnostics;

namespace Com.Bring.PMP.PreComFW.PickUp.Views
{
    public partial class FormPackageTypeAndTransport : Form
    {
        private const string SelectMessage = "<Select equipment>";
        private RescoNumericUpDown _numericUpDown, _activeNumeric;
        private Cell _activeCell;


        public FormPackageTypeAndTransport()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            OnShow();
        }

        private void OnShow()
        {
            var row = new Row(new[] { "Text1", "1" });

            gridPackageType.Rows.Add(row);
            row = new Row(new[] { "Text2", "2" });
            gridPackageType.Rows.Add(row);
            row = new Row(new[] { "Text3", "0" });
            gridPackageType.Rows.Add(row);

            var cb = CreateComboBox();

            cb.Items.Add("Text1");
            cb.Items.Add("Text2");
            cb.Items.Add("Text3");

            columnPackageType.CellEdit = CellEditType.Custom;
            columnPackageType.CellEditCustomControl = cb;

            _numericUpDown = CreateNumericUpDown();

            columnPackageQuantity.CellEdit = CellEditType.Custom;
            columnPackageQuantity.CellEditCustomControl = _numericUpDown;

            gridPackageType.BeforeCustomEdit += BeforeCustomEdit;
            gridPackageType.AfterCustomEdit += AfterCustomEdit;


            columnTransportType.CellEdit = CellEditType.Custom;
            columnTransportType.CellEditCustomControl = cb;

            columnTransportQuantity.CellEdit = CellEditType.Custom;
            columnTransportQuantity.CellEditCustomControl = _numericUpDown;

            gridTransportEquipment.BeforeCustomEdit += BeforeCustomEdit;
            gridTransportEquipment.AfterCustomEdit += AfterCustomEdit;


        }

        ComboBox CreateComboBox()
        {
            var cb = new ComboBox { DropDownStyle = ComboBoxStyle.DropDownList, Font = gridPackageType.Font };
            cb.SelectedValueChanged += OnComboBoxChanged;
            return cb;
        }

        RescoNumericUpDown CreateNumericUpDown()
        {
            var numericUpDown = new RescoNumericUpDown
                                    {
                                        Font = gridPackageType.Font,
                                        //UpDownStyle = UpDownStyle.DownUp,
                                        UpDownAlign = UpDownAlignment.HorizontalSplit,
                                        UpDownWidth = 25,
                                        TextAlign = HorizontalAlignment.Center,
                                        ReadOnly = true,
                                        Maximum = 100
                                    };

            numericUpDown.MinusButton.AdornmentType = AdornmentType.Minus;
            numericUpDown.MinusButton.AdornmentSize = 12;
            numericUpDown.PlusButton.AdornmentType = AdornmentType.Plus;
            numericUpDown.PlusButton.AdornmentSize = 12;
            numericUpDown.AutoSelectOnPlusMinus = false;
            numericUpDown.MouseDown += OnNumericClick;
            numericUpDown.ValueChanged += OnNumericChanged;


            return numericUpDown;
        }

        /// <summary>
        /// Launch numeric keyboard if we press in the edit-filed of numeric up-down
        /// </summary>
        void OnNumericClick(object sender, EventArgs e)
        {
            _activeNumeric = (RescoNumericUpDown)sender;
            var keyPad = new Keypad("custom", "[1|2|3][4|5|6][7|8|9][{0}{Backspace}]");
            var panelItem = new InputPanelItem(_activeNumeric, "Enter number", GetNumberFromKeyboard, keyPad);

            _activeNumeric.Value = 34;

            //PreCom.Controls.PreComInputPanel2.Show(panelItem);
        }

        void OnNumericChanged(object sender, EventArgs e)
        {
            _activeCell.Data = ((RescoNumericUpDown)sender).Text;
        }

        void OnComboBoxChanged(object sender, EventArgs e)
        {
            _activeCell.Data = ((ComboBox)sender).SelectedItem;
        }


        private void GetNumberFromKeyboard(object sender, InputPanelArgs e)
        {
            if (e.Reason == InputPanelReason.Ok)
            {
                _activeNumeric.Text = e.Text;
            }
        }

        void AfterCustomEdit(object sender, CustomEditEventArgs e)
        {
            var numericUpDown = (e.Control) as RescoNumericUpDown;
            if (numericUpDown != null)
            {
                // We are in a numeric type cell (column 1)
                e.Cell.Data = numericUpDown.Value.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                // We are in a combobox type cell (column 0)
                var comboBox = e.Control as ComboBox;
                Debug.Assert(comboBox != null);
                var value = (string)comboBox.SelectedItem;
                if (string.IsNullOrEmpty(value))
                    value = SelectMessage;
                e.Cell.Data = value;
            }
        }

        void BeforeCustomEdit(object sender, CustomEditEventArgs e)
        {
            _activeCell = e.Cell;
            var numericUpDown = (e.Control) as RescoNumericUpDown;
            if (numericUpDown != null)
            {

                int value = Convert.ToInt32(e.Cell.Data);
                // We are in a numeric type cell (column 1)
                numericUpDown.Value = value;
            }
            else
            {
                // We are in a combobox type cell (column 0)
                var comboBox = e.Control as ComboBox;
                Debug.Assert(comboBox != null);
                var value = (string)e.Cell.Data;
                if (value != SelectMessage)
                    comboBox.SelectedItem = e.Cell.Data;
                else
                    comboBox.SelectedIndex = -1;
            }
        }



        private void ButtonOkClick(object sender, EventArgs e)
        {
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
        }

        // Delete last active row, if count was 0
        private void OnSelectionChanged(object sender, EventArgs e)
        {
            var grid = (SmartGrid) sender;
            int activeRowIndex = grid.ActiveRowIndex;

            // Remove other rows with 0 count 
            for (int i = grid.Rows.Count - 1; i >= 0; i --)
            {
                if (i != activeRowIndex)
                {
                    var counterCell = grid.Cells[i, 1];
                    if (Convert.ToInt32(counterCell.Data) == 0)
                    {
                        grid.Rows.RemoveAt(i);
                    }
                }
            }
        }

        private void OnAddTransportEquipmentToGrid(object sender, EventArgs e)
        {
            var row = new Row(new[] { SelectMessage, "0" });
            gridTransportEquipment.Rows.Add(row);
        }

        private void OnAddPackageTypeToGrid(object sender, EventArgs e)
        {
            var row = new Row(new[] { SelectMessage, "0" });
            gridPackageType.Rows.Add(row);
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {

        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {

        }

        private void BtnRejectClick(object sender, EventArgs e)
        {

        }

        private void BtnAcceptClick(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SampleScreenDesigner.Forms
{
    public partial class FormConnectionSatus : Form
    {
        public FormConnectionSatus()
        {
            InitializeComponent();
            SetTextToControls();
        }
        private void SetTextToControls()
        {
            lblMessage.Text = "Wish to keep existing connection "+Environment.NewLine + " or create New ?";
            this.lblMessageDisconnected.Text = "Physical carrier already disconnected";
            this.lblOldDestination.Text = "Destination 2050";
            this.lblOldPower.Text = "PWR 1";
            this.lblNewConnection.Text = "New" + Environment.NewLine + "Connection";
            this.lblNewPwr.Text = "PWR 1";
            this.lblNewDestination.Text = "Destination 2050";
            this.lblOldConnection.Text = "Old Connection";
            this.btnNewConnection.Text = "New Connection";
            this.btnKeepOld.Text = "Keep Old";
            this.lblPhysicalLoadConnected.Text = "Physical carrier already disconnected";
            this.lblModuleName.Text = "Load Line haul";
        }
    }
 

}
﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Views
{
    partial class FormReconcillation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.webBrowserDetail = new System.Windows.Forms.WebBrowser();
            this.buttonDeviation = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonItems = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonDetails = new Resco.Controls.OutlookControls.ImageButton();
            this.ListConsignments = new Resco.Controls.AdvancedList.AdvancedList();
            this.templateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellHeaderCol0 = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellSelectedConsignmentStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.imageListStatus = new System.Windows.Forms.ImageList();
            this.cellSelectedConsignmentId = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedConsignmentStatusCount = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowUnselected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellUnselectedConsignmentStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.cellUnselectedConsignmentId = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedConsignmentStatusCount = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelectedConsItems = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellItemSelectedConsItemId = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemSelectedConsItemStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.templateRowUnselectedConsItems = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellItemUnselectedConsItemId = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemUnselectedConsItemStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonDelete = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDeviation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.buttonDelete);
            this.touchPanel.Controls.Add(this.webBrowserDetail);
            this.touchPanel.Controls.Add(this.buttonDeviation);
            this.touchPanel.Controls.Add(this.buttonItems);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.buttonBack);
            this.touchPanel.Controls.Add(this.buttonDetails);
            this.touchPanel.Controls.Add(this.ListConsignments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // webBrowserDetail
            // 
            this.webBrowserDetail.Location = new System.Drawing.Point(11, 61);
            this.webBrowserDetail.Name = "webBrowserDetail";
            this.webBrowserDetail.Size = new System.Drawing.Size(459, 360);
            // 
            // buttonDeviation
            // 
            this.buttonDeviation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonDeviation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonDeviation.ForeColor = System.Drawing.Color.White;
            this.buttonDeviation.Location = new System.Drawing.Point(161, 426);
            this.buttonDeviation.Name = "buttonDeviation";
            this.buttonDeviation.Size = new System.Drawing.Size(158, 50);
            this.buttonDeviation.TabIndex = 34;
            this.buttonDeviation.Click += new System.EventHandler(this.ButtonDeviationClick);
            // 
            // buttonItems
            // 
            this.buttonItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonItems.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonItems.ForeColor = System.Drawing.Color.White;
            this.buttonItems.Location = new System.Drawing.Point(0, 426);
            this.buttonItems.Name = "buttonItems";
            this.buttonItems.Size = new System.Drawing.Size(158, 50);
            this.buttonItems.TabIndex = 33;
            this.buttonItems.Text = "Items";
            this.buttonItems.Click += new System.EventHandler(this.ButtonItemsClick);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(322, 483);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(158, 50);
            this.buttonOk.TabIndex = 31;
            this.buttonOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(0, 483);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(158, 50);
            this.buttonBack.TabIndex = 29;
            this.buttonBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // buttonDetails
            // 
            this.buttonDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonDetails.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonDetails.ForeColor = System.Drawing.Color.White;
            this.buttonDetails.Location = new System.Drawing.Point(161, 483);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(158, 50);
            this.buttonDetails.TabIndex = 28;
            this.buttonDetails.Click += new System.EventHandler(this.ButtonDetailClick);
            // 
            // ListConsignments
            // 
            this.ListConsignments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.ListConsignments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListConsignments.DataRows.Clear();
            this.ListConsignments.GridColor = System.Drawing.Color.Black;
            this.ListConsignments.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.ListConsignments.Location = new System.Drawing.Point(11, 61);
            this.ListConsignments.Name = "ListConsignments";
            this.ListConsignments.ScrollbarSmallChange = 32;
            this.ListConsignments.ScrollbarWidth = 26;
            this.ListConsignments.ShowHeader = true;
            this.ListConsignments.Size = new System.Drawing.Size(459, 360);
            this.ListConsignments.TabIndex = 3;
            this.ListConsignments.Templates.Add(this.templateHeader);
            this.ListConsignments.Templates.Add(this.templateRowSelected);
            this.ListConsignments.Templates.Add(this.templateRowUnselected);
            this.ListConsignments.Templates.Add(this.templateRowSelectedConsItems);
            this.ListConsignments.Templates.Add(this.templateRowUnselectedConsItems);
            this.ListConsignments.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            // 
            // templateHeader
            // 
            this.templateHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol0);
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol1);
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol2);
            this.templateHeader.Height = 28;
            this.templateHeader.Name = "templateHeader";
            // 
            // cellHeaderCol0
            // 
            this.cellHeaderCol0.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderCol0.CellSource.ConstantData = "S";
            this.cellHeaderCol0.DesignName = "cellHeaderCol0";
            this.cellHeaderCol0.Location = new System.Drawing.Point(20, 0);
            this.cellHeaderCol0.Name = "cellHeaderCol0";
            this.cellHeaderCol0.Size = new System.Drawing.Size(-1, 28);
            this.cellHeaderCol0.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellHeaderCol1
            // 
            this.cellHeaderCol1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellHeaderCol1.CellSource.ConstantData = "Consignments";
            this.cellHeaderCol1.DesignName = "cellHeaderCol1";
            this.cellHeaderCol1.Location = new System.Drawing.Point(40, 0);
            this.cellHeaderCol1.Name = "cellHeaderCol1";
            this.cellHeaderCol1.Size = new System.Drawing.Size(280, 28);
            this.cellHeaderCol1.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // cellHeaderCol2
            // 
            this.cellHeaderCol2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderCol2.CellSource.ConstantData = "Actual/Planned";
            this.cellHeaderCol2.DesignName = "cellHeaderCol2";
            this.cellHeaderCol2.Location = new System.Drawing.Point(280, 0);
            this.cellHeaderCol2.Name = "cellHeaderCol2";
            this.cellHeaderCol2.Size = new System.Drawing.Size(-1, 28);
            this.cellHeaderCol2.TextFont = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowSelected
            // 
            this.templateRowSelected.BackColor = System.Drawing.Color.DarkGray;
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedConsignmentStatusSymbol);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedConsignmentId);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedConsignmentStatusCount);
            this.templateRowSelected.Height = 30;
            this.templateRowSelected.Name = "templateRowSelected";
            // 
            // cellSelectedConsignmentStatusSymbol
            // 
            this.cellSelectedConsignmentStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellSelectedConsignmentStatusSymbol.AutoTransparent = true;
            this.cellSelectedConsignmentStatusSymbol.CellSource.ColumnIndex = 0;
            this.cellSelectedConsignmentStatusSymbol.DesignName = "cellSelectedConsignmentStatusSymbol";
            this.cellSelectedConsignmentStatusSymbol.ImageList = this.imageListStatus;
            this.cellSelectedConsignmentStatusSymbol.Location = new System.Drawing.Point(20, 0);
            this.cellSelectedConsignmentStatusSymbol.Size = new System.Drawing.Size(-1, 30);
       
            // 
            // cellSelectedConsignmentId
            // 
            this.cellSelectedConsignmentId.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellSelectedConsignmentId.CellSource.ColumnIndex = 1;
            this.cellSelectedConsignmentId.DesignName = "cellSelectedConsignmentId";
            this.cellSelectedConsignmentId.Location = new System.Drawing.Point(40, 0);
            this.cellSelectedConsignmentId.Size = new System.Drawing.Size(280, 30);
            this.cellSelectedConsignmentId.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            // 
            // cellSelectedConsignmentStatusCount
            // 
            this.cellSelectedConsignmentStatusCount.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellSelectedConsignmentStatusCount.CellSource.ColumnIndex = 2;
            this.cellSelectedConsignmentStatusCount.DesignName = "cellSelectedConsignmentStatusCount";
            this.cellSelectedConsignmentStatusCount.Location = new System.Drawing.Point(280, 0);
            this.cellSelectedConsignmentStatusCount.Size = new System.Drawing.Size(-1, 30);
            this.cellSelectedConsignmentStatusCount.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowUnselected
            // 
            this.templateRowUnselected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedConsignmentStatusSymbol);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedConsignmentId);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedConsignmentStatusCount);
            this.templateRowUnselected.Height = 30;
            this.templateRowUnselected.Name = "templateRowUnselected";
            // 
            // cellUnselectedConsignmentStatusSymbol
            // 
            this.cellUnselectedConsignmentStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellUnselectedConsignmentStatusSymbol.AutoTransparent = true;
            this.cellUnselectedConsignmentStatusSymbol.CellSource.ColumnIndex = 0;
            this.cellUnselectedConsignmentStatusSymbol.DesignName = "cellUnselectedConsignmentStatusSymbol";
            this.cellUnselectedConsignmentStatusSymbol.ImageList = this.imageListStatus;
            this.cellUnselectedConsignmentStatusSymbol.Location = new System.Drawing.Point(20, 0);
            this.cellUnselectedConsignmentStatusSymbol.Size = new System.Drawing.Size(-1, 30);
            // 
            // cellUnselectedConsignmentId
            // 
            this.cellUnselectedConsignmentId.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellUnselectedConsignmentId.CellSource.ColumnIndex = 1;
            this.cellUnselectedConsignmentId.DesignName = "cellUnselectedConsignmentId";
            this.cellUnselectedConsignmentId.Location = new System.Drawing.Point(40, 0);
            this.cellUnselectedConsignmentId.Size = new System.Drawing.Size(280, 30);
            this.cellUnselectedConsignmentId.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            // 
            // cellUnselectedConsignmentStatusCount
            // 
            this.cellUnselectedConsignmentStatusCount.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellUnselectedConsignmentStatusCount.CellSource.ColumnIndex = 2;
            this.cellUnselectedConsignmentStatusCount.DesignName = "cellUnselectedConsignmentStatusCount";
            this.cellUnselectedConsignmentStatusCount.Location = new System.Drawing.Point(280, 0);
            this.cellUnselectedConsignmentStatusCount.Size = new System.Drawing.Size(-1, 30);
            this.cellUnselectedConsignmentStatusCount.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowSelectedConsItems
            // 
            this.templateRowSelectedConsItems.BackColor = System.Drawing.Color.DarkGray;
            this.templateRowSelectedConsItems.CellTemplates.Add(this.cellItemSelectedConsItemId);
            this.templateRowSelectedConsItems.CellTemplates.Add(this.cellItemSelectedConsItemStatusSymbol);
            this.templateRowSelectedConsItems.Height = 30;
            this.templateRowSelectedConsItems.Name = "templateRowSelectedConsItems";
            // 
            // cellItemSelectedConsItemId
            // 
            this.cellItemSelectedConsItemId.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellItemSelectedConsItemId.DesignName = "cellItemSelectedConsItemId";
            this.cellItemSelectedConsItemId.Location = new System.Drawing.Point(40, 0);
            this.cellItemSelectedConsItemId.Size = new System.Drawing.Size(280, 30);
            this.cellItemSelectedConsItemId.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            // 
            // cellItemSelectedConsItemStatusSymbol
            // 
            this.cellItemSelectedConsItemStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellItemSelectedConsItemStatusSymbol.AutoTransparent = true;
            this.cellItemSelectedConsItemStatusSymbol.CellSource.ColumnIndex = 1;
            this.cellItemSelectedConsItemStatusSymbol.DesignName = "cellItemSelectedConsItemStatusSymbol";
            this.cellItemSelectedConsItemStatusSymbol.ImageList = this.imageListStatus;
            this.cellItemSelectedConsItemStatusSymbol.Location = new System.Drawing.Point(280, 0);
            this.cellItemSelectedConsItemStatusSymbol.Size = new System.Drawing.Size(-1, 30);
            // 
            // templateRowUnselectedConsItems
            // 
            this.templateRowUnselectedConsItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowUnselectedConsItems.CellTemplates.Add(this.cellItemUnselectedConsItemId);
            this.templateRowUnselectedConsItems.CellTemplates.Add(this.cellItemUnselectedConsItemStatusSymbol);
            this.templateRowUnselectedConsItems.Height = 30;
            this.templateRowUnselectedConsItems.Name = "templateRowUnselectedConsItems";
            // 
            // cellItemUnselectedConsItemId
            // 
            this.cellItemUnselectedConsItemId.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellItemUnselectedConsItemId.DesignName = "cellItemUnselectedConsItemId";
            this.cellItemUnselectedConsItemId.Location = new System.Drawing.Point(40, 0);
            this.cellItemUnselectedConsItemId.Size = new System.Drawing.Size(280, 30);
            this.cellItemUnselectedConsItemId.TextFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            // 
            // cellItemUnselectedConsItemStatusSymbol
            // 
            this.cellItemUnselectedConsItemStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellItemUnselectedConsItemStatusSymbol.AutoTransparent = true;
            this.cellItemUnselectedConsItemStatusSymbol.CellSource.ColumnIndex = 1;
            this.cellItemUnselectedConsItemStatusSymbol.DesignName = "cellItemUnselectedConsItemStatusSymbol";
            this.cellItemUnselectedConsItemStatusSymbol.ImageList = this.imageListStatus;
            this.cellItemUnselectedConsItemStatusSymbol.Location = new System.Drawing.Point(280, 0);
            this.cellItemUnselectedConsItemStatusSymbol.Size = new System.Drawing.Size(-1, 30);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 58);
            this.labelModuleName.Text = "Header";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonDelete.ForeColor = System.Drawing.Color.White;
            this.buttonDelete.Location = new System.Drawing.Point(322, 426);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(158, 50);
            this.buttonDelete.TabIndex = 36;
            // 
            // FormReconcillation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Name = "FormReconcillation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonDeviation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonDelete)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.AdvancedList.AdvancedList ListConsignments;
        private Resco.Controls.AdvancedList.RowTemplate templateHeader;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol0;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol1;
        private Resco.Controls.AdvancedList.ImageCell cellSelectedConsignmentStatusSymbol;
        private Resco.Controls.AdvancedList.TextCell cellSelectedConsignmentId;
        private Resco.Controls.AdvancedList.TextCell cellSelectedConsignmentStatusCount;
        private Resco.Controls.AdvancedList.ImageCell cellUnselectedConsignmentStatusSymbol;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedConsignmentId;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedConsignmentStatusCount;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol2;
        private System.Windows.Forms.ImageList imageListStatus;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselectedConsItems;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelectedConsItems;
        private Resco.Controls.AdvancedList.TextCell cellItemUnselectedConsItemId;
        private Resco.Controls.AdvancedList.TextCell cellItemSelectedConsItemId;
        private Resco.Controls.AdvancedList.ImageCell cellItemUnselectedConsItemStatusSymbol;
        private Resco.Controls.AdvancedList.ImageCell cellItemSelectedConsItemStatusSymbol;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
        private Resco.Controls.OutlookControls.ImageButton buttonDetails;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.OutlookControls.ImageButton buttonDeviation;
        private Resco.Controls.OutlookControls.ImageButton buttonItems;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelected;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselected;
        private WebBrowser webBrowserDetail;
        private Resco.Controls.OutlookControls.ImageButton buttonDelete;
    }
}
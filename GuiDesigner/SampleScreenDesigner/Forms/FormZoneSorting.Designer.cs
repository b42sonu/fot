﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.OutlookControls;
using PreCom.Utils;

namespace SampleScreenDesigner.Forms
{
    partial class FormZoneSorting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoadList));
            this.pnlMessage = new Panel();
            this.lblInfoHeading = new System.Windows.Forms.Label();
            this.picMessage = new System.Windows.Forms.PictureBox();
            this.btnZone1Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone2Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone3Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone4Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone5Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone6Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone7Details = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone8Details = new Resco.Controls.OutlookControls.ImageButton();


            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationListIcons = new System.Windows.Forms.ImageList();
            this.btnSortZone = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone1 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone2 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone3 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone4 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone5 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone6 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone7 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnZone8 = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancelZoneItemSort = new ImageButton();
            
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsigneeAdr = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeAdrHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.listDeliveries = new Resco.Controls.AdvancedList.AdvancedList();
            this.RowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.ImageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.ImageCellAlternameOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.lblConsignee = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsigneeHead = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentItemsHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.tabControlDeliveries = new Resco.Controls.CommonControls.TabControl();
            this.tabNotStarted = new Resco.Controls.CommonControls.TabPage();
            this.tabCompleted = new Resco.Controls.CommonControls.TabPage();
            this.tabAll = new Resco.Controls.CommonControls.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdrHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItemsHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).BeginInit();
            this.tabControlDeliveries.SuspendLayout();
            this.tabNotStarted.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Load List";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            this.labelHeading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelHeading.AutoSize = false;
            this.labelHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeading.Location = new System.Drawing.Point(11, 30);
            this.labelHeading.Name = "labelModuleName";
            this.labelHeading.Size = new System.Drawing.Size(459, 27);
            this.labelHeading.Text = "Click on Zone to assign selected row";
            this.labelHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;

            this.labelMessage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelMessage.AutoSize = false;
            this.labelMessage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelMessage.Location = new System.Drawing.Point(11, 57);
            this.labelMessage.Name = "labelModuleName";
            this.labelMessage.Size = new System.Drawing.Size(459, 27);
            this.labelMessage.Text = "Delivery moved into Zone 3";
            this.labelMessage.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;


            this.imageListOperationListIcons.Images.Clear();
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("Delivery"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource2"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource3"))));
            this.imageListOperationListIcons.Images.Add(((System.Drawing.Image)(resources.GetObject("resource1"))));
            
            // 
            // touchPanel
            //
            this.touchPanel.Controls.Add(this.pnlMessage);
            this.touchPanel.Controls.Add(this.lblConsigneeAdr);
            this.touchPanel.Controls.Add(this.lblConsigneeAdrHeading);
            
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.btnSortZone);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.labelHeading);
            this.touchPanel.Controls.Add(this.labelMessage);
            this.touchPanel.Controls.Add(this.lblConsignee);
            this.touchPanel.Controls.Add(this.lblConsignmentItems);
            this.touchPanel.Controls.Add(this.lblConsigneeHead);
            this.touchPanel.Controls.Add(this.lblConsignmentItemsHeading);
            this.touchPanel.Controls.Add(this.btnZone1);
            this.touchPanel.Controls.Add(this.btnZone2);
            this.touchPanel.Controls.Add(this.btnZone3);
            this.touchPanel.Controls.Add(this.btnZone4);
            this.touchPanel.Controls.Add(this.btnZone5);
            this.touchPanel.Controls.Add(this.btnZone6);
            this.touchPanel.Controls.Add(this.btnZone7);
            this.touchPanel.Controls.Add(this.btnZone8);
            
            this.touchPanel.Controls.Add(listDeliveries);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);

            // 
            // pnlMessage
            // 
            this.pnlMessage.BackColor = System.Drawing.Color.White;
            this.pnlMessage.Location = new System.Drawing.Point(15, 90);
            this.pnlMessage.Name = "pnlButtons";
            this.pnlMessage.Controls.Add(this.lblInfoHeading);
            this.pnlMessage.Controls.Add(this.picMessage);
            this.pnlMessage.Controls.Add(this.btnZone1Details);
            this.pnlMessage.Controls.Add(this.btnZone2Details);
            this.pnlMessage.Controls.Add(this.btnZone3Details);
            this.pnlMessage.Controls.Add(this.btnZone4Details);
            this.pnlMessage.Controls.Add(this.btnZone5Details);
            this.pnlMessage.Controls.Add(this.btnZone6Details);
            this.pnlMessage.Controls.Add(this.btnZone7Details);
            this.pnlMessage.Controls.Add(this.btnZone8Details);
            this.pnlMessage.Controls.Add(this.btnCancelZoneItemSort);
            this.pnlMessage.Size = new System.Drawing.Size(450, 380);
            this.pnlMessage.Paint += PnlConfirmMessagePaint;

            

            // 
            // lblInfoHeading
            // 
            this.lblInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblInfoHeading.Location = new System.Drawing.Point(10, 90);
            this.lblInfoHeading.Name = "lblAccepted";
            this.lblInfoHeading.Size = new System.Drawing.Size(430, 30);
            this.lblInfoHeading.Text = "Select a Zone to sort";
            this.lblInfoHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;

            //  
            // picMessage
            // 
            this.picMessage.Image = SampleScreenDesigner.Properties.Resources.FOT_icon_attention;
            this.picMessage.Location = new System.Drawing.Point(200, 20);
            this.picMessage.Name = "picMessage";
            this.picMessage.Size = new System.Drawing.Size(50, 50);

            // 
            // btnZone1Details
            // 
            this.btnZone1Details.Location = new System.Drawing.Point(10, 140);
            this.btnZone1Details.Name = "btnZone1Details";
            this.btnZone1Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone1Details.TabIndex = 3;
            this.btnZone1Details.Text =  "1" + System.Environment.NewLine + "(#20)";
            this.btnZone1Details.Paint += PnlBtnZoneDetailsPaint;
            this.btnZone1Details.TextAlignment = Alignment.MiddleCenter;
            this.btnZone1Details.Click += new System.EventHandler(btnZone1Details_Click);
            // 
            // btnZone2Details
            // 
            this.btnZone2Details.Location = new System.Drawing.Point(120, 140);
            this.btnZone2Details.Name = "btnZone1Details";
            this.btnZone2Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone2Details.TabIndex = 3;
            this.btnZone2Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone2Details.Paint += PnlBtnZoneDetailsPaint;

            // 
            // btnZone3Details
            // 
            this.btnZone3Details.Location = new System.Drawing.Point(230, 140);
            this.btnZone3Details.Name = "btnZone1Details";
            this.btnZone3Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone3Details.TabIndex = 3;
            this.btnZone3Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone3Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone4Details
            // 
            this.btnZone4Details.Location = new System.Drawing.Point(340, 140);
            this.btnZone4Details.Name = "btnZone1Details";
            this.btnZone4Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone4Details.TabIndex = 3;
            this.btnZone4Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone4Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone5Details
            // 
            this.btnZone5Details.Location = new System.Drawing.Point(10, 230);
            this.btnZone5Details.Name = "btnZone1Details";
            this.btnZone5Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone5Details.TabIndex = 3;
            this.btnZone5Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone5Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone6Details
            // 
            this.btnZone6Details.Location = new System.Drawing.Point(120, 230);
            this.btnZone6Details.Name = "btnZone1Details";
            this.btnZone6Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone6Details.TabIndex = 3;
            this.btnZone6Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone6Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone7Details
            // 
            this.btnZone7Details.Location = new System.Drawing.Point(230, 230);
            this.btnZone7Details.Name = "btnZone1Details";
            this.btnZone7Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone7Details.TabIndex = 3;
            this.btnZone7Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone7Details.Paint += PnlBtnZoneDetailsPaint;
            // 
            // btnZone8Details
            // 
            this.btnZone8Details.Location = new System.Drawing.Point(340, 230);
            this.btnZone8Details.Name = "btnZone1Details";
            this.btnZone8Details.Size = new System.Drawing.Size(100, 80);
            this.btnZone8Details.TabIndex = 3;
            this.btnZone8Details.Text = "1" + System.Environment.NewLine + "(#20)";
            this.btnZone8Details.Paint += PnlBtnZoneDetailsPaint;


            // 
            // btnOk
            // 
            this.btnCancelZoneItemSort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancelZoneItemSort.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancelZoneItemSort.ForeColor = System.Drawing.Color.White;
            this.btnCancelZoneItemSort.Location = new System.Drawing.Point(146, 315);
            this.btnCancelZoneItemSort.Name = "btnCancelZoneItemSort";
            this.btnCancelZoneItemSort.Size = new System.Drawing.Size(158, 50);
            this.btnCancelZoneItemSort.TabIndex = 32;
            this.btnCancelZoneItemSort.Text = "Cancel";
            this.btnCancelZoneItemSort.Click += new System.EventHandler(btnOk_Click);


            // 
            // btnPredef
            // 
            this.btnSortZone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnSortZone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnSortZone.ForeColor = System.Drawing.Color.White;
            this.btnSortZone.Location = new System.Drawing.Point(161, 483);
            this.btnSortZone.Name = "btnSortZone";
            this.btnSortZone.Size = new System.Drawing.Size(158, 50);
            this.btnSortZone.TabIndex = 30;
            this.btnSortZone.Text = "Sort Zone";
            this.btnSortZone.Click += new System.EventHandler(btnSortZone_Click);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 483);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 31;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(btnBack_Click);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(322, 483);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(158, 50);
            this.btnOk.TabIndex = 32;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(btnOk_Click);

            // 
            // btnZone1
            // 
            this.btnZone1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone1.ForeColor = System.Drawing.Color.White;
            this.btnZone1.Location = new System.Drawing.Point(20, 390);
            this.btnZone1.Name = "btnZone1";
            this.btnZone1.Size = new System.Drawing.Size(50, 50);
            this.btnZone1.TabIndex = 32;
            this.btnZone1.Text = "1";
            this.btnZone1.TextAlignment = Alignment.TopCenter;
            this.btnZone1.Click += new System.EventHandler(BtnZoneClick);

            // 
            // btnZone2
            // 
            this.btnZone2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone2.ForeColor = System.Drawing.Color.White;
            this.btnZone2.Location = new System.Drawing.Point(75, 390);
            this.btnZone2.Name = "btnZone2";
            this.btnZone2.Size = new System.Drawing.Size(50, 50);
            this.btnZone2.TabIndex = 32;
            this.btnZone2.Text = "2";
            this.btnZone2.TextAlignment = Alignment.TopCenter;
            this.btnZone2.Click += new System.EventHandler(BtnZoneClick);


            // 
            // btnZone3
            // 
            this.btnZone3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone3.ForeColor = System.Drawing.Color.White;
            this.btnZone3.Location = new System.Drawing.Point(130, 390);
            this.btnZone3.Name = "btnZone3";
            this.btnZone3.Size = new System.Drawing.Size(50, 50);
            this.btnZone3.TabIndex = 32;
            this.btnZone3.Text = "3";
            this.btnZone3.TextAlignment = Alignment.TopCenter;
            this.btnZone3.Click += new System.EventHandler(BtnZoneClick);

            // 
            // btnZone4
            // 
            this.btnZone4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone4.ForeColor = System.Drawing.Color.White;
            this.btnZone4.Location = new System.Drawing.Point(185, 390);
            this.btnZone4.Name = "btnZone4";
            this.btnZone4.Size = new System.Drawing.Size(50, 50);
            this.btnZone4.TabIndex = 32;
            this.btnZone4.Text = "4";
            this.btnZone4.TextAlignment = Alignment.TopCenter;
            this.btnZone4.Click += new System.EventHandler(BtnZoneClick);

            // 
            // btnZone5
            // 
            this.btnZone5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone5.ForeColor = System.Drawing.Color.White;
            this.btnZone5.Location = new System.Drawing.Point(240, 390);
            this.btnZone5.Name = "btnZone5";
            this.btnZone5.Size = new System.Drawing.Size(50, 50);
            this.btnZone5.TabIndex = 32;
            this.btnZone5.Text = "5";
            this.btnZone5.TextAlignment = Alignment.TopCenter;
            this.btnZone5.Click += new System.EventHandler(BtnZoneClick);

            // 
            // btnZone6
            // 
            this.btnZone6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone6.ForeColor = System.Drawing.Color.White;
            this.btnZone6.Location = new System.Drawing.Point(295, 390);
            this.btnZone6.Name = "btnZone6";
            this.btnZone6.Size = new System.Drawing.Size(50, 50);
            this.btnZone6.TabIndex = 32;
            this.btnZone6.Text = "6";
            this.btnZone6.TextAlignment = Alignment.TopCenter;
            this.btnZone6.Click += new System.EventHandler(BtnZoneClick);

            // 
            // btnZone7
            // 
            this.btnZone7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone7.ForeColor = System.Drawing.Color.White;
            this.btnZone7.Location = new System.Drawing.Point(350, 390);
            this.btnZone7.Name = "btnZone7";
            this.btnZone7.Size = new System.Drawing.Size(50, 50);
            this.btnZone7.TabIndex = 32;
            this.btnZone7.Text = "7";
            this.btnZone7.TextAlignment = Alignment.TopCenter;
            this.btnZone7.Click += new System.EventHandler(BtnZoneClick);

            // 
            // btnZone8
            // 
            this.btnZone8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnZone8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnZone8.ForeColor = System.Drawing.Color.White;
            this.btnZone8.Location = new System.Drawing.Point(405, 390);
            this.btnZone8.Name = "btnZone8";
            this.btnZone8.Size = new System.Drawing.Size(50, 50);
            this.btnZone8.TabIndex = 32;
            this.btnZone8.Text = "8";
            this.btnZone8.TextAlignment = Alignment.TopCenter;
            this.btnZone8.Click += new System.EventHandler(BtnZoneClick);
            

           

            // 
            // listDeliveries
            // 
            this.listDeliveries.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listDeliveries.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listDeliveries.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listDeliveries.DataRows.Clear();
            this.listDeliveries.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.listDeliveries.Location = new System.Drawing.Point(0, 90);
            this.listDeliveries.MultiSelect = true;
            this.listDeliveries.Name = "listDeliveries";
            this.listDeliveries.ScrollbarSmallChange = 32;
            this.listDeliveries.ScrollbarWidth = 26;
            this.listDeliveries.SelectedTemplateIndex = 2;
            this.listDeliveries.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listDeliveries.ShowHeader = true;
            this.listDeliveries.Size = new System.Drawing.Size(480, 290);
            this.listDeliveries.TabIndex = 2;
            this.listDeliveries.TemplateIndex = 1;
            this.listDeliveries.Templates.Add(this.RowTemplateHeader);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOp);
            this.listDeliveries.Templates.Add(this.RowTemplatePlannedOpAlternateTemp);
            this.listDeliveries.TouchScrolling = true;
            this.listDeliveries.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.RowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RowTemplateHeader.CellTemplates.Add(this.TextCellHeaderTemplate);
            this.RowTemplateHeader.Height = 0;
            this.RowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.TextCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.TextCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.TextCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.TextCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.TextCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.TextCellHeaderTemplate.Visible = false;
            // 
            // RowTemplatePlannedOp
            // 
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationType);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationDetail);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.ImageCellOperationStatus);
            this.RowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationOperationId);
            this.RowTemplatePlannedOp.Height = 64;
            this.RowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.ImageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationType.AutoResize = true;
            this.ImageCellOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellOperationType.DesignName = "ImageCellOperationType";
            this.ImageCellOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationType.Location = new System.Drawing.Point(15, 10);
            this.ImageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // ImageCellOperationStatus
            // 
            this.ImageCellOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellOperationStatus.DesignName = "ImageCellOperationStatus";
            this.ImageCellOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "DeliveryOperation.Consignment.ConsignmentNumber";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;
            
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.RowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternateOperationType);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.ImageCellAlternameOperationStatus);
            this.RowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.RowTemplatePlannedOpAlternateTemp.Height = 64;
            this.RowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.ImageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternateOperationType.AutoResize = true;
            this.ImageCellAlternateOperationType.CellSource.ColumnName = "OperationType";
            this.ImageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.ImageCellAlternateOperationType.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternateOperationType.Location = new System.Drawing.Point(15, 10);
            this.ImageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // ImageCellAlternameOperationStatus
            // 
            this.ImageCellAlternameOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.ImageCellAlternameOperationStatus.CellSource.ColumnName = "Status";
            this.ImageCellAlternameOperationStatus.DesignName = "ImageCellAlternameOperationStatus";
            this.ImageCellAlternameOperationStatus.ImageList = this.imageListOperationListIcons;
            this.ImageCellAlternameOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.ImageCellAlternameOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;
           

            // 
            // lblConsigneeHead
            // 
            this.lblConsigneeHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeHead.Location = new System.Drawing.Point(3, 337);
            this.lblConsigneeHead.Name = "lblConsigneeHead";
            this.lblConsigneeHead.Size = new System.Drawing.Size(61, 14);
            this.lblConsigneeHead.Text = "Consignee:";
            // 
            // lblConsignee
            // 
            this.lblConsignee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignee.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignee.Location = new System.Drawing.Point(170, 337);
            this.lblConsignee.Name = "lblConsignee";
            this.lblConsignee.Size = new System.Drawing.Size(21, 14);
            this.lblConsignee.Text = "Abc";

            // 
            // lblConsignmentItemsHeading
            // 
            this.lblConsignmentItemsHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItemsHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsignmentItemsHeading.Location = new System.Drawing.Point(3, 369);
            this.lblConsignmentItemsHeading.Name = "lblConsignmentItemsHeading";
            this.lblConsignmentItemsHeading.Size = new System.Drawing.Size(81, 14);
            this.lblConsignmentItemsHeading.Text = "Consignments:";
           
            // 
            // lblConsignmentItems
            // 
            this.lblConsignmentItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsignmentItems.Location = new System.Drawing.Point(170, 368);
            this.lblConsignmentItems.Name = "lblConsignmentItems";
            this.lblConsignmentItems.Size = new System.Drawing.Size(20, 15);
            this.lblConsignmentItems.Text = "Abc";
           

            // 
            // lblConsigneeAdrHeading
            // 
            this.lblConsigneeAdrHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeAdrHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblConsigneeAdrHeading.Location = new System.Drawing.Point(3, 398);
            this.lblConsigneeAdrHeading.Name = "lblConsigneeAdrHeading";
            this.lblConsigneeAdrHeading.Size = new System.Drawing.Size(82, 14);
            this.lblConsigneeAdrHeading.Text = "Consignee adr:";
           
            
            // 
            // lblConsigneeAdr
            // 
            this.lblConsigneeAdr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblConsigneeAdr.Location = new System.Drawing.Point(170, 397);
            this.lblConsigneeAdr.Name = "lblConsigneeAdr";
            this.lblConsigneeAdr.Size = new System.Drawing.Size(20, 15);
            this.lblConsigneeAdr.Text = "Abc";
            
            


            // 
            // FormLoadList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormLoadList";
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSortZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeAdrHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeHead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentItemsHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlDeliveries)).EndInit();
            this.tabControlDeliveries.ResumeLayout(false);
            this.tabNotStarted.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        

        

        
        

        #endregion
        private Resco.Controls.CommonControls.TabControl tabControlDeliveries;
        private Resco.Controls.CommonControls.TabPage tabNotStarted;
        private Resco.Controls.CommonControls.TabPage tabCompleted;
        private Resco.Controls.CommonControls.TabPage tabAll;

        private System.Windows.Forms.ImageList imageListOperationListIcons;
       
        private Resco.Controls.OutlookControls.ImageButton btnSortZone;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.OutlookControls.ImageButton btnZone1;
        private Resco.Controls.OutlookControls.ImageButton btnZone2;
        private Resco.Controls.OutlookControls.ImageButton btnZone3;
        private Resco.Controls.OutlookControls.ImageButton btnZone4;
        private Resco.Controls.OutlookControls.ImageButton btnZone5;
        private Resco.Controls.OutlookControls.ImageButton btnZone6;
        private Resco.Controls.OutlookControls.ImageButton btnZone7;
        private Resco.Controls.OutlookControls.ImageButton btnZone8;
        


        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listDeliveries;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignee;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItems;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeHead;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentItemsHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeAdr;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeAdrHeading;
        
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelHeading;
        private Resco.Controls.CommonControls.TransparentLabel labelMessage;


        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell TextCellHeaderTemplate;
        
        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;

        private Resco.Controls.AdvancedList.RowTemplate RowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.ImageCell ImageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;

        private Panel pnlMessage;
        private PictureBox picMessage;
        private Label lblInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnZone1Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone2Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone3Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone4Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone5Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone6Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone7Details;
        private Resco.Controls.OutlookControls.ImageButton btnZone8Details;
        private Resco.Controls.OutlookControls.ImageButton btnCancelZoneItemSort;


       




    }
}
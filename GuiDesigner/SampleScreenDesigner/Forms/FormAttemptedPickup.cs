﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    public partial class FormAttemptedPickup : Form
    {

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public FormAttemptedPickup()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
        }

        
        private void ButtonOkClick(object sender, EventArgs e)
        {
        }


        

        private void ButtonCancelClick(object sender, EventArgs e)
        {

        }

        private void OnCauseChanged(object sender, EventArgs e)
        {

        }
        
    }



}
﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Setting.Views
{
    public partial class FormSetKeyLock : Form
    {
        public FormSetKeyLock()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            TranslateUi();
        }

        private void TranslateUi()
        {
            labelModuleName.Text = GlobalTexts.AutomaticKeyLock;
            CheckBoxKeyLock.Text = GlobalTexts.ActivateAutomaticKeyLock;
        }

        private void CheckBoxKeyLock_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}
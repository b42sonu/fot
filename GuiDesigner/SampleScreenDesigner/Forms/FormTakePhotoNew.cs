﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using SampleScreenDesigner.Controls;
using System.IO;
using System.Reflection;
using System.Drawing;

namespace SampleScreenDesigner.Forms
{
    public partial class FormTakePhotoNew : Form
    {
        #region "Variables/objects adn constrctor"

        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private TabMultipleButtons _tabButtons;
        private int _capturePhotoCount;
        private int _previousPhotoCount;
        //private FormDataCamera _formData;
        string _folderName;
        public FormTakePhotoNew()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
                touchPanelPhotos.BackgroundImage = image;
            }
            SetTextToGui();

            //string path = @"\My Documents\thumb\20121220-165513.jpg";
           // AddImage(@"\My Documents\thumb\Image_00062.jpg");
           // AddImage(@"\My Documents\thumb\Image_00061.jpg");
            
        }
        private void SetTextToGui()
        {
            _tabButtons = new TabMultipleButtons();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.OkEnter, ButtonConfirmClick, TabButtonOkName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            lableThumbnail.Text = GlobalTexts.LabelThumbnail;
            lableCameraInstruction.Text = "Remember! Use Enter button on PDA\r\n to capture picture";
            lableDamageFor.Text = GlobalTexts.DamageFor;

            
            labelModuleName.Text = GlobalTexts.DamageRegistration;
            btnDiscard.Text = GlobalTexts.Discard;
            btnSave.Text = GlobalTexts.Save;



            lableDeleteInstruction.Text = "Delete picture?Click on thumbnail";
        }
        #endregion

        #region "Events"
        private void ButtonBackClick(object sender, EventArgs e)
        {
            /* try
             {
                 Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method ButtonBackClick");
                 SendBackPannel();
                 bool processCancel = true;
                 if (_capturePhotoCount > 0)
                 {
                     var result = GuiCommon.ShowModalDialog(GlobalTexts.Attention, GlobalTexts.DeleteCapturePhotos, Severity.Info, GlobalTexts.Yes, GlobalTexts.No);
                     if (result.Button.Text == GlobalTexts.No)
                         processCancel = false;
                 }

                 ViewEvent.Invoke(TakePhotoViewEvent.Cancel, _folderName, _capturePhotoCount > _previousPhotoCount, processCancel);
             }
             catch (Exception ex)
             {
                 Logger.LogException(ex, "ButtonConfirmClick");
             }*/

        }
        private void ButtonCameraClick(object sender, EventArgs e)
        {
            /*try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method ButtonCameraClick");

                if (_folderName != null)
                {
                    _formData.PhotoPath = string.Empty;
                    _formData.ErrorOccured = false;

                    string directoryPath = CreateDirectory(true);

                    if (_formData.SettingPhotoCount > _capturePhotoCount)
                    {
                        ViewEvent.Invoke(TakePhotoViewEvent.Camera, _formData.CameraResoluion, directoryPath);

                        if (!_formData.ErrorOccured && !string.IsNullOrEmpty(_formData.PhotoPath))
                        {
                            if (_formData.KeepPicture)
                            {
                                if (_folderName != null)
                                {
                                    string thumbFolderName = CreateDirectory(false);
                                    //DisableOnScreenKeyboard();
                                    ViewEvent.Invoke(TakePhotoViewEvent.Save, thumbFolderName, _folderName);
                                    //EnableOnScreenKeyboard();
                                }

                                if (_formData != null && !_formData.ErrorOccured)
                                {
                                    AddImage(_formData.PhotoPath);
                                    SendBackPannel();
                                    _capturePhotoCount++;
                                }
                            }
                            else
                            {
                                SendBackPannel();
                            }
                            _tabButtons.Enable(TabButtonOkName, _capturePhotoCount > 0);
                        }
                    }
                    else
                    {
                        GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.PhotoLimitOver, Severity.Warning, GlobalTexts.Ok);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonCameraClick");
            }
            //if (PreCom.Application.Instance.State == PreCom.ApplicationState.Activated)
            //    PreCom.Application.Instance.State = PreCom.ApplicationState.Deactivated;*/
        }
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            /*try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method ButtonConfirmClick");
                SendBackPannel();
                ViewEvent.Invoke(TakePhotoViewEvent.Ok, _folderName, _capturePhotoCount > _previousPhotoCount);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonConfirmClick");
            }
            //if (PreCom.Application.Instance.State == PreCom.ApplicationState.Activated)
            //    PreCom.Application.Instance.State = PreCom.ApplicationState.Deactivated;*/
        }

        private void BtnDiscardClick(object sender, EventArgs e)
        {
            /*try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method BtnDiscardClick");
                SendBackPannel();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonConfirmClick");
            }*/
            string imagePath = Convert.ToString(pictureBoxPopUp.Tag);
            if (File.Exists(imagePath))
            {
                File.Delete(imagePath);
                if (_pictureBoxSelected != null)
                {
                    touchPanelPhotos.Controls.Remove(_pictureBoxSelected);
                    _pictureBoxSelected = null;
                }
            }

            SendBackPannel();

        }

        private void BtnSaveClick(object sender, EventArgs e)
        {
            /* try
             {
                 Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method BtnSaveClick");

                 if (_folderName != null)
                 {
                     string thumbFolderName = CreateDirectory(false);
                     DisableOnScreenKeyboard();
                     ViewEvent.Invoke(TakePhotoViewEvent.Save, thumbFolderName, _folderName);
                     EnableOnScreenKeyboard();
                 }

                 if (_formData != null && !_formData.ErrorOccured)
                 {
                     AddImage(_formData.PhotoPath);
                     SendBackPannel();
                     _capturePhotoCount++;
                 }
                 _tabButtons.Enable(TabButtonOkName, _capturePhotoCount > 0);

             }
             catch (Exception ex)
             {
                 Logger.LogException(ex, "BtnSaveClick");
             }*/


        }
        /// <summary>
        /// method for create directory for captured phot and thumnail photos
        /// </summary>
        /// <param name="isReturnMainDirectorPath"></param>
        /// <returns></returns>
        private string CreateDirectory(bool isReturnMainDirectorPath)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase), "Photo");
            string directoryPath = string.Format("{0}\\{1}", path, _folderName); //@"\My Documents\thumb";

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            string thumbFolderName = directoryPath + "\\thumb\\";

            if (!Directory.Exists(thumbFolderName))
            {
                Directory.CreateDirectory(thumbFolderName);
            }

            if (isReturnMainDirectorPath)
                return directoryPath;
            else
                return thumbFolderName;
        }

        #endregion

        #region "Methods"
        private void SendBackPannel()
        {
            panelPopUp.Visible = false;
            panelPopUp.SendToBack();
            _tabButtons.Enable(TabButtonOkName, _capturePhotoCount > 0);
            _tabButtons.Enable(TabButtonBackName, true);
            //pictureBoxPopUp.CompactImage = null;
            pictureBoxPopUp.Image = null;
        }
        private void BringToFrontPannel()
        {
            panelPopUp.Visible = true;
            panelPopUp.BringToFront();
        }
        private void AddImage(string imagePath)
        {
            if (File.Exists(imagePath))
            {
                Image captureImage = new Bitmap(imagePath);
                int quotient = touchPanelPhotos.Controls.Count / 3;
                int remainder = touchPanelPhotos.Controls.Count % 3;

                var pointImageBox = new Point { X = remainder == 0 ? 8 : 146 * remainder, Y = quotient == 0 ? 4 : 105 * quotient };

                var imageBox = new PictureBox
                {
                    Location = pointImageBox,
                    Name = "imageBox1",
                    Size = new Size(128, 95),
                    TabIndex = 0,
                    Image = captureImage,
                    Tag = imagePath,
                    SizeMode = PictureBoxSizeMode.StretchImage,

                };
                imageBox.Click += ImageBoxClick;
                touchPanelPhotos.Controls.Add(imageBox);
            }
        }

        private PictureBox _pictureBoxSelected;
        void ImageBoxClick(object sender, EventArgs e)
        {
            if (sender != null)
            {
                _pictureBoxSelected = ((PictureBox)sender);
                pictureBoxPopUp.Image = ((PictureBox)sender).Image;
                pictureBoxPopUp.Tag = ((PictureBox)sender).Tag;
                BringToFrontPannel();
            }
        }



        /*public override void OnShow(IFormData formData)
        {
            ClearControls();
            _capturePhotoCount = 0;
            _previousPhotoCount = 0;
            //_folderName = Convert.ToString(Guid.NewGuid());
            //if (PreCom.Application.Instance.State == PreCom.ApplicationState.Activated)
            //    PreCom.Application.Instance.State = PreCom.ApplicationState.Deactivated;

            _formData = (FormDataCamera)formData;
            labelModuleName.Text = _formData.HeaderText;

            if (_formData != null)
            {
                btnCamera.Enabled = _formData.IsCameraAvailable;
                //lableItemDetail.Text = _formData.PlannedConsignmentItem + " " + _formData.ConsignmentItemNumber;
                lableDamageFor.Text = _formData.FunctionalityFor + " for " + _formData.ConsignmentItemType + " " + _formData.ConsignmentItemNumber;

                if (!string.IsNullOrEmpty(_formData.FolderName))
                {
                    _folderName = _formData.FolderName;
                    ShowAlreadyCaptureImgaes(_formData.FolderName);

                }
                else
                {
                    _folderName = Convert.ToString(Guid.NewGuid());
                }

            }
            else
            {
                _folderName = Convert.ToString(Guid.NewGuid());
            }
            _previousPhotoCount = _capturePhotoCount;
            _tabButtons.Enable(TabButtonOkName, _capturePhotoCount > 0);

        }*/
        /// <summary>
        /// 
        /// </summary>
        private void ClearControls()
        {
            touchPanelPhotos.Controls.Clear();
        }

        /* public override void OnUpdateView(IFormData iformData)
         {
             _formData = (FormDataCamera)iformData;
             if (_formData != null)
             {
                 if (_formData.ErrorOccured)
                     GuiCommon.ShowModalDialog(GlobalTexts.Error, _formData.ErrorMessage, Severity.Error, GlobalTexts.Ok);
             }
         }*/


        #endregion
        /// <summary>
        /// show already captured images if folder name provide by previous process ..
        /// and it means same process for same consignment and consignment item...
        /// </summary>
        /// <param name="folderName"></param>
        private void ShowAlreadyCaptureImgaes(string folderName)
        {
            string directoryPath =
                Path.Combine(Path.Combine(
                    Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase), "Photo"),
                    folderName), "thumb");
            if (Directory.Exists(directoryPath))
            {
                string[] photosPaths = Directory.GetFiles(directoryPath, "*.jpg");

                if (photosPaths.Length > 0)
                {
                    foreach (var photoPath in photosPaths)
                    {
                        AddImage(photoPath);
                        _capturePhotoCount++;
                    }
                }
            }
        }

    }





}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace SampleScreenDesigner.Forms
{
    public partial class FormDeliveryInfo : Form
    {
        private PlannedConsignmentsType _selectedConsignment;
        public FormDeliveryInfo()
        {
            InitializeComponent();
            BindOperations();
            ShowDetails();
        }

        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            switch (e.DataRow.CurrentTemplateIndex)
            {
                case 1: // The master row has to be collapsed
                    break;
                case 2: // The master row has to be expanded  
                    UnSelectOtherRows();
                    e.DataRow.Selected = true;
                    break;
            }
        }

        private void UnSelectOtherRows()
        {
            for (var i = 0; i <= listConsignmentItems.DataRows.Count - 1;i++ )
            {
                var row = listConsignmentItems.DataRows[i];
                row.Selected = false;
            }
        }

      

        private void BindOperations()
        {
            listConsignmentItems.BeginUpdate();
            _selectedConsignment = GetMockedConsignment();
            if (_selectedConsignment == null || _selectedConsignment.ConsignmentItem == null) return;
            listConsignmentItems.DataSource = _selectedConsignment.ConsignmentItem;
            listConsignmentItems.EndUpdate();
        }

        private void ShowDetails()
        {
            lblConsigneeDetails.Text = GetConsigneeDetails(_selectedConsignment);
            lblConsignorDetails.Text = GetConsignorDetails(_selectedConsignment);

        }

        private string GetConsigneeDetails(PlannedConsignmentsType consignment)
        {
            var details = new StringBuilder();
            
            var consignee = consignment.Consignee;
            if (consignee == null) return details.ToString();

            //Append name
            if (!string.IsNullOrEmpty(consignee.Name1))
                details.Append(consignee.Name1);

            //Append address if not null
            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1))
                details.Append(Environment.NewLine + consignment.Consignee.DeliveryAddress1);

            //Append Postal code if not null
            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalCode))
            {
                //If delivery address is not null, then append comma
                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1))
                    details.Append(",");
                //Append postal code
                details.Append(consignment.Consignee.DeliveryPostalCode);
            }

            //Append postal name if not null
            if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
            {
                if (!string.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1) || !string.IsNullOrEmpty(consignment.Consignee.DeliveryPostalCode))
                    details.Append(",");
                details.Append(consignment.Consignee.DeliveryPostalName);
            }
                
            
            return details.ToString();
            
        }


        private string GetConsignorDetails(PlannedConsignmentsType consignment)
        {
            var details = new StringBuilder();

            var consignor = consignment.Consignor;
            if (consignor == null) return details.ToString();

            //Append name
            if (!string.IsNullOrEmpty(consignor.Name1))
                details.Append(consignor.Name1);

            //Append address if not null
            if (!string.IsNullOrEmpty(consignment.Consignor.Address1))
                details.Append(Environment.NewLine + consignment.Consignor.Address1);

            //Append Postal code if not null
            if (!string.IsNullOrEmpty(consignment.Consignor.PostalCode))
            {
                //If delivery address is not null, then append comma
                if (!string.IsNullOrEmpty(consignment.Consignor.Address1))
                    details.Append(",");
                //Append postal code
                details.Append(consignment.Consignor.PostalCode);
            }

            //Append postal name if not null
            if (!string.IsNullOrEmpty(consignment.Consignor.PostalName))
            {
                if (!string.IsNullOrEmpty(consignment.Consignor.Address1) || !string.IsNullOrEmpty(consignment.Consignor.PostalCode))
                    details.Append(",");
                details.Append(consignment.Consignor.PostalName);
            }

            return details.ToString();
        }



        private PlannedConsignmentsType GetMockedConsignment()
        {
            var consignment1 = new PlannedConsignmentsType();
            consignment1.ConsignmentNumber = "70713750024118488";
            consignment1.ConsignmentItemCount = 2;


            var consignee1 = new ConsigneeType();
            consignee1.Name1 = "Rakesh Aggarwal";
            consignee1.DeliveryAddress1 = "#1462, Sector 39/b";
            consignee1.DeliveryPostalCode = "1234";
            consignee1.DeliveryPostalName = "ABC Postal";

            var consignor1 = new ConsignorType();

            consignor1.Name1 = "Aggarwal Rakesh";
            consignor1.Address1 = "#1462, Sector b/39";
            consignor1.PostalCode = "4321";
            consignor1.PostalName = "CBA Postal";

            consignment1.Consignee = consignee1;
            consignment1.Consignor = consignor1;


            var consignmentItem1 = new ConsignmentItemType();
            consignmentItem1.ConsignmentItemNumber = "370713750043854986";

            var consignmentItem2 = new ConsignmentItemType();
            consignmentItem2.ConsignmentItemNumber = "370713750043854993";

            var consignmentItem3 = new ConsignmentItemType();
            consignmentItem3.ConsignmentItemNumber = "370713750043851234";

            var consignmentItem4 = new ConsignmentItemType();
            consignmentItem4.ConsignmentItemNumber = "370713750043855678";

            consignment1.ConsignmentItem = new[] { consignmentItem1, consignmentItem2, consignmentItem3, consignmentItem4 };

            return consignment1;
        }
    }
}
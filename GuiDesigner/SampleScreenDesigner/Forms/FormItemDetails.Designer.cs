﻿using Resco.Controls.SmartGrid;

namespace SampleScreenDesigner.Forms
{
    partial class FormItemDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblReceipientValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPckValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPck = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAddress = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReceipient = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAddressValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblTown = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblTownValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblInfo = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDelete = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipientValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPckValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTownValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Controls.Add(this.buttonBack);
            this.touchPanel.Controls.Add(this.btnDelete);
            this.touchPanel.Controls.Add(this.lblInfo);
            this.touchPanel.Controls.Add(this.lblTownValue);
            this.touchPanel.Controls.Add(this.lblTown);
            this.touchPanel.Controls.Add(this.lblAddressValue);
            this.touchPanel.Controls.Add(this.lblReceipientValue);
            this.touchPanel.Controls.Add(this.lblPckValue);
            this.touchPanel.Controls.Add(this.lblPck);
            this.touchPanel.Controls.Add(this.lblAddress);
            this.touchPanel.Controls.Add(this.lblReceipient);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.GotFocus += new System.EventHandler(this.touchPanel_GotFocus);
            // 
            // lblReceipientValue
            // 
            this.lblReceipientValue.AutoSize = false;
            this.lblReceipientValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblReceipientValue.Location = new System.Drawing.Point(122, 88);
            this.lblReceipientValue.Name = "lblReceipientValue";
            this.lblReceipientValue.Size = new System.Drawing.Size(355, 24);
            // 
            // lblPckValue
            // 
            this.lblPckValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblPckValue.Location = new System.Drawing.Point(122, 51);
            this.lblPckValue.Name = "lblPckValue";
            this.lblPckValue.Size = new System.Drawing.Size(0, 0);
            // 
            // lblPck
            // 
            this.lblPck.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblPck.Location = new System.Drawing.Point(22, 51);
            this.lblPck.Name = "lblPck";
            this.lblPck.Size = new System.Drawing.Size(46, 24);
            this.lblPck.Text = "Pck:";
            // 
            // lblAddress
            // 
            this.lblAddress.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblAddress.Location = new System.Drawing.Point(22, 125);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(85, 24);
            this.lblAddress.Text = "Address:";
            // 
            // lblReceipient
            // 
            this.lblReceipient.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblReceipient.Location = new System.Drawing.Point(22, 88);
            this.lblReceipient.Name = "lblReceipient";
            this.lblReceipient.Size = new System.Drawing.Size(78, 24);
            this.lblReceipient.Text = "Receip:";
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(69, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(350, 27);
            this.labelModuleName.Text = "Delivered To Another Location";
            // 
            // lblAddressValue
            // 
            this.lblAddressValue.AutoSize = false;
            this.lblAddressValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblAddressValue.Location = new System.Drawing.Point(122, 125);
            this.lblAddressValue.Name = "lblAddressValue";
            this.lblAddressValue.Size = new System.Drawing.Size(355, 24);
            // 
            // lblTown
            // 
            this.lblTown.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblTown.Location = new System.Drawing.Point(22, 167);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(59, 24);
            this.lblTown.Text = "Town:";
            // 
            // lblTownValue
            // 
            this.lblTownValue.AutoSize = false;
            this.lblTownValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblTownValue.Location = new System.Drawing.Point(98, 167);
            this.lblTownValue.Name = "lblTownValue";
            this.lblTownValue.Size = new System.Drawing.Size(355, 24);
            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblInfo.Location = new System.Drawing.Point(22, 206);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(42, 24);
            this.lblInfo.Text = "Info:";
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(322, 485);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(158, 50);
            this.buttonOk.TabIndex = 50;
            this.buttonOk.Text = "Ok";
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(0, 485);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(158, 50);
            this.buttonBack.TabIndex = 49;
            this.buttonBack.Text = "Back";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(161, 485);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(158, 50);
            this.btnDelete.TabIndex = 48;
            this.btnDelete.Text = "Delete";
            // 
            // FormItemDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormItemDetails";
            this.Text = "FormAttemptedDeliveryList";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipientValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPckValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTownValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblReceipient;
        private Resco.Controls.CommonControls.TransparentLabel lblAddress;
        private Resco.Controls.CommonControls.TransparentLabel lblPck;
        private Resco.Controls.CommonControls.TransparentLabel lblReceipientValue;
        private Resco.Controls.CommonControls.TransparentLabel lblPckValue;
        private Resco.Controls.CommonControls.TransparentLabel lblAddressValue;
        private Resco.Controls.CommonControls.TransparentLabel lblInfo;
        private Resco.Controls.CommonControls.TransparentLabel lblTownValue;
        private Resco.Controls.CommonControls.TransparentLabel lblTown;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
        private Resco.Controls.OutlookControls.ImageButton btnDelete;
    }
}
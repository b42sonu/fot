﻿using System;
using System.Drawing;
using SampleScreenDesigner.Properties;

namespace Com.Bring.PMP.PreComFW.Shared.Utility
{
    public class GuiCommon
    {
        private static GuiCommon _instance;

        public static GuiCommon Instance
        {
            get { return _instance ?? (_instance = new GuiCommon()); }
        }

        private Image _imageBackground;
        public Image BackgroundImage
        {
            get { return _imageBackground ?? (_imageBackground = InitBackgroundImage()); }
        }

        private Image InitBackgroundImage()
        {
            try
            {
                return Resources.MC65_Background;
            }

            catch (Exception)
            {
                return null;
            }
        }

        private Image _okImage;
        public Image ImageForOk
        {
            get { return _okImage ?? (_okImage = InitOKImage()); }
        }

        private Image InitOKImage()
        {
            try
            {
                return Resources.FOT_icon_verified_ok;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Image LoadingIcon()
        {
            try
            {
                return Resources.Loading;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Complete()
        {
            try
            {
                return Resources.Completed;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Uncomplete()
        {
            try
            {
                return Resources.Uncomplete;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Pickup()
        {
            try
            {
                return Resources.PickUp;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image PlusLarge()
        {
            try
            {
                return Resources.PlusLarge;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image MinusLarge()
        {
            try
            {
                return Resources.MinusLarge;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Unloading()
        {
            try
            {
                return Resources.Unloading;
            }

            catch (Exception)
            {
                return null;
            }
        }
        public Image ReturnedDelivery()
        {
            try
            {
                return Resources.Unloading;
            }

            catch (Exception)
            {
                return null;
            }
        }
        public Image Delivery()
        {
            try
            {
                return Resources.Delivery;
            }

            catch (Exception)
            {
                return null;
            }
        }


        public Image Finished()
        {
            try
            {
                return Resources.Finished;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image New()
        {
            try
            {
                return Resources.New;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Started()
        {
            try
            {
                return Resources.Started;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Attention()
        {
            try
            {
                return Resources.FOT_icon_attention;
            }

            catch (Exception)
            {
                return null;
            }
        }

        public Image Loading()
        {
            try
            {
                return Resources.FOT_icon_attention;
            }

            catch (Exception)
            {
                return null;
            }
        }
    }
}

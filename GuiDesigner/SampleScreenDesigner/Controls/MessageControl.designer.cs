﻿namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    partial class MessageControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support  do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxIcon = new System.Windows.Forms.PictureBox();
            this.labelMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.panelPicture = new System.Windows.Forms.Panel();
            this.panelText = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.labelMessage)).BeginInit();
            this.panelPicture.SuspendLayout();
            this.panelText.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxIcon
            // 
            this.pictureBoxIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxIcon.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new System.Drawing.Size(104, 141);
            this.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = false;
            this.labelMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMessage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelMessage.Location = new System.Drawing.Point(0, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(332, 141);
            this.labelMessage.Text = "transparentLabel1";
            this.labelMessage.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleLeft;
            // 
            // panelPicture
            // 
            this.panelPicture.Controls.Add(this.pictureBoxIcon);
            this.panelPicture.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelPicture.Location = new System.Drawing.Point(0, 0);
            this.panelPicture.Name = "panelPicture";
            this.panelPicture.Size = new System.Drawing.Size(104, 141);
            // 
            // panelText
            // 
            this.panelText.Controls.Add(this.labelMessage);
            this.panelText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelText.Location = new System.Drawing.Point(104, 0);
            this.panelText.Name = "panelText";
            this.panelText.Size = new System.Drawing.Size(332, 141);
            // 
            // MessageControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panelText);
            this.Controls.Add(this.panelPicture);
            this.Name = "MessageControl";
            this.Size = new System.Drawing.Size(436, 141);
            ((System.ComponentModel.ISupportInitialize)(this.labelMessage)).EndInit();
            this.panelPicture.ResumeLayout(false);
            this.panelText.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxIcon;
        private Resco.Controls.CommonControls.TransparentLabel labelMessage;
        private System.Windows.Forms.Panel panelPicture;
        private System.Windows.Forms.Panel panelText;
    }
}


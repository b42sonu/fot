﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace SampleScreenDesigner.Controls
{
    public partial class TabMultipleButtons : UserControl
    {

        #region Variables and Objects
        public string NextButtonText = GlobalTexts.Next;
        public string PreviousButtonText = GlobalTexts.Previous;
        public int NextButtonIndex = 5;
        public int PreviousButtonIndex = 0;
        public int ButtonSpace = ScaleUtil.GetScaledPosition(2);
        public Size ButtonSize = ScaleUtil.GetScaledSize(157, 50);
        public int CurrentPage = 1;
        public int PageCount = 1;
        private List<TabButton> _listActiveButtons;
        public List<TabButton> ListButtons = new List<TabButton>();
        private readonly List<TabButton> _removedButtons = new List<TabButton>();
        private TabButton _buttonPrevious;
        private TabButton _buttonNext;
        private int _nextToAdd;
        private int _fixedButtonCount;
        private int _buttonsPerRow = 3;
        #endregion

        #region Constructor

        public TabMultipleButtons()
        {
            InitializeComponent();
            panelButtonContainer.BackColor = SystemColors.InactiveCaption;
            //set properties of back button
            _buttonPrevious = new TabButton();
            _buttonPrevious.Click += PreviousButtonClick;
            _buttonPrevious.IsPositionFixed = true;
            _buttonPrevious.BackColor = Color.FromArgb(46, 48, 50);
            _buttonPrevious.Font = new Font("Arial", 9F, FontStyle.Regular);
            _buttonPrevious.ForeColor = Color.White;
            ResizePanel(true);
            Top = ScaleUtil.GetScaledPosition(Top);
            if (ScaleUtil.IsHighDensityDevice) Top += 15;

            Width = ScaleUtil.GetScaledPosition(Width);
            Height = ScaleUtil.GetScaledPosition(Height);

        }

        #endregion

        #region Methods

        /// <summary>
        /// set default properties to ListActiveButtons
        /// </summary>
        private void SetDefaultProperties()
        {
            foreach (TabButton btn in _listActiveButtons)
            {
                //set default properties if UseDefaultProperties is true
                if (btn.UseDefaultProperties)
                {
                    btn.Size = ButtonSize;
                    if (btn.Text.Length < 16)//if text is not too long
                    {
                        SetDefaultProperties(btn);
                    }
                    else
                    {
                        //adjust font if text is too long
                        if (!btn.Text.Contains(Environment.NewLine))
                        { AdjustFont(btn); }
                    }
                }
            }
        }
        void SetDefaultProperties(TabButton btn)
        {
            btn.BackColor = Color.FromArgb(46, 48, 50);
            btn.Font = new Font("Arial", 9F, FontStyle.Regular);
            btn.ForeColor = Color.White;
        }

        private string[] _words;
        void AdjustFont(TabButton btn)
        {
            string text = btn.Text;

            btn.BackColor = Color.FromArgb(46, 48, 50);
            btn.Font = new Font("Arial", 8F, FontStyle.Regular);
            btn.ForeColor = Color.White;
            if (text.Length <= 15)
                return;

            string newText = string.Empty;
            _words = text.Split(' ');
            for (int i = 0; i < _words.Length; i++)
            {
                newText += _words[i];
                int len = newText.Length + _words[i].Length;
                if (len > 15 || len - NextLine(i + 1).Length <= 8)
                {
                    newText += Environment.NewLine + NextLine(i + 1);
                    break;
                }
            }
            newText.Replace('_', ' ');
            btn.Text = newText;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        string NextLine(int index)
        {
            string nextLine = string.Empty;
            for (; index < _words.Length; index++)
            {
                nextLine += _words[index] + ' ';
            }
            if (!string.IsNullOrEmpty(nextLine) && nextLine[nextLine.Length - 1] == ' ')
                nextLine = nextLine.Substring(0, nextLine.Length - 1);
            return nextLine;
        }
        /// <summary>
        /// set positions to ListActiveButtons
        /// </summary>
        private void SetLocations()
        {
            int left = 0;
            int top = ScaleUtil.GetScaledPosition(_listActiveButtons.Count <= 3 ? 0 : 52);
            int btnIndex = 1;
            var loc = new Point(left, top);
            //position the buttons to appropriate locations
            foreach (TabButton btn in _listActiveButtons)
            {

                if (!btn.IsPositionSet)
                {
                    loc = new Point(left, top);
                    btn.Location = loc;
                    left += btn.Width + ButtonSpace;
                    if (btnIndex % _buttonsPerRow == 0)
                    {
                        top -= (btn.Height + ButtonSpace);
                        left = 0;
                    }
                    if (btnIndex % (_buttonsPerRow * 2) == 0)
                    {
                        return;
                    }
                }
                if (btn.Location == loc)
                    btnIndex++;
            }
        }

        /// <summary>
        /// add ListActiveButtons to panel
        /// </summary>
        private void AddButtonsToPanel()
        {
            //add buttons in _listActiveButtons to panel
            for (int i = 0; i < _nextToAdd && i < _listActiveButtons.Count; i++)
            {
                TabButton tabButton = _listActiveButtons[i];
                panelButtonContainer.Controls.Add(tabButton);
                tabButton.IsPositionSet = true;
            }
            //remove the buttons that have been added to panel from _listActiveButtons
            for (int i = 0; i < _nextToAdd; i++)
            {
                if (_listActiveButtons.Count > 0)
                    _listActiveButtons.RemoveAt(0);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void GenerateButtons()
        {
            //define how many buttons to add for first time
            _nextToAdd = _buttonsPerRow * 2;
            SetButtons();
        }

        TabButton CreateEmptyButton()
        {
            var btn = new TabButton("", null)
            {
                Enabled = false,
                BackColor = Color.FromArgb(40, 42, 43),
                ForeColor = Color.FromArgb(19, 20, 20),
                Size = ButtonSize,
                UseDefaultProperties = false
            };
            return btn;
        }

        void SetCancelConfirmButton()
        {
            int btnCount = (from n in _listActiveButtons where n.ButtonType == TabButtonType.Cancel select n).Count();
            if (btnCount > 1)
            {
                throw new Exception("More than one ListActiveButtons added with Type.Canell");
            }

            btnCount = (from n in _listActiveButtons where n.ButtonType == TabButtonType.Confirm select n).Count();
            if (btnCount > 1)
            {
                throw new Exception("More than one ListActiveButtons added with Type.Confirm");
            }

            TabButton button = (from n in _listActiveButtons where n.ButtonType == TabButtonType.Cancel select n).SingleOrDefault();
            if (button != null)
            {
                _listActiveButtons.Remove(button);
                _listActiveButtons.Insert(PreviousButtonIndex, button);
                //if (PreviousButtonIndex == 0)
                //{ PreviousButtonIndex = 1; }
            }
            button = (from n in _listActiveButtons where n.ButtonType == TabButtonType.Confirm select n).SingleOrDefault();
            if (button != null)
            {
                if (_listActiveButtons.Count > 2)
                {
                    _listActiveButtons.Remove(button);
                    _listActiveButtons.Insert(2, button);
                }
                else
                {
                    _listActiveButtons.Remove(button);
                    _listActiveButtons.Add(button);
                }
            }
        }

        public void Clear()
        {
            //clear everylist and panel
            if (ListButtons != null)
                ListButtons.Clear();
            if (_removedButtons != null)
                _removedButtons.Clear();
            if (_listActiveButtons != null)
                _listActiveButtons.Clear();
            if (panelButtonContainer != null)
                panelButtonContainer.Controls.Clear();
        }
        public void RefreshTab()
        {
            _nextToAdd = _buttonsPerRow * 2;
            _fixedButtonCount = 0;
            //set each button's IsPositionSet to false to reset the  position
            foreach (TabButton btn in ListButtons)
            {
                btn.IsPositionSet = false;
            }
            SetButtons();
            //go to the current page
            for (int i = 1; i < CurrentPage; i++)
            {
                Next();
            }
        }
        void SetButtons()
        {
            if (_removedButtons != null)
                _removedButtons.Clear();
            if (_listActiveButtons != null)
                _listActiveButtons.Clear();
            panelButtonContainer.Controls.Clear();
            _buttonNext = null;
            _buttonPrevious = null;
            GC.Collect();
            //get buttons from ListButtons that are visible
            _listActiveButtons = (from n in ListButtons where n.Visible select n).ToList();
            //get the number of buttons that have IsPositionFixed set to true 
            _fixedButtonCount = (from n in ListButtons where n.IsPositionFixed select n).Count();
            //calculate total number of pages
            PageCount = (_listActiveButtons.Count) / (_buttonsPerRow * 2);
            if (((_listActiveButtons.Count) % (_buttonsPerRow * 2)) > 0)
                PageCount++;

            switch (_listActiveButtons.Count)
            {
                case 0:
                    return;
                case 1:
                    {
                        ResizePanel(true);
                        if (_listActiveButtons[0].ButtonType == TabButtonType.Confirm)
                        {
                            _listActiveButtons.Insert(0, CreateEmptyButton());
                            _listActiveButtons.Insert(0, CreateEmptyButton());
                        }
                        else
                        {
                            _listActiveButtons.Add(CreateEmptyButton());
                            _listActiveButtons.Add(CreateEmptyButton());
                        }
                        SetDefaultProperties();
                        SetLocations();
                        AddButtonsToPanel();
                        return;
                    }

                case 2:
                    {

                        ResizePanel(true);
                        SetCancelConfirmButton();
                        _listActiveButtons.Insert(1, CreateEmptyButton());
                        SetDefaultProperties();
                        SetLocations();
                        AddButtonsToPanel();
                        return;

                    }
                case 3:
                    {
                        ResizePanel(true);
                        break;
                    }
                default:
                    {
                        int count = _listActiveButtons.Count + _fixedButtonCount;
                        TabButtonType type = _listActiveButtons[_listActiveButtons.Count - 1].ButtonType;
                        if (count % 4 == 0)
                        {

                            //if cancel or confirm button is added at last
                            if (type == TabButtonType.Cancel || type == TabButtonType.Confirm)
                            { SetCancelConfirmButton(); }
                            _listActiveButtons.Insert(count - 1, CreateEmptyButton());
                            _listActiveButtons.Insert(count - 1, CreateEmptyButton());
                        }
                        else if (count % 4 == 1)
                        {
                            //if cancel or confirm button is added at last
                            if (type == TabButtonType.Cancel || type == TabButtonType.Confirm)
                            { SetCancelConfirmButton(); }
                            _listActiveButtons.Insert(count - 1, CreateEmptyButton());
                        }
                        ResizePanel(false);
                        break;
                    }
            }
            SetCancelConfirmButton();
            if (_listActiveButtons.Count > 6)
            {
                _buttonNext = new TabButton { Size = _listActiveButtons[0].Size, Text = NextButtonText, IsPositionFixed = true, IsPositionSet = false };
                _buttonNext.Click += NextButtonClick;
                _listActiveButtons.Insert(NextButtonIndex, _buttonNext);

                _buttonPrevious = new TabButton
                {
                    Size = _listActiveButtons[0].Size,
                    Text = PreviousButtonText,
                    IsPositionFixed = true,
                    IsPositionSet = false
                };
                _buttonPrevious.Click += PreviousButtonClick;
            }
            _fixedButtonCount = (from n in _listActiveButtons where n.IsPositionFixed select n).Count();
            if (_listActiveButtons.Count > 6)
                _listActiveButtons.Insert(PreviousButtonIndex + 6, _buttonPrevious);
            SetDefaultProperties();
            SetLocations();
            AddButtonsToPanel();
        }
        public void Next()
        {
            _nextToAdd = 0;
            _buttonPrevious.IsPositionFixed = true;
            bool isLastPage = false;
            if (_listActiveButtons.Count <= 6 - (_fixedButtonCount - 1))
            {
                _buttonNext.IsPositionFixed = false;
                _removedButtons.Add(_buttonNext);
                panelButtonContainer.Controls.Remove(_buttonNext);
                isLastPage = true;
            }
            //var loc = new Point(159, 0);
            foreach (TabButton tb in panelButtonContainer.Controls)
            {
                if (!tb.IsPositionFixed)
                {
                    _removedButtons.Add(tb);
                    if (_nextToAdd < _listActiveButtons.Count)
                    {
                        if (_listActiveButtons[_nextToAdd].IsPositionSet == false)
                        {
                            _listActiveButtons[_nextToAdd].IsPositionSet = true;
                            _listActiveButtons[_nextToAdd].Location = tb.Location;
                        }
                    }
                    _nextToAdd++;
                }
            }
            if (isLastPage)
            {
                if (_nextToAdd < _listActiveButtons.Count)
                {
                    if (_listActiveButtons[_nextToAdd].IsPositionSet == false)
                    {
                        _listActiveButtons[_nextToAdd].IsPositionSet = true;
                        _listActiveButtons[_nextToAdd].Location = _buttonNext.Location;

                    }

                }
                _nextToAdd++;
            }
            panelButtonContainer.Controls.Add(_buttonPrevious);
            //remove ListActiveButtons from panel
            for (int i = 0; i < _nextToAdd; i++)
            {
                panelButtonContainer.Controls.Remove(_removedButtons[_removedButtons.Count - (i + 1)]);
            }
            AddButtonsToPanel();
            if (panelButtonContainer.Controls.Count <= 3)
            {
                foreach (TabButton btn in panelButtonContainer.Controls)
                {
                    btn.Location = new Point(btn.Location.X, 0);
                }

                int btnCount = (from TabButton n in panelButtonContainer.Controls where n.Location == _buttonPrevious.Location select n).Count();
                if (btnCount > 1)//if previous and other button set at same position
                {
                    //get location of correct button  other thatn _buttonPrevious
                    TabButton correctButton = (TabButton)(from TabButton n in panelButtonContainer.Controls where n.Location != _buttonPrevious.Location select n).SingleOrDefault();

                    int x = 0;
                    int y = 0;
                    Point rightLocation = new Point(x, y);
                    while (rightLocation == _buttonPrevious.Location || rightLocation == correctButton.Location)
                    {
                        x += _buttonPrevious.Width + ButtonSpace;
                        rightLocation = new Point(x, y);
                    }
                    //Get Button at wrong location
                    TabButton wrongButton = (from TabButton n in panelButtonContainer.Controls where (n.Location == _buttonPrevious.Location && n != _buttonPrevious) select n).SingleOrDefault();
                    wrongButton.Location = rightLocation;
                }
                ResizePanel(true);
            }
        }
        public void Previous()
        {
            _nextToAdd = 0;
            int fixedButtons = (from n in panelButtonContainer.Controls.OfType<TabButton>() where n.IsPositionFixed select n).Count();

            if (_removedButtons.Count + fixedButtons - 1 <= 6)
            {
                _buttonPrevious.IsPositionFixed = false;
            }

            fixedButtons = 0;
            foreach (TabButton tb in panelButtonContainer.Controls)
            {
                if (tb == _buttonNext)
                    tb.IsPositionFixed = true;

                if (!tb.IsPositionFixed)
                {
                    _listActiveButtons.Insert(_nextToAdd, tb);
                    _nextToAdd++;
                }
                else
                {
                    fixedButtons++;
                }
            }

            for (int i = 0; i < _nextToAdd; i++)
            {
                panelButtonContainer.Controls.Remove(_listActiveButtons[i]);
            }
            var heightLimit = ScaleUtil.GetScaledPosition(102);
            if (panelButtonContainer.Height < heightLimit)
            { ResizePanel(false); }
            for (int i = 0; i < 6 - fixedButtons; i++)
            {
                panelButtonContainer.Controls.Add(_removedButtons[_removedButtons.Count - 1]);
                _removedButtons.RemoveAt(_removedButtons.Count - 1);
            }
            if (_removedButtons.Count == 0 && _buttonPrevious != null)
            {
                panelButtonContainer.Controls.Remove(_buttonPrevious);
            }
            var fifty = ScaleUtil.GetScaledPosition(50);
            if (panelButtonContainer.Controls.Count > 3 && PreviousButtonIndex < 3)
            {
                _buttonPrevious.Location = new Point(ScaleUtil.GetScaledPosition(_buttonPrevious.Location.X), fifty + ButtonSpace);
            }
        }

        /// <summary>
        /// check if a button with given name is in pannel currently
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Boolean IsActive(string name)
        {
            return
                (from TabButton btn in panelButtonContainer.Controls
                 where btn.Name.Equals(name)
                 select btn.Name.Equals(name)).FirstOrDefault();
        }


        /// <summary>
        /// check if a button with given name is in pannel currently is enabled
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Boolean IsEnabled(string name)
        {
            foreach (TabButton btn in panelButtonContainer.Controls)
            {
                if (btn.Name.Equals(name))
                {
                    return btn.Enabled;
                }
            }

            foreach (TabButton btn in _listActiveButtons)
            {
                if (btn.Name.Equals(name))
                {
                    return btn.Enabled;
                }
            }

            foreach (TabButton btn in _removedButtons)
            {
                if (btn.Name.Equals(name))
                {
                    return btn.Enabled;
                }
            }
            return false;
        }

        /// <summary>
        /// check if a button with given name is in pannel currently is enabled
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Boolean IsVisible(string name)
        {
            foreach (TabButton btn in _listActiveButtons)
            {
                if (btn.Name.Equals(name))
                {
                    return btn.Visible;
                }
            }
            throw new Exception("Button Not found");
        }

        /// <summary>
        /// change enabled status of a named button  to given status
        /// </summary>
        /// <param name="name"></param>
        /// <param name="status"></param>
        public void Enable(string name, Boolean status)
        {
            bool isButtonFound = false;
            foreach (TabButton btn in panelButtonContainer.Controls)
            {
                if (btn.Name.Equals(name))
                {
                    btn.Enabled = status;
                    isButtonFound = true;
                    break;
                }
            }
            if (isButtonFound)
                return;
            foreach (TabButton btn in _listActiveButtons)
            {
                if (btn.Name.Equals(name))
                {
                    btn.Enabled = status;
                    isButtonFound = true;
                    break;
                }
            }
            if (isButtonFound)
                return;
            foreach (TabButton btn in _removedButtons)
            {
                if (btn.Name.Equals(name))
                {
                    btn.Enabled = status;
                    break;
                }
            }
        }

        public TabButton GetButton(string name)
        {

            foreach (TabButton btn in ListButtons)
            {
                if (btn.Name.Equals(name))
                {
                    return btn;
                }
            }

            return null;
        }
        public void IsVisible(string name, Boolean status)
        {
            bool isButtonFound = false;
            foreach (TabButton btn in ListButtons)
            {
                if (btn.Name.Equals(name))
                {
                    btn.Visible = status;
                    isButtonFound = true;
                }
            }
            if (isButtonFound)
            { Refresh(); }
        }

        /// <summary>
        /// change visible status of a nemed button to given status
        /// </summary>
        /// <param name="name"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public void HideButton(string name, Boolean status)
        {
            foreach (TabButton btn in panelButtonContainer.Controls)
            {
                if (btn.Name.Equals(name))
                {
                    if (status)
                    {
                        btn.BackColor = Color.FromArgb(46, 48, 50);
                        btn.ForeColor = Color.White;
                        btn.Enabled = true;
                    }
                    else
                    {
                        btn.BackColor = BackColor = Color.FromArgb(19, 20, 20);
                        btn.Enabled = false;
                        btn.ForeColor = Color.FromArgb(19, 20, 20);
                    }
                    return;
                }
            }
            foreach (TabButton btn in _listActiveButtons)
            {
                if (btn.Name.Equals(name))
                {
                    if (status)
                    {
                        btn.BackColor = Color.FromArgb(46, 48, 50);
                        btn.ForeColor = Color.White;
                        btn.Enabled = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(19, 20, 20);
                        btn.Enabled = false;
                        btn.ForeColor = Color.FromArgb(19, 20, 20);
                    }
                    return;
                }
            }

            foreach (TabButton btn in _removedButtons)
            {
                if (btn.Name.Equals(name))
                {
                    if (status)
                    {
                        btn.BackColor = Color.FromArgb(46, 48, 50);
                        btn.ForeColor = Color.White;
                        btn.Enabled = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(19, 20, 20);
                        btn.Enabled = false;
                        btn.ForeColor = Color.FromArgb(19, 20, 20);
                    }
                    return;
                }
            }
            foreach (TabButton btn in ListButtons)
            {
                if (btn.Name.Equals(name))
                {
                    if (status)
                    {
                        btn.BackColor = Color.FromArgb(46, 48, 50);
                        btn.ForeColor = Color.White;
                        btn.Enabled = true;
                    }
                    else
                    {
                        btn.BackColor = Color.FromArgb(19, 20, 20);
                        btn.Enabled = false;
                        btn.ForeColor = Color.FromArgb(19, 20, 20);
                    }
                    return;
                }
            }
        }

        void ResizePanel(bool isToShorten)
        {
            if (isToShorten)
            {
                Size = ScaleUtil.GetScaledSize(476, 50);
                Location = ScaleUtil.GetScaledPoint(2, 485);
            }
            else
            {
                Location = ScaleUtil.GetScaledPoint(2, 433);
                Size = ScaleUtil.GetScaledSize(476, 102);
            }
            if (ScaleUtil.IsHighDensityDevice) Top += 15;

        }

        #endregion

        #region Events

        /// <summary>
        /// show next 6 or rest ListActiveButtons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextButtonClick(object sender, EventArgs e)
        {
            CurrentPage++;
            Next();
        }

        /// <summary>
        /// show previous 6 ListActiveButtons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreviousButtonClick(object sender, EventArgs e)
        {
            CurrentPage--;
            Previous();
        }

        #endregion
    }

    public enum TabButtonType
    {
        Normal = 1,
        Confirm = 2,
        Cancel = 3
    }

    public class TabButton : Resco.Controls.OutlookControls.ImageButton
    {
        #region Variables and Objects
        public Boolean IsPositionFixed = false;
        public Boolean IsPositionSet = false;
        public Boolean UseDefaultProperties = true;
        public new TabButtonType ButtonType = TabButtonType.Normal;
        #endregion

        #region Overloaded Constructors

        /// <summary>
        /// if all properties are to be set in parent form
        /// </summary>
        public TabButton()
        {
        }

        /// <summary>
        /// specify buttontext and event handler
        /// </summary>
        /// <param name="buttonText"></param>
        /// <param name="eh"></param>
        public TabButton(string buttonText, EventHandler eh)
        {
            Text = buttonText;
            Click += eh;
        }

        /// <summary>
        /// button with Type
        /// </summary>
        /// <param name="buttonText"></param>
        /// <param name="eh"></param>
        /// <param name="type"></param>
        public TabButton(string buttonText, EventHandler eh, TabButtonType type)
        {
            Text = buttonText;
            Click += eh;
            ButtonType = type;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        /// <summary>
        /// create named button 
        /// </summary>
        /// <param name="buttonText"></param>
        /// <param name="eh"></param>
        /// <param name="buttonName"> </param>
        public TabButton(string buttonText, EventHandler eh, string buttonName)
        {
            Text = buttonText;
            Click += eh;
            Name = buttonName;
        }
        #endregion
    }
}

﻿using System.Drawing;
using System.Windows.Forms;
using SampleScreenDesigner.Properties;

//using Com.Bring.PMP.PreComFW.Shared.Properties;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
 
{
    public class MessageHolder
    {
        public MessageHolder()
        {
            Text = string.Empty;
            State = MessageState.Empty;
        }

        public MessageHolder(string text, MessageState state)
        {
            Text = text;
            State = state;
        }

        public string Text;
        public MessageState State;
    }

    public enum MessageState
    {
        Empty,
        Error,
        Information,
        Warning,
        ModalMessage
    }

    public partial class MessageControl : UserControl
    {


        public MessageControl()
        {
            InitializeComponent();
            Visible = false;
        }




        /// <summary>
        /// Specifies the text for message as set by user.
        /// </summary>
        public string MessageText
        {
            get { return labelMessage.Text.Trim(); }
            set { labelMessage.Text = value; }
        }

        /// <summary>
        /// Specifies the type of message as set by user.
        /// </summary>
        public MessageState CurrentMessageState { get; set; }


        public MessageHolder Message
        {
            get { return new MessageHolder(MessageText, CurrentMessageState); }
            set { MessageText = value.Text; CurrentMessageState = value.State; }
        }


        public Color SetLableFontColor
        {
            get { return labelMessage.ForeColor; }
            set { labelMessage.ForeColor = value; }
        }

        public void ShowMessage(MessageHolder messageHolder)
        {
            ShowMessage(messageHolder.Text, messageHolder.State);
        }

        public void ClearMessage()
        {
            ShowMessage("", MessageState.Empty);
        }

        public void ShowMessage(string messageText, MessageState messageState)
        {
            MessageText = messageText;
            CurrentMessageState = messageState;

            switch (messageState)
            {
                case MessageState.Empty:
                    Visible = false;
                    break;
                case MessageState.Error:
                    pictureBoxIcon.Image = Resources.FOT_icon_error;

                    Visible = true;
                    break;
                case MessageState.Warning:
                    pictureBoxIcon.Image = Resources.FOT_icon_attention;
                    Visible = true;
                    break;
                case MessageState.Information:
                    pictureBoxIcon.Image = Resources.FOT_icon_verified_ok;
                    Visible = true;
                    break;
                default:
                    System.Diagnostics.Debug.Fail("Unhandled messagetype detected: " + messageState.ToString());
                    break;

            }
        }
    }
}


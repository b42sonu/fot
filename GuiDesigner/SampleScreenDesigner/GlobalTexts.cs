﻿using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.Constants
{
    public class GlobalTexts
    {
        // Text for buttons
        public static string Signature { get { return Language.Translate("Signature"); } }
        public static string NewDamage { get { return Language.Translate("New Damage"); } }
        public static string TakePhoto { get { return Language.Translate("Take picture"); } }
        public static string Items { get { return Language.Translate("Items"); } }
        public static string Deviation { get { return Language.Translate("Deviation"); } }
        public static string Delete { get { return Language.Translate("Delete"); } }
        public static string DifferentStop { get { return Language.Translate("Different Stop"); } }
        public static string Save { get { return Language.Translate("Save"); } }
        public static string Camera { get { return Language.Translate("Camera"); } }
        public static string Reset { get { return Language.Translate("Reset"); } }
        public static string TmsAvailability { get { return Language.Translate("TMS Availability"); } }
        public static string GpsSettings { get { return Language.Translate("GPS Settings"); } }
        public static string TmiSettings { get { return Language.Translate("TMI Settings"); } }
        public static string Del { get { return Language.Translate("Del"); } }
        public static string ChangeProfile { get { return Language.Translate("Change Profile"); } }
        public static string ChangeLogLevel { get { return Language.Translate("Change LogLevel"); } }
        public static string Attempt { get { return Language.Translate("Attempt"); } }
        public static string OkEnter { get { return Language.Translate("Ok/Enter"); } }
        public static string Confirm { get { return Language.Translate("Confirm"); } }
        public static string UnplannedPick { get { return Language.Translate("Unpl. pick"); } }
        public static string AttemptedDel { get { return Language.Translate("Attempted Del"); } }
        public static string Departure { get { return Language.Translate("Departure"); } }
        public static string Refresh { get { return Language.Translate("Refresh"); } }
        public static string Details { get { return Language.Translate("Details"); } }
        public static string Open { get { return Language.Translate("Open"); } }
        public static string Get { get { return Language.Translate("Get"); } }
        public static string Back { get { return Language.Translate("Back"); } }
        public static string Scan { get { return Language.Translate("Scan"); } }
        public static string Yes { get { return Language.Translate("Yes"); } }
        public static string No { get { return Language.Translate("No"); } }
        public static string Ok { get { return Language.Translate("Ok"); } }
        public static string Stop { get { return Language.Translate("Stop"); } }
        public static string Break { get { return Language.Translate("Break"); } }
        public static string Trace { get { return Language.Translate("Trace"); } }
        public static string On { get { return Language.Translate("On"); } }
        public static string Off { get { return Language.Translate("Off"); } }
        public static string Finish { get { return Language.Translate("Finish"); } }
        public static string Cancel { get { return Language.Translate("Cancel"); } }
        public static string ChgCarrier { get { return Language.Translate("Chg.Carrier"); } }
        public static string Unload { get { return Language.Translate("Unload"); } }
        public static string Load { get { return Language.Translate("Load"); } }
        public static string Reconcillation { get { return Language.Translate("Reconcilliation"); } }
        public static string Attempted { get { return Language.Translate("Attempted"); } }
        public static string Continue { get { return Language.Translate("Continue"); } }
        public static string Erase { get { return Language.Translate("Erase"); } }
        public static string AddTransportEquipment { get { return Language.Translate("Add transport equipment"); } }
        public static string AddPackageType { get { return Language.Translate("Add package type"); } }
        public static string Info { get { return Language.Translate("Info"); } }
        public static string Reject { get { return Language.Translate("Reject"); } }
        public static string Accept { get { return Language.Translate("Accept"); } }
        public static string AcceptAll { get { return Language.Translate("Accept all"); } }
        public static string Login { get { return Language.Translate("Login"); } }
        public static string Logout { get { return Language.Translate("Logout"); } }
        public static string Planning { get { return Language.Translate("Planning"); } }
        public static string StartBreak { get { return Language.Translate("Start Break"); } }
        public static string StopBreak { get { return Language.Translate("Stop Break"); } }
        public static string Hour1 { get { return Language.Translate("1H"); } }
        public static string Hour2 { get { return Language.Translate("2H"); } }
        public static string Hour4 { get { return Language.Translate("4H"); } }
        public static string Hour8 { get { return Language.Translate("8H"); } }
        public static string NextPage { get { return Language.Translate("Next page"); } }
        public static string PreviousPage { get { return Language.Translate("Previous page"); } }
        public static string SortStops { get { return Language.Translate("Sort stops"); } }
        public static string ChangeAvailableStatus { get { return Language.Translate("Change available status"); } }
        public static string ExchangeLoadMaterial { get { return Language.Translate("Exchange load material"); } }
        public static string RegisterTemperatureBtn { get { return Language.Translate("Register temperature"); } }
        public static string ChangeOperationlistStatus { get { return Language.Translate("Change status operation list user"); } }
        public static string Pickup { get { return Language.Translate("Pickup"); } }
        public static string Up { get { return Language.Translate("Up"); } }
        public static string Down { get { return Language.Translate("Down"); } }
        public static string Predef { get { return Language.Translate("Predef"); } }
        public static string AsLoaded { get { return Language.Translate("As Loaded"); } }
        public static string ManualSort { get { return Language.Translate("Manual Sort"); } }
        public static string Zone { get { return Language.Translate("Zone"); } }
        public static string Sorting { get { return Language.Translate("Sorting"); } }
        public static string ManualSorting { get { return Language.Translate("Manual Sorting"); } }
        public static string NextZone { get { return Language.Translate("Next Zone"); } }
        public static string PrevZone { get { return Language.Translate("Prev Zone"); } }
        public static string ChangeZone { get { return Language.Translate("Change Zone"); } }
        public static string Sort { get { return Language.Translate("Sort"); } }
        public static string InternalScanner { get { return Language.Translate("Internal scanner"); } }
        public static string ExternalScanner { get { return Language.Translate("External scanner"); } }
        public static string ActivateAutomaticKeyLock { get { return Language.Translate("Automatic KeyLock"); } }


        // Text for labels
        public static string Delivery { get { return Language.Translate("Delivery"); } }
        public static string ConfirmSignature { get { return Language.Translate("Please confirm with or without signature"); } }
        public static string ConfirmDamageForItem { get { return Language.Translate("Confirm damage for item type and item number"); } }
        public static string Comment { get { return Language.Translate("Comment"); } }
        public static string PackageTypeDamaged { get { return Language.Translate("Package type was totally damaged"); } }
        public static string FellOutOfCar { get { return Language.Translate("Fell out of car"); } }
        public static string Destruert { get { return Language.Translate("Destruert"); } }
        public static string TakePictureConfirmation { get { return Language.Translate("Do you want to take a picture?"); } }
        public static string LabelDamageForItem { get { return Language.Translate("Damage for Item Text Item Number"); } }
        public static string DamageType { get { return Language.Translate("Damage type"); } }
        public static string APackageDamage { get { return Language.Translate("A Package Damage"); } }
        public static string PackageDamage { get { return Language.Translate("Package Damage"); } }
        public static string DamageInformation { get { return Language.Translate("Damage Information"); } }
        public static string Text { get { return Language.Translate("Text"); } }
        public static string EarlierEvents { get { return Language.Translate("Earlier events for item"); } }
        public static string NrOfConsignments { get { return Language.Translate("Consigment count"); } }
        public static string StopId { get { return Language.Translate("Stop Id"); } }
        public static string DeliveryToPostOffice { get { return Language.Translate("Delivery to Post Office"); } }
        public static string UnplannedPickup { get { return Language.Translate("Unplanned Pickup"); } }
        public static string WithoutStop { get { return Language.Translate("Without stop"); } }
        public static string Operations { get { return Language.Translate("Operations"); } }
        public static string Stops { get { return Language.Translate("Stops"); } }
        public static string InformationConsignmentScanned { get { return Language.Translate("You have scanned a consignment"); } }
        public static string ConsignmentId { get { return Language.Translate("Consignment id"); } }
        public static string ConfirmConsignmentCount { get { return Language.Translate("Confirm consignment item count"); } }
        public static string Discard { get { return Language.Translate("Discard"); } }
        public static string DamageRegistration { get { return Language.Translate("Damage registration"); } }
        public static string DamageFor { get { return Language.Translate("Damage for"); } }
        public static string CameraInstruction { get { return Language.Translate("Click on camera  to activate \r\ncamera .User enter button to\r\ncapture a photo"); } }
        public static string LabelThumbnail { get { return Language.Translate("Thumbnail for already captured photos"); } }
        public static string ChangeTmsAvailability { get { return Language.Translate("Change TMS Availability"); } }
        public static string PlanTmsAvailability { get { return Language.Translate("Plan TMS Availability"); } }
        public static string ChangeConfirmInformation { get { return Language.Translate("Change or Confirm Information"); } }
        public static string ScanOrEnterBarcode { get { return Language.Translate("Scan or enter barcode"); } }
        public static string AttemptedPickUp { get { return Language.Translate("Attempted Pickup"); } }
        public static string DepartFromStop { get { return Language.Translate("Are you sure you would like to depart from this stop?"); } }
        public static string OperationDetails { get { return Language.Translate("Operation Details"); } }
        public static string StartOperationList { get { return Language.Translate("Start Operation list from main menu to begin your work"); } }
        public static string OperationListPresent { get { return Language.Translate("Operation list present"); } }
        public static string Consignee { get { return Language.Translate("Consignee"); } }
        public static string Consignments { get { return Language.Translate("Consignments"); } }
        public static string ConsigneeAddr { get { return Language.Translate("Consignee address"); } }
        public static string Completed { get { return Language.Translate("Completed"); } }
        public static string NotCompleted { get { return Language.Translate("Not Completed"); } }
        public static string All { get { return Language.Translate("All"); } }
        public static string GrossWeight { get { return Language.Translate("Gross Weight"); } }
        public static string CustomerNr { get { return Language.Translate("Customer id"); } }
        public static string ConsignorAddr { get { return Language.Translate("Consignor address"); } }
        public static string OrderNo { get { return Language.Translate("Order No"); } }
        public static string Cause { get { return Language.Translate("Cause"); } }
        public static string Measure { get { return Language.Translate("Measure"); } }
        public static string Description { get { return Language.Translate("Description"); } }
        public static string GenerateBookingNumber { get { return Language.Translate("Generate booking number"); } }
        public static string ClickToGenrateBookingNumber { get { return Language.Translate("Or click  here to generate"); } }
        public static string BookingNumber { get { return Language.Translate("Booking Number"); } }
        public static string CustomerNumber { get { return Language.Translate("Customer Number"); } }
        public static string UnplanedPickUp { get { return Language.Translate("Unplanned pick up"); } }
        public static string ChangePassword { get { return Language.Translate("Change Password"); } }
        public static string CurrentPassword { get { return Language.Translate("Current Password"); } }
        public static string NewPassword { get { return Language.Translate("New Password"); } }
        public static string ConfirmPassword { get { return Language.Translate("Confirm Password"); } }
        public static string AttemptDeliveryInfo { get { return Language.Translate("Attempted delivery information"); } }
        public static string OptionalComment { get { return Language.Translate("Comment (optional)"); } }
        public static string OptionalComments { get { return Language.Translate("Comments (optional)"); } }
        public static string AttemptedDelivery { get { return Language.Translate("Attempted delivery"); } }
        public static string ConsignmentItemNumber { get { return Language.Translate("Consignment/item number"); } }
        public static string TripId { get { return Language.Translate("Trip Id"); } }
        public static string ConfirmInfo { get { return Language.Translate("Are new carrier and select operation type you wish to start"); } }
        public static string NewCarrier { get { return Language.Translate("New Carrier"); } }
        public static string SelectAddCarrier { get { return Language.Translate("Select a carrier from the list and click Continue or New Carrier to add another"); } }
        public static string WorkList { get { return Language.Translate("Work List"); } }
        public static string AddCarrierSelectOperation { get { return Language.Translate("Add new carrier and select operation type you wish to start"); } }
        public static string SignatureInfo { get { return Language.Translate("Your signature will be displayed in Posten/Brings tracking solution online"); } }
        public static string NrOfConsignmentItem { get { return Language.Translate("Nr of consignment items"); } }
        public static string CustomerName { get { return Language.Translate("Customer name"); } }
        public static string VerifyChangePowerUnit { get { return Language.Translate("Verify or change Power Unit ID"); } }
        public static string LoadedByCustomer { get { return Language.Translate("Load by customer"); } }
        public static string PowerUnitId { get { return Language.Translate("Power Unit ID"); } }
        public static string Quantity { get { return Language.Translate("Quantity"); } }
        public static string PackageTypes { get { return Language.Translate("Package types"); } }
        public static string PackageTypeAndTransportEquipment { get { return Language.Translate("Package type and transport equipment"); } }
        public static string TransportEquipment { get { return Language.Translate("Transport equipment"); } }
        public static string PrivateContactDetail { get { return Language.Translate("Private Contact Detail"); } }
        public static string GroupInformation { get { return Language.Translate("Group Information"); } }
        public static string Settings { get { return Language.Translate("Settings"); } }
        public static string Available { get { return Language.Translate("Available"); } }
        public static string Unavailable { get { return Language.Translate("Unavailable"); } }
        public static string Date { get { return Language.Translate("Date"); } }
        public static string VerifyChangeRouteId { get { return Language.Translate("Verify or change Route ID"); } }
        public static string SortZone { get { return Language.Translate("Sort Zone"); } }
        public static string ZoneSortingView { get { return Language.Translate("Zone Sorting View"); } }
        public static string DeliveryMovedToZone { get { return Language.Translate("Delivery moved into Zone "); } }
        public static string AssignZone { get { return Language.Translate("Click on Zone to assign selected row"); } }
        public static string ScanConsignmentItemOnly { get { return Language.Translate("Scan or enter consignment item"); } }
        public static string FinishedZoneGrouping { get { return Language.Translate("Finished grouping into zones"); } }
        public static string EnterConsigneeName { get { return Language.Translate("Enter Consignee Name"); } }
        public static string HelpComingSoon { get { return Language.Translate("Help is coming soon"); } }




        // Short statements and headers
        public static string InvalidSignature { get { return Language.Translate("Invalid signature"); } }
        public static string ConsignmentDeleted { get { return Language.Translate("Consignment deleted"); } }
        public static string ConfirmationOperationList { get { return Language.Translate("Operation List Confirmation"); } }
        public static string Attention { get { return Language.Translate("Attention"); } }
        public static string Error { get { return Language.Translate("Error"); } }
        public static string Goods { get { return Language.Translate("Goods"); } }
        public static string Offline { get { return Language.Translate("Offline"); } }
        public static string Summary { get { return Language.Translate("Summary"); } }
        public static string Unknown { get { return Language.Translate("Unknown"); } }
        public static string ScanConsignment { get { return Language.Translate("Scan required consignment"); } }
        public static string Temperature { get { return Language.Translate("Temperature"); } }
        public static string ConsignmentItem { get { return Language.Translate("Consignment item"); } }
        public static string ConsignmentItemShort { get { return Language.Translate("Cons. item"); } }
        public static string Consignment { get { return Language.Translate("Consignment"); } }
        public static string OrgUnitId { get { return Language.Translate("OrgUnit Id"); } }
        public static string PowerUnit { get { return Language.Translate("Power Unit"); } }
        public static string LoadCarrier { get { return Language.Translate("Load Carrier"); } }
        public static string ContainmentUnit { get { return Language.Translate("Containment Unit"); } }
        public static string UnitId { get { return Language.Translate("Unit Id"); } }
        public static string PostalCode { get { return Language.Translate("Postal Code"); } }
        public static string TmsAffiliation { get { return Language.Translate("TMS Affiliation"); } }
        public static string RouteId { get { return Language.Translate("Route Id"); } }
        public static string CompanyCode { get { return Language.Translate("Company code"); } }
        public static string CountryCode { get { return Language.Translate("Country code"); } }
        public static string TelephoneNumber { get { return Language.Translate("Phone no"); } }
        public static string UserProfile { get { return Language.Translate("User Profile"); } }
        public static string OperationFormHeaderText { get { return Language.Translate("Operation list"); } }
        public static string TmsAvailabilityInformation { get { return Language.Translate("Tasks information"); } }
        public static string DriverAvailability { get { return Language.Translate("Driver availability"); } }
        public static string UnfinishedOperationsTitle { get { return Language.Translate("Unfinished stops"); } }
        public static string UnfinishedDeliveriesTitle { get { return Language.Translate("Unfinished deliveries"); } }
        public static string ZoneItemSortHeading1 { get { return Language.Translate("Sort items inside Zone "); } }
        public static string ZoneItemSortHeading2 { get { return Language.Translate("Select a row move it up/down"); } }
        public static string ChangeZoneHeading { get { return Language.Translate("Select zone to move item to, or press cancel"); } }
        public static string Help { get { return Language.Translate("Help"); } }

        // Information
        public static string ScanConsignmentDeleted { get { return Language.Translate("Selected scanned consignment/consignment Item(s) has been deleted from cache"); } }
        public static string AreYouSureYouWantToDelete { get { return Language.Translate("Are you sure you want to delete"); } }
        public static string PackageTypeAndTransportVerified { get { return Language.Translate("Package type and transport equipment verified"); } }
        public static string ConsignmentAddedToItem { get { return Language.Translate("Consignment {0} added to consignment item {1}"); } }
        public static string ConsignmentItemCount { get { return Language.Translate("Consignment item count"); } }
        public static string ChooseCorrectionForConsignment { get { return Language.Translate("Consignment scanned earlier. Choose correction for"); } }
        public static string ChooseCorrectionForItem { get { return Language.Translate("Item scanned earlier. Choose correction for"); } }
        public static string ProcessForDeleteItemFailed { get { return Language.Translate("Process for Deletion of Item {0} failed"); } }
        public static string ItemDeleted { get { return Language.Translate("Consignment {0} deleted"); } }
        public static string ConsignmentItemDeleted { get { return Language.Translate("Consignment item {0} deleted"); } }
        public static string RegisterTempratureForItem { get { return Language.Translate("Process for registering temperature triggered for item"); } }
        public static string RegisterCorrectionForItem { get { return Language.Translate("Process for registering correction triggered for item"); } }
        public static string LoadingProfile { get { return Language.Translate("Please wait, loading profile from server"); } }
        public static string UserLoggedOffFromAsset { get { return Language.Translate("User is no longer associated with the current Asset or logged off by another user"); } }
        public static string OfflineMessage { get { return Language.Translate("You are not connected to server, and have logged in offline"); } }
        public static string UserProfileFromServer { get { return Language.Translate("Getting profile from server..."); } }
        public static string ConnectedTolocalProfile { get { return Language.Translate("You were logged in offline with local profile"); } }
        public static string ProfileRetrived { get { return Language.Translate("Profile retrieved successfully from server"); } }
        public static string InConsignment { get { return Language.Translate("in consignment "); } }
        public static string RegisteredConsignment { get { return Language.Translate("Registered consignment "); } }
        public static string RegisteredConsignmentItem { get { return Language.Translate("Registered consignment item "); } }
        public static string NoAssociatedPowerunit { get { return Language.Translate("You are no longer associated to the current Power Unit in the TMS. You will not get updated planning information for the Power Unit"); } }
        public static string AcceptRejectOrder { get { return Language.Translate("{0} orders accepted and {1} orders rejected"); } }
        public static string NewOperationListRecived { get { return Language.Translate("New operation list recieved"); } }
        public static string UpdatedOperationListRecived { get { return Language.Translate("Updated operation list recieved"); } }
        public static string PhotoAdded { get { return Language.Translate("Photo added"); } }
        public static string ConsignmentForItemDiffersFromPlan { get { return Language.Translate("Consignment returned from LM {0}, differs from consignment in plan: {1}\n Can not continue with this barcode"); } }
        public static string ArrivedAtWarehouse { get { return Language.Translate("Arrived at warehouse"); } }
        public static string InternalScannerActivated { get { return Language.Translate("Internal scanner activated"); } }
        public static string ExternalScannerActivated { get { return Language.Translate("External scanner activated"); } }
        public static string DeliveryDateRegistered { get { return Language.Translate("{0} has a planned delivery date registered"); } }

        //Verifications
        public static string ConfirmOperationListMessage { get { return Language.Translate("Backup operation list found on device. Do you want to load it?"); } }
        public static string VerifyPowerUnitLoadingMessage { get { return Language.Translate("You have no loading operations in your operation list. Please verify if the current Power Unit ID is correct or change it by input the correct one"); } }
        public static string VerifyPowerUnitLoadListMessage { get { return Language.Translate("Please provide your current Power Unit ID"); } }
        public static string VerifyRouteIdLoadListMessage { get { return Language.Translate("Please provide your current Route ID"); } }

        public static string VerifyScannedUnplannedConsignment { get { return Language.Translate("Please confirm you want to handle scanned, unplanned consignment"); } }
        public static string UnloadingVerification { get { return Language.Translate("Please verify if unloading for this stop is completed"); } }
        public static string PendingTasksOperationListMessage { get { return Language.Translate("You have pending operations in operation list. Do you want to continue?"); } }
        public static string PendingTasksLoadListMessage { get { return Language.Translate("You have pending tasks in Load list. Do you want to continue?"); } }


        // Confirmation
        public static string NoPendingTasksMessage { get { return Language.Translate("User has no pending tasks in the OperationList so logging off the user from TMS"); } }
        public static string PendingTasksMessage { get { return Language.Translate("User has some pending tasks. Do you really want to continue"); } }
        public static string UserProfileLabel { get { return Language.Translate("Change or confirm Information"); } }
        public static string DriverOverridden { get { return Language.Translate("The Asset already has a driver assigned. Do you wish to take over the asset?"); } }
        public static string UnloadingCompleted { get { return Language.Translate("Unloading completed?"); } }
        public static string UnfinishedOperationsDescription { get { return Language.Translate("You have unfinished operations in the operation list. Do you want to log out?"); } }
        public static string UnfinishedDeliveriesDescription { get { return Language.Translate("You have unfinished deliveries in the delivery list"); } }
        public static string AreYouSureYouWantToLogOut { get { return Language.Translate("Are you sure you want to log out?"); } }
        public static string EnterOptionalConsignment { get { return Language.Translate("Optional consignment number is missing. Do you want to provide it?"); } }

        // Abortions
        public static string AttemptedDeliveryAborted { get { return Language.Translate("Attempted delivery aborted"); } }
        public static string PackageTypeAndTransportAborted { get { return Language.Translate("Verify package type and transport equipment aborted"); } }
        public static string ScanningConnsignmentAborted { get { return Language.Translate("Scanning of consignment is aborted"); } }
        public static string HandleScannedUnplannedConsignmentAborted { get { return Language.Translate("Handling of scanned, unplanned consignment aborted"); } }
        public static string CorrectionMenuAborted { get { return Language.Translate("Correction menu aborted"); } }
        public static string ConsignmentItemConfirmationAborted { get { return Language.Translate("Confirmation of consignment item count aborted"); } }
        public static string FlowAborted { get { return Language.Translate("Flow aborted"); } }
        public static string ScanningAborted { get { return Language.Translate("Scanning aborted"); } }
        // Flows
        public static string NotDelivered { get { return Language.Translate("Not Delivered"); } }
        public static string Unloading { get { return Language.Translate("Unloading"); } }
        public static string Loading { get { return Language.Translate("Loading"); } }
        public static string LoadingLineHaulTruck { get { return Language.Translate("Loading of line haul truck"); } }
        public static string LoadingDistributionTruck { get { return Language.Translate("Loading of distribution truck"); } }
        public static string LoadingCombinationTruck { get { return Language.Translate("Loading of combination truck"); } }
        public static string LoadingCombinationTrip { get { return Language.Translate("Loading of combination trip"); } }
        public static string UnloadingPickupTruck { get { return Language.Translate("Unloading of pickup truck"); } }
        public static string UnloadingLineHaulTruck { get { return Language.Translate("Unloading of line haul truck"); } }
        public static string PickupFromCustomer { get { return Language.Translate("Pickup from Customer"); } }
        public static string BuildingOfLoadCarrier { get { return Language.Translate("Building Of Load Carrier"); } }
        public static string RemainingGoodsAtHub { get { return Language.Translate("Remaining Goods at Hub"); } }
        public static string PrintLoadList { get { return Language.Translate("Print or mail load list"); } }
        public static string PrintUnloadList { get { return Language.Translate("Print or mail unloading list"); } }
        public static string ControlScan { get { return Language.Translate("Control Scan"); } }
        public static string IntoHoldingAtHub { get { return Language.Translate("Into holding at hub"); } }
        public static string OutOfHoldingAtHub { get { return Language.Translate("Out of holding at hub"); } }
        public static string RegisterDamage { get { return Language.Translate("Register Damage"); } }
        public static string CorrectWeightAndVolume { get { return Language.Translate("Correct Weight and Volume"); } }
        public static string ChangeProductGroup { get { return Language.Translate("Change Product Group"); } }
        public static string GeneralDeviations { get { return Language.Translate("General Deviations"); } }
        public static string ArrivalRegistration { get { return Language.Translate("Arrival Registration"); } }
        public static string LoadList { get { return Language.Translate("Load List"); } }
        public static string ExecuteLinking { get { return Language.Translate("Execute Linking"); } }
        public static string CorrectWeightVolume { get { return Language.Translate("Correct w/v"); } }
        public static string DeliveryToSubContractor { get { return Language.Translate("Delivered to sub-contractor/driver"); } }
        public static string CorrectionMenu { get { return Language.Translate("Correction menu"); } }
        public static string RegisterTemperature { get { return Language.Translate("Register temperature"); } }
        public static string BulkRegistration { get { return Language.Translate("Bulk Registration"); } }
        public static string DeliveryToCustomer { get { return Language.Translate("Delivery to customer"); } }
        public static string DeliveredByCustomer { get { return Language.Translate("Delivered by customer"); } }
        public static string DeliveredToCustomerAtPoPib { get { return Language.Translate("Delivered to customer at PO/PiB"); } }
        public static string DeliveredAtHub { get { return Language.Translate("Delivered at Hub"); } }
        public static string PickingFromWarehouse { get { return Language.Translate("Picking from warehouse"); } }
        public static string HandedInTerminal { get { return Language.Translate("Handed in at Terminal"); } }
        public static string InTerminal { get { return Language.Translate("In Terminal"); } }
        public static string PreparingForDriver { get { return Language.Translate("Out of shelf - preparing for driver"); } }
        public static string SelectScanner { get { return Language.Translate("Select scanner"); } }
        public static string AutomaticKeyLock { get { return Language.Translate("Automatic KeyLock"); } }

        // Error messages
        public static string PackageTypeRequired { get { return Language.Translate("At least one package-type item is required"); } }
        public static string ItemSelectedMultipleTimes { get { return Language.Translate(" {0} {1} has been selected multiple times"); } }
        public static string SignatureClarityMessage { get { return Language.Translate("The signature must be large and clear"); } }
        public static string NameStrength { get { return Language.Translate("Name must be min 2, max 28 characters. Please try again"); } }
        public static string NameRequired { get { return Language.Translate("Name must be filled in"); } }
        public static string IllegalBarcode { get { return Language.Translate("Illegal barcode. Invalid format or checksum"); } }
        public static string ScanningConsignmentNotAuthorized { get { return Language.Translate("User is not authorized for scanning consignments"); } }
        public static string IllegalConsignment { get { return Language.Translate("Illegal consignment '{0}': Wrong type or invalid checksum. Please try again!"); } }
        public static string SavePhotoError { get { return Language.Translate("Problem occured when saving photo"); } }
        public static string CameraInitializeError { get { return Language.Translate("Problem occured when Initializing the Camera"); } }
        public static string InvalidPowerUnitMessage { get { return Language.Translate("User has put in Power Unit {0} which does not exist in {1}"); } }
        public static string InvalidBookingNumber { get { return Language.Translate("Booking number is not valid!"); } }
        public static string InvalidLocation { get { return Language.Translate("Invalid Location Id"); } }
        public static string LocationNotFound { get { return Language.Translate("Location Id could not be found in the unit name inventory. Please wait until the table have been properly synchronized.Current progress"); } }
        public static string NotHavingValidProfile { get { return Language.Translate("Not connected to server, nor having valid local profile"); } }
        public static string ProfileRoleEmpty { get { return Language.Translate("Invalid profile"); } }
        public static string PickupMultipleConsignmentsUnauthorized { get { return Language.Translate("Pick-up of multiple consignments not authorized"); } }
        public static string InvalidPowerUnitId { get { return Language.Translate("The Power unit/load carrier id is not valid.Please enter a new one"); } }
        public static string InvalidUserId { get { return Language.Translate("Invalid user Id.\n User not known in TMS.You will not recive assignment from the TMS.Contact help desk"); } }
        public static string OfflineProfile { get { return Language.Translate("The terminal cannot connect to the TMS.\n The terminal will try to reconnect  with the system automatically"); } }
        public static string FailedSavingScanning { get { return Language.Translate("Failed to store scanned scanning"); } }
        public static string IllegalAddress { get { return Language.Translate("Illegal address detected in plan"); } }
        public static string LoadListError { get { return Language.Translate("Load list could not be retrieved due to error"); } }
        public static string PasswordExpired { get { return Language.Translate("Your password is expired"); } }
        public static string ErrorWhileScanning { get { return Language.Translate("Unspecific error situation encountered while scanning barcode"); } }
        public static string ErrorDriverAvailability { get { return Language.Translate("Error code {0} reported from DriverAvailability: {1}"); } }
        public static string SyncronousErrorDriverAvailability { get { return Language.Translate("Syncronous request to DriverAvailability failed to execute"); } }
        public static string MissingConsignmentId { get { return Language.Translate("Could not process consignment item {0} as consignment id is missing"); } }
        public static string FailedOverrideAsset { get { return Language.Translate("Failed to override asset {0}. Error-code: {1}"); } }
        public static string AssetAlreadyTaken { get { return Language.Translate("Asset {0} already taken"); } }
        public static string LoginToTmsFailed { get { return Language.Translate("Failed to login to TMS. It is recomended to try to login and out to reedem this situation"); } }
        public static string DriverAvailaibiltyNoOfflineLogin { get { return Language.Translate("Failed to login to TMS as PDA is offline"); } }
        public static string NoScannerAvailable { get { return Language.Translate("No scanner object available"); } }
        public static string ScannerActivationFailed { get { return Language.Translate("Failed to activate scanner"); } }
        public static string ScannedItemStopped { get { return Language.Translate("Item {0} has been stopped. Stop code: {1}"); } }
        public static string ActorBlocked { get { return Language.Translate("Item {0} can not be processed as actor {1} is blocked"); } }

        //Warning messages
        public static string ConsignmentItemCantZero { get { return Language.Translate("Number of consignment items can not be 0"); } }
        public static string PhotoCancelled { get { return Language.Translate("Photo cancelled"); } }
        public static string NoPhotoAdded { get { return Language.Translate("No photo added"); } }
        public static string PasswordSoonExpired { get { return Language.Translate("Your password will expire in {0} days.\n\nDo you want to change now?"); } }
        public static string WarningNoTmsConnected { get { return Language.Translate("You are not connected to any TMS and will not receive new operation lists"); } }
        public static string InactiveUserLoggedOut { get { return Language.Translate("User logged out due to inactivity"); } }
        public static string DoYouWantFotToDelete { get { return Language.Translate("Do you want FOT to remove the items from your list of deliveries? "); } }




        // Prompts
        public static string EnterTemprature { get { return Language.Translate("Enter temperature"); } }
        public static string ValidateTemprature { get { return Language.Translate("Enter valid temperature"); } }
        public static string EnterBarcode { get { return Language.Translate("Enter barcode"); } }
        public static string EnterPowerUnitId { get { return Language.Translate("Please enter Power Unit Id"); } }
        public static string SelectTmsAffilationValue { get { return Language.Translate("Please select TMS Affiliation Value"); } }
        public static string EnterCustomerNumber { get { return Language.Translate("Enter customer number"); } }
        public static string EnterBookingNumber { get { return Language.Translate("Enter booking number"); } }
        public static string LocationId { get { return Language.Translate("Please enter location id"); } }
        public static string InvalidLocationId { get { return Language.Translate("Please enter a valid location id"); } }

        //New properties for Label 
        public static string GoodsHandled { get { return Language.Translate("goods handled"); } }
        public static string SelectEquipment { get { return Language.Translate("<Select equipment>"); } }
        public static string PlannedOperation { get { return Language.Translate("Planned Operation"); } }
        public static string SelectPlannedOperation { get { return Language.Translate("Please select planned operation"); } }
        public static string PositionModule { get { return Language.Translate("Position module"); } }
        public static string ErrorPositionModule { get { return Language.Translate("Error while retreiving position module"); } }
        public static string Desktop { get { return Language.Translate("Desktop"); } }
        public static string Lan { get { return Language.Translate("LAN"); } }
        public static string Wlan { get { return Language.Translate("WLAN"); } }
        public static string Address { get { return Language.Translate("Address"); } }
        public static string ConnectionStatus { get { return Language.Translate("Connected to"); } }
        public static string TmiInformation { get { return Language.Translate("TMI Does not accept positions"); } }
        public static string TmiInformationMessage { get { return Language.Translate("The current TMI does not accept positioning, and therefore you cannot change the GPS status"); } }
        public static string SettingModule { get { return Language.Translate("Settings module"); } }
        public static string ErrorInSettingModule { get { return Language.Translate("Error while retreiving settings module"); } }
        public static string To { get { return Language.Translate("to"); } }
        public static string Switching { get { return Language.Translate("Switching"); } }
        public static string LocationError { get { return Language.Translate("Location error"); } }
        public static string LocationIdError { get { return Language.Translate("Location Id could not be found in the unit name inventory. Please wait until the table have been properly synchronized. Current progress"); } }
        public static string IsRegistered { get { return Language.Translate("is registered"); } }
        public static string PendingOperations { get { return Language.Translate("There are operations present in your Operation list"); } }
        public static string GoodsAvailabityError { get { return Language.Translate("No loaded goods available due to timeout/an error"); } }
        public static string GoodsAvailabityErrorPowerunit { get { return Language.Translate("Currently no loaded goods are available for your power unit"); } }
        public static string GoodsAvailabityErrorRouteId { get { return Language.Translate("Currently no loaded goods are available for your route id"); } }

        public static string CauseNotSet { get { return Language.Translate("Measure is not set"); } }
        public static string ItemAlreadyDamage { get { return Language.Translate("Item is already registered as damage"); } }
        public static string Placement { get { return Language.Translate("Placement"); } }
        public static string Weight { get { return Language.Translate("Weight"); } }
        public static string Volume { get { return Language.Translate("Volume"); } }
        public static string OperationList { get { return Language.Translate("OperationList"); } }
        public static string Warning { get { return Language.Translate("Warning"); } }
        public static string PasswordCanNotChange { get { return Language.Translate("Password cannot be changed more than once per day"); } }
        public static string PasswordChange { get { return Language.Translate("Password change"); } }
        public static string LogonError { get { return Language.Translate("Could not dis/connect syncronously with driver availaibilty"); } }
        public static string WarningOfSlowProcess { get { return Language.Translate("Turning on logging of debug information will slow down the application"); } }
        public static string LogOnFromTmMessage { get { return Language.Translate("You are currently logged on with asset {0} to {1} do you want to log off?"); } }
        public static string LogOffFromTmMessage { get { return Language.Translate("You are currently logged off with asset {0} to {1} do you want to log on?"); } }
        public static string NoUnloadingOperations { get { return Language.Translate("You have no unloading operations in your operation list. Please verify Power Unit ID"); } }
        public static string NoUnloadingOperationsForPowerunit { get { return Language.Translate("There are no available unloading operations for your power unit"); } }
        public static string PreComIsNotConnected { get { return Language.Translate("Password can be changed only when PreCom is connected"); } }
        public static string PleaseEnter { get { return Language.Translate("Please enter"); } }
        public static string AppendCurrentPassword { get { return Language.Translate("current password"); } }
        public static string AppendNewPassword { get { return Language.Translate("new password"); } }
        public static string AppendConfirmPassword { get { return Language.Translate("confirm password"); } }
        public static string PasswordDoesNotMatch { get { return Language.Translate("New password and confirm password is not equal"); } }
        public static string PasswordDoesNotMatchPolicy { get { return Language.Translate("New password is mismatch with the password policy"); } }
        public static string SelectMeasureDescription { get { return Language.Translate("Select measure description"); } }
        public static string SelectCauseDescription { get { return Language.Translate("Select cause description"); } }
        public static string SelectAtLeastOneItem { get { return Language.Translate("Please select at least one item from list"); } }
        public static string NoItemInList { get { return Language.Translate("No item in list for selection"); } }
        public static string PhotoLimitOver { get { return Language.Translate("Your photo capture limit is over"); } }
        public static string EnterItemCount { get { return Language.Translate("Enter item count"); } }
        public static string ReconcilliationCancelled { get { return Language.Translate("Reconcilliation canceled"); } }
        public static string Qty { get { return Language.Translate("Qty"); } }
        public static string Consignor { get { return Language.Translate("Consignor"); } }
        public static string DamageCodeNotSet { get { return Language.Translate("Damage code is not set"); } }
        public static string MeasureNotSet { get { return Language.Translate("Measure is not set"); } }
        public static string Type { get { return Language.Translate("Type"); } }
        public static string ConfirmDamageFor { get { return Language.Translate("Confirm damage for"); } }
        public static string TempratureRegisterd { get { return Language.Translate("Temperature registered"); } }
        public static string RegisterTempratureCancelled { get { return Language.Translate("Register temperature cancelled"); } }
        public static string WrongCarrierFormat { get { return Language.Translate("Wrong carrier format. Please enter a valid carrier"); } }
        public static string AddCarrierAndSelectOperation { get { return Language.Translate("Add new carrier and select operation type you wish to start"); } }
        public static string SignatureCancelled { get { return Language.Translate("Signature cancelled"); } }
        public static string UserInformation { get { return Language.Translate("User Information"); } }
        public static string WorkContactDetail { get { return Language.Translate("Work Contact Detail"); } }
        public static string ReconcilliationAborted { get { return Language.Translate("Reconcilliation aborted"); } }
        public static string InvalidInput { get { return Language.Translate("Invalid Input"); } }
        public static string InvalidInputMessage { get { return Language.Translate("Please enter valid input"); } }
        public static string ReconcilliationListHeading { get { return Language.Translate("S/P/LM"); } }
        public static string ConsignmentItems { get { return Language.Translate("Consignment Items"); } }
        public static string Status { get { return Language.Translate("Status"); } }
        public static string NoConsignmentCodesSynced { get { return Language.Translate("No consignmentEntity codes synced"); } }
        public static string NoCauseMeasureSynced { get { return Language.Translate("No cause/measure synced"); } }
        public static string DamageInformationNotSynced { get { return Language.Translate("Damage information not synced"); } }
        public static string DriverInstruction { get { return Language.Translate("Driver instruction"); } }
        public static string NumberOfItems { get { return Language.Translate("Number of items"); } }
        public static string LastProductionEvent { get { return Language.Translate("Last production event"); } }
        public static string EarlierEventsFor { get { return Language.Translate("Earlier events for"); } }
        public static string Name { get { return Language.Translate("Name"); } }
        public static string Damage { get { return Language.Translate("Damage"); } }
        public static string RegisterDamageCancelled { get { return Language.Translate("Register damage cancelled"); } }
        public static string DamageRegistered { get { return Language.Translate("Damage registered"); } }
        public static string Data { get { return Language.Translate("Data"); } }
        public static string PrCon { get { return Language.Translate("Pr con"); } }
        public static string WkCon { get { return Language.Translate("Wk Con"); } }
        public static string Sett { get { return Language.Translate("Sett"); } }
        public static string Groups { get { return Language.Translate("Groups"); } }
        public static string Group { get { return Language.Translate("Group"); } }
        public static string Value { get { return Language.Translate("Value"); } }

        public static string FirstName { get { return Language.Translate("First name"); } }

        public static string MiddleName { get { return Language.Translate("Middle name"); } }
        public static string LastName { get { return Language.Translate("Last name"); } }
        public static string PersonId { get { return Language.Translate("Person id"); } }
        public static string IsEmployee { get { return Language.Translate("Is Employee"); } }
        public static string EmployeeNumber { get { return Language.Translate("Employee Number"); } }
        public static string IsConsultant { get { return Language.Translate("Is Consultant"); } }
        public static string ConsultantNumber { get { return Language.Translate("Consultant Number"); } }
        public static string UserText { get { return Language.Translate("User Text"); } }
        public static string Organization { get { return Language.Translate("Organization"); } }
        public static string ManagerId { get { return Language.Translate("Manager Id"); } }
        public static string StartDate { get { return Language.Translate("Start Date"); } }
        public static string EndDate { get { return Language.Translate("End Date"); } }
        public static string DeprovisioningDate { get { return Language.Translate("Deprovisioning Date"); } }
        public static string OrganizationUnitId { get { return Language.Translate("Organization Unit Id"); } }
        public static string OrganizationUserId { get { return Language.Translate("Organization User Id"); } }
        public static string DivisionName { get { return Language.Translate("Division Name"); } }
        public static string ExternalStatus { get { return Language.Translate("External Status"); } }
        public static string JobFunction { get { return Language.Translate("Job Function"); } }
        public static string IsManager { get { return Language.Translate("Is Manager"); } }
        public static string IsProxy { get { return Language.Translate("Is Proxy"); } }
        public static string LanguageTxt { get { return Language.Translate("Language"); } }
        public static string Style { get { return Language.Translate("Style"); } }
        public static string AddressLine { get { return Language.Translate("Address Line"); } }
        public static string Region { get { return Language.Translate("Region"); } }
        public static string ZipCode { get { return Language.Translate("Zip Code"); } }
        public static string City { get { return Language.Translate("City"); } }
        public static string Country { get { return Language.Translate("Country"); } }
        public static string Phone { get { return Language.Translate("Phone"); } }
        public static string Mobile { get { return Language.Translate("Mobile"); } }
        public static string Email { get { return Language.Translate("Email"); } }
        public static string EnterNumber { get { return Language.Translate("Enter number"); } }
        public static string TransportEquipmentType { get { return Language.Translate("Transport-equipment type"); } }

        public static string PackageType { get { return Language.Translate("Package-type"); } }

        public static string TeleNo { get { return Language.Translate("Tele. No"); } }

        public static string UnplannedConsignmentNotVerified { get { return Language.Translate("Unplanned consignment not verified"); } }

        public static string SwitchingGprsToLan { get { return Language.Translate("Switching GPRS to LAN"); } }
        public static string SwitchingWlanToLan { get { return Language.Translate("Switching WLAN to LAN"); } }
        public static string SwitchingDesktopToLan { get { return Language.Translate("Switching Desktop to LAN"); } }
        public static string SwitchingGprsToDesktop { get { return Language.Translate("Switching GPRS to Desktop"); } }
        public static string SwitchingLanToDesktop { get { return Language.Translate("Switching LAN to Desktop"); } }
        public static string SwitchingWlanToDesktop { get { return Language.Translate("Switching WLAN to Desktop"); } }
        public static string SwitchingGprsToWlan { get { return Language.Translate("Switching GPRS to WLAN"); } }
        public static string SwitchingLanToWlan { get { return Language.Translate("Switching LAN to WLAN"); } }
        public static string SwitchingDesktopToWlan { get { return Language.Translate("Switching Desktop to WLAN"); } }
        public static string SwitchingLanToGprs { get { return Language.Translate("Switching LAN to GPRS"); } }
        public static string SwitchingDeskopToGprs { get { return Language.Translate("Switching Deskop to GPRS"); } }
        public static string SwitchingWlanTo3G { get { return Language.Translate("Switching WLAN to 3G"); } }
        public static string SwitchingLanTo3G { get { return Language.Translate("Switching LAN to 3G"); } }
        public static string SwitchingWlanToGprs { get { return Language.Translate("Switching WLAN to GPRS"); } }
        public static string SwitchingDesktopTo3G { get { return Language.Translate("Switching Desktop to 3G"); } }
        public static string BookingRef { get { return Language.Translate("Booking ref"); } }

        public static string ConsignmentOrItem { get { return Language.Translate("Consignment(item)"); } }
        public static string ProductCategory { get { return Language.Translate("Product Category"); } }
        public static string OrderNumber { get { return Language.Translate("Order Number"); } }
        public static string TimeFrame { get { return Language.Translate("TimeFrame"); } }
        public static string EnterLoadCarrier { get { return Language.Translate("Enter Load Carrier"); } }
        public static string FullName { get { return Language.Translate("Full Name"); } }
        public static string InvalidPowerUnit { get { return Language.Translate("Invalid Power Unit"); } }
        public static string ChangePowerUnit { get { return Language.Translate("You are logged in by invalid Power Unit.Please change your Power Unit"); } }
        public static string InvalidUser { get { return Language.Translate("Invalid User"); } }
        public static string InvalidUserInTm { get { return Language.Translate("User '{0}' is not a valid user in TM"); } }

        public static string RequestGoodsListResponse { get { return Language.Translate("Goods List Response"); } }
        public static string GoodsNotReturnedFromLm { get { return Language.Translate("No Stops/Trips available from LM"); } }
        public static string SelectCauseAndMeasure { get { return Language.Translate("Select Cause and Measure"); } }
        public static string OperationId { get { return Language.Translate("OperationId"); } }
        public static string LogDebugInformation { get { return Language.Translate("Log debug information"); } }
        public static string Vas { get { return Language.Translate("Vas"); } }

        public static string ConsignmentItemUnknownInLm { get { return Language.Translate("Consignment item number unknown in LM, scan consignment number"); } }
        public static string Terminal { get { return Language.Translate("Terminal"); } }
        public static string TerminalAddress { get { return Language.Translate("Terminal Address"); } }
        public static string TerminalPostNumber { get { return Language.Translate("Terminal Post Number"); } }
        public static string TerminalCity { get { return Language.Translate("Terminal City"); } }
        public static string PackagetypeSlashTransportEquipment { get { return Language.Translate("Package type / Transport equipment"); } }
        public static string Registertemprature { get { return Language.Translate("Register temperature"); } }
        public static string Registerdamage { get { return Language.Translate("Register damage"); } }
        public static string DeleteCapturePhotos { get { return Language.Translate("Do you want to delete all pictures for the event?"); } }
        public static string Enabled { get { return Language.Translate("Enable"); } }
        public static string Disabled { get { return Language.Translate("Disabled"); } }
        public static string GpsStatus { get { return Language.Translate("GPS Status"); } }
        public static string Altitude { get { return Language.Translate("Altitude"); } }
        public static string Latitude { get { return Language.Translate("Latitude"); } }
        public static string Position { get { return Language.Translate("Position"); } }
        public static string Satellites { get { return Language.Translate("Satellites"); } }
        public static string EnableDisableGps { get { return Language.Translate("Enable or disable the GPS"); } }


        public static string AlterProductGroup { get { return Language.Translate("Alter Product Group"); } }
        public static string ChooseProductGroup { get { return Language.Translate("Choose Product Group"); } }
        public static string ProductGroupChanged { get { return Language.Translate("Product Group changed from {0} to {1} for consignment {2}"); } }
        public static string ProductGroupChangeRegistered { get { return Language.Translate("Product Group change is registered for {0}"); } }

        public static string AcceptChange { get { return Language.Translate("Accept Change"); } }
        public static string DoesNotContainLegalProductCategories { get { return Language.Translate("Product Group does not have legal product categories"); } }
        public static string Version { get { return Language.Translate("Version"); } }
        public static string ChangeProductGroupAborted { get { return Language.Translate("Change Product Group Aborted"); } }
        public static string Height { get { return Language.Translate("Height"); } }
        public static string Width { get { return Language.Translate("Width"); } }
        public static string Length { get { return Language.Translate("Length"); } }
        public static string Skip { get { return Language.Translate("Skip"); } }
        public static string Print { get { return Language.Translate("Print"); } }
        public static string NewRegistration { get { return Language.Translate("New Registration"); } }
        public static string ScanPdf417 { get { return Language.Translate("Scan PDF417(or consignment number)"); } }
        public static string UnknownConsignmentScanPdf { get { return Language.Translate("Unknown consignment information for item {0}. Scan PDF to register consignment information"); } }
        public static string ItemRegisteredWithoutConsignmentInformation { get { return Language.Translate("Item {0} is registered without consignment information"); } }

        public static string NewNumberOfItemsWillBeSet { get { return Language.Translate("New number of items will be set to {0}. Click Back if not Ok to accept. "); } }
        public static string LoadLineHaul { get { return Language.Translate("Load line haul"); } }
        public static string LoadDistributionTruck { get { return Language.Translate("Load distribution truck"); } }
        public static string UnloadLineHaul { get { return Language.Translate("Unload line haul"); } }
        public static string UnloadPickUpTruck { get { return Language.Translate("Unload pickup truck"); } }
        public static string LoadOrActivateLoadCarrier { get { return Language.Translate("Do you want to load the carrier or do you want to activate for loading?"); } }
        public static string UnloadOrActivateLoadCarrier { get { return Language.Translate("Do you want to unload the carrier or you want to activate for unloading?"); } }
        public static string Corectwvfor { get { return Language.Translate("Correct w/v for"); } }
        public static string NumberOfItemsInShipment { get { return Language.Translate("Number of items in shipment "); } }
        public static string Connect { get { return Language.Translate("Connect"); } }
        public static string ScanOrEnterLogLoadCarrierForAssembly { get { return Language.Translate("Scan or enter log.load carrier for assembly"); } }
        public static string LoadCarrierScanAborted { get { return Language.Translate("Scanning of carrier aborted"); } }
        public static string StopIdLabel { get { return Language.Translate("Stop Id"); } }
        public static string CarrierMoveId { get { return Language.Translate("Carrier Move Id"); } }
        public static string UseExternalScanner { get { return Language.Translate("Use external scanner"); } }
        public static string ChangeRole { get { return Language.Translate("Change role"); } }
        public static string ActiveRole { get { return Language.Translate("Active role"); } }
        public static string InvalidSessionId { get { return Language.Translate("Tried to use invalid session id '{0}' for driver availiability"); } }

        public static string Reason { get { return Language.Translate("Reason"); } }
        public static string Action { get { return Language.Translate("Action"); } }
        public static string SearchTextAction { get { return Language.Translate("Enter search text. Choose action and"); } }
        public static string SearchTextReason { get { return Language.Translate("Enter searchtext. Choose a reason and"); } }
        public static string PressOkEnter { get { return Language.Translate("press Ok/Enter"); } }
        public static string ScanOrEnterItemOrCarrier { get { return Language.Translate("Scan/enter item and carrier id"); } }
        public static string FreeText { get { return Language.Translate("Free Text"); } }
        public static string ItemAlreadyRegisterd { get { return Language.Translate(" Item is already registered as General Deviation"); } }
        public static string ItemAlreadyRegistered { get { return Language.Translate(" Item is already registered"); } }


        public static string ProductGroupNotFound { get { return Language.Translate("Product Group Not Found"); } }
        public static string InvalidItem { get { return Language.Translate("is an invalid item"); } }
        public static string GeneralDeviationCancelled { get { return Language.Translate("General Deviation cancelled"); } }
        public static string GeneralDeviationCompleted { get { return Language.Translate("General Deviation completed"); } }
        public static string Loadcarrier { get { return Language.Translate("Loadcarrier"); } }
        public static string InvalidLoadCarrier { get { return Language.Translate("is an invalid load carrier"); } }
        public static string InvalidConsignmentItem { get { return Language.Translate("is an invalid number"); } }
        public static string ItemUnloaded { get { return Language.Translate("Item {0} Unloaded"); } }
        public static string ActiveShelf { get { return Language.Translate("Active Shelf"); } }
        public static string NoOfItems { get { return Language.Translate("No. Items"); } }
        public static string ScanItemOrShelf { get { return Language.Translate("Scan/enter consignment item or new shelf"); } }
        public static string ScanItemOrPlacement { get { return Language.Translate("Scan/enter item or new placement"); } }
        public static string Complete { get { return Language.Translate("Complete"); } }


        public static string ScanConsignmentItemOrDeliveryCode { get { return Language.Translate("Scan/Enter Consignment Item or Delivery Code"); } }
        public static string Pckg { get { return Language.Translate("Pckg"); } }
        public static string Recipient { get { return Language.Translate("Recipient"); } }
        public static string Town { get { return Language.Translate("Town"); } }
        public static string ScanPdfOrConsignmentNumberMessage { get { return Language.Translate("Please scan a consignment number or a PDF417 Waybill"); } }
        public static string ConsignmentItemRegisteredWithoutConsignment { get { return Language.Translate("Item {0} is registered without consignment information"); } }
        public static string ScanConsignmentItemMessage { get { return Language.Translate("Please scan a consignment item"); } }
        public static string OfflineOrTimeout { get { return Language.Translate("Offline or Timeout. No response from LM. Try again later "); } }
        public static string ConsignmentItemConnectConsignemnt { get { return Language.Translate("Consignment item {0} has been connected to consignment {1}"); } }
        public static string ConsignmentActivated { get { return Language.Translate("Consignment {0} is activated"); } }
        public static string ScanConsignmentItem { get { return Language.Translate("Scan or enter consignment item"); } }
        public static string ScanPdfOrConsignmentNumber { get { return Language.Translate("Scan Consignment or PDF417 WayBill"); } }

        public static string ScanConsignmentOrLoadCarrier { get { return Language.Translate("Scan or enter Cons. item or log. load carrier"); } }
        public static string ScanItemOrPdf { get { return Language.Translate("Scan or enter item ID/PDF417"); } }
        public static string ItemIsDeleted { get { return Language.Translate("Item {0} deleted"); } }
        public static string ScanEnterLocationId { get { return Language.Translate("Scan/Enter Location Id"); } }
        public static string DeliveredToAnotherLocations { get { return Language.Translate("Delivered To Another Location"); } }
        public static string DeliveryToAnotherLocation { get { return Language.Translate("Delivered to another location"); } }
        public static string ConfirmDelWantToDelete { get { return Language.Translate("Confirm del. Want to delete  item"); } }
        public static string NoShelfLocationInLm { get { return Language.Translate("No Shelf location in LM"); } }
        public static string PleaseScanValidOrgUnit { get { return Language.Translate("Please scan valid OrgUnit Id"); } }
        public static string DeliveredToPoPibByCustomer { get { return Language.Translate("Delivered to PO/PiB by customer"); } }
        public static string DeliveryCodeScanned { get { return Language.Translate("Delivery code scanned"); } }
        public static string Activate { get { return Language.Translate("Activate"); } }
        public static string LoadCarrierAlreadyScanned { get { return Language.Translate("Load carrier already Scanned."); } }

        public static string LoadCarrierRegistered { get { return Language.Translate("load carrier {0} is registered into shelf {1}"); } }

        public static string Above25 { get { return Language.Translate("Above 25"); } }
        public static string Below25 { get { return Language.Translate("Below 25"); } }
        public static string TooYoung { get { return Language.Translate("Too young"); } }
        public static string DeliveryToConsignee { get { return Language.Translate("Delivery to Consignee"); } }
        public static string Next { get { return Language.Translate("Next"); } }
        public static string Previous { get { return Language.Translate("Previous"); } }
        public static string SignOffAppliesTo { get { return Language.Translate("Sign off applies to: consignment item and contract signed with the supplier: Do you want to continue?"); } }
        public static string ConsignmentRefusedByConsignee { get { return Language.Translate("The following consignment items are registered as refused by the consignee. The items shall not be delivered. Do you want to continue?"); } }
        public static string HeaderDelivery { get { return Language.Translate("Delivery"); } }
        public static string ItemRegisteredOutOfHolding { get { return Language.Translate("Item {0} is registered out of holding"); } }
        public static string LoadCarrierRegisteredOutOfHolding { get { return Language.Translate("load carrier {0} is registered out of holding"); } }
        public static string ScanOrEnterConsignmentItem { get { return Language.Translate("Scan or enter consignment item"); } }
        public static string CanNotOpenDeviationForLoadCarrier { get { return Language.Translate("Can not open deviation menu for load carrier"); } }
        public static string NoItemScanned { get { return Language.Translate("No item scanned!"); } }
        public static string ShelfNotEntered { get { return Language.Translate("Shelf not entered!"); } }
        public static string ItemLoadCarrierOrShelfNotEntered { get { return Language.Translate("Consignment item, Shelf or Load Carrier not entered!"); } }
        public static string RegisterTimeConsumption { get { return Language.Translate("Register time consumption"); } }
        public static string NoTimeConsumptionSelected { get { return Language.Translate("Please select time from list"); } }
        public static string ArrivedAtCustomerWareHouse { get { return Language.Translate("Arrived At Customer WareHouse"); } }
        public static string PleaseEnterConsignmentItemOrDeliveryCode { get { return Language.Translate("Please enter consignment item number or delivery code"); } }
        public static string ShelfLocationIsDifferent { get { return Language.Translate("The shelf location is different from org unit id {0} - {1}"); } }

        public static string HandedInAtTerminal { get { return Language.Translate("Handed in at Terminal"); } }
        public static string AcceptNoConnection { get { return Language.Translate("Accept no connection"); } }
        public static string ScanConsignmentOrPdf417 { get { return Language.Translate("Scan or enter consignment or PDF417"); } }
        public static string PleaseUsePos { get { return Language.Translate("Offline : Please use POS in Post office"); } }
        public static string HasNoConsignmentInformation { get { return Language.Translate("has no consignment information"); } }
        public static string Item { get { return Language.Translate("Item"); } }
        public static string CreateConnection { get { return Language.Translate("Create connection"); } }
        public static string CorrectCountOfItems { get { return Language.Translate("Correct Number of items in consignment"); } }
        public static string ConsignmentNumber { get { return Language.Translate("Consignment Number"); } }
        public static string EnterValidNumberOfItems { get { return Language.Translate("Please enter valid number of items"); } }
        public static string NoOfItemsScanned { get { return Language.Translate("Number of items scanned"); } }
        public static string CorrectNumberOfItems { get { return Language.Translate("Correct Number of Items"); } }
        public static string NumberOfItemsInConsignment { get { return Language.Translate("Number of Items in Consignment"); } }
        public static string ItemAlreadyRegisteredNotDelivered { get { return Language.Translate("Item is already registered as Not Delivered"); } }
        public static string LoadCarrierAlreadyRegistered { get { return Language.Translate("Loadcarrier  {0} already registered"); } }
        public static string ScanRbtOrEnterPowerUnit { get { return Language.Translate("Scan R/B/T label or enter power unit id"); } }
        public static string PleaseEnterPowerunit { get { return Language.Translate("Please enter powerunit"); } }
        public static string NewAction { get { return Language.Translate("New Action"); } }
        public static string PleaseEnterItemOrLoadCarrier { get { return Language.Translate("Please enter consignment item or loadcarrier"); } }
        public static string ChooseOrgUnit { get { return Language.Translate("Choose Org. Unit"); } }
        public static string ConsigneeNameLength { get { return Language.Translate("Consignee Name should be between 2 to 28 characters"); } }
        public static string IdDetailsLength { get { return Language.Translate("Id Details should be 2 to 12 characters"); } }
        public static string New { get { return Language.Translate("New"); } }
        public static string Registration { get { return Language.Translate("Registration"); } }
        public static string SelectReasonValue { get { return Language.Translate("Selected reason value"); } }
        public static string SelectedActionValue { get { return Language.Translate("Selected action value"); } }
        public static string CancelTryOrProcessOffline { get { return Language.Translate("You can cancel,try again or proceed in offline mode."); } }
        public static string WishToKeepExistingConnection { get { return Language.Translate("Wish to keep existing connection {0}or create New ?"); } }
        public static string PhysicalCarrierAlreadyDisconnected { get { return Language.Translate("Physical carrier already disconnected"); } }
        public static string Destination { get { return Language.Translate("Destination {0}"); } }
        public static string NewConnection { get { return Language.Translate("New {0}Connection"); } }
        public static string OldConnection { get { return Language.Translate("Old Connection"); } }
        public static string KeepOld { get { return Language.Translate("Keep Old"); } }
        public static string CanPrint { get { return Language.Translate("Only Carrier with postal service code major client and crossdocking may print"); } }
        public static string PrintSuccessful { get { return Language.Translate("Print or mail sucessful"); } }
        public static string PrintError { get { return Language.Translate("Error while sending printer request"); } }
        public static string NoPrinterFor { get { return Language.Translate("No Printers for {0}"); } }
        public static string PrintVoucher { get { return Language.Translate("Print voucher"); } }
        public static string SelectPrinterFromlist { get { return Language.Translate("Select Printer in list"); } }
        public static string ChangePrinter { get { return Language.Translate("Change {0}Printer"); } }
        public static string ActivateCarierForPrinting { get { return Language.Translate("Activate carier for printing"); } }
        public static string Printer { get { return Language.Translate("Printer"); } }
        public static string ScanLoadCarrierAndTripId { get { return Language.Translate("Scan or enter log. load carrier and Trip id"); } }
        public static string OrgUnit { get { return Language.Translate("Org. Unit"); } }
        public static string ItemHandledInPostOffice { get { return Language.Translate("item  {0} was handed in at Post office"); } }
        public static string AlertConsignmentItem { get { return Language.Translate("Please scan/enter consignment item no."); } }
        public static string ShelfIdIsActivated { get { return Language.Translate("Shelf {0} is activated"); } }
        public static string ConsignmentItemWasMovedToShelf { get { return Language.Translate("{0} {1} was moved to new shelf {2}"); } }
        public static string ConsignmentItemRegisteredInToShelf { get { return Language.Translate("{0} {1} is registered into shelf {2}"); } }
        public static string UseArrivalRegistrationToRegisterGoods { get { return Language.Translate("Use the Arrival Registration to register goods that has arrived and to register change of shelf location"); } }
        public static string ScanOrEnterShelf { get { return Language.Translate("Scan or enter shelf id"); } }
        public static string NoReplyFromServer { get { return Language.Translate("No reply from server"); } }
        public static string PlaceIsActivated { get { return Language.Translate("Place  {0} is activated"); } }
        public static string IsRegisteredIntoPlace { get { return Language.Translate("is registered into place"); } }
        public static string ScanOrEnterGoodsPlacement { get { return Language.Translate("Scan or enter goods placement"); } }
        public static string CarrierRemovedFromWorkList { get { return Language.Translate("Carrier removed from Work List"); } }
        public static string ConfirmDelete { get { return Language.Translate("Confirm delete:\r\nWant to delete item\r\n"); } }
        public static string ChangeOrgUnitAborted { get { return Language.Translate("Change of org. unit aborted."); } }
        public static string ValidRbtScanned { get { return Language.Translate("Valid rbt scanned!"); } }
        public static string ChangeOrgUnit { get { return Language.Translate("Change\r\n Org. Unit"); } }
        public static string LoadCombTrip { get { return Language.Translate("Load Comb. trip"); } }
        public static string RegisterDestTrip { get { return Language.Translate("Register distr. (trip1)"); } }
        public static string ScanItemOrNewActiveTrip { get { return Language.Translate("Scan item or new active trip"); } }
        public static string CarrList { get { return Language.Translate("Carr.List"); } }
        public static string NewTrip { get { return Language.Translate("New Trip"); } }
        public static string Carrier { get { return Language.Translate("Carrier"); } }
        public static string Loaded { get { return Language.Translate("loaded"); } }
        public static string IsActivated { get { return Language.Translate("is activated."); } }
        public static string Route { get { return Language.Translate("Route"); } }
        public static string ScanningRbtAborted { get { return Language.Translate("Scanning of RBT aborted."); } }
        public static string TransportLoadCarrierSaved { get { return Language.Translate("Transport/Load carrier saved"); } }
        public static string ItemIsAlreadyLoadedTo { get { return Language.Translate("Item is already loaded on to {0}"); } }
        public static string ScanOrEnterTransportCarrierId { get { return Language.Translate("Scan or enter Transport Carrier Id"); } }
        public static string ScanningLoadCarrierRbtAborted { get { return Language.Translate("Scanning of load carrier/Rbt aborted."); } }
        public static string ScanOrEnterConsignmentItemTrip { get { return Language.Translate("Scan or enter consignment item number or trip"); } }
        public static string CarrierId { get { return Language.Translate("Carrier id"); } }
        public static string LoadedItems { get { return Language.Translate("Loaded items"); } }
        public static string NotAvailableShortForm { get { return Language.Translate("N/A"); } }
        public static string AlterWeightVol { get { return Language.Translate("Alter weight/vol"); } }
        public static string ManualInput { get { return Language.Translate("Manual input"); } }
        public static string Cancelled { get { return Language.Translate("Cancelled"); } }
        public static string PleaseEnterValidCount { get { return Language.Translate("Please Enter Valid Consignment Item Count"); } }
        public static string PleaseEnterValidScannedCount { get { return Language.Translate("Please Enter Scanned Consignment Item Count"); } }
        public static string DeliveryByCustomer { get { return Language.Translate("Delivery By Customer"); } }
        public static string PlaceId { get { return Language.Translate("Place Id"); } }
        public static string Shelf { get { return Language.Translate("Shelf"); } }


        public static string IsLoadedOn { get { return Language.Translate("is loaded on"); } }
        public static string IsLoaded { get { return Language.Translate("is loaded"); } }
        public static string IsAlreadyLoaded { get { return Language.Translate("Item is already loaded."); } }
        public static string IsAlreadyLoadedOnTo { get { return Language.Translate("Item is already loaded on to"); } }
        public static string IsUnknown { get { return Language.Translate("is unknown"); } }
        public static string ConsignmentIsUnknown { get { return Language.Translate("Consignment Is Unknown"); } }


        public static string Cons { get { return Language.Translate("Cons"); } }
        public static string LEv { get { return Language.Translate("L.Ev"); } }
        public static string Send { get { return Language.Translate("Send"); } }
        public static string Recp { get { return Language.Translate("Recp"); } }
        public static string WidthVolume { get { return Language.Translate("W/V"); } }
        public static string Blk { get { return Language.Translate("Blk!"); } }
        public static string Plc { get { return Language.Translate("Plc"); } }
        public static string Rtrn { get { return Language.Translate("Rtrn"); } }
        public static string DegrC { get { return Language.Translate("°C"); } }
        public static string Cat { get { return Language.Translate("Cat"); } }
        public static string ItemNotFound { get { return Language.Translate("Item {0} not found"); } }
        public static string NotAvailable { get { return Language.Translate("Not available"); } }
        public static string DateAndTimeFormat { get { return Language.Translate("dd.MM.yyyy HH:mm"); } }

        public static string IdShouldBeOfLength { get { return Language.Translate("Id should be of length between 2 and 28"); } }
        public static string ConsignmentNameLength { get { return Language.Translate("Consignee Name should be of length between 2 and 12"); } }
        public static string PleaseSelectAtLeastOne { get { return Language.Translate("Please check atleast one checkbox"); } }

        public static string ItemCanNotBeDelivered { get { return Language.Translate("Consignment Item shall not be delivered - Stop Order"); } }
        public static string NotAllowedToDeliverLogicalLoadCarrier { get { return Language.Translate("Not Allowed to Deliver Logical Load Carrier"); } }
        public static string AlreadyActivated { get { return Language.Translate("already activated."); } }
        public static string InvalidDeliveryCode { get { return Language.Translate("Invalid Delivery Code."); } }


        public static string IsAllItemsAreScanned { get { return Language.Translate("You have scanned {0} consignment items for consignment {1}. The expected number of consignment items is {2}"); } }
        public static string CameraUpdatedInstruction { get { return Language.Translate("Remember! Use Enter button on PDA\r\n to capture picture"); } }
        public static string CameraDeleteInstruction { get { return Language.Translate("Delete picture?Click on thumbnail"); } }
        public static string CameraDeletePicture { get { return Language.Translate("Delete this Picture?"); } }
    }
}


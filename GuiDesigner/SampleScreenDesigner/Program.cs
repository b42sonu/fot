﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Setting.Views;
using SampleScreenDesigner.Forms;


namespace SampleScreenDesigner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            Application.Run(new FormAlystraPushOperationList());
        }
    }
}
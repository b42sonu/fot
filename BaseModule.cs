﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom;
using PreCom.Core;
using PreCom.Controls;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Application = PreCom.Application;
using Process = Com.Bring.PMP.PreComFW.Shared.Storage.Process;

namespace Com.Bring.PMP.PreComFW.Shared
{
    public abstract class BaseModule : ModuleBase, IForm, IStatusIcon
    {
        public BaseFlow InitialFlow { get; set; }
        public IFlowData InitialFlowData { get; set; }
        public Process InitialFlowProcess { get; set; }
        public static bool IsForceActivate { get; set; }
        public bool DoSupressDefaultFlowForModule { get; set; }
        private bool _isToShowPopup = true;
        /// <summary>
        /// Every module sets this every time module gets activated
        /// </summary>
        public static BaseModule ActiveModule { get; set; }
        private bool _onScreenKeyboardEnabled;
        protected readonly Application Application;
        protected readonly CommunicationBase Communication;
        protected readonly PlatformBase Platform;
        protected readonly SettingsBase Settings;


        protected BaseModule(Application application, CommunicationBase communication, PlatformBase platform, SettingsBase settings)
        {
            Application = application;
            Communication = communication;
            Platform = platform;
            Settings = settings;
        }


        /// <summary>
        /// Event you can subscribe to if you want to know when the module is deactivated
        /// </summary>
        public delegate void DeactivateModuleDelegate();
        public event DeactivateModuleDelegate DeactivateModuleEvent;


        internal class FlowExecutionData
        {
            internal FlowExecutionData(BaseFlow flow, int returnState)
            {
                Flow = flow;
                ReturnState = returnState;
            }

            internal BaseFlow Flow;
            //Each time main flow will
            //start execution for sub flow, it will push a State into this stack, this State will specify
            //from where the execution will start when execution of sub flow will end and When the execution of sub
            //flow will end this stack will have pop command to get State to execute.
            internal int ReturnState;
        }


        //Specifies the stack for flows and sub flows. Each time control will move from
        //one flow to another flow, flow execution data will be pushed and poped into this stack.
        private static Stack<FlowExecutionData> FlowDataStack { get; set; }



        /// <summary>
        /// Specifies the current flow under execution
        /// </summary>
        public static BaseFlow CurrentFlow { get; set; }

        /// <summary>
        ///Specifies the container form for each form/popup in module 
        /// </summary>
        public static readonly BaseForm MainForm = new BaseForm(); // Create imedeiatly so sure to create on correct thread
        /// <summary>
        /// Icon on bottom taskbar for displaying OnScreen Keyboard
        /// </summary>
        private StatusIconItem _onScreenKeyboardIconItem;

        public StatusIconItem OnScreenKeyboardIconItem
        {
            get { return _onScreenKeyboardIconItem; }
            set { _onScreenKeyboardIconItem = value; }
        }

        public static void StartFlow<T>(IFlowData flowData, Process process) where T : BaseFlow, new()
        {
            CurrentFlow = new T { CurrentProcess = process };

            CurrentFlow.InitFlow(flowData);
        }

        public static void StartFlow(BaseFlow flow, IFlowData flowData, Process process)
        {
            CurrentFlow = flow;
            flow.CurrentProcess = process;
            flow.InitFlow(flowData);
        }

        public static void StartSubFlow(BaseFlow flow, IFlowData flowData, int returnState, Process process)
        {
            Logger.LogEvent(Severity.Debug, "Starting subflow {0}", flow.GetType().Name);

            flow.CurrentProcess = process;

            if (CurrentFlow != null)
            {
                if (FlowDataStack == null)
                {
                    FlowDataStack = new Stack<FlowExecutionData>();
                }

                FlowDataStack.Push(new FlowExecutionData(CurrentFlow, returnState));
            }

            CurrentFlow = flow;
            CurrentFlow.InitFlow(flowData);
        }

        public static BaseFlow ParentFlow
        {
            get
            {
                BaseFlow parentFlow = null;
                if (FlowDataStack != null)
                {
                    parentFlow = FlowDataStack.Peek().Flow;
                }
                return parentFlow;
            }
        }

        // Find flow in flow stack that is work-procees
        internal static BaseFlow ParentWorkProcessFlow
        {
            get
            {
                return (from flowExecutionData in FlowDataStack
                        where flowExecutionData.Flow.IsWorkProcess
                        select flowExecutionData.Flow).FirstOrDefault();
            }
        }


        public static BaseFlow StartSubFlow<T>(IFlowData flowData, int returnState, Process process) where T : BaseFlow, new()
        {
            BaseFlow flow = new T();

            StartSubFlow(flow, flowData, returnState, process);
            return flow;
        }

        /// <summary>
        /// This method gets the previous flow that was under execution, before starting the sub flow.
        /// Then gets State that need to execute now and executes the same.
        /// </summary>
        public static void EndSubFlow(IFlowResult flowResult)
        {
            using (ProfilingMeasurement.MeasureFunction("BaseModule.EndSubFlow"))
            {
                try
                {
                    ProfilingMeasurement.EndProfilingForFlow();

                    String flowName = CurrentFlow != null ? CurrentFlow.GetType().Name : "Unknown flow";
                    Logger.LogEvent(Severity.Debug, "Ending subflow for flow {0}", flowName);

                    if (CurrentFlow != null && FlowDataStack != null && FlowDataStack.Count > 0)
                    {
                        //Get the previous flow and set it as current flow
                        CurrentFlow.EndSubFlow();
                        var flowExecutionData = FlowDataStack.Pop();

                        CurrentFlow = flowExecutionData.Flow;
                        CurrentFlow.SubflowResult = flowResult;
                        if (CurrentFlow.CurrentView != null)
                        {
                            ViewCommands.ShowView(true, CurrentFlow.CurrentView, null);
                        }

                        //Execute the State retrived on the curren flow
                        CurrentFlow.ExecuteState(flowExecutionData.ReturnState);
                    }
                    else
                    {
                        if (CurrentFlow != null)
                        {
                            CurrentFlow.EndSubFlow();
                        }
                        if (ActiveModule != null)
                            ActiveModule.ExitModule();
                    }
                }
                catch (Exception e)
                {
                    Logger.LogException(e, "BaseModule.EndSubFlow");
                }
            }
        }



        /// <summary>
        /// Leave module and go back to main menu
        /// </summary>
        public void ExitModule()
        {
            Logger.LogEvent(Severity.Debug, "Executing BaseModule.ExitModule()");
            BusyUtil.Reset();
            IsForceActivate = true;
            bool cancel;
            ShowRequest(null, out cancel, false);
        }

        public void ActivateModule(bool forceShow)
        {
            Logger.LogEvent(Severity.Debug, "Executing BaseModule.ActivateModule()");

            if (ActiveModule != this)
            {
                ActiveModule = this;

                bool cancel;
                ShowRequest(this, out cancel, forceShow);
            }
        }

        public void ActivateModule()
        {
            ActivateModule(false);
        }

        /// <summary>
        /// Display custom form. Allows other modules/forms to cancel the request
        /// </summary>
        public bool DisplayModule()
        {
            bool cancel;
            IsForceActivate = true;
            ShowRequest(this, out cancel, false);
            IsForceActivate = false;
            return cancel;
        }

        /// <summary>
        /// The activate function gets called by the framework before the Form is displayed
        /// </summary>
        /// <returns>
        /// Returns true if the method completed sucessfully, if not return false
        /// </returns>
        public bool Activate()
        {
            BusyUtil.Activate();
            Logger.LogEvent(Severity.Debug, "Executing BaseModule.Activate()");
            ActiveModule = this;

            // Reset operation indexes as they can have been left initialised by another module
            ModelMain.SelectedOperationProcess.Clear();

            bool result = OnActivate();

            BusyUtil.Reset();

            return result;
        }

        public abstract bool OnActivate();

        /// <summary>
        /// The deactivate function gets called by the framework before the Form is hidden
        /// </summary>
        /// <param name="cancel">A return parameter (out) to accept to be deactivated or not. By setting cancel to true the component can force to stay activated</param>
        /// <returns>
        /// Returns true if the method completed sucessfully, if not return false
        /// </returns>
        public bool Deactivate(out bool cancel)
        {
            Logger.LogEvent(Severity.Debug, "Executing BaseModule.Deactivate()");

            //Help related code
            bool result;
#if DEBUG

            bool disableHelp = Settings.Read(new ModelUser.UserSettings(), ModelUser.UserSettings.DisableHelp, false, false);
#else
            const bool disableHelp = false;
#endif
            if (disableHelp == false && IsForceActivate == false)//Ensure deactivate is called by clicking on posten bring icon not through application 
            {
                HelpCommands.IsEditHelpAlowedDefault = Settings.Read(new ModelUser.UserSettings(), ModelUser.UserSettings.EditHelp, false, false);
                ShowHelp(HelpCommands.IsEditHelpAlowedDefault);
                cancel = true;
                result = false;
            }
            else
            {
                HideOnScreenKeyboardIcon();

                IsForceActivate = false;
                if (DeactivateModuleEvent != null)
                {
                    DeactivateModuleEvent.Invoke();
                }
                result = OnDeactivate(out cancel);

                // Turn off scanner
                FotScanner.Instance.Deactivate();
            }

            if (!cancel)
            {
                ReleaseResources();
            }

            // Fix issue with title bar dissappearing if closing form with text box
            Application.State = ApplicationState.Activated;

            return result;
        }

        private void ShowHelp(bool editHelp)
        {
            //Ugly hack for change password    
            var formName = GetCurrentFormName();
            var realModuleName = Name;
            if (Name.Contains("_"))
                realModuleName = Name.Split(Convert.ToChar("_"))[0];
            var processName = CurrentFlow != null ? Convert.ToString(CurrentFlow.CurrentProcess) : string.Empty;
            string description = SettingDataProvider.Instance.GetHelpText(realModuleName, processName,
                                                                          formName, ModelUser.UserProfile.LanguageCode);
            Cursor.Current = Cursors.Default; // Framework has turned on busy-pointer when we come here
            if (_isToShowPopup)
            {
                _isToShowPopup = false;
                string result;

                if (editHelp)//Show in edit mode to add help texts
                    result = GuiCommon.EditHelpText(realModuleName, processName, formName, description);
                else // show help in normal mode
                {
                    result = GuiCommon.ShowModalDialog(GlobalTexts.Help, description, Severity.Info, GlobalTexts.Ok
                        //uncomment next line to be able to terminate the program via the help dialogue
                        //, GlobalTexts.Break
                         );
                }
                if (result == GlobalTexts.Break) System.Windows.Forms.Application.Exit();

                _isToShowPopup = true;
            }
        }


        /// <summary>
        /// Gets the name of currently visible form 
        /// </summary>
        /// <returns></returns>
        private string GetCurrentFormName()
        {
            string formName = string.Empty;
            if (CurrentFlow != null && CurrentFlow.CurrentView != null)
            {
                formName = ((_currentView != null) && (_currentView.Name == "FormChangePassword"))
                                ? "FormChangePassword"
                                : CurrentView.Name;
            }
            else //Get forName from Parent Flow If exists (case for Vas Matrix popups)
            {
                if ((ParentFlow != null) && (ParentFlow.CurrentView != null))
                {
                    formName = ParentFlow.CurrentView.Name;
                }
            }
            return formName;
        }

        private static void ReleaseResources()
        {
            //If deactivated successfully then set activemodule to null
            ActiveModule = null;

            var viewsToRemove = new List<string>();

            //TODO: Remove this.
            foreach (var keyValuePair in ViewCommands.Views)
            {
                // Ugly hack. Do not dispose this as FormChangeLocation is used as long-lived login-step
                if (keyValuePair.Value.GetType().Name != "FormChangeProfile")
                {
                    keyValuePair.Value.Hide();
                    viewsToRemove.Add(keyValuePair.Key);
                    // Can not remove from list we are itereating, so store for later removal
                }
                else
                {
                    keyValuePair.Value.Visible = false;
                }
            }

            foreach (string viewName in viewsToRemove)
            {
                ViewCommands.Views.Remove(viewName);
            }


            CurrentFlow = null;
            FlowDataStack = null;
            ViewCommands.CurrentView = null;
            ModelMain.ResetIsOnSensistiveScreenWithoutSignal();
        }

        public abstract bool OnDeactivate(out bool cancel);


        public abstract Image Icon { get; }

        /// <summary>
        /// Gets the main form of the component (read only)
        /// </summary>
        public abstract PreComForm Form { get; }

        /// <summary>
        /// The event to invoke for requesting to  be shown or hidden (see ShowRequestDelegate)
        /// </summary>
        public event ShowRequestDelegate ShowRequest;

        public void ShowOnScreenKeyboardIcon()
        {
            if (_onScreenKeyboardIconItem == null)
            {
                Image keyboardIconImage = ScaleUtil.GetImage(ScaleUtil.OnScreenKeyboardIcon);
                _onScreenKeyboardIconItem = new StatusIconItem(keyboardIconImage, 1000);
                _onScreenKeyboardEnabled = true;
                _onScreenKeyboardIconItem.Click += OnScreenKeyboardIconClick;
            }
            if (!_onScreenKeyboardIconItem.Visible)
            {
                ShowStatusIcon(_onScreenKeyboardIconItem);
            }
        }

        private void OnScreenKeyboardIconClick(object sender, MouseEventArgs e)
        {
            if (!_onScreenKeyboardEnabled)
            {
                return;
            }
            try
            {
                if (CurrentView != null)
                {
                    CurrentView.ShowOnScreenKeyboard();
                }
                else
                {
                    Logger.LogEvent(Severity.Debug, "CurrentView != null in OnScreenKeyboardIconClick");
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BaseModule.OnScreenKeyboardIconClick");
            }
        }
        internal void DisableOnScreenKeyboardIcon()
        {
            _onScreenKeyboardEnabled = false;

        }
        internal void EnableOnScreenKeyboardIcon()
        {
            _onScreenKeyboardEnabled = true;
        }

        /// <summary>
        /// CurrentView will return the CurrentFlow.CurrentView if it is in a flow.
        /// If not, CurrentView must be set manually. CurrentView is set by those forms that is not part of the View-Flow-Command architecture
        /// </summary>
        private BaseForm _currentView;
        public BaseForm CurrentView
        {
            get
            {
                if (CurrentFlow != null && CurrentFlow.CurrentView != null)
                {
                    return CurrentFlow.CurrentView;
                }
                return _currentView;
            }
            set { _currentView = value; }
        }

        public void HideOnScreenKeyboardIcon()
        {
            if (_onScreenKeyboardIconItem != null && HideStatusIcon != null)
                HideStatusIcon(_onScreenKeyboardIconItem);
        }



#pragma warning disable 67
        public event StatusIconDelegate ShowStatusIcon;
        public event StatusIconDelegate HideStatusIcon;
        public event StatusIconDelegate UpdateStatusIcon;
#pragma warning restore 67

    }
}

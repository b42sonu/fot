﻿using System;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public class TabButton : Resco.Controls.OutlookControls.ImageButton
    {
        #region Variables and Objects
        public Boolean UseDefaultProperties = true;
        public new TabButtonType ButtonType = TabButtonType.Normal;
        public ActivateOn Activation = ActivateOn.Always;
        internal bool DisabledBecauseOfScan = false;
        internal int Index { get; set; }
        public EventHandler ClickHandler { get; private set; }

        #endregion

        #region Overloaded Constructors

        /// <summary>
        /// if all properties are to be set in parent form
        /// </summary>
        public TabButton()
        {
        }

        /// <summary>
        /// specify buttontext and event handler
        /// </summary>
        /// <param name="buttonText"></param>
        /// <param name="clickHandler"></param>
        public TabButton(string buttonText, EventHandler clickHandler)
        {
            Text = buttonText;
            Click += clickHandler;
            ClickHandler = clickHandler;
        }

        /// <summary>
        /// button with Type
        /// </summary>
        public TabButton(string buttonText, EventHandler clickHandler, TabButtonType type)
        {
            Text = buttonText;
            ClickHandler = clickHandler;
            Click += clickHandler;
            ButtonType = type;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        /// <summary>
        /// create named button 
        /// </summary>
        /// <param name="buttonText"></param>
        /// <param name="clickHandler"></param>
        /// <param name="buttonName"> </param>
        public TabButton(string buttonText, EventHandler clickHandler, string buttonName)
        {
            Text = buttonText;
            Click += clickHandler;
            Name = buttonName;
            ClickHandler = clickHandler;
            Enabled = true;
        }

        #endregion
    }
}
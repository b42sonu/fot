﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{    
    /// <summary>
    /// This class will act as a base form for each and every form, that will be part of this application.
    /// </summary>
    public partial class SettingsBaseForm : PreCom.Controls.PreComSettingsForm
    {
        #region Private variables and properties

        /// <summary>
        /// Flows should store results of invoked events in this varaiable
        /// </summary>
        public EventResult EventResult;

        /// <summary>
        /// Descending forms will invoke all events through this delegate
        /// As subscribee lives longer than subscriber, this is in effect a single call event
        /// subscribable by one subscriber only. This means when a new subscriber comes
        /// he will overwrite the old subscription. WIth this we avoid setting off events through
        /// flows that should have been garbage collected if it was not for the event still hold a reference to it.
        /// Removing events through -= is non-deterministic, and often not happens causing tremendous problems where
        /// view lives longer than subscriber. New subscriptions are set throug SetEventHandler()
        /// </summary>
        public ViewEventDelegate ViewEvent{ get; private set; }
        
        /// <summary>
        /// Set eventhandler for view with this call. You can only have one subscriber, so new subscriptions will
        /// overwrite old. To cancel a subscription set this to null
        /// </summary>
        /// <param name="viewEvent">The function that should be invoked by event</param>
        public void SetEventHandler(ViewEventDelegate viewEvent)
        {
            ViewEvent = viewEvent;
        }

        #endregion

        /// <summary>
        /// Constructor with single parameter
        /// </summary>
        /// <param>object for current module <name>objModule</name> </param>
        public SettingsBaseForm()
        {
            InitializeComponent();
            EventResult = new EventResult();
        }

        /// <summary>
        /// This is the virtual function, and each screen will have to override the same
        /// This function will be use full in case we need to refresh a screen.
        /// </summary>
        public virtual void UpdateView(params Object[] data) { }

        /// <summary>
        /// Override this method to do initialising before a view is shown
        /// </summary>
        /// <param name="data"></param>
        public virtual void OnShow(params Object[] data){}

        /// <summary>
        /// Called when window is closed (in reality hidden). Override to do cleanup
        /// </summary>
        public virtual void OnClose(){}
    }
}
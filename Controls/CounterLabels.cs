﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public class CounterLabels
    {
        private readonly TransparentLabel _lblNumberOfCarriers;
        private readonly TransparentLabel _lblNumberOfItems;
        public CounterLabels(TouchPanel touchPanel)
        {
            _lblNumberOfCarriers =
                 new TransparentLabel
                 {
                     Size = new System.Drawing.Size(438, 30),
                     Location = new System.Drawing.Point(21, 415),
                     Text = GlobalTexts.NoOfCarriers + ":"
                 };

            touchPanel.Controls.Add(_lblNumberOfCarriers);
            _lblNumberOfCarriers.BringToFront();

            _lblNumberOfItems =
                    new TransparentLabel
                    {
                        Size = new System.Drawing.Size(438, 30),
                        Location = new System.Drawing.Point(21, 385),
                        Text = GlobalTexts.NoOfItems + ":"
                    };
            touchPanel.Controls.Add(_lblNumberOfItems);
            _lblNumberOfItems.BringToFront();

        }

        public bool LoadCarriersCountIsVisible()
        {
            var currentProcess = BaseModule.CurrentFlow != null ? BaseModule.CurrentFlow.CurrentProcess : Process.Unknown;
            var returnvalue = currentProcess != Process.RegisterDamage;
            return returnvalue;
        }


        public void SetValues(int numberOfCarriers, int numberOfItems)
        {
            _lblNumberOfCarriers.Visible = LoadCarriersCountIsVisible();
            _lblNumberOfCarriers.Text = GlobalTexts.NoOfCarriers + ": " + numberOfCarriers;
            _lblNumberOfItems.Text = GlobalTexts.NoOfItems + ": " + numberOfItems;
        }

        public void SetValues(EntityMap entityMap)
        {
            var consignmentItems = 0;
            var loadCarriers = 0;

            if (entityMap != null)
            {
                consignmentItems = entityMap.ScannedConsignmentItemsTotalCount;
                loadCarriers = entityMap.ScannedLoadCarrierTotalCount;
            }
            SetValues(loadCarriers, consignmentItems);
        }

    }
}

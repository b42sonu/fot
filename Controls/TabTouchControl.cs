﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.OutlookControls;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{

    //
    //     For creating tabs according to desgin.
    //Usage:
    //    tabTouchControl.TabSelected += TabSelected;
    //    List<String> tabTitleList = new List<string>();
    //    tabTitleList.Add("Tab 1");
    //    tabTitleList.Add("Tab 2");
    //    tabTitleList.Add("Tab 3");
    //    tabTouchControl.AddTabs(tabTitleList);
    public partial class TabTouchControl : UserControl
    {
        readonly List<ImageButton> _tabButtonList = new List<ImageButton>();

        private static readonly int TabHeight = ScaleUtil.GetScaledPosition(60);
        //Margin outside the tab buttons. (Left and reight side) 
        private static readonly int TabWidthMargin = ScaleUtil.GetScaledPosition(20);

        private const int SpacingBetweenTabButtons = 1;

        private readonly Color _selectColor = Color.White;
        private readonly Color _unSelectColor = Color.Black;

        private int _selectedTabIndex;

        public delegate void TabSelectedDelegate(int tabIndex);
        public TabSelectedDelegate TabSelected { get; set; }

        public TabTouchControl()
        {
            InitializeComponent();

            BackColor = _selectColor;
        }

        public void AddTabs(List<String> tabTitles)
        {
            if (tabTitles == null || tabTitles.Count == 0)
                throw new InvalidOperationException("Do not add null or empty tabs");

            RemoveCurrentTabs();

            int totalPixelBetweenButtons = (tabTitles.Count() - 1) * SpacingBetweenTabButtons;
            int tabWidth = ((Width - (TabWidthMargin * 2) - totalPixelBetweenButtons) / tabTitles.Count());

            int tabIndex = 0;
            foreach (var tabTitle in tabTitles)
            {
                var tabButton = CreateTabButton(tabWidth, tabIndex, tabTitle);
                Controls.Add(tabButton);
                _tabButtonList.Add(tabButton);
                tabIndex++;
            }

            var lastButton = _tabButtonList[_tabButtonList.Count - 1];
            AddPaddingColorAsParentBackground(lastButton.Location.X + lastButton.Size.Width);

            SelectTab(0);
        }

        private void RemoveCurrentTabs()
        {
            foreach (var tabButton in _tabButtonList)
            {
                tabButton.Click -= TabButtonClick;
                Controls.Remove(tabButton);
            }

            _tabButtonList.Clear();
        }

        private ImageButton CreateTabButton(int tabWidth, int tabIndex, string tabTitle)
        {
            int x = (tabWidth * tabIndex) + TabWidthMargin;

            //Add spacing between buttons, not the first one
            if (tabIndex != 0)
                x += SpacingBetweenTabButtons * tabIndex;

            var tabButton = new ImageButton
                {
                    Location = new Point(x, 0),
                    Name = "tabButton" + tabIndex,
                    Size = new Size(tabWidth, TabHeight),
                    TabIndex = tabIndex,
                    Text = tabTitle,
                    BorderSize = 0,
                    Font = new Font("Tahoma", 8F, FontStyle.Bold),
                };
            tabButton.Click += TabButtonClick;
            tabButton.BorderColor = Color.White;
            return tabButton;
        }

        private void AddPaddingColorAsParentBackground(int lastTabXRightCorner)
        {
            var paddingLeftTab = new UserControl()
            {
                Location = new Point(0, 0),
                Name = "paddingLeftTab",
                Size = new Size(TabWidthMargin, TabHeight),
                BackColor = Parent.BackColor
            };
            Controls.Add(paddingLeftTab);

            var paddingrightTab = new UserControl()
            {
                Location = new Point(lastTabXRightCorner, 0),
                Name = "paddingrightTab",
                Size = new Size(Width - lastTabXRightCorner, TabHeight),
                BackColor = Parent.BackColor
            };
            Controls.Add(paddingrightTab);
        }


        private void TabButtonClick(object sender, EventArgs e)
        {
            try
            {
                var tabButton = (ImageButton)sender;

                //Ignore if tab already selected
                if (_selectedTabIndex == tabButton.TabIndex)
                    return;

                SelectTab(tabButton.TabIndex);
                TabSelected.Invoke(tabButton.TabIndex);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "TabTouchControl.TabButtonClick");
            }
        }

        private void SelectTab(int selectedIndex)
        {
            _selectedTabIndex = selectedIndex;
            int index = 0;
            foreach (var tabButton in _tabButtonList)
            {
                if (index == selectedIndex)
                    SetSelectedColor(tabButton);
                else
                    SetUnselectedColor(tabButton);
                index++;
            }
        }

        private void SetSelectedColor(ImageButton tabButton)
        {
            tabButton.FocusedBorderColor = _selectColor;
            tabButton.ForeColor = _unSelectColor;
            tabButton.BackColor = _selectColor;
        }

        private void SetUnselectedColor(ImageButton tabButton)
        {
            tabButton.FocusedBorderColor = _unSelectColor;
            tabButton.ForeColor = _selectColor;
            tabButton.BackColor = _unSelectColor;
        }
    }
}

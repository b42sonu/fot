﻿using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    /// <summary>
    /// container for reconsiliation details
    /// </summary>
    public sealed class DetailsPanel : Panel
    {
        private readonly Font _boldFont;
        private readonly Panel _leftPanel;
        private readonly Panel _topPanel;
        public DetailsPanel()
        {
            Size = new Size(480 - 2 * 21, 552 - 90 - 70 + 9);
            Location = new Point(21, 81);
            AutoScroll = true;
            BackColor = Color.Transparent;

            _boldFont = new Font("Arial", 9F, FontStyle.Bold);

            _leftPanel = new Panel { Width = 8, Dock = DockStyle.Left, };
            _topPanel = new Panel { Height = 8, Dock = DockStyle.Top, };
        }

        public void AddSections(List<Info> infolist)
        {
            infolist.Reverse();
            SuspendLayout();
            Controls.Clear();
            Controls.Add(_leftPanel);

            foreach (var info in infolist)
            {
                    Controls.Add(new Panel {Height = 6, Dock = DockStyle.Top});

                    Controls.Add(new TransparentLabel
                                     {
                                         Text = info.Section, AutoWidth = Width - 50, Dock = DockStyle.Top,
                                     });

                    Controls.Add(new TransparentLabel
                                     {
                                         Text = info.Heading + ":", Font = _boldFont, AutoWidth = Width - 50, Dock = DockStyle.Top
                                     });
            }
            _leftPanel.SendToBack();
            Controls.Add(_topPanel);
            ResumeLayout();
            BringToFront();
        }

        public void AddFormattedSections(List<Info> infolist)
        {
            infolist.Reverse();
            SuspendLayout();
            Controls.Clear();
            Controls.Add(_leftPanel);

            foreach (var info in infolist)
            {
                Controls.Add(new Panel { Height = 6, Dock = DockStyle.Top });

                var details = new Panel() { Dock = DockStyle.Top };
                var labelValue = new TransparentLabel
                {
                    Text = info.Section,
                    Dock = DockStyle.Left,
                    AutoSize = true,

                };
                details.Controls.Add(labelValue);
                var labelHeading = new TransparentLabel
                {
                    Text = info.Heading + ": ",
                    Font = _boldFont,
                    Dock = DockStyle.Left,
                    AutoSize = true
                };

                if (labelHeading.Width + labelValue.Width > Width)
                    labelValue.AutoWidth = Width - labelHeading.Width - 50;
                details.Height = labelValue.Height > labelHeading.Height ? labelValue.Height : labelHeading.Height;
                details.Controls.Add(labelHeading);
                Controls.Add(details);

            }
            _leftPanel.SendToBack();
            Controls.Add(_topPanel);
            ResumeLayout();
            BringToFront();
        }

        public class Info
        {
            public string Heading;
            public string Section;
        }
    }
}

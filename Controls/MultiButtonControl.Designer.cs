﻿using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    partial class MultiButtonControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelButtonContainer = new Resco.Controls.CommonControls.TouchPanel();
            this.SuspendLayout();
            // 
            // panelButtonContainer
            // 
            this.panelButtonContainer.BackColor = System.Drawing.Color.Transparent;
            this.panelButtonContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelButtonContainer.Location = new System.Drawing.Point(0, 0);
            this.panelButtonContainer.Name = "panelButtonContainer";
            this.panelButtonContainer.Size = ScaleUtil.GetScaledSize(480, 102);
            // 
            // MultiButtonControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.WhiteSmoke;// System.Drawing.Color.Transparent;
            this.Controls.Add(this.panelButtonContainer);
            this.Location = ScaleUtil.GetScaledPoint(0, 442-6);
            this.Name = "MultiButtonControl";
            this.Size = ScaleUtil.GetScaledSize(480, 102);
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel panelButtonContainer;
    }
}

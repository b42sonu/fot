﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public abstract class BaseBarcodeScanForm : BaseForm
    {

#if PROFILING
        protected BaseBarcodeScanForm()
        {
            FotScanner.Instance.SetBarcodeScannedProfilingHandler(OnTuneBarcodeScanned);
        }
#endif

        private void StartScan(FotCodeScannedDelegate onBarcodeScannedEventHandler)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Scanner))
            {
                FotScanner.Instance.Activate(onBarcodeScannedEventHandler);
            }
        }

        public void StopScan()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Scanner))
            using (ProfilingMeasurement.MeasureFunction("BaseBarcodeScanForm.StopScan"))
                FotScanner.Instance.Deactivate();
        }

#if PROFILING
        private void OnTuneBarcodeScanned()
        {
            ProfilingMeasurement.StartProfiling(GetType());
        }
#endif


        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            StartScan();
        }

        public override void OnUpdateView(IFormData iformData)
        {
            base.OnUpdateView(iformData);
            StartScan();
        }

        public override void OnClose()
        {
            base.OnClose();
            StopScan();
        }

        public void StartScan()
        {
            StartScan(OnBarcodeScanned);
        }

        private void OnBarcodeScanned(FotScannerOutput scannerOutput)
        {
            try
            {
                BarcodeScanned(scannerOutput);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BaseBarcodeScanForm.OnBarcodeScanned");
            }
        }

        public abstract void BarcodeScanned(FotScannerOutput scannerOutput);

    }

}

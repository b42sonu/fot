﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public partial class MultiButtonControl : UserControl
    {
        #region Variables and Objects
        public List<TabButton> ListButtons = new List<TabButton>();
        private List<TabButton> ListVisibleButtons
        {
            get 
            {
                    return _listVisibleButtons ?? (_listVisibleButtons = (from n in ListButtons where n.Visible select n).ToList());
            }
            set { _listVisibleButtons = value; }
        }


        private EventHandler HardwareKeyboardEnter { get; set; }
        private readonly string _nextButtonText = GlobalTexts.Next;
        private readonly string _previousButtonText = GlobalTexts.Previous;
        private const short NextButtonIndex = 5;
        private const short PreviousButtonIndex = 0;
        private readonly int _buttonSpace = ScaleUtil.GetScaledPosition(2);
        private Point _tabLocation = ScaleUtil.GetScaledPoint(2, 485);
        private const int ButtonHeight = 50;
        private Size _buttonSize = ScaleUtil.GetScaledSize(157, ButtonHeight);
        private int _currentPage = 1;
        private int _pageCount = 1;
        
        

        private readonly List<TabButton> _currentButtons = new List<TabButton>();
        private readonly List<TabButton> _previousButtons = new List<TabButton>();
        private TabButton _buttonPrevious;
        private TabButton _buttonNext;

        private const int ButtonsPerPage = 6;


        private int _scannedItemCount;
        private readonly Color _blackStripColor = Color.FromArgb(40, 42, 43);
        private readonly Color _normalColor = Color.FromArgb(46, 48, 50);
        private int _listIndex;
        private bool _updateComplete = true;
        private bool _refreshTabRequired;
        #endregion

        #region Accessors

        public int ScannedItemCount
        {
            get { return _scannedItemCount; }
            set
            {
                // set state to value
                _scannedItemCount = value;
                ItemScanned();
            }
        }

        #endregion

        #region Constructor

        public MultiButtonControl()
        {
            InitializeComponent();
            panelButtonContainer.BackColor = SystemColors.InactiveCaption;
            ResizePanel(false);
        }

        #endregion

        #region Methods

        /// <summary>
        /// set default properties to ListActiveButtons
        /// </summary>
        private void SetDefaultProperties()
        {
            foreach (var btn in ListVisibleButtons.Where(btn => btn.UseDefaultProperties))
            {
                SetDefaultProperties(btn);
                if (btn.Text.Length > 15 || btn.Text.Contains("\r\n"))
                {
                    AdjustFont(btn);
                }
                if (btn.Visible) continue;
                btn.Visible = true;
                ShowButton(btn, false);
            }
        }

        private void SetDefaultProperties(TabButton btn)
        {
            btn.BackColor = _normalColor;
            btn.Font = new Font("Arial", 9F, FontStyle.Regular);
            btn.ForeColor = Color.White;
            btn.Size = _buttonSize;
        }

        private string[] _words;

        private void AdjustFont(TabButton btn)
        {
            var text = btn.Text;
            btn.Font = new Font("Arial", 8F, FontStyle.Regular);
            if (text.Contains("\r\n") || text.Length <= 15)
                return;

            var newText = string.Empty;
            _words = text.Split(' ');
            for (var i = 0; i < _words.Length; i++)
            {
                newText += _words[i];
                var len = newText.Length + _words[i].Length;
                if (len <= 15 && len - NextLine(i + 1).Length > 8) continue;
                newText += Environment.NewLine + NextLine(i + 1);
                break;
            }
            newText.Replace('_', ' ');
            btn.Text = newText;
        }

        private string NextLine(int index)
        {
            var nextLine = string.Empty;
            for (; index < _words.Length; index++)
            {
                nextLine += _words[index] + ' ';
            }
            if (!string.IsNullOrEmpty(nextLine) && nextLine[nextLine.Length - 1] == ' ')
                nextLine = nextLine.Substring(0, nextLine.Length - 1);
            return nextLine;
        }

        /// <summary>
        /// add ListActiveButtons to panel
        /// </summary>
        private void SetButtonIndex()
        {
            short index = 1;
            var count = 1;

            foreach (var button in ListVisibleButtons)
            {
                button.Index = index;
                if (index == 5)
                {
                    if (ListVisibleButtons.Count - 1 != count)
                        index = 1;
                }

                index++;
                count++;
            }
            PreviousPage();
        }

        void AddFirst()
        {
            _previousButtons.Clear();
            _currentButtons.Clear();
            if (_pageCount == 1)
            {
                _currentButtons.AddRange(ListVisibleButtons);
                return;
            }
            _listIndex = 0;
            for (var i = 0; i < 6 && _listIndex < ListVisibleButtons.Count; i++)
            {
                if (i == 5)
                {
                    var lastButton = GetLastButton(_listIndex);
                    if (lastButton != null && _currentButtons.Contains(lastButton) == false)
                    {
                        _currentButtons.Add(lastButton);
                    }
                    else
                    {
                        lastButton = new TabButton { Index = NextButtonIndex + 1 };
                        SetDefaultProperties(lastButton);
                        ShowButton(lastButton, false);
                        _currentButtons.Add(lastButton);
                    }
                    if (_currentButtons.Count == 6 && _currentButtons[5].Visible == false && _currentButtons[4].Visible && _currentButtons[4].Visible)
                    {
                        var btn = _currentButtons[4];
                        _currentButtons.RemoveAt(4);
                        _currentButtons.Add(btn);
                    }

                }
                else
                {
                    if (ListVisibleButtons[_listIndex].Visible && _currentButtons.Contains(ListVisibleButtons[_listIndex]) == false)
                        _currentButtons.Add(ListVisibleButtons[_listIndex]);
                    else
                    {
                        var nextButtonAtThisLocation = GetButton(_listIndex, true);
                        if (_currentButtons.Contains(nextButtonAtThisLocation) == false)
                            _currentButtons.Add(nextButtonAtThisLocation);
                    }

                    _listIndex++;
                }
            }
        }

        void NextPage()
        {
            foreach (var button in _currentButtons.Where(button => _previousButtons.Contains(button) == false))
            {
                _previousButtons.Add(button);
            }
            _currentButtons.Clear();
            if (_currentPage > 1)
                _currentButtons.Add(_buttonPrevious);
            for (var i = 0; i < 4 && _listIndex < ListVisibleButtons.Count; i++)
            {
                var button = GetButton(_listIndex, true);
                _listIndex++;
                if (button != null)
                {
                    if (button.Index < 0)//if the index is not specified
                        button.Index = i + 2;
                    _currentButtons.Add(button);
                }
            }
        }
        void PreviousPage()
        {
            if (!_updateComplete)
                return;
            AddFirst();
            if (_currentPage > 1)
            {
                for (var i = 1; i < _currentPage; i++)
                {
                    NextPage();
                    var enabledButtons = (from btn in _currentButtons where btn.Visible && btn != _buttonPrevious && btn != _buttonNext select btn).Count();
                    if (enabledButtons >= 1 || _currentPage <= 1) continue;
                    _currentPage--;
                    PreviousPage();
                }
                var nextButton = GetLastButton(_listIndex);
                if (nextButton != null)
                    _currentButtons.Add(nextButton);
                SetButtonPositionByIndex();
            }
            else
            {
                SetButtonPositionByIndex();
            }
        }

        private TabButton GetButton(int index, bool forward)
        {

            var button = ListVisibleButtons[index];

            if (forward)
            {
                if (button.Visible == false || _previousButtons.Contains(button))
                {
                    for (var i = index + 1; i < ListVisibleButtons.Count; i++)
                        if (ListVisibleButtons[i].Visible && ListVisibleButtons[i].Index == button.Index && _previousButtons.Contains(ListVisibleButtons[i]) == false)
                        {
                            button = ListVisibleButtons[i];
                            break;
                        }
                    if (_previousButtons.Contains(button) && _currentPage >= _pageCount)
                    {
                        button = new TabButton();
                        ShowButton(button, false);
                        SetDefaultProperties(button);
                        button.Index = -9;//indicate that the index of this button has not been specified
                    }
                }
            }
            else
            {
                if (button.Visible == false || _previousButtons.Contains(button))
                {
                    for (var i = index - 1; i > 0; i--)
                        if (ListVisibleButtons[i].Visible && ListVisibleButtons[i].Index == button.Index)
                        {
                            button = ListVisibleButtons[i];
                            break;
                        }
                }
            }
            return button;
        }


        TabButton GetLastButton(int index)
        {
            var nextPageButtons = (from btn in ListVisibleButtons
                                   where
                                       _previousButtons.Contains(btn) == false &&
                                       _currentButtons.Contains(btn) == false && btn.Visible
                                   select btn).ToList();
            if (nextPageButtons.Count < 4)
            {
                if (_currentButtons[1].Visible == false)
                {
                    var secondButton = ((from btn in nextPageButtons where btn.Index == 2 select btn).SingleOrDefault() ??
                                        (from btn in nextPageButtons where btn.Index == 5 select btn).SingleOrDefault()) ??
                                       (from btn in nextPageButtons where btn.Index == 4 select btn).SingleOrDefault();
                    if (secondButton == null && nextPageButtons.Count == 2)
                        secondButton = (from btn in nextPageButtons where btn.Index == 3 select btn).SingleOrDefault();
                    if (secondButton != null)
                    {
                        _currentButtons.Remove(_currentButtons[1]);
                        _currentButtons.Insert(1, secondButton);
                        nextPageButtons.Remove(secondButton);
                    }
                }
                if (_currentButtons[2].Visible == false && nextPageButtons.Count != 1)
                {
                    var thirdButton =
                        (from btn in nextPageButtons where btn.Index == 3 select btn).SingleOrDefault() ??
                        (from btn in nextPageButtons where btn.Index == 4 select btn).SingleOrDefault();

                    if (thirdButton != null)
                    {
                        _currentButtons.Remove(_currentButtons[2]);
                        _currentButtons.Insert(2, thirdButton);
                        nextPageButtons.Remove(thirdButton);
                    }
                }


                if (_currentButtons[3].Visible == false)
                {
                    var fourthButton = (from btn in nextPageButtons where btn.Index == 1 select btn).SingleOrDefault(); //??

                    if (fourthButton == null && _currentButtons[4].Visible)
                    {
                        fourthButton = (from btn in nextPageButtons where btn.Index == 5 select btn).SingleOrDefault() ??
                                      (from btn in nextPageButtons where btn.Index == 2 select btn).SingleOrDefault() ??
                                       (from btn in nextPageButtons where btn.Index == 3 select btn).SingleOrDefault();
                    }
                    if (fourthButton != null)
                    {
                        _currentButtons.Remove(_currentButtons[3]);
                        _currentButtons.Insert(3, fourthButton);
                        nextPageButtons.Remove(fourthButton);
                    }
                }

                if (_currentButtons[4].Visible == false)
                {
                    var fifthButton = ((from btn in nextPageButtons where btn.Index == 5 select btn).SingleOrDefault() ??
                                        (from btn in nextPageButtons where btn.Index == 2 select btn).SingleOrDefault()) ??
                                       (from btn in nextPageButtons where btn.Index == 3 select btn).SingleOrDefault();

                    if (fifthButton != null)
                    {
                        _currentButtons.Remove(_currentButtons[4]);
                        _currentButtons.Insert(4, fifthButton);
                        nextPageButtons.Remove(fifthButton);
                    }
                }
            }

            var nextActiveButtonCount = 0;
            TabButton lastButton = null;
            for (var i = index; i < ListVisibleButtons.Count; i++)
            {
                if (ListVisibleButtons[i].Visible && ListVisibleButtons[i] != _buttonPrevious && _previousButtons.Contains(ListVisibleButtons[i]) == false && _currentButtons.Contains(ListVisibleButtons[i]) == false)
                {
                    nextActiveButtonCount++;
                    lastButton = ListVisibleButtons[i];
                    if (nextActiveButtonCount > 1)
                    {
                        return _buttonNext;
                    }
                }

            }
            if (index < ListVisibleButtons.Count && lastButton == null)
            {
                lastButton = new TabButton { Index = NextButtonIndex + 1 };
                SetDefaultProperties(lastButton);
                ShowButton(lastButton, false);
                return lastButton;
            }
            return lastButton ?? ListVisibleButtons[ListVisibleButtons.Count - 1];
        }

        public void BeginUpdate()
        {
            _updateComplete = false;
        }

        public void EndUpdate()
        {
            _updateComplete = true;
            if (_refreshTabRequired)
            {
                RefreshTab();
                _refreshTabRequired = false;
            }
            else if (_currentPage == 1)
            {
                PreviousPage();
            }
        }

        public void GenerateButtons()
        {
            SetButtons();
            ItemScanned();

            var confirmButton = (from btn in ListVisibleButtons where btn.ButtonType == TabButtonType.Confirm select btn).SingleOrDefault();
            if (confirmButton != null)
                HardwareKeyboardEnter = confirmButton.ClickHandler;

            if (confirmButton != null && confirmButton.Visible)
                ActivateHardwareEnter();
            else
                HardwareKeyboard.ClearEnterKeyPressedSubscription();

            // Subscribe to events where parent form is set visible 
            if (ParentForm != null)
                ParentForm.OnSetVisible = OnSetVisible;
        }

        private void OnSetVisible()
        {
            if (HardwareKeyboard.OnEnterKeyPressed != HardwareKeyboardEnter)
            {
                var confirmButton = (from btn in ListVisibleButtons where btn.ButtonType == TabButtonType.Confirm select btn).SingleOrDefault();
                if (confirmButton != null && confirmButton.Enabled)
                    ActivateHardwareEnter();
                else
                    HardwareKeyboard.ClearEnterKeyPressedSubscription();
            }
        }

        private TabButton CreateEmptyButton()
        {
            var btn = new TabButton(string.Empty, null);
            SetDefaultProperties(btn);
            ShowButton(btn, true);
            btn.UseDefaultProperties = false;
            btn.Enabled = false;
            btn.Visible = true;
            return btn;
        }

        private void SetCancelConfirmButton()
        {
            var btnCount = (from n in ListVisibleButtons where n.ButtonType == TabButtonType.Cancel select n).Count();
            if (btnCount > 1)
            {
                throw new Exception("More than one ListActiveButtons added with Type.Canell");
            }

            btnCount = (from n in ListVisibleButtons where n.ButtonType == TabButtonType.Confirm select n).Count();
            if (btnCount > 1)
            {
                throw new Exception("More than one ListActiveButtons added with Type.Confirm");
            }

            var button = (from n in ListVisibleButtons where n.ButtonType == TabButtonType.Cancel select n).SingleOrDefault();
            if (button != null)
            {
                ListVisibleButtons.Remove(button);
                ListVisibleButtons.Insert(PreviousButtonIndex, button);
            }
            button = (from n in ListVisibleButtons where n.ButtonType == TabButtonType.Confirm select n).SingleOrDefault();

            if (button == null)
                return;

            if (ListVisibleButtons.Count > 2)
            {
                ListVisibleButtons.Remove(button);
                ListVisibleButtons.Insert(2, button);
            }
            else
            {
                ListVisibleButtons.Remove(button);
                ListVisibleButtons.Add(button);
            }
        }

        public void Clear()
        {
            //clear everylist and panel
            if (ListButtons != null)
                ListButtons.Clear();

            if (ListVisibleButtons != null)
                ListVisibleButtons.Clear();

            if (panelButtonContainer != null)
                panelButtonContainer.Controls.Clear();
        }


        private void RefreshTab()
        {
            SetButtons();
            //go to the current page
            for (var i = 1; i < _currentPage; i++)
            {
                Next();
            }
        }

        private void SetButtons()
        {
            panelButtonContainer.Controls.Clear();

            //get buttons from ListButtons that are visible
            ListVisibleButtons = (from n in ListButtons where n.Visible select n).ToList();

            SetPageCount();

            switch (ListVisibleButtons.Count)
            {
                case 0:
                    return;

                case 1:
                    if (ListVisibleButtons[0].ButtonType == TabButtonType.Confirm)
                    {
                        ListVisibleButtons.Insert(0, CreateEmptyButton());
                        ListVisibleButtons.Insert(0, CreateEmptyButton());
                    }
                    else
                    {
                        ListVisibleButtons.Add(CreateEmptyButton());
                        ListVisibleButtons.Add(CreateEmptyButton());
                    }
                    break;


                case 2:
                    SetCancelConfirmButton();
                    ListVisibleButtons.Insert(1, CreateEmptyButton());
                    SetDefaultProperties();
                    break;

                case 3:
                    break;

                default:
                    var count = ListVisibleButtons.Count;
                    var type = ListVisibleButtons[ListVisibleButtons.Count - 1].ButtonType;
                    switch (count % 4)
                    {
                        case 0:
                            if (type == TabButtonType.Cancel || type == TabButtonType.Confirm)
                            {
                                SetCancelConfirmButton();
                            }
                            ListVisibleButtons.Insert(count - 1, CreateEmptyButton());
                            ListVisibleButtons.Insert(count - 1, CreateEmptyButton());
                            break;

                        case 1:
                            if (type == TabButtonType.Cancel || type == TabButtonType.Confirm)
                            {
                                SetCancelConfirmButton();
                            }
                            ListVisibleButtons.Insert(count - 1, CreateEmptyButton());
                            break;
                    }
                    break;

            }
            SetCancelConfirmButton();



            // Enable the "Next" button if have active buttons over index 6
            if (_pageCount > 1)
            {
                var nextButtonIndex = NextButtonIndex;
                nextButtonIndex++;
                _buttonNext = new TabButton
                {
                    Size = ListVisibleButtons[0].Size,
                    Text = _nextButtonText,
                    Index = nextButtonIndex
                };
                _buttonNext.Click += NextButtonClick;
                SetDefaultProperties(_buttonNext);

                //set properties of back button
                _buttonPrevious = new TabButton();
                var previousButtonIndex = PreviousButtonIndex;
                previousButtonIndex++;
                _buttonPrevious.Text = _previousButtonText;
                _buttonPrevious.Index = previousButtonIndex;
                _buttonPrevious.Click += PreviousButtonClick;
                SetDefaultProperties(_buttonPrevious);
            }
            SetDefaultProperties();
            SetButtonIndex();
        }

        private void SetPageCount()
        {
            if (ListButtons.Count <= ButtonsPerPage)
                _pageCount = 1;
            else
            {
                int lastIndex = 0;
                for (int i = 0; i < ListButtons.Count; i++)
                {
                    if (ListButtons[i].Enabled)
                        lastIndex = i;
                }
                if (lastIndex == 0)
                    _pageCount = 1;
                else
                {
                    _pageCount = lastIndex / ButtonsPerPage;
                    if (_pageCount == 0 || _pageCount % ButtonsPerPage > 0)
                        _pageCount++;
                }
            }
        }

        private void Next()
        {
            _currentPage++;
            NextPage();
            TabButton nextButton = null;
            if (_listIndex < ListVisibleButtons.Count)
            {
                nextButton = GetLastButton(_listIndex);

                if (nextButton != null)
                    _currentButtons.Add(nextButton);
            }
            SetButtonPositionByIndex();
            if (nextButton != null && nextButton.Index != _buttonNext.Index)
                SetButtonPositionByIndex(nextButton, _buttonNext.Index);

        }

        private void Previous()
        {
            if (_currentPage > 1)
                _currentPage--;
            PreviousPage();
        }


        /// <summary>
        /// check if a button with given name is in pannel currently is Visible
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Boolean IsButtonEnabled(string name)
        {
            var button = GetButton(name);
            if (button != null)
            {
                return button.Visible;
            }
            throw new Exception("Button not found");
        }


        public void SetButtonText(string buttonName, string text)
        {
            var button = GetButton(buttonName);
            if (button == null) return;
            if (button.Visible)
                button.Text = text;
            else
                button.Tag = text;
        }

        public TabButton GetButton(string name)
        {
            return (from btn in ListButtons where btn.Name.Equals(name) select btn).SingleOrDefault();
        }


        /// <summary>
        /// shows or hides a button with the given name.
        /// to be used only when deciding which buttons are relevant
        /// for the current scenario. For temporarely inactive buttons, use 
        /// SetButtonEnabledState
        /// </summary>
        /// <param name="name">the name of the button</param>
        /// <param name="status">true: the button is visible, false: the button is invisible</param>
        public void SetButtonVisibleState(string name, Boolean status)
        {
            var button = GetButton(name);
            SetButtonVisibleState(button, status);
        }

        private void SetButtonVisibleState(TabButton button, bool status)
        {
            if (button != null)
            {
                if (button.Visible != status)
                {
                    _refreshTabRequired = !_updateComplete;
                    //specify that this method was called when _updateComplete was false which prevented calling RefreshTab(); so call it on EndUpdate()
                    button.Visible = status;
                    if (_updateComplete)
                        RefreshTab();
                }
            }
        }

        /// <summary>
        /// change enabled status of a named button
        /// Button will be set to visible if invisible. Disabling a button will keep the visibility
        /// and location, but the text color and the response to click is changed
        /// </summary>
        /// <param name="name">the name of the button</param>
        /// <param name="enabled">true: the button is enabled, false: the button is disabled</param>
        public void SetButtonEnabledState(string name, Boolean enabled)
        {
            try
            {
                var button = GetButton(name);
                if (button != null)
                {
                    button.Enabled = enabled;
                    SetButtonVisibleState(button, true);
                }

                var confirmButton = (from btn in ListVisibleButtons where btn.ButtonType == TabButtonType.Confirm select btn).SingleOrDefault();
                if (confirmButton != null && confirmButton.Name == name)
                {
                    if (enabled)
                        ActivateHardwareEnter();
                    else
                        HardwareKeyboard.ClearEnterKeyPressedSubscription();
                }
                
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "MultiButtonControl.SetButtonEnabledState");
            }
            
        }

        /// <summary>
        /// Checks if a button with given name exists
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Exists(string name)
        {
            var btn = GetButton(name);
            return btn != null;
        }


        private void ShowButton(TabButton button, Boolean visible)
        {
            if (button.ButtonType == TabButtonType.Confirm)
                HardwareKeyboard.SubscribeEnterKeyPressed(visible ? HardwareKeyboardEnter : null);

            if (visible && button.Visible == false)
            {
                button.BackColor = _normalColor;
                button.Visible = true;
                if (string.IsNullOrEmpty(Convert.ToString(button.Tag)) == false)
                    button.Text = Convert.ToString(button.Tag);
            }

            else if (button.Visible)
            {
                button.BackColor = _blackStripColor;
                button.Visible = false;
                if (string.IsNullOrEmpty(Convert.ToString(button.Text)) == false)
                {
                    button.Tag = button.Text;
                    button.Text = string.Empty;
                }
            }
        }

        public void ActivateHardwareEnter()
        {
            HardwareKeyboard.SubscribeEnterKeyPressed(HardwareKeyboardEnter);
        }

        private void ItemScanned()
        {
            foreach (var btn in ListButtons)
            {
                switch (btn.Activation)
                {
                    case ActivateOn.IfScanned:
                        if (ScannedItemCount > 0)
                        {
                            if (btn.DisabledBecauseOfScan)
                            {
                                SetButtonEnabledState(btn.Name, true);
                            }
                        }
                        else
                        {
                            btn.DisabledBecauseOfScan = true;
                            SetButtonEnabledState(btn.Name, false);
                        }
                        break;

                    case ActivateOn.IfNotScanned:
                        if (ScannedItemCount == 0)
                        {
                            btn.DisabledBecauseOfScan = true;
                            SetButtonEnabledState(btn.Name, true);
                        }
                        else
                        {
                            btn.DisabledBecauseOfScan = true;
                            SetButtonEnabledState(btn.Name, false);
                        }
                        break;
                }
            }
            SetButtonPositionByIndex();
        }

        private void ResizePanel(bool isToShorten)
        {
            if (isToShorten && panelButtonContainer.Controls.Count < 4)
            {
                panelButtonContainer.Size = ScaleUtil.GetScaledSize(476, ButtonHeight);
                Size = ScaleUtil.GetScaledSize(476, ButtonHeight);
                Location = _tabLocation;
                Top += 15;
            }
            else
            {
                Location = new Point(_tabLocation.X, _tabLocation.Y - ScaleUtil.GetScaledPosition(52));
                Size = ScaleUtil.GetScaledSize(476, ButtonHeight * 2 + 2);
                panelButtonContainer.Size = ScaleUtil.GetScaledSize(476, ButtonHeight * 2 + 2);
                Top += 15;
            }
        }

        private void SetButtonPositionByIndex()
        {
            panelButtonContainer.Controls.Clear();

            var enabledButtons = _currentButtons;
            if (_currentButtons.Count == 0)
                return;
            if (_pageCount == 1)
            {
                enabledButtons =
                    (from btn in _currentButtons where btn.Visible && btn.Index > 3 select btn).ToList();
                if (enabledButtons.Count == 0)
                {
                    panelButtonContainer.Controls.Add(_currentButtons[0]);
                    SetButtonPositionByIndex(_currentButtons[0], 4);
                    panelButtonContainer.Controls.Add(_currentButtons[1]);
                    SetButtonPositionByIndex(_currentButtons[1], 5);
                    panelButtonContainer.Controls.Add(_currentButtons[2]);
                    SetButtonPositionByIndex(_currentButtons[2], 6);
                    ResizePanel(true);
                }
                else
                {
                    enabledButtons =
                        (from btn in _currentButtons where btn.Visible && btn.Index < 4 select btn).ToList();
                    if (enabledButtons.Count == 0)
                    {
                        if (_currentButtons.Count > 3)
                        {
                            panelButtonContainer.Controls.Add(_currentButtons[3]);
                            SetButtonPositionByIndex(_currentButtons[3], 4);
                            panelButtonContainer.Controls.Add(_currentButtons[4]);
                            SetButtonPositionByIndex(_currentButtons[4], 5);
                            panelButtonContainer.Controls.Add(_currentButtons[5]);
                            SetButtonPositionByIndex(_currentButtons[5], 6);
                        }
                        else
                        {
                            panelButtonContainer.Controls.Add(_currentButtons[0]);
                            SetButtonPositionByIndex(_currentButtons[0], 4);
                            panelButtonContainer.Controls.Add(_currentButtons[1]);
                            SetButtonPositionByIndex(_currentButtons[1], 5);
                            panelButtonContainer.Controls.Add(_currentButtons[2]);
                            SetButtonPositionByIndex(_currentButtons[2], 6);
                        }
                        ResizePanel(true);
                    }
                    else
                    {
                        ResizePanel(false);
                        int index = 1;
                        foreach (TabButton button in _currentButtons)
                        {
                            panelButtonContainer.Controls.Add(button);
                            SetButtonPositionByIndex(button, index++);
                        }
                    }
                }

                return;
            }

            if (_currentButtons.Count > 3 && _currentPage >= _pageCount && _pageCount > 1)
                enabledButtons =
                    (from btn in _currentButtons where btn.Visible && btn != _buttonPrevious select btn).ToList();

            if (_currentPage >= _pageCount && _pageCount > 1 && enabledButtons.Count < 3)
            {
                panelButtonContainer.Controls.Add(_buttonPrevious);
                SetButtonPositionByIndex(_buttonPrevious, 4);

                if (enabledButtons.Count < 2)
                {

                    var disabledButton = (from btn in _currentButtons where btn.Visible == false select btn).SingleOrDefault();
                    if (enabledButtons[0].Index == 3 || enabledButtons[0].Index == 6)
                        enabledButtons.Insert(0, disabledButton);
                    else
                        enabledButtons.Add(disabledButton);

                }
                if (enabledButtons.Count > 1 && enabledButtons[0].Index < enabledButtons[1].Index)
                {
                    panelButtonContainer.Controls.Add(enabledButtons[0]);
                    SetButtonPositionByIndex(enabledButtons[0], 5);
                    panelButtonContainer.Controls.Add(enabledButtons[1]);
                    SetButtonPositionByIndex(enabledButtons[1], 6);
                }
                else
                {
                    panelButtonContainer.Controls.Add(enabledButtons[0]);
                    SetButtonPositionByIndex(enabledButtons[0], 6);
                    panelButtonContainer.Controls.Add(enabledButtons[1]);
                    SetButtonPositionByIndex(enabledButtons[1], 5);
                }
                ResizePanel(true);
                return;
            }

            var enabledButtonCount = (from btn in _currentButtons where btn.Visible select btn).Count();
            if (_currentButtons.Count == 6 & enabledButtonCount < 4)
            {
                var buttonRow = _currentButtons.GetRange(3, 3);
                enabledButtonCount = (from btn in buttonRow where btn.Visible select btn).Count();
                if (enabledButtonCount == 0)
                    _currentButtons.RemoveRange(3, 3);
                else
                {
                    buttonRow = _currentButtons.GetRange(0, 3);
                    enabledButtonCount = (from btn in buttonRow where btn.Visible select btn).Count();
                    if (enabledButtonCount == 0)
                        _currentButtons.RemoveRange(0, 3);
                }
            }
            ResizePanel(false);
            foreach (var button in _currentButtons)
            {
                panelButtonContainer.Controls.Add(button);
            }
            if (panelButtonContainer.Controls.Count < 4)
            {
                foreach (var button in panelButtonContainer.Controls.OfType<TabButton>()
                    .Where(button => !button.IsDisposed))
                {
                    switch (button.Index)
                    {
                        case 1:
                        case 4:
                            SetButtonPositionByIndex(button, 4);
                            break;
                        case 2:
                        case 5:
                            SetButtonPositionByIndex(button, 5);
                            break;
                        case 3:
                        case 6:
                            SetButtonPositionByIndex(button, 6);
                            break;
                    }
                }
                ResizePanel(true);
            }
            else
            {
                ResizePanel(false);
                var index = 1;
                foreach (var button in panelButtonContainer.Controls.OfType<TabButton>()
                    .Where(button => !button.IsDisposed))
                {
                    SetButtonPositionByIndex(button, index++);
                }
            }
        }

        private void SetButtonPositionByIndex(Control button, int index)
        {
            int x, y;
            switch (index)
            {
                case 1:
                    x = 0;
                    y = ButtonHeight + 2;
                    button.Location = new Point(x, y);
                    break;
                case 2:
                    x = _buttonSize.Width + _buttonSpace;
                    y = ButtonHeight + 2;
                    button.Location = new Point(x, y);
                    break;
                case 3:
                    x = (_buttonSize.Width + _buttonSpace) * 2;
                    y = ButtonHeight + 2;
                    button.Location = new Point(x, y);
                    break;
                case 4:
                    button.Location = new Point(0, 0);
                    break;
                case 5:
                    x = _buttonSize.Width + _buttonSpace;
                    y = 0;
                    button.Location = new Point(x, y);
                    break;
                case 6:
                    x = (_buttonSize.Width + _buttonSpace) * 2;
                    y = 0;
                    button.Location = new Point(x, y);
                    break;
            }
        }
        /// <summary>
        /// Adds empty button to list (visible, disabled and no text)
        /// To be used if default positioning of buttons should be overridden
        /// </summary>
        public void AddEmptyButton()
        {
            ListButtons.Add(CreateEmptyButton());
        }

        private BaseForm _parentForm;
        private List<TabButton> _listVisibleButtons;

        private BaseForm ParentForm
        {
            get
            {
                if (_parentForm == null)
                {
                    var form = Parent;
                    while (form != null)
                    {
                        _parentForm = form as BaseForm;
                        if (_parentForm == null)
                            form = form.Parent;
                        else
                            break;
                    }
                }

                return _parentForm;
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// show next 6 or rest ListActiveButtons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextButtonClick(object sender, EventArgs e)
        {
            try
            {
                Next();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "MultiButtonControl.NextButtonClick");
            }
        }

        /// <summary>
        /// show previous 6 ListActiveButtons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreviousButtonClick(object sender, EventArgs e)
        {
            try
            {
                Previous();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "MultiButtonControl.PreviousButtonBack");
            }
        }

        #endregion
    }

    public enum TabButtonType
    {
        Normal = 1,
        Confirm = 2,
        Cancel = 3
    }

    public enum ActivateOn
    {
        Always,
        IfScanned,
        IfNotScanned,
    }
}

﻿using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Properties;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public class MessageHolder
    {
        
        private const string VasWarning = "VasWarning:";

        public MessageHolder()
        {
            Text = string.Empty;
            State = MessageState.Empty;
        }

        public MessageHolder(string text, MessageState state)
        {
            Text = text;
            State = state;
        }

        public void Update(string text, MessageState state)
        {
            State = state;
            Text = text;
        }

        public void Clear()
        {
            State = MessageState.Empty;
            Text = string.Empty;
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            private set
            {
                if (value != null)
                {
                    if (value.StartsWith(VasWarning) == false)
                        _text = value;
                    else
                    {
                        _text = value.Substring(11);
                        State = MessageState.Warning;
                        SoundUtil.Instance.PlayWarningSound();
                    }
                }
            }
        }


        public MessageState State { get; private set; }

        public ErrorInfo ErrorInfo { get; set; }
    }

    public enum MessageState
    {
        Empty,
        Error,
        Information,
        Warning,
        ModalMessage
    }


    public partial class MessageControl : UserControl
    {
        public MessageControl()
        {
            InitializeComponent();
            Visible = false;
        }

        private static Color _infoColor = Color.Gray;
        private Color InfoColor
        {
            get
            {
                if (_infoColor == Color.Gray)
                {
                    _infoColor = GetMessageColor(GlobalConstants.INFO_COLOR_RGB);
                }
                return _infoColor;


            }
        }

        private static Color _infoColorText = Color.Black;
        private Color InfoColorText
        {
            get
            {
                if (_infoColorText == Color.Black)
                    _infoColorText = GetMessageColor(GlobalConstants.TEXT_COLOR_INFO);
                return _infoColorText;

            }
        }


        private static Color _warningColor = Color.Gray;
        private Color WarningColor
        {
            get
            {
                if (_warningColor == Color.Gray)
                {
                    _warningColor = GetMessageColor(GlobalConstants.WARNING_COLOR_RGB);
                }
                return _warningColor;
            }
        }

        private static Color _warningColorText = Color.Black;
        private Color WarningColorText
        {
            get
            {
                if (_warningColorText == Color.Black)
                    _warningColorText = GetMessageColor(GlobalConstants.TEXT_COLOR_WARNING);
                return _warningColorText;
            }
        }

        private static Color _errorColor = Color.Gray;
        private Color ErrorColor
        {
            get
            {
                if (_errorColor == Color.Gray)
                {
                    _errorColor = GetMessageColor(GlobalConstants.ERROR_COLOR_RGB);
                }
                return _errorColor;
            }
        }

        private static Color _errorColorText = Color.Black;
        private Color ErrorColorText
        {
            get
            {
                if (_errorColorText == Color.Black)
                    _errorColorText = GetMessageColor(GlobalConstants.TEXT_COLOR_ERROR);
                return _errorColorText;
            }
        }

        private Color GetMessageColor(string commonSetting)
        {
            Color result = Color.Gray;

            try
            {
                string colourCode = SettingDataProvider.Instance.GetCommonSetting(commonSetting);
                if (string.IsNullOrEmpty(colourCode) == false)
                {
                    if (Regex.IsMatch(colourCode, @"[0x]([0-9]|[a-f]|[A-F]){6}\b"))
                    {
                        result = Color.FromArgb(Convert.ToInt32(colourCode, 16));
                    }
                }
                result = Color.Gray;// todo: remove line when server database values are corrected
                if (result == Color.Gray)// no colour found - use default
                {
                    Logger.LogEvent(Severity.Debug, "Using default color for " + commonSetting);
                    switch (commonSetting)
                    {
                        case GlobalConstants.INFO_COLOR_RGB:
                            result = Color.FromArgb(147, 225, 115);
                            break;

                        case GlobalConstants.WARNING_COLOR_RGB:
                            result = Color.FromArgb(255, 255, 185);
                            break;

                        case GlobalConstants.ERROR_COLOR_RGB:
                            result = Color.FromArgb(0xFF3333);
                            break;

                        case GlobalConstants.TEXT_COLOR_INFO:
                        case GlobalConstants.TEXT_COLOR_WARNING:
                            result = Color.Black;
                            break;

                        case GlobalConstants.TEXT_COLOR_ERROR:
                            result = Color.White;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "MessageControl.GetMessageColor");
            }
            return result;
        }



        /// <summary>
        /// Specifies the text for message as set by user.
        /// </summary>
        public string MessageText
        {
            get { return labelMessage.Text.Trim(); }
            set { labelMessage.Text = value; }
        }

        /// <summary>
        /// Specifies the type of message as set by user.
        /// </summary>
        public MessageState CurrentMessageState { get; set; }


        public MessageHolder Message
        {
            get { return new MessageHolder(MessageText, CurrentMessageState); }
            set
            {
                MessageText = value.Text;
                CurrentMessageState = value.State;
            }
        }


        public Color SetLabelFontColor
        {
            get { return labelMessage.ForeColor; }
            set { labelMessage.ForeColor = value; }
        }

        public void ShowMessage(MessageHolder messageHolder)
        {
            ShowMessage(messageHolder.Text, messageHolder.State);
            Logger.LogEvent(Severity.Info, "MessageControl.InitMessage :" + StringUtil.GetTextForConsignmentEntity(messageHolder.Text, true));
        }

        public void ClearMessage()
        {
            ShowMessage(string.Empty, MessageState.Empty);
        }

        /// <summary>
        /// Function to display scroll bar in message box if text exceeds the given dimentions.
        /// </summary>
        private void SetScrollProperties()
        {
            Graphics graphics = CreateGraphics();
            
            SizeF textArea = graphics.MeasureString(MessageText,
                                         labelMessage.Font);
            if (labelMessage.Text.EndsWith(Environment.NewLine))
            {
                // Include the height of a trailing new line in the height calculation        
                textArea.Height += graphics.MeasureString("A", labelMessage.Font).Height;
            }

            // Show the vertical ScrollBar if the text area
            // is taller than the control.
            if (Math.Ceiling(textArea.Width) >= (labelMessage.Bounds.Width) * 3.4)
            {
                touchPanel.AutoScroll = true;
                labelMessage.TextAlignment = Resco.Controls.CommonControls.Alignment.TopLeft;
                touchPanel.AutoScrollMargin = new Size(5, 5);
                var scrollToHeight = (int)((134 / 3.5) * (textArea.Width / 384));
                touchPanel.AutoScrollMinSize = new Size(0, scrollToHeight + 100);
            }
            else
            {
                touchPanel.AutoScroll = false;
                labelMessage.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleLeft;
            }
        }

    

        public void ShowMessage(string messageText, MessageState messageState)
        {
            touchPanel.SuspendLayout();
            SuspendLayout();
            MessageText = StringUtil.GetTextForConsignmentEntity(messageText, false);
            MessageText = Regex.Replace(MessageText, @"\s+", " ");
            

            SetScrollProperties();
            touchPanel.ResumeLayout(false);
            ResumeLayout();

            CurrentMessageState = messageState;

#if PROFILING
            if (ProfilingMeasurement.IsProfiling && messageState != MessageState.Empty)
            {
                ProfilingMeasurement.EndProfiling();
            }
#endif

            switch (messageState)
            {
                case MessageState.Empty:
                    Visible = false;
                    break;

                case MessageState.Error:
                    pictureBoxIcon.Image = Resources.FOT_icon_error;
                    SetBackGroundColor(ErrorColor, ErrorColorText);
                    Visible = true;
                    break;

                case MessageState.Warning:
                    pictureBoxIcon.Image = Resources.FOT_icon_attention;
                    SetBackGroundColor(WarningColor, WarningColorText);
                    Visible = true;
                    break;

                case MessageState.Information:
                    pictureBoxIcon.Image = Resources.FOT_icon_verified_ok;
                    SetBackGroundColor(InfoColor, InfoColorText);
                    Visible = true;
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Unhandled messagetype detected: " + messageState.ToString());
                    break;

            }
        }

        private void SetBackGroundColor(Color backgroundColor, Color textColor)
        {
            labelMessage.BackColor = backgroundColor;
            labelMessage.ForeColor = textColor;
            mainPanel.BackColor = backgroundColor;
            pictureBoxIcon.BackColor = backgroundColor;
        }
    }
}


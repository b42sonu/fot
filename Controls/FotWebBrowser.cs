﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public class FotWebBrowser : WebBrowser
    {
        [DllImport("coredll", SetLastError = true)]
        static extern IntPtr GetWindow(IntPtr hWnd, int wFlag);

        [DllImport("coredll", SetLastError = true)]
        static extern IntPtr GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("coredll", SetLastError = true)]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr newWndProc);

        [DllImport("coredll", SetLastError = true)]
        static extern IntPtr CallWindowProc(IntPtr lpPrevWndFunc, IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        [DllImport("coredll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);


        private delegate IntPtr WndProcDelegate(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        private readonly WndProcDelegate _functionDelegate;

        private IntPtr _hBrowserWin;
        private  IntPtr _oldWndProc;
        private readonly IntPtr _newWndProc;
        private bool _foundTarget;

        private const int GwlWndproc = (-4);
        private const int GwChild = 5;
        private const int GwHwndNext = 2;
        private const int WmContextMenu = 0x007B;
        private const int WmDestroy = 0x0002;


        public FotWebBrowser()
        {
            try
            {
                if (Handle != IntPtr.Zero)
                {
                    _hBrowserWin = FindChildWindowByParent("PIEHTML", Handle);
                    if (_hBrowserWin != IntPtr.Zero)
                    {
                        _oldWndProc = GetWindowLong(_hBrowserWin, GwlWndproc);
                        if (IsInitialized)
                        {
                            _functionDelegate = NewWndProc;
                            _newWndProc = Marshal.GetFunctionPointerForDelegate(_functionDelegate);
                            SetWindowLong(_hBrowserWin, GwlWndproc, _newWndProc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FotWebBrowser");
            }
            
        }

        private void OnDestroy()
        {
            try
            {
                if (IsInitialized)
                {
                    SetWindowLong(_hBrowserWin, GwlWndproc, _oldWndProc);
                    _hBrowserWin = IntPtr.Zero;
                    _oldWndProc = IntPtr.Zero;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FotWebBrowser.OnHandleDestroyed");
            }

        }

        private bool IsInitialized 
        {
            get { return _hBrowserWin != IntPtr.Zero && _oldWndProc != IntPtr.Zero; }
        }
        

        public IntPtr NewWndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
        {
            switch (msg)
            {
                    // Surpress the unwanted popup-menu
                case WmContextMenu:
                    return IntPtr.Zero;

                case WmDestroy:
                    OnDestroy();
                    break;

            } 

            return CallWindowProc(_oldWndProc, hWnd, msg, wParam, lParam);
        }


        private IntPtr FindChildWindowByParent(string strChildClassName, IntPtr hWndTopLevel)
        {
            IntPtr hwndCur = GetWindow(hWndTopLevel, GwChild);

            return RecurseFindWindow(strChildClassName, hwndCur);
        }

        private IntPtr RecurseFindWindow(string strChildClassName, IntPtr hWndParent)
        {
            IntPtr hwndCur = IntPtr.Zero;

            var windowClass = new StringBuilder(32);

            if (hWndParent == IntPtr.Zero)
                return IntPtr.Zero;

            //check if we get the searched class name
            GetClassName(hWndParent, windowClass, 256);

            string strWndClass = windowClass.ToString();
            _foundTarget = (strWndClass.ToLower() == strChildClassName.ToLower());
            if (_foundTarget)
                return hWndParent;


            //recurse into first child
            IntPtr hwndChild = GetWindow(hWndParent, GwChild);

            if (hwndChild != IntPtr.Zero)
                hwndCur = RecurseFindWindow(strChildClassName, hwndChild);

            if (!_foundTarget)
            {
                //enumerate each sibling windows and recurse into
                IntPtr hwndSibling;
                do
                {
                    hwndSibling = GetWindow(hWndParent, GwHwndNext);

                    hWndParent = hwndSibling;
                    if (hwndSibling != IntPtr.Zero)
                    {
                        hwndCur = RecurseFindWindow(strChildClassName, hwndSibling);
                        if (_foundTarget)
                            break;
                    }
                }

                while (hwndSibling != IntPtr.Zero);
            }
            return hwndCur;
        }
    }
}


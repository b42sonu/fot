﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;
using Resco.Controls.CommonControls;
using Timer = System.Threading.Timer;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public delegate void ViewEventDelegate(int flowState, params object[] data);

    /// <summary>
    /// This class will act as a base form for each and every form, that will be part of this application.
    /// </summary>
    public partial class BaseForm : PreComForm
    {
        /// <summary>
        /// Set to false if you dont want view to be automatically shown 
        /// when execution is regained after calling another flow
        /// </summary>
        public bool AutoShow;
        public const string Colon = ":";

        /// <summary>
        /// Flows should store results of invoked events in this varaiable
        /// </summary>
        public EventResult EventResult;
        private OnScreenKeyBoardUtil _onScreenKeyBoardUtil;

        /// <summary>
        /// Descending forms will invoke all events through this delegate
        /// As subscribee lives longer than subscriber, this is in effect a single call event
        /// subscribable by one subscriber only. This means when a new subscriber comes
        /// he will overwrite the old subscription. WIth this we avoid setting off events through
        /// flows that should have been garbage collected if it was not for the event still hold a reference to it.
        /// Removing events through -= is non-deterministic, and often not happens causing tremendous problems where
        /// view lives longer than subscriber. New subscriptions are set throug SetEventHandler()
        /// </summary>
        protected ViewEventDelegate ViewEvent { get; private set; }
        protected ViewEventDelegate ViewEventToBeInvokedAfterBusyCursor { get; private set; }

        /// <summary>
        /// Set eventhandler for view with this call. You can only have one subscriber, so new subscriptions will
        /// overwrite old. To cancel a subscription set this to null
        /// </summary>
        /// <param name="viewEvent">The function that should be invoked by event</param>
        public void SetEventHandler(ViewEventDelegate viewEvent)
        {
            ViewEventToBeInvokedAfterBusyCursor = viewEvent;
            ViewEvent = StartBusyViewEventHandler;
        }


        private void StartBusyViewEventHandler(int flowState, params object[] data)
        {
            BusyUtil.Activate();

            ViewEventToBeInvokedAfterBusyCursor.Invoke(flowState, data);
        }

        /// <summary>
        /// Constructor with single parameter
        /// </summary>
        /// <param>object for current module <name>objModule</name> </param>
        public BaseForm()
        {
            //AddIconLeftToHeaderLogo();
            //PrettifyHeader();
            //RemovePreComHeaderBar();
            InitializeComponent();
            AutoShow = true;
            EventResult = new EventResult();
        }

        /// <summary>
        /// Used to keep form topmost
        /// </summary>
        private Timer _topmostTimer;
        
#if notinuse
        
        //Don't remove. Njål
        private void PrettifyHeader()
        {
            var parentForm = (Form)Parent;
            
            foreach (Control control in parentForm.Controls)
            {
                var isHeaderLogo = control.Name.Equals("PreComMenu");
            
                if (isHeaderLogo)
                {
                    var imageHeaderLogo = ScaleUtil.GetImage(ScaleUtil.HeaderLogo);
                    var preComControlEx = (PreComControlEx)control;
                    preComControlEx.Image = imageHeaderLogo;
                    preComControlEx.Location = new Point(365, 6);
                    preComControlEx.Size = new Size(98, 44);
                }

                var isPreComControl = control.Name.Equals("lblTitle") || control.Name.Equals("pcbLogo");
                
                if (isPreComControl)
                    control.Visible = false;
            }
        }

        //Don't remove. Njål
        private void AddIconLeftToHeaderLogo()
        {
            var parentForm = (Form)Parent;

            int height = ScaleUtil.GetScaledPosition(40);
            int width = ScaleUtil.GetScaledPosition(40);
            int x = ScaleUtil.GetScaledPosition(320);
            int y = ScaleUtil.GetScaledPosition(8);

            Image infoImage = ScaleUtil.GetImage(ScaleUtil.Info);
            var buttonInfo = new PictureBox
            {
                Image = infoImage,
                Location = new Point(x, y),
                Name = "buttonInfo",
                Size = new Size(width, height)
            };
            buttonInfo.Click += ButtonInfoClick;
            parentForm.Controls.Add(buttonInfo);
            buttonInfo.BringToFront();
        }


        private void ButtonInfoClick(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("Info!");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BaseForm.ButtonInfoClick");
            }
        }

        //Don't remove. Njål
        private void RemovePreComHeaderBar()
        {
            var parentForm = (Form)Parent;
            foreach (Control control in parentForm.Controls)
            {
                Boolean controlIsHeader = control.Name.Equals("lblTop") || control.Name.Equals("pcbLogo") || control.Name.Equals("lblTitle") || control.Name.Equals("PreComMenu") || control.Name.Equals("pcbAlert");
                if (controlIsHeader)
                {
                    //Set point and size to 0, so that the PreCom calculate this form as full size and location at top. 
                    control.Size = new Size(0, 0);
                    control.Location = new Point(0, 0);
                    control.Visible = false;
                }
            }
        }
#endif

        /// <summary>
        /// This is a virtual function called when screen is refreshed
        /// Should not be called directly as the busy cursor will not end
        /// </summary>
        public virtual void OnUpdateView(IFormData iformData) { }

        public void UpdateView(IFormData iformData)
        {
            BusyUtil.Reset();
            OnUpdateView(iformData);
        }

        public virtual void OnShow(IFormData formData)
        {
        }

        public virtual void ShowingView()
        {
            BusyUtil.Reset();
        }

        /// <summary>
        /// Called after components has been initialized
        /// </summary>
        public void OnCreate(BaseModule module)
        {
            _onScreenKeyBoardUtil = new OnScreenKeyBoardUtil(module);
        }

        public void EnableOnScreenKeyboard()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Keyboard))
            {
                using (ProfilingMeasurement.MeasureFunction("BaseForm.DisableOnScreenKeyboard"))
                {
                    _onScreenKeyBoardUtil.OnViewCreate(Controls);
                }
            }
        }

        /// <summary>
        /// Called when form is displayed
        /// </summary>
        public void OnResume()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Keyboard))
            {
                using (ProfilingMeasurement.MeasureFunction("BaseForm.OnResume"))
                {
                    _onScreenKeyBoardUtil.OnViewResume(Controls);
                }
            }
        }

        public void ShowOnScreenKeyboard()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Keyboard))
            {
                using (ProfilingMeasurement.MeasureFunction("BaseForm.HideOnScreenKeyboard"))
                {
                    if(_onScreenKeyBoardUtil != null)
                        _onScreenKeyBoardUtil.ShowOnScreenKeyboard();
                }
            }
        }

        public void HideOnScreenKeyboard()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Keyboard))
            {
                using (ProfilingMeasurement.MeasureFunction("BaseForm.HideOnScreenKeyboard"))
                {
                    _onScreenKeyBoardUtil.HideOnScreenKeyboardIcon();
                }
            }
        }

        public void DisableOnScreenKeyboard()
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Keyboard))
            {
                using (ProfilingMeasurement.MeasureFunction("BaseForm.DisableOnScreenKeyboard"))
                {
                    _onScreenKeyBoardUtil.DisableOnScreenKeyboard(Controls);
                    HardwareKeyboard.ClearEnterKeyPressedSubscription();
                }
            }
        }

        public void InputControlClicked(Control inputControl)
        {
            _onScreenKeyBoardUtil.InputControlClicked(inputControl);
        }

        /// <summary>
        /// Called when window is closed (in reality hidden). Override to do cleanup
        /// </summary>
        public virtual void OnClose()
        {
        }

        public void FocusFirstInputControl()
        {
            _onScreenKeyBoardUtil.FocusInputControl();
        }

        protected void SetKeyboardState(KeyboardState state)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Keyboard))
            {
                using (ProfilingMeasurement.MeasureFunction("BaseForm.SetKeyboardState " + state))
                {
                    try
                    {
                        HardwareKeyboard.Instance.SetState(state);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogExpectedException(ex, GetType().Name);
                    }
                }
            }
        }

        /// <summary>
        /// uniforms controls in similar views
        /// if parameter is null, correction of this element is skipped
        /// </summary>
        /// <param name="lblProcess">First line in heading, process name</param>
        /// <param name="lblOrgUnit">Second line in heading, Location</param>
        /// <param name="lblScanHeader">Text above scanning field</param>
        /// <param name="txtScannedNumber">scanning field</param>
        /// <param name="msgMessage">Messagebox for errors, information and warning</param>
        /// <param name="lblItemCount">Item count line</param>
        /// <param name="lblAdditionalInfo">line avove Item count line</param>
        protected void SetStandardControlProperties(
                                            TransparentLabel lblProcess,
                                            TransparentLabel lblOrgUnit,
                                            TransparentLabel lblScanHeader,
                                            PreComInput2 txtScannedNumber,
                                            MessageControl msgMessage,
                                            TransparentLabel lblItemCount,
                                            TransparentLabel lblAdditionalInfo
            )
        {
            var boldFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            var regularFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            if (lblProcess != null)
            {
                lblProcess.AutoSize = false;
                lblProcess.Font = boldFont;
                lblProcess.Location = new System.Drawing.Point(0, 3 - 1);
                lblProcess.Size = new System.Drawing.Size(480, 30);
                lblProcess.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            }
            if (lblOrgUnit != null)
            {
                lblOrgUnit.AutoSize = false;
                lblOrgUnit.Font = boldFont;
                lblOrgUnit.Location = new System.Drawing.Point(0, 35);
                lblOrgUnit.Size = new System.Drawing.Size(480, 33);
                lblOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            }

            if (lblScanHeader != null)
            {
                lblScanHeader.Font = regularFont;
                lblScanHeader.AutoSize = false;
                lblScanHeader.Location = new System.Drawing.Point(21, 71);
                lblScanHeader.Size = new System.Drawing.Size(438, 30);
            }

            if (txtScannedNumber != null)
            {
                txtScannedNumber.Font = regularFont;
                txtScannedNumber.Location = new System.Drawing.Point(21, 108 - 3);
                txtScannedNumber.Size = new System.Drawing.Size(438, 40);
                txtScannedNumber.BringToFront();
            }

            if (msgMessage != null)
            {
                msgMessage.Location = new System.Drawing.Point(21, 156 - 8);
                msgMessage.Size = new System.Drawing.Size(438, 110);
            }

            if (lblAdditionalInfo != null)
            {
                lblAdditionalInfo.AutoSize = false;
                lblAdditionalInfo.Size = new System.Drawing.Size(438, 30);
                lblAdditionalInfo.Location = new System.Drawing.Point(21, 385);
                lblAdditionalInfo.BringToFront();
            }

            if (lblItemCount != null)
            {
                lblItemCount.AutoSize = false;
                lblItemCount.Size = new System.Drawing.Size(438, 30);
                lblItemCount.Location = new System.Drawing.Point(21, 415);
                lblItemCount.BringToFront();
            }


        }

        //  for testing 
        protected void LoggControlLocations(Control touchpanel)
        {
            foreach (Control control in touchpanel.Controls)
            {
                Logger.LogEvent(Severity.Debug, "controls;"
                    + control.Name + ","
                    + control.Height + ","
                    + control.Width + ","
                    + control.Left + ","
                    + control.Top + ","
                     + control.Text
                    );
            }
        }

        public Control ActiveControl
        {
            get
            {
                return GetFocusedControl(this);
            }

            set
            {
                if (!value.Focused)
                {
                    value.Focus();
                }
            }

        }

        private Control GetFocusedControl(Control parent)
        {
            if (parent.Focused)
            {
                return parent;
            }

            return (from Control ctrl in parent.Controls select GetFocusedControl(ctrl)).FirstOrDefault(temp => temp != null);
        }


        public Action OnSetVisible;
        
        public new bool Visible
        {
            get { return base.Visible; }


            set
            {
                base.Visible = value;
                if (OnSetVisible != null)
                    OnSetVisible();
            }
        }

        public static BaseForm GetParentForm(Control control)
        {
            try
            {
                if (control.IsDisposed == false)
                {
                    var form = control.Parent;
                    while (form != null && form.IsDisposed == false)
                    {
                        var parentForm = form as BaseForm;
                        if (parentForm == null)
                            form = form.Parent;
                        else
                            return parentForm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BaseForm.GetParentForm");
            }

            return null;
        }

        internal void FocusInputControl()
        {
            if (_onScreenKeyBoardUtil != null)
                _onScreenKeyBoardUtil.FocusInputControl();
        }

        private bool _keepTopmost;
        protected bool KeepTopmost
        {
            get { return _keepTopmost; }
            set
            {
                if (value != _keepTopmost)
                {
                    _keepTopmost = value;
                    if (_keepTopmost)
                    {
                        _topmostTimer = new Timer(OnTopmostTimer, null, 400, Timeout.Infinite);
                    }
                    else
                    {
                        if (_topmostTimer != null)
                        {
                            _topmostTimer.Dispose();
                            _topmostTimer = null;
                        }
                    }
                }
            }
        }

        private void OnTopmostTimer(object arg)
        {
            try
            {
                if (InvokeRequired && IsDisposed == false)
                    Invoke((Action)(() => OnTopmostTimer(null)));
                else
                {
                    if (Visible && KeepTopmost)
                    {
                        BringToFront();
                        // Restart timer
                        _topmostTimer.Change(400, Timeout.Infinite);
                    }
                }
            }

// ReSharper disable EmptyGeneralCatchClause
            // Just eat execption. Even if checking for IsDisposed,
            // we sometimes get IsDisposedException
            catch (Exception)
            {}
// ReSharper restore EmptyGeneralCatchClause            
        }
    }

    public interface IFormData
    {
        string HeaderText { get; }
    }

    public abstract class BaseFormData : IFormData
    {
        public string HeaderText { get; set; }
        public string OrgUnitName { get; set; }
    }
}

﻿using System;
using System.Globalization;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{

    /// <summary>
    /// This class 
    /// </summary>
    public class BaseConsignmentEntityDetailForm : BaseForm
    {
        private HtmlUtil _hmtlUtil;

        #region Html detail
        public string CreateConsignmentDetailHtml(PlannedConsignmentsType plannedConsignmentsType, StopType stopInformation)
        {
            _hmtlUtil = new HtmlUtil();
            if (plannedConsignmentsType != null)
            {
                CreateConsigneeHtml(plannedConsignmentsType.Consignee);
                CreateConsignorHtml(plannedConsignmentsType.Consignor, stopInformation);
                CreatePlannedConsignmentTypeHtml(plannedConsignmentsType, stopInformation); 
            }

            return _hmtlUtil.GetResult();
        }

        /// <summary>
        /// Create HTML for Consignment Detail
        /// Created for Operation List module and formatting according to the page '
        /// </summary>
        /// <returns>Html output as String</returns>
        public string CreateConsignmentDetailForOperationListHtml(PlannedConsignmentsType plannedConsignmentsType, StopType stopInformation)
        {
            _hmtlUtil = new HtmlUtil();
            if (plannedConsignmentsType != null)
            {
                AddOrderAndTimeInfo(plannedConsignmentsType, stopInformation);
                CreateConsignorHtml(plannedConsignmentsType.Consignor, stopInformation);
                CreateConsigneeHtml(plannedConsignmentsType.Consignee);
                CreatePlannedConsignmentTypeHtml(plannedConsignmentsType, stopInformation);
            }

            return _hmtlUtil.GetResult();
        }

        /// <summary>
        /// Adds Order and Time Frame Information '
        /// </summary>
        private void AddOrderAndTimeInfo(PlannedConsignmentsType plannedConsignmentsType, StopType stopInformation)
        {
            _hmtlUtil.AddTitle(GlobalTexts.OrderNo + " :" + "    ");
            _hmtlUtil.Append(plannedConsignmentsType.OrderNumber);
            _hmtlUtil.Newline();
            
            if (stopInformation != null)
            {
                _hmtlUtil.AddTitle(GlobalTexts.TimeFrame + " :" + "    ");
                _hmtlUtil.Append(stopInformation.EarliestStopTime + " - " + stopInformation.LatestStopTime);
                _hmtlUtil.Newline();
            }
        
        }

        private void CreatePlannedConsignmentTypeHtml(PlannedConsignmentsType plannedConsignmentsType, StopType stopInformation)
        {
            if (plannedConsignmentsType != null)
            {
                if (stopInformation != null && stopInformation.Description != null)
                {
                    _hmtlUtil.AddTitle(GlobalTexts.DriverInstruction + ":");
                    _hmtlUtil.Append(stopInformation.AdditionalInfo);
                    _hmtlUtil.Newline();
                }

                _hmtlUtil.StartTable();
                _hmtlUtil.AddToSimplTable(GlobalTexts.NumberOfItems,
                                                      plannedConsignmentsType.ConsignmentItemCount.ToString(
                                                          CultureInfo.InvariantCulture));

                if (plannedConsignmentsType.Weight != null)
                {
                    String weight = String.Format("{0} {1}", plannedConsignmentsType.Weight.Amount,
                                                  plannedConsignmentsType.Weight.MeasureUnit);
                    _hmtlUtil.AddToSimplTable(GlobalTexts.Weight, weight);
                }

                if (plannedConsignmentsType.Volume != null)
                {
                    String volume = String.Format("{0} {1}", plannedConsignmentsType.Volume.Amount,
                                                  plannedConsignmentsType.Volume.MeasureUnit);
                    _hmtlUtil.AddToSimplTable(GlobalTexts.Volume, volume);
                }

                if (plannedConsignmentsType.ProductCategory != null)
                    _hmtlUtil.AddToSimplTable(GlobalTexts.Temperature,
                                                          plannedConsignmentsType.ProductCategory);
                
                //Additional property added as per discussion with Merete , will be used in future
                //_hmtlUtil.AddToSimplTable(GlobalTexts.Vas, "");

                _hmtlUtil.EndTable();
            }
        }

        

        private void CreateConsignorHtml(ConsignorType consignor, StopType stopInformation)
        {
            if (consignor != null)
            {
                _hmtlUtil.AddTitle(GlobalTexts.Consignor + ":");
                if (consignor.Name1 != null)
                    _hmtlUtil.Append(consignor.Name1);

                if (consignor.Name2 != null)
                {
                    _hmtlUtil.Append(", ");
                    _hmtlUtil.Append(consignor.Name2);
                }

                if (consignor.Address1 != null)
                {
                    _hmtlUtil.Append(", ");
                    _hmtlUtil.Append(consignor.Address1);
                }

                if (consignor.PostalCode != null)
                {
                    _hmtlUtil.Append(", ");
                    _hmtlUtil.Append(consignor.PostalCode);
                }

                _hmtlUtil.Newline();

                if (consignor.PostalName != null)
                {
                    _hmtlUtil.Append(consignor.PostalName);
                    _hmtlUtil.Newline();
                }

                if (stopInformation != null && stopInformation.CustomerContact != null && stopInformation.PhoneNumber != null)
                {
                    _hmtlUtil.Newline();
                    _hmtlUtil.Append(String.Format("{0}, {1}", stopInformation.CustomerContact, stopInformation.PhoneNumber));
                    _hmtlUtil.Newline();
                }
            }
        }

        private void CreateConsigneeHtml(ConsigneeType consignee)
        {
            if (consignee != null)
            {
                _hmtlUtil.AddTitle(GlobalTexts.Consignee + ":");
                if (consignee.Name1 != null)
                    _hmtlUtil.Append(consignee.Name1);


                if (consignee.Name2 != null)
                {
                    _hmtlUtil.Append(", ");
                    _hmtlUtil.Append(consignee.Name2);
                }

                if (consignee.Address1 != null)
                {
                    _hmtlUtil.Append(", ");
                    _hmtlUtil.Append(consignee.Address1);
                }

                if (consignee.PostalCode != null)
                {
                    _hmtlUtil.Append(", ");
                    _hmtlUtil.Append(consignee.PostalCode);
                }

                _hmtlUtil.Newline();

                if (consignee.PostalName != null)
                {
                    _hmtlUtil.Append(consignee.PostalName);
                    _hmtlUtil.Newline();
                }
                if (consignee.PhoneNumber != null)
                {
                    _hmtlUtil.Newline();
                    _hmtlUtil.Append(consignee.PhoneNumber);
                    _hmtlUtil.Newline();
                }
            }
        }

        public string CreateConsignmentItemDetailHtml(ConsignmentItem currentConsignment)
        {
            var consignmentItem = currentConsignment.PlannedConsignmentItem;

            _hmtlUtil = new HtmlUtil();

            if (consignmentItem != null)
            {
                CreateConsingnmentItemProductionEvents(consignmentItem);
                _hmtlUtil.StartTable();
                if (consignmentItem.Weight != null)
                {
                    String weight = String.Format("{0} {1}", consignmentItem.Weight.Amount,
                                                  consignmentItem.Weight.MeasureUnit);
                    _hmtlUtil.AddToSimplTable(GlobalTexts.Weight, weight);
                }

                if (consignmentItem.Volume != null)
                {
                    String volum = String.Format("{0} {1}", consignmentItem.Volume.Amount,
                                                 consignmentItem.Volume.MeasureUnit);
                    _hmtlUtil.AddToSimplTable(GlobalTexts.Volume, volum);
                }
                _hmtlUtil.EndTable();
            }
            return _hmtlUtil.GetResult();
        }

        private void CreateConsingnmentItemProductionEvents(ConsignmentItemType consignmentItemType)
        {
            if (consignmentItemType.EventTypeList != null && consignmentItemType.EventTypeList.Length > 0)
            {
                _hmtlUtil.AddTitle(GlobalTexts.LastProductionEvent + ":");
                foreach (var eventType in consignmentItemType.EventTypeList)
                {
                    if (eventType != null)
                        _hmtlUtil.Newline(String.Format("{0}, {1}, {2}", eventType.EventCode, eventType.EventTime, eventType.LocationId));
                }
                _hmtlUtil.Newline();
            }
        }

        #endregion

    }
}
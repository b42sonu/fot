﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    partial class MessageControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support  do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxIcon = new System.Windows.Forms.PictureBox();
            this.labelMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.mainPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.labelMessage)).BeginInit();
            this.touchPanel.SuspendLayout();
            this.SuspendLayout();
       
            // 
            // pictureBoxIcon
            // 

            this.pictureBoxIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxIcon.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new System.Drawing.Size(104-24, 141);
            this.pictureBoxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            // 
            // labelMessage
            // 
            this.labelMessage.AutoSize = false;
            this.labelMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMessage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelMessage.Location = new System.Drawing.Point(65, 2);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(384, 134);
            this.labelMessage.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleLeft;
         
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.labelMessage);
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.Vertical;
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Name = "touchPanel";
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.mainPanel.Width = 10;
            this.mainPanel.Name = "panelRight";

            this.Controls.Add(this.pictureBoxIcon);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.touchPanel);
            this.touchPanel.BringToFront();
            // 
            // MessageControl
            // 
            this.BorderStyle = BorderStyle.FixedSingle;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.Name = "MessageControl";
            this.Size = new System.Drawing.Size(436, 141);
            ((System.ComponentModel.ISupportInitialize)(this.labelMessage)).EndInit();
            this.touchPanel.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxIcon;
        private Resco.Controls.CommonControls.TransparentLabel labelMessage;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private System.Windows.Forms.Panel mainPanel;
    }
}


﻿using System;
using System.Linq;
using System.Windows.Forms;
using PreCom.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Controls
{
    public class PositiveIntTextBox : PreComInput2
    {
        // Restricts the entry of characters to digits (including hex), the negative sign, 
        // the decimal point, and editing keystrokes (backspace). 
        // inspired by :
        // http://msdn.microsoft.com/en-us/library/ms229644%28v=vs.90%29.aspx
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);

            if (Char.IsDigit(e.KeyChar))
            {
                // Digits are OK
            }
            else if (e.KeyChar == '\b')
            {
                // Backspace key is OK
            }
            else
            {
                // Consume this invalid key and beep
                e.Handled = true;
                //    MessageBeep();
            }
        }

        public int IntValue
        {
            get
            {
                var result = Text.Where(Char.IsDigit)
                    .Aggregate("", (current, chr) => current + chr);
                return Text.Length==0 ? 0 : Int32.Parse(result);
            }
        }


       
    }
}

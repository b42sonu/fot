﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Bring.PMP.PreComFW.Shared
{
    public class PasswordChangedMessage : PreCom.Core.Communication.EntityBase
    {
        public string UserId { get; set; }
    }
}

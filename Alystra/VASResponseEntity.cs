using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Alystra
{
    public class VASResponseEntity : EntityBase
    {
        public string VASResponse { get; set; }
    }
}
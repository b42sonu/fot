﻿using System;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList
{
    public partial class PdaDeviation
    {
        private bool _isAddedDeviation;

        /// <remarks/>
        public bool IsAddedDeviation
        {
            get
            {
                return _isAddedDeviation;
            }
            set
            {
                _isAddedDeviation = value;
            }
        }       
    }
}

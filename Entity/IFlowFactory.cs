﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public enum FlowType
    {
        Loading,
        Unloading,
        LoadList,
        LoadLineHaulTruck,
        LoadDistributionTruck,
        UnloadLineHaulTruck,
        UnloadPickUpTruck,
        LoadCombinationTrip
    }

    public enum ModuleType
    {
        Goods,
        OperationList,
    }

    public interface IFlowFactory
    {
        BaseFlow CreateFlow(FlowType flowType);
    }
}

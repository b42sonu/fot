﻿using System;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList
{
    public partial class PlannedConsignmentsType
    {

        //As OperationList detail object and ValidateGoods detail object is diffrent, this method convert RetriveProductGroup object to OperationList object
        public void SetDataFromValidateGoodsResponse(ValidationResponseConsignment responseConsignment)
        {

            if (responseConsignment != null)
            {
                if (responseConsignment.Consignee != null)
                {
                    Consignee = new ConsigneeType
                    {
                        Name1 = responseConsignment.Consignee.Name1,
                        Address1 = responseConsignment.Consignee.Address1,
                        PostalCode = responseConsignment.Consignee.PostalCode,
                        PostalName = responseConsignment.Consignee.PostalName
                    };
                }

                if (responseConsignment.Consignor != null)
                {
                    Consignor = new ConsignorType
                    {
                        Name1 = responseConsignment.Consignor.Name1,
                        Address1 = responseConsignment.Consignor.Address1,
                        PostalCode = responseConsignment.Consignor.PostalCode,
                        PostalName = responseConsignment.Consignor.PostalName
                    };
                }

                ConsignmentItemCount = responseConsignment.ConsignmentItemCount;

                Weight = new MeasureType
                {
                    Amount = responseConsignment.Weight,
                    MeasureUnit = responseConsignment.WeightUnit
                };

                Volume = new MeasureType
                {
                    Amount = responseConsignment.Volume,
                    MeasureUnit = responseConsignment.VolumeUnit
                };

                ProductCategory = responseConsignment.ProductCategory;

                int i = 0;
                if (responseConsignment.PackageType != null)
                {
                    PackageType = new PackageTypeType[responseConsignment.PackageType.Length];
                    foreach (var responsePackageType in responseConsignment.PackageType)
                    {
                        if (responsePackageType != null)
                        {
                            var packageType = new PackageTypeType
                            {
                                NumberOf = GetNumber(responsePackageType.NumberOf),
                                Type = responsePackageType.Type
                            };

                            PackageType[i] = packageType;
                        }
                        i++;

                    }
                }

                i = 0;
                if (responseConsignment.TransportEquipment != null)
                {
                    TransportEquipment = new TransportEquipmentType[responseConsignment.TransportEquipment.Length];
                    foreach (var responseTransportEquipment in responseConsignment.TransportEquipment)
                    {
                        if (responseTransportEquipment != null)
                        {
                            var transportEquipmentType = new TransportEquipmentType
                            {
                                NumberOf = GetNumber(responseTransportEquipment.NumberOf),
                                Type = responseTransportEquipment.Type
                            };

                            TransportEquipment[i] = transportEquipmentType;
                        }
                        i++;
                    }
                }

                //Fill the items from the server to planned consignment
                var listType = new List<ConsignmentItemType>();
                if (responseConsignment.ConsignmentItem != null)
                    listType.AddRange(responseConsignment.ConsignmentItem.Select(item => new ConsignmentItemType() { ConsignmentItemNumber = item.ConsignmentItemNumber }));
                ConsignmentItem = listType.ToArray();


            }


        }

        private short GetNumber(string numberOf)
        {
            try
            {
                return Convert.ToInt16(numberOf);
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        public string GetConsigneeDetails()
        {
            var addressDetails = new StringBuilder();

            if (Consignee == null)
                addressDetails.ToString();
            else
            {
                if (!String.IsNullOrEmpty(Consignee.DeliveryAddress1))
                {
                    addressDetails.Append(Consignee.DeliveryAddress1);
                }
                else if (!String.IsNullOrEmpty(Consignee.Address1))
                {
                    addressDetails.Append(Consignee.Address1);
                }

                if (!String.IsNullOrEmpty(Consignee.DeliveryPostalCode))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(Consignee.DeliveryPostalCode);
                    //addressDetails.Append(Environment.NewLine + Consignee.DeliveryPostalCode);
                }
                else if (!String.IsNullOrEmpty(Consignee.PostalCode))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(Consignee.PostalCode);
                    //addressDetails.Append(Environment.NewLine + Consignee.PostalCode);
                }




                if (!String.IsNullOrEmpty(Consignee.DeliveryPostalName))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(" " + Consignee.DeliveryPostalName);
                }
                else if (!String.IsNullOrEmpty(Consignee.PostalName))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(Consignee.PostalName);
                    //addressDetails.Append(Environment.NewLine + Consignee.PostalName);
                }



                if (!String.IsNullOrEmpty(Consignee.DeliveryCountryCode))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(" " + Consignee.DeliveryCountryCode);
                }
                else if (!String.IsNullOrEmpty(Consignee.CountryCode))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(Consignee.CountryCode);
                    //addressDetails.Append(Environment.NewLine + Consignee.CountryCode);
                }

                /*else
                {
                    if (!String.IsNullOrEmpty(Consignee.DeliveryPostalName))
                    {
                        addressDetails.Append(Environment.NewLine + Consignee.DeliveryPostalName);
                    }
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    if (!String.IsNullOrEmpty(Consignee.DeliveryCountryCode))
                    {
                        addressDetails.Append(" " + Consignee.DeliveryCountryCode);
                    }
                }*/
                return addressDetails.ToString();
            }
            return string.Empty;
        }

        public string GetConsigneeName()
        {
            string name = string.Empty;
            if (Consignee != null)
                name = Consignee.Name1;
            return name;
        }

        public string GetConsigneeNameAndDetail()
        {
            string nameAndDetail = string.Empty;
            string name = GetConsigneeName();
            if (string.IsNullOrEmpty(name) == false)
            {
                nameAndDetail = name + "," + GetConsigneeDetails();
            }
            if (string.IsNullOrEmpty(nameAndDetail))
                nameAndDetail = ConsignmentNumber;
            return nameAndDetail;
        }

        public string GetOrderInfo()
        {
            bool hasNotes = operationNotesField != null &&
                                 operationNotesField.Length > 0;
            if (hasNotes)
            {
                return operationNotesField[0].Text;
            }
            return String.Empty;
        }
    }
}

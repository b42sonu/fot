﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class OrgUnit
    {
        public string OrgNumber { get; set; }
        public string OrgName { get; set; }
        public string PostalNumber { get; set; }
        public string CountryCode { get; set; }
        public string LocationId { get; set; }

    }
}

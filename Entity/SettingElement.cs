﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    /// <summary>
    /// Represent bring setting
    /// </summary>
    public class SettingElement
    {
        /// <summary>
        /// Gets or sets setting name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets sets setting value.
        /// </summary>
        public string Value { get; set; }
    }
}
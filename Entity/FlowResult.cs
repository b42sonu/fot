﻿
using System;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public enum FlowResultState
    {
        Ok = 0,
        Cancel,
        Warning,
        Error,
        Finished,
        Duplicate,
        RequestMoreOptions,
        Reconcilliation,
        Exception,
        
        AttemptedPickup,
        AttemptedDelivery
    }

    public enum ErrorInfo
    {
        NoError,
        IllegalBarcode,
        NotAuthorised,
        ShipmentStopped
    }

    public interface IFlowResult
    {
        FlowResultState State { get; set; }
    }

    public abstract class BaseFlowResult : IFlowResult
    {
        public String Message { get; set; }
        public FlowResultState State { get; set; }
        public IFlowResult InnerFlowResult { get; set; }
        public bool MessageIsFromVasMatrix { get; set; } // Set to true if message is set by VasMatrix flow
    }
}

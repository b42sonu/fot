﻿

using Com.Bring.PMP.PreComFW.Shared.Barcode;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class FlowResultLoadUnload : BaseFlowResult
    {
        public WorkListItem SelectedWorkListItem { get; set; }
        public BarcodeType ScannedbarCodeType { get; set; }
        public BarcodeRouteCarrierTrip Rbt { get; set; }
        public string LoadCarrier { get; set; }
        public OperationType SelectedProcessToStart { get; set; }
        public bool IsToStartAnotherProcess { get; set; }
        public bool IsToActivateNewWorkListItem { get; set; }
        public bool IsLoadCarrierKnownInLm { get; set; }
    }
}

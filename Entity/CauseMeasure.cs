﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    namespace Com.Bring.PMP.PreComFW.Shared.Entity
    {
        public class CauseMeasure
        {
            public string CauseCode { get; set; }
            public string MeasureCode { get; set; }

            //two below added for description value for cause and measure...
            public string MeasureDescription { get; set; }
            public string CauseDescription { get; set; }

            //two below added for code+description value for cause and measure...
            public string MeasureCodeAndMeasureDescription { get; set; }
            public string CauseCodeAndCauseDescription { get; set; }
            public string LangCode { get; set; }

            public CauseMeasure Clone()
            {
                var result = (CauseMeasure)MemberwiseClone();
                return result;
            }

        }
    }
}

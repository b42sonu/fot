﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public enum OperationListTab
    {
        NotStarted,
        Completed,
        All
    }

    public enum OperationTypeImage
    {
        Multiple = 3,
        ExpandMultiple = 8,
        Unload = 4,
        Load = 5,
        PickUp = 6,
        Delivery = 7,
        ReturnedDelivery = 9,
        LoadLineHaul = 10,
        UnloadLineHaul = 11,
        UnloadPickUpTruck = 12,
        LoadDistributionTruck = 13
    }

    public enum StopOperationStatusImage
    {
        Completed = 0,
        NotStarted = 1,
        Started = 2
    }

    public enum UpdateOperationListImageType
    {
        New = 0,
        Deleted = 1,
        Updated = 2
    }

    public enum AdvancedListTemplate
    {
        RowTemplateStop = 1,
        RowTemplateStopSelectedTemp = 2,
        RowTemplatePlanOper = 3,
        RowTemplatePlanOperSelectedTemp = 4,
        RowTemplateStopSingleOperSelectedTemp = 5

    }

    public class BindingStopAmphora
    {
        public string StopId { get; set; }

        public int StopStatus { get; set; }

        public String StopHeader
        {
            get
            {
                if (string.IsNullOrEmpty(StopTimeInfo) == false)
                    return StopTimeInfo + Environment.NewLine + StopAddress;
                return StopAddress;
            }
        }

        public string StopAddress { get; set; }

        public string StopSign { get; set; }

        public string AlternateStopSign { get; set; }

        public String StopTimeInfo { get; set; }

        public string OperationId { get; set; }

        public string OrderNumber { get; set; }

        //added more properties for merging load list operation..
        public bool IsLoadListOperation { get; set; }
        public PlannedConsignmentsType LoadListPlannedConsignment { get; set; }

        public int DisplaySortOrder { get; set; }
        public int OrignalSortOrder { get; set; }
        public int DeliveryDefaultSortOrder { get; set; }
        public ConsigneeType Consignee { get; set; }
        public DateTime StopEarliestTime { get; set; }
        public string PhysicalLoadCarrier { get; set; }
        public string LogicalLoadCarrier { get; set; }
        public string TripId { get; set; }
        public string ExternalTripId { get; set; }
        public string RouteId { get; set; }

        public StopType StopType { get; set; }
    }
}

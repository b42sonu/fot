﻿using Com.Bring.PMP.PreComFW.CommunicationEntity;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class ReasonType : IStatus
    {
        public string LangCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return Description;
        }

        public ReasonType Clone()
        {
            ReasonType result = (ReasonType)MemberwiseClone();
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class VasHistoryItem : IEquatable<VasHistoryItem>
    {
        public VasHistoryItem(ConsignmentEntity consignmentEntity)
        {
            ConsignmentId = consignmentEntity.ConsignmentId;
            var consignmentItem = consignmentEntity as ConsignmentItem;
            if (consignmentItem != null)
            {
                ConsignmentItemId = consignmentItem.ItemId;
            }
        }

        public VasHistoryItem(string consignmentId)
        {
            ConsignmentId = consignmentId;
        }

        public string ConsignmentId;
        public string ConsignmentItemId;
        public bool Equals(VasHistoryItem other)
        {
            return ConsignmentId == other.ConsignmentId && (ConsignmentItemId == other.ConsignmentItemId || other.ConsignmentItemId == null);
        }
    }

    public class VasHistory : Dictionary<int, List<VasHistoryItem>>
    {
        internal VasHistory(){}

        public void DeleteProcessHistoryForConsignmentEntity(ConsignmentEntity consignmentEntity)
        {
            if (consignmentEntity != null)
            {
                var processSupportIdsToRemove = new List<int>();
                var vasHistoryItem = new VasHistoryItem(consignmentEntity);

                foreach (var processSupportId in Keys)
                {
                    List<VasHistoryItem> vasHistoryItems;
                    if (TryGetValue(processSupportId, out vasHistoryItems))
                    {
                        if (vasHistoryItems.Remove(vasHistoryItem))
                        {
                            if (vasHistoryItems.Count == 0)
                                processSupportIdsToRemove.Add(processSupportId);
                        }
                    }
                }

                foreach (var processId in processSupportIdsToRemove)
                {
                    Remove(processId);
                }
            }
        }

        internal void AddUsedProcessSupportId(int processSupportId, ConsignmentEntity consignmentEntity)
        {
            List<VasHistoryItem> invokedEntities;

            if (TryGetValue(processSupportId, out invokedEntities) == false)
            {
                invokedEntities = new List<VasHistoryItem>();
                Add(processSupportId, invokedEntities);
            }

            if (consignmentEntity != null)
            {
                var vasHistoryItem = new VasHistoryItem(consignmentEntity);

                if (invokedEntities.Contains(vasHistoryItem) == false)
                    invokedEntities.Add(vasHistoryItem);
            }
        }

        internal bool IsProcessSupportShownBefore(int processSupportId)
        {
            // If we have shown this process support before, we finsh here...
            return ContainsKey(processSupportId);
        }

        internal bool IsProcessSupportForConsignmentShownBefore(int processSupportId, string consignmentId)
        {
            bool result = false;
            List<VasHistoryItem> invokedConsignments;
            var vasHistoryItem = new VasHistoryItem(consignmentId);
            if (TryGetValue(processSupportId, out invokedConsignments))
            {
                if (invokedConsignments.Contains(vasHistoryItem))
                    result = true;
            }
            return result;
        }
    }
}
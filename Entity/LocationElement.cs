﻿
namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class LocationElement
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string PostalNumber { get; set; }
        public string CountryCode { get; set; }
        public string NameAndNumber
        {
            get
            {
                if (string.IsNullOrEmpty(Name))
                    Name = string.Empty;
                if (string.IsNullOrEmpty(Number))
                    Number = string.Empty;
                return string.Format("{0} ({1})", Name, Number);
            }
        }

        public string NameNumberCode
        {
            get
            {
                if (string.IsNullOrEmpty(CountryCode))
                    CountryCode = string.Empty;
                if (string.IsNullOrEmpty(Name))
                    Name = string.Empty;
                if (string.IsNullOrEmpty(Number))
                    Number = string.Empty;
                if (string.IsNullOrEmpty(PostalNumber))
                    PostalNumber = string.Empty;
                return string.Format("{0} {1} {2} {3}", CountryCode, Number,PostalNumber, Name);
            }
        }

        public LocationElement Clone()
        {
            var location = (LocationElement)MemberwiseClone();
            return location;
        }


    }
}

﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class FlowDataModuleMenu: BaseFlowData
    {
    }

    public class FlowDataLoadUnload : BaseFlowData
    {
        public WorkListItem SelectedWorkListItem;
        public PlannedConsignmentsType[] SelectedConsignments;
        public string PowerUnitId;
        public string LoadCarrierId;
        public bool IsToActivateWorkListItem;
        public BarcodeRouteCarrierTrip Rbt;
        public BarcodeType ScannedBarcodeType;
        public LocationElement CurrentLocation;
        public ScanningProcessStartedForm ScanningProcessStartedForm;
    }
}

﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using System;
namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class DeliveryOperation
    {
        public string RouteId { get; set; }
        public PlannedConsignmentsType Consignment { get; set; }
        public GetGoodsListResponseOperationLoadCarrier LoadCarrier { get; set; }
        public string OperationDetail { get; set; }
        public DeliveryStatus Status { get; set; }
        public DeliveryOperationType OperationType { get; set; }
        public int DefaultSortNo { get; set; }
        public int CurrentSortNo { get; set; }
        public int TempSortNo { get; set; }
        public int ZoneNo { get; set; }
        public string PhysicalLoadCarrierId { get; set; }
        public string LogicalLoadCarrierId { get; set; }
        public string TripId { get; set; }
        public string ExternalTripId { get; set; }
        

        //Added property when sorting done with operation list stop ..in US47
        public double StopSequence { get; set; }

        //Added property used in sorting of stops in amphora..
        public int AmphoraStopSequence { get; set; }

        public DeliveryOperation Clone()
        {
            return (DeliveryOperation)MemberwiseClone();
        }

        public DateTime ScanTime { get; set; }
    }   

    public enum DeliveryOperationType
    {
        Consignment = 0,
        LoadCarrier = 4
    }

    public enum DeliveryStatus
    {
        New = 1,
        Started = 2,
        Finished = 3
    }
}

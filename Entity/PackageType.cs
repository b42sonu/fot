﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class PackageType
    {
        public string LanguageCode { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }
    }
}

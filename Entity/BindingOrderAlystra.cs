﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class BindingOrderAlystra
    {
        public string StopId { get; set; }
        public string StopAddress { get; set; }
        public string StopStatus { get; set; }
        public string OperationId { get; set; }
        public string StopSign { get; set; }
        public bool IsLoadListOperation { get; set; }
        public double StopSequence { get; set; }

        //below properties mainly used for order tabs...
        public string OrderId { get; set; }
        public string OrderDetail { get; set; }
        public bool IsMultipleStopOrder { get; set; }

        public string FirstOrderStopSign { get; set; }
        public string FirstOrderStopId { get; set; }
        public string FirstOrderOperationId { get; set; }

        public string SecondOrderStopSign { get; set; }
        public string SecondOrderStopId { get; set; }
        public string SecondOrderOperationId { get; set; }


        public System.Drawing.Font StopAddressFontStyle
        {
            get
            {
                switch (StopAddressStyle)
                {
                    case TextStyle.Normal:
                        return new System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Regular);
                    case TextStyle.Bold:
                        return new System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Regular | System.Drawing.FontStyle.Bold);
                    case TextStyle.BoldAndItalic:
                        return new System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
                    case TextStyle.Italic:
                        return new System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Italic);
                    case TextStyle.StrikeThrough:
                        return new System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Regular | System.Drawing.FontStyle.Strikeout);
                    default:
                        return new System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Regular);

                }
            }
        }
        public TextStyle StopAddressStyle { get; set; }

    }


    //added new property for CR FOT-150
    public enum TextStyle
    {
        Normal = 0,
        Bold = 1,
        Italic = 2,
        BoldAndItalic = 3,
        StrikeThrough = 4

    };
}
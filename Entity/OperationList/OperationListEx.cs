﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using OperationListEntity = Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList.OperationList;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.OperationList
{
    public class OperationListEx
    {
        public OperationListEx(OperationListEntity operationList)
        {
            _operationList = operationList;
        }

        public OperationListEx()
        {
            _operationList = new OperationListEntity();
        }

        private readonly OperationListEntity _operationList;
        public OperationListStop[] Stop
        {
            get { return _operationList.Stop; }
            set { _operationList.Stop = value; }
        }

        public string VersionNumber { get { return _operationList.VersionNumber; } set { _operationList.VersionNumber = value; } }
        public string SessionId { get { return _operationList.SessionId; } }
        public string OperationListId { get { return _operationList.OperationListId; } }
        public string CaptureEquipmentId { get { return _operationList.CaptureEquipmentId; } }
        public DateTime LastChanged { get { return _operationList.LastChanged; } }
        public string CorrelationId { get { return _operationList.CorrelationId; } }
        public string[] DeletedOrders { get { return _operationList.DeletedOrders; } }

        public static explicit operator OperationListEntity(OperationListEx d)  // implicit digit to byte conversion operator
        {
            return d._operationList;
        }


        /// <summary>
        /// Retrieve stop based on stop-id
        /// </summary>
        /// <param name="stopId"></param>
        /// <returns>operations for one stop</returns>
        public OperationListStop GetStop(String stopId)
        {
            OperationListStop result = null;
            if (Stop != null)
            {
                result = (from n in Stop
                          where n.StopInformation != null && n.StopInformation.StopId == stopId
                          select n).SingleOrDefault<OperationListStop>();
            }

            return result;
        }

        /// <summary>
        /// Retrieve stop containing a given operation
        /// </summary>
        /// <param name="operationId"></param>
        /// <returns>operations for one stop</returns>
        public OperationListStop GetStopByOperationId(String operationId)
        {
            OperationListStop result = null;
            if (Stop != null)
            {
                result = (from n in Stop
                          where n.StopInformation != null && n.PlannedOperation[0] != null && n.PlannedOperation[0].OperationId == operationId
                          select n).SingleOrDefault<OperationListStop>();
            }

            return result;
        }

        /// <summary>
        /// Retrieve the selected planned operations for stop
        /// </summary>
        /// <param name="stopId"></param>
        /// <param name="operationId"></param>
        /// <returns></returns>
        public PlannedOperationType GetPlannedOperation(String stopId, string operationId)
        {
            var operationListForStop = GetStop(stopId);
            if (operationListForStop == null)
            {
                return null;
            }

            var operation = (from n in operationListForStop.PlannedOperation
                             where n.OperationId == operationId
                             select n).SingleOrDefault<PlannedOperationType>();
            return operation;
        }

        // Amphora has empty stop-ids, and only one operation per stop
        // For that reason we put operation-id into stop-id field, so we can 
        // have a unique id per stop
        public void AddMissingStopIdsForAmphora()
        {
            try
            {
                if (Stop != null)
                {
                    foreach (var stop in Stop)
                    {
                        if (string.IsNullOrEmpty(stop.StopInformation.StopId))
                        {
                            stop.StopInformation.StopId = stop.PlannedOperation[0].OperationId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OperationList.AddMissingStopIdsForAmphora");
            }
        }

        // Add a stop to operationlist if it does not exist already if we have received a partial update
        public void AddStopIfMissing(OperationListStop stop)
        {
            try
            {
                if (stop.PlannedOperation != null && stop.PlannedOperation[0] != null)
                {
                    var existingStop = GetStopByOperationId(stop.PlannedOperation[0].OperationId);
                    // If stop not exists, we add it to list of stops
                    if (existingStop == null)
                    {
                        var existingStops = Stop.ToList();
                        existingStops.Add(stop);
                        Stop = existingStops.ToArray();
                        
                        // Add status for existing order(which by definition is confirmed already)
                        SetOrderConfirmationStatus(stop.PlannedOperation[0].OrderID, ConfirmationStatus.Confirmed);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OperationList.AddStopIfMissing");
            }
        }

        // Retrieve all distinct ordernumbers in OL
        public List<string> GetOrderNumbers()
        {
            var result = new List<string>();

            //Iterate for all operations in current stop
            if (Stop != null)
            {
                foreach (var stop in Stop)
                {
                    var orderNumber = stop.PlannedOperation[0].OrderID;

                    if (string.IsNullOrEmpty(orderNumber) == false && result.Contains(orderNumber) == false)
                    {
                        result.Add(orderNumber);
                    }
                }
            }

            return result;
        }

        // Return list of stops that qualify to a given condition
        public List<OperationListStop> Stops(Func<OperationListStop, bool> func)
        {
            return (from stop in Stop where stop != null && func(stop) select stop).ToList();
        }

        // Return list of operations that qualify to a given condition
        public List<PlannedOperationType> Operations(Func<PlannedOperationType, bool> func)
        {
            return (from stop in Stop from operation in stop.PlannedOperation where operation != null && func(operation) select operation).ToList();
        }

        // Retrieve an operation with given operation id
        public PlannedOperationType GetOperation(string operationId)
        {
            return Stop.SelectMany(stop => stop.PlannedOperation.Where(operation => operation != null)).FirstOrDefault(operation => operation.OperationId == operationId);
        }

        /// <summary>
        /// Update status of an order as following:
        ///     New: Order has only new operations
        ///     Confirmed: Order was confirmed earlier (in earlier update) or this is the first OL we receive and no operations are New
        ///     Updated: Has operations that are not finished 
        /// </summary>
        public string UpdateOrderConfirmationStatus(string orderNumber)
        {
            string result;

            // All operations cancelled
            if (Operations(o => o.OrderID == orderNumber && o.Status != OperationStatus.Canceled).Any() == false)
                result = ConfirmationStatus.Canceled;
            // All operations new
            else if (Operations(o => o.OrderID == orderNumber && o.Status != OperationStatus.New).Any() == false)
                result = ConfirmationStatus.New;
            // All operations finished
            else if (ModelOperationList.OperationList == null && Operations(o => o.OrderID == orderNumber && o.Status == OperationStatus.New).Any() == false ||
                Operations(o => o.OrderID == orderNumber && o.Status != OperationStatus.Finished).Any() == false)
            {
                result = ConfirmationStatus.Confirmed;
            }
            else
                result = ConfirmationStatus.Updated;

            SetOrderConfirmationStatus(orderNumber, result);

            return result;
        }

        public void ClearOrderStatus()
        {
            OrderConfirmationStatus.Clear();
        }

        private Dictionary<string, string> _orderConfirmationStatus;
        
        private Dictionary<string, string> OrderConfirmationStatus
        {
            get { return _orderConfirmationStatus ?? (_orderConfirmationStatus = new Dictionary<string, string>()); }
        }

        public string GetOrderConfirmationStatus(string orderNumber)
        {
            string status;
            OrderConfirmationStatus.TryGetValue(orderNumber, out status);
            if (status != ConfirmationStatus.Confirmed && status != ConfirmationStatus.New && status != ConfirmationStatus.Updated &&
                status != ConfirmationStatus.Accepted && status != ConfirmationStatus.Rejected && status != ConfirmationStatus.Canceled)
                Logger.LogEvent(Severity.Error, "GetOrderConfirmationStatus used for wrong order status '{0}'", status);
            return status;
        }

        public void SetOrderConfirmationStatus(string orderNumber, string status)
        {
            OrderConfirmationStatus[orderNumber] = status;
        }


        public bool IsOrderActive(string orderNumber)
        {
            // If we have started operations, then the order is active
            if (Operations(o => o.OrderID == orderNumber && o.Status == OperationStatus.Started).Any())
                return true;

            return false;
        }

        public bool IsOrderNew(string orderNumber)
        {
            // If we have started operations, then the order is active
            if (Operations(o => o.OrderID == orderNumber && o.Status != OperationStatus.New).Any())
                return false;

            return true;
        }

        public string GetOrderOperationStatus(string orderNumber)
        {
            //if no stop without cancelled staus, then order is cancelled
            if (Operations(o => o.OrderID == orderNumber && o.Status != OperationStatus.Canceled).Any() == false)
                return OperationStatus.Canceled;

            //if no stop with Finish status then order treated as New status
            if (Operations(o => o.OrderID == orderNumber && o.Status == OperationStatus.Finished).Any() == false)
                return OperationStatus.New;

            //if stop of order is started and some is finished/cancelled then it is order is started...
            if (Operations(o => o.OrderID == orderNumber && (o.Status == OperationStatus.Started)).Any() && Operations(o => o.OrderID == orderNumber &&
                (o.Status == OperationStatus.Finished || o.Status == OperationStatus.Canceled)).Any())
                return OperationStatus.Started;


            return OperationStatus.Finished;
        }

        public bool IsUpdated { get; set; }
        public int AcceptedOrderCount { get; set; }
        public int RejectedOrderCount { get; set; }

        public OperationListEx CreateCopy()
        {
            var innerObject = BaseActionCommands.GetCopyOfObject<OperationListEntity>(_operationList);
            return new OperationListEx(innerObject);

        }

        public bool HasNewOrders
        {
            get
            {
                var allOrders = GetOrderNumbers();
                return allOrders.Any(IsOrderNew);
            }
        }

        public bool IsStopSignatureRequired(string stopId)
        {
            if (string.IsNullOrEmpty(stopId) == false)
            {
                OperationListStop operationListStop = GetStop(stopId);
                if (operationListStop != null && operationListStop.AlystraBexOrderLevel != null && operationListStop.AlystraBexOrderLevel.VasItem != null && operationListStop.AlystraBexOrderLevel.VasItem.Any())
                    return operationListStop.AlystraBexOrderLevel.VasItem[0].SignatureRequired;
            }
            return true;
        }

        

        public void DeleteCanceledStops()
        {
            try
            {
                var cancelledStops = Stops(s => s.PlannedOperation[0].Status == OperationStatus.Canceled).Select(s => s.PlannedOperation[0].OperationId).ToArray();
                if (cancelledStops.Length >= 1)
                {
                    var cancelledStopString = string.Join(",", cancelledStops);
                    Stop = Stops(s => s.PlannedOperation[0].Status != OperationStatus.Canceled).ToArray();
                    Logger.LogEvent(Severity.Info, "Deleted stops '{0}'", cancelledStopString);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OperalistEx.DeleteOrder");
            }
        }
    }
}

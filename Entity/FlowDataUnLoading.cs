﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    /// <summary>
    /// This class acts as initial data for flow Unloading.
    /// </summary>
    public class FlowDataUnloading : BaseFlowData
    {
        public string PowerUnitId;
        public bool IsStartedFromOperationList;
        public bool IsUnloadingStartedFromLoading;
    }
}


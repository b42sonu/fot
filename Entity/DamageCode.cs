﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class DamageCode : IStatus
    {
        public string Code { get; set; }
        public string LangCode { get; set; }
        public string Description { get; set; }

        public string CodeAndDescription
        {
            get { return Code + " " + Description; }
        }
        public override string ToString()
        {
            return Code + " " + Description;
        }

        public DamageCode Clone()
        {
            DamageCode result = (DamageCode)MemberwiseClone();
            return result;
        }
    }
}

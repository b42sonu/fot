﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class ActionType : IStatus
    {
        public string Code { get; set; }
        public string LangCode { get; set; }
        public string Description { get; set; }
        
        public override string ToString()
        {
            return Description;
        }

        public ActionType Clone()
        {
            var result = (ActionType)MemberwiseClone();
            return result;
        }
    }
}

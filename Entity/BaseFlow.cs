﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public abstract class BaseFlow
    {
        protected BaseFlow()
        {
            InstanceId = Guid.NewGuid();
            if(IsWorkProcess)
                VasHistory = new VasHistory();
        }

        public void InitFlow(IFlowData flowData)
        {
            BeginFlow(flowData);
        }

        public abstract void BeginFlow(IFlowData flowData);

        public abstract void ExecuteState(int state);

        /// <summary>
        /// If this flow is a work process, we return it's name here
        /// A work process is a process that needs Vas data
        /// </summary>
        public virtual WorkProcess WorkProcess
        {
            get { return WorkProcess.None; }
        }

        /// <summary>
        /// Tells if this flow is a workprocess or not
        /// </summary>
        public bool IsWorkProcess { get { return WorkProcess != WorkProcess.None; } }

        /// <summary>
        /// Keeps track of what VAS support-ids and consignments have been used
        /// </summary>
        public readonly VasHistory VasHistory;

        /// <summary>
        /// Identifier used to separate separate instances of same class
        /// </summary>
        public Guid InstanceId { get; private set; }

        // Here you can cleanup before flow is disposed
        public virtual void CleanupFlow() { }

        /// <summary>
        /// Called vy framework when a subflow is ended
        /// </summary>
        public void EndSubFlow()
        {
            CleanupFlow();

            // If a workprocess has finished, we clean up the support data
            ClearVasHistory();
        }

        // Every time a flow opens a form, a pointer to it is stored here
        public BaseForm CurrentView { get; set; }

        public IFlowResult SubflowResult { get; set; }

        public void ReinstateView()
        {
            ViewCommands.ShowView(false, CurrentView, null);
        }

        private Process _currentProcess;
        /// Placeholder containing the current running process (set by starting flow or sub-flow)
        public Process CurrentProcess
        {
            get
            {
                Logger.LogAssert(_currentProcess != Process.Unknown);
                return _currentProcess;
            }
            internal set { _currentProcess = value; }
        }

        

        /// <summary>
        /// Used for starting a sub-flow from a flow
        /// </summary>
        protected BaseFlow StartSubFlow<T>(IFlowData flowData, int returnState, Process process) where T : BaseFlow, new()
        {
            if (process == Process.Inherit)
                process = CurrentProcess;
            return BaseModule.StartSubFlow<T>(flowData, returnState, process);
        }

        /// <summary>
        /// Returns wether to react to stop codes. Override if your flow is supposed to do that
        /// </summary>
        public virtual bool IsStopCodeActiveForProcess()
        {
            return false;
        }

        internal void ClearVasHistory()
        {
            if (VasHistory != null)
                VasHistory.Clear();
        }

        protected void LogUnhandledTransitionState(string state)
        {
            Logger.LogEvent(Severity.Error, "Unhandled transistion state '{0}' encountered in flow {1}", state, GetType().Name);
        }
    }

    /// <summary>
    /// Base class for arguments passed to flow 
    /// </summary>
    public interface IFlowData
    {
        string HeaderText { get; }
    }

    /// <summary>
    /// Base implementation of IFlowData. All other implementations should inherit from this
    /// </summary>
    public abstract class BaseFlowData : IFlowData
    {
        /// Placeholder for string you display in header. Used for shared flows
        public string HeaderText { get; set; }
    }
}

using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    [Flags]
    public enum EntityStatus
    {
        Unknown = 0,
        False = 0,
        Scanned = 1,
        Planned = 2,
        ScannedPlanned = Scanned | Planned,
        GoodsEventSent = 4,
        ValidatedUnplannedConsignment = 8, // Set if user has validated that this unplanned consignment is ok to enter consignment items for
        Any = 0xffff
    }

    public class EntityMap : Dictionary<string, Consignment>
    {

        public enum DeviationStatus
        {
            AcceptedStatus = 0,
            AdditionalStatus = 1,
            MissingStatus = 2,
            Empty = 3,
        };

        /// <summary>
        /// Returns total number of  scanned items. Manual entered count is used if it is higher than actual count
        /// </summary>
        public int ScannedConsignmentItemsTotalCount
        {
            get
            {
                int totalConsignment = 0;

                foreach (var consignment in Values)
                {
                    if ((consignment is LoadCarrier) == false)
                        totalConsignment += Math.Max(consignment.ScannedItemCount(), consignment.ManuallyEnteredConsignmentItemCount);
                }

                return totalConsignment;
            }
        }


        /// <summary>
        /// Returns total number of actually scanned items. Manual entered count is ignored
        /// </summary>
        public int ScannedConsignmentItemsActualTotalCount
        {
            get
            {
                int totalConsignment = 0;

                foreach (Consignment consignment in Values)
                {
                    if ((consignment is LoadCarrier) == false)
                        totalConsignment += consignment.ScannedItemCount();
                }

                return totalConsignment;
            }
        }

        public int ScannedConsignmentAndItemsTotalCount
        {
            get { return ScannedConsignmentItemsActualTotalCount + GetScannedConsignmentCount(); }
        }

        public int ScannedLoadCarrierTotalCount
        {
            get
            {
                return Values.OfType<LoadCarrier>().Count(loadcarrier => loadcarrier.HasStatus(EntityStatus.Scanned));
            }
        }

        public int GetItemCountFromConsignment(string consignmentId)
        {
            int result = 0;
            var consignment = GetConsignment(consignmentId);
            if (consignment != null)
            {
                result = Math.Max(consignment.ScannedItemCount(), consignment.ManuallyEnteredConsignmentItemCount);
            }
            return result;
        }

        /// <summary>
        /// Returns true if either at least one consignment or consignment item has been scanned
        /// </summary>
        /// <returns></returns>
        public bool HasScannedEntities
        {
            get { return HasScannedConsignments() || HasScannedConsignmentItems(); }
        }

        public bool HasScannedConsignments()
        {
            Logger.LogEvent(Severity.Debug, "Executing ReconciledEntityMap.HasScannedConsignment()");

            return Values.Any(consignment => consignment.HasStatus(EntityStatus.Scanned));
        }

        public bool HasScannedConsignmentItems()
        {
            return Values.Any(consignment => consignment.ItemCount(EntityStatus.Scanned, false) > 0);
        }

        /// <summary>
        /// This method will return list of all consignment and consignment items in those consignments
        /// </summary>
        /// <returns>Returns list of consignment entities.</returns>
        public List<ConsignmentEntity> GetConsignmentsWithoutEntities()
        {
            var entityList = new List<ConsignmentEntity>();

            foreach (var consignment in Values)
            {
                entityList.Add(consignment);
                if (!(consignment is LoadCarrier) && consignment.ConsignmentItemsMap != null)
                {
                    entityList.AddRange(consignment.ConsignmentItemsMap.Values.Cast<ConsignmentEntity>());
                }
            }

            return entityList;

        }


        public ConsignmentEntity Store(ConsignmentEntity consignmentEntity)
        {
            try
            {
                //Below is required, because while sending the print command to LM, we need time for first scanned item/load carrier
                //In any confusion please discuss (Rakesh Aggarwal)
                if (Count == 0)
                    ModelMain.SelectedOperationProcess.TimeForFirstScan = DateTime.Now;

                if (consignmentEntity != null)
                {
                    var loadCarrier = consignmentEntity as LoadCarrier;
                    if (loadCarrier != null)
                    {
                        //get existing load carrier 
                        var existingLoadCarrier = GetLoadCarrier(loadCarrier.ConsignmentId);
                        //if existing load carrier is available then mark it as scanned
                        //otherwise store it in entty map.
                        if (existingLoadCarrier != null)
                            existingLoadCarrier.EntityStatus |= EntityStatus.Scanned;
                        else
                        {
                            loadCarrier.EntityCreatedOn = DateTime.Now;
                            this[loadCarrier.ConsignmentId] = loadCarrier;    
                        }
                    }
                    else
                    {
                        if (consignmentEntity is ConsignmentItem)
                            consignmentEntity = StoreConsignmentItem(consignmentEntity.CastToConsignmentItem());
                        else
                            consignmentEntity = StoreConsignment(consignmentEntity.CastToConsignment());
                    }
                }
                else
                {
                    Logger.LogEvent(Severity.Error, "Tried to save null value in ReconciledEntityMap");
                }


                return consignmentEntity;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "EntityMap.StoreScanning");
                throw;
            }
        }


        private ConsignmentEntity StoreConsignmentItem(ConsignmentItem consignmentItem)
        {
            // In cases where the operation list contains consignment items without id, we will not add it to the map
            if (String.IsNullOrEmpty(consignmentItem.ItemId))
                return consignmentItem;

            consignmentItem.PlannedConsignmentItem = GetConsignmentItemFromPlannedConsignment(consignmentItem.PlannedConsignment, consignmentItem.ItemId);

            Consignment consignment = GetConsignment(consignmentItem);
            // If new consignment we create the new entry
            if (consignment == null)
            {
                if (string.IsNullOrEmpty(consignmentItem.ConsignmentId))
                {
                    Logger.LogEvent(Severity.Error, GlobalTexts.MissingConsignmentId, consignmentItem.ItemId);
                }
                else
                {
                    // Retrieve consignment not added to map, from consignment item
                    consignment = consignmentItem.ConsignmentInstance ?? new Consignment(consignmentItem.ConsignmentId)
                    {
                        ConsignmentItemsMap = new ConsignmentItemMap(consignmentItem),
                        EntityStatus = EntityStatus.Unknown,
                        ConsignmentItemCountFromLm = consignmentItem.ConsignmentItemCountFromLm,
                        EntityCreatedOn = DateTime.Now
                    };

                    //in case of offline, as consignment item count in consignment will be 0 means till now
                    //consignment item is not stored in consignment.
                    if (consignment.ConsignmentItemsMap != null && consignment.ConsignmentItemsMap.Count == 0)
                    {
                        consignment.ConsignmentItemsMap = new ConsignmentItemMap(consignmentItem.Clone());
                    }

                    if (consignment.PlannedConsignment == null)
                        consignment.PlannedConsignment = consignmentItem.PlannedConsignment;

                    Add(consignment.ConsignmentId, consignment);
                }
            }
            else
            {
                consignmentItem.ConsignmentInstance = consignment;

                // If item already exists, we or the status together, so planned and scanned forms a new status
                ConsignmentItem existingItem;
                if (consignment.ConsignmentItemsMap.TryGetValue(consignmentItem.ItemId, out existingItem))
                {
                    consignmentItem.EntityStatus |= existingItem.EntityStatus;
                }
                consignment.ConsignmentItemsMap[consignmentItem.ItemId] = consignmentItem;
            }

            return consignmentItem;
        }



        /// <summary>
        /// Stores data about previous scannings when only consignment id is known
        /// </summary>  
        private ConsignmentEntity StoreConsignment(Consignment consignment)
        {
            Consignment existingConsignment;
            consignment.EntityCreatedOn = DateTime.Now;
            if (TryGetValue(consignment.ConsignmentId, out existingConsignment) == false)
            {
                Add(consignment.ConsignmentId, consignment);
            }
            else
            {
                // Keep scanned and planned items, if not given by new consignment
                if (consignment.ConsignmentItemsMap != null && consignment.ConsignmentItemsMap.Count == 0 && existingConsignment.ConsignmentItemsMap != null)
                    consignment.ConsignmentItemsMap = existingConsignment.ConsignmentItemsMap;

                this[consignment.ConsignmentId] = consignment;
            }

            return consignment;
        }



        private ConsignmentItemType GetConsignmentItemFromPlannedConsignment(PlannedConsignmentsType plannedConsignment, string consignmentItemId)
        {
            if (plannedConsignment != null && plannedConsignment.ConsignmentItem != null)
            {
                return plannedConsignment.ConsignmentItem.FirstOrDefault(consignmentItem => consignmentItem.ConsignmentItemNumber.Equals(consignmentItemId));
            }

            return null;
        }



        /// <summary>
        /// Checks if a consignmententity has been already stored in ReconciledEntityMap, and retrieve it if possible
        /// </summary>
        /// <returns>True if the found consignment entity has been scanned</returns>
        public bool GetScannedBefore(ref ConsignmentEntity consignmentEntity)
        {
            bool result = false;
            if (consignmentEntity != null)
            {
                ConsignmentEntity foundConsignmentEntity;

                // Check if consignment item is known
                var consignmentItem = consignmentEntity as ConsignmentItem;
                if (consignmentItem != null)
                {
                    if (consignmentItem is DeliveryCode)
                    {
                        foundConsignmentEntity = IsDeliveryCodeInAnyConsignments(consignmentItem.DeliveryCode);
                    }
                    else
                    {
                        foundConsignmentEntity = IsConsignmentItemsIdInAnyConsignments(consignmentItem.ItemId, EntityStatus.ScannedPlanned);
                    }
                    if (foundConsignmentEntity != null)
                        result = foundConsignmentEntity.HasStatus(EntityStatus.Scanned);
                }
                else
                {
                    foundConsignmentEntity = GetConsignment(consignmentEntity.EntityId);
                    if (foundConsignmentEntity != null)
                    {
                        // Has consignment been directly scanned before
                        if (foundConsignmentEntity.HasStatus(EntityStatus.Scanned))
                            result = true;
                    }
                }
                if (result)
                    consignmentEntity = foundConsignmentEntity;
            }
            return result;
        }

        /// <summary>
        /// Checks if a consignmententity has been already stored in ReconciledEntityMap
        /// </summary>
        /// <returns>True if the found consignment entity has been scanned</returns>
        public bool IsScannedBefore(ConsignmentEntity consignmentEntity)
        {
            if (consignmentEntity != null)
            {
                return consignmentEntity is LoadCarrier
                           ? IsLoadCarrierScannedBefore(consignmentEntity)
                           : GetScannedBefore(ref consignmentEntity);
            }
            return false;
        }

        private bool IsLoadCarrierScannedBefore(ConsignmentEntity consignmentEntity)
        {
            var consignmentId = consignmentEntity.ConsignmentId;
            var consignment = GetConsignment(consignmentId);
            return consignment != null;
        }

        private ConsignmentItem IsConsignmentItemsIdInAnyConsignments(string consignmentItemId, EntityStatus status)
        {
            ConsignmentItem result = null;
            foreach (var consignment in Values)
            {
                if (!(consignment is LoadCarrier))
                {
                    if (consignment.ConsignmentItemsMap.ContainsKey(consignmentItemId))
                    {
                        var consignmentItem = consignment.ConsignmentItemsMap[consignmentItemId];
                        if (consignmentItem.HasStatus(status))
                        {
                            result = consignmentItem;
                        }
                    }
                }
            }
            return result;
        }

        private ConsignmentItem IsDeliveryCodeInAnyConsignments(string deliveryCode)
        {
            ConsignmentItem result = null;
            foreach (var consignment in Values)
            {
                if (!(consignment is LoadCarrier))
                {
                    if (consignment.ConsignmentItemsMap.Any(p => p.Value.DeliveryCode == deliveryCode))
                    {
                        var consignmentItemMap = consignment.ConsignmentItemsMap.FirstOrDefault(p => p.Value.DeliveryCode == deliveryCode);
                        if (consignmentItemMap.Key.Length > 0)
                        {
                            var consignmentItem =
                                consignment.ConsignmentItemsMap.FirstOrDefault(p => p.Value.DeliveryCode == deliveryCode)
                                    .Value;
                            if (consignmentItem != null)
                            {
                                result = consignmentItem;
                            }
                        }

                    }
                }
            }
            return result;
        }

        //Locate corresponding consignment in map
        public Consignment GetConsignment(string consignmentId)
        {
            Consignment result;
            TryGetValue(consignmentId, out result);

            return result;
        }

        public LoadCarrier GetLoadCarrier(string loadCarrierId)
        {
            var consignment = GetConsignment(loadCarrierId);

            return consignment == null ? null : consignment as LoadCarrier;
        }

        /// <summary>
        /// Find the consignment that containes consignment item
        /// </summary>
        public Consignment GetConsignment(ConsignmentItem consignmentItem)
        {
            if (string.IsNullOrEmpty(consignmentItem.ConsignmentId) == false)
            {
                Consignment result;
                if (TryGetValue(consignmentItem.ConsignmentId, out result))
                    return result;

                // If we could not find it, it is possible the item was stored before with undefined consignment
                foreach (var consignment in Values)
                {
                    if ((consignment is LoadCarrier) == false)
                    {
                        foreach (var consignmentItemMap in consignment.ConsignmentItemsMap)
                        {
                            if (consignmentItemMap.Key == consignmentItem.ItemId)
                            {
                                //Remove this item from consignment unknown as we know it's actual consignment has been found (online or through pdf/scanning)
                                Logger.LogEvent(Severity.Warning, "Removed consignment item {0} from consignment {1} as it was found to reside in consignment {2}",
                                                consignmentItem.ItemId, consignmentItem.ConsignmentId, consignment.ConsignmentId);
                                RemoveConsignmentItem(consignmentItemMap.Value);
                                break;
                            }
                        }
                        if (Values.Count == 0)
                            break;
                    }
                }
            }

            return null;
        }


        /// <summary>
        /// Retrrieve all consignment items for a consignment with specific id
        /// </summary>
        public ConsignmentItemMap GetConsignmentItems(string consignmentId)
        {
            Consignment consignment = GetConsignment(consignmentId);

            ConsignmentItemMap result = consignment != null ? consignment.ConsignmentItemsMap : new ConsignmentItemMap();

            return result;
        }

        public List<ConsignmentItem> GetScannedConsignmentItems()
        {
            List<ConsignmentItem> consignmentItemList = null;
            foreach (var consignment in Values)
            {
                if (consignment.ConsignmentItemsMap != null && consignment.ConsignmentItemsMap.Count > 0)
                {
                    if (consignmentItemList == null)
                        consignmentItemList = new List<ConsignmentItem>();
                    consignmentItemList.AddRange(consignment.ConsignmentItemsMap.Values);
                }
            }
            return consignmentItemList;
        }

        public List<LoadCarrier> GetScannedLoadCarriers()
        {
            var loadCarrierList = new List<LoadCarrier>();
            loadCarrierList.AddRange(Values.OfType<LoadCarrier>());
            return loadCarrierList;
        }



        /// <summary>
        /// Adds a all planned consignments to ReconciledEntityMap. Only consignment id's and consignment item'is is stored
        /// </summary>
        public bool StorePlannedConsignments(PlannedConsignmentsType[] plannedConsignments)
        {
            bool result = false;
            if (plannedConsignments != null)
            {
                foreach (var plannedConsignment in plannedConsignments)
                {
                    if (plannedConsignment.ConsignmentItem != null) //TODO: Add planned consignment item
                    {
                        foreach (var plannedConsignmentItem in plannedConsignment.ConsignmentItem)
                        {
                            var consignmentItem = new ConsignmentItem
                            {
                                ItemId = ConvertToUpperCase(plannedConsignmentItem.ConsignmentItemNumber),
                                ConsignmentInstance = new Consignment(ConvertToUpperCase(plannedConsignment.ConsignmentNumber)),
                                EntityStatus = EntityStatus.Planned,
                                PlannedConsignment = plannedConsignment
                            };

                            Store(consignmentItem);
                        }
                        result = true;
                    }
                    else
                    {
                        if (plannedConsignment.ConsignmentNumber != null)
                        {
                            var consignment = new Consignment(plannedConsignment.ConsignmentNumber, EntityStatus.Planned, plannedConsignment);
                            Store(consignment);
                            result = true;
                        }
                    }
                }
            }
            return result;
        }

        private string ConvertToUpperCase(string item)
        {
            return item != null ? item.ToUpper() : null;
        }


        /// <summary>
        /// Removes the scan status on the consignment entity. If the consignment is unknown/unplanned, it will be removed entirely
        /// </summary>
        public void RemoveEntity(ConsignmentEntity consignmentEntity)
        {
            bool result;
            if (consignmentEntity is Consignment)
            {
                if (consignmentEntity is LoadCarrier)
                {
                    Remove(consignmentEntity.EntityId);
                    result = true;
                }
                else
                {
                    // Remove pure consignment
                    result = RemoveConsignment(consignmentEntity.CastToConsignment());
                }
            }
            else
            {
                result = RemoveConsignmentItem(consignmentEntity.CastToConsignmentItem());
            }

            if (result == false)
            {
                Logger.LogEvent(Severity.Error, "Tried to remove {0} that does not exist in cache: {1}", consignmentEntity.GetType().Name, consignmentEntity.EntityId);
            }
        }


        private bool RemoveConsignmentItem(ConsignmentItem consignmentItem)
        {
            bool deleted = false;
            Consignment consignment = GetConsignment(consignmentItem);
            if (consignment == null)
            {
                Logger.LogEvent(Severity.Error, "Failed to locate consignmentitem with id '{0}'", consignmentItem.ItemId);
            }
            else
            {
                var consignmentItemsMap = consignment.ConsignmentItemsMap;
                if (consignmentItemsMap != null)
                {
                    if (consignmentItemsMap.TryGetValue(consignmentItem.ItemId, out consignmentItem))
                    {
                        consignmentItem.RemoveStatus(EntityStatus.Scanned);

                        //If it is not planned, remove it completly
                        bool notPlanned = consignmentItem.HasStatus(EntityStatus.Planned) == false;
                        if (notPlanned)
                        {
                            consignmentItemsMap.Remove(consignmentItem.ItemId);
                        }

                        if (consignment.ConsignmentItemsMap.Values.Any(n => n.HasStatus(EntityStatus.Scanned)) == false)
                            consignment.ManuallyEnteredConsignmentItemCount = 0;


                        //If no consignment item is scanned or planned, remove consignment if that is also not scanned or planned
                        bool hasScannedOrPlannedItems = consignment.ConsignmentItemsMap.Values.Any(c => c.HasStatus(EntityStatus.ScannedPlanned));
                        if (hasScannedOrPlannedItems == false)
                        {
                            if (consignment.HasStatus(EntityStatus.ScannedPlanned) == false)
                                Remove(consignment.ConsignmentId);
                        }
                        deleted = true;
                    }
                }
            }

            return deleted;
        }

        private bool RemoveConsignment(Consignment consignment)
        {
            bool result = false;
            consignment = GetConsignment(consignment.ConsignmentId);
            if (consignment != null)
            {
                // Remove the whole consignment if it was unplanned and has no planned/scanned consignment items
                bool hasConsignmentItems = consignment.ConsignmentItemsMap.Values.Any(c => c.HasStatus(EntityStatus.ScannedPlanned));
                if (hasConsignmentItems == false && (consignment.EntityStatus & EntityStatus.Planned) == EntityStatus.False)
                {
                    Remove(consignment.ConsignmentId);
                }
                else
                {
                    // Remove the scanned status only from consignment..
                    consignment.RemoveStatus(EntityStatus.Scanned);
                    consignment.ManuallyEnteredConsignmentItemCount = 0;
                    consignment.ManuallyEnteredConsignmentItemCountPlanned = 0;
                    consignment.RemoveStatus(EntityStatus.ValidatedUnplannedConsignment | EntityStatus.GoodsEventSent);
                }
                result = true;
            }
            return result;
        }

        public EntityMap Clone()
        {
            var result = new EntityMap();

            foreach (KeyValuePair<string, Consignment> pair in this)
            {
                result.Add(pair.Key, pair.Value.Clone());
            }

            return result;
        }

        public EntityMap DeepClone()
        {
            var result = new EntityMap();

            foreach (KeyValuePair<string, Consignment> pair in this)
            {
                var consignment = pair.Value.Clone();
                if (pair.Value.ConsignmentItemsMap != null)
                    consignment.ConsignmentItemsMap = pair.Value.ConsignmentItemsMap.Clone();
                result.Add(pair.Key, consignment);
            }

            return result;
        }

        public ConsignmentItem GetConsignmentItem(string consignmentId, string consignmentItemId)
        {
            var consignment = GetConsignment(consignmentId);
            if (consignment != null)
                return consignment.GetConsignmentItem(consignmentItemId);
            return null;
        }

        public int GetScannedConsignmentCount()
        {
            return Values.Count(consignment => consignment.HasStatus(EntityStatus.Scanned) && !(consignment is LoadCarrier));
        }
    }
}
﻿using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    /// <summary>
    /// This class will act as, object for Load Carrier which can be scanned like consignment.
    /// This class was supposed to inherit from consignment Entity, like we done with consignment. But as
    /// entity map we can store objects of type consignment only so we inherited this class form consignment.
    /// </summary>
    public class LoadCarrier : Consignment
    {


        public LoadCarrier(string loadCarrierId) : base(loadCarrierId) { }

        public override sealed Consignment ConsignmentInstance
        {
            get { return base.ConsignmentInstance; }
            set { base.ConsignmentInstance = value; }
        }

        public LogicalLoadCarrierType[] LogicalLoadCarrierDetails { get; set; }
        public PhysicalLoadCarrierType PhysicalLoadCarrier { get; set; }

        public new LoadCarrier Clone()
        {
            return (LoadCarrier)MemberwiseClone();
        }

        public bool IsTerminated
        {
            get { return PhysicalLoadCarrier != null && LogicalLoadCarrierDetails == null; }
        }

        public bool IsUnknown
        {
            get { return PhysicalLoadCarrier == null && LogicalLoadCarrierDetails == null; }
        }

        public void GetCarrierActivationMessage(MessageHolder messageHolder, bool isOfflineWorkListItem)
        {
            if (isOfflineWorkListItem)
            {
                messageHolder.Update(string.Format(GlobalTexts.OfflineCarrierActivated, EntityDisplayId), MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
            }
            else if (IsKnownInLm)
            {
                messageHolder.Update(string.Format(GlobalTexts.CarrierActivated, EntityDisplayId), MessageState.Information);
                SoundUtil.Instance.PlaySuccessSound();
            }
            else
            {
                switch (BaseModule.CurrentFlow.CurrentProcess)
                {
                    case Process.LoadLineHaul:
                    case Process.BuildLoadCarrier:
                        messageHolder.Update(IsTerminated ? 
                            string.Format(GlobalTexts.CarrierTerminatedCanNotBeUsedForLoading,EntityDisplayId) :
                            string.Format(GlobalTexts.CarrierUnknownCanNotBeUsedForLoading,EntityDisplayId), 
                            MessageState.Error);
                        break;

                    case Process.UnloadLineHaul:
                    case Process.UnloadPickUpTruck:
                        messageHolder.Update(IsTerminated ? 
                            string.Format(GlobalTexts.CarrierTerminatedButActivatedForUnloading, EntityDisplayId) : 
                            string.Format(GlobalTexts.CarrierUnknownButActivatedForUnloading, EntityDisplayId),
                            MessageState.Warning);
                        SoundUtil.Instance.PlayWarningSound();
                        break;
                    default:
                        messageHolder.Update(IsTerminated ? 
                            string.Format(GlobalTexts.CarrierTerminatedButActivated, EntityDisplayId) : 
                            string.Format(GlobalTexts.CarrierUnknownButActivated, EntityDisplayId),
                            MessageState.Warning);
                        SoundUtil.Instance.PlayWarningSound();
                        break;
                }
            }

        }

        public bool GetCarrierRegistrationMessage(MessageHolder messageHolder, string activeCarrier)
        {
            return GetCarrierRegistrationMessage(messageHolder, activeCarrier, WorkListItemType.LoadCarrier);
        }

        public bool GetCarrierRegistrationMessage(MessageHolder messageHolder, string activeCarrier, WorkListItemType typeOfWorkListItem)
        {
            switch (BaseModule.CurrentFlow.CurrentProcess)
            {
                case Process.LoadLineHaul:
                case Process.LoadDistribTruck:
                case Process.BuildLoadCarrier:
                    return GetMessageForLoadingOfCarrier(messageHolder, activeCarrier, typeOfWorkListItem);
                case Process.UnloadLineHaul:
                case Process.UnloadPickUpTruck:
                    return GetMessageForUnLoadingOfCarrier(messageHolder, activeCarrier);
                default:
                    return GetMessageForRegistrationOfCarrier(messageHolder, activeCarrier);
            }
        }

        public bool GetMessageForLoadingOfCarrier(MessageHolder messageHolder, string activeCarrier, WorkListItemType typeOfWorkListItem)
        {
            if (IsScannedOnline == false)
            {
                messageHolder.Update(typeOfWorkListItem == WorkListItemType.Route ? 
                    string.Format(GlobalTexts.CarrierLoadedOffLineIntoRoute, EntityDisplayId, activeCarrier) : 
                    string.Format(GlobalTexts.CarrierLoadedOffLine, EntityDisplayId, activeCarrier),
                    MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
                return true;
            }

            if (IsKnownInLm)
            {
                messageHolder.Update(typeOfWorkListItem == WorkListItemType.Route ? 
                    string.Format(GlobalTexts.CarrierLoadedIntoRoute, EntityDisplayId, activeCarrier) : 
                    string.Format(GlobalTexts.CarrierLoaded, EntityDisplayId, activeCarrier),
                    MessageState.Information);
                SoundUtil.Instance.PlaySuccessSound();
                return true;
            }


            switch (BaseModule.CurrentFlow.CurrentProcess)
            {
                case Process.LoadLineHaul:
                case Process.BuildLoadCarrier:
                case Process.LoadDistribTruck:
                    messageHolder.Update(IsTerminated ? 
                        string.Format(GlobalTexts.CarrierTerminatedCanNotLoad, EntityDisplayId, activeCarrier) : 
                        string.Format(GlobalTexts.CarrierUnknownCanNotLoad, EntityDisplayId, activeCarrier),
                        MessageState.Error);
                    return false;

                default:
                    if (IsTerminated)
                    {
                        messageHolder.Update(typeOfWorkListItem == WorkListItemType.Route
                                                 ? string.Format(GlobalTexts.CarrierTerminatedAndLoadedIntoRoute,
                                                                 EntityDisplayId, activeCarrier)
                                                 : string.Format(GlobalTexts.CarrierTerminatedAndLoaded, EntityDisplayId,
                                                                 activeCarrier),
                                             MessageState.Warning);
                    }
                    else
                    {
                        messageHolder.Update(typeOfWorkListItem == WorkListItemType.Route ? 
                            string.Format(GlobalTexts.CarrierUnknownAndLoadedIntoRoute, EntityDisplayId, activeCarrier) : 
                            string.Format(GlobalTexts.CarrierUnknownAndLoaded, EntityDisplayId, activeCarrier),
                            MessageState.Warning);
                    }
                    SoundUtil.Instance.PlayWarningSound();
                    return true;
            }
        }

        public bool GetMessageForUnLoadingOfCarrier(MessageHolder messageHolder, string activeCarrier)
        {
            if (IsScannedOnline == false)
            {
                messageHolder.Update(string.Format(GlobalTexts.CarrierUnLoadedOffLine, EntityDisplayId, activeCarrier), MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
                return true;
            }

            if (IsKnownInLm)
            {
                messageHolder.Update(string.Format(GlobalTexts.CarrierUnLoaded, EntityDisplayId, activeCarrier), MessageState.Information);
                SoundUtil.Instance.PlaySuccessSound();
                return true;
            }

            //Case of invalid carrier
            messageHolder.Update(IsTerminated ? 
                string.Format(GlobalTexts.CarrierTerminatedAndUnLoaded, EntityDisplayId, activeCarrier) : 
                string.Format(GlobalTexts.CarrierUnKnownAndUnLoaded, EntityDisplayId, activeCarrier),
                MessageState.Warning);
            SoundUtil.Instance.PlayWarningSound();
            return true;
        }


        public bool GetMessageForRegistrationOfCarrier(MessageHolder messageHolder, string activeCarrier)
        {

            if (IsScannedOnline == false)
            {
                messageHolder.Update(string.Format(GlobalTexts.CarrierRegisteredOffline, EntityDisplayId), MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
            }
            else if (IsKnownInLm)
            {
                messageHolder.Update(string.Format(GlobalTexts.CarrierRegistered, EntityDisplayId), MessageState.Information);
                SoundUtil.Instance.PlaySuccessSound();
            }
            else
            {
                messageHolder.Update(IsTerminated ? 
                    string.Format(GlobalTexts.CarrierTerminatedButRegistered, EntityDisplayId) : 
                    string.Format(GlobalTexts.CarrierUnknownButRegistered,EntityDisplayId),
                    MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
            }
            return true;
        }
    }

    public class DeliveryCode : ConsignmentItem
    {
        
        internal void UpdateConsignmentId(T20200_ValidateGoodsFromFOTReply validateGoodsResponse)
        {
            ConsignmentInstance.UpdateConsignmentId(validateGoodsResponse);
        }
    }
}

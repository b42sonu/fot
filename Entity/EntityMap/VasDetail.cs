﻿
namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
   public class VasDetail
    {
        public string VasId { get; set; }
        public string VasType { get; set; }

        //two below added for description value for cause and measure...
        public string VasMeasure { get; set; }
        public bool SignatureRequired { get; set; } 
   }
}


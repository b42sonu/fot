﻿
namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    /// <summary>
    /// This partial class will be used to add any attribute that is related to consignment but
    /// not specific to consignment.
    /// </summary>
    public partial class Consignment
    {
        public bool ConfirmedForNotToUnload { get; set; }
    }
}

using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    /// <summary>
    /// Keeps track of all consignmentitem in a consignment
    /// </summary>
    public class ConsignmentItemMap : Dictionary<string, ConsignmentItem>
    {
        // Creates the map 
        internal ConsignmentItemMap()
        {
        }

        // Creates the map and adds initial entry
        internal ConsignmentItemMap(ConsignmentItem consignmentItem)
        {
            if ((consignmentItem.ItemId != null))
            {
                Add(consignmentItem.ItemId, consignmentItem);
            }
            else
            {
                Logger.LogEvent(Severity.Error, "Null value was given to ConsignmentItemMap()");
            }
        }

        public ConsignmentItemMap Clone()
        {
            var result = new ConsignmentItemMap();

            foreach (KeyValuePair<string, ConsignmentItem> pair in this)
            {
                result.Add(pair.Key, pair.Value.Clone());
            }

            return result;
        }
    }
}
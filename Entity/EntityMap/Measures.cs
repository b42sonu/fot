namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    /// <summary>
    /// represents weight and volume entities 
    /// units are kg, cm and dm3
    /// </summary>
    public class Measures
    {
        public int WeightKg { get; set; }
        public int LengthCm { get; set; }
        public int WidthCm { get; set; }
        public int HeightCm { get; set; }
        public int VolumeDm3 { get; set; }
        public bool Changed { get; set; }
    }
}
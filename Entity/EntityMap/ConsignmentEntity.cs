using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{

    public abstract class ConsignmentEntity
    {
        // Set to false initially, and updated after we do a validation against LM
        // Set to true if we both are online, and validate call could be executed
        public bool IsScannedOnline { get; set; }

        // Set to true after validation against LM, if either scanned consignment is known
        // or consignment related to scanned consignment item could be retrieved
        public bool IsKnownInLm;

        public abstract string EntityId { get; }
        public abstract string EntityDisplayId { get; }

        public abstract string ConsignmentId { get; }
        public abstract string ConsignmentDisplayId { get; }
        public abstract string ConsignmentIdForGoodsEvent { get; }
        public abstract string ConsignmentIdForValidateGoods { get; }


        public bool IsMassRegistrationAllowed { get; set; }
        public const string UndefinedConsignmentId = "NPR";
        public const string UnknownProduct = "9999";
        public BarcodeSubType BarcodeSubType;

        public bool IsPruScanning
        {
            get { return (BarcodeSubType & BarcodeSubType.PruTracking) == BarcodeSubType.PruTracking; }
        }

        public DateTime EntityCreatedOn { get; set; }


        public abstract Measures Measures { get; set; }

        // ReSharper disable InconsistentNaming
        protected string _orgUnit;
        protected string _orderNumber;
        protected string _customerId;
        protected string _productCategory;
        // ReSharper restore InconsistentNaming        

        public bool HasUnknownConsignment
        {
            get
            {
                Logger.LogAssert(string.IsNullOrEmpty(ConsignmentId) == false);
                return ConsignmentId == UndefinedConsignmentId;
            }
        }

        public string OrgUnit { get; set; }
        public abstract string OrderNumber { get; set; }
        public abstract string CustomerId { get; set; }
        public abstract string ProductCategory { get; set; }
        public string ValidationCode { get; set; }
        public string ValidationCodeRouting { get; set; }
        public string ValidationEventCode { get; set; }
        public decimal Weight { get; set; }
        public decimal Volume { get; set; }


        public string ProductCode
        {
            get { return _productCode; }
            set { _productCode = string.IsNullOrEmpty(value) && CommunicationClient.Instance.IsConnected() ? UnknownProduct : value; }
        }

        public VASType[] Vas { get; set; }
        public EntityStatus EntityStatus;
        private Damage _damage;
        public Damage Damage
        {
            get { return _damage ?? (_damage = new Damage()); }
            set
            { _damage = value; }
        }

        public GoodsEvents GoodsEvent { get; set; }

        public string LoadCarrierId { get; set; }

        private PlannedConsignmentsType _plannedConsignment;
        private string _productCode;

        public PlannedConsignmentsType PlannedConsignment
        {
            get { return _plannedConsignment; }
            set
            {
                if (value != null)
                    _plannedConsignment = value;
            }
        }


        // If scanned a consignment, after successfull validation against LM this
        // will contain expected consignment item count for this consignment (if known)
        public int ConsignmentItemCountFromLm { get; set; }

        public CommunicationEntity.Validation.EventType[] Event { get; set; }

        public bool IsBlocked { get; set; }


        public bool HasStatus(EntityStatus hasThisStatus)
        {
            return (EntityStatus & hasThisStatus) != 0;
        }

        public void RemoveStatus(EntityStatus statusToRemove)
        {
            EntityStatus &= ~statusToRemove;
        }

        public abstract Consignment ConsignmentInstance { get; set; }

        public abstract bool IsConsignmentSynthetic { get; set; }

        public ConsignmentItem CastToConsignmentItem()
        {
            if (this is ConsignmentItem == false)
                Logger.LogEvent(Severity.Error, "Failed to cast ConsignmentEntity to ConsignmentItem");
            return (ConsignmentItem)this;
        }

        public Consignment CastToConsignment()
        {
            if (this is Consignment == false)
            {
                Logger.LogEvent(Severity.Error, "Failed to cast ConsignmentEntity to Consignment");
            }
            return (Consignment)this;
        }

        public LoadCarrier CastToLoadCarrier()
        {
            if (this is LoadCarrier == false)
            {
                Logger.LogEvent(Severity.Error, "Failed to cast ConsignmentEntity to Load Carrier");
            }
            return (LoadCarrier)this;
        }
    }
}
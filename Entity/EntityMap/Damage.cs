﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    //Used for registrating a damage consignment entity
    public class Damage
    {        
        public bool ValidScan { get; set; }
        public DamageCode DamageCode { get; set; }
        public CauseMeasure CauseMeasure { get; set; }
        public String Comment { get; set; }
        public bool Confirmed { get; set; }
        public int ConsignmentItemCount { get; set; }
        public string FolderName { get; set; }
        public string Placement { get; set; }
        public Signature Signature { get; set; }

        public ReasonType ReasonType { get; set; }
        public ActionType ActionType { get; set; }

        public int CountForSingelDamage()
        {
            //Return 1 if not item count has been set. 1 is for counting it self
            return ConsignmentItemCount != 0 ? ConsignmentItemCount : 1;
        }

        public void ResetDamageForNewScan()
        {
            ValidScan = false;
            ConsignmentItemCount = 0;
            Confirmed = false;
            FolderName = null;
            Signature = null;
        }

        public bool HasSignature()
        {
            return Signature != null;
        }

        public bool HasImages()
        {
            return FolderName != null;
        }

        public Damage Clone()
        {
            var result = (Damage)MemberwiseClone();
            if (Signature != null)
                result.Signature = Signature.Clone();
            if (DamageCode != null)
                result.DamageCode = DamageCode.Clone();
            if (CauseMeasure != null)
                result.CauseMeasure = CauseMeasure.Clone();
            return result;
        }
    }
}

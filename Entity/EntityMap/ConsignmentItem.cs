﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    public class ConsignmentItem : ConsignmentEntity
    {
        public string ItemId { get; set; }

        public string ItemDisplayId
        {
            get
            {
                //return ItemId != null ? StringUtil.ToDisplayEntity(ItemId) : ItemId;
                return String.Format(@"#ItemDisplayId:{0}|{1}|", ItemId != null ? StringUtil.ToDisplayEntity(ItemId) : ItemId, ItemId);
                // "Consignment number #ItemDisplayId:370...8939|3707193498939| is registered."   
            }
        }

        public override string EntityId { get { return ItemId; } }
        public override string EntityDisplayId { get { return ItemDisplayId; } }

        public override sealed string ConsignmentId
        {
            get { return ConsignmentInstance != null ? ConsignmentInstance.ConsignmentId : UndefinedConsignmentId; }
        }

        public override string ConsignmentDisplayId { get { return StringUtil.ToDisplayEntity(ConsignmentId); } }

        public override string ConsignmentIdForValidateGoods
        {
            get
            {
                if (ConsignmentInstance == null || string.IsNullOrEmpty(ConsignmentInstance.ConsignmentIdForValidateGoods))
                {
                    Logger.LogEvent(Severity.Error, "Can not retrieve ConsignmentIdForValidateGoods for item {0}", ItemId);
                    return UndefinedConsignmentId;
                }
                return ConsignmentInstance.ConsignmentIdForValidateGoods;
            }
        }

        public override string ConsignmentIdForGoodsEvent
        {
            get
            {
                if (ConsignmentInstance != null)
                    return ConsignmentInstance.ConsignmentIdForGoodsEvent;

                // Never return NPR in goods event or ConsignmentIdWarning
                if (ConsignmentId == UndefinedConsignmentId || ConsignmentId == Consignment.SyntheticConsignmentIdWarning)
                    return string.Empty;

                return ConsignmentId;
            }
        }

        public DateTime? PlannedDeliveryDate { get; set; }
        public string DeliveryCode { get; set; }

        public override string ToString()
        {
            return string.Format(@"{0}/{1}", ConsignmentId, EntityId);
        }
        internal ConsignmentItem(string consignmentId, string consignmentItemId, EntityStatus status,
                                 ConsignmentItemType plannedConsignmentItem)
        {
            PlannedConsignmentItem = plannedConsignmentItem;
            ConsignmentInstance = new Consignment(consignmentId);
            ItemId = consignmentItemId;
            EntityStatus = status;
        }

        public ConsignmentItem()
        {
        }

        private ConsignmentItemType _plannedConsignmentItem;


        public ConsignmentItemType PlannedConsignmentItem
        {
            get { return _plannedConsignmentItem; }
            set
            {
                if (value != null)
                    _plannedConsignmentItem = value;
            }
        }

        public ConsignmentItem Clone()
        {
            return (ConsignmentItem)MemberwiseClone();
        }

        public override string OrderNumber
        {
            get
            {
                if (ConsignmentInstance != null && string.IsNullOrEmpty(ConsignmentInstance.OrderNumber) == false)
                    return ConsignmentInstance.OrderNumber;
                return _orderNumber;
            }
            set
            {
                if (ConsignmentInstance != null)
                    ConsignmentInstance.OrderNumber = value;
                else
                    _orderNumber = value;
            }
        }

        public override string CustomerId
        {
            get
            {
                if (ConsignmentInstance != null && string.IsNullOrEmpty(ConsignmentInstance.CustomerId) == false)
                    return ConsignmentInstance.CustomerId;
                return _customerId;
            }
            set
            {
                if (ConsignmentInstance != null)
                    ConsignmentInstance.CustomerId = value;
                else
                    _customerId = value;
            }
        }

        public override string ProductCategory
        {
            get
            {
                if (ConsignmentInstance != null && string.IsNullOrEmpty(ConsignmentInstance.ProductCategory) == false)
                    return ConsignmentInstance.ProductCategory;

                if (string.IsNullOrEmpty(_productCategory) == false)
                    return _productCategory;

                return string.Empty;
            }

            set
            {
                if (ConsignmentInstance != null)
                    ConsignmentInstance.ProductCategory = value;
                _productCategory = value;
            }
        }

        public string Shelf { get; set; }
        public string ShelfLocationOrgUnit { get; set; }

        public override sealed Consignment ConsignmentInstance { get; set; }

        private Measures _measures;

        public override Measures Measures
        {
            get { return _measures ?? (_measures = new Measures()); }
            set
            { _measures = value; }
        }


        public override bool IsConsignmentSynthetic
        {
            get
            {
                if (ConsignmentInstance != null)
                    return ConsignmentInstance.IsConsignmentSynthetic;
                return false;
            }
            set
            {
                if (ConsignmentInstance != null)
                    ConsignmentInstance.IsConsignmentSynthetic = value;
            }
        }
    }
}


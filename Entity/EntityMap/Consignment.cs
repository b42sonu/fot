﻿using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap
{
    public partial class Consignment : ConsignmentEntity
    {
        public static readonly string SyntheticConsignmentIdWarning = GlobalTexts.SyntheticConsignmentIdWarning;

        private string _consignmentId, _consignmentIdValue;

        // The real consignment number 
        public override string ConsignmentId { get { return _consignmentId; } }
        // Consignment number used for showing on screen
        public override string ConsignmentDisplayId
        {
            get
            {
                return _consignmentId != null ? StringUtil.ToDisplayEntity(_consignmentId): _consignmentId;
            }
        }

        // The real consignment number accessibel from entity
        public override string EntityId { get { return ConsignmentId; } }
        // Consignment number used for showing on screen accessible from entity
        public override string EntityDisplayId { get { return ConsignmentDisplayId; } }

        public override string ConsignmentIdForValidateGoods { get { return _consignmentIdValue; } }
        public override string ConsignmentIdForGoodsEvent
        {
            get
            {
                if (IsConsignmentSynthetic || ConsignmentId == UndefinedConsignmentId || ConsignmentId == SyntheticConsignmentIdWarning)
                    return string.Empty;
                return ConsignmentId;
            }
        }


        public Consignment()
        {

        }

        public override string ToString() { return EntityId; }

        internal Consignment(T20200_ValidateGoodsFromFOTReply validateGoodsReply)
        {
            IsConsignmentSynthetic = validateGoodsReply.Consignment.IsSynthetic;
            // Set product code
            if (validateGoodsReply.Consignment.ConsignmentItem != null)
                ProductCode = validateGoodsReply.Consignment.ConsignmentItem[0].ProductCode;

            // Set payment type
            Payments = validateGoodsReply.Consignment.Payment;

            EntityStatus = EntityStatus.Unknown;

            _consignmentId = validateGoodsReply.GetConsignmentNumber();
            _consignmentIdValue = validateGoodsReply.Consignment.ConsignmentNumber;
        }

        public void UpdateConsignmentId(T20200_ValidateGoodsFromFOTReply validateGoodsReply)
        {
            _consignmentId = validateGoodsReply.GetConsignmentNumber();
            _consignmentIdValue = validateGoodsReply.Consignment.ConsignmentNumber;
        }

        public Consignment(string consignmentId)
        {
            _consignmentIdValue = _consignmentId = consignmentId;
            ConsignmentItemsMap = new ConsignmentItemMap();
        }

        internal Consignment(string consignmentId, EntityStatus status, PlannedConsignmentsType plannedConsignment)
        {
            PlannedConsignment = plannedConsignment;
            _consignmentIdValue = _consignmentId = consignmentId;

            EntityStatus = status;
            ConsignmentItemsMap = new ConsignmentItemMap();
        }

        public static Consignment CreateSyntheticConsignment(ConsignmentItem consignmentItem)
        {
            var consignment = new Consignment
            {
                EntityStatus = EntityStatus.Unknown,
                IsConsignmentSynthetic = true,
                _consignmentId = UndefinedConsignmentId,
                _consignmentIdValue = UndefinedConsignmentId
            };

            if (consignmentItem.ItemId != null)
                consignment.ConsignmentItemsMap = new ConsignmentItemMap(consignmentItem);

            return consignment;
        }

        public override string ProductCategory
        {
            get
            {
                if (string.IsNullOrEmpty(_productCategory) == false)
                    return _productCategory;

                if (PlannedConsignment != null && string.IsNullOrEmpty(PlannedConsignment.ProductCategory) == false)
                    return PlannedConsignment.ProductCategory;

                return string.Empty;
            }
            set { _productCategory = value; }
        }

        public override string OrderNumber
        {
            get
            {
                if (PlannedConsignment != null && string.IsNullOrEmpty(PlannedConsignment.OrderNumber) == false)
                    return PlannedConsignment.OrderNumber;

                if (_orderNumber != null)
                    return _orderNumber;

                return string.Empty;
            }
            set { _orderNumber = value; }
        }

        public override string CustomerId
        {
            get
            {
                if (PlannedConsignment != null && string.IsNullOrEmpty(PlannedConsignment.CustomerId) == false)
                    return PlannedConsignment.CustomerId;

                if (_customerId != null)
                    return _customerId;

                return string.Empty;
            }
            set { _customerId = value; }
        }

        public override Consignment ConsignmentInstance
        {
            get { return this; }
            set { Logger.LogEvent(Severity.Error, "Can not assign consignment instance to self. Consignment {0}", value); }
        }

        public override sealed bool IsConsignmentSynthetic { get; set; }

        internal ConsigneeType ConsigneeInfo
        {
            get
            {
                return PlannedConsignment != null ? PlannedConsignment.Consignee : null;
            }
            set
            {
                if (PlannedConsignment != null)
                    PlannedConsignment.Consignee = value;
            }
        }

        internal ConsignorType ConsignorInfo
        {
            get
            {
                return PlannedConsignment != null ? PlannedConsignment.Consignor : null;
            }
        }

        private Measures _measures;

        public override Measures Measures
        {
            get { return _measures ?? (_measures = new Measures()); }
            set { _measures = value; }
        }


        // Union of all consignmentitems for a consignment, both planned and unplanned
        internal ConsignmentItemMap ConsignmentItemsMap;

        // If only scanning a consignment, the user can manually enter number of
        // consignment items the consignment will contain. In that case, the actual consignmentitems will be unknown
        public int ManuallyEnteredConsignmentItemCount;

        internal int ManuallyEnteredConsignmentItemCountPlanned;

        public Consignment Clone()
        {
            var result = (Consignment)MemberwiseClone();
            if (ConsignmentItemsMap != null)
                result.ConsignmentItemsMap = ConsignmentItemsMap.Clone();
            return result;
        }

        public ConsignmentItem GetConsignmentItem(string consignmentItemId)
        {
            ConsignmentItem result;
            ConsignmentItemsMap.TryGetValue(consignmentItemId, out result);
            return result;
        }

        public string ActorNumber { get; set; }
        public bool IsConsignmentEventRegAllowed { get; set; }
        public PaymentType[] Payments { get; set; }

        /// <summary>
        /// Return total of consignment items with a given status contained in a consignment. Manual item count override actual count if set
        /// and does not need to have all bits set
        /// </summary>
        /// <param name="status"></param>
        /// <param name="statusAll">Set to true if you specify a status with several bits set, and you need all bits specified to be set on item looked for</param>
        /// <returns></returns>
        public int ItemCount(EntityStatus status, bool statusAll)
        {
            // Manual planned count has priority for planned items 
            if ((status & EntityStatus.Planned) != EntityStatus.False && ManuallyEnteredConsignmentItemCountPlanned > 0)
            {
                return ManuallyEnteredConsignmentItemCountPlanned;
            }

            // Actual scanned items have priority over manual count
            if (this is LoadCarrier)//If load carrier return 0
                return 0;

            // If status all is specified, you want to count items with all specified statuses, otherwise count items with at least one
            // of the statuses specified
            int result = ConsignmentItemsMap.Values.Count(consignmentItem => statusAll == false &&
                (consignmentItem.EntityStatus & status) != EntityStatus.False || statusAll && consignmentItem.EntityStatus == status);
            if (result > 0)
                return result;


            // Manual consignment count has priority for scanned items 
            if (status == EntityStatus.Scanned && ManuallyEnteredConsignmentItemCount > 0)
            {
                return ManuallyEnteredConsignmentItemCount;
            }

            return 0;
        }

        /// <summary>
        /// Return total of consignment items which has been scanned, but with ManualItemCount as second priority
        /// </summary>
        /// <returns></returns>
        public int ScannedItemCount()
        {
            // If status all is specified, you want to count items with all specified statuses, otherwise count items with at least one
            // of the statuses specified
            return ConsignmentItemsMap.Values.Count(consignmentItem => consignmentItem.HasStatus(EntityStatus.Scanned));
        }
    }
}

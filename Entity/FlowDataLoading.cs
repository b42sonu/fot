﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    /// <summary>
    /// This class acts as initial data for flow loading.
    /// </summary>
    public class FlowDataLoading : BaseFlowData
    {

        public PlannedConsignmentsType[] SelectedConsignments;
        public bool IsLoadingDifferentStop; // Specifies that user started operation from "Load Different Stop" button.
        public bool IsStartedFromOperationList;
        public string PowerUnitId;
        public string LoadCarrierId;
        public bool IsLoadingStartedFromUnloading;
        public bool IsLoadLineHaul;
    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class TmiSetting
    {
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
}

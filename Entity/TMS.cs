﻿using System;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class Tms
    {
        public string TmsId { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdatedUtc { get; set; }
        public DateTime UpdatedServerUtc { get; set; }
        public int PmcRowStatus { get; set; }
    }
}

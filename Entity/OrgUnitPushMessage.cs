﻿using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class OrgUnitPushMessage : EntityBase
    {
        public string Message { get; set; }
    }
}

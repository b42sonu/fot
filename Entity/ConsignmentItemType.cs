﻿using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using EventType = Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation.EventType;

namespace Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList
{
    public partial class ConsignmentItemType
    {

        public EventType[] EventTypeList
        {
            get;
            private set;
        }

        public string Status { get; set; }

        //As OperationList detail object and RetriveProductGroup detail object is diffrent, this method convert RetriveProductGroup object to OperationList object
        public void SetDataFromValidateGoodsResponse(ValidationResponseConsignmentConsignmentItem responseConsignmentItem)
        {
            if (responseConsignmentItem != null)
            {
                Weight = new MeasureType
                    {
                        Amount = responseConsignmentItem.Weight,
                        MeasureUnit = responseConsignmentItem.WeightUnit
                    };

                Volume = new MeasureType();
                Volume.Amount = responseConsignmentItem.Volume;
                if (responseConsignmentItem.VolumeUnit != null)
                    Volume.MeasureUnit = responseConsignmentItem.VolumeUnit;
                else
                    Volume.MeasureUnit = string.Empty;

                //Height = new MeasureType { Amount = responseConsignmentItem.Height };
                //Width = new MeasureType { Amount = responseConsignmentItem.Width };
                //Length = new MeasureType { Amount = responseConsignmentItem.Length };

                EventTypeList = responseConsignmentItem.Event;
            }

        }


    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    /// <summary>
    /// Dummy printer class
    /// </summary>
    public class EntityPrinter
    {
        public EntityPrinter(string printerName)
        {
            Name = printerName;
        }
        public string Name { get; set; }

    }

}

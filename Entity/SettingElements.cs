﻿using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    /// <summary>
    /// Class containing settingElement relate data.
    /// </summary>
    public class SettingElements : PreCom.Core.Communication.EntityBase
    {
        public SettingElements()
        {
            UserSettings = new List<SettingElement>();
            CommonSettings = new List<SettingElement>();
        }

        /// <summary>
        /// Gets or sets settings assigned to the user.
        /// </summary>
        public List<SettingElement> UserSettings { get; set; }

        /// <summary>
        /// Gets or sets settingElement common to all users
        /// </summary>
        public List<SettingElement> CommonSettings { get; set; }

        /// <summary>
        /// Gets or sets user name.
        /// </summary>
        public string UserName { get; set; }
    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public enum EventResultState
    {
        Ok,
        Cancel,
        Error,
        Finished
    }

    public class EventResult
    {
        public EventResult(){}

        public EventResult(EventResultState state, object data)
        {
            State = state;
            Data = data;
        }

        public EventResultState State;
        public object Data;
    }
}

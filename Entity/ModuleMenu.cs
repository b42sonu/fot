﻿using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class ModuleMenuItem
    {
        public string Text { get; set; }
        public RoleData[] Roles;
        public int ActionIndex;
        public bool Disabled { get; set; }
        public bool Hidden { get; set; }
    }

    public class RoleData
    {
        public RoleData(Role role, int menuIndex)
        {
            Role = role;
            MenuIndex = menuIndex;
            TMSAffiliation = null;
        }

        public RoleData(Role role, string tmsAffiliation, int menuIndex)
        {
            Role = role;
            TMSAffiliation = tmsAffiliation;

            MenuIndex = menuIndex;
        }

        public Role Role;
        public readonly string TMSAffiliation;
        public int MenuIndex; // Order index for menu item
    }

    public class ModuleMenu
    {
        public ModuleMenuItem[] MenuItems;
        public ModuleMenuItem[] FilterItems(Role role, string tmsAffiliation)
        {
            //Remove hidden items
            var noHiddenItems = MenuItems.Where(moduleMenuItem => moduleMenuItem.Hidden == false).ToList();
            
            var result = new List<ModuleMenuItem>();
            // Return only the menu items for given role
            foreach (var moduleMenuItem in noHiddenItems)
            {
                result.AddRange(from roleData in moduleMenuItem.Roles where roleData.Role == role && (roleData.TMSAffiliation == null || roleData.TMSAffiliation == tmsAffiliation) select moduleMenuItem);
            }

            result.Sort((entry1, entry2) => Compare(role, entry1.Roles, entry2.Roles));
            return result.ToArray();
        }

        int Compare(Role role, IEnumerable<RoleData> rolesA, IEnumerable<RoleData> rolesB)
        {
            var roleTypeA = rolesA.FirstOrDefault(roleData => roleData.Role == role);
            var roleTypeB = rolesB.FirstOrDefault(roleData => roleData.Role == role);
            if (roleTypeA != null && roleTypeB != null)
            {
                int valueA = roleTypeA.MenuIndex;
                int valueB = roleTypeB.MenuIndex;

                return valueA - valueB;
            }
            return 0;
        }

        // 

        /// <summary>
        /// If only one menu-item is accessible for role
        /// retrieve this action
        /// </summary>
        public int GetSingleAction()
        {
            if (MenuItems.Count() == 1)
            {
                return MenuItems[0].ActionIndex;
            }
            
            return -1;
        }
    }
}


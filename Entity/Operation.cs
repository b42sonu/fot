﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Process = Com.Bring.PMP.PreComFW.Shared.Storage.Process;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public enum OperationType
    {
        Loading = 0,
        Unloading = 1,
        Delivery = 2,
        LoadLineHaulTruck = 3,
        LoadingDistributionTruck = 4,
        UnloadLineHaulTruck = 5,
        UnloadPickUpTruck = 6,
        PickUp = 7,
        Unknown = 8,
        LoadCombinationTrip = 9,
        BuildingOfLoadCarrier = 10,
    }


    public enum LoadingOperationStatus
    {
        New = 0,
        Started = 1,
        Finished = 2
    }

    public class Operation
    {

        //TODO: DeliveryOperation and Operation should be combined. Fix when delivery 2 has started
        public Operation(DeliveryOperation deliveryOperation)
        {
            OperationDetail = deliveryOperation.OperationDetail;
            Status = (LoadingOperationStatus)(deliveryOperation.Status - 1);
            PlannedConsignment = deliveryOperation.Consignment;
            RouteId = deliveryOperation.RouteId;
            PhysicalLoadCarrierId = deliveryOperation.PhysicalLoadCarrierId;
            LogicalLoadCarrierId = deliveryOperation.LogicalLoadCarrierId;
            ExternalTripId = deliveryOperation.ExternalTripId;
            OperationId = deliveryOperation.Consignment.ConsignmentNumber;
            OperationType = OperationType.Delivery;
            TripId = deliveryOperation.TripId;
            IsLoadListOperation = true;
        }



        public PlannedConsignmentsType PlannedConsignment { get; set; }

        public Operation()
        {
        }

        public OperationType OperationType { get; set; }
        public static OperationType ActiveOperationType
        {
            get
            {
                Logger.LogAssert(BaseModule.CurrentFlow.CurrentProcess == Process.Loading
                             || BaseModule.CurrentFlow.CurrentProcess == Process.Unloading
                             || BaseModule.CurrentFlow.CurrentProcess == Process.LoadLineHaul
                             || BaseModule.CurrentFlow.CurrentProcess == Process.LoadDistribTruck
                             || BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer
                             || BaseModule.CurrentFlow.CurrentProcess == Process.UnloadLineHaul
                             || BaseModule.CurrentFlow.CurrentProcess == Process.UnloadPickUpTruck
                             || BaseModule.CurrentFlow.CurrentProcess == Process.Pickup
                             );
                switch (BaseModule.CurrentFlow.CurrentProcess)
                {
                    case Process.DeliveryToCustomer:
                        return OperationType.Delivery;

                    case Process.LoadLineHaul:
                        return OperationType.LoadLineHaulTruck;

                    case Process.LoadDistribTruck:
                        return OperationType.LoadingDistributionTruck;

                    case Process.Loading:
                        return OperationType.Loading;

                    case Process.Unloading:
                        return OperationType.Unloading;

                    case Process.UnloadLineHaul:
                        return OperationType.UnloadLineHaulTruck;

                    case Process.UnloadPickUpTruck:
                        return OperationType.UnloadPickUpTruck;
                    case Process.Pickup:
                        return OperationType.PickUp;
                    default:
                        Logger.LogEvent(Severity.Error, "Unexpected process found when determining OperationType");
                        return OperationType.Unloading;
                }
            }
        }

        public string OperationId { get; set; }
        public string StopId { get; set; }
        public string TripId { get; set; }
        public string OperationDetail { get; set; }
        public LoadingOperationStatus Status { get; set; }
        public string RouteId { get; set; }
        public string PhysicalLoadCarrierId { get; set; }
        public string LogicalLoadCarrierId { get; set; }
        public string ExternalTripId { get; set; }



        //new property added for consignor address and order number...
        public string OrderNumber { get; set; }
        public string ConsignorName { get; set; }
        public string ConsignorFullAddress { get; set; }
        public bool IsLoadListOperation { get; set; }
        public bool IsFromLoadList()
        {
            return OperationId == null;
        }


    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public abstract class ViewEvent
    {
        public int Value;

        public static implicit operator int(ViewEvent viewEvent)
        {
            return viewEvent.Value;
        }
    }
}

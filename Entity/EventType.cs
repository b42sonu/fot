﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class EventType : IStatus
    {
        public string LangCode { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }

        public override string ToString()
        {
            return Type;
        }
    }
}

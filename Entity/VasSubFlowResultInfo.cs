﻿using Com.Bring.PMP.PreComFW.CommunicationEntity;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    /// <summary>
    /// Stores VAS Information for process (to be used in Goods Event)
    /// </summary>
    public class VasSubFlowResultInfo
    {
        public Signature Signature { get; set; }

        public string RecipientName { get; set; }
        public string RecipientId { get; set; }
        public string IdType { get; set; }

        public bool AuthorizationUsed { get; set; }
        public string EventComment { get; set; }
    }
}

﻿using System;
namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class OperationProcess
    {
        public void Update(string stopId, string operationId, string tripId, string externalTripId, string routeId, bool isLoadListOperation, string orderId)
        {
            StopId = stopId;
            OperationId = operationId;
            TripId = tripId;
            ExternalTripId = externalTripId;
            RouteId = routeId;
            IsLoadListOperation = isLoadListOperation;
            OrderId = orderId;

        }

        public void UpdateFromWorkListItem(WorkListItem currrentWorkListItem)
        {
            if (currrentWorkListItem == null)
                return;
            WorkListItemType = currrentWorkListItem.Type;
            StopId = currrentWorkListItem.StopId;
            TripId = currrentWorkListItem.TripId;
            OperationId = currrentWorkListItem.OperationId;
            switch (currrentWorkListItem.Type)
            {
                case WorkListItemType.RouteCarrierTrip:
                    RouteId = currrentWorkListItem.Rbt != null ? currrentWorkListItem.Rbt.RouteId : string.Empty;
                    PhysicalLoadCarrierId = currrentWorkListItem.Rbt != null ? currrentWorkListItem.Rbt.LoadCarrierId : string.Empty;
                    PowerUnitId = currrentWorkListItem.Rbt != null ? currrentWorkListItem.Rbt.PowerUnitId : string.Empty;
                    LogicalLoadCarrierId = currrentWorkListItem.LoadCarrierId;
                    break;
                case WorkListItemType.LoadCarrier:
                    //If worklist item is default WLI, then it means either this WLI is created from
                    //operation list operation or from some stop. In both cases load carrier will be
                    //PowerUnitId and will be treated as PhysicalLoadCarrier.
                    if (currrentWorkListItem.IsDefaultWli == false)
                        LogicalLoadCarrierId = currrentWorkListItem.LoadCarrierId;
                    else
                        PhysicalLoadCarrierId = currrentWorkListItem.LoadCarrierId;
                    break;
                case WorkListItemType.Route:
                    RouteId = currrentWorkListItem.RouteId;
                    //In case if user entered the route, then LoadCarrier will have value of PowerUnitId
                    //from his profile and will be treated as Physical Load Carrier.
                    PhysicalLoadCarrierId = currrentWorkListItem.LoadCarrierId;
                    break;
            }
        }


        public string StopId { get; set; }
        public string OperationId { get; set; }

        public string TripId { get; set; }
        public string ExternalTripId { get; set; }
        public string RouteId { get; set; }
        
        /// <summary>
        /// this property user for new oprtation list implementation in 1j
        /// </summary>
        public bool IsLoadListOperation { get; set; }
        /// <summary>
        /// property used orderid and added for alystra operation list ...
        /// </summary>
        public string OrderId { get; set; }
        public string LogicalLoadCarrierId { get; set; }
        public string PhysicalLoadCarrierId { get; set; }
        public string PowerUnitId { get; set; }
        public WorkListItemType WorkListItemType { get; set; }
        public DateTime TimeForFirstScan { get; set; }
        public string OperationOriginSystem { get; set; }

        public string Location { get; set; }//Added by Syed to enable scanning in US 9 on 10/14/13
        

        public void ClearOperationProcessDetails()
        {
        
        }


        //As this is a global static reference, we need to set an empty object when starting a new process.        
        public void Clear()
        {
            StopId = string.Empty;
            OperationId = string.Empty;
            TripId = string.Empty;
            ExternalTripId = string.Empty;
            RouteId = string.Empty;
            IsLoadListOperation = false;
            OrderId = string.Empty;
            LogicalLoadCarrierId = string.Empty;
            PhysicalLoadCarrierId = string.Empty;
            PowerUnitId = string.Empty;
            WorkListItemType = WorkListItemType.UnKnown;            
        }


    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public interface IStatus
    {
        string Code { get; set; }
    }
}

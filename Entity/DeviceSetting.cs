﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class DeviceSetting
    {
        public string DeviceType { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        
 
    }
}

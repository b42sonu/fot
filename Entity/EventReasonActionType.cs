﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class EventReasonActionType
    {
        public string EventCode { get; set; }
        public string ReasonCode { get; set; }
        public string ActionCode { get; set; }
    }
}

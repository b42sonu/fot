﻿
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public enum WorkListItemType
    {
        UnKnown,
        LoadCarrier,
        RouteCarrierTrip,
        Route
    }

    public enum WorkListType
    {
        LoadUnload,
        BuildingOfLoadCarrier
    }

    public enum WorkListItemStatus
    {
        Completed,
        UnCompleted
    }

    public class CombinationTrip
    {
        public WorkListItem LoadLineHaulWli { get; set; }
        public WorkListItem LoadDistribWli { get; set; }
        public string Guid { get; set; }

    }

    public class WorkListItem
    {
        public EntityMap.EntityMap EntityMap { get; set; }
        public WorkListItemType Type { get; set; }
        public string LoadCarrierId { get; set; }
        public string PhysicalLoadCarrierId { get; set; }
        public string RouteId { get; set; }
        public string ExternalTripId { get; set; }
        public string StopId { get; set; }
        public string TripId { get; set; }
        public string PowerUnitId { get; set; }
        public BarcodeRouteCarrierTrip Rbt { get; set; }
        public string RecipentEmailAddress { get; set; }
        public LogicalLoadCarrierType LoadCarrierDetails { get; set; }
        public PhysicalLoadCarrierType PhysicalLoadCarrierDetails { get; set; }
        public OperationType StopType { get; set; }
        public bool IsForCombinationTrip { get; set; }
        public WorkListItemStatus Status { get; set; }
        public string Guid { get; set; }
        public OperationType CombinationTripType { get; set; }
        public bool IsDefaultWli { get; set; }
        public LocationElement Location { get; set; }
        public bool IsLoadCarrierKnownInLm { get; set; }
        public string OperationId { get; set; }
        public string LoadCarrierDisplayId
        {
            get
            {
                if (Type == WorkListItemType.LoadCarrier)
                    return StringUtil.ToDisplayEntity(LoadCarrierId);
                return LoadCarrierId;
            }
        }


        public string ActiveCarrier
        {
            get
            {
                var activeCarrier = Type == WorkListItemType.Route ? RouteId : LoadCarrierDisplayId;
                return activeCarrier;
            }
        }


        public string GetDestination()
        {
            switch (Type)
            {
                case WorkListItemType.LoadCarrier:
                    if (LoadCarrierDetails != null)
                        return LoadCarrierDetails.DestinationName1 + " " + LoadCarrierDetails.DestinationName2;
                    break;
                case WorkListItemType.RouteCarrierTrip:
                case WorkListItemType.Route:
                    return RouteId;

            }
            return string.Empty;
        }

        public static List<WorkListItem> GetWorkList(WorkListType workListType)
        {
            List<WorkListItem> workList = null;
            switch (workListType)
            {
                case WorkListType.LoadUnload:
                    workList = (from c in ModelMain.WorkListItems
                                where c.StopType != OperationType.BuildingOfLoadCarrier
                                select c).ToList();
                    break;
                case WorkListType.BuildingOfLoadCarrier:
                    workList = (from c in ModelMain.WorkListItems
                                where c.StopType == OperationType.BuildingOfLoadCarrier
                                select c).ToList();
                    break;
            }
            return workList;
        }


        public string Detail
        {
            get
            {
                var detail = string.Empty;
                switch (Type)
                {
                    case WorkListItemType.LoadCarrier:
                        detail = LoadCarrierId + (string.IsNullOrEmpty(TripId) ? string.Empty : "/" + TripId);
                        break;
                    case WorkListItemType.RouteCarrierTrip:
                        detail = LoadCarrierId + (string.IsNullOrEmpty(RouteId) ? string.Empty : "/" + RouteId) + (string.IsNullOrEmpty(TripId) ? string.Empty : "/" + TripId);
                        break;
                    case WorkListItemType.Route:
                        detail = RouteId + (string.IsNullOrEmpty(TripId) ? string.Empty : "/" + TripId);
                        break;
                }
                //Mark WLI's related to combination trip with star.
                if (!string.IsNullOrEmpty(detail) && StopType == OperationType.LoadCombinationTrip)
                {
                    detail = detail + " " + "*";
                }

                return detail;
            }
        }

        public WorkListItem Clone()
        {
            var workListItem = (WorkListItem)MemberwiseClone();
            if (EntityMap != null)
                workListItem.EntityMap = EntityMap.DeepClone();
            return workListItem;

        }
    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Entity
{
    public class Stop
    {
        public OperationType StopType { get; set; }
        public string StopId { get; set; }
        public string TripId { get; set; }
        public string OperationDetail { get; set; }
        public LoadingOperationStatus Status { get; set; }
        public string ExternalTripId { get; set; }
        public string Location { get; set; }//Added by syed to enable scanning in US 9 on 10/14/13
        public string PowerUnitId { get; set; }
        public string RouteId { get; set; }
        public string LoadCarrierId { get; set; }
    }


    
    public static class OperationStatus
    {
        public const string New = "New";
        public const string NewOrModified = "NewOrModified";
        public const string Started = "Started";
        public const string Finished = "Finished";
        public const string Canceled = "Canceled";
    }

    public static class ConfirmationStatus
    {
        public const string New = "New";
        public const string Updated = "Updated";
        public const string Confirmed = "Confirmed";
        public const string Canceled = "Canceled";

        public const string Accepted = "Accepted";
        public const string Rejected = "Rejected";
    }


    // The operation type of stops
    public static class StopOperationType
    {
        /* 
         1) This status comes as status of New operation from LM
         2) We use this status internally to trace if operation is new in current stop
        */
        public const string New = "New";
        //  We use this status to mark operation as accepted if user have clicked on accepted/accept all button. 
        public const string Accepted = "Accepted";
        //  We use this status to mark operation as rejected if user have clicked on reject button. 
        public const string Rejected = "Rejected";
        //  This status comes as status of deleted operation in case of both Alystra/Amphora from LM
        public const string Deleted = "Deleted";
        //  Currently we are using this status as Updated operation in case of Amphora from LM
        public const string Updated = "Updated";
        /*
         To check if operation is started by user.
         Internally using this status to mark status of stop, on the basis of status for its operations.
         Expecting this status for started operation in updated Amphora Operation list.
         */
        public const string Started = "Started";
        /*
         Using this status to mark operation status to finished after user sent events (goods event) for a particular operation.
         Specifies that current operation is finished by user.
         Expecting this status for finished operations for Amphora/Alystra, from LM
         To check if operation is already started.
        */
        public const string Finished = "Finished";
        //Using this status while sending operation finished event.
        public const string Completed = "Completed";
        //Using this status while sending ArriveAtStopRequest in case of Amphora
        public const string Arrived = "Arrived";
        //Not using this status currently
        public const string Received = "Received";
        //Using this status while sending DepartureFromStopRequest in case of Amphora
        public const string Departed = "Departed";

        public static bool IsStarted(string operationType)
        {
            return operationType == Started || operationType == Finished;
        }

        public static bool IsNotAcceptedRejected(string operationType)
        {
            return operationType != Accepted && operationType != Rejected;
        }

        public static bool IsAcceptedRejected(string operationType)
        {
            return operationType == Accepted || operationType == Rejected;
        }
    }
}

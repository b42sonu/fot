﻿using System;
using Com.Bring.PMP.PreComFW.IntegrationEngine;
using PMC.PreComX.Mock;
using PreCom.Core;
using Com.Bring.PMP.PreComFW.IntegrationEngine.Delegates;
using PreCom.Core.Communication;
using PreCom.Core.Modules;
using LogItem = PreCom.Core.Log.LogItem;

namespace Com.Bring.PMP.PreComFW.Shared
{
    public class ModuleRegistrationUtils
    {
        private readonly IIntegrationEngine _integration;
        private readonly ILog _log;
        private readonly ICommunicationBase _communication;

        public ModuleRegistrationUtils(IIntegrationEngine integration, ILog log, ICommunicationBase communication)
        {
            if (integration == null) throw new ArgumentNullException("integration");
            if (log == null) throw new ArgumentNullException("log");
            if (communication == null) throw new ArgumentNullException("communication");

            _integration = integration;
            _log = log;
            _communication = communication;
        }

        public void registerRequestHandler<T>(IntegrationRequestDelegate<T> method)
            where T : EntityBase
        {
            try
            {
                _log.Write(new LogItem(null, LogLevel.Information,
                                                      "RegisterRequestHandler", "Registrating handler for ",
                                                      typeof(T).ToString()));
                _integration.RegisterRequestNode<T>(method);
            }
            catch (InvalidOperationException e)
            {
                // The node is already registered. This error shall already have been logged by the integration. 
                _log.Write(new LogItem(null, LogLevel.Information, "RegisterRequestHandler",
                                                      "Catched exeption ", e.ToString()));
            }
        }

        public void registerResponseHandler<T>(IntegrationReceiveDelegate<T> method)
            where T : EntityBase
        {
            try
            {
                _log.Write(new LogItem(null, LogLevel.Information,
                                                      "RegisterResponseHandler", "Registrating handler for ",
                                                      typeof(T).ToString()));
                _integration.RegisterResponseNode<T>(method);
            }
            catch (InvalidOperationException e)
            {
                // The node is already registered. This error shall already have been logged by the integration. 
                _log.Write(new LogItem(null, LogLevel.Information, "RegisterResponseHandler",
                                                      "Catched exeption ", e.ToString()));
            }
        }

        public void registerReceiveHandler<T>(ReceiveDelegate<T> method)
            where T : EntityBase
        {
            try
            {
                _log.Write(new LogItem(null, LogLevel.Information,
                                                      "RegisterReceiveHandler", "Registrating handler for ",
                                                      typeof(T).ToString()));
                _communication.Register<T>(method);
            }
            catch (InvalidOperationException e)
            {
                // The node is already registered. This error shall already have been logged by the integration. 
                _log.Write(new LogItem(null, LogLevel.Information, "RegisterReceiveHandler",
                                                      "Catched exeption ", e.ToString()));
            }
        }


#if !PreComLT
        public void registerQueryHandler<T>(QueryDelegate<T> method)
            where T : EntityBase
        {
            try
            {
                _log.Write(new LogItem(null, LogLevel.Information,
                                                      "RegisterQueryHandler", "Registrating handler for ",
                                                      typeof(T).ToString()));
                _communication.Register<T>(method);
            }
            catch (InvalidOperationException e)
            {
                // The node is already registered. This error shall already have been logged by the integration. 
                _log.Write(new LogItem(null, LogLevel.Information, "RegisterQueryHandler",
                                                      "Catched exeption ", e.ToString()));
            }
        }
#endif
    }
}
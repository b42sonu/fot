﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Com.Bring.PMP.PreComFW.IntegrationEngine;
using Com.Bring.PMP.PreComFW.IntegrationEngine.Classes;
using PreCom.Core;
using PreCom.Core.Modules;

namespace Com.Bring.PMP.PreComFW.Shared
{
    /// <summary>
    /// The logger.
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Logs the info message.
        /// </summary>
        public static void LogErrorMessage(ModuleBase source, string headerMessage, string bodyMessage, string dumpMessage)
        {
            if (source != null)
            {
                WriteToLog(source, source.Name, headerMessage, bodyMessage, dumpMessage, LogLevel.Error);
            }
            else
            {
                WriteToLog(null, string.Empty, headerMessage, bodyMessage, dumpMessage, LogLevel.Error);
            }
        }

        /// <summary>
        /// Logs the error message.
        /// </summary>
        public static void LogErrorMessage(ModuleBase source, string message)
        {
            if (source != null)
            {
                WriteToLog(source, source.Name, null, message, null, LogLevel.Error);
            }
            else
            {
                WriteToLog(null, string.Empty, null, message, null, LogLevel.Error);
            }
        }

        /// <summary>
        /// Logs the info message.
        /// </summary>
        public static void LogInfoMessage(ModuleBase source, string headerMessage, string bodyMessage, string dumpMessage)
        {
            if (source != null)
            {
                WriteToLog(source, source.Name, headerMessage, bodyMessage, dumpMessage, LogLevel.Information);
            }
            else
            {
                WriteToLog(null, string.Empty, headerMessage, bodyMessage, dumpMessage, LogLevel.Information);
            }
        }

        /// <summary>
        /// Logs the info message.
        /// </summary>
        public static void LogInfoMessage(ModuleBase source, string message)
        {
            if (source != null)
            {
                WriteToLog(source, source.Name, null, message, null, LogLevel.Information);
            }
            else
            {
                WriteToLog(null, string.Empty, null, message, null, LogLevel.Information);
            }
        }

        /// <summary>
        /// Logs the debug message.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="headerMessage">The header message.</param>
        /// <param name="bodyMessage">The body message.</param>
        /// <param name="dumpMessage">The dump message.</param>
        public static void LogDebugMessage(ModuleBase source, string headerMessage, string bodyMessage, string dumpMessage)
        {
            if (source != null)
            {
                WriteToLog(source, source.Name, headerMessage, bodyMessage, dumpMessage, LogLevel.Debug);
            }
            else
            {
                WriteToLog(null, string.Empty, headerMessage, bodyMessage, dumpMessage, LogLevel.Debug);
            }
        }

        /// <summary>
        /// Logs the debug message.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="message">The message.</param>
        public static void LogDebugMessage(ModuleBase source, string message)
        {
            if (source != null)
            {
                WriteToLog(source, source.Name, null, message, null, LogLevel.Debug);
            }
            else
            {
                WriteToLog(null, string.Empty, null, message, null, LogLevel.Debug);
            }
        }

        /// <summary>
        /// Writes to log, asynchronously.
        /// </summary>
        private static void WriteToLog(ModuleBase source, string modulename, string headerMessage, string bodyMessage, string dumpMessage, LogLevel logLevel)
        {
#if !PreComLT
            Task.Factory.StartNew(() =>
                                      {
                                          ILog log = PreCom.Application.Instance.CoreComponents.Get<ILog>();

                                          if (log != null)
                                          {
                                              // If write to log fails, try agin - workaround for bug in PreCom logging framework.
                                              bool writestatus = false;
                                              int tries = 0;
                                              while (!writestatus && tries++ < 5)
                                              {
                                                  writestatus = log.Write(new PreCom.Core.Log.LogItem()
                                                                              {
                                                                                  Source = source,
                                                                                  ID = GetModulename(modulename),
                                                                                  Level = logLevel,
                                                                                  CustomLevel = 0,
                                                                                  Header = GetHeaderMessage(headerMessage),
                                                                                  Body = GetBodyMessage(bodyMessage),
                                                                                  Dump = GetDumpMessage(dumpMessage)
                                                                              });
                                                  Thread.Sleep(5);
                                              }
                                          }
                                      });
#endif
        }

        public static void LogTiming(IntegrationMessageBase message, ushort timingType, TimedMessageType messageType, DateTime eventDateTimeUtc)
        {
#if !PreComLT
            IIntegrationEngine log = PreCom.Application.Instance.CoreComponents.Get<IIntegrationEngine>();
            if (log != null)
                log.LogTiming(message, timingType, messageType, eventDateTimeUtc);       
#endif
        }

        /// <summary>
        /// Gets the modulename.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Module name</returns>
        private static string GetModulename(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "";
            }

            return name;
        }

        /// <summary>
        /// Gets the header message.
        /// </summary>
        /// <param name="headerMessage">The header message.</param>
        /// <returns>header Message.</returns>
        private static string GetHeaderMessage(string headerMessage)
        {
            if (string.IsNullOrEmpty(headerMessage))
            {
                headerMessage = "";
            }

            return headerMessage;
        }

        /// <summary>
        /// Gets the body message.
        /// </summary>
        /// <param name="bodyMessage">The body message.</param>
        /// <returns>body Message.</returns>
        private static string GetBodyMessage(string bodyMessage)
        {
            if (string.IsNullOrEmpty(bodyMessage))
            {
                bodyMessage = "";
            }

            return bodyMessage;
        }

        /// <summary>
        /// Gets the dump message.
        /// </summary>
        /// <param name="dumpMessage">The dump message.</param>
        /// <returns>Dump Messag.</returns>
        private static string GetDumpMessage(string dumpMessage)
        {
            if (string.IsNullOrEmpty(dumpMessage))
            {
                dumpMessage = "";
            }

            return dumpMessage;
        }

        public static string ReadFaultException(FaultException faultException)
        {
            MessageFault messageFault = faultException.CreateMessageFault();
            StringBuilder logMessage = new StringBuilder();

            if (messageFault.HasDetail)
            {
                logMessage.AppendFormat("Exception message:\n{0}\n", faultException.Message);
                logMessage.AppendFormat("Code namespace   : {0}\n", faultException.Code.Namespace);
                logMessage.AppendFormat("Code name        : {0}\n", faultException.Code.Name);

                using (var xmlReader = messageFault.GetReaderAtDetailContents())
                {
                    while (!xmlReader.EOF)
                    {
                        if (xmlReader.NodeType != XmlNodeType.EndElement)
                        {
                            try
                            {
                                string localName = xmlReader.LocalName;
                                string readElementString = xmlReader.ReadElementString();
                                logMessage.AppendFormat("{0}: {1}\n", localName, readElementString);
                            }
                            catch (Exception)
                            { }
                        }
                        else
                        {
                            xmlReader.ReadEndElement();
                        }
                    }
                }
            }

            return logMessage.ToString();
        }
    }
}

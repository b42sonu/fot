﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Bring.PMP.PreComFW.Shared.Amphora
{
    /*
           <item xsi:type="ns2:axia_amphora_module_online_webservice_UserUpdate">
              <privatTelefonnummer xsi:type="xsd:string">97015195</privatTelefonnummer>
              <fornavn xsi:type="xsd:string">Dag</fornavn>
              <ETrustID xsi:type="xsd:string">1000</ETrustID>
              <dato xsi:type="xsd:string">2013-11-29T14:03:58</dato>
              <landkode xsi:type="xsd:string">NO</landkode>
              <postnummer xsi:type="xsd:string">1726</postnummer>
              <epost xsi:type="xsd:string">dag.lokkebakken@axia.no</epost>
              <id xsi:type="xsd:string">1</id>
              <rolle xsi:type="xsd:string">15</rolle>
              <fulltNavn xsi:type="xsd:string">Dag Løkkebakken</fulltNavn>
              <mobiltelefon xsi:type="xsd:string">97015195</mobiltelefon>
              <sjaforNr xsi:type="xsd:string">109992748</sjaforNr>
              <adresse xsi:type="xsd:string">Opsundveien 73A</adresse>
              <agentNr xsi:type="xsd:string"/>
              <etternavn xsi:type="xsd:string">Løkkebakken</etternavn>
              <sprak xsi:type="xsd:string">NO</sprak>
              <typeOppdatering xsi:type="xsd:string">I</typeOppdatering>
           </item>
     */

    public class AmphoraUserData
    {
        public AmphoraUserData()
        {
            IsCreated = false;
            IsUpdated = false;
        }

        public string UpdateType { get; set; }
        public string UserName { get; set; }
        public string AmpId { get; set; }
        public string SsaId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Date { get; set; }
        public string Language { get; set; }
        public string UserRole { get; set; }
        public string OrgUnitId { get; set; }
        public string CompanyCode { get; set; }
        public bool IsCreated { get; set; }
        public bool IsUpdated { get; set; }

        public string ToString()
        {
            return UpdateType + ";" +
                   UserName + ";" +
                   AmpId + ";" +
                   SsaId + ";" +
                   FirstName + ";" +
                   LastName + ";" +
                   Phone + ";" +
                   Email + ";" +
                   Date + ";" +
                   Language + ";" +
                   UserRole + ";" +
                   OrgUnitId + ";" +
                   CompanyCode + ";" +
                   IsCreated + ";" +
                   IsUpdated + ";";
        }
    }
}

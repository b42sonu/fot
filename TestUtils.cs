﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Com.Bring.PMP.PreComFW.IntegrationEngine;
using Com.Bring.PMP.PreComFW.IntegrationEngine.Classes;
using Com.Bring.PMP.CSharp;

namespace Com.Bring.PMP.PreComFW.Shared
{
    public static class TestUtils
    {
        public static IIntegrationEngine LogTiming(Guid transactionId, ushort timingType, TimedMessageType messageType, DateTime eventDateTimeUtc, Type t)
//        public static IIntegrationEngine LogTiming(Guid id)
        {
#if !PreComLT
            IIntegrationEngine integration = PreCom.Application.Instance.Modules.Get<IIntegrationEngine>();
            if (integration == null) return null;
            try
            {
                integration.LogTiming(transactionId, timingType, messageType, eventDateTimeUtc, t);
            }
            catch (Exception)
            {
                // Probably in test mode
            }
            return integration;
#else
            return null;
#endif        
        }
    }
}

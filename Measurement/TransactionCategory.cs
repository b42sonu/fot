﻿namespace Com.Bring.PMP.PreComFW.Shared.Measurement
{
    public enum TransactionCategory : byte
    {
        ValidateGoodsFromFot = 24,
        T20210_DriverAvailabilityToTMSRequest = 25,
        UserDetail = 26,
        GoodsEventsEventInformation = 27,
        UserInformation = 28,
        GoodsEvents = 29,
        OperationListEvent = 30,
        Settings = 31,
        UserLocation = 32,
        StaticDataRequest = 33,
        PushMessage = 34,
        ActualsSummaryMessage = 35,
        DepartureFromStopEvent = 36,
        OrgUnitPushUpdateMessage = 37,
        ConsignmentNote = 38,
        GetPrinterNames = 39,
        LoadCarrierEvent = 40,
        ConnectPhysicalToLogicalCarrierEvent = 41,
        ArrivalRegistration = 42,
        ControlScan = 43,
        DeliveryToCutomerAtPo = 44,
        Login = 45,
        ValidateResortedOperationlistRequest = 46,
        VASResponseEntity = 47,
        WorkStatusRequest = 48,
        AckOperationList = 49,
        PasswordChangedMessage = 50,
    }
}

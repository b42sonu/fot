﻿using System;
using System.Threading;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.MeasurementCore;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Measurement
{
    public static class TransactionLogger
    {
#if !PreComLT
        public static void LogTiming(Guid transactionId, Timing timing)
        {
#pragma warning disable 618
            PreCom.Application.Instance.Modules.Get<IMeasurementCore>().LogTiming(transactionId, timing);
#pragma warning restore 618
        }
#endif

        //public static ushort CalculateTimingType(this Timing source, TimingComponent component, TimingType type, TimingComponent toOrFrom)
        //{
        //    return TimingMeasurement.CalculateTimingCategoryValue(component, type, toOrFrom);
        //}

        public static void LogTiming<T>(Transaction transaction, Timing timing, uint timingType, CommunicationProtocol protocol, IMeasurementCore measurementModule, uint credentialId, string clientId)
            where T : EntityBase
        {
            timing.AssemblyType = typeof(T).FullName;
#if PreComLT
            timing.NetworkType = measurementModule.GetByteForNetworkType(PreCom.Core.Network.NetworkType.LAN);
            timing.IpAddress = GetIpAddress();
#else
            timing.NetworkType = measurementModule.GetByteForNetworkType(PreCom.Application.Instance.Platform.CurrentNetwork.Type);
            timing.IpAddress = PreCom.Application.Instance.Platform.CurrentNetwork.IpAddress;
#endif

            timing.ClientId = clientId;
            timing.CredentialId = credentialId;
            timing.CommunicationProtocol = protocol;

#if PreComLT
            timing.Platform = "PreComLT";
#else
            timing.Platform = PreCom.Application.Instance.Platform.Name;
#endif
            timing.Type = timingType;

            ThreadPool.QueueUserWorkItem(LogTimingInSeparateThread, new object[] { transaction, timing, measurementModule });
    
        }

        public static void LogTiming<T>(Transaction transaction, Timing timing, uint timingType, CommunicationProtocol protocol)
            where T : EntityBase
        {
#if PreComLT
            throw new NotImplementedException("Please use overloaded method for PreComLT");
#else
#pragma warning disable 618
            var client = PreCom.Application.Instance.Communication.GetClient();
#pragma warning restore 618
            var clientId = client.ClientID;
            var credentialId = (uint)client.CredentialId;
            Logger.LogEvent(Severity.Debug, "LogTiming start");

#pragma warning disable 618
            LogTiming<T>(transaction, timing, timingType, protocol, PreCom.Application.Instance.Modules.Get<IMeasurementCore>(), credentialId, clientId);
#pragma warning restore 618
            Logger.LogEvent(Severity.Debug, "LogTiming;" + (TransactionCategory)transaction.Category + " / " + (TimingType)timingType + ";ms=" + timing.TickCount + ";");
#endif
        }

#if PreComLT
        private static string GetIpAddress()
        {
            System.Net.IPHostEntry host;
            string localIP = "?";
            host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());

            foreach (System.Net.IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }

            return localIP;
        }
#endif

        private static void LogTimingInSeparateThread(object e)
        {
            try
            {
                var data = e as object[];

                if (data != null)
                {
                    var measurementCore = data[2] as IMeasurementCore;
                    if (measurementCore != null)
                        measurementCore.LogTiming(data[0] as Transaction, data[1] as Timing);
                }
            }
            catch (Exception ex)
            {
#if !PreComLT
                Logger.LogException(ex, "TransactionLogger.LogTimingInSeparateThread");
#endif
            }
        }
    }
}

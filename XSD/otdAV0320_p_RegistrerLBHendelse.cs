﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.0.30319.17929.
// 
namespace IS.Converters {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:posten.no/eC3/ONLINE/Elements/0320p_RegistrerLBHendelse")]
    [System.Xml.Serialization.XmlRootAttribute("x0320p_RegistrerLBHendelse", Namespace="urn:posten.no/eC3/ONLINE/Elements/0320p_RegistrerLBHendelse", IsNullable=false)]
    public partial class HD_CplxType {
        
        private VOHeader_CplxType headerField;
        
        private AV0320_RegistrerLBHendelse_CplxType dataField;
        
        /// <remarks/>
        public VOHeader_CplxType header {
            get {
                return this.headerField;
            }
            set {
                this.headerField = value;
            }
        }
        
        /// <remarks/>
        public AV0320_RegistrerLBHendelse_CplxType data {
            get {
                return this.dataField;
            }
            set {
                this.dataField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:posten.no/eC3/ONLINE/Elements/0320p_RegistrerLBHendelse")]
    public partial class VOHeader_CplxType {
        
        private string systemCodeField;
        
        private string securityTokenField;
        
        private string userNameField;
        
        private string versionField;
        
        private string timestampField;
        
        private string timestampEcInField;
        
        /// <remarks/>
        public string SystemCode {
            get {
                return this.systemCodeField;
            }
            set {
                this.systemCodeField = value;
            }
        }
        
        /// <remarks/>
        public string SecurityToken {
            get {
                return this.securityTokenField;
            }
            set {
                this.securityTokenField = value;
            }
        }
        
        /// <remarks/>
        public string UserName {
            get {
                return this.userNameField;
            }
            set {
                this.userNameField = value;
            }
        }
        
        /// <remarks/>
        public string Version {
            get {
                return this.versionField;
            }
            set {
                this.versionField = value;
            }
        }
        
        /// <remarks/>
        public string Timestamp {
            get {
                return this.timestampField;
            }
            set {
                this.timestampField = value;
            }
        }
        
        /// <remarks/>
        public string TimestampEcIn {
            get {
                return this.timestampEcInField;
            }
            set {
                this.timestampEcInField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.17929")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:posten.no/eC3/ONLINE/Elements/0320p_RegistrerLBHendelse")]
    public partial class AV0320_RegistrerLBHendelse_CplxType {
        
        private string selskapskodeField;
        
        private string enhetsnummerField;
        
        private string logiskLBField;
        
        private string logiskLBParentField;
        
        private string hendelsestypeField;
        
        private string arsakField;
        
        private string attributt1Field;
        
        private string attributt2Field;
        
        private string attributt3Field;
        
        private string avsenderField;
        
        private string avvikskodeField;
        
        private string enhetsnrHendelseField;
        
        private string enhetsnrOverlevertField;
        
        private string mottakerField;
        
        private string posisjonField;
        
        private string datoTidField;
        
        private string postnrHendelseField;
        
        private string tiltakField;
        
        private string signaturField;
        
        private string bildeField;
        
        private string kommentarField;
        
        /// <remarks/>
        public string Selskapskode {
            get {
                return this.selskapskodeField;
            }
            set {
                this.selskapskodeField = value;
            }
        }
        
        /// <remarks/>
        public string Enhetsnummer {
            get {
                return this.enhetsnummerField;
            }
            set {
                this.enhetsnummerField = value;
            }
        }
        
        /// <remarks/>
        public string LogiskLB {
            get {
                return this.logiskLBField;
            }
            set {
                this.logiskLBField = value;
            }
        }
        
        /// <remarks/>
        public string LogiskLBParent {
            get {
                return this.logiskLBParentField;
            }
            set {
                this.logiskLBParentField = value;
            }
        }
        
        /// <remarks/>
        public string Hendelsestype {
            get {
                return this.hendelsestypeField;
            }
            set {
                this.hendelsestypeField = value;
            }
        }
        
        /// <remarks/>
        public string Arsak {
            get {
                return this.arsakField;
            }
            set {
                this.arsakField = value;
            }
        }
        
        /// <remarks/>
        public string Attributt1 {
            get {
                return this.attributt1Field;
            }
            set {
                this.attributt1Field = value;
            }
        }
        
        /// <remarks/>
        public string Attributt2 {
            get {
                return this.attributt2Field;
            }
            set {
                this.attributt2Field = value;
            }
        }
        
        /// <remarks/>
        public string Attributt3 {
            get {
                return this.attributt3Field;
            }
            set {
                this.attributt3Field = value;
            }
        }
        
        /// <remarks/>
        public string Avsender {
            get {
                return this.avsenderField;
            }
            set {
                this.avsenderField = value;
            }
        }
        
        /// <remarks/>
        public string Avvikskode {
            get {
                return this.avvikskodeField;
            }
            set {
                this.avvikskodeField = value;
            }
        }
        
        /// <remarks/>
        public string EnhetsnrHendelse {
            get {
                return this.enhetsnrHendelseField;
            }
            set {
                this.enhetsnrHendelseField = value;
            }
        }
        
        /// <remarks/>
        public string EnhetsnrOverlevert {
            get {
                return this.enhetsnrOverlevertField;
            }
            set {
                this.enhetsnrOverlevertField = value;
            }
        }
        
        /// <remarks/>
        public string Mottaker {
            get {
                return this.mottakerField;
            }
            set {
                this.mottakerField = value;
            }
        }
        
        /// <remarks/>
        public string Posisjon {
            get {
                return this.posisjonField;
            }
            set {
                this.posisjonField = value;
            }
        }
        
        /// <remarks/>
        public string DatoTid {
            get {
                return this.datoTidField;
            }
            set {
                this.datoTidField = value;
            }
        }
        
        /// <remarks/>
        public string PostnrHendelse {
            get {
                return this.postnrHendelseField;
            }
            set {
                this.postnrHendelseField = value;
            }
        }
        
        /// <remarks/>
        public string Tiltak {
            get {
                return this.tiltakField;
            }
            set {
                this.tiltakField = value;
            }
        }
        
        /// <remarks/>
        public string Signatur {
            get {
                return this.signaturField;
            }
            set {
                this.signaturField = value;
            }
        }
        
        /// <remarks/>
        public string Bilde {
            get {
                return this.bildeField;
            }
            set {
                this.bildeField = value;
            }
        }
        
        /// <remarks/>
        public string Kommentar {
            get {
                return this.kommentarField;
            }
            set {
                this.kommentarField = value;
            }
        }
    }
}

﻿using System.Linq;
using System.Text;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using System.Collections.Generic;
using System;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using PreCom.Core.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using LoggingInformationType = Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading.LoggingInformationType;
using Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{

    public enum ScanningProcessStartedForm
    {
        FromLoadUnloadOperation,
        FromStopListOperation,
        FromOperationListOperation,
        FromLoadUnloadMenu,
        FromOtherProcess
    }


    public class CommandsOperations : BaseActionCommands
    {
        //TODO :: Write Unit Test
        public static OperationType GetStopTypeFromProcess(Process currentProcess)
        {
            switch (currentProcess)
            {
                case Process.Loading:
                    return OperationType.Loading;

                case Process.LoadDistribTruck:
                    return OperationType.LoadingDistributionTruck;

                case Process.LoadLineHaul:
                    return OperationType.LoadLineHaulTruck;

                case Process.Unloading:
                    return OperationType.Unloading;

                case Process.UnloadLineHaul:
                    return OperationType.UnloadLineHaulTruck;

                case Process.UnloadPickUpTruck:
                    return OperationType.UnloadPickUpTruck;

                case Process.LoadCombinationTrip:
                    return OperationType.LoadCombinationTrip;
            }
            return OperationType.Unknown;
        }


        public static bool GetStopListForRbt(BarcodeRouteCarrierTrip rbt, GetGoodsListRequestRequestType requestType, out T20240_GetGoodsListFromFOTReply responseFromLm, bool isForCombinationTrip, LoadCombinationTripType tripType)
        {
            var powerUnitId = rbt == null ? string.Empty : rbt.PowerUnitId;
            var routeId = rbt == null ? string.Empty : rbt.RouteId;
            var tripId = rbt == null ? string.Empty : rbt.TripId;

            var actionType = string.Empty;

            if (isForCombinationTrip)
            {
                actionType = tripType == LoadCombinationTripType.LoadLineHaul
                                 ? GetActionTypeFromProcess(Process.LoadLineHaul)
                                 : GetActionTypeFromProcess(Process.LoadDistribTruck);
            }

            return GetStopList(powerUnitId, requestType, out responseFromLm, routeId, tripId, actionType);
        }


        public static bool GetStopListForPowerUnit(string powerUnitId, GetGoodsListRequestRequestType requestType, out T20240_GetGoodsListFromFOTReply responseFromLm, string routeId)
        {
            return GetStopList(powerUnitId, requestType, out responseFromLm, routeId, string.Empty, string.Empty);
        }


        /// <summary>
        /// This function retrives the load list from LM/TM sets the response in out variable.
        /// </summary>
        /// <returns>Returns bool value specifiying that, load list retrived successfully or there was error in retriving the same.</returns>
        public static bool GetStopList(string powerUnitId, GetGoodsListRequestRequestType requestType, out T20240_GetGoodsListFromFOTReply responseFromLm,
            string routeId, string tripId, string actionType)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsLoadList.RequestLoadList()");


                //Send call to server for getting stop list
                var userProfile = ModelUser.UserProfile;

                Logger.LogAssert(userProfile.OrgUnitId != null);

                var request = new T20240_GetGoodsListFromFOTRequest
                {
                    RequestType = requestType,
                    TMSAffiliationId = userProfile.TmsAffiliation,
                    LocationInformation = new GetGoodsListRequestLocationInformation
                    {
                        NumberOfHours = 24, //TODO: Future configurable
                        OrgUnitId = userProfile.OrgUnitId,
                        PowerUnitId = powerUnitId,
                        CountryCode = userProfile.CountryCode,
                        PostalCode = userProfile.PostalCode,
                    },
                    LoggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationType>("Load list", "RequestLoadList", userProfile),
                };


                request.LocationInformation.ActionType = string.IsNullOrEmpty(actionType) ?
                    GetActionTypeFromProcess(BaseModule.CurrentFlow.CurrentProcess) : actionType;

                if (string.IsNullOrEmpty(routeId) == false)
                    request.LocationInformation.Route = routeId;
                if (string.IsNullOrEmpty(tripId) == false)
                    request.LocationInformation.TripId = tripId;

                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ValidateGoodsFromFot, powerUnitId);
                responseFromLm = CommunicationClient.Instance.Query<T20240_GetGoodsListFromFOTReply>(request, transaction);
                if (responseFromLm == null)
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.LoadListError, Severity.Error, GlobalTexts.Continue);
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsLoadList.RequestLoadList");
                responseFromLm = null;
                return true;
            }
            return false;
        }

        public static string GetActionTypeFromProcess(Process currentProcess)
        {
            switch (currentProcess)
            {
                case Process.LoadLineHaul:
                    return "LINJETUR";
                case Process.DeliveredOtherLoc:
                case Process.LoadDistribTruck:
                    return "DIS_LOK";
                case Process.UnloadLineHaul:
                    return "LOSS_LINJE";
                case Process.UnloadPickUpTruck:
                    return "LOSS_HENTEBIL";
                case Process.Pickup:
                    return "HENTEORDRE";
            }
            return "LINJETUR";
        }

        public static List<LocationElement> ProcessGoodsListReplyToGetLocations(T20240_GetGoodsListFromFOTReply goodsListReply, OperationType operationType)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.ProcessGoodsListReplyToGetLocations()");
            List<LocationElement> result = null;

            if (goodsListReply != null && goodsListReply.PlannedStop != null)
            {
                var plannedStops = goodsListReply.PlannedStop;
                result = GetListOfLocations(plannedStops, operationType);
            }

            return result;
        }

        /// <summary>
        /// This method will create List of objects for type Operation from List of objects for type GetGoodsListResponsePlannedStop
        /// </summary>
        public static List<Stop> ProcessGoodsListReplyToGetStops(T20240_GetGoodsListFromFOTReply goodsListReply, OperationType operationType)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.ProcessGoodsListReplyToGetStops()");
            List<Stop> result = null;

            if (goodsListReply != null && goodsListReply.PlannedStop != null)
            {
                var plannedStops = goodsListReply.PlannedStop;
                result = GetListOfStops(plannedStops, operationType);
            }

            return result;
        }

        /// <summary>
        /// This function takes array of planned stops and returns the list of LocationElement. 
        /// </summary>
        /// <returns>Returns list of LocationElement.</returns>
        //TODO :: Write Unit Test
        public static List<LocationElement> GetListOfLocations(GetGoodsListResponsePlannedStop[] plannedStops, OperationType operationType)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.GetListOfLocations()");

            List<LocationElement> listLoadingOperations = null;
            if (plannedStops != null && plannedStops.Any())
            {
                listLoadingOperations = new List<LocationElement>();
                foreach (GetGoodsListResponsePlannedStop stop in plannedStops)
                {
                    var locationElement = new LocationElement
                    {
                        Name = stop.locationName,
                        CountryCode = string.Empty,
                        Number = string.Empty,
                        PostalNumber = stop.locationPostalCode
                    };

                    listLoadingOperations.Add(locationElement);
                }
            }

            return listLoadingOperations;
        }

        /// <summary>
        /// This function takes array of planned stops and returns the list of loading operations. 
        /// </summary>
        /// <returns>Returns list of loading operations.</returns>
        //TODO :: Write Unit Test
        public static List<Stop> GetListOfStops(GetGoodsListResponsePlannedStop[] plannedStops, OperationType operationType)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.GetListOfStops()");

            List<Stop> listLoadingOperations = null;
            if (plannedStops != null && plannedStops.Any())
            {
                listLoadingOperations = new List<Stop>();
                foreach (GetGoodsListResponsePlannedStop stop in plannedStops)
                {
                    var loadingOperation = new Stop
                        {
                            StopId = stop.stopId,
                            Status = LoadingOperationStatus.New,
                            TripId = stop.tripId,
                            StopType = operationType,
                            OperationDetail =
                                String.Format("{0:t}", stop.earliestStartTime) + "-" +
                                String.Format("{0:t}", stop.latestEndTime) +
                                Environment.NewLine +
                                stop.locationAddress1 + (string.IsNullOrEmpty(stop.locationPostalCode) ? string.Empty : " , " + stop.locationPostalCode),
                            Location = stop.locationName,//Added by syed to enable scanning in US 9 on 10/14/13,
                            LoadCarrierId = stop.loadCarrierId,
                            PowerUnitId = stop.powerUnitId,
                            RouteId = stop.route
                        };
                    listLoadingOperations.Add(loadingOperation);
                }
            }

            return listLoadingOperations;
        }

        /// <summary>
        /// This function takes a status as string and returns corresponding operation type from enum LoadingOperationStatus.
        /// </summary>
        /// <param name="status">Specifies the status for which we need, required status.</param>
        /// <returns>Required status from enum LoadingOperationStatus.</returns>
        public static LoadingOperationStatus GetLoadingOperationStatus(string status)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.GetLoadingOperationStatus()");

            switch (status)
            {
                case StopOperationType.New:
                    return LoadingOperationStatus.New;
                case StopOperationType.Started:
                    return LoadingOperationStatus.Started;
                case StopOperationType.Finished:
                    return LoadingOperationStatus.Finished;
            }

            return LoadingOperationStatus.New;
        }

        /// <summary>
        /// This function checks that operations are returned from LM/TM or not. It checks the count of
        /// stops and returns true/false on the basis of that
        /// </summary>
        /// <returns></returns>
        public static bool IsTripsAndStopsReturned(List<Stop> operations)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.IsTripsAndStopsReturned()");
            return operations != null && operations.Count > 0;
        }


        /// <summary>
        /// This method does following
        /// Clears all scanning done in current session.
        /// </summary>
        public static void EmptyLocalCache(EntityMap entityMap)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.EmptyLocalCache()");

            if (entityMap != null)
            {
                entityMap.Clear();
            }
        }

        /// <summary>
        /// This method places the collection of consignments in, cache means ReconciledEntityMap.
        /// </summary>
        /// <param name="entityMap">Specifies the object of cache/ReconciledEntityMap.</param>
        /// <param name="plannedConsignments">Specifies the planned consignments that need to add in cache.</param>
        public static void PlacePlannedConsigmentsInCache(EntityMap entityMap, PlannedConsignmentsType[] plannedConsignments)
        {
            entityMap.StorePlannedConsignments(plannedConsignments);
        }

        /// <summary>
        /// This method will create load list either from current scanning in local cache or
        /// from load list info returned from LM/TM.
        /// </summary>
        //TODO :: Write Unit Test
        public static List<DeliveryOperation> CreateLoadListFromCache(EntityMap entityMap, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.CreateLoadList()");

            if (entityMap == null)
            {
                return null;
            }

            //Initialize new object of load list.
            var loadList = entityMap.Values
                .Select(consignment => GetDeliveryOperationFromConsignmentEntity(consignment, routeId))
                .Where(operation => operation != null && operation.Status != DeliveryStatus.Finished).ToList();
            return loadList;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="consigmentEntity"></param>
        /// <param name="routeId"> </param>
        /// <returns></returns>
        //TODO :: Write Unit Test
        public static DeliveryOperation GetDeliveryOperationFromConsignmentEntity(ConsignmentEntity consigmentEntity, string routeId)
        {
            if (consigmentEntity is LoadCarrier)
                return GetDeliveryOperationFromLoadCarrier(consigmentEntity.CastToLoadCarrier(), routeId);
            return GetDeliveryOperationFromConsignment(consigmentEntity.CastToConsignment(), routeId);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadCarrier"></param>
        /// <param name="routeId"></param>
        /// <returns></returns>
        //TODO :: Write Unit Test
        public static DeliveryOperation GetDeliveryOperationFromLoadCarrier(LoadCarrier loadCarrier, string routeId)
        {

            if (loadCarrier == null || loadCarrier.HasStatus(EntityStatus.Scanned) == false)
                return null;

            var plannedConsignment = new PlannedConsignmentsType
                                         {
                                             ConsignmentNumber = loadCarrier.LoadCarrierId
                                         };

            
                    
                var loadCarrierDetail= new GetGoodsListResponseOperationLoadCarrier
                {
                    loadCarrierId = loadCarrier.LoadCarrierId                 

                };

        
            var operationDetail = GetOperationDetailForLoadCarrier(loadCarrierDetail);

            // Defect 784 Fixes
            if (loadCarrier.LogicalLoadCarrierDetails.Count() > 0)
            {
                loadCarrierDetail.destinationName1 = loadCarrier.LogicalLoadCarrierDetails[0].DestinationName1;
                loadCarrierDetail.destinationName2 = loadCarrier.LogicalLoadCarrierDetails[0].DestinationName2;
                loadCarrierDetail.cDConsigneeId = loadCarrier.LogicalLoadCarrierDetails[0].RecipientId;
            }

               

            var deliveryOperation = new DeliveryOperation
            {
                RouteId = routeId,
                Consignment = plannedConsignment,
                LoadCarrier = loadCarrierDetail,
                OperationDetail = operationDetail,
                Status = DeliveryStatus.New,
                OperationType = DeliveryOperationType.LoadCarrier,
                PhysicalLoadCarrierId = ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId,
                LogicalLoadCarrierId = ModelMain.SelectedOperationProcess.LogicalLoadCarrierId,
                TripId = ModelMain.SelectedOperationProcess.TripId,
                ExternalTripId = ModelMain.SelectedOperationProcess.ExternalTripId,
                ScanTime = loadCarrier.EntityCreatedOn
            };

            return deliveryOperation;
        }


        /// <summary>
        /// This method marks the status of current delivery operation as finished.
        /// </summary>
        /// <param name="loadList">Specifies the current load list, in which to mark status of operation.</param>
        /// <param name="currentConsignment">Specifies the consignment in current delivery operation.</param>
        public static void MarkOperationStatusAsFinished(List<DeliveryOperation> loadList, PlannedConsignmentsType currentConsignment)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsLoadList.MarkOperationStatusAsFinished()");
            if (loadList == null || currentConsignment == null) return;
            foreach (var deliveryOperation in loadList)
            {
                if (deliveryOperation.Consignment.ConsignmentNumber.Equals(currentConsignment.ConsignmentNumber) && deliveryOperation.Status != DeliveryStatus.Finished)
                {
                    deliveryOperation.Status = DeliveryStatus.Finished;
                }
            }
        }

        /// <summary>
        /// This method marks the status of current delivery operation as finished.
        /// </summary>
        /// <param name="loadList">Specifies the current load list, in which to mark status of operation.</param>
        /// <param name="currentConsignment">Specifies the consignment in current delivery operation.</param>
        public static void MarkItemStatusAsFinished(List<DeliveryOperation> loadList, Consignment consignmentInCache)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsLoadList.MarkOperationStatusAsFinished()");

            if (loadList == null || consignmentInCache == null) return;
            //Iterate for all delivery operations in load list
            foreach (var deliveryOperation in loadList)
            {
                if (deliveryOperation.Consignment.ConsignmentNumber.Equals(consignmentInCache.ConsignmentId))
                {
                    if (deliveryOperation.Consignment.ConsignmentItem != null)
                    {
                    //Iterate for all the consignment items in delivery operation from load list
                    foreach (var consignmentItem in deliveryOperation.Consignment.ConsignmentItem)
                    {
                        //Get the same consignment
                        var ConsignmentItemInCache = consignmentInCache.ConsignmentItemsMap.Where(c => c.Key == consignmentItem.ConsignmentItemNumber).SingleOrDefault();

                        //If same consignment item does not exists or exists but not having status as scanned then return false
                        // Check for null pointer exception implemented Issue in delivery Defect 601 
                        if (ConsignmentItemInCache.Value!=null)
                        {
                        if (ConsignmentItemInCache.Value.HasStatus(EntityStatus.Scanned) == true)
                            consignmentItem.Status="Scanned";
                        }
                       }
                    }
                   
                }
            }
        }

        public static bool IsToCreateLoadList()
        {
            return ModelUser.UserProfile.Role == Role.Driver1 || ModelUser.UserProfile.Role == Role.Driver2 ||
                   ModelUser.UserProfile.Role == Role.Landpostbud;
        }



        /// <summary>
        /// This function retrives the details of consignment from object of Damage
        /// and creates object of type DeliveryOperation.
        /// </summary>
        /// <param name="consignment">Specifies the consignment for which need, object of DeliveryOperation.</param>
        /// <param name="routeId"> </param>
        /// <returns>Returns object for DeliveryOperation.</returns>
        //TODO :: Write Unit Test
        public static DeliveryOperation GetDeliveryOperationFromConsignment(Consignment consignment, string routeId)
        {
            var consignmentItems = new List<ConsignmentItemType>();


            if (consignment == null || consignment.ConsignmentItemsMap.Values == null || consignment.ConsignmentItemsMap.Values.Any(c => c.HasStatus(EntityStatus.Scanned)) == false)
                return null;


            consignmentItems.AddRange(consignment.ConsignmentItemsMap.Values.Select(consignmentItem => new ConsignmentItemType { ConsignmentItemNumber = consignmentItem.ItemId, Status="Pending" }));



            if (consignment.ConsigneeInfo == null && consignment.PlannedConsignment != null)
                consignment.ConsigneeInfo = new ConsigneeType
                {
                    DeliveryAddress1 = string.Empty,
                    DeliveryPostalCode = string.Empty,
                    DeliveryPostalName = string.Empty
                };
            else
            {
                if (consignment.ConsigneeInfo != null)
                {
                    if (string.IsNullOrEmpty(consignment.ConsigneeInfo.DeliveryAddress1))
                        consignment.ConsigneeInfo.DeliveryAddress1 = string.Empty;
                    if (string.IsNullOrEmpty(consignment.ConsigneeInfo.DeliveryPostalCode))
                        consignment.ConsigneeInfo.DeliveryPostalCode = string.Empty;
                    if (string.IsNullOrEmpty(consignment.ConsigneeInfo.DeliveryPostalName))
                        consignment.ConsigneeInfo.DeliveryPostalName = string.Empty;
                }
                else
                {
                    consignment.ConsigneeInfo = new ConsigneeType
                    {
                        DeliveryAddress1 = string.Empty,
                        DeliveryPostalCode = string.Empty,
                        DeliveryPostalName = string.Empty
                    };
                }
            }


            var plannedConsignment = new PlannedConsignmentsType
                {
                    ConsignmentNumber = consignment.ConsignmentId,
                    Consignee = consignment.ConsigneeInfo,
                    Consignor = consignment.ConsignorInfo,
                    ConsignmentItem = consignmentItems.ToArray(),
                };


            SetDeliveryAddress(plannedConsignment.Consignee);

            string operationDetail = plannedConsignment.GetConsigneeNameAndDetail(); //GetOperationDetailForConsignment(plannedConsignment);

            var deliveryOperation = new DeliveryOperation
                {
                    RouteId = routeId,
                    Consignment = plannedConsignment,
                    OperationDetail = operationDetail,
                    Status = DeliveryStatus.New,
                    OperationType = DeliveryOperationType.Consignment,
                    PhysicalLoadCarrierId = ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId,
                    LogicalLoadCarrierId = ModelMain.SelectedOperationProcess.LogicalLoadCarrierId,
                    TripId = ModelMain.SelectedOperationProcess.TripId,
                    ExternalTripId = ModelMain.SelectedOperationProcess.ExternalTripId,
                    ScanTime = consignment.EntityCreatedOn

                };

            return deliveryOperation;
        }


        public static void SetDeliveryAddress(ConsigneeType consigneeToConvert)
        {
            if (consigneeToConvert == null)
                return;
            //According to FOT - 139, If delivery address is null or delivery address is not null but having spaces then
            //set address1 as a delivery address
            if (string.IsNullOrEmpty(consigneeToConvert.DeliveryAddress1) == true || (string.IsNullOrEmpty(consigneeToConvert.DeliveryAddress1) == false && string.IsNullOrEmpty(consigneeToConvert.DeliveryAddress1.Trim()) == true))
            {
                consigneeToConvert.DeliveryAddress1 = consigneeToConvert.Address1;
            }

            if (string.IsNullOrEmpty(consigneeToConvert.DeliveryAddress2) == true || (string.IsNullOrEmpty(consigneeToConvert.DeliveryAddress2) == false && string.IsNullOrEmpty(consigneeToConvert.DeliveryAddress1.Trim()) == true))
            {
                consigneeToConvert.DeliveryAddress2 = consigneeToConvert.Address2;
            }

        }



        /// <summary>
        /// This function reads the locally stored XML file and retrieve, load list and stored the
        /// same locally in cache.
        /// </summary>
        /// <returns>Returns the load list having list of objects of type delivery operation.</returns>
        public static List<DeliveryOperation> GetLocallyStoredLoadList(string routeId)
        {
            string path = FileUtil.GetApplicationPath();
            string filePath = path + "\\xml\\LoadList.xml";

            if (!Directory.Exists(path + "\\xml") || !File.Exists(filePath))
            {
                return null;
            }

            using (var xmlReader = XmlReader.Create(filePath))
            {
                var xmlSerializer = new XmlSerializer(typeof(DeliveryOperation[]));
                var deliveryOperations = (DeliveryOperation[])xmlSerializer.Deserialize(xmlReader);


                if (string.IsNullOrEmpty(routeId))
                    return deliveryOperations.ToList();

                if (deliveryOperations != null && deliveryOperations.Length > 0 && deliveryOperations[0].RouteId == routeId)
                    return deliveryOperations.ToList();


                return null;
            }
        }

        /// <summary>
        /// This method deleted the existing LoadList.xml file and serialized the load list once again.
        /// </summary>
        /// <param name="loadList">Specifies the load list that need to serialize.</param>
        public static void SaveLoadListLocally(List<DeliveryOperation> loadList)
        {
            string path = FileUtil.GetApplicationPath();
            string filePath = path + "\\xml\\LoadList.xml";

            if (!Directory.Exists(path + "\\xml"))
                Directory.CreateDirectory(path + "\\xml");
            else if (File.Exists(filePath))
            {
                //If file already exists, then delete the same.
                File.Delete(filePath);
            }

            //Serialize the load list again
            using (var streamWriter = new StreamWriter(filePath))
            {
                var xmlSerializer = new XmlSerializer(typeof(DeliveryOperation[]));
                xmlSerializer.Serialize(streamWriter, loadList.ToArray());
            }
        }

        #region Helper Methods


        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadCarrier"></param>
        /// <returns></returns>
        public static string GetOperationDetailForLoadCarrier(GetGoodsListResponseOperationLoadCarrier loadCarrier)
        {
            var addressDetails = new StringBuilder();
            var operationDetails = new StringBuilder();

            if (string.IsNullOrEmpty(loadCarrier.destinationName1) && string.IsNullOrEmpty(loadCarrier.destinationName2))
            {
                operationDetails.Append(loadCarrier.loadCarrierId);
                return operationDetails.ToString();
            }

            if (!String.IsNullOrEmpty(loadCarrier.destinationName1))
            {
                addressDetails.Append(loadCarrier.destinationName1);
            }

            if (!String.IsNullOrEmpty(loadCarrier.destinationName2))
            {

                if (!String.IsNullOrEmpty(loadCarrier.destinationName1))
                {
                    addressDetails.Append(",");
                }

                addressDetails.Append(loadCarrier.destinationName2);
            }


            if (string.IsNullOrEmpty(addressDetails.ToString()))
            {
                if (!String.IsNullOrEmpty(loadCarrier.loadCarrierId))
                {
                    operationDetails.Append(loadCarrier.loadCarrierId);
                }
            }
            else
                operationDetails.Append(addressDetails);

            return addressDetails.ToString();
        }


       /* /// <summary>
        /// This method takes PlannedConsignmentType as parameter and returns details of operation
        /// by concatenating ConsignmentNumber and Consignee details.
        /// </summary>
        /// <param name="consignment">Specifies the consignment for which we need details.</param>
        /// <returns>Returns string having details of load list operation.</returns>
        public static string GetOperationDetailForConsignment(PlannedConsignmentsType consignment)
        {

            var addressDetails = new StringBuilder();
            var operationDetails = new StringBuilder();



            var consignee = consignment.Consignee;

            if (consignee == null)
            {
                if (!String.IsNullOrEmpty(consignment.ConsignmentNumber))
                {
                    operationDetails.Append(consignment.ConsignmentNumber);
                }
                return operationDetails.ToString();
            }


            if (!String.IsNullOrEmpty(consignment.Consignee.Name1))
            {
                addressDetails.Append(consignment.Consignee.Name1);
            }

            if (!String.IsNullOrEmpty(consignment.Consignee.DeliveryAddress1))
            {
                if (!String.IsNullOrEmpty(addressDetails.ToString()))
                {
                    addressDetails.Append(", ");
                }

                addressDetails.Append(consignment.Consignee.DeliveryAddress1);
            }

            if (!String.IsNullOrEmpty(consignment.Consignee.DeliveryPostalCode))
            {

                if (!String.IsNullOrEmpty(addressDetails.ToString()))
                {
                    addressDetails.Append(", ");
                }

                addressDetails.Append(consignment.Consignee.DeliveryPostalCode);

                if (!String.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
                {
                    addressDetails.Append(" " + consignment.Consignee.DeliveryPostalName);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(consignment.Consignee.DeliveryPostalName))
                {
                    if (!String.IsNullOrEmpty(addressDetails.ToString()))
                    {
                        addressDetails.Append(",");
                    }
                    addressDetails.Append(consignment.Consignee.DeliveryPostalName);
                }
            }

            if (string.IsNullOrEmpty(addressDetails.ToString()))
            {
                if (!String.IsNullOrEmpty(consignment.ConsignmentNumber))
                {
                    operationDetails.Append(consignment.ConsignmentNumber);
                }
            }
            else
                operationDetails.Append(addressDetails);


            return operationDetails.ToString();
        }*/
        #endregion


        /// <summary>
        /// This method checks that if, unfinished deliveries are there or not.
        /// </summary>
        /// <returns>Returns boolean value, which specifies that there are nfinished deliveries or not.</returns>
        public bool LoadListContainsUnfinishedDeliveries
        {
            get
            {
                var list = GetLocallyStoredLoadList(string.Empty);

                if (list != null)
                {
                    return list.Count(c => c.Status == DeliveryStatus.Started || c.Status == DeliveryStatus.New) > 0;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true if both trip-id and stop-id is known. This happens if comming from operation list
        /// </summary>
        /// <returns></returns>
        //TODO :: Write Unit Test
        public static bool IsTripAndStopKnown(OperationProcess operationProcess)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.IsTripAndStopKnown()");
            if (operationProcess == null) return false;
            return String.IsNullOrEmpty(operationProcess.StopId) == false || String.IsNullOrEmpty(operationProcess.TripId) == false;
        }


        /// <summary>
        /// This method marks the status of loading stop, as specified.
        /// </summary>
        /// <param name="selectedStopId">Specifies the id for stop which is to mark.</param>
        /// <param name="loadingStops">Specifies the list of stops, in which to mark status of stop.</param>
        /// <param name="status">Specifies the status to mark.</param>
        //TODO :: Write Unit Test
        public static void MarkStatusOfLoadingStop(string selectedStopId, List<Stop> loadingStops, LoadingOperationStatus status)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsLoading.MarkStatusOfLoadingStop()");
            if (loadingStops != null)
            {
                var selectedStop = loadingStops.FirstOrDefault(c => c.StopId.Equals(selectedStopId));
                if (selectedStop != null)
                    selectedStop.Status = status;
            }
        }


        #region "Related to load list"
        /// <summary>
        /// This function retrives the load list from LM/TM sets the response in out variable.
        /// </summary>
        /// <param name="powerUnit">Specifies the power unit id for which to retrieve the load list.</param>
        /// <param name="routeId">The route to get info for</param>
        /// <param name="responseFromLm">Specifies reply from LM.</param>
        /// <returns>Returns bool value specifiying that, load list retrived successfully or there was error in retriving the same.</returns>
        public static bool RequestLoadList(string powerUnit, string routeId, out T20240_GetGoodsListFromFOTReply responseFromLm)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsLoadList.RequestLoadList()");

                //Send call to server for getting stop list
                UserProfile userProfile = ModelUser.UserProfile;

                Logger.LogAssert(userProfile.OrgUnitId != null);

                var request = new T20240_GetGoodsListFromFOTRequest
                {
                    RequestType = GetGoodsListRequestRequestType.LoadList,
                    TMSAffiliationId = userProfile.TmsAffiliation,
                    LocationInformation = new GetGoodsListRequestLocationInformation
                    {
                        ActionType = "LA",
                        LoadCarrierId = powerUnit,
                        NumberOfHours = 24, //TODO: Future configurable
                        OrgUnitId = userProfile.OrgUnitId,
                        Route = routeId

                    },
                    LoggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationType>("Load list", "RequestLoadList", userProfile),
                };

                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ValidateGoodsFromFot, powerUnit);
                responseFromLm = CommunicationClient.Instance.Query<T20240_GetGoodsListFromFOTReply>(request, transaction);
                if (responseFromLm == null)
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.LoadListError, Severity.Error, GlobalTexts.Continue);
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsLoadList.RequestLoadList");
                responseFromLm = null;
                return true;
            }
            return false;
        }


        #region Modified new methods related to load list


        /// <summary>
        /// This method does following.
        /// 1) Retrieves locally stored load list.
        /// 2) If no local load list exist then it saves the new local list to local.
        /// 3) If local load list exists
        /// 4) Iterates through all operation in new load list and checks if operation already exists.
        /// 5) If operation already exists then it updates its details.
        /// 6) If operation does not exists then add the same, to local load list.
        /// 7) Save the local load list to local again
        /// </summary>
        /// <param name="newLoadList">Specifies the newly generated load list.</param>
        /// <param name="routeId"> </param>
        public static void AppendNewLoadListToLocalLoadList(List<DeliveryOperation> newLoadList, string routeId)
        {
            if (newLoadList == null)
            {
                return;
            }

            //Get existing load list from file.
            var existingLoadList = GetLocallyStoredLoadList(routeId);

            //Merge new and existing load list
            var mergedLoadList = MergeNewAndLocalLoadList(newLoadList, existingLoadList, routeId);

            //Save load list locally
            SaveLoadListLocally(mergedLoadList);
        }

        //TODO :: Write Unit Test
        private static List<DeliveryOperation> MergeNewAndLocalLoadList(IEnumerable<DeliveryOperation> newLoadList, List<DeliveryOperation> existingLoadList, string routeId)
        {
            var mergedLoadList = new List<DeliveryOperation>();
            //If local load list is available and current route id matches with local loadlist's route then
            //merge both load lists otherwise assign new load list to local load list.
            if (existingLoadList != null && existingLoadList.Count > 0 && existingLoadList[0].RouteId == routeId)
            {
                foreach (var operation in newLoadList)
                {
                    //Get existing operation with same consignment number
                    var existingOperation =
                    existingLoadList.FirstOrDefault(c => c.Consignment.ConsignmentNumber.Equals(operation.Consignment.ConsignmentNumber));

                    //if operation already exists
                    if (existingOperation != null)
                    {

                        //Operation with same consignment number already exists and it is already finished
                        //then remove that operation from existing load list and add new operation with same consignment number.
                        if (existingOperation.Status == DeliveryStatus.Finished)
                        {
                            existingLoadList.Remove(existingOperation);
                            existingLoadList.Add(operation);
                            continue;
                        }


                        UpdatedDeliveryOperation(operation, existingOperation);

                    }
                    else
                    {

                        var maxDefaultSortNo = existingLoadList[existingLoadList.Count - 1].DefaultSortNo;
                        operation.DefaultSortNo = maxDefaultSortNo + 1;
                        existingLoadList.Add(operation);
                    }
                }
            }
            else
            {
                existingLoadList = new List<DeliveryOperation>();
                existingLoadList.AddRange(newLoadList);
                var count = 1;
                foreach (var operation in existingLoadList)
                {
                    operation.DefaultSortNo = count;
                    operation.CurrentSortNo = count;
                    count = count + 1;
                }
            }
            mergedLoadList.AddRange(existingLoadList);
            return mergedLoadList;
        }


        /// <summary>
        /// This method updates the specified delivery operation, with values from, specified consignment.
        /// </summary>
        /// <param name="operation">Specifies consignment with which to update the deliveryOperation.</param>
        /// <param name="existingOperation">Specifies delivery operation which is to update.</param>
        //TODO :: Write Unit Test
        public static void UpdatedDeliveryOperation(DeliveryOperation operation, DeliveryOperation existingOperation)
        {
            Logger.LogEvent(Severity.Debug, "Executing method ActionCommandsLoadList.UpdatedDeliveryOperation()");

            if (existingOperation.OperationType == DeliveryOperationType.Consignment)
                UpdateDeliveryOperationOfTypeConsignment(operation, existingOperation);
            else
                UpdateDeliveryOperationOfTypeLoadCarrier(operation, existingOperation);

            existingOperation.Status = operation.Status;
        }

        //TODO :: Write Unit Test
        public static void UpdateDeliveryOperationOfTypeConsignment(DeliveryOperation operation, DeliveryOperation existingOperation)
        {
            //Iterate for all consignment items, to check which one already exists and which
            //one is missing.
            foreach (var consignmentItem in operation.Consignment.ConsignmentItem)
            {
                //Get existing consignment item
                var existingConsignmentItem =
                    existingOperation.Consignment.ConsignmentItem.SingleOrDefault(
                        c => c.ConsignmentItemNumber.Equals(consignmentItem.ConsignmentItemNumber));

                //If existing consignment item not found then add that consignment item in consignment.
                if (existingConsignmentItem == null)
                {
                    var consignmentItems = existingOperation.Consignment.ConsignmentItem.ToList();
                    consignmentItems.Add(consignmentItem);
                    existingOperation.Consignment.ConsignmentItem = consignmentItems.ToArray();
                }
            }

            //Set consignee details
            existingOperation.Consignment.Consignee = operation.Consignment.Consignee;

            //Replace operation detail.
            existingOperation.OperationDetail = operation.Consignment.GetConsigneeNameAndDetail();//GetOperationDetailForConsignment(operation.Consignment);

        }

        public static void UpdateDeliveryOperationOfTypeLoadCarrier(DeliveryOperation operation, DeliveryOperation existingOperation)
        {
            existingOperation.OperationDetail = GetOperationDetailForLoadCarrier(operation.LoadCarrier);
            existingOperation.LoadCarrier = operation.LoadCarrier;
        }



        /// <summary>
        /// This function takes planned consignments from LM and existing load list as parameter
        /// </summary>
        /// <param name="responseFromLm"> </param>
        /// <param name="existingLoadList">Specifies the existing consignments.</param>
        /// <param name="routeId"> </param>
        /// <returns>Returns load list in the form of list of consignments.</returns>
        public static void RefreshLoadList(T20240_GetGoodsListFromFOTReply responseFromLm, ref List<DeliveryOperation> existingLoadList, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsLoadList.RefreshLoadList()");
            List<DeliveryOperation> mergedLoadList = null;
            if (responseFromLm == null) return;

            //Get locally stored load list.
            existingLoadList = GetLocallyStoredLoadList(routeId);

            //If existing load list null, just get load list operations from 
            //planned consignments and put in exiting loat list
            if (existingLoadList == null)
            {
                mergedLoadList = GetListOfDeliveryOperationsFromLmResponse(responseFromLm, routeId);
                existingLoadList = new List<DeliveryOperation>();
                existingLoadList.AddRange(mergedLoadList);
                var count = 1;
                foreach (var operation in existingLoadList)
                {
                    operation.DefaultSortNo = count;
                    operation.CurrentSortNo = count;
                    count = count + 1;
                }
            }
            else
            {
                var newLoadList = GetListOfDeliveryOperationsFromLmResponse(responseFromLm, routeId);
                mergedLoadList = MergeNewAndLocalLoadList(newLoadList, existingLoadList, routeId);
                existingLoadList.Clear();
                existingLoadList.AddRange(mergedLoadList);
            }
            SaveLoadListLocally(mergedLoadList);
        }





        /// <summary>
        /// This function takes array of planned consignments as parameter and returns list of delivery operations.
        /// </summary>
        /// <param name="responseFromLm"> </param>
        /// <param name="routeId"> </param>
        /// <returns>Returns list for objects of type DeliveryOperation.</returns>
        //TODO :: Write Unit Test
        public static List<DeliveryOperation> GetListOfDeliveryOperationsFromLmResponse(T20240_GetGoodsListFromFOTReply responseFromLm, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing function ActionCommandsLoadList.GetListOfDeliveryOperationsFromConsignments()");
            if (responseFromLm.Operation == null || responseFromLm.Operation.consignment == null) return null;

            //Get list of delivery operations from consignments
            var operationsFromConsignments =
                GetDeliveryOperationsFromConsignmentsInLmResponse(responseFromLm.Operation.consignment, routeId);

            //Get list of delivery operations from load carriers
            var operationsFromCarriers = GetDeliveryOperationsFromLoadCarriersInLmResponse(responseFromLm.Operation.loadCarrier, routeId);

            var finalListOfOperations = new List<DeliveryOperation>();
            if (operationsFromConsignments != null)
                finalListOfOperations.AddRange(operationsFromConsignments);
            if (operationsFromCarriers != null)
                finalListOfOperations.AddRange(operationsFromCarriers);
            return finalListOfOperations;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="consignments"></param>
        /// <param name="routeId"></param>
        /// <returns></returns>
        //TODO :: Write Unit Test
        public static List<DeliveryOperation> GetDeliveryOperationsFromConsignmentsInLmResponse(GetGoodsListResponseOperationConsignment[] consignments, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing function ActionCommandsLoadList.GetListOfDeliveryOperationsFromConsignments()");
            if (consignments == null)
                return null;
            return consignments.Select(consignment => GetDeliveryOperationFromConsignmentInLmResponse(consignment, routeId)).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadCarriers"></param>
        /// <param name="routeId"></param>
        /// <returns></returns>
        public static List<DeliveryOperation> GetDeliveryOperationsFromLoadCarriersInLmResponse(GetGoodsListResponseOperationLoadCarrier[] loadCarriers, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing function ActionCommandsLoadList.GetListOfDeliveryOperationsFromConsignments()");

            if (loadCarriers == null)
                return null;


            return loadCarriers.Select(loadCarrier => GetDeliveryOperationFromLoadCarrierInLmResponse(loadCarrier, routeId)).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadCarrier"></param>
        /// <param name="routeId"></param>
        /// <returns></returns>
        //TODO :: Write Unit Test
        public static DeliveryOperation GetDeliveryOperationFromLoadCarrierInLmResponse(GetGoodsListResponseOperationLoadCarrier loadCarrier, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing function ActionCommandsLoadList.GetNewDeliveryOperationFromConsignment()");
            if (loadCarrier == null) return null;

            var operationDetail = GetOperationDetailForLoadCarrier(loadCarrier);
            var deliveryOperation = new DeliveryOperation
            {
                Consignment = new PlannedConsignmentsType { ConsignmentNumber = loadCarrier.loadCarrierId },
                LoadCarrier = loadCarrier,
                Status = DeliveryStatus.New,
                OperationType = DeliveryOperationType.LoadCarrier,
                OperationDetail = operationDetail,
                RouteId = routeId
            };
            return deliveryOperation;
        }


        /// <summary>
        /// This function takes planned consignment as parameter and returns object of delivery operation.
        /// </summary>
        /// <param name="consignment">Specifies the object planned consignment for which need DeliveryOperation.</param>
        /// <param name="routeId"> </param>
        /// <returns>Returns object of delivery operation.</returns>
        public static DeliveryOperation GetDeliveryOperationFromConsignmentInLmResponse(GetGoodsListResponseOperationConsignment consignment, string routeId)
        {
            Logger.LogEvent(Severity.Debug, "Executing function ActionCommandsLoadList.GetNewDeliveryOperationFromConsignment()");
            if (consignment == null) return null;

            //Get objects for planned consignment items
            var plannedConsignment = ConvertConsignmentFromLmResponseIntoPlannedConsignmentType(consignment);

            var operationDetail = plannedConsignment.GetConsigneeNameAndDetail(); //GetOperationDetailForConsignment(plannedConsignment);
            var deliveryOperation = new DeliveryOperation
            {
                Consignment = plannedConsignment,
                Status = DeliveryStatus.New,
                OperationType = DeliveryOperationType.Consignment,
                OperationDetail = operationDetail,
                RouteId = routeId
            };
            return deliveryOperation;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="consignment"></param>
        /// <returns></returns>
        //TODO:: Write unit test
        public static PlannedConsignmentsType ConvertConsignmentFromLmResponseIntoPlannedConsignmentType(GetGoodsListResponseOperationConsignment consignment)
        {
            var plannedConsignmentItems = new List<ConsignmentItemType>();
            if (consignment.consignmentItem != null)
            {
                plannedConsignmentItems.AddRange(consignment.consignmentItem.Select(consignmentItem => new ConsignmentItemType { ConsignmentItemNumber = consignmentItem.consignmentItemNumber }));
            }

            //Get object of planned consignment
            var plannedConsignment = new PlannedConsignmentsType
            {
                ConsignmentNumber = consignment.consignmentNumber,
                ConsignmentItem = plannedConsignmentItems.ToArray(),
                Consignee = Flows.Reconciliation.ActionCommandsReconciliation.ConvertConsignee(consignment.consignee)
            };

            if (plannedConsignment.Consignee != null) SetDeliveryAddress(plannedConsignment.Consignee);

            return plannedConsignment;
        }




        #endregion



        #endregion





    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public partial class CommandsOperationList : BaseActionCommands
    {
        public enum AmphoraSortType
        {
            Up,
            Down,
            Default,
            Reversed,
            ZipCode,
            InitialSort,

        }


        public static void UpdateOperationProcessFromOperationListOperation(PlannedOperationType operation, string operationId, string stopId)
        {
            if (operation != null)
            {
                //Pass values of PowerLoadCarrier and LoadCarrier from selected operation. Because when
                //user will performs any process on selected operation then PowerLoadCarrier will be treated as
                //Physical load carrier and Load carrier will be treated as Logical load Carrier.
                ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId = operation.PowerLoadCarrierId;
                ModelMain.SelectedOperationProcess.LogicalLoadCarrierId = operation.LoadCarrierId;
                ModelMain.SelectedOperationProcess.OperationId = operationId;
                ModelMain.SelectedOperationProcess.StopId = stopId;
                ModelMain.SelectedOperationProcess.TripId = operation.TripId;
                ModelMain.SelectedOperationProcess.ExternalTripId = operation.ExternalTripId;
                if (string.IsNullOrEmpty(operation.Route) == false)
                    ModelMain.SelectedOperationProcess.RouteId = operation.Route;
                ModelMain.SelectedOperationProcess.PowerUnitId = operation.PowerLoadCarrierId;
                ModelMain.SelectedOperationProcess.OperationOriginSystem = operation.OperationOrigin;
            }
        }

        public static void UpdateOperationProcessFromLoadListOperation(BindingStopAmphora operation)
        {
            if (operation != null)
            {
                //Pass values of PowerLoadCarrier and LoadCarrier from selected operation. Because when
                //user will performs any process on selected operation then PowerLoadCarrier will be treated as
                //Physical load carrier and Load carrier will be treated as Logical load Carrier.
                ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId = operation.PhysicalLoadCarrier;
                ModelMain.SelectedOperationProcess.LogicalLoadCarrierId = operation.LogicalLoadCarrier;
                ModelMain.SelectedOperationProcess.StopId = string.Empty;
                ModelMain.SelectedOperationProcess.OperationId = operation.OperationId;
                ModelMain.SelectedOperationProcess.TripId = operation.TripId;
                ModelMain.SelectedOperationProcess.ExternalTripId = operation.ExternalTripId;
                if (string.IsNullOrEmpty(operation.RouteId) == false)
                    ModelMain.SelectedOperationProcess.RouteId = operation.RouteId;
                else
                    ModelMain.SelectedOperationProcess.RouteId = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora &&
                                               ModelUser.UserProfile.Role == Role.Driver1
                                                   ? ModelUser.UserProfile.PowerUnit
                                                   : ModelUser.UserProfile.UserId;

                ModelMain.SelectedOperationProcess.PowerUnitId = ModelUser.UserProfile.TmsAffiliation ==
                                                                 TmsAffiliation.Amphora &&
                                                                 ModelUser.UserProfile.Role == Role.Driver1
                                                                     ? ModelUser.UserProfile.PowerUnit
                                                                     : string.Empty;

            }
        }

        public static void UpdateOperationProcessFromLoadListOperation(DeliveryOperation operation)
        {
            if (operation != null)
            {
                //Pass values of PowerLoadCarrier and LoadCarrier from selected operation. Because when
                //user will performs any process on selected operation then PowerLoadCarrier will be treated as
                //Physical load carrier and Load carrier will be treated as Logical load Carrier.
                ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId = operation.PhysicalLoadCarrierId;
                ModelMain.SelectedOperationProcess.LogicalLoadCarrierId = operation.LogicalLoadCarrierId;
                ModelMain.SelectedOperationProcess.StopId = string.Empty;
                ModelMain.SelectedOperationProcess.TripId = operation.TripId;
                ModelMain.SelectedOperationProcess.ExternalTripId = operation.ExternalTripId;
                if (string.IsNullOrEmpty(operation.RouteId) == false)
                    ModelMain.SelectedOperationProcess.RouteId = operation.RouteId;
                else
                    ModelMain.SelectedOperationProcess.RouteId = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora &&
                                               ModelUser.UserProfile.Role == Role.Driver1
                                                   ? ModelUser.UserProfile.PowerUnit
                                                   : ModelUser.UserProfile.UserId;

                ModelMain.SelectedOperationProcess.PowerUnitId = ModelUser.UserProfile.TmsAffiliation ==
                                                                 TmsAffiliation.Amphora &&
                                                                 ModelUser.UserProfile.Role == Role.Driver1
                                                                     ? ModelUser.UserProfile.PowerUnit
                                                                     : string.Empty;

            }
        }


        public static void UpdateOperationProcessFromLoadListOperation(Operation operation)
        {
            if (operation != null)
            {
                //Pass values of PowerLoadCarrier and LoadCarrier from selected operation. Because when
                //user will performs any process on selected operation then PowerLoadCarrier will be treated as
                //Physical load carrier and Load carrier will be treated as Logical load Carrier.
                ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId = operation.PhysicalLoadCarrierId;
                ModelMain.SelectedOperationProcess.LogicalLoadCarrierId = operation.LogicalLoadCarrierId;

                ModelMain.SelectedOperationProcess.TripId = operation.TripId;
                ModelMain.SelectedOperationProcess.ExternalTripId = operation.ExternalTripId;
                if (string.IsNullOrEmpty(operation.RouteId) == false)
                    ModelMain.SelectedOperationProcess.RouteId = operation.RouteId;
                else
                    ModelMain.SelectedOperationProcess.RouteId = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora &&
                                               ModelUser.UserProfile.Role == Role.Driver1
                                                   ? ModelUser.UserProfile.PowerUnit
                                                   : ModelUser.UserProfile.UserId;

                ModelMain.SelectedOperationProcess.PowerUnitId = ModelUser.UserProfile.TmsAffiliation ==
                                                                 TmsAffiliation.Amphora &&
                                                                 ModelUser.UserProfile.Role == Role.Driver1
                                                                     ? ModelUser.UserProfile.PowerUnit
                                                                     : string.Empty;

            }
        }

        public static void UpdateOperationProcessFromOperation(Operation operation)
        {

            //Set details in operation process from OL Operation
            if (operation.IsLoadListOperation == false)
            {
                // Retrieve the selected planned operations for stop
                var plannedOperation = ModelOperationList.OperationList.GetPlannedOperation(operation.StopId,
                                                                         operation.OperationId);

                UpdateOperationProcessFromOperationListOperation(plannedOperation, operation.OperationId, operation.StopId);
            }
            else //Set details in operation process from load list operation
            {
                UpdateOperationProcessFromLoadListOperation(operation);
            }
        }




        /// <summary>
        /// This function returns true/false on the basis of that, specified type of operation exists, in
        /// operation list or not.
        /// </summary>
        /// <param name="wantedOperationType">Type of operations to check for</param>
        /// <returns>Returns boolean value on the basis that, operations found or not.</returns>
        public static bool IsOperationListContainingOperationType(PlannedOperationTypeOperationType wantedOperationType)
        {
            var operationList = ModelOperationList.OperationList;

            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.IsOperationListStopContainingOperationType()");

            if (operationList != null && operationList.Stop != null)
            {
                //get count of loading operations
                foreach (OperationListStop operationListStop in operationList.Stop)
                {
                    foreach (PlannedOperationType plannedOperation in operationListStop.PlannedOperation)
                    {
                        if (CompareOperationType(wantedOperationType, plannedOperation.OperationType))
                        {
                            if ((plannedOperation.Status == StopOperationType.New || plannedOperation.Status == StopOperationType.Started))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Get details about current selected stop from current operationlist
        /// </summary>
        /// <returns></returns>
        public static OperationListStop GetStop(string stopId)
        {
            OperationListStop result = null;
            if (ModelOperationList.OperationList != null && String.IsNullOrEmpty(stopId) == false)
            {
                result = ModelOperationList.OperationList.GetStop(stopId);
            }
            return result;
        }

        /// <summary>
        /// This function returns true/false on the basis of that an unfinished operation exists or not.
        /// </summary>
        /// <returns>Returns boolean value on the basis that, unfinished operations exist found or not.</returns>
        public static bool IsOperationListContainingUnfinishedOperations()
        {
            if (ModelOperationList.OperationList != null)
            {

                var operationList = ModelOperationList.OperationList;

                Logger.LogEvent(Severity.Debug,
                                "Executing command CommandsOperationList.IsOperationListContainingUnfinishedOperations()");
                if (operationList != null && operationList.Stop != null)
                {
                    bool startedOperationsExists = operationList.Stop.Where(z => z != null).Count
                            (x => x.PlannedOperation.Count(y => y.Status.Equals(StopOperationType.Started) || y.Status.Equals(StopOperationType.New)) > 0) > 0;

                    return startedOperationsExists;
                }
            }

            return false;
        }

        /// <summary>
        /// For Frigo (TM_OTC) are all loading and unloading operations treated as same
        /// For every other TM the operations must be compared directly
        /// </summary>
        private static bool CompareOperationType(PlannedOperationTypeOperationType wantedOperationType, PlannedOperationTypeOperationType actualOperationType)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperations.CompareOperationType()");

            return wantedOperationType == actualOperationType;
        }


        /// <summary>
        /// This function takes operation list as parameter and returns list of loading operation from that.
        /// </summary>
        /// <returns>Returns list of loading operations from operation list.</returns>
        public static List<Operation> GetListOfOperations(PlannedOperationTypeOperationType plannedOperationType)
        {
            // So far supports only these three types
            Logger.LogAssert(plannedOperationType == PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL
                        || plannedOperationType == PlannedOperationTypeOperationType.LOAD
                      || plannedOperationType == PlannedOperationTypeOperationType.UNLOAD
                      || plannedOperationType == PlannedOperationTypeOperationType.UNLOAD_PICKUP
                      || plannedOperationType == PlannedOperationTypeOperationType.DELIVERY
                      || plannedOperationType == PlannedOperationTypeOperationType.LOAD_LINE_HAUL
                      || plannedOperationType == PlannedOperationTypeOperationType.LOAD_DISTRIBUTION
                      || plannedOperationType == PlannedOperationTypeOperationType.PICKUP);


            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.GetListOfOperations()");
            List<Operation> listLoadingOperations = null;

            if (ModelOperationList.OperationList != null && ModelOperationList.OperationList.Stop != null)
            {
                var operationList = ModelOperationList.OperationList;
                listLoadingOperations = new List<Operation>();

                foreach (var stop in operationList.Stop)
                {
                    if (stop.PlannedOperation == null || !stop.PlannedOperation.Any())
                    {
                        continue;
                    }

                    var operations = stop.PlannedOperation.Where(
                            c => (c.Status == StopOperationType.New || c.Status == StopOperationType.Started) &&
                                 CompareOperationType(plannedOperationType, c.OperationType)).ToList();

                    foreach (var operation in operations)
                    {
                        string operationDetail = String.Empty;
                        if (!String.IsNullOrEmpty(Convert.ToString(operation.EarliestStartTime)) &&
                            !String.IsNullOrEmpty(Convert.ToString(operation.LatestStartTime)))
                            operationDetail += String.Format("{0:t}", operation.EarliestStartTime) + "-" +
                                               String.Format("{0:t}", operation.LatestStartTime);


                        if (plannedOperationType == PlannedOperationTypeOperationType.PICKUP && stop.StopInformation != null) 
                            operationDetail += " " + stop.StopInformation.StopName;

                        if (stop.StopInformation != null && stop.StopInformation.Address != null)
                            operationDetail += Environment.NewLine +
                                               stop.StopInformation.Address.StreetName + "," + stop.StopInformation.Address.PostalCode + " " +
                                                        stop.StopInformation.Address.PostalName;


                        var loadingOperation = new Operation
                            {
                                OperationId = operation.OperationId,
                                StopId = stop.StopInformation == null ? string.Empty : stop.StopInformation.StopId,
                                Status = CommandsOperations.GetLoadingOperationStatus(operation.Status),
                                TripId = operation.TripId,
                                OperationType = Operation.ActiveOperationType,
                                OperationDetail = operationDetail,
                            };
                        // added order number and consignor detail if operation type is delivery....
                        if (plannedOperationType == PlannedOperationTypeOperationType.DELIVERY)
                        {
                            loadingOperation.OrderNumber = operation.GetOrderNumber();
                            loadingOperation.ConsignorName = operation.GetConsignorName();
                            loadingOperation.ConsignorFullAddress = operation.GetConsignorFullAddress();
                        }

                        listLoadingOperations.Add(loadingOperation);

                    }
                }
            }
            return listLoadingOperations;
        }



        /// <summary>
        /// Get details about current selected stop from current operationlist
        /// </summary>
        /// <returns></returns>
        public static PlannedOperationType GetPlannedOperation(string stopId, string operationId)
        {
            Logger.LogEvent(Severity.Debug, "Retrieveing planned operation");
            Logger.LogAssert(ModelOperationList.OperationList != null);

            PlannedOperationType result = null;
            if (ModelOperationList.OperationList != null)
            {
                var operationListStop = ModelOperationList.OperationList.GetStop(stopId);
                if (operationListStop != null && operationListStop.PlannedOperation != null)
                    result = operationListStop.PlannedOperation.SingleOrDefault(c => c.OperationId.Equals(operationId));
            }
            return result;
        }


        /// <summary>
        /// Get PlannedConsignment details from current selected stop and selected planned operation
        /// </summary>
        /// <returns></returns>
        public static PlannedConsignmentsType GetPlannedConsignmentFromPlannedOperation(string consignmentId, string stopId, string operationId)
        {
            PlannedOperationType plannedOperation = null;
            if (ModelOperationList.OperationList != null)
            {
                plannedOperation = GetPlannedOperation(stopId, operationId);
            }
            PlannedConsignmentsType plannedConsignmentsType = null;

            if (plannedOperation != null)
            {
                plannedConsignmentsType =
               plannedOperation.PlannedConsignments.FirstOrDefault(c => c.ConsignmentNumber.Equals(consignmentId));
            }
            return plannedConsignmentsType;
        }


        /// <summary>
        /// Get details about current selected stop from Received operationlis 
        /// </summary>
        /// <returns></returns>
        public static StopType GetStopInformation(string stopId, OperationListEx operationList)
        {
            var stop = operationList != null ? operationList.GetStop(stopId) : null;
            return stop != null ? stop.StopInformation : null;
        }

        /// <summary>
        /// Get the type of stop from stop id
        /// </summary>
        /// <returns>Type of stop </returns>
        public static StopType GetStopType(String stopId)
        {
            StopType stopType = null;
            if (ModelOperationList.OperationList != null)
            {
                var stop = ModelOperationList.OperationList.GetStop(stopId);
                stopType = stop != null ? stop.StopInformation : null;
            }
            return stopType;
        }

        /// <summary>
        /// Retrieve all planned consignments for an operation at stop
        /// </summary>
        public static PlannedConsignmentsType[] GetPlannedConsignmentsForOperation(OperationProcess operationProcess)
        {
            PlannedConsignmentsType[] result = null;
            Logger.LogEvent(Severity.Debug, "GetPlannedConsignmentsForOperation");

            try
            {
                var operationList = ModelOperationList.OperationList;

                if (operationList != null && operationProcess != null)
                {
                    // Retrieve the operationlist for the current stop
                    var operationListForStop = operationList.GetStop(operationProcess.StopId);
                    if (operationListForStop != null)
                    {
                        // Retrieve the selected planned operations for stop
                        var plannedOperation = operationList.GetPlannedOperation(operationProcess.StopId, operationProcess.OperationId);

                        if (plannedOperation != null)
                        {
                            result = plannedOperation.PlannedConsignments;
                        }
                        else
                        {
                            Logger.LogEvent(Severity.Error, "Could not retrieve selected consignments");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsOperationList.GetPlannedConsignmentsForOperation");
            }

            return result;
        }

        public static string GetOperationListId()
        {
            return ModelOperationList.OperationList != null ? ModelOperationList.OperationList.OperationListId : String.Empty;
        }

        /// <summary>
        /// method used for bind the advance list control with stops on the basis of tab selected and apply filter 
        /// </summary>
        public static void GetStopsInfo()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.GetStopsInfo()");
            Logger.LogEvent(Severity.Debug, "GetStopsInfo() started at " + DateTime.Now.ToLongTimeString());

            try
            {
                //SplitPickUpOperations();
                ModelOperationList.ClientOperationStops = null;
                //add load list operation
                MergerLoadListOperation();
                var operationList = ModelOperationList.OperationList;

                if (operationList != null && operationList.Stop != null && operationList.Stop.Any())
                {
                    IList<BindingStopAmphora> listClientOperationStops;
                    List<PlannedOperationType> listPlannedOperations;
                    var arrayOperationListStop = operationList.Stop;

                    switch (ModelOperationList.SelectedOperationListTab)
                    {
                        case OperationListTab.NotStarted:
                            listClientOperationStops = new List<BindingStopAmphora>();
                            foreach (var operationListStop in arrayOperationListStop)
                            {
                                if (operationListStop == null || operationListStop.PlannedOperation == null)
                                {
                                    continue;
                                }
                                listPlannedOperations = GetListOfPlannedOperation(operationListStop, true);

                                if (listPlannedOperations != null &&
                                    listPlannedOperations.Count() != operationListStop.PlannedOperation.Count())
                                {
                                    listPlannedOperations = GetListOfPlannedOperation(operationListStop, false);
                                    AddStopInfo(listClientOperationStops, operationListStop, listPlannedOperations);
                                }

                            }
                            break;
                        case OperationListTab.Completed:
                            listClientOperationStops = new List<BindingStopAmphora>();
                            foreach (var operationListStop in arrayOperationListStop)
                            {
                                if (operationListStop == null || operationListStop.PlannedOperation == null)
                                {
                                    continue;
                                }
                                listPlannedOperations = GetListOfPlannedOperation(operationListStop, true);

                                if (listPlannedOperations != null &&
                                    listPlannedOperations.Count() == operationListStop.PlannedOperation.Count())
                                {
                                    AddStopInfo(listClientOperationStops, operationListStop, listPlannedOperations);
                                }
                            }
                            break;
                        case OperationListTab.All:
                            listClientOperationStops = new List<BindingStopAmphora>();
                            foreach (var operationListStop in arrayOperationListStop)
                            {
                                if (operationListStop == null || operationListStop.PlannedOperation == null)
                                {
                                    continue;
                                }
                                listPlannedOperations = GetListOfPlannedOperation(operationListStop, false);

                                if (listPlannedOperations.Count > 0)
                                {
                                    AddStopInfo(listClientOperationStops, operationListStop, listPlannedOperations);
                                }
                            }
                            break;
                        default:
                            listClientOperationStops = new List<BindingStopAmphora>();
                            foreach (var operationListStop in arrayOperationListStop)
                            {
                                if (operationListStop == null || operationListStop.PlannedOperation == null)
                                {
                                    continue;
                                }
                                listPlannedOperations = GetListOfPlannedOperation(operationListStop, true);

                                if (listPlannedOperations != null &&
                                    listPlannedOperations.Count() != operationListStop.PlannedOperation.Count())
                                {
                                    listPlannedOperations = GetListOfPlannedOperation(operationListStop, false);
                                    AddStopInfo(listClientOperationStops, operationListStop, listPlannedOperations);
                                }
                            }
                            break;
                    }


                    //operationListStop.StopInformation.OriginalSortNo
                    if (listClientOperationStops.Any())
                    {
                        int maxSortNumber = listClientOperationStops.Max(n => n.OrignalSortOrder);
                        if (maxSortNumber == 0)
                        {
                            listClientOperationStops =
                                listClientOperationStops.OrderBy(n => n.StopEarliestTime).ToList();
                        }
                        if (ModelOperationList.ClientOperationStops == null)
                            ModelOperationList.ClientOperationStops = new List<BindingStopAmphora>();
                        listClientOperationStops = (from c in listClientOperationStops orderby c.OrignalSortOrder select c).ToList();
                        foreach (var operationListStop in listClientOperationStops)
                            ModelOperationList.ClientOperationStops.Add(operationListStop);
                    }
                    //ModelOperationList.ClientOperationStops = listClientOperationStops;
                }

                ModelOperationList.ClientOperationStops = CreateSortedOperationList(AmphoraSortType.InitialSort);
                Logger.LogEvent(Severity.Debug, "GetStopsInfo() ended at " + DateTime.Now.ToLongTimeString());


            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionOperationListCommands.GetStopsInfo");
                throw;
            }
        }


        /// <summary>
        /// method for merging loadlist operation with operation list operation......
        /// </summary>
        public static void MergerLoadListOperation()
        {
            var operations = GetDeliveryOperationOnTabBase(ModelOperationList.SelectedOperationListTab);
            if (operations != null && operations.Any())
            {
                int maxSortNumber = operations.Max(n => n.AmphoraStopSequence);
                if (maxSortNumber == 0)
                    operations = operations.OrderByDescending(n => n.ScanTime).ToList();

                foreach (var deliveryOperation in operations)
                {
                    if (deliveryOperation != null && deliveryOperation.Consignment != null)
                    {
                        var stop = new BindingStopAmphora
                                       {
                                           StopId = deliveryOperation.Consignment.ConsignmentNumber,
                                           IsLoadListOperation = true,
                                           StopAddress = deliveryOperation.Consignment.GetConsigneeNameAndDetail(),
                                           StopStatus = LoadListStopStatus(deliveryOperation.Status),
                                           OperationId = deliveryOperation.Consignment.ConsignmentNumber,
                                           StopSign = Convert.ToString(OperationCommands.GetOperationType(PlannedOperationTypeOperationType.DELIVERY)),
                                           AlternateStopSign = Convert.ToString(OperationCommands.GetOperationType(PlannedOperationTypeOperationType.DELIVERY)),
                                           LoadListPlannedConsignment = deliveryOperation.Consignment,
                                           OrignalSortOrder = deliveryOperation.AmphoraStopSequence,
                                           DeliveryDefaultSortOrder = deliveryOperation.DefaultSortNo,
                                           Consignee = deliveryOperation.Consignment.Consignee,
                                           StopEarliestTime = deliveryOperation.ScanTime,
                                           RouteId = deliveryOperation.RouteId,
                                           PhysicalLoadCarrier = deliveryOperation.PhysicalLoadCarrierId,
                                           TripId = deliveryOperation.TripId,
                                           ExternalTripId = deliveryOperation.ExternalTripId,
                                           LogicalLoadCarrier = deliveryOperation.LogicalLoadCarrierId

                                       };

                        if (ModelOperationList.ClientOperationStops == null)
                            ModelOperationList.ClientOperationStops = new List<BindingStopAmphora>();

                        ModelOperationList.ClientOperationStops.Add(stop);
                    }
                }
            }
        }

        #region "Methods for using creating custom operation for treeview binding"

        /// <summary>
        /// method for sorting custom operation list from cobination of both amphora operations and deliveries operations
        /// </summary>
        public static IList<BindingStopAmphora> CreateSortedOperationList(AmphoraSortType sortType)
        {
            IList<BindingStopAmphora> list = null;
            if (ModelOperationList.ClientOperationStops != null)
            {
                list = ModelOperationList.ClientOperationStops;
                if (list != null && list.Any())
                {
                    List<BindingStopAmphora> sortedlist = null;
                    int maxStopSortNumber = 0;
                    IList<BindingStopAmphora> deliveryStops = list.Where(n => n.IsLoadListOperation).ToList();//.OrderByDescending(n => n.DeliveryDefaultSortOrder).ToList();
                    IList<BindingStopAmphora> operationListStops = list.Where(n => n.IsLoadListOperation == false).ToList();
                    switch (sortType)
                    {
                        case AmphoraSortType.InitialSort:

                            int maxSortDeliveryNumber = deliveryStops.Any() ? deliveryStops.Max(c => c.OrignalSortOrder) : 0;

                            maxStopSortNumber = maxSortDeliveryNumber == 0 ? 0 : list.Max(c => c.OrignalSortOrder);
                            foreach (var sortStop in list)
                            {

                                if (maxSortDeliveryNumber == 0)
                                {
                                    maxStopSortNumber++;
                                    sortStop.OrignalSortOrder = maxStopSortNumber;
                                }
                                else if (sortStop.OrignalSortOrder == 0)
                                {
                                    maxStopSortNumber++;
                                    sortStop.OrignalSortOrder = maxStopSortNumber;
                                }
                            }


                            /*int maxSortNumberOperationlist = 0;
                            if (operationListStops.Any())
                            {
                                maxSortNumberOperationlist = operationListStops.Max(c => c.OrignalSortOrder);
                            }
                            maxStopSortNumber = list.Max(c => c.OrignalSortOrder);
                            int minDeliverytStopSortNumber = deliveryStops.Any() ? deliveryStops.Min(c => c.OrignalSortOrder) : 0;

                            foreach (var sortStop in list)
                            {
                                if (sortStop.OrignalSortOrder == 0)
                                {
                                    maxStopSortNumber++;
                                    sortStop.OrignalSortOrder = maxStopSortNumber;
                                }
                                else if (sortStop.IsLoadListOperation && maxSortNumberOperationlist != 0 && minDeliverytStopSortNumber == 1)
                                {
                                    maxStopSortNumber++;
                                    sortStop.OrignalSortOrder = maxStopSortNumber;
                                }
                            }*/
                            break;

                        case AmphoraSortType.Default:

                            deliveryStops = deliveryStops.OrderByDescending(n => n.StopEarliestTime).ToList();
                            operationListStops = operationListStops.OrderBy(n => n.StopEarliestTime).ToList();

                            sortedlist = deliveryStops.ToList();
                            sortedlist.AddRange(operationListStops);

                            list = sortedlist;

                            //list = (from c in list orderby c.StopEarliestTime select c).ToList();
                            foreach (var sortStop in list)
                            {
                                maxStopSortNumber++;
                                sortStop.OrignalSortOrder = maxStopSortNumber;
                            }

                            /*if (operationListStops.Any())
                                maxStopSortNumber = operationListStops.Max(c => c.OrignalSortOrder);

                            foreach (var sortStop in operationListStops)
                            {
                                if (sortStop.OrignalSortOrder == 0)
                                {
                                    maxStopSortNumber++;
                                    sortStop.OrignalSortOrder = maxStopSortNumber;
                                }
                            }
                            foreach (var sortStop in deliveryStops)
                            {
                                maxStopSortNumber++;
                                sortStop.OrignalSortOrder = maxStopSortNumber;
                            }*/
                            break;

                        case AmphoraSortType.Reversed:
                            /*foreach (var sortStop in operationListStops)
                            {
                                if (sortStop.OrignalSortOrder == 0)
                                {
                                    maxStopSortNumber++;
                                    sortStop.OrignalSortOrder = maxStopSortNumber;
                                }
                            }

                            foreach (var sortStop in deliveryStops)
                            {
                                maxStopSortNumber++;
                                sortStop.OrignalSortOrder = maxStopSortNumber;
                            }*/

                            deliveryStops = deliveryStops.OrderBy(n => n.StopEarliestTime).ToList();
                            //operationListStops = operationListStops.OrderByDescending(n => n.StopEarliestTime).ToList();

                            sortedlist = deliveryStops.ToList();
                            sortedlist.AddRange(operationListStops);

                            list = sortedlist;

                            //list = (from c in list orderby c.StopEarliestTime descending select c).ToList();
                            foreach (var sortStop in list)
                            {
                                maxStopSortNumber++;
                                sortStop.OrignalSortOrder = maxStopSortNumber;
                            }
                            break;

                        case AmphoraSortType.ZipCode:
                            var sortedDeliveryStops = (from c in deliveryStops
                                                       let consigneeType = c.Consignee
                                                       where consigneeType != null
                                                       orderby consigneeType.PostalCode, consigneeType.Address1,
                                                           consigneeType.Name1
                                                       select c).ToList();
                            var deliveryWithOutSorted = deliveryStops.Except(sortedDeliveryStops).ToList();
                            sortedDeliveryStops.AddRange(deliveryWithOutSorted);



                            var sortedOperationListStops = (from c in operationListStops
                                                            let stopType = c.StopType
                                                            where stopType != null
                                                            let addressType = stopType.Address
                                                            orderby addressType != null ? addressType.PostalCode : "", addressType != null ? addressType.StreetName : "", stopType.StopName
                                                            select c).ToList();
                            var operationListStopsWithOutSorted = operationListStops.Except(sortedOperationListStops).ToList();
                            sortedOperationListStops.AddRange(operationListStopsWithOutSorted);

                            sortedDeliveryStops.AddRange(sortedOperationListStops);

                            list = sortedDeliveryStops;

                            foreach (var sortStop in list)
                            {
                                maxStopSortNumber++;
                                sortStop.OrignalSortOrder = maxStopSortNumber;
                            }
                            break;
                    }
                    list = (from c in list orderby c.OrignalSortOrder select c).ToList();
                    int iCount = 1;
                    foreach (var sortStop in list)
                    {
                        sortStop.DisplaySortOrder = iCount;
                        iCount += 1;
                    }
                }
            }
            return list;
        }
        #endregion
        public static List<DeliveryOperation> GetDeliveryOperationOnTabBase(OperationListTab operationListTab)
        {
            List<DeliveryOperation> operations = null;
            List<DeliveryOperation> operationsFromLoadlist = CommandsOperations.GetLocallyStoredLoadList(string.Empty);
            if (operationsFromLoadlist != null)
            {
                switch (operationListTab)
                {
                    case OperationListTab.Completed:
                        operations = (operationsFromLoadlist.Where(
                            n => n.Status == DeliveryStatus.Finished)).ToList();
                        break;
                    case OperationListTab.NotStarted:
                        operations = (operationsFromLoadlist.Where(
                            n => n.Status != DeliveryStatus.Finished)).ToList();
                        break;
                    default:
                        operations = operationsFromLoadlist;
                        break;
                }
            }
            return operations;
        }

        /// <summary>
        /// This method returns the Capture equipment id for current operation list.
        /// </summary>
        /// <returns></returns>
        public static string GetCapturedEquipmentId()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.GetCapturedEquipmentId()");

            return ModelOperationList.OperationList != null ? ModelOperationList.OperationList.CaptureEquipmentId : string.Empty;
        }

        /// <summary>
        /// This method, return bool value saying that operation list exists or not.
        /// </summary>
        /// <returns></returns>
        public static bool IsOperationListAvailable()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.IsOperationListAvailable()");

            return ModelOperationList.OperationList != null;
        }

        /// <summary>
        /// This method marks the status of Specified operation in specified stop with specified status.
        /// </summary>
        /// <param name="operationProcess"></param>
        /// <param name="status">Specifies the status which is to mark.</param>
        public static void MarkSelectedOperationStatus(OperationProcess operationProcess, string status)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.MarkSelectedOperationStatus()");

            if (ModelOperationList.OperationList != null && ModelOperationList.OperationList.Stop != null &&
                String.IsNullOrEmpty(status) == false)
            {
                var stop = GetStop(operationProcess.StopId);
                if (stop != null)
                {
                    var operation = stop.PlannedOperation.SingleOrDefault(d => d.OperationId == operationProcess.OperationId);
                    if (operation != null)
                    {
                        operation.Status = status;
                        if (ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra)
                            stop.Status = status;
                    }
                }
            }
        }

        /// <summary>
        /// This function returns true or false, on the basis of that, if operation of particular type
        /// available with status started.
        /// </summary>
        /// <param name="stopId">Specifies the stop id, for which to check.</param>
        /// <param name="operationType">Specifies the type of operation to check for.</param>
        /// <param name="status">Specifies the status that need to check.</param>
        /// <returns>Returns bool status for the availability of operations.</returns>
        public static bool IsOperationsOfTypePendingOnStop(string stopId, PlannedOperationTypeOperationType operationType, string status)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.IsOperationsOfTypePendingOnStop()");
            var stop = (from c in ModelOperationList.OperationList.Stop
                        where c.StopInformation.StopId.Equals(stopId)
                        select c).FirstOrDefault();

            if (stop == null || stop.PlannedOperation == null || String.IsNullOrEmpty(status))
            {
                return false;
            }

            int operationCount = stop.PlannedOperation.Count(d => d.OperationType == operationType && !String.IsNullOrEmpty(d.Status) && (status.Contains(d.Status)));

            return operationCount > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void PersistOperationListChanges()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.PersistOperationListChanges()");
                if (ModelOperationList.OperationList != null)
                {
                    string path = FileUtil.GetApplicationPath();

                    if (!Directory.Exists(path + "\\xml"))
                    {
                        Directory.CreateDirectory(path + "\\xml");
                    }

                    string filePath = path + "\\xml\\OperationList.xml";
                    //If file already exists, then delete it.
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    //Serialize the operation list again
                    Serializer.SerializeFile<OperationList>(filePath, (OperationList)ModelOperationList.OperationList);
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "CommandsOperationList.PersistOperationListChanges");
            }

        }

        public static OperationListEx RetrieveLocalOperationList(string filePath)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing command CommandsOperationList.RetrieveLocalOperationList()");
                var innerList = Serializer.DeserializeFile<OperationList>(filePath);
                if (innerList != null)
                {
                    var operationList = new OperationListEx(innerList);
                    //Add missing stop id for amphora
                    if (ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora)
                        operationList.AddMissingStopIdsForAmphora();
                    return operationList;
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OperationListHandlerAlystra.RetrieveLocalOperationList");
            }
            return null;
        }

        /// <summary>
        /// method for clear  operation and load ,work list file and object while loggin and logoff
        /// </summary>
        public static void ClearOperations()
        {
            ClearLoadList();
            ClearOperationList();
        }

        /// <summary>
        /// Deletes Load List 
        /// </summary>
        public static void ClearLoadList()
        {
            try
            {
                var path = FileUtil.GetApplicationPath();
                //Delete LoadList.xml
                if (Directory.Exists(path + "\\xml") && File.Exists(path + "\\xml\\LoadList.xml"))
                    File.Delete(path + "\\xml\\LoadList.xml");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "CommandsOperationList.ClearLoadList");
            }


            ModelMain.WorkListItems = null;
        }

        /// <summary>
        /// Deletes Operation List
        /// </summary>
        public static void ClearOperationList()
        {
            try
            {
                var path = FileUtil.GetApplicationPath();
                //Delete OperationList.xml
                if (File.Exists(path + "\\xml\\OperationList.xml"))
                    File.Delete(path + "\\xml\\OperationList.xml");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "CommandsOperationList.ClearOperationList");
            }


            ModelOperationList.OperationList = null;
            ModelOperationList.NewStops.Clear();
        }

        /// <summary>
        /// This method iterates for operations in all stops that are the part of
        /// NewStops collection, and checks if any is pending without having status
        /// either accept or reject. If found then returns false otherwise returns true.
        /// </summary>
        /// <returns>Specifies the boolean result.</returns>
        public static bool AreAllStopsInUpdateProcessed(OperationListEx operationList)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: AreAllStopsInUpdateProcessed() method executed");
            bool isCompleted = true;

            if (operationList != null)
            {
                //Get all stops that user was supposed to accept or reject
                var stops = (from c in operationList.Stop
                             where ModelOperationList.NewStops.Contains(c.StopInformation.StopId)
                             select c).ToList<OperationListStop>();

                foreach (var stop in stops)
                {
                    //get count of pending operation in stop
                    var exists = stop.PlannedOperation.Any(
                        c => c.Status != StopOperationType.Accepted && c.Status != StopOperationType.Rejected && c.Status != StopOperationType.Finished && c.Status != StopOperationType.Started);
                    if (exists)
                    {
                        isCompleted = false;
                    }
                }
            }

            return isCompleted;
        }
    }
}
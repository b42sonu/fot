﻿using System;
using System.Linq;
using System.Threading;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Views;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using PreCom.Controls;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListResponse;
using PreCom.Core.Communication;


namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class PushMessageCommands : BaseCommands
    {
        public static CommunicationModel Model { get { return CommunicationModel.Instance; } }

        #region Action Commands

        /// <summary>
        /// This method will be called every time when user will have updated operation list,
        /// through push messages.
        /// </summary>
        /// <returns></returns>
        public static bool OperationListReceived()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: OperationListReceived command executed");
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: Received xml is " + CommunicationModel.Instance.CurrentPushMessage);
           
            CommunicationModel.Instance.UpdatedOperationList = GetOperationListFromXml(CommunicationModel.Instance.CurrentPushMessage);
            if (CommunicationModel.Instance.UpdatedOperationList != null)
            {
                ////If session id is null, it means user is not logged in, so just send the response as
                ////rejected for all operations and stop.
                //if (string.IsNullOrEmpty(TMI_Settings.Instance.UserProfileSessionId))
                //{
                //    //Send operation response to server
                //    SendOperationListResponse(true);
                //    return false;
                //}
                //else //Receive new or updated operation list
                //{

                if (CommunicationModel.Instance.IsLogon)
                {
                    //if (BaseModule.ActiveModule == null &&
                    //    CommunicationModel.Instance.CurrentPushMessageHandler.Name == "OperationList")
                    //{
                    //    Logger.LogEvent(Severity.Debug,
                    //                           "PushMessageCommands: No module was active, so activating operation list module for showing updated operation list");
                    //    CommunicationModel.Instance.CurrentPushMessageHandler.ActivateModule();
                    //}

                    BaseModule.AllowToDeactivate = false;

                    //If operation list does not exists, it means its new one
                    if (!CommunicationModel.Instance.OperationListExists)
                    {
                        Logger.LogEvent(Severity.Debug, "Received new operation list in method OperationListReceived()");
                        var status = ReceiveNewOperationList();
                        if (!status) return false;
                    }
                    else
                    {
                        Logger.LogEvent(Severity.Debug, "Received updated operation list in method OperationListReceived()");
                        var status = ReceiveUpdatedOperationList();
                        if (!status) return false;
                    }
                }
                // }
            }
            else
            {
                return false;
            }
            return true;
        }




        /// <summary>
        /// This method shows pop up for driver availability response.
        /// </summary>
        /// <returns>Returns true if every thing is fine.</returns>
        public static bool DriverAvailabilityResponseReceived()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: DriverAvailabilityResponseReceived command executed");
            var driverArgs = new ModalMessageBoxArgument("Driver Availability", "You are no longer associated to the current Power Unit in the TMS. You will not get updated planning information for the Power Unit", new ModalMessageBoxButton("OK"));
            BaseModule.ShowModelPopUp(driverArgs);
            return true;
        }

        /// <summary>
        /// Every time Accept, Reject or Accept All buttons will be clicked on new or updated operation
        /// list screen, this command will execute. This command is common for three types of commands
        /// Accept, Reject or Accept All.
        /// </summary>
        /// <param name="commandName">Name of the command to execute Accept, Reject or Accept All</param>
        public static void AcceptRejectOperations(string commandName)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: AcceptRejectOperations command executed");
            switch (commandName)
            {
                case "Accept":
                    //First check what is selected stop or operation
                    //If stop is selected then mark status of stop or operation
                    if (Model.UpdateIsStopSelected)
                        AcceptStop();
                    else //If operation is selected then mark status of operation
                        AcceptOperation();
                    Logger.LogEvent(Severity.Debug, "PushMessageCommands: Operation accepted successfully");
                    break;
                case "Reject":
                    //First check what is selected stop or operation
                    //If stop is selected then mark status of stop or operation
                    if (Model.UpdateIsStopSelected)
                        RejectStop(Model.UpdateSelectedStopId);
                    else //If operation is selected then mark status of operation
                        RejectOperation();
                    Logger.LogEvent(Severity.Debug, "PushMessageCommands: Operation rejected successfully");
                    break;

                case "AcceptAll":
                    AcceptAll();
                    Logger.LogEvent(Severity.Debug, "PushMessageCommands: All pending operation accepted successfully");
                    break;
            }

            PrepareListOfOperationStubs();
            GetFilteredOperationsForStop();
            Logger.LogEvent(Severity.Debug, "To refresh the updated operation list,List of stops and operation prepared successfully");
            Model.IsOperationListUpdated = IsProcessCompleted();


        }

        /// <summary>
        /// This method shows the summary of operations as accepted/rejected by user
        /// </summary>
        public static bool ShowOperationListUpdateSummary()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: ShowOperationListUpdateSummary command executed successfully");
            var args = new ModalMessageBoxArgument("Summary", Model.UpdateAcceptCount + " orders accepted and " + Model.UpdateRejectCount + " orders rejected", new ModalMessageBoxButton(PreCom.Utils.Language.Translate("Ok")));
            ModalMessageBox.Show(args);
            SendOperationListResponse(false);
            return CommunicationModel.Instance.CurrentPushMessageHandler.Name == "OperationList";
        }
        /// <summary>
        /// This method shows the consignment detail screen.
        /// </summary>
        /// <param name="autoResetEvent">Specifies the object for AutoResetEvent class, calling set method
        /// on this object will notify calling class then it can proceed with next statement.</param>
        public static void ShowConsignmentDetail(AutoResetEvent autoResetEvent)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: ShowConsignmentDetail command executed");
            if (CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.InvokeRequired)
            {
                ThreadStart myMethod = () => ShowConsignmentDetail(autoResetEvent);
                CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.BeginInvoke(myMethod);
                return;
            }
            Model.PackageTypes = SettingDataProvider.Instance.GetPackageTypes();
            Model.UpdateOperationConsignments = GetConsignmentNumbers();
            ShowView<FormConsignmentDetail>(1);
            autoResetEvent.Set();
        }

        /// <summary>
        /// when there is no other message is pending to process, then this command gets executed.
        /// </summary>
        public static void EndFlow()
        {

            if (CommunicationModel.Instance.CurrentPushMessageHandler.MainForm==null)
                CommunicationModel.Instance.CurrentPushMessageHandler.EndPushMessageFlow();
            else
            {
                if (CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.InvokeRequired)
                {
                    ThreadStart myMethod = EndFlow;
                    CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.BeginInvoke(myMethod);
                    return;
                }

                Logger.LogEvent(Severity.Debug, "PushMessageCommands: In pushmessage commands, EndFlow command executed");
                CommunicationModel.Instance.CurrentPushMessageHandler.EndPushMessageFlow();    
            }
        }
        #endregion


        #region #region Methods and functions for manupulating existing operation list or updated operation list

        public static void ActivateOperationListModule()
        {

            if (BaseModule.ActiveModule != null ||
                CommunicationModel.Instance.CurrentPushMessageHandler.Name != "OperationList") return;
            Logger.LogEvent(Severity.Debug,
                                   "PushMessageCommands: No module was active, so activating operation list module for showing updated operation list");
            CommunicationModel.Instance.CurrentPushMessageHandler.ActivateModule();
        }

        /// <summary>
        /// This method increments the reject count and mark the status of operation as "Rejected".
        /// </summary>
        public static void RejectOperation()
        {
            var status=MarkOperationStatus(Model.UpdateSelectedStopId, Model.UpdateSelectedOperationId, "Rejected");
            if(status)
                Model.UpdateRejectCount += 1;
        }


        /// <summary>
        /// This method is called when PDA received new operation list. It performs following tasks
        /// 1) Creates copy of newly received operation list, removes all stops init and places the same in
        /// object for Operation list.
        /// 2) Marks status of unnecessary operations as Accepted or Rejected in received operation list.
        /// 3) Prepares the list of stops, that are eligible for user interaction.
        /// 4) If no eligible stop is there, then sends operation list response.
        /// 4) Shows pop for new operation list.
        /// </summary>
        /// <returns>By default returns true.</returns>
        public static bool ReceiveNewOperationList()
        {
            Logger.LogEvent(Severity.Debug,"PushMessageCommands: Executed command ReceiveNewOperationList()");

            //Get copy of updated operation list
            var copyOfOperationList =
                (OperationList)
                GetCopyOfObject<OperationList>(CommunicationModel.Instance.UpdatedOperationList);

            //Remove all stops
            copyOfOperationList.Stop = null;

            //Set current operation list
            CommunicationModel.Instance.OperationList = copyOfOperationList;
            CommunicationModel.Instance.IsNewOperationList = true;

            MarkStatusWhenNewOperationList();

            //get list of stops that can be accepted or rejected
            GetStopsToAcceptReject();
            //If list list is enpty, then send rejected response to server
            if (Model.StopsToAcceptReject == null || Model.StopsToAcceptReject.Count == 0)
            {
                Logger.LogEvent(Severity.Debug, "Not found any suitable operation to Accept/Reject in ReceiveNewOperationList()");
                SendOperationListResponse(true);
                return false;
            }
            ActivateOperationListModule();
            var args = new ModalMessageBoxArgument("Operation List",
                                                                   "New Operation List Recieved",
                                                                   new ModalMessageBoxButton(
                                                                       "OK"));
            BaseModule.ShowModelPopUp(args);
            CommunicationModel.Instance.OperationListExists = true;

            return true;
        }


        /// <summary>
        /// This method is called when PDA receives updated operation list. This method performs following tasks.
        /// 1) It marks the status of unnecessary operations as Accepted or Rejected in received operation list.
        /// 2) Prepares the list of stops, that are eligible for user interaction.
        /// 3) If no eligible stop is there, then sends operation list response.
        /// 4) Shows the pop up for updated operation list.
        /// </summary>
        /// <returns></returns>
        public static bool ReceiveUpdatedOperationList()
        {
            Logger.LogEvent(Severity.Debug,
                                       "PushMessageCommands: Executed command ReceiveUpdatedOperationList()");
            CommunicationModel.Instance.IsNewOperationList = false;
            //Mark status 
            MarkStatusWhenUpdatedOperationList();
            //get list of stops that can be accepted or rejected
            GetStopsToAcceptReject();
            //If list list is enpty, then send rejected response to server
            if (Model.StopsToAcceptReject == null || Model.StopsToAcceptReject.Count == 0)
            {
                Logger.LogEvent(Severity.Debug, "Not found any suitable operation to Accept/Reject in ReceiveUpdatedOperationList()");
                SendOperationListResponse(true);
                return false;
            }
            ActivateOperationListModule();
            var args = new ModalMessageBoxArgument("Operation List",
                                                                       "Updated Operation List Recieved",
                                                                       new ModalMessageBoxButton
                                                                           ("OK"));
            BaseModule.ShowModelPopUp(args);
            return true;
        }


        /// <summary>
        /// This method marks status of operations in newly received operation list, as New or Rejected on the basis of their current status.
        /// 1) If status of operation is coming as "Started" or "Updated" then these will be treated as New operations
        /// 2) If status of operaion is coming as "Finish" or "Deleted" then these will be treated as Rejected operations
        /// </summary>
        public static void MarkStatusWhenNewOperationList()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method MarkStatusWhenNewOperationList()");
            foreach (var stop in Model.UpdatedOperationList.Stop)
            {
                foreach (var operation in stop.PlannedOperation)
                {
                    if (operation.Status.Equals("Started") || operation.Status.Equals("Updated"))
                        MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "New");
                    else if (operation.Status.Equals("Finish") || operation.Status.Equals("Deleted"))
                        MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "Rejected");
                }
            }

        }

        /// <summary>
        /// This method marks status of operations in updated operation list, as New, Accepted or Rejected on the bssis of their current status.
        /// 1) if incoming stop does not exist in current operation list then
        ///     a) if status of operation is Started or Updated then mark it as New
        ///     b) If status of operation is Deleted or Finish then mark it as Rejected
        /// 2) If incoming stop exists and operation exists in current operation list also then
        ///     a) If status of operation is Started, Finish or New then mark it as Accepted. 
        /// 3) If incoming stop exists and operation does not exist in current operation list then
        ///     a) If status of operation is Started or Updated then, mark it as New
        ///     b) If status of operation is Deleted or Finish then mark it as Rejected 
        /// 4) If incoming stop exists with some other status
        ///     a) Mark status of all operations as Rejected
        /// </summary>
        public static void MarkStatusWhenUpdatedOperationList()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method MarkStatusWhenUpdatedOperationList()");
            OperationListStop stopFromCurrent = null;
            foreach (var stop in Model.UpdatedOperationList.Stop)
            {
                //Get same stop from current operation list
                var copyOfStop = stop;
                if (Model.OperationList != null)
                    stopFromCurrent =
                        Model.OperationList.Stop.SingleOrDefault(c => c.StopInformation.StopId.Equals(copyOfStop.StopInformation.StopId));
                //If the same stop exists in current operation list also
                if (stopFromCurrent != null)
                {
                    //then get stop status
                    var statusInCurrent = GetStopStatus(stopFromCurrent);
                    //If status of stop in current operation is new
                    if (statusInCurrent.Equals("New"))
                    {
                        //Iterate for all operations in current stop of updated operation list
                        foreach (var operation in stop.PlannedOperation)
                        {
                            //check if same operation exists in current operation list
                            var operationFromCurrent =
                                stopFromCurrent.PlannedOperation.SingleOrDefault(
                                    c => c.OperationId.Equals(operation.OperationId));
                            //If same operation exists in current operation list also
                            if (operationFromCurrent != null)
                            {
                                Logger.LogEvent(Severity.Debug, "Operation ( Status : " + operation.Status + " ) with operationId " + operationFromCurrent.OperationId + 
                                    " in stop with StopId " + stopFromCurrent.StopInformation.StopId + 
                                    " marked Accepted, as status of operation is already New in Existing operation list");
                                // If the operation status is coming as started, finish or new then mark it as accepted
                                if (operation.Status.Equals("Started") || operation.Status.Equals("Finish") || operation.Status.Equals("New"))
                                    MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "Accepted");
                            }
                            else //Operation does not exists in current operation list
                            {
                                // If the operation status is coming as started or updated then mark that as new 
                                if (operation.Status.Equals("Started") || operation.Status.Equals("Updated"))
                                {
                                    MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "New");
                                }
                                    // If coming as deleted or finish then mark as rejected, because its of no use
                                else if (operation.Status.Equals("Deleted") || operation.Status.Equals("Finish"))
                                {
                                    Logger.LogEvent(Severity.Debug, "Operation ( Status : " + operation.Status + " ) with operationId " + operation.OperationId +
                                    " in stop with StopId " + stopFromCurrent.StopInformation.StopId +
                                    " marked Rejected, as status of operation is Deleted/Finish in received operation list");
                                    MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "Rejected");
                                }
                            }
                        }
                    }
                    else
                    {
                        //Mark each operation status as rejected
                        foreach (var operation in stop.PlannedOperation)
                        {
                            Logger.LogEvent(Severity.Debug, "Operation ( Status : " + operation.Status + " ) with operationId " + operation.OperationId +
                                    " in stop with StopId " + stopFromCurrent.StopInformation.StopId +
                                    " marked Rejected, as status of this stop is not New");
                            //Simply mark status as rejected
                            MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "Rejected");
                        }
                    }
                }
                else // If stop does not exists then
                {

                    foreach (var operation in stop.PlannedOperation)
                    {
                        // If the operation status is coming as started or updated then mark that as new 
                        if (operation.Status.Equals("Started") || operation.Status.Equals("Updated"))
                            MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "New");
                            // If coming as deleted or finish then mark as rejected, because its of no use
                        else if (operation.Status.Equals("Deleted") || operation.Status.Equals("Finish"))
                        {
                            Logger.LogEvent(Severity.Debug, "Operation ( Status : " + operation.Status + " ) with operationId " + operation.OperationId +
                                    " in stop with StopId " + stop.StopInformation.StopId +
                                    " marked Rejected, as this operation does not exists in existing operation list " +
                                     "and status of operation is coming as Deleted/Finish in updated operation list");
                            MarkOperationStatus(stop.StopInformation.StopId, operation.OperationId, "Rejected");
                        }

                    }

                }
            }
        }



        /// <summary>
        /// This function returns Array of PlannedConsignmentType for selected operation. 
        /// </summary>
        public static PlannedConsignmentsType[] GetConsignmentNumbers()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method GetConsignmentNumbers()");
            PlannedOperationType operation = null;
            PlannedConsignmentsType[] consignments = null;
            var operationListStop = (CommunicationModel.Instance.UpdatedOperationList.Stop.Where(c => c.StopInformation.StopId.Equals(CommunicationModel.Instance.UpdateSelectedStopId))).SingleOrDefault();
            if (operationListStop != null)
            {
                operation = (operationListStop.
                    PlannedOperation.Where(
                        d => d.OperationId.Equals(CommunicationModel.Instance.UpdateSelectedOperationId))).SingleOrDefault<PlannedOperationType>();
            }

            if (operation != null)
            {
                if (operation.PlannedConsignments != null)
                    consignments = operation.PlannedConsignments.Where(c => !string.IsNullOrEmpty(c.ConsignmentNumber)).ToArray();
            }
            

            return consignments;
        }



        /// <summary>
        /// This method send the operation list response to TM.
        /// </summary>
        public static void SendOperationListResponse(bool isToReject)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method SendOperationListResponse()");
            var operationListResponse = GetOperationListResponseObject(isToReject);

            var transaction = new Transaction()
            {
                Id = Guid.NewGuid(),
                Category = (byte
                    )TransactionCategory.T20210_DriverAvailabilityToTMSRequest,
                Description = "OperationListId:" + Model.UpdatedOperationList.OperationListId
            };
            CommunicationClient.Instance.SendAsynchronously(operationListResponse, transaction);

            Logger.LogEvent(Severity.Debug, "PushMessageCommands : OperationListResponse sent successfully in method SendOperationListResponse()");

            BaseModule.AllowToDeactivate = true;

            //Do some cleaning
            //If current operation list is having stop as null
            //It means user have not accepted any operation in new operation list
            if (Model.OperationList.Stop == null)
            {
                Model.OperationList = null;
                CommunicationModel.Instance.OperationListExists = false;
            }

            Model.IsOperationListUpdated = false;
            Model.UpdatedOperationList = null;
            Model.UpdateAcceptCount = 0;
            Model.UpdateFilteredOperationsForStop = null;
            Model.UpdateIsStopSelected = false;
            Model.UpdateOperationConsignments = null;
            Model.UpdateRejectCount = 0;
            Model.UpdateSelectedOperationId = "";
            Model.UpdateSelectedStatus = 0;
            Model.UpdateSelectedStopId = "";
            Model.UpdatedOperationList = null;
            Model.UpdatedOperationListStubs = null;
            Model.StopsToAcceptReject = null;


        }

        #region Functions related to sending operation list response

        /// <summary>
        /// This method prepares the object for OperationListResponse using currently received operation list.
        /// </summary>
        /// <param name="isToReject">Specifies that is to mark status of Stop and operations as Rejected or not. If true
        /// then marks status of all stops and their operations as rejected by default. other calculates the status
        /// of stop on the basis of status for their operations and keeps the status of operations as it is.</param>
        /// <returns>Returns object for operation list response.</returns>
        public static OperationListResponse GetOperationListResponseObject(bool isToReject)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method GetOperationListResponseObject()");
            OperationListResponse operationListResponse = null;
            var operationList = CommunicationModel.Instance.UpdatedOperationList;
            var stops = operationList.Stop;

            if (stops != null && stops.Any())
            {
                operationListResponse = new OperationListResponse();
                //Get response stops
                var operationListResponseStops = new List<OperationListResponseStop>();
                foreach (var stop in stops)
                {
                    var responseStop = GetResponseStop(stop, isToReject);
                    operationListResponseStops.Add(responseStop);
                }
                operationListResponse.Stop = operationListResponseStops.ToArray();
                operationListResponse.OperationListId = operationList.OperationListId;
                operationListResponse.OperationListTime = DateTime.Now;
                operationListResponse.SessionId = CommunicationModel.Instance.UserProfileSessionId;
                operationListResponse.CorrelationId = operationList.CorrelationId ?? string.Empty;
                operationListResponse.StatusInformation = "";
                operationListResponse.TMSAffiliationId = CommunicationModel.Instance.UserProfile.TmsAffiliationId;

                //get operation list status
                var totalCount = operationListResponseStops.Count();
                var acceptedCount = operationListResponseStops.Count(c => c.Status.Equals(StatusType.Accepted));
                var rejectdCount = operationListResponseStops.Count(c => c.Status.Equals(StatusType.Rejected));

                if (acceptedCount > 0 && acceptedCount == totalCount)
                    operationListResponse.Status = StatusType.Accepted;
                else if (rejectdCount > 0 && rejectdCount == totalCount)
                    operationListResponse.Status = StatusType.Rejected;
                else if (acceptedCount > 0 && rejectdCount > 0)
                    operationListResponse.Status = StatusType.PartlyAccepted;

            }
            return operationListResponse;
        }



        /// <summary>
        /// This function returns object for OperationListResponseStop for specified stop.
        /// </summary>
        /// <param name="stop">Stop for which we need to generate object for OperationListResponseStop.</param>
        /// <param name="isToReject">Specifies that is to mark status of Stop and operations as Rejected or not. If true
        /// then marks status of all stops and their operations as rejected by default. other calculates the status
        /// of stop on the basis of status for their operations and keeps the status of operations as it is.</param>
        /// <returns>Returns object of OperationListResponseStop as required.</returns>
        public static OperationListResponseStop GetResponseStop(OperationListStop stop, bool isToReject)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method GetResponseStop()");
            OperationListResponseStop responseStop=null;
            if (stop != null)
            {
                responseStop = new OperationListResponseStop();
                responseStop.StopId = stop.StopInformation.StopId;
                responseStop.StatusInformation = "";
                //Get stop status
                if (stop.PlannedOperation != null && stop.PlannedOperation.Any())
                {
                    //If session id exists
                    if (!isToReject)
                    {
                        var totalCount = stop.PlannedOperation.Count();
                        var acceptedCount = stop.PlannedOperation.Count(c => c.Status.Equals("Accepted"));
                        var rejectdCount = stop.PlannedOperation.Count(c => c.Status.Equals("Rejected"));

                        if (acceptedCount > 0 && acceptedCount == totalCount)
                            responseStop.Status = StatusType.Accepted;
                        else if (rejectdCount > 0 && rejectdCount == totalCount)
                            responseStop.Status = StatusType.Rejected;
                        else if (acceptedCount > 0 && rejectdCount > 0)
                            responseStop.Status = StatusType.PartlyAccepted;
                    }
                    else
                        responseStop.Status = StatusType.Rejected;
                }
                else
                {
                    responseStop.Status = StatusType.Rejected;
                }
                //Get operations array
                responseStop.Operation = GetResponseOperations(stop, isToReject);
                //Get trip Id
                responseStop.TripId = GetTripIdFromOperationListStop(stop);
            }
            return responseStop;
        }

        public static string GetTripIdFromOperationListStop(OperationListStop stop)
        {
            string tripId = string.Empty;
           //If stop is null or operation in stop not available return empty 
            if(stop==null || stop.PlannedOperation==null || !stop.PlannedOperation.Any()) return tripId;
            //Iterate for each operation to get not null trip id
            foreach (var operation in stop.PlannedOperation.Where(operation => !string.IsNullOrEmpty(operation.TripId)))
            {
                tripId = operation.TripId;
                break;
            }
            //If trip id from operations is empty, then get it from stop
            if (string.IsNullOrEmpty(tripId) && !string.IsNullOrEmpty(stop.TripId))
                tripId = stop.TripId;
            return tripId;
        }



        /// <summary>
        /// This function returns array for objects of type OperationListResponseStopOperation for specified stop.
        /// </summary>
        /// <param name="stop">Stop for which to get array for objects of type OperationListResponseStopOperation.</param>
        /// <param name="isToReject">Specifies that is to mark status of operations as Rejected or keep them as it is.</param>
        /// <returns>Returns array for objects of type OperationListResponseStopOperation.</returns>
        public static OperationListResponseStopOperation[] GetResponseOperations(OperationListStop stop, bool isToReject)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method GetResponseOperations()");
            var listOperations = new List<OperationListResponseStopOperation>();

            if (stop==null || stop.PlannedOperation == null) return null;
            foreach (var operation in stop.PlannedOperation)
            {

                var responseOperation = new OperationListResponseStopOperation();
                responseOperation.OperationId = operation.OperationId;
                responseOperation.Orders = null;

                if (!isToReject)
                    responseOperation.Status = operation.Status.Equals("Accepted")
                                                   ? StatusType.Accepted.ToString()
                                                   : StatusType.Rejected.ToString();
                else
                    responseOperation.Status = "Rejected";

                responseOperation.StatusInformation = "";
                if (operation.PlannedConsignments != null && operation.PlannedConsignments.Any())
                    responseOperation.Consignment = GetResponseConsignments(operation, isToReject);
                listOperations.Add(responseOperation);
            }
            return listOperations.ToArray();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="isToReject"></param>
        /// <returns></returns>
        public static OperationListResponseStopOperationConsignment[] GetResponseConsignments(PlannedOperationType operation, bool isToReject)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands : Executing method GetResponseConsignments()");
            var responseConsignments = new List<OperationListResponseStopOperationConsignment>();
            foreach (var consignment in operation.PlannedConsignments)
            {

                var responseConsignment = new OperationListResponseStopOperationConsignment();
                responseConsignment.ConsignmentNumber = string.IsNullOrEmpty(consignment.ConsignmentNumber) ? "" : consignment.ConsignmentNumber;
                if (!isToReject)
                    responseConsignment.Status = operation.Status.Equals("Accepted")
                                                   ? StatusType.Accepted
                                                   : StatusType.Rejected;
                else
                    responseConsignment.Status = StatusType.Rejected;

                responseConsignment.StatusInformation = string.Empty;
                //create array for shipment
                var listShipments = new List<OperationListResponseStopOperationConsignmentShipment>();
                if (consignment.Shipment != null)
                    foreach (var shipment in consignment.Shipment)
                    {
                        listShipments.Add(new OperationListResponseStopOperationConsignmentShipment()
                        {
                            ShipmentId = shipment.ShipmentId,
                            ShipmentDetailId = shipment.ShipmentDetailId
                        });
                    }

                responseConsignment.Shipment = listShipments.ToArray();
                responseConsignments.Add(responseConsignment);
            }
            return responseConsignments.ToArray();
        }

        #endregion


        /// <summary>
        /// This method prepares the list of StubOperationListStop objects. So that it can bind to grid
        /// because original stops can't be binded to advanced list.
        /// </summary>
        public static void PrepareListOfOperationStubs()
        {

            Logger.LogEvent(Severity.Debug, "PushMessageCommands: Executing method PrepareListOfOperationStubs.");
            if (CommunicationModel.Instance.UpdatedOperationList == null) return;
            var operationListStubs = new List<StubOperationListStop>();
            foreach (var stop in Model.UpdatedOperationList.Stop)
            {
                //If stop is not eligible to show then just continue for next stop
                if (!IsStopToShow(stop.StopInformation.StopId)) continue;
                //
                var stopStub = GetStubOperationListStop(stop);
                if (CommunicationModel.Instance.UpdateFilteredOperationsForStop == null)
                {
                    CommunicationModel.Instance.UpdateSelectedStopId = stop.StopInformation.StopId;
                    GetFilteredOperationsForStop();
                }
                operationListStubs.Add(stopStub);
            }

            Model.UpdatedOperationListStubs = operationListStubs;

            Logger.LogEvent(Severity.Debug, "PushMessageCommands: GetConsignmentNumbers method executed.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stop"></param>
        /// <returns></returns>
        public static StubOperationListStop GetStubOperationListStop(OperationListStop stop)
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: GetStubOperationListStop method executed.");

            StubOperationListStop stubOperationListStop;

            if (stop != null)
            {
                //get planned operations in stop
                var plannedOperations = (from n in stop.PlannedOperation
                                                     where true
                                                     select n).ToList<PlannedOperationType>();

                string stStopType;
                string stOperationId;
                string stOrderNumber;
                stubOperationListStop = new StubOperationListStop
                {
                    StopStatus =
                        GetStopStatus(plannedOperations, out stStopType, out stOperationId,
                                      out stOrderNumber),
                    StopSign = stStopType,
                    OperationId = stOperationId,
                    OrderNumber = stOrderNumber
                };
                if (String.Compare(stStopType, StopOperationType.OperationPlus, StringComparison.Ordinal) == 0)
                    stubOperationListStop.AlternateStopSign = StopOperationType.OperationMinus;
                else
                    stubOperationListStop.AlternateStopSign = stStopType;

                stubOperationListStop.StopTimeInfo = stop.StopInformation.EarliestStopTime.TimeOfDay + "-" +
                                              stop.StopInformation.LatestStopTime.TimeOfDay + " " +
                                              stop.StopInformation.StopName;

                AddressType address = stop.StopInformation.Address;
                if (address != null)
                    stubOperationListStop.StopAddress = address.StreetName + " " + address.StreetNumber + "," +
                                                 address.PostalCode + " " + address.PostalName;
                stubOperationListStop.StopId = stop.StopInformation.StopId;
            }
            else
                return null;



            return stubOperationListStop;
        }






        /// <summary>
        /// This methods gets the list of operations for current selected stop that are eligible to show
        /// on screen and places the same in CommunicationModel.Instance.UpdateFilteredOperationsForStop
        /// </summary>
        public static void GetFilteredOperationsForStop()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: GetFilteredOperationsForStop method executed.");
            if (CommunicationModel.Instance.UpdatedOperationList == null) return;    
            var operationListStop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                     where c.StopInformation.StopId.Equals(CommunicationModel.Instance.UpdateSelectedStopId)
                                     select c).SingleOrDefault<OperationListStop>();
            if (operationListStop == null) return;
            var stopOperations = operationListStop.PlannedOperation.ToList();
            var filteredOperations = stopOperations.Where(operation => IsOperationToShow(Model.UpdateSelectedStopId, operation.OperationId)).ToList();
            if (filteredOperations.Any())
            CommunicationModel.Instance.UpdateFilteredOperationsForStop = filteredOperations;
        }


        public static void GetStopsToAcceptReject()
        {
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: GetStopsToAcceptReject method executed.");

            Model.StopsToAcceptReject = new List<string>();

            //If case of new operation list
            if (CommunicationModel.Instance.OperationList.Stop == null)
            {
                //Iterate for each stop in collection of stops
                foreach (var stop in CommunicationModel.Instance.UpdatedOperationList.Stop)
                {
                    //Get the stop status and check if its new, then add in collection
                    if (GetStopStatus(stop) == StopOperationType.OperationNew)
                        Model.StopsToAcceptReject.Add(stop.StopInformation.StopId);
                }
            }
            else
            {
                //Iterate for each stop in in updated operation list
                foreach (var stop in CommunicationModel.Instance.UpdatedOperationList.Stop.Where(c => c.PlannedOperation != null && c.PlannedOperation.Any()))
                {
                    //Get count of operations having status other than New,Updated,Deleted
                    var countNotAcceptedRejected =
                        stop.PlannedOperation.Count(c => !c.Status.Equals("Accepted") && !c.Status.Equals("Rejected"));
                    //If exists, then stop is of no use, as it is already started
                    if (countNotAcceptedRejected == 0)
                        continue;


                    //Get count of operations having status other than New,Updated,Deleted
                    var countStartedOperations =
                        stop.PlannedOperation.Count(c => !c.Status.Equals("New") && !c.Status.Equals("Updated") && !c.Status.Equals("Deleted") && !c.Status.Equals("Accepted") && !c.Status.Equals("Rejected"));
                    //If exists, then stop is of no use, as it is already started
                    if (countStartedOperations > 0)
                        continue;

                    //Now check status of same stop in current operation list
                    var stopToCheck = CommunicationModel.Instance.OperationList.Stop.SingleOrDefault(c => c.StopInformation.StopId == stop.StopInformation.StopId);

                    if (stopToCheck == null)
                        Model.StopsToAcceptReject.Add(stop.StopInformation.StopId);
                    else
                    {
                        //Get status of object  
                        var status = GetStopStatus(stopToCheck);
                        //If status is new
                        if (status == StopOperationType.OperationNew)
                            Model.StopsToAcceptReject.Add(stop.StopInformation.StopId);
                    }

                }
            }

        }

        /// <summary>
        /// This method takes the list of operations for selected stop and returns output variables for
        /// stStopType --
        /// operationId --
        /// orderNumber --
        /// </summary>
        /// <param name="listOfOperations"> </param>
        /// <param name="stopType"> </param>
        /// <param name="operationId"></param>
        /// <param name="orderNumber"> </param>
        /// <returns></returns>
        public static int GetStopStatus(List<PlannedOperationType> listOfOperations, out string stopType, out string operationId, out string orderNumber)
        {
            int stopStatus = 0;   //0 for completed
            operationId = string.Empty;
            orderNumber = string.Empty;
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: GetStopStatus method executed.");
            if (listOfOperations.Count == 1)
            {
                var plannedOperation = listOfOperations[0];
                stopType = GetOperationType(plannedOperation.OperationType);
                stopStatus = PlannedOperationStatus(plannedOperation.Status);
                operationId = plannedOperation.OperationId;
            }
            else
            {
                //operationId = "";
                stopType = StopOperationType.OperationPlus;

                foreach (PlannedOperationType plOt in listOfOperations)
                {
                    int tempStatus = PlannedOperationStatus(plOt.Status);
                    if (tempStatus == 1)  //not started
                        stopStatus = stopStatus != 0 ? stopStatus : tempStatus;
                    else if (tempStatus == 2)  //started but not completed
                    {
                        stopStatus = tempStatus;
                        break;
                    }
                    else
                    {
                        stopStatus = stopStatus > 0 ? 2 : tempStatus;
                    }
                }
            }

            return stopStatus;
        }

        /// <summary>
        /// this method used if operation type is pickup for checking is this operation is with different order number consignment
        /// </summary>
        /// <param name="plannedOperation"></param>
        /// <returns></returns>
        public static int CheckPickUpConsignOrderNo(PlannedOperationType plannedOperation)
        {
            List<string> orderNumbers = null;

            if (plannedOperation != null)
            {
                orderNumbers = new List<string>();
                foreach (var plannedConsignment in plannedOperation.PlannedConsignments)
                {
                    if (!string.IsNullOrEmpty(plannedConsignment.OrderNumber) && !orderNumbers.Contains(plannedConsignment.OrderNumber))
                        orderNumbers.Add(plannedConsignment.OrderNumber);
                }
            }

            return orderNumbers == null ? 0 : orderNumbers.Count;
        }


        /// <summary>
        /// method used for evaluated the planned operation status...
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int PlannedOperationStatus(string status)
        {
            int iStatus;


            if (String.CompareOrdinal(status, UpdateOperationListStatus.New.ToString()) == 0)
            {
                iStatus = (int)UpdateOperationListImageType.New;
            }
            else if (String.CompareOrdinal(status, UpdateOperationListStatus.Updated.ToString()) == 0)
            {
                iStatus = (int)UpdateOperationListImageType.Updated;
            }
            else
            {
                iStatus = (int)UpdateOperationListImageType.Deleted;
            }
            return iStatus;
        }
        /// <summary>
        /// method for evaluate PlannedOperation Text OperationType and cast their value in our created type.
        /// </summary>
        /// <param name="type">Specifies the type of operation, that we need to check for.</param>
        /// <returns></returns>
        public static string GetOperationType(PlannedOperationTypeOperationType type)
        {
            string stOperationType = string.Empty;

            switch (type)
            {
                case PlannedOperationTypeOperationType.DELIVERY:
                    stOperationType = StopOperationType.OperationDelivery;
                    break;
                case PlannedOperationTypeOperationType.LOAD:
                    stOperationType = StopOperationType.OperationLoad;
                    break;
                case PlannedOperationTypeOperationType.LOAD_DISTRIBUTION:
                    stOperationType = StopOperationType.OperationLoad;
                    break;
                case PlannedOperationTypeOperationType.LOAD_LINE_HAUL:
                    stOperationType = StopOperationType.OperationLoad;
                    break;
                case PlannedOperationTypeOperationType.PICKUP:
                    stOperationType = StopOperationType.OperationPickUp;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD:
                    stOperationType = StopOperationType.OperationUnload;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL:
                    stOperationType = StopOperationType.OperationUnload;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD_PICKUP:
                    stOperationType = StopOperationType.OperationUnload;
                    break;
            }


            return stOperationType;
        }



        /// <summary>
        /// This method gets the selected stop from updated operation list
        /// and adds/removes in the current operation list.
        /// </summary>
        /// <param name="updateType"></param>
        /// <param name="stopId"></param>
        public static void AddUpdateRemoveStop(int updateType, string stopId)
        {
            //Get selected stop from updated operation list
            Logger.LogEvent(Severity.Debug, "PushMessageCommands: AddUpdateRemoveStop method executed.");
            switch (updateType)
            {
                case 0: //Add stop to current operation list

                    //Get specified stop from updated operation list
                    var stopToAdd = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                     where c.StopInformation.StopId.Equals(stopId)
                                     select c).SingleOrDefault<OperationListStop>();

                    //Get stop with current stop id from current operation list

                    //If current operation list is null
                    OperationListStop stop;
                    if (CommunicationModel.Instance.OperationList.Stop == null)
                    {
                        //Get copy of object and add the same to operation list
                        stop = (OperationListStop)GetCopyOfObject<OperationListStop>(stopToAdd);

                        var oprlist = new List<PlannedOperationType>();
                        foreach (var oper in stop.PlannedOperation.Where(c => !c.Status.Equals("Rejected") && !c.Status.Equals("Rejected")))
                        {
                            Model.UpdateAcceptCount += 1;
                            var copyOfOperation = (PlannedOperationType)GetCopyOfObject<PlannedOperationType>(oper);
                            oprlist.Add(copyOfOperation);
                        }
                        stop.PlannedOperation = oprlist.ToArray();
                        CommunicationModel.Instance.OperationList.Stop = new OperationListStop[] { stop };

                    }
                    else
                    {
                        stop = CommunicationModel.Instance.OperationList.Stop.SingleOrDefault(c => c.StopInformation.StopId == stopId);
                        //If stop exists
                        if (stop != null)
                        {
                            //Then add only the pending operations
                            if (stopToAdd != null)
                                foreach (var operation in stopToAdd.PlannedOperation.Where(c => !c.Status.Equals("Accepted") && !c.Status.Equals("Rejected")))
                                {
                                    Model.UpdateAcceptCount += 1;
                                    stop.PlannedOperation.ToList().Add(operation);
                                }
                            Logger.LogEvent(Severity.Debug, "PushMessageCommands: All new operations from method executed.");
                        }
                        else
                        {
                            //Add complete stop in one go
                            CommunicationModel.Instance.OperationList.Stop.ToList<OperationListStop>().Add(stopToAdd);
                        }

                    }
                    break;
                case 1: //Remove stop from current operation list
                    //Get stop to remove from updated operation list
                    var stopToRemove = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                        where c.StopInformation.StopId.Equals(stopId)
                                        select c).SingleOrDefault<OperationListStop>();
                    //Get all operations from stop which are neither accepted nor rejected
                    //and Iterate for each operation
                    if (stopToRemove != null)
                        foreach (var operation in stopToRemove.PlannedOperation.Where(c => !c.Status.Equals("Accepted") && !c.Status.Equals("Rejected")))
                        {
                            Model.UpdateAcceptCount += 1;
                            //Remove operation from current operation list where operation id matches
                            var operationListStop = CommunicationModel.Instance.OperationList.Stop.SingleOrDefault(c => c.StopInformation.StopId == stopId);
                            if (operationListStop != null)
                                operationListStop
                                    .PlannedOperation.ToList().RemoveAll(d => d.OperationId == operation.OperationId);
                        }
                    break;
                case 2:
                    {
                        //Get specified stop from updated operation list
                        var stopToUpdate = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                            where c.StopInformation.StopId.Equals(stopId)
                                            select c).SingleOrDefault<OperationListStop>();
                        //Iterate for all operations in current stop
                        if (stopToUpdate != null)
                            foreach (var plannedOpr in stopToUpdate.PlannedOperation)
                            {
                                Model.UpdateAcceptCount += 1;
                                //Get status of operation, that it is added, updated or deleted
                                int status = PlannedOperationStatus(plannedOpr.Status);
                                //Add,Remmove or update operation on the basis of status
                                AddUpdateRemoveOperation(status, stopToUpdate.StopInformation.StopId, plannedOpr.OperationId);
                            }
                        break;
                    }
            }


        }


        /// <summary>
        /// This method gets the selected operation from updated operation list
        /// and adds/removes in current operation list.
        /// </summary>
        /// <param name="updateType">Specifies the action (add/update/delete) to perform</param>
        /// <param name="stopId">Selected stop id.</param>
        /// <param name="operationId">Selectd operation id.</param>
        public static void AddUpdateRemoveOperation(int updateType, string stopId, string operationId)
        {
            OperationListStop stopInWhichToAdd = null;
            //Get selected stop from updated operation list
            var stop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                        where c.StopInformation.StopId.Equals(stopId)
                        select c).SingleOrDefault<OperationListStop>();

            //Get the selected operation
            if (stop == null) return;
            var operation = (from c in stop.PlannedOperation where c.OperationId.Equals(operationId) select c).SingleOrDefault<PlannedOperationType>();



            switch (updateType)
            {
                case 0: //Add Operation
                    {

                        if (CommunicationModel.Instance.OperationList.Stop != null)
                            //get stop in which to add
                            stopInWhichToAdd = (from c in CommunicationModel.Instance.OperationList.Stop
                                                where c.StopInformation.StopId.Equals(stopId)
                                                select c).SingleOrDefault<OperationListStop>();
                        //If stop does not exist in current operation list
                        if (stopInWhichToAdd == null)
                        {
                            //Get object of stop from Updated Operation list
                            var stopInWhichToAddNew = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                                       where c.StopInformation.StopId.Equals(stopId)
                                                       select c).SingleOrDefault<OperationListStop>();

                            var copyOfStop = (OperationListStop)GetCopyOfObject<OperationListStop>(stopInWhichToAddNew);

                            var copyOfOperation = (PlannedOperationType)GetCopyOfObject<PlannedOperationType>(operation);

                            //reinitialize the planned operation collection
                            copyOfStop.PlannedOperation = new PlannedOperationType[] { copyOfOperation };

                            //Set stop status as new one
                            copyOfStop.Status = "New";
                            //Then first create the stop

                            if (CommunicationModel.Instance.OperationList.Stop == null)
                                CommunicationModel.Instance.OperationList.Stop = new OperationListStop[] { copyOfStop };
                            else
                            {
                                var currentStops = CommunicationModel.Instance.OperationList.Stop.ToList<OperationListStop>();
                                currentStops.Add(copyOfStop);
                                CommunicationModel.Instance.OperationList.Stop = currentStops.ToArray();
                            }

                        }
                        else
                        {
                            var isOperationExistcount = stopInWhichToAdd.PlannedOperation.Count(c => operation != null && c.OperationId.Equals(operation.OperationId));
                            if (isOperationExistcount == 0)
                            {
                                //Add operation now
                                var operations = stopInWhichToAdd.PlannedOperation.ToList<PlannedOperationType>();
                                var copyOfOperation =
                                    (PlannedOperationType)GetCopyOfObject<PlannedOperationType>(operation);
                                operations.Add(copyOfOperation);
                                stopInWhichToAdd.PlannedOperation = operations.ToArray<PlannedOperationType>();
                            }

                        }
                        break;
                    }
                case 1: //Remove operation
                    //Get stop from which operation to remove
                    var stopInWhichToRemove = (from c in CommunicationModel.Instance.OperationList.Stop
                                               where c.StopInformation.StopId.Equals(stopId)
                                               select c).SingleOrDefault<OperationListStop>();
                    //get list of operation from stop
                    if (stopInWhichToRemove != null)
                    {
                        var operationsToRemoveFrom = stopInWhichToRemove.PlannedOperation.ToList<PlannedOperationType>();

                        //Check if operation to remove is last one
                        var count = operationsToRemoveFrom.Count();
                        //If count is 0, then remove the stop also
                        if (count == 1)
                        {
                            var stops = CommunicationModel.Instance.OperationList.Stop.ToList();
                            stops.RemoveAll(
                                d => d.StopInformation.StopId == stopId);
                            CommunicationModel.Instance.OperationList.Stop = stops.ToArray<OperationListStop>();
                        }
                        else
                        {
                            //Remove required operation
                            operationsToRemoveFrom.RemoveAll(d => operation != null && d.OperationId == operation.OperationId);

                            //Place the modified collection of operations
                            stopInWhichToRemove.PlannedOperation = operationsToRemoveFrom.ToArray<PlannedOperationType>();
                        }
                    }

                    break;
                case 2:
                    {   //Update Operation
                        //Get the reference of stop in which to update
                        var stopInWhichToUpdate = (from c in CommunicationModel.Instance.OperationList.Stop
                                                   where c.StopInformation.StopId.Equals(stopId)
                                                   select c).SingleOrDefault<OperationListStop>();
                        //get operations from stop
                        if (stopInWhichToUpdate != null)
                        {
                            var operationsToUpdate = stopInWhichToUpdate.PlannedOperation.ToList<PlannedOperationType>();
                            //Remove required operation
                            operationsToUpdate.RemoveAll(d => operation != null && d.OperationId == operation.OperationId);
                            //Get copy of operation
                            var copyOfOperation = (PlannedOperationType)GetCopyOfObject<PlannedOperationType>(operation);
                            copyOfOperation.Status = "New";
                            //Add modified one
                            operationsToUpdate.Add(copyOfOperation);
                            //Place modified operations in stop again
                            stopInWhichToUpdate.PlannedOperation = operationsToUpdate.ToArray<PlannedOperationType>();
                        }
                        break;
                    }
            }
        }





        /// <summary>
        /// This method checks that if we need to show this stop in list of stops and returns the result on the 
        /// basis of following:
        /// 1) If stop is not in the list of stops to show then returns false.
        /// 2) If stop is not having any thing pending to accept/reject then returns false.
        /// 3) If the status of the stop is started or completed in current operation list it returns false.
        /// </summary>
        /// <param name="stopId">Specifies the id of the stop, that we need to check for.</param>
        /// <returns>Specifies the id of the stop which we need to check status of</returns>
        public static bool IsStopToShow(string stopId)
        {

            if (!Model.StopsToAcceptReject.Contains(stopId))
                return false;

            //Get stop from updated operation list
            var stop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                      where c.StopInformation.StopId.Equals(stopId)
                                      select c).SingleOrDefault<OperationListStop>();

            //checkIf there is any operation pending that is having
            //status other than Accepted or Rejected 
            //then return true
            bool isAnyPending = false;
            if (stop != null)
#pragma warning disable 168
                foreach (var plannedOperation in stop.PlannedOperation.Where(plannedOpr => !plannedOpr.Status.Equals("Accepted") &&
#pragma warning restore 168
                                                                                     !plannedOpr.Status.Equals("Rejected")))
                    isAnyPending = true;
            if (!isAnyPending)
                return false;


            if (CommunicationModel.Instance.OperationList.Stop == null)
                stop = null;
            else
            {
                //get object of stop for specified stop id from current operation list
                stop = (from c in CommunicationModel.Instance.OperationList.Stop
                        where c.StopInformation.StopId.Equals(stopId)
                        select c).SingleOrDefault<OperationListStop>();
            }


            //If stop not found in current operation list then return true
            //as it can be accepted or rejected without thinking, because it is new one
            if (stop == null)
                return true;
            else
            {
                var status = GetStopStatus(stop);
                if (status.Equals(StopOperationType.OperationCompleted) || status.Equals(StopOperationType.OperationStarted))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// This method returns the status of the stop on the following basis:
        /// 1) Count of operations with status "Started"
        /// 2) count of operations with status "Finish"
        /// 3) Count of operations with status "New"
        /// </summary>
        /// <param name="stop"></param>
        /// <returns></returns>
        public static string GetStopStatus(OperationListStop stop)
        {
            string status = string.Empty;

            if (stop != null)
            {
                var countOfStarted =
                    stop.PlannedOperation.Count(c => c.Status.Equals("Started"));
                var countOfFinish =
                    stop.PlannedOperation.Count(c => c.Status.Equals("Finish"));
                var countOfNew =
                    stop.PlannedOperation.Count(c => c.Status.Equals("New"));

                //stop.PlannedOperation.Count(c => c.Status.Equals("Updated"));

                if (Model.IsNewOperationList)
                {
                    if (countOfStarted > 0 || countOfNew > 0)
                        return "New";
                }
                else
                {
                    if (countOfStarted == 0 && countOfNew == 0)
                        return "Finish";
                    if ((countOfFinish == 0 && countOfNew == 0) || (countOfFinish > 0 && countOfNew > 0) || countOfStarted > 0)
                        return "Started";
                    if (countOfStarted == 0 && countOfFinish == 0)
                        return "New";
                }
            }
            return status;
        }



        /// <summary>
        /// Ths method returns boolean value on the following basis:
        /// 1) If operation is already accepted/rejected returns false.
        /// 2) If Operation is related to any stop that is not in the list of stops to show then will return false.
        /// 3) If operation is related to any invalid stop then will return false.
        /// 4) If operation is is invalid in updated operation list, then will return false. 
        /// </summary>
        public static bool IsOperationToShow(string stopId, string operationId)
        {
            //If stop does not exist, return false
            var stop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                        where c.StopInformation.StopId.Equals(stopId)
                        select c).SingleOrDefault<OperationListStop>();
            if (stop == null)
                return false;

            //If stop exists in list of eligible stops, if not then return false
            if (!Model.StopsToAcceptReject.Contains(stopId))
                return false;
            
            
            //Get required operation from stop and check for null
            var operation = (from d in
                                 stop.PlannedOperation
                             where d.OperationId.Equals(operationId)
                             select d).SingleOrDefault<PlannedOperationType>();
            if (operation == null)
                return false;
            string operationStatus = operation.Status;
            
            //If new operation list then, just check if 
            if (Model.IsNewOperationList)
            {
                //If operation is having status as  accepted,rejected then just return false;
                if (operationStatus.Equals("Accepted") || operationStatus.Equals("Rejected") || operationStatus.Equals("Finish"))
                    return false;
            }
            else
            {
                //If operation is having status as  accepted,rejected then just return false;
                if (operationStatus.Equals("Accepted") || operationStatus.Equals("Rejected"))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// This method marks the status of secified stop with specified status
        /// </summary>
        /// <param name="stopId">Id for stop of which to mark status</param>
        /// <param name="status">Status which is to update with.</param>
        public static void MarkStopStatus(string stopId, string status)
        {
            //Iterate for each planned operation in current stop
            var operationListStop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                     where c.StopInformation.StopId.Equals(stopId)
                                     select c).SingleOrDefault<OperationListStop>();
            if (operationListStop != null)
                foreach (var operation in operationListStop.PlannedOperation.Where(operation => !operation.Status.Equals("Accepted") && !operation.Status.Equals("Rejected")))
                {
                    operation.Status = status;
                }
        }

        /// <summary>
        /// This method marks the status of specified operation in secified stop with specified status
        /// </summary>
        /// <param name="stopId">If for stop in which there is specified operation.</param>
        /// <param name="operationId">Id for operation of which to mark the status.</param>
        /// <param name="status">Status which is to update with.</param>
        public static bool MarkOperationStatus(string stopId, string operationId, string status)
        {
            var stop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                        where c.StopInformation.StopId.Equals(stopId)
                        select c).SingleOrDefault<OperationListStop>();
            if (stop == null) return false;
            var operation = stop.PlannedOperation.SingleOrDefault(d => d.OperationId == operationId);
            if (operation != null) operation.Status = status; else return false;
            return true;
        }

        /// <summary> 
        /// This method accepts the current selected stop, does changes to current operation list and 
        /// mark that operations as Accepted/Rejected in updated operation list.
        /// </summary>
        public static void AcceptStop()
        {
            AddUpdateRemoveStop(Model.UpdateSelectedStatus, Model.UpdateSelectedStopId);
            MarkStopStatus(Model.UpdateSelectedStopId, "Accepted");
        }

        public static void RejectStop(string stopId)
        {
            //Iterate for each planned operation in current stop
            var operationListStop = (from c in CommunicationModel.Instance.UpdatedOperationList.Stop
                                     where c.StopInformation.StopId.Equals(stopId)
                                     select c).SingleOrDefault<OperationListStop>();
            if (operationListStop != null)
                foreach (var operation in operationListStop.PlannedOperation.Where(operation => !operation.Status.Equals("Accepted") && !operation.Status.Equals("Rejected")))
                {
                    Model.UpdateRejectCount += 1;
                    operation.Status = "Rejected";
                }
        }


        /// <summary>
        /// This method acceptes the current selected operation, does changes in current operation list and
        /// mark that operation as Accepted/Rejected in updated operation list.
        /// </summary>
        public static void AcceptOperation()
        {
            Model.UpdateAcceptCount += 1;
            AddUpdateRemoveOperation(Model.UpdateSelectedStatus, Model.UpdateSelectedStopId, Model.UpdateSelectedOperationId);
            MarkOperationStatus(Model.UpdateSelectedStopId, Model.UpdateSelectedOperationId, "Accepted");

        }



        /// <summary>
        /// This method accepts all the pending operations in all stops and mark them as accepted or rejected.
        /// </summary>
        public static void AcceptAll()
        {


            //Get all stops with status other than completed or started
            var stops = Model.UpdatedOperationList.Stop
                         .Where<OperationListStop>(c => Model.StopsToAcceptReject.Contains(c.StopInformation.StopId))
                         .ToList<OperationListStop>();
            //Iterate for each stop
            foreach (var stop in stops)
            {
                //If stop to process
                bool isStopInList = IsStopToShow(stop.StopInformation.StopId);
                //If yes
                if (!isStopInList) continue;
                Model.UpdateSelectedStopId = stop.StopInformation.StopId;
                //get operations in stop and iterate for all
                var operations = stop.PlannedOperation.Where(c => !c.Status.Equals("Accepted") && !c.Status.Equals("Rejected"));
                foreach (var opr in operations)
                {
                    Model.UpdateSelectedOperationId = opr.OperationId;
                    Model.UpdateSelectedStatus = PlannedOperationStatus(opr.Status);
                    AcceptOperation();
                }
            }

        }


        /// <summary>
        /// This method iterates for operations in all stops that are the part of
        /// StopsToAcceptReject collection, and checks if any is pending without having status
        /// either accept or reject. If found then returns true otherwise returns false.
        /// </summary>
        /// <returns>Specifies the boolean result.</returns>
        public static bool IsProcessCompleted()
        {
            bool isAnyPending = false;


            //Get all stops that user was supposed to accept or reject
            var stops = (from c in Model.UpdatedOperationList.Stop
                         where Model.StopsToAcceptReject.Contains(c.StopInformation.StopId)
                         select c).ToList<OperationListStop>();

            foreach (var stop in stops)
            {
                //get count of pending operation in stop
                var count = stop.PlannedOperation.Count(c => (!c.Status.Equals("Accepted") && !c.Status.Equals("Rejected")));
                if (count > 0)
                    isAnyPending = true;
            }
            return !isAnyPending;
        }


        /// <summary>
        /// This method gets the exact copy of the object passed.
        /// </summary>
        /// <typeparam name="T">Specifies the type of which we need object.</typeparam>
        /// <param name="source">Specifies the object that we need copy of.</param>
        /// <returns></returns>
        public static object GetCopyOfObject<T>(object source)
        {
            var jsonText = JsonConvert.SerializeObject(source);
            object copyOfObject = JsonConvert.DeserializeObject<T>(jsonText);
            return copyOfObject;
        }

        #endregion

        #region Other General Methods
        /// <summary>
        /// This function takes xml in the form of string, and deserialize the same in to operation list entity.
        /// </summary>
        /// <param name="message">XML string that need to convert in operation list entity.</param>
        /// <returns></returns>
        public static OperationList GetOperationListFromXml(string message)
        {
            OperationList operationList=null;
            try
            {
                if (string.IsNullOrEmpty(message))
                    return null;
                var ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(@message));
                var xmlReader = XmlReader.Create(ms);
                var type = typeof(OperationList);
                var xmlSerializer = new XmlSerializer(type);
                operationList = (OperationList)xmlSerializer.Deserialize(xmlReader);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsDepartureFromStop.TriggerSummaryMessage()");
            }
            Logger.LogEvent(Severity.Debug, "OperationList deserialized successfully in method GetOperationListFromXml()");
            return operationList;
        }

        #endregion

        #region View Commands

        /// <summary>
        /// This method shows the updated operation list screen, with updates from server.
        /// </summary>
        /// <param name="autoResetEvent"></param>
        public static void ShowUpdatedOperationList(AutoResetEvent autoResetEvent)
        {

            if (CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.InvokeRequired)
            {
                ThreadStart myMethod = () => ShowUpdatedOperationList(autoResetEvent);
                CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.BeginInvoke(myMethod);
                return;
            }
            BaseModule.AllowToDeactivate = false;
            PrepareListOfOperationStubs();
            ShowView<FormReceiveOperationList>(1);
            autoResetEvent.Set();

        }

        /// <summary>
        ///This method refreshes the operation list view if it is in collection of views 
        /// </summary>
        public static void RefreshOperationList()
        {

            if (CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.InvokeRequired)
            {
                ThreadStart myMethod = RefreshOperationList;
                CommunicationModel.Instance.CurrentPushMessageHandler.MainForm.BeginInvoke(myMethod);
                return;
            }

            //Check if operation list exists in collection of views
            var result = CommunicationModel.Instance.CurrentPushMessageHandler.Views[
                    "Com.Bring.PMP.PreComFW.OperationList.Views.FormOperationList"];

            if (result == null) return;
            //Create stop info
            SharedCommands.GetStopsInfo();
            if (CommunicationModel.Instance.ListClientOperationStops != null)
                result.UpdateView(null);
        }


        #endregion


    }
}

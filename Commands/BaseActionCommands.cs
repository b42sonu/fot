﻿using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Newtonsoft.Json;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class BaseActionCommands
    {
        /// <summary>
        /// Decode a barcode and determine if it is of valid type and that it is a consignment
        /// </summary>
        /// <param name="scannerOutput">Output from scanner</param>
        /// <param name="validTypes">protocols that the flow has marked as legal barcodes</param>
        /// <param name="barcodeData">The decoded barcode data (if successful) </param>
        /// <returns></returns>
        public static bool ValidateBarcode(FotScannerOutput scannerOutput, BarcodeType validTypes, out BarcodeData barcodeData)
        {
            SoundUtil.Instance.PlayScanSound();
            bool result = false;
            barcodeData = Decoder.Decode(scannerOutput);
            if (barcodeData.BarcodeType != BarcodeType.Unknown)
            {
                if ((barcodeData.BarcodeType & validTypes) != BarcodeType.Unknown)
                {
                    result = true;
                }

                // When scanning a pdf, the barcodetype will be given as consignment-item, even though the consignment is also known
                if ((validTypes & BarcodeType.ConsignmentNumber) != BarcodeType.Unknown)
                {
                    if (string.IsNullOrEmpty(barcodeData.ConsignmentId) == false)
                        result = true;
                }

            }

            return result;
        }

        /// <summary>
        /// calculates text to be used above scan field and in error message based on what is legal barcode types
        /// </summary>
        /// <param name="validBarcodeTypes"></param>
        /// <param name="scantext"></param>
        /// <returns></returns>
        public static string ScanTextFromValidBarcodeTypes(BarcodeType validBarcodeTypes, string scantext)
        {
            var currentProcess = BaseModule.CurrentFlow != null ? BaseModule.CurrentFlow.CurrentProcess : Process.Unknown;

            Logger.LogEvent(Severity.Debug, "ScanTextFromValidBarcodeTypes;validBarcodeTypes:'" + validBarcodeTypes + "' CurrentProcess:" + currentProcess );
            switch (validBarcodeTypes)
            {
                case (BarcodeType.ConsignmentItemNumber):
                    scantext = GlobalTexts.ScanOrEnterConsignmentItem;
                    break;

                case (BarcodeType.DeliveryCode | BarcodeType.ConsignmentItemOrLoadCarrier):
                    scantext = ModelUser.UserProfile.Role==Role.Landpostbud ?
                        GlobalTexts.ScanOrEnterItemOrCarrier : GlobalTexts.ScanConsignmentItemDeliveryCodeOrLoadcarrier;
                    break;

                //case (BarcodeType.ConsignmentNumber | BarcodeType.ConsignmentItemOrLoadCarrier):
                case (BarcodeType.Shipment | BarcodeType.LoadCarrier):
                    scantext = GlobalTexts.ScanConsignmentOrItemOrLoadcarrier;
                    break;

                case (BarcodeType.Shipment):
                    scantext = currentProcess == Process.ExecuteLinking ?
                        GlobalTexts.ScanPdfOrConsignmentNumber
                        : GlobalTexts.ScanOrEnterConsignmentAndItemNumber;
                    break;

                case (BarcodeType.ConsignmentItemOrLoadCarrier):
                    scantext = GlobalTexts.ScanOrEnterItemOrCarrier;
                    break;

                case (BarcodeType.OrgUnitId):
                    scantext = GlobalTexts.ScanEnterLocationId;
                    break;

                case (BarcodeType.Shelf):
                    scantext = GlobalTexts.ScanOrEnterShelf;
                    break;

                case (BarcodeType.Shelf | BarcodeType.ConsignmentItemOrLoadCarrier):
                    scantext =GlobalTexts.EnterItemCarrierOrNewShelf;
                    break;

                case (BarcodeType.LoadCarrier):
                    scantext = GlobalTexts.ScanOrEnterLoadCarrierForAssembly;
                    break;

                case (BarcodeType.RouteId | BarcodeType.Rbt):
                    scantext = GlobalTexts.ScanTripInfoOrRouteId;
                    break;

                case (BarcodeType.LoadCarrier | BarcodeType.Rbt):
                    scantext = GlobalTexts.ScanLoadCarrierAndTripId;
                    break;

                case (BarcodeType.LoadCarrier | BarcodeType.Rbt | BarcodeType.RouteId):
                    switch (currentProcess)
                    {
                        case Process.UnloadLineHaul:
                            scantext = GlobalTexts.ScanLoadCarrierAndTripId;
                            break;

                        default:
                            scantext = GlobalTexts.ScanOrEnterRbtOrPowerUnit;
                            break;
                    }
                    break;
                case (BarcodeType.Rbt):
                    switch (currentProcess)
                    {
                        case Process.UnloadPickUpTruck:
                            scantext = GlobalTexts.ScanOrEnterRbtOrPowerUnit;
                            break;

                        case Process.UnloadLineHaul:
                            scantext = GlobalTexts.ScanLoadCarrierAndTripId;
                            break;

                        case Process.LoadDistribTruck:
                        case Process.LoadLineHaul:
                            scantext = GlobalTexts.ScanOrEnterRbt;
                            break;

                        default:
                            scantext = GlobalTexts.RegisterDistribVehicle;
                            break;
                    }
                    break;
                case (BarcodeType.Shipment | BarcodeType.Rbt | BarcodeType.LoadCarrier):
                    scantext = GlobalTexts.EnterTripCarrierItemConsignment;
                    break;
            }
            return scantext;
        }

        public static bool ValidateBarcode(FotScannerOutput scannerOutput, BarcodeType validTypes, bool playSoundRequired)
        {
            bool result = false;
            if (playSoundRequired)
                SoundUtil.Instance.PlayScanSound();

            var barcodeData = Decoder.Decode(scannerOutput);
            if (barcodeData.BarcodeType != BarcodeType.Unknown)
            {
                if ((barcodeData.BarcodeType & validTypes) != BarcodeType.Unknown)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// This method gets the exact copy of the object passed.
        /// </summary>
        /// <typeparam name="T">Specifies the type of which we need object.</typeparam>
        /// <param name="source">Specifies the object that we need copy of.</param>
        /// <returns></returns>
        public static T GetCopyOfObject<T>(object source)
        {
            var jsonText = JsonConvert.SerializeObject(source);
            var copyOfObject = JsonConvert.DeserializeObject<T>(jsonText);

            return copyOfObject;
        }


    }
}

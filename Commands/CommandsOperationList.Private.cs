﻿using System;
using System.Collections.Generic;
using System.Linq;

using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    // Functions that should never be called (private) except from UnitTests
    public partial class CommandsOperationList
    {
        /// <summary>
        /// add stop in OperationList
        /// </summary>
        public static void AddStopInfo(IList<BindingStopAmphora> listClientOperationStops,
                                        OperationListStop operationListStop,
                                        List<PlannedOperationType> listPlannedOperationType)
        {
            int stopType;
            string stOperationId;
            string stOrderNumber;
            var stop = new BindingStopAmphora
            {
                StopStatus = GetStopStatus(listPlannedOperationType, out stopType, out stOperationId, out stOrderNumber),
                StopSign = Convert.ToString(stopType),
                OperationId = stOperationId,
                OrderNumber = stOrderNumber,
                AlternateStopSign = stopType == (int)OperationTypeImage.Multiple
                        ? Convert.ToString((int)OperationTypeImage.ExpandMultiple)
                        : Convert.ToString(stopType),
                OrignalSortOrder = operationListStop.StopInformation.OriginalSortNo,
            };


            if (operationListStop.StopInformation != null)
            {
                stop.StopTimeInfo = GetConcatenateTime(operationListStop.StopInformation.EarliestStopTime,
                                                       operationListStop.StopInformation.LatestStopTime);
                stop.StopEarliestTime = operationListStop.StopInformation.EarliestStopTime;
            }

            bool hasOneOperations = operationListStop.PlannedOperation != null && operationListStop.PlannedOperation.Length == 1;
            PlannedOperationType plannedOperation = null;
            if (hasOneOperations)
                plannedOperation = operationListStop.PlannedOperation[0];


            if (stopType != (int)OperationTypeImage.Multiple && hasOneOperations && plannedOperation != null && stop.StopTimeInfo == string.Empty)
            {
                stop.StopTimeInfo = GetConcatenateTime(plannedOperation.EarliestStartTime,
                                                       plannedOperation.LatestStartTime);
                stop.StopEarliestTime = plannedOperation.EarliestStartTime;
            }

            if (operationListStop.StopInformation != null && string.IsNullOrEmpty(operationListStop.StopInformation.StopName) == false)
            {
                stop.StopTimeInfo += " " + operationListStop.StopInformation.StopName;
            }
            else if (hasOneOperations && plannedOperation != null && plannedOperation.OperationType == PlannedOperationTypeOperationType.DELIVERY)
            {
                stop.StopTimeInfo += " " + plannedOperation.GetConsigneeName();
            }

            if (operationListStop.StopInformation != null)
            {
                AddressType stopAddress = operationListStop.StopInformation.Address;
                stop.StopType = operationListStop.StopInformation;
                if (stopAddress != null && string.IsNullOrEmpty(stopAddress.StreetName) == false)
                    stop.StopAddress = stopAddress.StreetName + "," + stopAddress.PostalCode + " " +
                                                        stopAddress.PostalName;

                else if (hasOneOperations && plannedOperation != null && (plannedOperation.OperationType == PlannedOperationTypeOperationType.DELIVERY || plannedOperation.OperationType == PlannedOperationTypeOperationType.RETURNEDDELIVERY))
                {
                    stop.StopAddress = plannedOperation.GetConsigneeBothAddress();
                }
            }

            if (operationListStop.StopInformation != null)
                stop.StopId = operationListStop.StopInformation.StopId;


            //added for sorting..
            if (operationListStop.PlannedOperation != null && operationListStop.PlannedOperation.Any())
            {
                if (operationListStop.PlannedOperation[0].PlannedConsignments != null && operationListStop.PlannedOperation[0].PlannedConsignments.Any())
                    stop.Consignee =
                        operationListStop.PlannedOperation[0].PlannedConsignments[0].Consignee;
            }


            listClientOperationStops.Add(stop);

        }

        public static string GetConcatenateTime(DateTime earliestTime, DateTime latestTime)
        {
            string time = string.Empty;
            if (earliestTime.Date != DateTime.MinValue.Date || latestTime.Date != DateTime.MinValue.Date)
            {
                if (earliestTime.Date != DateTime.MinValue.Date)
                    time += String.Format("{0:t}", earliestTime);
                if (latestTime.Date != DateTime.MinValue.Date)
                    time += string.IsNullOrEmpty(time)
                               ? String.Format("{0:t}", latestTime)
                               : "-" +
                                 String.Format("{0:t}", latestTime);
            }
            return time;
        }


        private static List<PlannedOperationType> GetListOfPlannedOperation(OperationListStop operationListStop, bool isFilter)
        {
            List<PlannedOperationType> listPlannedOperations;
            if (isFilter)
                listPlannedOperations = (from n in operationListStop.PlannedOperation
                                         where StopOperationType.Finished.Contains(n.Status)
                                         select n).ToList<PlannedOperationType>();
            else
                listPlannedOperations = (from n in operationListStop.PlannedOperation
                                         select n).ToList<PlannedOperationType>();

            return listPlannedOperations;
        }

        /// <summary>
        /// method for get stop status and stop type and operation id
        /// </summary>
        public static int GetStopStatus(List<PlannedOperationType> listPlannedOperationType, out int stopType, out string operationId, out string orderNumber)
        {
            //Logger.LogEvent(Severity.Debug, "Executing command ActionOperationListCommands.GetStopStatus(List<PlannedOperationType> listPlannedOperationType, out string stStopType, out string operationId, out string orderId)");
            int iStopStatus;   //0 for completed
            operationId = String.Empty;
            orderNumber = String.Empty;

            if (listPlannedOperationType.Count == 1)
            {
                PlannedOperationType plannedOperation = listPlannedOperationType[0];
                stopType = OperationCommands.GetOperationType(plannedOperation.OperationType);
                iStopStatus = OperationCommands.PlannedOperationStatus(plannedOperation.Status);
                operationId = plannedOperation.OperationId;

                /*if (stopType == (int)OperationTypeImage.PickUp)
                {
                    if (CheckPickUpConsignmentOrderId(plannedOperation) > 1)
                    {
                        stopType = (int)OperationTypeImage.Multiple;
                        operationId = String.Empty;
                    }
                    else
                    {
                        if (plannedOperation.PlannedConsignments != null && plannedOperation.PlannedConsignments[0] != null)
                        {
                            var plannedConsignment = plannedOperation.PlannedConsignments[0];
                            orderNumber = plannedConsignment.OrderNumber;
                        }
                    }
                }*/
            }
            else
            {
                stopType = (int)OperationTypeImage.Multiple;
                iStopStatus = GetStopStatus(listPlannedOperationType);

            }

            return iStopStatus;
        }


        internal static int GetStopStatus(List<PlannedOperationType> listPlannedOperationType)
        {
            const int stopStatus = (int)StopOperationStatusImage.NotStarted;
            if (listPlannedOperationType != null)
            {
                //Get count of completed operations
                var count = listPlannedOperationType.Count(n => n.Status != null && n.Status == StopOperationType.Finished);
                //If all operations completed then return stop status as completed
                if (count == listPlannedOperationType.Count)
                    return (int)StopOperationStatusImage.Completed;


                //get count of started or finished operations
                count = listPlannedOperationType.Count(n => n.Status != null && (n.Status == StopOperationType.Started || n.Status == StopOperationType.Finished));
                //If there is minimum one started or completed operation then stop status will be started one
                if (count > 0)
                    return (int)StopOperationStatusImage.Started;
            }
            return stopStatus;
        }
        /// <summary>
        /// method for load list stop status...
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        internal static int LoadListStopStatus(DeliveryStatus status)
        {
            var loadStopStatus = (int)StopOperationStatusImage.NotStarted;
            switch (status)
            {
                case DeliveryStatus.New:
                    loadStopStatus = (int)StopOperationStatusImage.NotStarted;
                    break;
                case DeliveryStatus.Started:
                    loadStopStatus = (int)StopOperationStatusImage.Started;
                    break;
                case DeliveryStatus.Finished:
                    loadStopStatus = (int)StopOperationStatusImage.Completed;
                    break;
            }
            return loadStopStatus;
        }

        internal static int GetStopStatus(string stopId)
        {
            List<PlannedOperationType> listPlannedOperationType = null;
            var operationList = ModelOperationList.OperationList;
            if (operationList != null && operationList.Stop != null)
            {
                OperationListStop operationListStop = operationList.GetStop(stopId);
                if (operationListStop != null)
                    listPlannedOperationType = GetListOfPlannedOperation(operationListStop, false);
            }
            return GetStopStatus(listPlannedOperationType);
        }

        /// <summary>
        /// this method used if operation type is pickup for checking is this operation is with different order number consignment
        /// </summary>
        /// <param name="plannedOperation"></param>
        /// <returns></returns>
        public static int CheckPickUpConsignmentOrderId(PlannedOperationType plannedOperation)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionOperationListCommands.CheckPickUpConsignmentOrderId()");
            List<string> listOrderNumber = null;

            if (plannedOperation != null && plannedOperation.PlannedConsignments != null)
            {
                listOrderNumber = new List<string>();
                foreach (PlannedConsignmentsType plannedConsignmentsType in plannedOperation.PlannedConsignments)
                {
                    if (!String.IsNullOrEmpty(plannedConsignmentsType.OrderNumber) && !listOrderNumber.Contains(plannedConsignmentsType.OrderNumber))
                        listOrderNumber.Add(plannedConsignmentsType.OrderNumber);
                }
            }
            return listOrderNumber == null ? 0 : listOrderNumber.Count;
        }

        #region "Methods used for showing count in Amphora operation list view"
        public static int GetNotCompletedOperationCount()
        {
            int count = 0;
            var operationList = ModelOperationList.OperationList;
            if (operationList != null && operationList.Stop != null)
                count =
                    operationList.Stop.Sum(
                        stop =>
                        stop.StopInformation != null &&
                        (stop.PlannedOperation != null &&
                         GetStopStatus(stop.StopInformation.StopId) != (int)StopOperationStatusImage.Completed)
                            ? stop.PlannedOperation.Count() : 0);

            //for add load list operation..
            var operations = GetDeliveryOperationOnTabBase(OperationListTab.NotStarted);
            if (operations != null)
                count += operations.Count();

            return count;
        }
        public static int GetCompletedOperationCount()
        {
            int count = 0;
            var operationList = ModelOperationList.OperationList;
            if (operationList != null && operationList.Stop != null)
                count =
                    operationList.Stop.Sum(
                        stop =>
                        stop.StopInformation != null && (stop.PlannedOperation != null && GetStopStatus(stop.StopInformation.StopId) == (int)StopOperationStatusImage.Completed) ? stop.PlannedOperation.Count(
                            operation => StopOperationType.Finished.Contains(operation.Status)) : 0);
            //for add load list operation..
            var operations = GetDeliveryOperationOnTabBase(OperationListTab.Completed);
            if (operations != null)
                count += operations.Count();
            return count;
        }
        public static int GetAllOperationCount()
        {
            int count = 0;
            var operationList = ModelOperationList.OperationList;
            if (operationList != null && operationList.Stop != null)
                count = operationList.Stop.Sum(stop => stop.PlannedOperation != null ? stop.PlannedOperation.Count() : 0);
            //for add load list operation..
            var operations = GetDeliveryOperationOnTabBase(OperationListTab.All);
            if (operations != null)
                count += operations.Count();
            return count;
        }
        #endregion
    }

}
﻿using System;
using System.Threading;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class DriverAvailability : IDisposable
    {
        // TM_OTC
        public const string InvalidUserId = "507";
        public const string InvalidAssetOrSessionId = "2003";
        public const string MissingSessionId = "2005";


        // ReSharper disable InconsistentNaming
        private const string Ok_Amphora = "OK";
        private const string Ok_Alystra = "Ok";

        private const string Error_LoginAmphora = "FEIL";


        private const string LogoutOk = "LogoutOk";

        private const string Error_AssetIdAlreadyInUse = "UNIT_BUSY";
        private const string Error_IllegalAsset = "UNIT_DOES_NOT_EXIST";
        private const string Error_AlreadyLoggedout = "UNIT_ALREADY_LOGGED_OUT";
        // ReSharper restore InconsistentNaming

        public static Action<MessageState, string> ShowGuiMessageHandler { get; set; }
        private static Action _logoutCallback;


        private readonly UserProfile _userProfile;

        public DriverAvailability(UserProfile userProfile)
        {
            _userProfile = userProfile;
        }

        public DriverAvailability(UserProfile userProfile, Action<MessageState, string> showGuiMessageHandler)
            : this(userProfile)
        {
            // Add handler so driver avialibiity can update message in this form
            ShowGuiMessageHandler = showGuiMessageHandler;
        }

        public DriverAvailability(UserProfile userProfile, Action logoutCallback)
            : this(userProfile)
        {
            // Add handler so driver avialibiity can update message in this form
            _logoutCallback = logoutCallback;
        }

        

        private void ShowPopupMessage(MessageState messageState, string message)
        {
            Severity severity;

            switch (messageState)
            {
                case MessageState.Information:
                    severity = Severity.Info;
                    break;

                case MessageState.Warning:
                    severity = Severity.Warning;
                    break;

                default:
                    severity = Severity.Error;
                    break;
            }

            GuiCommon.ShowModalDialog(GlobalTexts.DriverAvailability, message, severity, GlobalTexts.Ok);
        }


        private void ShowGuiMessage(MessageState messageState, string message)
        {
            ShowGuiMessage(messageState, message, false);
        }

        private void ShowGuiMessage(MessageState messageState, string message, bool noFallback)
        {
            if (ShowGuiMessageHandler != null)
            {
                Logger.LogEvent(Severity.Debug, "Showing GUI message '{0}' from DriverAvailability", message);
                ShowGuiMessageHandler(messageState, message);
            }
            else if (noFallback == false)
            {
                ShowPopupMessage(messageState, message);
            }
        }

        /// <summary>
        /// send tms request when changing location from settings menu
        /// </summary>
        public bool UpdateStatus()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.UpdateStatus");

                //Log off if alreday logged in
                bool wasLoggedIn = ModelUser.IsLoggedInToTms;
                if (wasLoggedIn)
                {
                    if (Logout(true) == false)
                        return false;
                }

                if (wasLoggedIn)
                {
                    // Wait to give async logout-request a headstart on the login request
                    Thread.Sleep(1200);
                }
                //send request for logon
                Login();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "DriverAvailability.UpdateStatus");
                return false;
            }
            return true;
        }



        // Can have only one subscriber at any time
        public void DesubscribeToTmsMessages()
        {
            // Remove message subscription
            ShowGuiMessageHandler = null;
        }


        public bool UpdateTmsAvailabilityStatus()
        {
            Logger.LogEvent(Severity.Debug, "Executing function FormChangeProfile.UpdateTmsAvailabilityStatus");
            bool result = true;
            try
            {
                if (ShouldLoginToTmsWithDriverAvailability())
                    Login();
                else if (ShouldLogoffFromTmsWithDriverAvailability())
                    result = Logout(true);
                else if (ShouldUpdateDriverAvailability())
                    result = UpdateStatus();
                else
                {
                    Logger.LogEvent(Severity.Debug, "Detected no change in TMS status neccesary");
                }
            }
            catch (Exception ex)
            {
                result = false;
                Logger.LogExpectedException(ex, "FormChangeProfile.SendRequestToTMS");
            }

            return result;
        }

        private bool ShouldLoginToTmsWithDriverAvailability()
        {
            // If we have no old session, but user role contains power unit, we log on
            if (ModelUser.IsLoggedInToTms == false && _userProfile.IsDriverAvailabilityUser)
            {
                Logger.LogEvent(Severity.Debug, "Detected we need to login to TMS");
                return true;
            }

            return false;
        }

        private bool ShouldLogoffFromTmsWithDriverAvailability()
        {
            // Check if we have session to log-off
            if (ModelUser.IsLoggedInToTms)
            {
                // Check if new role has power unit
                if (_userProfile.IsDriverAvailabilityUser == false)
                {
                    Logger.LogEvent(Severity.Debug, "Detected we need to logoff from TMS");
                    return true;
                }
            }

            return false;
        }

        private bool ShouldUpdateDriverAvailability()
        {
            // If we are not logged in and changing to no-tms, then no need to update 
            if (_userProfile.IsDriverAvailabilityUser)
            {
                // If we have changed power unit or tms
                if (IsPowerUnitChanged() || IsTmsAffilationChanged() || IsOrgUnitChanged())
                {
                    Logger.LogEvent(Severity.Debug, "Detected we need to update status in TMS as PowerUnit/TmsAffiliation/OrgUnit has changed");
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///  Check if power unit has changed since last login
        /// </summary>
        private bool IsPowerUnitChanged()
        {
            Logger.LogEvent(Severity.Debug, "Executing function FormChangeProfile.IsPowerUnitChanged");
            bool isPowerUnitChanged = ModelUser.UserProfile.PowerUnit != _userProfile.PowerUnit;

            return isPowerUnitChanged;
        }

        /// <summary>
        ///  Check if tms affiliation  has changed since last login
        /// </summary>
        private bool IsTmsAffilationChanged()
        {
            Logger.LogEvent(Severity.Debug, "Executing function FormChangeProfile.IsTmsAffilationChanged");

            bool isTmsAffilationChanged = ModelUser.UserProfile.TmsAffiliation != _userProfile.TmsAffiliation;

            return isTmsAffilationChanged;
        }

        /// <summary>
        ///  Check if org unit has changed since last login
        /// </summary>
        private bool IsOrgUnitChanged()
        {
            // Only viable for Amphora
            bool isOrgUnitChanged = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora && ModelUser.UserProfile.OrgUnitId != _userProfile.OrgUnitId;

            return isOrgUnitChanged;
        }

        /// <summary>
        /// This method will create and send the request to log on to the TMS
        /// </summary>
        public void Login()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.Login()");
                if (_userProfile != null) // user has logged off, no request can or should be sent
                {
                    if (_userProfile.IsDriverAvailabilityUser)
                    {
                        T20210_DriverAvailabilityToTMSRequest tmsRequest = GetDriverAvailabilityLogonRequest(false);
                        CommunicationClient.Instance.SendDriverAvailabilityRequest(tmsRequest);
                        CommunicationClient.Instance.HasAttemptedToLogInToTMS = true;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "DriverAvailability.SendDriverAvailableToTmsDependingOnRole");
            }
        }

        /// <summary>
        /// This method will create and send the request to logoff from the TMS
        /// </summary>
        public bool Logout(bool validateLogoutWithUser)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.Logout");
                if (ModelUser.IsLoggedInToTms)
                {
                    if (validateLogoutWithUser && CanLogoffFromTms() == false)
                        return false;

                    CommunicationClient.Instance.HasAttemptedToLogInToTMS = false;
                    return LogoffFromTms(ModelUser.SessionId, true);
                }
                return true;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "DriverAvailability.SendDriverAvailableToTmsDependingOnRole");
                return false;
            }
        }


        /// <summary>
        /// If previous user was not logged out succesfully (i.e. crash), we log out his session here
        /// </summary>
        public void LogoffHangingSession()
        {
            string sessionId = ModelUser.GetSavedSessionId();

            //If TMS is Alystra then just clear operations as it is necessary without depending on session Id.
            if (ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra)
            {
                CommandsOperationList.ClearOperations();
            }

            if (sessionId != string.Empty)
            {
                Logger.LogEvent(Severity.Warning, "Logging off hanging session '{0}'", sessionId);

                LogoffFromTms(sessionId, false);
            }
        }


        private bool LogoffFromTms(string sessionId, bool clearOperations)
        {
            try
            {
                if (string.IsNullOrEmpty(sessionId))
                    Logger.LogEvent(Severity.Error, "Tried to log off with empty session id");
                else
                {
                    // Logout async
                    var tmsRequest = GetDriverAvailabilityLogoffRequest(sessionId);
                    if (tmsRequest != null && CommunicationClient.Instance.SendDriverAvailabilityRequest(tmsRequest) == false)
                        return false;

                    ModelUser.ClearSessionId();

                    if (clearOperations)
                        CommandsOperationList.ClearOperations();

                    ShowGuiMessage(MessageState.Information, GlobalTexts.UserLoggedOffFromTms, true);
                }
                return true;
            }

            catch (Exception exception)
            {
                Logger.LogException(exception, "DriverAvailability.LogoffFromTms");
                return false;
            }
        }

        //In case of User wants to Loggoff from TMS for particular asset, then we need 
        //to check whether he has some pending tasks and if yes, then need to confirm from user
        private bool CanLogoffFromTms()
        {
            bool result = true;

            Logger.LogEvent(Severity.Debug, "Checking if user has pending tasks and can not logoff TMS");

            if (CommandsOperationList.IsOperationListContainingUnfinishedOperations())
            {
                result = AskMessageBoxQuestion(GlobalTexts.PendingTasksMessage);
            }
            if (result && FileUtil.LoadListExist())
            {
                result = AskMessageBoxQuestion(GlobalTexts.PendingTasksLoadListMessage);
                if (result)
                {
                    //Delete LoadList.xml
                    CommandsOperationList.ClearLoadList();
                }
            }
            return result;
        }


        /// <summary>
        /// This method shows pop up for driver availability response
        /// </summary>
        /// <returns>Returns true if every thing is fine.</returns>
        public void HandleDriverAvailabilityResponse(DriverAvailabilityResponse response)
        {
            if (response.SessionOvertaken != null && response.SessionOvertaken.SessionOvertaken)
            {
                HandleSessionOvertaken(response);
            }
            else
            {
                //CR 154 Fixes
                string state = response.State.Trim();
                switch (state)
                {
                    case Error_AssetIdAlreadyInUse:
                        HandleAssetInUse();
                        break;

                    case Error_IllegalAsset:
                        HandleIllegalAsset();
                        break;

                    case Ok_Amphora:
                    case Ok_Alystra:
                        HandleLoginOk(response.SessionId, response.TMSLogOnTime);
                        //OnPropertyChanged("Icon");
                        break;

                    case Error_LoginAmphora:
                        HandleLoginError(response.ErrorMessage);
                        break;

                    case LogoutOk:
                        HandleLogout();
                        //OnPropertyChanged("Icon");
                        break;

                    case Error_AlreadyLoggedout:
                        Logger.LogEvent(Severity.Warning, "Session '{0}' was already logged out");
                        HandleLogout();
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "Unexpected asyncronous driveravailaibity message received. State: {0}  Text: {1}",
                                        response.State, response.ErrorMessage);
                        break;
                }
            }
        }


        private void HandleLoginOk(string sessionId, DateTime logonTime)
        {
            if (sessionId != ModelUser.SessionId)
            {
                if (ModelUser.SetSessionId(sessionId, logonTime))
                    ShowGuiMessage(MessageState.Information, GlobalTexts.LoginToTmsSucceded);
            }
        }

        private void HandleLoginError(string errorMessage)
        {
            // No code for illegal asset in Amphora, so have to decide based on error message
            if (errorMessage.StartsWith("Transportbærer") && errorMessage.EndsWith("finnes ikke i Amphora"))
                HandleIllegalAsset();
            else
            {
                ShowGuiMessage(MessageState.Error, GlobalTexts.LoginToTmsFailed + errorMessage);
            }
        }


        private void HandleIllegalAsset()
        {
            string message = GlobalTexts.InvalidPowerUnit + ": " + _userProfile.PowerUnit;
            ShowGuiMessage(MessageState.Error, message);
            ModelMain.DoLaunchSettingsForm();
        }

        private void HandleLogout()
        {
            if (_logoutCallback != null)
                _logoutCallback();
        }

        private void HandleAssetInUse()
        {
            if (ShouldOverrideAsset())
            {
                OverrideAsset();
            }
        }

        private void HandleSessionOvertaken(DriverAvailabilityResponse response)
        {
            // Only respond to session overtaken messages sent out after we logged on
            if (response.TMSLogOnTime > ModelUser.SessionLogonTime)
            {
                ModelUser.ClearSessionId();
                SoundUtil.Instance.PlayNewMessageSound();

                ShowGuiMessage(MessageState.Information, GlobalTexts.NoAssociatedPowerunit);
            }
        }


        private void OverrideAsset()
        {
            Logger.LogEvent(Severity.Info, "Overriding asset " + _userProfile.PowerUnit);

            var tmsRequest = GetDriverAvailabilityLogonRequest(true);
            CommunicationClient.Instance.SendDriverAvailabilityRequest(tmsRequest);
        }


        /// <summary>
        /// This function will prepare request object for sending DriverAvailability to TMS.
        /// </summary>
        /// <param name="overrideLoadCarrier">bool status which specifies, that user wants to override the load carrier or not.</param>
        public T20210_DriverAvailabilityToTMSRequest GetDriverAvailabilityLogonRequest(bool overrideLoadCarrier)
        {
            return CreateDriverAvailabilityRequest(_userProfile, true, overrideLoadCarrier, null);
        }

        private T20210_DriverAvailabilityToTMSRequest CreateDriverAvailabilityRequest(UserProfile userProfile, bool available, bool overrideLoadCarrier, string sesssionId)
        {
            string heartbeatInterval = "0";
            if(available)
                heartbeatInterval = SettingDataProvider.Instance.GetTmiValue<string>(TmiSettings.HeartbeatInterval, userProfile.RoleName, userProfile.TmsAffiliation);

            var request = new T20210_DriverAvailabilityToTMSRequest
            {
                UserLogonId = userProfile.UserId,
                UserRoleId = userProfile.RoleName,
                CountryCode = userProfile.CountryCode,
                OrgUnitId = userProfile.OrgUnitId,
                PostalCode = userProfile.PostalCode,
                TMSAffiliationId = userProfile.TmsAffiliation,
                LoadCarrierId = userProfile.LoadCarrier,
                PowerLoadCarrierId = userProfile.PowerUnit,
                HeartbeatIntervalMinutes = heartbeatInterval,
                Available = available,
                Override = overrideLoadCarrier,
                CaptureEquipmentId = ModelModules.Platform.Hardware.Tag,
                SessionId = sesssionId,
            };
            return request;
        }

        public T20210_DriverAvailabilityToTMSRequest GetDriverAvailabilityLogoffRequest(string sessionId)
        {
            T20210_DriverAvailabilityToTMSRequest request = null;

            if (string.IsNullOrEmpty(sessionId) == false)
            {
                if (ModelUser.UserProfile != null)
                    request = CreateDriverAvailabilityRequest(ModelUser.UserProfile, false, false, sessionId);
            }
            else
            {
                Logger.LogEvent(Severity.Error, "Tried to create logoff request with empty session id");
            }

            return request;
        }


        private bool ShouldOverrideAsset()
        {
            bool shouldOverrideAsset = false;
            Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.ShouldOverrideAsset");

            if (ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra)
            {
                if (string.IsNullOrEmpty(ModelUser.SessionId))
                    ShowGuiMessage(MessageState.Error, GlobalTexts.AssetCanNotBeOvertaken);
            }
            else
            {
                if (AskMessageBoxQuestion(GlobalTexts.DriverOverridden))
                {
                    shouldOverrideAsset = true;
                }
            }

            return shouldOverrideAsset;
        }

        private static bool AskMessageBoxQuestion(string question)
        {
            var messageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.DriverAvailability, question, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
            return messageBoxResult == GlobalTexts.Yes;
        }


        #region "Methods for 62 user story"
        public bool CanChangeDriverAvailability()
        {
            return CanIgnorePendingOperationlistTasks() && CanIgnorePendingLoadlistTasks();

        }
        private bool CanIgnorePendingOperationlistTasks()
        {
            Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.HasPendingOperations");
            bool result = true;
            if (CommandsOperationList.IsOperationListContainingUnfinishedOperations())
            {
                if (AskMessageBoxQuestion(GlobalTexts.PendingTasksOperationListMessage) == false)
                {
                    result = false;
                    ActivateOperationListModule();
                }
            }

            return result;
        }

        private bool CanIgnorePendingLoadlistTasks()
        {
            Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.HasPendingOperationsInLoadList");
            bool result = true;
            if (FileUtil.LoadListExist())
            {
                if (AskMessageBoxQuestion(GlobalTexts.PendingTasksLoadListMessage) == false)
                {
                    result = false;
                    ActivateLoadListModule();
                }
            }

            return result;
        }


        public void ActivateOperationListModule()
        {
            Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.ActivateOperationListModule");
            ModelModules.OperationListModule.DisplayModule();
        }

        public void ActivateLoadListModule()
        {
            Logger.LogEvent(Severity.Debug, "Executing function DriverAvailability.ActivateLoadListModule");
            var flowDataLoadList = new FlowDataLoadList { HeaderText = GlobalTexts.LoadList };
            var loadListFactory = ModelMain.FlowFactories[FlowType.LoadList];
            var flowLoadList = loadListFactory.CreateFlow(FlowType.LoadList);

            if (ModelMain.Modules.ContainsKey(ModuleType.Goods))
            {
                BaseModule goodsModule = ModelMain.Modules[ModuleType.Goods];
                if (goodsModule != null)
                {
                    goodsModule.DoSupressDefaultFlowForModule = true;
                    goodsModule.InitialFlowData = flowDataLoadList;
                    goodsModule.InitialFlow = flowLoadList;
                    goodsModule.InitialFlowProcess = BaseModule.CurrentFlow.CurrentProcess;
                    goodsModule.DisplayModule();
                }
            }
        }
        #endregion

        public void Dispose()
        {
            ShowGuiMessageHandler = null;
        }
    }
}
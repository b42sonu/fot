﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using PreCom.Core.Communication;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount;
using Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class FlowDataHandleScannedBarcode : BaseFlowData
    {
        public ConsignmentEntity ConsignmentEntity;
        public EntityMap EntityMap;
        public PlannedConsignmentsType[] PlannedConsignments;
        public string PowerUnit;

        //both two properties mainly added fro unplanned pickup process..
        public string ExternalStopId;
        public string ExternalTripId;
        public LocationElement Location;
        public WorkListItemType CurrentWorkListItemType { get; set; }
    }

    public class FlowResultHandleScannedBarcode : BaseFlowResult
    {
    }

    public class CommandsHandleScannedBarcodes
    {
        public FlowResultHandleScannedBarcode FlowResult;
        public bool IsNotToSendPowerUnitInGoodsEvent { get; set; }

        public CommandsHandleScannedBarcodes()
        {
            // Separated out of constructor to be callable from unit test
            Init();
        }
        public CommandsHandleScannedBarcodes(bool isNotToSendPowerUnitInGoodsEvent)
        {
            IsNotToSendPowerUnitInGoodsEvent = isNotToSendPowerUnitInGoodsEvent;
            // Separated out of constructor to be callable from unit test
            Init();
        }

        public void Init()
        {
            FlowResult = new FlowResultHandleScannedBarcode();
        }

        /// <summary>
        /// Returns true if there are any planned consignments in the operation
        /// </summary>
        public bool ActionHasPlannedConsignments(PlannedConsignmentsType[] selectedConsignments)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.ActionHasPlannedConsignments()");
            bool result = false;

            if (selectedConsignments != null)
            {
                if (selectedConsignments.Any(consignment => string.IsNullOrEmpty(consignment.ConsignmentNumber) == false))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the scanned consignmentEntity was planned
        /// </summary>
        public bool IsScannedConsignmentPlanned(ConsignmentEntity consignmentEntity, PlannedConsignmentsType[] plannedConsignments)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsScannedConsignmentPlanned()");
            bool result = false;
            try
            {
                if (ActionHasPlannedConsignments(plannedConsignments))
                {
                    string consignmentId = consignmentEntity.ConsignmentId;
                    result = OperationCommands.GetPlannedConsignment(consignmentId, plannedConsignments) != null;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "CommandsHandleScannedBarcodes.IsScannedConsignmentPlanned");
            }
            return result;
        }

        /// <summary>
        /// Returns true if user needs to verify an unplanned consignmentEntity
        /// </summary>
        public bool IsVerificationOfUnplannedConsignmentsRequired(Process process)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsVerificationOfUnplannedConsignmentsRequired()");
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsVerificationOfUnplannedConsignmentsRequired);
        }



        public bool StoreScannedConsignmentEntity(EntityMap entityMap, ConsignmentEntity consignmentEntity)
        {
            try
            {
                consignmentEntity.EntityStatus |= EntityStatus.Scanned;
                entityMap.Store(consignmentEntity);
                FlowResult.State = FlowResultState.Ok;
            }
            catch (Exception e)
            {
                FlowResult.State = FlowResultState.Error;
                FlowResult.Message = GlobalTexts.FailedSavingScanning;
                Logger.LogException(e, "StoreScannedConsignmentEntity");
            }
            return FlowResult.State == FlowResultState.Ok;
        }

        /// <summary>
        /// Returns true if user must verify items in consignmentEntity
        /// </summary>
        public bool IsConsignmentItemCountVerificationRequired()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsConsignmentItemCountVerificationRequired()");
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsConsignmentItemCountVerificationRequired);
        }


        /// <summary>
        /// Let user verify if he really wants to do an operation on an unplanned consignmentEntity
        /// </summary>
        /// <returns></returns>
        public bool VerifyUnplannedConsignment(Consignment consignment)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.VerifyUnplannedConsignment()");
            bool result;

            if (consignment.HasStatus(EntityStatus.ValidatedUnplannedConsignment))
            {
                result = true;
            }
            else
            {
                string messageText = GlobalTexts.VerifyScannedUnplannedConsignment;

                var messageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Attention, messageText, Severity.Warning,
                                                                GlobalTexts.Cancel, GlobalTexts.Ok);

                if (messageBoxResult == GlobalTexts.Ok)
                {
                    FlowResult.State = FlowResultState.Ok;
                    consignment.EntityStatus |= EntityStatus.ValidatedUnplannedConsignment;
                }
                else
                {
                    FlowResult.State = FlowResultState.Cancel;
                    FlowResult.Message = GlobalTexts.HandleScannedUnplannedConsignmentAborted;
                }
                result = FlowResult.State == FlowResultState.Ok;
            }
            return result;
        }


        public static bool ShowPopUpForShouldNotBeLoadedOrUnloaded(ConsignmentEntity consignmentEntity, EntityMap entityMap)
        {
            Consignment consignment;

            if (consignmentEntity == null)
                return false;

            if (consignmentEntity is ConsignmentItem)
            {
                var consignmentItem = consignmentEntity as ConsignmentItem;
                consignment = entityMap.GetConsignment(consignmentItem);
            }
            else
                consignment = entityMap.GetConsignment(consignmentEntity.ConsignmentId);


            string message;



            if (consignmentEntity is ConsignmentItem)
            {
                if (BaseModule.CurrentFlow.CurrentProcess == Process.LoadLineHaul || BaseModule.CurrentFlow.CurrentProcess == Process.LoadDistribTruck)
                    message = string.Format(GlobalTexts.GoodsShouldNotbeLoaded,
                                           GlobalTexts.ConsignmentItem + " " + consignmentEntity.CastToConsignmentItem().ItemDisplayId);
                else
                    message = string.Format(GlobalTexts.GoodsShouldNotbeUnloaded,
                                           GlobalTexts.ConsignmentItem + " " + consignmentEntity.CastToConsignmentItem().ItemDisplayId);
            }
            else
            {
                if (BaseModule.CurrentFlow.CurrentProcess == Process.LoadLineHaul || BaseModule.CurrentFlow.CurrentProcess == Process.LoadDistribTruck)
                    message = string.Format(GlobalTexts.GoodsShouldNotbeLoaded,
                                           GlobalTexts.Consignment + " " + consignmentEntity.CastToConsignmentItem().ItemDisplayId);
                else
                    message = string.Format(GlobalTexts.GoodsShouldNotbeUnloaded,
                                      GlobalTexts.Consignment + " " + consignmentEntity.ConsignmentDisplayId);
            }


            if (consignment == null)
            {
                //show confirmation pop up for unloading
                var result = GuiCommon.ShowModalDialog(GlobalTexts.Warning, message, Severity.Warning, GlobalTexts.Ok, GlobalTexts.Cancel);
                return result == GlobalTexts.Ok;
            }
            GuiCommon.ShowModalDialog(GlobalTexts.Warning, message, Severity.Warning, GlobalTexts.Ok);
            return true;
        }

        /// <summary>
        /// Register the scanned consignmentEntity item with LM
        /// 
        /// Eventcode: A: pickup, 6A: Unloading, Loading: 3A, Pickup: I
        /// </summary>
        public void SendRegisterScanningEvent(ConsignmentEntity consignmentEntity, OperationProcess operationProcess)
        {
            SendRegisterScanningEvent(consignmentEntity, operationProcess, null, null, null, null);
        }

        /// <summary>
        /// Register the scanned consignmentEntity item with LM
        /// 
        /// Eventcode: A: pickup, 6A: Unloading, Loading: 3A, Pickup: I
        /// this method mainly created for unplanned pickup for passing external trip and stop id
        /// </summary>
        public void SendRegisterScanningEvent(ConsignmentEntity consignmentEntity, OperationProcess operationProcess, string stopId, string tripId, string eventCode, LocationElement location)
        {
            try
            {
                var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity);
                if (goodsEvent != null)
                {

                    if (string.IsNullOrEmpty(stopId) == false)
                        goodsEvent.OperationInformation.StopId = stopId.ToUpper();
                    if (string.IsNullOrEmpty(tripId) == false)
                        goodsEvent.OperationInformation.TripId = tripId.ToUpper();

                    if (!string.IsNullOrEmpty(eventCode))
                        goodsEvent.EventInformation.EventCode = eventCode;

                    if (!string.IsNullOrEmpty(operationProcess.PowerUnitId) && !IsNotToSendPowerUnitInGoodsEvent)
                    {
                        goodsEvent.OperationInformation.PowerUnitId = operationProcess.PowerUnitId.ToUpper();
                    }

                    if (IsNotToSendPowerUnitInGoodsEvent)
                        goodsEvent.OperationInformation.PowerUnitId = string.Empty;

                    if (location != null)
                    {
                        if (string.IsNullOrEmpty(location.Number) == false)
                            goodsEvent.LocationInformation.EventOrgUnitId = location.Number;
                        goodsEvent.LocationInformation.EventPostalCode = location.PostalNumber;
                    }


                    var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GoodsEventsEventInformation, consignmentEntity.ConsignmentId);

                    CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);

                    consignmentEntity.GoodsEvent = goodsEvent;
                    consignmentEntity.ConsignmentInstance.EntityStatus |= EntityStatus.GoodsEventSent;
                    FlowResult.State = FlowResultState.Ok;
                }
            }

            catch (PreCom.Core.NotLoggedInExeption ex)
            {
                Logger.LogExpectedException(ex, "CommandsHandleScannedBarcodes.SendRegisterScanningEvent");
            }
            catch (Exception ex)
            {
                FlowResult.State = FlowResultState.Error;
                Logger.LogException(ex, "CommandsHandleScannedBarcodes.SendRegisterScanningEvent");
            }

        }


        public void SendGoodsEventForCombinationTrip(ConsignmentEntity consignmentEntity, CombinationTrip combinationTrip, string validationEventCode)
        {
            var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity);
            if (goodsEvent != null)
            {
                //Set event code returned from LMK
                if (goodsEvent.EventInformation != null)
                    goodsEvent.EventInformation.EventCode = string.IsNullOrEmpty(validationEventCode) ? "2T" : validationEventCode;

                if (goodsEvent.OperationInformation != null)
                {
                    //Set details related to load line haul
                    if (combinationTrip != null && combinationTrip.LoadLineHaulWli != null)
                    {
                        goodsEvent.OperationInformation.TripId = combinationTrip.LoadLineHaulWli.TripId;
                        goodsEvent.OperationInformation.RouteId = combinationTrip.LoadLineHaulWli.RouteId;
                    }

                    //Set details related to load distrib.
                    if (combinationTrip != null && combinationTrip.LoadDistribWli != null)
                    {
                        goodsEvent.OperationInformation.Combinationtripinfo = new GoodsEventsOperationInformationCombinationtripinfo
                        {
                            Route = combinationTrip.LoadDistribWli.RouteId,
                            TripId = combinationTrip.LoadDistribWli.TripId
                        };
                    }

                    if (!string.IsNullOrEmpty(ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId))
                    {
                        goodsEvent.OperationInformation.PhysicalLoadCarrier = ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId.ToUpper();
                    }

                    if (!string.IsNullOrEmpty(ModelMain.SelectedOperationProcess.PowerUnitId) && !IsNotToSendPowerUnitInGoodsEvent)
                    {
                        goodsEvent.OperationInformation.PowerUnitId = ModelMain.SelectedOperationProcess.PowerUnitId.ToUpper();
                    }
                }

                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GoodsEventsEventInformation, consignmentEntity.ConsignmentId);

                CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);

                consignmentEntity.GoodsEvent = goodsEvent;
                consignmentEntity.ConsignmentInstance.EntityStatus |= EntityStatus.GoodsEventSent;
                FlowResult.State = FlowResultState.Ok;
            }
        }


        public void VerifyFlowDepartureFromStopResult(FlowResultDepartureFromStop flowResult)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.VerifyFlowDepartureFromStopResult()");

            FlowResult.Message = flowResult.Message;
        }


        public bool BackFromFlowConfirmConsignmentItemCount(FlowResultConfirmConsignmentItemCount subflowResult)
        {
            var result = BackFromFlowConfirmConsignmentItemCount(subflowResult, null);
            return result;
        }

        public bool BackFromFlowConfirmConsignmentItemCount(FlowResultConfirmConsignmentItemCount subflowResult, MessageHolder messageHolder)
        {
            if (messageHolder != null)
            {
                var messageState = MessageState.Information;
                switch (subflowResult.State)
                {
                    case FlowResultState.Cancel:
                        messageState = MessageState.Warning;
                        break;

                    case FlowResultState.Ok:
                        return true;

                    case FlowResultState.Warning:
                        messageState = MessageState.Warning;
                        break;

                    case FlowResultState.Error:
                        messageState = MessageState.Error;
                        break;
                }
                messageHolder.Update(subflowResult.Message, messageState);
            }
            else
            {
                FlowResult.Message = subflowResult.Message;
                FlowResult.State = subflowResult.State;
                return FlowResult.State == FlowResultState.Ok;
            }

            return false;
        }


        public bool BackFromFlowConfirmConsignmentItemCount(ConsignmentEntity consignmentEntity, FlowResultConfirmConsignmentItemCount subflowResult, MessageHolder messageHolder, bool hasMessageFromVasMatrix)
        {
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    if (consignmentEntity != null)
                    {
                        if (hasMessageFromVasMatrix == false)
                        {
                            if (consignmentEntity.IsScannedOnline)
                            {
                                messageHolder.Update(string.Format(GlobalTexts.ConsignmentRegistered, consignmentEntity.EntityDisplayId), MessageState.Information);
                                SoundUtil.Instance.PlaySuccessSound();
                            }
                            else
                            {
                                messageHolder.Update(string.Format(GlobalTexts.OfflineConsignmentRegistered, consignmentEntity.EntityDisplayId), MessageState.Warning);
                                SoundUtil.Instance.PlayWarningSound();
                            }
                        }
                        else
                            SoundUtil.Instance.PlayWarningSound();
                    }
                    return true;

                case FlowResultState.Cancel:
                    messageHolder.Update(subflowResult.Message, MessageState.Warning);
                    return false;

            }
            return false;
        }




        /// <summary>
        /// Returns true if multiple cosnignments are allowed for a pickup operation
        /// </summary>
        public bool IsMultipleConsignmentsAllowedForVerifiedConsignment()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsMultipleConsignmentsAllowed()");

            var result = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsMultipleConsignmentsAllowed);
            return result;
        }

        public bool IsMultipleConsignmentsAllowedForOperation()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsMultipleConsignmentsAllowed()");

            var result = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsMultipleConsignmentsAllowed);
            if (result == false)
            {
                FlowResult.State = FlowResultState.Error;
                FlowResult.Message = GlobalTexts.PickupMultipleConsignmentsUnauthorized;
            }
            return result;
        }


        /// <summary>
        /// Returns true if the user is allowed to enter (scan) an unplanned consignmentEntity
        /// </summary>
        public bool IsScanningUnplannedConsignmentsAllowed()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsScanningUnplannedConsignmentsAllowed()");
            var result = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsUnplannedConsignmentsAllowed);
            if (result == false)
            {
                FlowResult.State = FlowResultState.Error;
                FlowResult.Message = GlobalTexts.ScanningUnplannedConsignment;
            }
            else
            {
                FlowResult.State = FlowResultState.Ok;
            }
            return result;
        }


        /// <summary>
        /// Checks if a consignmentEntity has been validated (GoodsEvent has been sent)
        /// </summary>
        /// <returns></returns>
        public bool IsConsignmentValidatedBefore(ConsignmentEntity consignmentEntity, EntityMap entityMap)
        {
            bool result = false;
            var consignmentHistoryInfo = entityMap.GetConsignment(consignmentEntity.ConsignmentId);
            if (consignmentHistoryInfo != null)
            {
                // Check that is has been scanned, not only planned
                if ((consignmentHistoryInfo.EntityStatus & EntityStatus.GoodsEventSent) != EntityStatus.False)
                    result = true;
            }
            Logger.LogEvent(Severity.Debug, "Executed command CommandsHandleScannedBarcodes.IsConsignmentValidatedBefore();" + result);

            return result;
        }


        /// <summary>
        /// Checks result of flow. If flow resulted ok, we will start to use HistoryMap altered by this flow
        /// </summary>
        /// <param name="consignmentEntity">Data about most recent scanning</param>
        /// <param name="flowResult">Result given from flow PickupOrder</param>
        /// <param name="messageHolder">Message to fill out so can be displayed in scanning gui</param>
        /// <param name="entityMap">History map containing original data before flow was called</param>
        /// <param name="entityMapClone">History map containing potentially altered data</param>
        /// <returns>True if pickup flow should continue, false is now finished</returns>
        public static bool ValidateFlowHandleScannedNumber(ConsignmentEntity consignmentEntity, BaseFlowResult flowResult, MessageHolder messageHolder, ref EntityMap entityMap,
            EntityMap entityMapClone)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsReconciliation.ValidateFlowHandleScannedNumber()");

            bool result;
            switch (flowResult.State)
            {
                case FlowResultState.Cancel:
                    messageHolder.Update(string.IsNullOrEmpty(messageHolder.Text) ? GlobalTexts.FlowAborted : flowResult.Message,
                        MessageState.Warning);
                    SoundUtil.Instance.PlayWarningSound();
                    result = true;
                    break;

                case FlowResultState.Finished:
                    entityMap = entityMapClone;
                    result = false;
                    break;

                case FlowResultState.Ok:
                    entityMap = entityMapClone;
                    if (consignmentEntity != null)
                    {
                        if (flowResult.MessageIsFromVasMatrix)
                            UpdateMessageHolderWithMessageFromVasMatrix(flowResult, messageHolder);
                        else
                            UpdateMessageHolderWithScannedEntityInfo(consignmentEntity, messageHolder);
                    }
                    else
                    {
                        Logger.LogEvent(Severity.Error, "ConsignmentEntity uninitialzed in ValidateFlowHandleScannedNumber()");
                    }
                    result = true;
                    break;

                case FlowResultState.Error:
                    messageHolder.Update(string.IsNullOrEmpty(flowResult.Message) == false ?
                        flowResult.Message : GlobalTexts.ErrorWhileScanning, MessageState.Error);
                    SoundUtil.Instance.PlayScanErrorSound();
                    result = true;
                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unhandled state {0} encountered in ValidateFlowHandleScannedNumber()", flowResult.State);
                    break;
            }

            return result;
        }

        private static void UpdateMessageHolderWithMessageFromVasMatrix(BaseFlowResult flowResult, MessageHolder messageHolder)
        {
            messageHolder.Update(flowResult.Message, MessageState.Warning);
            
            flowResult.MessageIsFromVasMatrix = false; // In case of reuse of FlowResult
        }

        private static void UpdateMessageHolderWithScannedEntityInfo(ConsignmentEntity consignmentEntity, MessageHolder messageHolder)
        {
            var consignmentItem = consignmentEntity as ConsignmentItem;
            
            if (consignmentEntity.IsScannedOnline)
            {
                messageHolder.Update(consignmentItem != null ?
                    string.Format(GlobalTexts.ConsignmentItemRegistered, consignmentItem.EntityDisplayId) :
                    GlobalTexts.RegisteredConsignment + " " + consignmentEntity.ConsignmentDisplayId,
                    MessageState.Information);
                SoundUtil.Instance.PlaySuccessSound();
            }
            else
            {
                messageHolder.Update(consignmentItem != null ?
                    string.Format(GlobalTexts.OfflineConsignmentItemRegistered, consignmentItem.EntityDisplayId) :
                    GlobalTexts.OfflineRegisteredConsignment + " " + consignmentEntity.ConsignmentDisplayId,
                    MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
            }
        }


        public bool IsSendingRegisterConsignmentEventRequired()
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.IsSendingRegisterConsignmentEventRequired()");
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsSendingConsignmentEventRequired);
        }

        public bool ValidateFlowReconcilliation(FlowResultReconciliation subFlowResult, ref EntityMap entityMap)
        {
            bool result = true;
            FlowResult.State = subFlowResult.State;


            if (subFlowResult.EntityMap != null)
                entityMap = subFlowResult.EntityMap;


            switch (subFlowResult.State)
            {
                case FlowResultState.Cancel:
                    FlowResult.Message = GlobalTexts.ReconcilliationAborted;
                    result = false;
                    break;

                case FlowResultState.Ok:
                    FlowResult.State = FlowResultState.Finished;
                    entityMap = subFlowResult.EntityMap;
                    break;
            }

            return result;
        }

        public bool VerifyFlowResultVasMatrix(FlowResultVasMatrix flowResultVasMatrix)
        {
            FlowResult.State = flowResultVasMatrix.State;
            FlowResult.InnerFlowResult = flowResultVasMatrix.SubFlowResult;

            if (string.IsNullOrEmpty(flowResultVasMatrix.Message) == false)
            {
                FlowResult.Message = flowResultVasMatrix.Message;
                FlowResult.MessageIsFromVasMatrix = true;
            }

            return flowResultVasMatrix.State == FlowResultState.Ok;
        }
    }
}

using System;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using Com.Bring.PMP.PreComFW.Shared.Measurement;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class SettingsCommands
    {
        private static SettingsCommands _instance;

        public static SettingsCommands Instance
        {
            get { return _instance ?? (_instance = new SettingsCommands()); }
        }

        /// <summary>
        /// save user profile locally and on application server when profile view either opened after login or opened through setting Module
        /// </summary>
        public void SaveUserProfile(UserProfile userProfile)
        {
            Logger.LogEvent(Severity.Debug, "Executing function FormChangeProfile.SaveUserProfile");

            //Save user profile on server
            SaveUserProfileOnServer(userProfile);

            //Save user progile in local database
            SaveUserProfileLocally(userProfile);
        }


        public void SaveUserProfileLocally(UserProfile userProfile)
        {
            Logger.LogEvent(Severity.Debug, "Executing function SettingsCommands.SaveUserProfileLocally");

            //Now update user profile
            ModelUser.UserProfile = userProfile;
            SettingDataProvider.Instance.SaveUserProfile(userProfile);
        }



        private void SaveUserProfileOnServer(UserProfile userProfile)
        {
            var userInformation = GetUserInformationFromServer(userProfile.CredentialId);
            if (userInformation != null)
            {
                //copy profile related info from local object of userprofile
                userInformation.UserDetail.ProfileCompanyCode = userProfile.CompanyCode;
                userInformation.UserDetail.ProfileLoadCarrier = userProfile.LoadCarrier;
                userInformation.UserDetail.ProfileOrgUnitId = userProfile.OrgUnitId;
                userInformation.UserDetail.ProfilePowerUnit = userProfile.PowerUnit;
                userInformation.UserDetail.ProfileRouteId = userProfile.RouteId;
                userInformation.UserDetail.ProfileTelephoneNumber = userProfile.TelephoneNumber;
                userInformation.UserDetail.ProfileTmsAffiliationId = userProfile.TmsAffiliation;
                userInformation.UserDetail.ProfileUserRole = userProfile.RoleName;
                userInformation.UserDetail.ProfileDefaultPrinter = userProfile.DefaultPrinter;
                
                userInformation.User.Id = ModelModules.Communication.GetClient().CredentialId;

                //Send synchronous call to save user profile
                CommunicationClient.Instance.SendUserInformationEvent(userInformation);
            }
        }

        /// <summary>
        /// This function calls the GetUserProfile function from Login class, to send synchronous message
        /// to server for getting user profile.
        /// </summary>
        /// <param name="credentialId">Specifies credentialid for user</param>
        /// <returns>Returns the user profile either from server or mocked one.</returns>
        public UserProfile GetUserProfileFromServer(int credentialId)
        {
            UserProfile userProfile = null;
            try
            {
                var userInformation = GetUserInformationFromServer(credentialId);
                if (userInformation != null)
                {
                    Logger.LogEvent(Severity.Debug, "User Id is " + userInformation.User.Id);
                    Logger.LogEvent(Severity.Debug, "User name is " + userInformation.User.Username);
                    
                    //Copy user profile part into local entity
                    userProfile = new UserProfile(userInformation);
                }
            }
            catch (Exception ex)
            {

                Logger.LogException(ex, "SettingsCommands.GetUserProfileFromServer");
            }
            return userProfile;
        }

        private static UserInformation GetUserInformationFromServer(int credentialId)
        {
            //Send Synchronous request for getting user profile from server
            var userInformationRequest = new UserInformationRequest
            {
                CredentialId = credentialId
            };

            var transaction = new Transaction(Guid.NewGuid(), (byte) TransactionCategory.UserDetail, "UserID: " + credentialId);
            var userInformation = CommunicationClient.Instance.Query<UserInformation>(userInformationRequest, transaction);
            return userInformation;
        }
    }
}

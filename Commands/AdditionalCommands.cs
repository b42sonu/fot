﻿using System.Linq;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class AdditionalCommands
    {

        //TODO :: Write Unit Test
        public static void UpdateLoadListOperationStatus(List<DeliveryOperation> loadList, EntityMap entityMap)
        {
            if (loadList == null)
                loadList = CommandsOperations.GetLocallyStoredLoadList(string.Empty);

            if (loadList != null)
            {
                //Iterate for all consignment scanned and stored in local cache
                foreach (var consignment in entityMap.Values)
                {
                    //Check if corresponding to consignment in cache, operation in load list exists or not
                    //If not exists then continue with next consignment
                    if (loadList.Count(c => c.Consignment.ConsignmentNumber.Equals(consignment.ConsignmentId)) <= 0)
                        continue;
                    
                    //Mark status of all consignment items as scanned, which are having status as scanned
                    //in cache
                    CommandsOperations.MarkItemStatusAsFinished(loadList, consignment);
                    

                    //Check if all consignment item in consignment scanned by user or not
                    bool allItemsScanned = CheckIfAllConsignmentItemScanned(consignment, loadList);

                    //If all items in consignment scanned then mark status of operation to finished
                    if (allItemsScanned == true)
                    {
                        var plannedConsignment = new PlannedConsignmentsType
                        {
                            ConsignmentNumber = consignment.ConsignmentId
                        };
                        //Mark operation status
                        CommandsOperations.MarkOperationStatusAsFinished(loadList, plannedConsignment);
                    }
                }

                //Store updated load list locally
                CommandsOperations.SaveLoadListLocally(loadList);
            }

        }


        /// <summary>
        /// This method checks, if all items for a load list operation exists in consigment from cache and
        /// having status as scanned.
        /// </summary>
        /// <param name="consignmentInCache">Consignment from cache.</param>
        /// <param name="loadList">Load list, from which operation to check.</param>
        /// <returns>Returns bool, value saying that all items from operation in load list are scanned or not.</returns>
        private static bool CheckIfAllConsignmentItemScanned(Consignment consignmentInCache, List<DeliveryOperation> loadList)
        {
            //Get delivery operation where consignment number matches
            var operation = loadList.Where(c => c.Consignment.ConsignmentNumber == consignmentInCache.ConsignmentId).SingleOrDefault<DeliveryOperation>();

            // nulll Check (If load carrier have no consignment item)
            if (operation.Consignment.ConsignmentItem != null)
            {

                //Iterate for all the consignment items in delivery operation from load list
                foreach (var consignmentItem in operation.Consignment.ConsignmentItem)
                {
                    if (consignmentItem.Status == "Pending")
                        return false;
                }
            }
            return true;
        }



        //TODO :: Write Unit Test
        /// <summary>
        /// This method is only used in attempted delivery
        /// </summary>
        /// <param name="loadList"></param>
        /// <param name="consignmentNumber"></param>
        public static void UpdateLoadListOperationStatus(List<DeliveryOperation> loadList, string consignmentNumber)
        {
            if (loadList != null)
            {
                if (loadList.Count(c => c.Consignment.ConsignmentNumber.Equals(consignmentNumber)) > 0)
                {
                    var plannedConsignment = new PlannedConsignmentsType
                    {
                        ConsignmentNumber = consignmentNumber
                    };
                    //Mark operation status
                    CommandsOperations.MarkOperationStatusAsFinished(loadList, plannedConsignment);

                    //Store updated load list locally
                    CommandsOperations.SaveLoadListLocally(loadList);
                }
            }
        }

        /// <summary>
        /// This method is specific to FlowDeparture only
        /// </summary>
        /// <param name="consignmentNumber"></param>
        public static void UpdateLoadListOperationStatus(string consignmentNumber)
        {
            List<DeliveryOperation> loadList = null;
            loadList = CommandsOperations.GetLocallyStoredLoadList(string.Empty);

            if (loadList != null)
            {
                if (loadList.Count(c => c.Consignment.ConsignmentNumber.Equals(consignmentNumber)) > 0)
                {
                    var plannedConsignment = new PlannedConsignmentsType
                    {
                        ConsignmentNumber = consignmentNumber
                    };
                    //Mark operation status
                    CommandsOperations.MarkOperationStatusAsFinished(loadList, plannedConsignment);

                    //Store updated load list locally
                    CommandsOperations.SaveLoadListLocally(loadList);
                }
            }
        }

        /// <summary>
        /// This method is specific to flow departure from stop
        /// </summary>
        /// <param name="entityMap"></param>
        public static void UpdateLoadListOperationStatus(EntityMap entityMap)
        {
            var loadList = CommandsOperations.GetLocallyStoredLoadList(string.Empty);
            UpdateLoadListOperationStatus(loadList, entityMap);
        }


    }


}

﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    /// <summary>
    /// Contains several useful functions for handling operations
    /// </summary>
    public class OperationCommands
    {
        /// <summary>
        /// Get PlannedOperation  on the basis of OperationId from a particular stop
        /// </summary>
        // TODO: Remove this when PlannedOPeration is property on consignment
        public static PlannedOperationType GetPlannedOperation(OperationListStop operationListStop, string operationId)
        {
            Logger.LogEvent(Severity.Debug, "Retrieveing planned operation type");
            PlannedOperationType result = null;

            if (operationListStop != null && operationId != null)
            {
                result = (from n in operationListStop.PlannedOperation
                          where n.OperationId == operationId
                          select n).FirstOrDefault<PlannedOperationType>();
            }
            else
            {
                Logger.LogEvent(Severity.Debug, "Failed to retrieve planned operation in GoodsEventHelper.GetPlannedOperation");
            }

            return result;
        }



        /// <summary>
        /// Retrieve the planned consignment information for a consignment-id
        /// </summary>
        public static PlannedConsignmentsType GetPlannedConsignment(string consignmentId, PlannedConsignmentsType[] plannedConsignments)
        {
            PlannedConsignmentsType plannedConsignment = null;
            // If we have scanned a planned consignment, use details from plan
            if (plannedConsignments != null)
            {
                foreach (var consignment in plannedConsignments)
                {
                    if (consignment.ConsignmentNumber == consignmentId)
                    {
                        plannedConsignment = consignment;
                        break;
                    }
                }
            }
            return plannedConsignment;
        }

        /// <summary>
        /// method used for evaluated the planned operation status...
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static int GetOperationType(PlannedOperationTypeOperationType type)
        {
            int operationType = 0;

            //Logger.LogEvent(Severity.Debug, "Executing command ActionOperationListCommands.GetOperationType(PlannedOperationTypeOperationType type)");
            switch (type)
            {
                case PlannedOperationTypeOperationType.DELIVERY:
                    operationType = (int)OperationTypeImage.Delivery;//StopOperationType.Delivery;
                    break;
                case PlannedOperationTypeOperationType.LOAD:
                    operationType = (int)OperationTypeImage.Load;
                    break;
                case PlannedOperationTypeOperationType.LOAD_DISTRIBUTION:
                    operationType = (int)OperationTypeImage.LoadDistributionTruck;
                    break;
                case PlannedOperationTypeOperationType.LOAD_LINE_HAUL:
                    operationType = (int)OperationTypeImage.LoadLineHaul;
                    break;
                case PlannedOperationTypeOperationType.PICKUP:
                    operationType = (int)OperationTypeImage.PickUp;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD:
                    operationType = (int)OperationTypeImage.Unload;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL:
                    operationType = (int)OperationTypeImage.UnloadLineHaul;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD_PICKUP:
                    operationType = (int)OperationTypeImage.UnloadPickUpTruck;
                    break;
                case PlannedOperationTypeOperationType.RETURNEDDELIVERY:
                    operationType = (int)OperationTypeImage.ReturnedDelivery;
                    break;
            }
            return operationType;
        }

        /// <summary>
        /// method used for evaluated the planned operation status...
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static int PlannedOperationStatus(string status)
        {
            // Logger.LogEvent(Severity.Debug, "Executing command ActionOperationListCommands.PlannedOperationStatus(string Status)");
            int plannedOperationStatus;

            if (String.CompareOrdinal(status, StopOperationType.Finished) == 0)
            {
                plannedOperationStatus = (int)StopOperationStatusImage.Completed;
            }
            else if (String.CompareOrdinal(status, StopOperationType.Started) == 0)
            {
                plannedOperationStatus = (int)StopOperationStatusImage.Started;
            }
            else
            {
                plannedOperationStatus = (int)StopOperationStatusImage.NotStarted;
            }


            return plannedOperationStatus;
        }

        /// <summary>
        /// method used for create more operation if in operation is pikup and with repeated order number with different customer id...
        /// </summary>
        /// <param name="operationListStop"></param>
        /// <returns></returns>
        public static OperationListStop ManipulatePickupOperationsInStop(OperationListStop operationListStop)
        {
            //List<string> listOrderNumber = new List<string>();

            if (operationListStop == null || operationListStop.PlannedOperation == null) return operationListStop;

            var plannedOperations = (from n in operationListStop.PlannedOperation
                                     where
                                         n.OperationType ==
                                         PlannedOperationTypeOperationType.PICKUP
                                     select n).ToList<PlannedOperationType>();

            bool isFirstConsignment = true;
            foreach (PlannedOperationType plannedOperation in plannedOperations)
            {
                //listOrderNumber.Clear();
                if (plannedOperation.PlannedConsignments != null && plannedOperation.PlannedConsignments.Length > 1)
                {
                    foreach (PlannedConsignmentsType plannedConsignmentsType in plannedOperation.PlannedConsignments)
                    {
                        /*if (!string.IsNullOrEmpty(plannedConsignmentsType.BookingNo) && !listOrderNumber.Contains(plannedConsignmentsType.BookingNo))
                            listOrderNumber.Add(plannedConsignmentsType.BookingNo);
                        else
                        {*/



                        if (!isFirstConsignment)
                        {
                            string newOperationId = plannedOperation.OperationId + "_" +
                                                    plannedConsignmentsType.CustomerId + "_" +
                                                    plannedConsignmentsType.OrderNumber;


                            var existPlannedOperation = (from n in operationListStop.PlannedOperation
                                                         where n.OperationId == newOperationId
                                                         select n).FirstOrDefault<PlannedOperationType>();

                            if (existPlannedOperation != null &&
                                existPlannedOperation.Status == StopOperationType.New) //
                            {
                                var listPlannedOperation = operationListStop.PlannedOperation.ToList();
                                listPlannedOperation.RemoveAll(
                                    d => d.OperationId == newOperationId);

                                operationListStop.PlannedOperation =
                                    listPlannedOperation.ToArray<PlannedOperationType>();
                                operationListStop = AddNewPickUpOperation(operationListStop, plannedConsignmentsType,
                                                                          newOperationId, plannedOperation);

                            }
                            else if (existPlannedOperation == null)
                            //create new plannedoperation with one PlannedConsignment...
                            {
                                operationListStop = AddNewPickUpOperation(operationListStop, plannedConsignmentsType,
                                                                          newOperationId, plannedOperation);
                            }

                            //remove the PlannedConsignment ...
                            var listPlannedConsignments = plannedOperation.PlannedConsignments.ToList();
                            listPlannedConsignments.RemoveAll(
                                d =>
                                d.OrderNumber == plannedConsignmentsType.OrderNumber &&
                                d.CustomerId == plannedConsignmentsType.CustomerId);

                            var updatedPlannedOperation = (from n in operationListStop.PlannedOperation
                                                           where n.OperationId == plannedOperation.OperationId
                                                           select n).FirstOrDefault<PlannedOperationType>();
                            if (updatedPlannedOperation != null)
                            {
                                updatedPlannedOperation.PlannedConsignments =
                                    listPlannedConsignments.ToArray<PlannedConsignmentsType>();

                            }
                        }
                        isFirstConsignment = false;
                        //}
                    }
                }
            }

            return operationListStop;
        }

        public static OperationListStop AddNewPickUpOperation(OperationListStop operationListStop, PlannedConsignmentsType plannedConsignmentsType,
                                                              string newOperationId, PlannedOperationType originalPlannedOperation)
        {

            if (operationListStop == null || String.IsNullOrEmpty(newOperationId) || originalPlannedOperation == null)
                return operationListStop;

            var plannedOperationNew = new PlannedOperationType
            {
                ConsignmentItemCount = 1,
                ConsignmentItemCountSpecified = originalPlannedOperation.ConsignmentItemCountSpecified,
                EarliestStartTime = originalPlannedOperation.EarliestStartTime,
                EarliestStartTimeSpecified = originalPlannedOperation.EarliestStartTimeSpecified,
                ExternalTripId = originalPlannedOperation.ExternalTripId,
                LatestStartTime = originalPlannedOperation.LatestStartTime,
                LatestStartTimeSpecified = originalPlannedOperation.LatestStartTimeSpecified,
                LoadCarrierId = originalPlannedOperation.LoadCarrierId,
                OperationOrigin = originalPlannedOperation.OperationOrigin,
                OperationType = originalPlannedOperation.OperationType,
                PackageType = originalPlannedOperation.PackageType,
                PickupOrderNumber = originalPlannedOperation.PickupOrderNumber,
                PowerLoadCarrierId = originalPlannedOperation.PowerLoadCarrierId,
                Repositioning = originalPlannedOperation.Repositioning,
                Route = originalPlannedOperation.Route,
                Status = originalPlannedOperation.Status,
                TransportEquipment = originalPlannedOperation.TransportEquipment,
                TripId = originalPlannedOperation.TripId,
                Volume = originalPlannedOperation.Volume,
                Weight = originalPlannedOperation.Weight,
                OperationId = newOperationId,
                PlannedConsignments = null,
                OrderID = originalPlannedOperation.OrderID,
            };


            var plannedConsignmentsArray = new PlannedConsignmentsType[1];
            plannedConsignmentsArray[0] = plannedConsignmentsType;
            plannedOperationNew.PlannedConsignments = plannedConsignmentsArray;

            var currentStopPlannedOperations =
                operationListStop.PlannedOperation.ToList();
            currentStopPlannedOperations.Add(plannedOperationNew);

            operationListStop.PlannedOperation = currentStopPlannedOperations.ToArray();

            return operationListStop;
        }
    };
}

﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class ViewCommands
    {
        /// <summary>
        /// Specifies the collection of views.
        /// </summary>
        public static Dictionary<string, BaseForm> Views
        {
            get { return _views ?? (_views = new Dictionary<string, BaseForm>()); }
        }

        private static Dictionary<string, BaseForm> _views;

        /// <summary>
        /// This property will have object for current view.
        /// </summary>
        public static BaseForm CurrentView { get; set; }

        public static bool ViewExists<T>()
        {
            bool result = false;
            string viewName = typeof(T).FullName;

            // If view not available in collection
            if (Views.ContainsKey(viewName))
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// This method gets the object for required view
        /// Used when created form dont need constructor, so can initialize it later
        /// </summary>
        public static T GetOrCreateView<T>() where T : BaseForm, new()
        {
            T view;
            string viewName = typeof(T).FullName;

            // If view not available in collection
            if (Views.ContainsKey(viewName))
            {
                view = (T)Views[viewName];
                //Added to fix 1795
                view.Visible = true;
            }
            else
            {
                //Create the object, cast it to required type
                view = new T();
                Views.Add(viewName, view);
                view.OnCreate(BaseModule.ActiveModule);
            }

            return view;
        }

        internal static T GetView<T>() where T : BaseForm
        {
            T view = null;
            string viewName = typeof(T).FullName;

            // If view not available in collection
            if (Views.ContainsKey(viewName))
            {
                view = (T)Views[viewName];
            }
            return view;
        }

        public static T ShowView<T>() where T : BaseForm, new()
        {
            return ShowView<T>(null);
        }

        public static T ShowView<T>(IFormData formData) where T : BaseForm, new()
        {
            T result = null;

            try
            {
                var view = GetOrCreateView<T>();
                ShowView(false, view, formData);
                result = view;
            }
            catch (Exception e)
            {
                Logger.LogException(e, "ViewCommands.ShowView");
            }

            return result;
        }


        public static void ShowView(BaseForm view, IFormData formData)
        {
            ShowView(false, view, formData);
        }

        public static void ShowView(bool returningFromSubFlow, BaseForm view, IFormData formData)
        {
            try
            {
                if (returningFromSubFlow == false || view.AutoShow)
                {
                    // Sometimes we call a flow without view, so can come calling this function where previous view is same as the new view 
                    // It is not neccesary to close and re-open same view again when coming back
                    if (view != CurrentView)
                    {
                        if (CurrentView != null)
                        {
                            CurrentView.Visible = false;
                            CurrentView.DisableOnScreenKeyboard();
                            CurrentView.OnClose();
                        }
                    }

                    if (view.AutoShow)
                    {
                        view.OnShow(formData);
                    }

                    if (view != CurrentView)
                    {
                        if (view.IsDisposed == false)
                        {
                            view.EnableOnScreenKeyboard();
                            view.OnResume();

                            view.Visible = true;
                            view.Show(BaseModule.MainForm);
                            CurrentView = view;
                        }
                        #if DEBUG || TestNewFeature
                        else
                        {
                            Logger.LogEvent(Severity.Error, "Detected usage of disposed view");
                            CurrentView = null;
                        }
                        #endif
                    }


                    if (BaseModule.CurrentFlow != null)
                    {
                        BaseModule.CurrentFlow.CurrentView = view;
                    }

                    view.ShowingView();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "BaseModule.Show");
            }
        }
    }
}
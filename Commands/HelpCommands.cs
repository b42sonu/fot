﻿namespace Com.Bring.PMP.PreComFW.Shared.Commands
{
    public class HelpCommands
    {
        /// <summary>
        /// Help in Edit mode will only show only if user allow it
        /// </summary>
        public static bool IsEditHelpAllowedByUser { get; set; }

        /// <summary>
        /// Help in Edit mode will only show only if this property will false
        /// </summary>
        public static bool IsEditHelpAlowedDefault  { get; set; }
   
    
    }
}

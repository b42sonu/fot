﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.IntegrationEngine;
using Com.Bring.PMP.PreComFW.IntegrationEngine.Classes;
using PreCom.Core;
using PreCom.Core.Communication;
using PreCom.Core.Modules;

namespace Com.Bring.PMP.PreComFW.Shared
{
    // Synchronous Send/Receive utility methods 
    public static class SynchSRUtils
    {
#if !PreComLT
        public static IntegrationMessage<T> PrepareIntegrationMsg<T>(RequestInfo requestInfo, IIntegrationEngine integration, DateTime arrivedDate, T request) where T : EntityBase
        {
            //Prepare message for integration engine
            IntegrationMessage<T> msg = new IntegrationMessage<T>();
            msg.Id = integration.GetNewSequentialId();
            msg.Timings = new Timings();
            msg.Timings.ReceivedServerFromPDA = arrivedDate;
            msg.Message = request;
            msg.Transaction = requestInfo.Transaction.ToIntegrationTransaction();
            msg.Sender = new SenderDetails()
            {
                CredentialId = requestInfo.Client.CredentialId,
                InstanceId = requestInfo.Client.ClientID,
                Node = integration.Node
            };
            return msg;
        }
#endif
        public static IntegrationMessageBase SendToWebIntegrationModule<T>(IIntegrationEngine integration, IntegrationMessage<T> msg) where T : EntityBase
        {
            integration.LogTiming(msg, TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.ApplicationServerCommunicationModule, TimingType.Sending, TimingComponent.ApplicationServerIntegrationComponent), TimedMessageType.Synchronous, DateTime.UtcNow);
            var res = integration.Send(msg, false, false, NodeType.APPLICATION_SERVER);
            integration.LogTiming(msg, TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.ApplicationServerCommunicationModule, TimingType.Queued, TimingComponent.ApplicationServerIntegrationComponent), TimedMessageType.Synchronous, DateTime.UtcNow);
            return res;
        }

        public static T SetResultFromWIMIntoReply<T>(IntegrationMessageBase res, IIntegrationEngine integration, IntegrationMessageBase msg) where T : EntityBase
        {
            string msgname = typeof(T).Name;
            T reply = null;
            if (res.Status == MessageStatus.SUCCESS)
            {
                Logger.LogInfoMessage(null, "2", "Information", "In ProductionRequest, success received for " + msgname);
                reply = res.Response as T;
                integration.LogTiming(msg, TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.ApplicationServerCommunicationModule, TimingType.Sending, TimingComponent.Client), TimedMessageType.Synchronous, DateTime.UtcNow);
            }
            else
            {
                Logger.LogInfoMessage(null, "3", "Information", "FAILURE return from " + msgname + ", Status: " + res.Status);
                integration.LogTiming(msg, TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.ApplicationServerCommunicationModule, TimingType.Sending, TimingComponent.Client), TimedMessageType.Synchronous, DateTime.UtcNow);

                throw new WebFaultException<string>(res.Status.ToString() + " ErrMsg: " + res.Error.Message, HttpStatusCode.InternalServerError);
            }
            return reply;
        }

    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.CheckPayment
{
    public enum FlowStateCheckPayment
    {
        ActionCheckPayment,
        ReturnSuccess,
        ReturnFailure,
        FlowComplete,
    }



    public class FlowResultCheckPayment : BaseFlowResult
    {
    }

    // US 77k: http://kp-confluence.postennorge.no/display/FOT/77k
    public class FlowCheckPayment : BaseFlow
    {
        private FlowDataVasProcessSupport _flowData;
        private readonly ActionCommandsCheckPayment _actionCommands;
        private readonly FlowResultCheckPayment _flowResult;


        public FlowCheckPayment()
        {
            _flowResult = new FlowResultCheckPayment {State = FlowResultState.Ok};
            _actionCommands = new ActionCommandsCheckPayment();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowCheckPayment");
            _flowData = (FlowDataVasProcessSupport)flowData;


            ExecuteActionState(FlowStateCheckPayment.ActionCheckPayment);
        }

        // Not used
        public override void ExecuteState(int state)
        {
            throw new NotImplementedException();
        }

        public void ExecuteActionState(FlowStateCheckPayment state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowCheckPayment;" + state);
                    switch (state)
                    {

                        case FlowStateCheckPayment.ActionCheckPayment:
                            state = _actionCommands.IsEntityPaid(_flowData.CurrentConsignmentEntity) ?
                                FlowStateCheckPayment.ReturnSuccess : FlowStateCheckPayment.ReturnFailure;
                            break;

                        case FlowStateCheckPayment.ReturnSuccess:
                            // Nothing to do
                            state = FlowStateCheckPayment.FlowComplete;
                            break;

                        case FlowStateCheckPayment.ReturnFailure:
                            _flowResult.State = _actionCommands.ShowProcessSupport(_flowData.CurrentConsignmentEntity, _flowData.Param) ? FlowResultState.Ok : FlowResultState.Cancel;
                            state = FlowStateCheckPayment.FlowComplete;
                            break;


                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateCheckPayment.FlowComplete);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowCheckPayment.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
            }


            EndFlow();
        }



        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowCheckPayment ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

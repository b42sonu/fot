﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.CheckPayment
{
    class ActionCommandsCheckPayment
    {
        /// <summary>
        /// checked the is consignment entity payment paid or not..
        /// </summary>
        internal bool IsEntityPaid(ConsignmentEntity consignmentEntity)
        {
            bool result = false;
            if (consignmentEntity.ConsignmentInstance != null && consignmentEntity.ConsignmentInstance.Payments != null)
            {
                result = consignmentEntity.ConsignmentInstance.Payments.All(payment => payment.Paid);
            }

            return result;
        }
        /// <summary>
        /// set data for popup on the basis of vas..
        /// </summary>
        internal bool ShowProcessSupport(ConsignmentEntity consignmentEntity, string parameter)
        {
            try
            {
                var vasParamParser = new VasParamParser(parameter);

                var severityText = vasParamParser.GetEntry("Severity");
                var text = vasParamParser.GetEntry("Text" + ModelUser.UserProfile.LanguageCode);
                text = VasStringFormatter.Format(text, consignmentEntity);
                return ShowPopup(severityText, text);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsCheckPayment.ShowProcessSupport");
                return false;
            }
        }
        /// <summary>
        /// show popup on the basis of passign severity ..
        /// </summary>
        private bool ShowPopup(string severity, string text)
        {
            switch (severity)
            {
                case "Error":
                    GuiCommon.ShowModalDialog(Language.Translate(severity), text, Severity.Error, GlobalTexts.Ok);
                    return false;

                case "Warning":
                    return GuiCommon.ShowModalDialog(Language.Translate(severity), text, Severity.Warning, GlobalTexts.Ok, GlobalTexts.Cancel) == GlobalTexts.Ok;

                case "Info":
                    GuiCommon.ShowModalDialog(Language.Translate(severity), text, Severity.Info, GlobalTexts.Ok);
                    return true;

                default:
                    throw new Exception("Illegal message severity: " + severity);
            }
        }



    }
}

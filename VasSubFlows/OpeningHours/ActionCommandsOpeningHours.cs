﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.OpeningHours
{
    class ActionCommandsOpeningHours
    {
        #region private data members

        private readonly string _countryCode = ModelUser.UserProfile.CountryCode;
        private bool isDateFound;
        #endregion

        #region Methods

        public bool ValidateDateIsFixedClosingDate()
        {
            return SettingDataProvider.IsFixedClosingDate(_countryCode);

        }

        public FlowStateOpeningHours ValidateIsWithinLimitedOpeningHours()
        {
            const FlowStateOpeningHours state = FlowStateOpeningHours.ActionValidateIsWithinDefaultOpeningHours;
            var result = SettingDataProvider.IsWithinLimitedOpeningHours(_countryCode, out isDateFound);
            return !result && isDateFound
                       ? FlowStateOpeningHours.ActionIsToOverrideItem
                       : (result ? FlowStateOpeningHours.ActionSetFlowResultOk : state);
        }

        public bool ValidateIsWithinDefaultOpeningHours()
        {
            return SettingDataProvider.IsWithinDefaultOpeningHours(_countryCode);
        }

        public void SetFlowResultToCancel(FlowResultOpeningHours flowresult)
        {
            GuiCommon.ShowModalDialog(GlobalTexts.Delivery, GlobalTexts.DeliveryOfAlcoholNotAllowedNow, Severity.Error, GlobalTexts.Ok);
            flowresult.State = FlowResultState.Cancel;
        }

        public bool ActionOverrideItem()
        {
            var result = false;

            var modalMessageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.DeliveryFromVinmonopoletInOpeningHour, Severity.Warning, GlobalTexts.Override, GlobalTexts.Abort);
            if (modalMessageBoxResult == GlobalTexts.Override)

                result = true;
            return result;
        }


        public void SetFlowResultToOk(FlowResultOpeningHours flowresult)
        {
            flowresult.State = FlowResultState.Ok;
        }

        # endregion
    }
}

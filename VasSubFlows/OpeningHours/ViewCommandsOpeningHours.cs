﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.OpeningHours
{
    class ViewCommandsOpeningHours
    {
        internal void ViewOverrideOpeningHours(ViewEventDelegate viewEventHandler, MessageHolder messageHolder)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsOpeningHours.ViewOverrideOpeningHours()");

            var view = ViewCommands.ShowView<FormOverrideOpeningHours>();
            view.SetEventHandler(viewEventHandler);
        }
    }
}

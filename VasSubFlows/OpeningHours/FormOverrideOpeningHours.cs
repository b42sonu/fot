﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.OpeningHours
{
    public class FormDataOverrideOpeningHours : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
    }
    public partial class FormOverrideOpeningHours : BaseForm
    {
        //   private FormDataOverrideOpeningHours _formData;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        private readonly MultiButtonControl _tabButtons;
        public FormOverrideOpeningHours()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };
            textBoxEnterCode.Tag = keyBoardProperty;
        }

        private void SetTextToGui()
        {
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, OkButton));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            textBoxEnterCode.Text = string.Empty;
            _tabButtons.SetButtonEnabledState(OkButton, textBoxEnterCode.Text != string.Empty);
            transparentLabel1.Text = GlobalTexts.Call999ToGetAcceptanceCode;
            labelCause.Text = GlobalTexts.Code +" :";
            labelModuleName.Text = GlobalTexts.OverrideOpeningHours;
        }


        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _tabButtons.SetButtonEnabledState(OkButton, textBoxEnterCode.Text != string.Empty);
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(OverrideOpeningHoursViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormOverrideOpeningHours.BtnBackClick");
            }
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(OverrideOpeningHoursViewEvents.Ok, textBoxEnterCode.Text.Trim());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormOverrideOpeningHours.BtnOkClick");
            }
        }
    }
    public class OverrideOpeningHoursViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
    }
}
﻿namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.OpeningHours
{
    partial class FormOverrideOpeningHours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.textBoxEnterCode = new System.Windows.Forms.TextBox();
            this.transparentLabel1 = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel1)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.AutoScroll = false;
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.transparentLabel1);
            this.touchPanel.Controls.Add(this.textBoxEnterCode);
            this.touchPanel.Controls.Add(this.labelCause);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelCause
            // 
            this.labelCause.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelCause.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelCause.Location = new System.Drawing.Point(22, 142);
            this.labelCause.Name = "labelCause";
            this.labelCause.Size = new System.Drawing.Size(120,30);
            this.labelCause.Text = "Code :";
            this.labelCause.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.Text = "Override Opening Hours";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // textBox1
            // 
            this.textBoxEnterCode.Location = new System.Drawing.Point(22, 172);
            this.textBoxEnterCode.Name = "textBox1";
            this.textBoxEnterCode.Size = new System.Drawing.Size(435, 41);
            this.textBoxEnterCode.TabIndex = 32;
            // 
            // transparentLabel1
            // 
            this.transparentLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.transparentLabel1.AutoSize = false;
            this.transparentLabel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.transparentLabel1.Location = new System.Drawing.Point(22, 54);
            this.transparentLabel1.Name = "transparentLabel1";
            this.transparentLabel1.Size = new System.Drawing.Size(435,70);
            this.transparentLabel1.Text = "Call 90 51 40 18 to get acceptance code for the delivery";
            this.transparentLabel1.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // FormAttemptedPickup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormOverrideOpeningHours";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transparentLabel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelCause;
        private Resco.Controls.CommonControls.TransparentLabel transparentLabel1;
        private System.Windows.Forms.TextBox textBoxEnterCode;
    }
}
﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.OpeningHours
{
    public enum FlowStateOpeningHours
    {
        ActionValidateDateIsFixedClosingDate,
        ActionValidateIsWithinLimitedOpeningHours,
        ActionValidateIsWithinDefaultOpeningHours,
        ActionIsToOverrideItem,
        ActionSetFlowResultToCancel,
        ActionSetFlowResultOk,
        ViewCommands,
        ViewOverrideOpeningHours,
        ThisFlowComplete,
    }

    public class FlowDataOpeningHours : BaseFlowData
    {
    }

    public class FlowResultOpeningHours : BaseFlowResult
    {
    }

    // US77f: http://kp-confluence.postennorge.no/display/FOT/77f+-+Vinmonopolet+-+opening+hours
    public class FlowOpeningHours : BaseFlow
    {
        private readonly FlowResultOpeningHours _flowResult;
        private readonly ActionCommandsOpeningHours _actionCommandsOpeningHours;
        private readonly ViewCommandsOpeningHours _viewCommandsOpeningHours;
        private readonly MessageHolder _messageHolder;
        private FlowDataVasProcessSupport _flowData;

        public FlowOpeningHours()
        {
            _flowResult = new FlowResultOpeningHours();
            _actionCommandsOpeningHours = new ActionCommandsOpeningHours();
            _viewCommandsOpeningHours = new ViewCommandsOpeningHours();
            _messageHolder = new MessageHolder();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowOpeningHours.BeginFlow");
            //ExecuteState(FlowStateOpeningHours.ViewOverrideOpeningHours);
            _flowData = (FlowDataVasProcessSupport)flowData;
            ExecuteState(FlowStateOpeningHours.ActionValidateDateIsFixedClosingDate);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateOpeningHours)state);
        }

        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowOpeningHours, which acts as name for next command.</param>
        public void ExecuteState(FlowStateOpeningHours state)
        {
            if (state > FlowStateOpeningHours.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStateOpeningHours state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing state " + state + " in flow FlowOpeningHours");

                switch (state)
                {
                    case FlowStateOpeningHours.ThisFlowComplete:
                        EndFlow();
                        break;

                    case FlowStateOpeningHours.ViewOverrideOpeningHours:
                        _viewCommandsOpeningHours.ViewOverrideOpeningHours(ViewOverrideOpeningHoursEventHandler, _messageHolder);
                        break;
                }

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowOpeningHours.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateOpeningHours.ThisFlowComplete);
            }
        }

        private void ViewOverrideOpeningHoursEventHandler(int overrideOpeningHoursViewEvents, params object[] data)
        {
            switch (overrideOpeningHoursViewEvents)
            {
                case OverrideOpeningHoursViewEvents.Ok:
                    _flowData.VasSubFlowResultInfo.EventComment = data[0].ToString();
                    ExecuteState(FlowStateOpeningHours.ActionSetFlowResultOk);
                    break;

                case OverrideOpeningHoursViewEvents.Back:
                    ExecuteState(FlowStateOpeningHours.ActionSetFlowResultToCancel);
                    break;
            }
        }


        public void ExecuteActionState(FlowStateOpeningHours state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "Executing state " + state + "in flow FlowOpeningHours");

                    switch (state)
                    {
                        case FlowStateOpeningHours.ActionValidateDateIsFixedClosingDate:
                            bool result = _actionCommandsOpeningHours.ValidateDateIsFixedClosingDate();
                            state = result ? FlowStateOpeningHours.ActionSetFlowResultToCancel : FlowStateOpeningHours.ActionValidateIsWithinLimitedOpeningHours;
                            break;

                        case FlowStateOpeningHours.ActionValidateIsWithinLimitedOpeningHours:
                            state = _actionCommandsOpeningHours.ValidateIsWithinLimitedOpeningHours();
                           // state = result ? FlowStateOpeningHours.ActionSetFlowResultToCancel : FlowStateOpeningHours.ActionValidateIsWithinDefaultOpeningHours;
                            break;

                        case FlowStateOpeningHours.ActionValidateIsWithinDefaultOpeningHours:
                            result = _actionCommandsOpeningHours.ValidateIsWithinDefaultOpeningHours();
                            state = result ? FlowStateOpeningHours.ActionSetFlowResultOk : FlowStateOpeningHours.ActionIsToOverrideItem;
                            break;

                        case FlowStateOpeningHours.ActionIsToOverrideItem:
                           result = _actionCommandsOpeningHours.ActionOverrideItem();
                           state = result ? FlowStateOpeningHours.ViewOverrideOpeningHours : FlowStateOpeningHours.ActionSetFlowResultToCancel;
                           break;

                        case FlowStateOpeningHours.ActionSetFlowResultToCancel:
                            _actionCommandsOpeningHours.SetFlowResultToCancel(_flowResult);
                            state = FlowStateOpeningHours.ThisFlowComplete;
                            break;

                        case FlowStateOpeningHours.ActionSetFlowResultOk:
                            _actionCommandsOpeningHours.SetFlowResultToOk(_flowResult);
                            state = FlowStateOpeningHours.ThisFlowComplete;
                            break;

                    }

                } while (state < FlowStateOpeningHours.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowOpeningHours.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateOpeningHours.ThisFlowComplete;
            }
            ExecuteViewState(state);

        }


        /// <summary>
        /// This method ends the flow FlowOpeningHours.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowOpeningHours.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}
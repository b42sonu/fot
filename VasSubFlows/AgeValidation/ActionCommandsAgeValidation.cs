﻿using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using System;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.AgeValidation
{
    class ActionCommandsAgeValidation
    {
        private static ICommunicationClient _communicationClient;
        internal const string CauseCodeNo = "07";
        internal const string CauseCodeTooYoung = "08";
        internal const string CauseCodeBelow25 = "09";
        //TODO :: Write Unit Test
        public ActionCommandsAgeValidation(ICommunicationClient communicationClient)
        {
            _communicationClient = communicationClient;
        }
        /// <summary>
        /// validate result of flow CauseAndMeasure and set action/data on the basis of result...
        /// </summary>
        public bool BackFromFlowCauseAndMeasure(FlowResultCauseAndMeasure subflowResult, out MessageHolder messageHolder, out string causeCode, out  string actionCode, out string comment)
        {
            bool verifiedSuccessfully = subflowResult != null && subflowResult.State == FlowResultState.Ok;
            causeCode = string.Empty;
            actionCode = string.Empty;
            comment = string.Empty;
            messageHolder = null;
            if (verifiedSuccessfully && subflowResult.SelectedReasonAndActionCombination != null)
            {
                causeCode = subflowResult.SelectedReasonAndActionCombination.CauseCode;
                actionCode = subflowResult.SelectedReasonAndActionCombination.MeasureCode;
                comment = subflowResult.Comment;
                //messageHolder = null;
            }

            return verifiedSuccessfully;
        }
        #region "Goods event detail"
        /// <summary>
        /// send good event for entity with cause and action and also include the process data..
        /// </summary>
        internal bool SendDocShownGoodsEvent(OperationProcess operationProcess, ConsignmentEntity consignmentEntity, string causeCode, string actionCode, string eventComment, string vasCode, string eventCode)
        {

            var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity, eventCode);


            if (goodsEvent != null && goodsEvent.EventInformation != null)
            {
                goodsEvent.EventInformation.CauseCode = causeCode;
                goodsEvent.EventInformation.ActionCode = actionCode;
                goodsEvent.EventInformation.EventComment = eventComment;
            }
            bool result = false;
            if (goodsEvent != null)
            {
                var transaction = new Transaction(Guid.NewGuid(),
                                                  (byte)TransactionCategory.GoodsEventsEventInformation,
                                                   consignmentEntity != null ? consignmentEntity.EntityId : null);
                if (_communicationClient != null)
                {
                    _communicationClient.SendGoodsEvent(goodsEvent, transaction);
                    result = true;
                }
            }

            return result;
        }


        internal bool SendConfirmIdGoodsEvent(OperationProcess operationProcess, EntityMap entityMap, string idDetails, string idCode, string recipientName, string causeCode, string actionCode, string vasCode)
        {
            //var eventInformation = new GoodsEventsEventInformation
            //{
            //    EventCode = "H",//EventCodeUtil.GetEventCodeFromProcess(),
            //    EventTime = DateTime.Now,
            //    CauseCode = causeCode,
            //    ActionCode = actionCode,
            //    Consignment = GetConsignmentsForGoodsEvent(entityMap, vasCode),
            //    RecipientId = idDetails,
            //    IdentificationDocumentType = idCode,
            //};

            //if (eventInformation.Signature == null && recipientName != null)
            //{
            //    eventInformation.Signature = new GoodsEventsEventInformationSignature { RecipientName = recipientName };

            //}

            bool result = false;
            var goodsEvent = GoodsEventHelper.CreateGoodsEventMessage(entityMap, operationProcess, null, "H");

            if (goodsEvent != null && goodsEvent.EventInformation != null)
            {
                goodsEvent.EventInformation.CauseCode = causeCode;
                goodsEvent.EventInformation.ActionCode = actionCode;
                goodsEvent.EventInformation.RecipientId = idDetails;
                goodsEvent.EventInformation.IdentificationDocumentType = idCode;
                if (goodsEvent.EventInformation.Signature == null && recipientName != null)
                {
                    goodsEvent.EventInformation.Signature = new GoodsEventsEventInformationSignature { RecipientName = recipientName };
                }
            }

            if (goodsEvent != null)
            {
                var transaction = new Transaction(Guid.NewGuid(),
                                                  (byte)TransactionCategory.GoodsEventsEventInformation,
                                                  operationProcess.OperationId);
                if (_communicationClient != null)
                {
                    _communicationClient.SendGoodsEvent(goodsEvent, transaction);
                    result = true;
                }
            }
            return result;

        }


        // Create a goodsevent for each of the checked consignmets
        internal GoodsEventsEventInformationConsignment[] GetConsignmentsForGoodsEvent(EntityMap entityMap, string vasCode)
        {
            var consignments = new List<GoodsEventsEventInformationConsignment>();
            var consignmentItems = new List<GoodsEventsEventInformationConsignmentConsignmentItem>();
            if (entityMap != null && (entityMap.Values != null && entityMap.Count > 0))
            {
                foreach (var consignment in entityMap.Values)
                {
                    if (consignment != null)
                    {

                        consignmentItems.Clear();
                        var consignmentItemHistoryMap = entityMap.GetConsignmentItems(consignment.ConsignmentId);
                        var scannedConsignmentItems = consignmentItemHistoryMap.Where(c => (c.Value.EntityStatus & EntityStatus.Scanned) != 0 && HasVasCode(c.Value.Vas, vasCode));

                        consignmentItems.AddRange(scannedConsignmentItems.Select(consignmentItem =>
                            new GoodsEventsEventInformationConsignmentConsignmentItem { ConsignmentItemNumber = consignmentItem.Value.ItemId }));

                        var consignmentItemsAreScanned = consignmentItems.Count > 0;
                        var consignmentAreScanned = consignment.HasStatus(EntityStatus.Scanned) && HasVasCode(consignment.Vas, vasCode);

                        //If consignment is having status scanned means user scanned consignment
                        //If consigmant has scanned items, ConsignmentItemEvent is sent
                        //if (consignment.HasStatus(Status.Scanned) == false && consignmentItemsAreScanned == false)
                        if (consignmentAreScanned == false && consignmentItemsAreScanned == false)
                            continue;

                        var consignmentInfo = new GoodsEventsEventInformationConsignment
                        {
                            ConsignmentNumber = consignment.ConsignmentIdForGoodsEvent,
                            OrderNumber = consignment.OrderNumber,
                            CustomerNumber = consignment.CustomerId,
                            ConsignmentItem = consignmentItemsAreScanned ? consignmentItems.ToArray() : null,
                            ConsignmentItemsHandled = consignmentItems.Count,
                            ConsignmentItemsHandledSpecified = consignmentItemsAreScanned,
                            MassRegistration = consignmentItemsAreScanned == false,
                            EventType = consignmentItemsAreScanned == false
                                    ? GoodsEventsEventInformationConsignmentEventType.ConsignmentEvent
                                    : GoodsEventsEventInformationConsignmentEventType.ConsignmentItemEvent,
                        };


                        // Use user specified consignmentitem count if existing
                        if (consignment.ManuallyEnteredConsignmentItemCount > 0)
                        {
                            consignmentInfo.ConsignmentItemsHandledSpecified = true;
                            consignmentInfo.ConsignmentItemsHandled = consignment.ManuallyEnteredConsignmentItemCount;
                        }

                        consignments.Add(consignmentInfo);
                    }
                }
            }

            return consignments.ToArray();
        }

        internal bool HasVasCode(VASType[] vas, string vasCode)
        {
            return vas != null && vas.Count(c => c.VASCode == vasCode) > 0;
        }


        #endregion
        /// <summary>
        /// method for delete the consignment entity for particular vas code from entity map...
        /// </summary>
        /// <param name="entityMap"></param>
        /// <param name="vasCode"></param>
        internal bool DeleteVasItemFromEntityMap(EntityMap entityMap, string vasCode)
        {
            var result = GuiCommon.ShowModalDialog(GlobalTexts.Warning,
                                      GlobalTexts.DoYouWantFotToDelete,
                                      Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
            if (result == GlobalTexts.Yes)
            {
                SendConfirmIdGoodsEvent(ModelMain.SelectedOperationProcess, entityMap, null, null, null, "13", "E", vasCode);

                Logger.LogEvent(Severity.Debug, "Deleting items with VAS Code from Entity " + vasCode);
                if (entityMap != null && entityMap.Values != null)
                {
                    var list = GetConsignmentEntityList(entityMap, vasCode);
                    if (list != null)
                        foreach (var consignmentEntity in list)
                        {
                            entityMap.RemoveEntity(consignmentEntity);
                        }
                }
            }

            return result == GlobalTexts.Yes;

        }
        /// <summary>
        /// get the consignment entity for specific vascode that passed to this method..
        /// </summary>
        public List<ConsignmentEntity> GetConsignmentEntityList(EntityMap entityMap, string vasCode)
        {
            var list = new List<ConsignmentEntity>();
            foreach (var consignment in entityMap.Values)
            {
                if (consignment != null)
                {
                    //if (HasVasCode(consignment.Vas, vasCode))
                    //{
                    var consignmentItemHistoryMap = entityMap.GetConsignmentItems(consignment.ConsignmentId);
                    if (consignmentItemHistoryMap != null)
                    {
                        var scannedConsignmentItems =
                            consignmentItemHistoryMap.Where(
                                c => (c.Value.EntityStatus & EntityStatus.Scanned) != 0 && HasVasCode(c.Value.Vas, vasCode));
                        list.AddRange(
                            scannedConsignmentItems.Select(scannedConsignmentItem => scannedConsignmentItem.Value).Cast
                                <ConsignmentEntity>());
                    }
                    //}
                }
            }
            return list;
        }

        /// <summary>
        /// open popup and also on the basis of popup result it trigger the next  action...
        /// </summary>
        internal FlowStateAgeValidation ShowPopUp(string text, ref CauseMeasure causeMeasure, ref FlowResultAgeValidation flowResult, out string causeCode, out string actionCode, params string[] buttonTexts)
        {
            string dialogResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning, text, Severity.Warning, buttonTexts);
            return ValidatePopResult(dialogResult, ref causeMeasure, ref flowResult, out causeCode, out actionCode);

        }
        /// <summary>
        /// validate pop up result on the basis of dialog result..
        /// </summary>
        private FlowStateAgeValidation ValidatePopResult(string dialogResult, ref CauseMeasure causeMeasure, ref FlowResultAgeValidation flowResult, out string causeCode, out string actionCode)
        {
            causeCode = string.Empty;
            actionCode = string.Empty;
            var flowStateAgeValidation = FlowStateAgeValidation.ThisFlowComplete;

            if (String.Compare(dialogResult, GlobalTexts.Yes, StringComparison.Ordinal) == 0)
                flowStateAgeValidation = FlowStateAgeValidation.ActionShowPopUp2;
            else if (String.Compare(dialogResult, GlobalTexts.No, StringComparison.Ordinal) == 0)
            {
                causeMeasure.CauseCode = CauseCodeNo;
                flowStateAgeValidation = FlowStateAgeValidation.FlowCauseAndMeasure;
            }
            else if (String.Compare(dialogResult, GlobalTexts.Back, StringComparison.Ordinal) == 0)
            {
                flowResult.State = FlowResultState.Cancel;
                flowStateAgeValidation = FlowStateAgeValidation.ThisFlowComplete;
            }
            else if (String.Compare(dialogResult, GlobalTexts.Above25, StringComparison.Ordinal) == 0)
            {
                flowResult.State = FlowResultState.Ok;
                flowStateAgeValidation = FlowStateAgeValidation.ThisFlowComplete;
            }
            else if (String.Compare(dialogResult, GlobalTexts.Below25, StringComparison.Ordinal) == 0)
            {
                causeCode = CauseCodeBelow25;
                actionCode = "Z";
                flowStateAgeValidation = FlowStateAgeValidation.ActionSendGoodsEvent;
            }
            else if (String.Compare(dialogResult, GlobalTexts.TooYoung, StringComparison.Ordinal) == 0)
            {
                causeMeasure.CauseCode = CauseCodeTooYoung;
                flowStateAgeValidation = FlowStateAgeValidation.FlowCauseAndMeasure;
            }
            else
                Logger.LogEvent(Severity.Error, "Encountered unhandled event in ActionCommandsAgeValidation.ValidatePopResult");

            return flowStateAgeValidation;
        }

    }

}

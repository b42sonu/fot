﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

// US 77d: http://kp-confluence.postennorge.no/display/FOT/77d+Age+validation+Vinmonopolet
namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.AgeValidation
{
    public enum FlowStateAgeValidation
    {
        ActionShowPopUp1,
        ActionShowPopUp2,
        ActionSendGoodsEvent,
        ActionBackFromFlowCauseAndMeasure,
        FlowCommands,
        FlowCauseAndMeasure,
        ThisFlowComplete,
    }

    public class FlowResultAgeValidation : BaseFlowResult
    {
    }

    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowAgeValidation : BaseFlow
    {
        private MessageHolder _messageHolder;
        private FlowResultAgeValidation _flowResult;
        private readonly ActionCommandsAgeValidation _actionCommands;
        private FlowDataVasProcessSupport _flowData;
        private string _causeCode;
        private string _actionCode;
        private string _comment;
        private CauseMeasure _causeMeasure;

        public FlowAgeValidation()
        {
            _flowResult = new FlowResultAgeValidation();
            _messageHolder = new MessageHolder();
            _actionCommands = new ActionCommandsAgeValidation(CommunicationClient.Instance);
            _flowData = new FlowDataVasProcessSupport();
            _causeMeasure = new CauseMeasure();


        }
        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowAgeValidation");
            _flowData = (FlowDataVasProcessSupport)flowData;

            ExecuteState(FlowStateAgeValidation.ActionShowPopUp1);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateAgeValidation)state);
        }

        public void ExecuteState(FlowStateAgeValidation stateAgeValidation)
        {

            if (stateAgeValidation > FlowStateAgeValidation.FlowCommands)
            {
                ExecuteFlowState(stateAgeValidation);
            }
            else
            {
                ExecuteActionState(stateAgeValidation);
            }
        }

        private void ExecuteFlowState(FlowStateAgeValidation state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowAgeValidation;" + state);

                switch (state)
                {
                    case FlowStateAgeValidation.FlowCauseAndMeasure:
                        var flowDataCauseAndMeasure = new FlowDataCauseAndMeasure
                        {
                            Comment = string.Empty,
                            SelectedReasonAndActionCombination = _causeMeasure,
                            HeaderText = GlobalTexts.DeliveryToConsignee,
                            IsCallFromVasMatrixSubprocess = true
                        };
                        StartSubFlow<FlowCauseAndMeasure>(flowDataCauseAndMeasure, (int)FlowStateAgeValidation.ActionBackFromFlowCauseAndMeasure, Process.AgeValidation);
                        break;

                    case FlowStateAgeValidation.ThisFlowComplete:
                        EndFlow();
                        break;

                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowAgeValidation.ExecuteFlowState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateAgeValidation.ThisFlowComplete);
            }
        }




        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateAgeValidation state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowAgeValidation;" + state);
                    switch (state)
                    {
                        case FlowStateAgeValidation.ActionShowPopUp1:
                            state = _actionCommands.ShowPopUp(GlobalTexts.TheConsignmentItemContainsAlcoholetc, ref _causeMeasure, ref _flowResult, out _causeCode, out _actionCode, GlobalTexts.Yes, GlobalTexts.Back, GlobalTexts.No);
                            break;
                        case FlowStateAgeValidation.ActionShowPopUp2:
                            state = _actionCommands.ShowPopUp(GlobalTexts.CheckIdPapersToVerifyAge, ref _causeMeasure, ref _flowResult, out _causeCode, out _actionCode, GlobalTexts.Above25, GlobalTexts.TooYoung, GlobalTexts.Below25);
                            break;

                        case FlowStateAgeValidation.ActionBackFromFlowCauseAndMeasure:
                            bool isValid = _actionCommands.BackFromFlowCauseAndMeasure((FlowResultCauseAndMeasure)SubflowResult, out _messageHolder, out _causeCode, out _actionCode, out _comment);
                            _flowResult.State = isValid ? FlowResultState.Ok : FlowResultState.Cancel;
                            state = isValid ? FlowStateAgeValidation.ActionSendGoodsEvent : FlowStateAgeValidation.ThisFlowComplete;
                            break;
                        case FlowStateAgeValidation.ActionSendGoodsEvent:

                            string eventCode = _causeCode == ActionCommandsAgeValidation.CauseCodeNo || _causeCode == ActionCommandsAgeValidation.CauseCodeTooYoung
                                                   ? "H"
                                                   : "V";
                            _actionCommands.SendDocShownGoodsEvent(ModelMain.SelectedOperationProcess, _flowData.CurrentConsignmentEntity, _causeCode,
                                                                   _actionCode, _comment, "1082", eventCode);
                            _flowResult.State = _causeCode == ActionCommandsAgeValidation.CauseCodeNo || _causeCode == ActionCommandsAgeValidation.CauseCodeTooYoung ? FlowResultState.Cancel : FlowResultState.Ok;
                            state = FlowStateAgeValidation.ThisFlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateAgeValidation.FlowCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowAgeValidation.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateAgeValidation.ThisFlowComplete;
            }


            ExecuteFlowState(state);
        }
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowAgeValidation ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

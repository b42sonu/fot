﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.EnterIdDetails;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.ValueAndRegisteredDelivery
{
    public enum FlowStateValueAndRegisteredDelivery
    {
        ActionSaveVasInformation,
        ActionIsConsignmentAuthorized,
        ActionGetMatchingItems,
        ViewCommands,
        ViewId,
        FlowComplete,
        FlowCancelled
    }

    public class FlowResultValueAndRegisteredDelivery : BaseFlowResult
    {
    }


    // US 77j : http://kp-confluence.postennorge.no/display/FOT/77j+-+Value+and+Registered+Delivery+in+FOT
    public class FlowValueAndRegisteredDelivery : BaseFlow
    {
        private FlowDataVasProcessSupport _flowData;
        private readonly ViewCommandsValueAndRegisteredDelivery _viewCommands;
        private readonly CommandsHandleScannedBarcodes _commandsHandleScannedBarcodes;
        private readonly FlowResultValueAndRegisteredDelivery _flowResult;
        private readonly MessageHolder _messageHolder;
        private string _idDetails;
        private string _idCode;
        private bool _authorizationUsed;
        private static readonly string[] ProductCodes = new[] { "2564", "2565" };
        private List<ConsignmentEntity> _listConsignments;
        private readonly ActionCommandsIdDetails _actionCommands;

        public FlowValueAndRegisteredDelivery()
        {
            _flowResult = new FlowResultValueAndRegisteredDelivery();
            _messageHolder = new MessageHolder();
            _viewCommands = new ViewCommandsValueAndRegisteredDelivery();
            _commandsHandleScannedBarcodes = new CommandsHandleScannedBarcodes();
            _actionCommands = new ActionCommandsIdDetails();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowValueAndRegisteredDelivery");
            _flowData = (FlowDataVasProcessSupport)flowData;
            if (_flowData != null && _flowData.CurrentConsignmentEntity != null && _flowData.EntityMap == null)
            {
                _flowData.EntityMap = new EntityMap();
                _commandsHandleScannedBarcodes.StoreScannedConsignmentEntity(_flowData.EntityMap, _flowData.CurrentConsignmentEntity);
            }
            ExecuteState(FlowStateValueAndRegisteredDelivery.ActionGetMatchingItems);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateValueAndRegisteredDelivery)state);
        }

        public void ExecuteState(FlowStateValueAndRegisteredDelivery stateId)
        {

            if (stateId > FlowStateValueAndRegisteredDelivery.ViewCommands)
            {
                ExecuteViewState(stateId);
            }
            else
            {
                ExecuteActionState(stateId);
            }
        }

        private void ExecuteViewState(FlowStateValueAndRegisteredDelivery stateId)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowValueAndRegisteredDelivery;" + stateId);

                switch (stateId)
                {
                    case FlowStateValueAndRegisteredDelivery.ViewId:
                        _viewCommands.ViewIdDetails(IdViewEventHandler, _flowData.EntityMap, _listConsignments.Count,  _messageHolder);
                        break;

                    case FlowStateValueAndRegisteredDelivery.FlowComplete:
                        EndFlow();
                        break;

                    case FlowStateValueAndRegisteredDelivery.FlowCancelled:
                        _flowResult.State = FlowResultState.Cancel;
                        EndFlow();
                        break;

                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowValueAndRegisteredDelivery.ExecuteFlowState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateValueAndRegisteredDelivery.FlowComplete);
            }
        }


        public void ExecuteActionState(FlowStateValueAndRegisteredDelivery stateId)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowIdContract;" + stateId);
                    switch (stateId)
                    {

                        case FlowStateValueAndRegisteredDelivery.ActionGetMatchingItems:
                            _listConsignments = _actionCommands.GetMatchingEntitiesForProductCode(_flowData.EntityMap, ProductCodes);
                            stateId = FlowStateValueAndRegisteredDelivery.ViewId;
                            break;

                        case FlowStateValueAndRegisteredDelivery.ActionIsConsignmentAuthorized:
                            var messageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning,
                                                                           GlobalTexts.ConsignmentDeliveredByAuthorization,
                                                                           Severity.Warning, GlobalTexts.Yes,
                                                                           GlobalTexts.No);
                            if (messageBoxResult == GlobalTexts.Yes)
                            {
                                _authorizationUsed = true;
                            }
                            stateId = FlowStateValueAndRegisteredDelivery.ActionSaveVasInformation;
                            break;

                        case FlowStateValueAndRegisteredDelivery.ActionSaveVasInformation:
                            _flowData.VasSubFlowResultInfo.IdType = _idCode;
                            _flowData.VasSubFlowResultInfo.RecipientId = _idDetails;
                            _flowData.VasSubFlowResultInfo.AuthorizationUsed = _authorizationUsed;
                            stateId = FlowStateValueAndRegisteredDelivery.FlowComplete;
                            break;
                    }
                } while (stateId < FlowStateValueAndRegisteredDelivery.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowIdContract.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                stateId = FlowStateValueAndRegisteredDelivery.FlowComplete;
            }


            ExecuteViewState(stateId);
        }

        private void IdViewEventHandler(int confirmIdViewEvents, params object[] data)
        {
            switch (confirmIdViewEvents)
            {
                case IdViewEvents.Ok:
                    _idDetails = data[0].ToString();
                    _idCode = data[1].ToString();
                    ExecuteState(FlowStateValueAndRegisteredDelivery.ActionIsConsignmentAuthorized);
                    break;
                case IdViewEvents.Back:
                    ExecuteState(FlowStateValueAndRegisteredDelivery.FlowCancelled);
                    break;

            }
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowValueAndRegisteredDelivery ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

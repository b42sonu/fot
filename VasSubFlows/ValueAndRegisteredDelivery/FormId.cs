﻿using System;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.ValueAndRegisteredDelivery
{

    public class FormDataId : BaseFormData
    {
        public int ItemCount { get; set; }
        public int LoadCarrierCount { get; set; }
        public MessageHolder MessageHolder { get; set; }
    }

    public partial class FormId : BaseForm
    {
        #region Private Vars
        private FormDataId _formData;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        private readonly MultiButtonControl _tabButtons;
        #endregion

        #region Constructor
        public FormId()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }
        #endregion

        public override void OnShow(IFormData iformData)
        {
            base.OnShow(iformData);
            ClearFields();
            _formData = (FormDataId)iformData;
            if (_formData != null)
            {
                lblNumberOfItems.Text = GlobalTexts.NumberOfConsignmentItems + " : " + _formData.ItemCount;
                lblNumberOfLoadCarriers.Text = GlobalTexts.NumberOfLoadCarriers + " : " + _formData.LoadCarrierCount;
                PopulateMessage(_formData.MessageHolder);
                _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled());
            }
            OnUpdateView(iformData);
        }

        #region Private Methods
        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric,
                                                                  KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };
            txtIdDetails.Tag = keyBoardProperty;

        }

        //Set text
        private void SetTextToGui()
        {
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, OkButton) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            lblModuleName.Text = GlobalTexts.IdConfirmation;
            lblRecieved.Text = GlobalTexts.YouHaveRecieved + " : ";
            lblNumberOfItems.Text = GlobalTexts.NumberOfConsignmentItems + " : ";
            lblIdDetails.Text = GlobalTexts.IdDetails;
            lblChkBankId.Text = GlobalTexts.EuropeanId;
            lblChkDriverLisence.Text = GlobalTexts.DriversLicence;
            lblChkPassport.Text = GlobalTexts.Passport;
            lblChkId.Text = GlobalTexts.PostenId;


            _tabButtons.GenerateButtons();
        }

        //Clear previously entered text
        private void ClearFields()
        {
            txtIdDetails.Text = string.Empty;
            chkEuropeanId.Checked = false;
            chkDriversLicence.Checked = false;
            chkPassport.Checked = false;
            chkPostenId.Checked = false;
        }

        //Populate Message
        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
            {
                if (messageHolder.State != MessageState.Empty)
                {
                    MsgMessage.ShowMessage(messageHolder);
                }
                else
                {
                    MsgMessage.ClearMessage();
                }
            }
        }

        //Check code for Seletected ID
        private string GetSelectedIdCode()
        {

            foreach (Control chk in touchPanel.Controls)
            {
                var checkBox = chk as CheckBox;
                if (checkBox != null)
                {
                    if (checkBox.Checked)
                    {
                        switch (checkBox.Name)
                        {
                            case "chkEuropeanId":
                                return "05";
                            case "chkDriversLicence":
                                return "02";
                            case "chkPostenId":
                                return "03";
                            case "chkPassport":
                                return "04";

                        }
                    }
                }
            }
            return "";
        }

        //Check if Id is filled
        private bool IsIdFilled()
        {
            return txtIdDetails.Text.Trim().Length > 0;
        }

        //Check validation of all fields on page
        private bool ValidateInputs()
        {

            if ((txtIdDetails.Text.Trim().Length < 2) || (txtIdDetails.Text.Trim().Length > 12))
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, String.Format(GlobalTexts.IdShouldBeOfLength, 2, 12),
                                          Severity.Error, GlobalTexts.Ok);
                return false;
            }

            if (!IsAnyCheckBoxChecked())
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.PleaseSelectAtLeastOne,
                                         Severity.Error, GlobalTexts.Ok);
                return false;
            }
            return true;
        }

        //Is any check box checked
        private bool IsAnyCheckBoxChecked()
        {
            if (touchPanel.Controls.OfType<CheckBox>().Any(checkBox => checkBox.Checked))
            {
                return true;
            }
            return false;
        }

        //Uncheck all checkboxes
        private void UnselectAllCheckBoxes(Control control)
        {
            foreach (Control chk in touchPanel.Controls)
            {
                var checkBox = chk as CheckBox;
                if (checkBox != null)
                {
                    if (chk.Name != control.Name)
                    {
                        checkBox.Checked = false;
                    }
                }
            }
        }
        #endregion

        #region EventHandlers

        private void BtnOkClick(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInputs())
                    ViewEvent.Invoke(IdViewEvents.Ok, txtIdDetails.Text, GetSelectedIdCode());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConfirmId.BtnOkClick");
            }
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(IdViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConfirmId.BtnBackClick");
            }
        }

        private void IdCheckBoxStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (((CheckBox)sender).Checked)
                {
                    UnselectAllCheckBoxes(sender as Control);
                    txtIdDetails.Text = String.Empty;
                }
                ScannedNumberTextChanged(false, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConfirmId.IdCheckBoxStateChanged");
            }
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled());
        }
        #endregion
    }

    public class IdViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }

}

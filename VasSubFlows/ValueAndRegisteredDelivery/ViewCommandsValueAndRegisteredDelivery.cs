﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.ValueAndRegisteredDelivery
{
    public class ViewCommandsValueAndRegisteredDelivery
    {
        internal void ViewIdDetails(ViewEventDelegate viewEventHandler, EntityMap entityMap, int itemCount, MessageHolder messageHolder)
        {
            var formData = new FormDataId
                {
                MessageHolder = messageHolder,
                ItemCount = itemCount,
                LoadCarrierCount = entityMap.ScannedLoadCarrierTotalCount,
            };
            var view = ViewCommands.ShowView<FormId>(formData);
            view.SetEventHandler(viewEventHandler);
        }
    }
}

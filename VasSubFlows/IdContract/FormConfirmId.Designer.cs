﻿using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.IdContract
{
    partial class FormConfirmId
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblChkBankId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkDriverLisence = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkPassport = new Resco.Controls.CommonControls.TransparentLabel();
            this.chkPassport = new System.Windows.Forms.CheckBox();
            this.txtConsigneeName = new PreCom.Controls.PreComInput2();
            this.lblIdDetails = new Resco.Controls.CommonControls.TransparentLabel();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.chkDriversLicence = new System.Windows.Forms.CheckBox();
            this.chkPostenId = new System.Windows.Forms.CheckBox();
            this.chkBankId = new System.Windows.Forms.CheckBox();
            this.txtIdDetails = new PreCom.Controls.PreComInput2();
            this.lblConsigneeName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNumberOfItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblIdDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfItems)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblNumberOfItems);
            this.touchPanel.Controls.Add(this.chkPassport);
            this.touchPanel.Controls.Add(this.txtIdDetails);
            this.touchPanel.Controls.Add(this.txtConsigneeName);
            this.touchPanel.Controls.Add(this.lblIdDetails);
            this.touchPanel.Controls.Add(this.lblOrgName);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.chkDriversLicence);
            this.touchPanel.Controls.Add(this.chkPostenId);
            this.touchPanel.Controls.Add(this.chkBankId);
            this.touchPanel.Controls.Add(this.lblConsigneeName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblChkBankId);
            this.touchPanel.Controls.Add(this.lblChkDriverLisence);
            this.touchPanel.Controls.Add(this.lblChkId);
            this.touchPanel.Controls.Add(this.lblChkPassport);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // chkPassport
            // 
            this.chkPassport.Location = new System.Drawing.Point(234, 125);
            this.chkPassport.Name = "chkPassport";
            this.chkPassport.BackColor = System.Drawing.Color.White;
            this.chkPassport.Size = new System.Drawing.Size(30, 30);
            this.chkPassport.TabIndex = 33;
            this.chkPassport.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // lblChkPassport
            // 
            this.lblChkPassport.Location = new System.Drawing.Point(270, 125);
            this.lblChkPassport.Name = "lblRecieved";
            this.lblChkPassport.Size = new System.Drawing.Size(200, 29);
            // 
            // txtConsigneeName
            // 
            this.txtConsigneeName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtConsigneeName.Location = new System.Drawing.Point(22, 337);
            this.txtConsigneeName.Name = "txtConsigneeName";
            this.txtConsigneeName.Size = new System.Drawing.Size(436, 48);
            this.txtConsigneeName.TabIndex = 35;
            this.txtConsigneeName.TextTranslation = false;
            // 
            // lblIdDetails
            // 
            this.lblIdDetails.Location = new System.Drawing.Point(22, 227);
            this.lblIdDetails.Name = "lblIdDetails";
            this.lblIdDetails.Size = new System.Drawing.Size(101, 29);
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(22, 403);
            this.MsgMessage.MessageText = "";
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(436, 93);
            this.MsgMessage.TabIndex = 33;
            // 
            // chkDriversLicence
            // 
            this.chkDriversLicence.Location = new System.Drawing.Point(22, 125);
            this.chkDriversLicence.BackColor = System.Drawing.Color.White;
            this.chkDriversLicence.Name = "chkDriversLicence";
            this.chkDriversLicence.Size = new System.Drawing.Size(30, 30);
            this.chkDriversLicence.TabIndex = 30;
            this.chkDriversLicence.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // lblChkDriverLisence
            // 
            this.lblChkDriverLisence.Location = new System.Drawing.Point(60, 125);
            this.lblChkDriverLisence.Name = "lblChkDriverLisence";
            this.lblChkDriverLisence.Size = new System.Drawing.Size(190, 29);
            // 
            // chkPostenId
            // 
            this.chkPostenId.Location = new System.Drawing.Point(234, 90);
            this.chkPostenId.Name = "chkPostenId";
            this.chkPostenId.BackColor = System.Drawing.Color.White;
            this.chkPostenId.Size = new System.Drawing.Size(30, 30);
            this.chkPostenId.TabIndex = 29;
            this.chkPostenId.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // lblChkDriverLisence
            // 
            this.lblChkId.Location = new System.Drawing.Point(270, 90);
            this.lblChkId.Name = "lblChkId";
            this.lblChkId.Size = new System.Drawing.Size(200, 29);
            // 
            // chkBankId
            // 
            this.chkBankId.Location = new System.Drawing.Point(22, 90);
            this.chkBankId.Name = "chkBankId";
            this.chkBankId.BackColor = System.Drawing.Color.White;
            this.chkBankId.Size = new System.Drawing.Size(30, 30);
            this.chkBankId.TabIndex = 28;
            this.chkBankId.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // lblChkBankId
            // 
            this.lblChkBankId.Location = new System.Drawing.Point(60, 90);
            this.lblChkBankId.Name = "lblChkBankId";
            this.lblChkBankId.Size = new System.Drawing.Size(190, 29);
            // 
            // txtIdDetails
            // 
            this.txtIdDetails.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtIdDetails.Location = new System.Drawing.Point(22, 257);
            this.txtIdDetails.Name = "txtIdDetails";
            this.txtIdDetails.Size = new System.Drawing.Size(336, 48);
            this.txtIdDetails.TabIndex = 34;
            this.txtIdDetails.TextTranslation = false;
            // 
            // lblConsigneeName
            // 
            this.lblConsigneeName.Location = new System.Drawing.Point(22, 307);
            this.lblConsigneeName.Name = "lblConsigneeName";
            this.lblConsigneeName.Size = new System.Drawing.Size(178, 29);
            //
            // lblOrgName
            // 
            this.lblOrgName.AutoSize = false;
            this.lblOrgName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgName.Name = "lblOrgName";
            this.lblOrgName.Size = new System.Drawing.Size(477, 33);
            this.lblOrgName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(15, 56);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(0, 0);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            //// 
            //// lblRecieved
            //// 
            //this.lblRecieved.Location = new System.Drawing.Point(22, 150);
            //this.lblRecieved.Name = "lblRecieved";
            //this.lblRecieved.Size = new System.Drawing.Size(218, 29);
            // 
            // lblNumberOfItems
            // 
            this.lblNumberOfItems.Location = new System.Drawing.Point(22, 184);
            this.lblNumberOfItems.Name = "lblNumberOfItems";
            this.lblNumberOfItems.Size = new System.Drawing.Size(343, 29);

            // 
            // FormConfirmId
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormConfirmId";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblIdDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfItems)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 txtIdDetails;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private System.Windows.Forms.CheckBox chkDriversLicence;
        private System.Windows.Forms.CheckBox chkPostenId;
        private System.Windows.Forms.CheckBox chkBankId;
        private System.Windows.Forms.CheckBox chkPassport;
        private PreCom.Controls.PreComInput2 txtConsigneeName;
        private MessageControl MsgMessage;
        private TransparentLabel lblIdDetails;
        private TransparentLabel lblNumberOfItems;
        private TransparentLabel lblChkPassport;
        private TransparentLabel lblChkId;
        private TransparentLabel lblChkDriverLisence;
        private TransparentLabel lblChkBankId;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgName;

    }
}
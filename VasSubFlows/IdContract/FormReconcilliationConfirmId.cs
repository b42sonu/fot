﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.IdContract
{
    public class FormDataReconcilliationConfirmId : BaseFormData
    {
        public List<ConsignmentEntity> ListConsignmentEntity { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public bool ShowBackButton { get; set; }
    }

    public partial class FormReconcilliationConfirmId : BaseForm
    {
        private FormDataReconcilliationConfirmId _formData;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        private readonly MultiButtonControl _tabButtons;
        public FormReconcilliationConfirmId()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetStandardControlProperties(labelModuleName, lblOrgName, null, null, null, null, null);
        }

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.DeliveryToConsignee;
            if (ModelUser.UserProfile.OrgUnitName != null) lblOrgName.Text = ModelUser.UserProfile.OrgUnitName;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, OkButton) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        public override void OnShow(IFormData iformData)
        {
            _formData = (FormDataReconcilliationConfirmId)iformData;
            if (_formData != null)
                labelModuleName.Text = _formData.HeaderText;
            labelListHeader.Text = GlobalTexts.ContractedItems;
            BindList();
        }

        private static void ShowDialog(MessageHolder messageHolder, ViewEventDelegate viewEvent)
        {
            if (messageHolder.State == MessageState.Empty) return;
            var dialogWarningUserResponse = GuiCommon.ShowModalDialog(GlobalTexts.Warning, messageHolder.Text, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
            if (dialogWarningUserResponse == GlobalTexts.Yes)
            {
                viewEvent.Invoke(ReconcilliationConfirmIdViewEvents.Yes);
            }
            else
            {
                var dialogContinueUserResponse = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.ConsignmentRefusedByConsignee, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
                viewEvent.Invoke(dialogContinueUserResponse == GlobalTexts.Yes
                                     ? ReconcilliationConfirmIdViewEvents.Refused
                                     : ReconcilliationConfirmIdViewEvents.Back);
            }
        }


        private void BindList()
        {
            if (_formData != null && _formData.ListConsignmentEntity != null)
            {
                ListConsignments.BeginUpdate();
                ListConsignments.DataRows.Clear();
                foreach (ConsignmentEntity consignmentEntity in _formData.ListConsignmentEntity)
                {
                    if (consignmentEntity is ConsignmentItem)
                    {
                        var colTextValues = new string[2];
                        colTextValues[0] = consignmentEntity.EntityId;//StringUtil.GetTextForConsignmentEntity(consignmentEntity.EntityDisplayId, false);
                        colTextValues[1] = consignmentEntity.EntityId;
                        var insertRow = new Row(2, 1, colTextValues);
                        ListConsignments.DataRows.Add(insertRow);
                    }
                    else if (consignmentEntity is LoadCarrier)
                    {
                        var colTextValues = new string[2];
                        colTextValues[0] = consignmentEntity.EntityDisplayId;
                        colTextValues[1] = consignmentEntity.EntityId;
                        var insertRow = new Row(2, 1, colTextValues);
                        ListConsignments.DataRows.Add(insertRow);
                    }
                }
                ListConsignments.ActiveRowIndex = 0;
                ListConsignments.EndUpdate();
            }
        }

       

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ReconcilliationConfirmIdViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliationConfirmId.ButtonBackClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                ShowDialog(_formData.MessageHolder, ViewEvent);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliationConfirmId.ButtonOkClick");
            }
        }
    }

    public class ReconcilliationConfirmIdViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int No = 2;
        public const int Yes = 3;
        public const int Refused = 4;
    }
}
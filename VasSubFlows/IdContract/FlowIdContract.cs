﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.AgeValidation;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.IdContract
{
    public enum FlowStateIdContract
    {
        ActionRefusedByConsignee,
        ActionGetMatchingVasEntity,
        ActionValidateDetails,
        ActionSendGoodsEvent,
        ActionBackFromScanBarcode,
        ActionDeleteItems,
        ViewCommands,
        ViewDeliverToConsignee,
        ViewAttemptedDelivery,
        ViewIdConfirmation,
        FlowComplete,
    }



    public class FlowResultIdContract : BaseFlowResult
    {
    }

    // US 77b: http://kp-confluence.postennorge.no/display/FOT/77b+ID-Contract+process+support
    public class FlowIdContract : BaseFlow
    {
        private FlowDataVasProcessSupport _flowData;
        private readonly CommandsHandleScannedBarcodes _commandsHandleScannedBarcodes;
        private readonly ActionCommandsAgeValidation _actionCommandsAgeValidation;
        private readonly FlowResultIdContract _flowResultId;
        private readonly MessageHolder _messageHolder;
        private readonly ViewCommandsIdContract _viewCommandsId;
        private const string VasCode = "1063";
        private FlowScanBarcode _flowScanBarcodeInstance;
        private readonly ActionCommandsConfirmId _actionCommands;
        private List<ConsignmentEntity> _listCurrentVasConsignments;
        private string _recipientName;
        private string _idDetails;
        private readonly EntityMap _entityMap;
        private string _idCode;

        private bool _isOnConfirmView;

        public FlowIdContract()
        {

            _flowResultId = new FlowResultIdContract();
            _messageHolder = new MessageHolder();
            _viewCommandsId = new ViewCommandsIdContract();
            _entityMap = new EntityMap();
            _actionCommands = new ActionCommandsConfirmId();
            _actionCommandsAgeValidation = new ActionCommandsAgeValidation(CommunicationClient.Instance);
            _commandsHandleScannedBarcodes = new CommandsHandleScannedBarcodes();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowIdContract");
            _flowData = (FlowDataVasProcessSupport)flowData;
            if (_flowData != null && _flowData.CurrentConsignmentEntity != null && _flowData.EntityMap == null)
            {
                _flowData.EntityMap = new EntityMap();
                _commandsHandleScannedBarcodes.StoreScannedConsignmentEntity(_flowData.EntityMap, _flowData.CurrentConsignmentEntity);
            }

            if (_flowData != null && (_flowData.EntityMap == null || !(_flowData.EntityMap.Count > 0)))
            {
                GuiCommon.ShowModalDialog("No consignments found", "No consignments found", Severity.Warning,
                                          GlobalTexts.Ok);
                ExecuteState(FlowStateIdContract.FlowComplete);
            }
            else
            {
                ExecuteState(FlowStateIdContract.ActionGetMatchingVasEntity);
            }
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateIdContract)state);
        }

        public void ExecuteState(FlowStateIdContract stateIdContract)
        {

            if (stateIdContract > FlowStateIdContract.ViewCommands)
            {
                ExecuteViewState(stateIdContract);
            }
            else
            {
                ExecuteActionState(stateIdContract);
            }
        }

        private void ExecuteViewState(FlowStateIdContract state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowIdContract;" + state);

                switch (state)
                {
                    case FlowStateIdContract.ViewDeliverToConsignee:
                        _viewCommandsId.ViewDeliverToConsignee(ReconcilliationViewEventHandler, _listCurrentVasConsignments, _messageHolder, _flowData.HeaderText);
                        break;

                    case FlowStateIdContract.ViewAttemptedDelivery:
                        _viewCommandsId.ViewAttemptedDelivery(ReconcilliationViewEventHandler, _listCurrentVasConsignments, _messageHolder);
                        break;

                    case FlowStateIdContract.ViewIdConfirmation:
                        _viewCommandsId.ViewIdConfirmation(ConfirmIdViewEventHandler, _idDetails, _messageHolder, _listCurrentVasConsignments.Count);
                        break;

                    case FlowStateIdContract.FlowComplete:
                        EndFlow();
                        break;
                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowIdContract.ExecuteFlowState");
                _flowResultId.State = FlowResultState.Exception;
                ExecuteState(FlowStateIdContract.FlowComplete);
            }
        }

        public void ExecuteActionState(FlowStateIdContract state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowIdContract;" + state);
                    switch (state)
                    {

                        case FlowStateIdContract.ActionGetMatchingVasEntity:
                            _listCurrentVasConsignments = _actionCommands.GetMatchingVasCodeConsignmentEntities(_flowData.EntityMap, VasCode);
                            state = FlowStateIdContract.ViewDeliverToConsignee;
                            break;

                        case FlowStateIdContract.ActionRefusedByConsignee:
                            state = FlowStateIdContract.ActionDeleteItems;
                            break;

                        case FlowStateIdContract.ActionDeleteItems:
                            _actionCommandsAgeValidation.DeleteVasItemFromEntityMap(_flowData.EntityMap, VasCode);
                            _flowResultId.State = FlowResultState.Cancel;
                            state = FlowStateIdContract.FlowComplete;
                            break;

                        case FlowStateIdContract.ActionBackFromScanBarcode:
                            state = _actionCommands.ValidateFlowScanBarcodeForVas((FlowResultScanBarcode)SubflowResult, BarcodeType.DriversLicence, _messageHolder, out _idDetails);
                            break;

                        case FlowStateIdContract.ActionSendGoodsEvent:
                            _actionCommands.SetVasParameters(_idDetails, _idCode, _recipientName, _flowData.VasSubFlowResultInfo);
                            _flowResultId.State = FlowResultState.Ok;
                            state = FlowStateIdContract.FlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateIdContract.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowIdContract.ExecuteActionState");
                _flowResultId.State = FlowResultState.Exception;
                state = FlowStateIdContract.FlowComplete;
            }


            ExecuteViewState(state);
        }


        private void ReconcilliationViewEventHandler(int reconcilliationViewEvents, params object[] data)
        {
            switch (reconcilliationViewEvents)
            {
                case ReconcilliationConfirmIdViewEvents.Ok:
                    _messageHolder.Clear();
                    ExecuteState(_isOnConfirmView
                                     ? FlowStateIdContract.ActionRefusedByConsignee
                                     : FlowStateIdContract.ViewIdConfirmation);
                    break;

                case ReconcilliationConfirmIdViewEvents.Back:
                    _flowResultId.State = FlowResultState.Cancel;
                    ExecuteState(FlowStateIdContract.FlowComplete);
                    break;

                case ReconcilliationConfirmIdViewEvents.No:
                    _isOnConfirmView = true;
                    ExecuteState(FlowStateIdContract.ViewAttemptedDelivery);
                    break;

                case ReconcilliationConfirmIdViewEvents.Yes:
                    _messageHolder.Clear();
                    ExecuteState(FlowStateIdContract.ViewIdConfirmation);
                    break;

                case ReconcilliationConfirmIdViewEvents.Refused:
                    _messageHolder.Clear();
                    ExecuteState(FlowStateIdContract.ActionRefusedByConsignee);
                    break;
            }
        }

        private void ConfirmIdViewEventHandler(int confirmIdViewEvents, params object[] data)
        {
            switch (confirmIdViewEvents)
            {
                case ConfirmIdViewEvents.Ok:
                    _idDetails = data[0].ToString();
                    _recipientName = data[1].ToString();
                    _idCode = data[2].ToString();
                    ExecuteState(FlowStateIdContract.ActionSendGoodsEvent);
                    break;

                case ConfirmIdViewEvents.Back:
                    ExecuteState(FlowStateIdContract.ViewDeliverToConsignee);
                    break;

                case ConfirmIdViewEvents.BarcodeScanned:
                    GoToFlowScanBarcode(BarcodeType.DriversLicence, FlowStateIdContract.ActionBackFromScanBarcode);//
                    _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                    _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
                    break;
            }
        }

        private void GoToFlowScanBarcode(BarcodeType barcodeType, FlowStateIdContract stateId)
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.Vas, _messageHolder, barcodeType, _entityMap)
            {
                HaveCustomForm = true
            };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)stateId, Process.Inherit);
        }


        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowIdContract ");
            BaseModule.EndSubFlow(_flowResultId);
        }
    }
}

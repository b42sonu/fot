﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.IdContract
{
    class ActionCommandsConfirmId
    {
        //TODO :: Write Unit Test
        internal FlowStateIdContract ValidateFlowScanBarcodeForVas(FlowResultScanBarcode subflowResult, BarcodeType validTypes, MessageHolder messageHolder, out string licenceId)
        {
            var state = new FlowStateIdContract();
            licenceId = null;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    if ((subflowResult.BarcodeType & validTypes) == BarcodeType.Unknown)
                    {
                        messageHolder.Update(GlobalTexts.IllegalBarcode, messageHolder.State);
                    }
                    else
                    {
                        messageHolder.Clear();
                        if (!String.IsNullOrEmpty(subflowResult.LicenceId))
                        {
                            licenceId = subflowResult.LicenceId;
                            state = FlowStateIdContract.ViewIdConfirmation;
                        }
                    }
                    break;
                case FlowResultState.Error:
                    Logger.LogEvent(Severity.Debug, "ValidateFlowScanBarcodeForVas;FlowResultState.Error");
                    messageHolder.Update(subflowResult.MessageHolder.Text, subflowResult.MessageHolder.State);
                    state = FlowStateIdContract.ViewIdConfirmation;
                    break;
            }

            return state;
        }

        /// <summary>
        /// Get Matching 
        /// </summary>
        internal List<ConsignmentEntity> GetMatchingVasCodeConsignmentEntities(EntityMap entityMap, string vasCode)
        {
            return entityMap.GetConsignmentsWithoutEntities().OfType<ConsignmentItem>().Where(consignmentEntity => IsVasCodeContains(consignmentEntity.Vas, vasCode)).Cast<ConsignmentEntity>().ToList();
        }

        internal bool IsVasCodeContains(VASType[] vas, string vasCode)
        {
            return vas != null && vas.Count(c => c.VASCode == vasCode) > 0;
        }

        public void SetVasParameters(string idDetails, string idCode, string recipientName, VasSubFlowResultInfo vasSubFlowResultInfo)
        {
            vasSubFlowResultInfo.IdType = idCode;
            vasSubFlowResultInfo.RecipientId = idDetails;
            vasSubFlowResultInfo.RecipientName = recipientName;
        }
    }
}

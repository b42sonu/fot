﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.IdContract
{
    class ViewCommandsIdContract
    {

        internal void ViewDeliverToConsignee(ViewEventDelegate viewEventHandler, List<ConsignmentEntity> listEntity, MessageHolder messageHolder,string headerText)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsIdContract.ViewDeliverToConsignee()");

            messageHolder.Update(GlobalTexts.SignOffAppliesTo, MessageState.Warning);

            var formData = new FormDataReconcilliationConfirmId
                {
                    ListConsignmentEntity = listEntity,
                    MessageHolder = messageHolder,
                    ShowBackButton = true,
                    HeaderText = headerText
                };

            var view = ViewCommands.ShowView<FormReconcilliationConfirmId>(formData);
            view.SetEventHandler(viewEventHandler);
        }

        internal void ViewAttemptedDelivery(ViewEventDelegate viewEventHandler, List<ConsignmentEntity> listConsignment, MessageHolder messageHolder)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsIdContract.ViewAttemptedDelivery()");

            messageHolder.Update(GlobalTexts.ConsignmentRefusedByConsignee, MessageState.Warning);

            var formData = new FormDataReconcilliationConfirmId
                {
                    ListConsignmentEntity = listConsignment,
                MessageHolder = messageHolder,
                ShowBackButton = false
            };

            var view = ViewCommands.ShowView<FormReconcilliationConfirmId>(formData);
            view.SetEventHandler(viewEventHandler);
        }

        internal void ViewIdConfirmation(ViewEventDelegate viewEventHandler, string licenceId, MessageHolder messageHolder, int itemCount)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsIdContract.ViewIdConfirmation()");
            var formData = new FormDataConfirmId
                {
                LicenceId = licenceId,
                MessageHolder = messageHolder,
                ItemCount = itemCount
            };

            var view = ViewCommands.ShowView<FormConfirmId>(formData);
            view.SetEventHandler(viewEventHandler);
        }

    }
}

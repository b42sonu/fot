﻿using System;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.IdContract
{

    public class FormDataConfirmId : BaseFormData
    {
        public int ItemCount { get; set; }
        public string LicenceId { get; set; }
        public MessageHolder MessageHolder { get; set; }
    }

    public partial class FormConfirmId : BaseBarcodeScanForm
    {
        private FormDataConfirmId _formData;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        private readonly MultiButtonControl _tabButtons;
        public FormConfirmId()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
            SetStandardControlProperties(lblHeading, lblOrgName, null, null, null, null, null);
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToGui()
        {
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, OkButton) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            lblHeading.Text = GlobalTexts.IdConfirmation;
            if (ModelUser.UserProfile.OrgUnitName != null)
                lblOrgName.Text = ModelUser.UserProfile.OrgUnitName;
            //lblRecieved.Text = GlobalTexts.YouHaveRecieved + " : ";
            lblNumberOfItems.Text = GlobalTexts.NumberOfConsignmentItems + ": ";
            lblChkBankId.Text = GlobalTexts.BankId;
            lblChkDriverLisence.Text = GlobalTexts.DriversLicence;
            lblChkPassport.Text = GlobalTexts.Passport;
            lblChkId.Text = GlobalTexts.PostenId;
            lblIdDetails.Text = GlobalTexts.IdDetails + ":";
            lblConsigneeName.Text = GlobalTexts.ConsigneeName + ":";

            _tabButtons.GenerateButtons();
        }

        public override void OnShow(IFormData iformData)
        {
            base.OnShow(iformData);
            ClearFields();
            _formData = (FormDataConfirmId)iformData;
            if (_formData != null)
            {
                lblNumberOfItems.Text = GlobalTexts.NumberOfConsignmentItems + ": " + _formData.ItemCount;
                PopulateMessage(_formData.MessageHolder);
                if (!String.IsNullOrEmpty(_formData.LicenceId))
                {
                    chkDriversLicence.Checked = true;
                    txtIdDetails.Text = _formData.LicenceId;
                }
                _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled() && IsConsigneeNameFilled());
            }
            OnUpdateView(iformData);
        }

        private void ClearFields()
        {
            txtConsigneeName.Text = string.Empty;
            txtIdDetails.Text = string.Empty;
            chkBankId.Checked = false;
            chkDriversLicence.Checked = false;
            chkPassport.Checked = false;
            chkPostenId.Checked = false;
        }

        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
            {
                if (messageHolder.State != MessageState.Empty)
                {
                    MsgMessage.ShowMessage(messageHolder);
                }
                else
                {
                    MsgMessage.ClearMessage();
                }
            }
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInputs())
                    ViewEvent.Invoke(ConfirmIdViewEvents.Ok, txtIdDetails.Text, txtConsigneeName.Text, GetSelectedIdCode());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConfirmId.BtnOkClick");
            }
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ConfirmIdViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConfirmId.BtnBackClick");
            }
        }

        private string GetSelectedIdCode()
        {

            foreach (Control chk in touchPanel.Controls)
            {
                var checkBox = chk as CheckBox;
                if (checkBox != null)
                {
                    if (checkBox.Checked)
                    {
                        switch (checkBox.Name)
                        {
                            case "chkBankId":
                                return "01";
                            case "chkDriversLicence":
                                return "02";
                            case "chkPostenId":
                                return "03";
                            case "chkPassport":
                                return "04";

                        }
                    }
                }
            }
            return "";
        }

        private bool ValidateInputs()
        {

            if (IsValidId() == false)
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, String.Format(GlobalTexts.IdShouldBeOfLength, 2, 12),
                                          Severity.Error, GlobalTexts.Ok);
                txtIdDetails.Focus();
                return false;
            }
            if (IsValidConsigneeName() == false)
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, String.Format(GlobalTexts.ConsigneeNameLength, 2, 28),
                                          Severity.Error, GlobalTexts.Ok);
                txtConsigneeName.Focus();
                return false;
            }
            if (IsAnyCheckBoxChecked() == false)
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.PleaseSelectAtLeastOne,
                                         Severity.Error, GlobalTexts.Ok);
                chkBankId.Focus();
                return false;
            }
            return true;
        }

        private bool IsValidConsigneeName()
        {
            return (txtConsigneeName.Text.Trim().Length >= 2) && (txtConsigneeName.Text.Trim().Length <= 28);
        }
        private bool IsConsigneeNameFilled()
        {
            return txtConsigneeName.Text.Trim().Length > 0;
        }

        private bool IsValidId()
        {
            return (txtIdDetails.Text.Trim().Length >= 2) && (txtIdDetails.Text.Trim().Length <= 12);
        }

        private bool IsIdFilled()
        {
            return txtIdDetails.Text.Trim().Length > 0;
        }

        /// <summary>
        /// returns true if any check box is checked
        /// </summary>
        /// <returns></returns>
        private bool IsAnyCheckBoxChecked()
        {
            if (touchPanel.Controls.OfType<CheckBox>().Any(checkBox => checkBox.Checked))
            {
                return true;
            }
            return false;
        }


        private void IdCheckBoxStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (((CheckBox)sender).Checked)
                {
                    UnselectAll(sender as Control);
                    txtIdDetails.Text = String.Empty;
                }
                ScannedNumberTextChanged(false, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConfirmId.IdCheckBoxStateChanged");
            }
        }

        /// <summary>
        /// Uncheck all other checkboxes
        /// </summary>
        /// <param name="control"></param>
        private void UnselectAll(Control control)
        {
            foreach (Control chk in touchPanel.Controls)
            {
                var checkBox = chk as CheckBox;
                if (checkBox != null)
                {
                    if (chk.Name != control.Name)
                    {
                        checkBox.Checked = false;
                    }
                }
            }
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            ViewEvent.Invoke(ConfirmIdViewEvents.BarcodeScanned, scannerOutput);
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric,
                                                                  KeyPadTypes.Qwerty)
                                       {
                                           TextChanged = ScannedNumberTextChanged
                                       };
            txtIdDetails.Tag = keyBoardProperty;


            txtConsigneeName.Tag = new OnScreenKeyboardProperties(GlobalTexts.EnterConsigneeName, KeyPadTypes.Qwerty)
                                       {
                                           TextChanged = ScannedNumberTextChangedForConsignee
                                       };
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            if (MsgMessage != null)
                MsgMessage.ClearMessage();
            _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled() && IsConsigneeNameFilled());
        }

        private void ScannedNumberTextChangedForConsignee(bool userFinishedTyping, string textentered)
        {
            _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled() && IsConsigneeNameFilled());
        }

    }

    public class ConfirmIdViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
    }

    public enum IdType
    {
        BankId = 01,
        DriversLicence = 02,
        PostenId = 03,
        Passport = 04
    }
}
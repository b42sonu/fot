﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.EnterIdDetails
{
    class ViewCommandsIdDetails
    {



        internal void ViewIdDetails(ViewEventDelegate viewEventHandler, string licenceId, MessageHolder messageHolder, EntityMap entityMap, string vasMessage , int itemCount)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsIdDetails.ViewIdConfirmation()");
            var formData = new FormDataIdDetails
                {
                    VasMessage = vasMessage,
                    MessageHolder = messageHolder,
                    ItemCount = itemCount,
                    LoadCarrierCount = entityMap.ScannedLoadCarrierTotalCount,
                    LicenceId = licenceId,
                };

            var view = ViewCommands.ShowView<FormIdDetails>(formData);
            view.SetEventHandler(viewEventHandler);
        }

    }
}

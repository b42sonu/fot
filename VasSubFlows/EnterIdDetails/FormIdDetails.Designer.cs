﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.EnterIdDetails
{
    partial class FormIdDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblItemCount = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNumberOfItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkBankId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkDriverLisence = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChkPassport = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNumberOfLoadCarriers = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblRecieved = new Resco.Controls.CommonControls.TransparentLabel();
            this.chkPassport = new System.Windows.Forms.CheckBox();
            this.txtIdDetails = new PreCom.Controls.PreComInput2();
            this.txtConsigneeName = new PreCom.Controls.PreComInput2();
            this.lblIdDetails = new Resco.Controls.CommonControls.TransparentLabel();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.chkDriversLicence = new System.Windows.Forms.CheckBox();
            this.chkPostenId = new System.Windows.Forms.CheckBox();
            this.chkBankId = new System.Windows.Forms.CheckBox();
            this.lblConsigneeName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfLoadCarriers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecieved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIdDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblItemCount);
            this.touchPanel.Controls.Add(this.lblNumberOfItems);
            this.touchPanel.Controls.Add(this.lblNumberOfLoadCarriers);
            this.touchPanel.Controls.Add(this.lblRecieved);
            this.touchPanel.Controls.Add(this.chkPassport);
            this.touchPanel.Controls.Add(this.txtIdDetails);
            this.touchPanel.Controls.Add(this.txtConsigneeName);
            this.touchPanel.Controls.Add(this.lblIdDetails);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.chkDriversLicence);
            this.touchPanel.Controls.Add(this.chkPostenId);
            this.touchPanel.Controls.Add(this.chkBankId);
            this.touchPanel.Controls.Add(this.lblConsigneeName);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblChkBankId);
            this.touchPanel.Controls.Add(this.lblChkDriverLisence);
            this.touchPanel.Controls.Add(this.lblChkId);
            this.touchPanel.Controls.Add(this.lblChkPassport);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblItemCount
            // 
            this.lblItemCount.Location = new System.Drawing.Point(233, 172);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(14, 29);
            // 
            // lblNumberOfItems
            // 
            this.lblNumberOfItems.Location = new System.Drawing.Point(14, 172);
            this.lblNumberOfItems.Name = "lblNumberOfItems";
            this.lblNumberOfItems.Size = new System.Drawing.Size(181, 29);
            // 
            // lblNumberOfLoadCarriers
            // 
            this.lblNumberOfLoadCarriers.Location = new System.Drawing.Point(14, 204);
            this.lblNumberOfLoadCarriers.Name = "lblNumberOfLoadCarriers";
            this.lblNumberOfLoadCarriers.Size = new System.Drawing.Size(260, 29);
            // 
            // lblRecieved
            // 
            this.lblRecieved.Location = new System.Drawing.Point(15, 136);
            this.lblRecieved.Name = "lblRecieved";
            this.lblRecieved.Size = new System.Drawing.Size(211, 29);

            // 
            // lblChkPassport
            // 
            this.lblChkPassport.Location = new System.Drawing.Point(270, 125);
            this.lblChkPassport.Name = "lblRecieved";
            this.lblChkPassport.Size = new System.Drawing.Size(200, 29);

            // 
            // lblChkId
            // 
            this.lblChkId.Location = new System.Drawing.Point(270, 90);
            this.lblChkId.Name = "lblChkId";
            this.lblChkId.Size = new System.Drawing.Size(200, 29);

            // 
            // lblChkBankId
            // 
            this.lblChkBankId.Location = new System.Drawing.Point(60, 90);
            this.lblChkBankId.Name = "lblChkBankId";
            this.lblChkBankId.Size = new System.Drawing.Size(190, 29);

            // 
            // lblChkDriverLisence
            // 
            this.lblChkDriverLisence.Location = new System.Drawing.Point(60, 125);
            this.lblChkDriverLisence.Name = "lblChkDriverLisence";
            this.lblChkDriverLisence.Size = new System.Drawing.Size(190, 29);
            // 
            // chkPassport
            // 
            this.chkPassport.Location = new System.Drawing.Point(234, 125);
            this.chkPassport.Name = "chkPassport";
            this.chkPassport.BackColor = System.Drawing.Color.White;
            this.chkPassport.Size = new System.Drawing.Size(30, 30);
            this.chkPassport.TabIndex = 33;
            this.chkPassport.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // txtIdDetails
            // 
            this.txtIdDetails.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtIdDetails.Location = new System.Drawing.Point(14, 273);
            this.txtIdDetails.Name = "txtIdDetails";
            this.txtIdDetails.Size = new System.Drawing.Size(336, 48);
            this.txtIdDetails.TabIndex = 35;
            this.txtIdDetails.TextTranslation = false;
            // 
            // txtConsigneeName
            // 
            this.txtConsigneeName.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtConsigneeName.Location = new System.Drawing.Point(14, 353);
            this.txtConsigneeName.Name = "txtConsigneeName";
            this.txtConsigneeName.Size = new System.Drawing.Size(453, 48);
            this.txtConsigneeName.TabIndex = 36;
            this.txtConsigneeName.TextTranslation = false;
            // 
            // lblIdDetails
            // 
            this.lblIdDetails.Location = new System.Drawing.Point(14, 244);
            this.lblIdDetails.Name = "lblIdDetails";
            this.lblIdDetails.Size = new System.Drawing.Size(101, 29);
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(11, 403);
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(459, 93);
            this.MsgMessage.TabIndex = 33;
            // 
            // chkDriversLicence
            // 
            this.chkDriversLicence.Location = new System.Drawing.Point(22, 125);
            this.chkDriversLicence.BackColor = System.Drawing.Color.White;
            this.chkDriversLicence.Name = "chkDriversLicence";
            this.chkDriversLicence.Size = new System.Drawing.Size(30, 30);
            this.chkDriversLicence.TabIndex = 30;
            this.chkDriversLicence.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // chkPostenId
            // 
            this.chkPostenId.Location = new System.Drawing.Point(234, 90);
            this.chkPostenId.Name = "chkPostenId";
            this.chkPostenId.BackColor = System.Drawing.Color.White;
            this.chkPostenId.Size = new System.Drawing.Size(30, 30);
            this.chkPostenId.TabIndex = 29;
            this.chkPostenId.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);
            // 
            // chkBankId
            // 
            this.chkBankId.Location = new System.Drawing.Point(22, 90);
            this.chkBankId.Name = "chkBankId";
            this.chkBankId.BackColor = System.Drawing.Color.White;
            this.chkBankId.Size = new System.Drawing.Size(30, 30);
            this.chkBankId.TabIndex = 28;
            this.chkBankId.CheckStateChanged += new System.EventHandler(this.IdCheckBoxStateChanged);

            // 
            // lblConsigneeName
            // 
            this.lblConsigneeName.Location = new System.Drawing.Point(15, 323);
            this.lblConsigneeName.Name = "lblConsigneeName";
            this.lblConsigneeName.Size = new System.Drawing.Size(178, 29);
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(122, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // lblHeading
            // 
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(15, 56);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(0, 0);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormAttemptedDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormIdDetails";
            this.Text = "FormIdDetails";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfLoadCarriers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRecieved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIdDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsigneeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 txtIdDetails;
        private Resco.Controls.CommonControls.TransparentLabel lblConsigneeName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private System.Windows.Forms.CheckBox chkDriversLicence;
        private System.Windows.Forms.CheckBox chkPostenId;
        private System.Windows.Forms.CheckBox chkBankId;
        private System.Windows.Forms.CheckBox chkPassport;
        private PreCom.Controls.PreComInput2 txtConsigneeName;
        private MessageControl MsgMessage;
        private TransparentLabel lblIdDetails;
        private TransparentLabel lblNumberOfItems;
        private TransparentLabel lblRecieved;
        private TransparentLabel lblItemCount;
        private TransparentLabel lblNumberOfLoadCarriers;
        private TransparentLabel lblChkPassport;
        private TransparentLabel lblChkId;
        private TransparentLabel lblChkDriverLisence;
        private TransparentLabel lblChkBankId;
    }
}
﻿using System;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.EnterIdDetails
{

    public class FormDataIdDetails : BaseFormData
    {
        public int ItemCount { get; set; }
        public int LoadCarrierCount { get; set; }
        public string LicenceId { get; set; }
        public string VasMessage { get; set; }
        public MessageHolder MessageHolder { get; set; }

    }

    public partial class FormIdDetails : BaseBarcodeScanForm
    {
        #region Private Vars
        private FormDataIdDetails _formData;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        private readonly MultiButtonControl _tabButtons;
        private string _vasMessage;
        #endregion

        #region Constructor
        public FormIdDetails()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
            SetStandardControlProperties(lblModuleName, lblHeading, null, null, null, null, null);
            InitOnScreenKeyBoardProperties();
        }
        #endregion

        #region Overriden Methods

        public override void OnShow(IFormData iformData)
        {
            base.OnShow(iformData);
            ClearFields();
            _formData = (FormDataIdDetails)iformData;
            if (_formData != null)
            {
                lblItemCount.Text = Convert.ToString(_formData.ItemCount);
                lblNumberOfLoadCarriers.Text = GlobalTexts.NumberOfLoadCarriers + ":  " + Convert.ToString(_formData.LoadCarrierCount);
                PopulateMessage(_formData.MessageHolder);
                if (!String.IsNullOrEmpty(_formData.LicenceId))
                {
                    chkDriversLicence.Checked = true;
                    txtIdDetails.Text = _formData.LicenceId;
                }
                _vasMessage = _formData.VasMessage;
                _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsValidId() && IsValidConsigneeName());
            }
            OnUpdateView(iformData);
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            ViewEvent.Invoke(IdDetailsViewEvents.BarcodeScanned, scannerOutput);
        }

        public override void ShowingView()
        {
            base.ShowingView();
            if (String.IsNullOrEmpty(_vasMessage) == false)
                ShowVasPopupMessage(_vasMessage);

        }

        #endregion

        #region Event Handlers

        private void BtnOkClick(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInputs())
                    ViewEvent.Invoke(IdDetailsViewEvents.Ok, txtIdDetails.Text, txtConsigneeName.Text, GetSelectedIdCode());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormIdDetails.BtnOkClick");
            }
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(IdDetailsViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormIdDetails.BtnBackClick");
            }
        }

        private void IdCheckBoxStateChanged(object sender, EventArgs e)
        {
            try
            {
                if (((CheckBox)sender).Checked)
                {
                    UnselectAllCheckBoxes(sender as Control);
                    txtIdDetails.Text = String.Empty;
                }
                ScannedNumberTextChanged(false, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormIdDetails.IdCheckBoxStateChanged");
            }
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            if (MsgMessage != null)
                MsgMessage.ClearMessage();
            _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled() && IsConsigneeNameFilled());
        }

        private void ScannedNumberTextChangedForConsignee(bool userFinishedTyping, string textentered)
        {
            _tabButtons.SetButtonEnabledState(OkButton, IsAnyCheckBoxChecked() && IsIdFilled() && IsConsigneeNameFilled());
        }

        #endregion

        #region Private Methods

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };
            txtIdDetails.Tag = keyBoardProperty;
            txtConsigneeName.Tag = txtConsigneeName.Tag = new OnScreenKeyboardProperties(GlobalTexts.EnterConsigneeName, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChangedForConsignee
            };

        }

        //Is Consignee Name filled
        private bool IsConsigneeNameFilled()
        {
            return txtConsigneeName.Text.Trim().Length > 0;
        }

        //Codes for selected Id
        private string GetSelectedIdCode()
        {

            foreach (Control chk in touchPanel.Controls)
            {
                var checkBox = chk as CheckBox;
                if (checkBox != null)
                {
                    if (checkBox.Checked)
                    {
                        switch (checkBox.Name)
                        {
                            case "chkBankId":
                                return "01";
                            case "chkDriversLicence":
                                return "02";
                            case "chkPostenId":
                                return "03";
                            case "chkPassport":
                                return "04";

                        }
                    }
                }
            }
            return "";
        }

        //Apply validation
        private bool ValidateInputs()
        {

            if ((txtIdDetails.Text.Trim().Length < 2) || (txtIdDetails.Text.Trim().Length > 12))//Id details should be between 2 and 12
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, String.Format(GlobalTexts.IdShouldBeOfLength, 2, 12),
                                          Severity.Error, GlobalTexts.Ok);
                txtIdDetails.Focus();
                return false;
            }
            if ((txtConsigneeName.Text.Trim().Length < 2) || (txtConsigneeName.Text.Trim().Length > 28))//Consignee Name should be between 2 and 28
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, String.Format(GlobalTexts.ConsigneeNameLength, 2, 28),
                                          Severity.Error, GlobalTexts.Ok);
                txtConsigneeName.Focus();
                return false;
            }
            if (!IsAnyCheckBoxChecked())
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.PleaseSelectAtLeastOne,
                                         Severity.Error, GlobalTexts.Ok);
                chkBankId.Focus();
                return false;
            }
            return true;
        }

        //Is Id Details Empty
        private bool IsIdFilled()
        {
            return txtIdDetails.Text.Trim().Length > 0;
        }

        //Validation for Consignee Name
        private bool IsValidConsigneeName()
        {
            return (txtConsigneeName.Text.Trim().Length >= 2) && (txtConsigneeName.Text.Trim().Length <= 28);
        }

        //Only one checkbox should be checked at a time
        private bool IsAnyCheckBoxChecked()
        {
            if (touchPanel.Controls.OfType<CheckBox>().Any(checkBox => checkBox.Checked))
            {
                return true;
            }
            return false;
        }

        //Validation for Id Detaio
        private bool IsValidId()
        {
            return (txtIdDetails.Text.Trim().Length >= 2) && (txtIdDetails.Text.Trim().Length <= 12);
        }

        //UnCheck CheckBoxes
        private void UnselectAllCheckBoxes(Control control)
        {
            foreach (Control chk in touchPanel.Controls)
            {
                var checkBox = chk as CheckBox;
                if (checkBox != null)
                {
                    if (chk.Name != control.Name)
                    {
                        checkBox.Checked = false;
                    }
                }
            }
        }

        //Show message coming from VAS
        private void ShowVasPopupMessage(string vasMessage)
        {
            if (!String.IsNullOrEmpty(vasMessage))
                GuiCommon.ShowModalDialog(GlobalTexts.Information, vasMessage, Severity.Warning, GlobalTexts.Ok);
            _vasMessage = string.Empty;//Clear it once shown
        }

        //Clear previously selected values
        private void ClearFields()
        {
            txtConsigneeName.Text = string.Empty;
            txtIdDetails.Text = string.Empty;
            chkBankId.Checked = false;
            chkDriversLicence.Checked = false;
            chkPassport.Checked = false;
            chkPostenId.Checked = false;
        }

        //Set Texts
        private void SetTextToGui()
        {
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, OkButton) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            lblModuleName.Text = GlobalTexts.IdConfirmation;
            lblHeading.Text = ModelUser.UserProfile.OrgUnitName;
            lblNumberOfItems.Text = GlobalTexts.NumberOfConsignmentItems + ":";
            lblChkBankId.Text = GlobalTexts.BankId;
            lblChkDriverLisence.Text = GlobalTexts.DriversLicence;
            lblChkPassport.Text = GlobalTexts.Passport;
            lblChkId.Text = GlobalTexts.PostenId;
            lblIdDetails.Text = GlobalTexts.IdDetails;
            lblConsigneeName.Text = GlobalTexts.ConsigneeName;
            lblNumberOfLoadCarriers.Text = GlobalTexts.NumberOfLoadCarriers;

            _tabButtons.GenerateButtons();
        }

        //Fill or clear message
        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
            {
                if (messageHolder.State != MessageState.Empty)
                {
                    MsgMessage.ShowMessage(messageHolder);
                }
                else
                {
                    MsgMessage.ClearMessage();
                }
            }
        }
        #endregion

    }

    public class IdDetailsViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.EnterIdDetails
{
    class ActionCommandsIdDetails
    {
        //TODO :: Write Unit Test
        internal FlowStateIdDetails ValidateFlowScanBarcodeForVas(FlowResultScanBarcode subflowResult, BarcodeType validTypes, MessageHolder messageHolder, out string licenceId)
        {
            var state = new FlowStateIdDetails();
            licenceId = null;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    if ((subflowResult.BarcodeType & validTypes) == BarcodeType.Unknown)
                    {
                        messageHolder.Update(GlobalTexts.IllegalBarcode, MessageState.Error);
                    }
                    else
                    {
                        messageHolder.Clear();
                        if (!String.IsNullOrEmpty(subflowResult.LicenceId))
                        {
                            licenceId = subflowResult.LicenceId;
                            state = FlowStateIdDetails.ViewIdDetails;
                        }
                    }
                    break;
                case FlowResultState.Error:
                    messageHolder.Update(subflowResult.MessageHolder.Text, subflowResult.MessageHolder.State);
                    state = FlowStateIdDetails.ViewIdDetails;
                    break;
            }

            return state;
        }

      

        internal void SetVasParameters(string idDetails, string idCode, string recipientName, VasSubFlowResultInfo vasSubFlowResultInfo)
        {
            vasSubFlowResultInfo.IdType = idCode;
            vasSubFlowResultInfo.RecipientId = idDetails;
            vasSubFlowResultInfo.RecipientName = recipientName;
        }

        internal List<ConsignmentEntity> GetMatchingEntities(EntityMap entityMap, string[] vasCodes)
        {
            return entityMap.GetConsignmentsWithoutEntities().OfType<ConsignmentItem>().Where(consignmentEntity => IsVasCodeContains(consignmentEntity.Vas, vasCodes)).Cast<ConsignmentEntity>().ToList();
        }

        internal bool IsVasCodeContains(VASType[] vas, string[] vasCodes)
        {
            return vas != null && vas.Count(c => vasCodes.Contains(c.VASCode)) > 0;
        }

        internal List<ConsignmentEntity> GetMatchingEntitiesForProductCode(EntityMap entityMap, string[] productCodes)
        {
            return entityMap.GetConsignmentsWithoutEntities().OfType<ConsignmentItem>().Where(consignmentEntity => productCodes.Contains(consignmentEntity.ProductCode)).Cast<ConsignmentEntity>().ToList();
        }
    }
}

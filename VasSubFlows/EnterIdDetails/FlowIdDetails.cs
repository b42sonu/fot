﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.EnterIdDetails
{
    public enum FlowStateIdDetails
    {
        ActionValidateDetails,
        ActionSetGoodsEventParams,
        ActionBackFromScanBarcode,
        ActionGetMatchingEntities,
        ViewCommands,
        ViewIdDetails,
        FlowComplete,
    }

    public class FlowResultIdDetails : BaseFlowResult
    {
    }

    // US 77h: http://kp-confluence.postennorge.no/display/FOT/77h+-+Enter+ID+Paper+details
    public class FlowIdPaperDetails : BaseFlow
    {
        private FlowDataVasProcessSupport _flowData;
        private readonly CommandsHandleScannedBarcodes _commandsHandleScannedBarcodes;
        private readonly FlowResultIdDetails _flowResultId;
        private readonly MessageHolder _messageHolder;
        private readonly ViewCommandsIdDetails _viewCommandsId;
        private FlowScanBarcode _flowScanBarcodeInstance;
        private readonly ActionCommandsIdDetails _actionCommands;
        private string _recipientName;
        private string _idDetails;
        private readonly EntityMap _entityMap;
        private string _idCode;
        private string _vasMessage;
        private static readonly string[] VasCodes = new[] { "1133", "1134" };
        private List<ConsignmentEntity> _listConsignments;


        public FlowIdPaperDetails()
        {

            _flowResultId = new FlowResultIdDetails();
            _messageHolder = new MessageHolder();
            _viewCommandsId = new ViewCommandsIdDetails();
            _entityMap = new EntityMap();
            _actionCommands = new ActionCommandsIdDetails();
            _commandsHandleScannedBarcodes = new CommandsHandleScannedBarcodes();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowIdPaperDetails");
            _flowData = (FlowDataVasProcessSupport)flowData;
            if (_flowData != null && _flowData.CurrentConsignmentEntity != null && _flowData.EntityMap == null)
            {
                _flowData.EntityMap = new EntityMap();
                _commandsHandleScannedBarcodes.StoreScannedConsignmentEntity(_flowData.EntityMap, _flowData.CurrentConsignmentEntity);
            }

            if (_flowData != null && (_flowData.EntityMap == null || !(_flowData.EntityMap.Count > 0)))
            {
                GuiCommon.ShowModalDialog("No consignments found", "No consignments found", Severity.Warning,
                                          GlobalTexts.Ok);
                ExecuteState(FlowStateIdDetails.FlowComplete);
            }
            else
            {
                if (_flowData != null) _vasMessage = Language.Translate(_flowData.Param);
                ExecuteState(FlowStateIdDetails.ActionGetMatchingEntities);
            }
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateIdDetails)state);
        }

        public void ExecuteState(FlowStateIdDetails stateIdContract)
        {

            if (stateIdContract > FlowStateIdDetails.ViewCommands)
            {
                ExecuteViewState(stateIdContract);
            }
            else
            {
                ExecuteActionState(stateIdContract);
            }
        }

        private void ExecuteViewState(FlowStateIdDetails state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowIdPaperDetails;" + state);

                switch (state)
                {
                    case FlowStateIdDetails.ViewIdDetails:
                        _viewCommandsId.ViewIdDetails(IdDetailsViewEventHandler, _idDetails, _messageHolder, _flowData.EntityMap, _vasMessage, _listConsignments.Count);//TODO: Get from VAS
                        break;

                    case FlowStateIdDetails.FlowComplete:
                        EndFlow();
                        break;

                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowIdContract.ExecuteFlowState");
                _flowResultId.State = FlowResultState.Exception;
                ExecuteState(FlowStateIdDetails.FlowComplete);
            }
        }

        public void ExecuteActionState(FlowStateIdDetails state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowIdContract;" + state);
                    switch (state)
                    {
                        case FlowStateIdDetails.ActionGetMatchingEntities:
                            _listConsignments = _actionCommands.GetMatchingEntities(_flowData.EntityMap, VasCodes);
                            state = FlowStateIdDetails.ViewIdDetails;
                            break;
                        case FlowStateIdDetails.ActionBackFromScanBarcode:
                            state = _actionCommands.ValidateFlowScanBarcodeForVas((FlowResultScanBarcode)SubflowResult, BarcodeType.DriversLicence, _messageHolder, out _idDetails);
                            break;

                        case FlowStateIdDetails.ActionSetGoodsEventParams:
                            _actionCommands.SetVasParameters(_idDetails, _idCode, _recipientName, _flowData.VasSubFlowResultInfo);
                            _flowResultId.State = FlowResultState.Ok;
                            state = FlowStateIdDetails.FlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateIdDetails.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowIdContract.ExecuteActionState");
                _flowResultId.State = FlowResultState.Exception;
                state = FlowStateIdDetails.FlowComplete;
            }


            ExecuteViewState(state);
        }


        private void IdDetailsViewEventHandler(int idDetailsViewEvents, params object[] data)
        {
            switch (idDetailsViewEvents)
            {
                case IdDetailsViewEvents.Ok:
                    _idDetails = data[0].ToString();
                    _recipientName = data[1].ToString();
                    _idCode = data[2].ToString();
                    ExecuteState(FlowStateIdDetails.ActionSetGoodsEventParams);
                    break;
                case IdDetailsViewEvents.Back:
                    _flowResultId.State = FlowResultState.Cancel;
                    ExecuteViewState(FlowStateIdDetails.FlowComplete);
                    break;

                case IdDetailsViewEvents.BarcodeScanned:
                    _vasMessage = string.Empty;
                    GoToFlowScanBarcode(BarcodeType.DriversLicence, FlowStateIdDetails.ActionBackFromScanBarcode);//
                    _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                    _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
                    break;
            }
        }

        private void GoToFlowScanBarcode(BarcodeType barcodeType, FlowStateIdDetails stateId)
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.Vas, _messageHolder, barcodeType, _entityMap)
            {
                HaveCustomForm = true
            };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)stateId, Process.Inherit);
        }


        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowIdContract ");
            BaseModule.EndSubFlow(_flowResultId);
        }
    }
}

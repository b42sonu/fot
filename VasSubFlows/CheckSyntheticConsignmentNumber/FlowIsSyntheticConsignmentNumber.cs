﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.CheckSyntheticConsignmentNumber
{

    public enum FlowStateCheckSyntheticConsignment
    {
        ActionIsSyntheticConsignment,
        ViewCommands,
        FlowComplete,
    }

    public class FlowResultCheckSyntheticConsignment : BaseFlowResult
    {}


    public class FlowCheckSyntheticConsignment : BaseFlow
    {
        private FlowDataVasProcessSupport _flowData;
        private FlowResultCheckSyntheticConsignment _flowResult;
        private readonly ActionCommandsIsSyntheticConsignmentNumber _actionCommands;


        public FlowCheckSyntheticConsignment()
        {
            _flowResult = new FlowResultCheckSyntheticConsignment();
            _actionCommands = new ActionCommandsIsSyntheticConsignmentNumber();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowCheckSyntheticConsignment");
            _flowData = (FlowDataVasProcessSupport)flowData;
            ExecuteState(FlowStateCheckSyntheticConsignment.ActionIsSyntheticConsignment);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateCheckSyntheticConsignment)state);
        }

        public void ExecuteState(FlowStateCheckSyntheticConsignment state)
        {

            if (state > FlowStateCheckSyntheticConsignment.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStateCheckSyntheticConsignment state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowCheckSyntheticConsignment;" + state);

                switch (state)
                {
                    case FlowStateCheckSyntheticConsignment.FlowComplete:
                        EndFlow();
                        break;
                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowCheckSyntheticConsignment.ExecuteFlowState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateCheckSyntheticConsignment.FlowComplete);
            }
        }

        public void ExecuteActionState(FlowStateCheckSyntheticConsignment state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowCheckSyntheticConsignment;" + state);
                    switch (state)
                    {
                        case FlowStateCheckSyntheticConsignment.ActionIsSyntheticConsignment:
                            _actionCommands.IsConsignmentSynthetic(_flowData.CurrentConsignmentEntity, ref _flowResult);
                            state = FlowStateCheckSyntheticConsignment.FlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateCheckSyntheticConsignment.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowCheckSyntheticConsignment.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateCheckSyntheticConsignment.FlowComplete;
            }

            ExecuteViewState(state);
        }


        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowCheckSyntheticConsignment ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

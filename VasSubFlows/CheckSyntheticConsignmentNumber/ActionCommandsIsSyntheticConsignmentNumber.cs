﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.CheckSyntheticConsignmentNumber
{
    class ActionCommandsIsSyntheticConsignmentNumber
    {
        //TODO :: Write Unit Test
        internal FlowStateCheckSyntheticConsignment IsConsignmentSynthetic(ConsignmentEntity consignmentEntity, ref FlowResultCheckSyntheticConsignment flowResult)
        {
            flowResult.State = FlowResultState.Ok;
            if (consignmentEntity != null)
            {
                var item = consignmentEntity as ConsignmentItem;
                if (item != null)
                {
                    if (item.IsConsignmentSynthetic)
                    {
                        GuiCommon.ShowModalDialog(GlobalTexts.SyntheticConsignment,
                                                  GlobalTexts.ExtraInfoNeededForSyntheticConsignmentNumber,
                                                  Severity.Error, GlobalTexts.Ok);
                        flowResult.State = FlowResultState.Cancel;
                    }
                }
            }
            return FlowStateCheckSyntheticConsignment.FlowComplete;
        }
    }
}

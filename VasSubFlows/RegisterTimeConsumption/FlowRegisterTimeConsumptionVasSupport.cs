﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

// US 79: http://kp-confluence.postennorge.no/display/FOT/79+-+Register+time+consumption+on+delivery
namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.RegisterTimeConsumption
{
    public enum FlowStateRegisterTimeConsumptionVasSupport
    {
        ActionIsTimeConsumptionAllowed,
        ThisFlowComplete,
    }

    public class FlowResultRegisterTimeConsumptionVasSupport : BaseFlowResult
    {
    }

    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowCanRegisterTimeConsumption : BaseFlow
    {
        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "FlowRegisterTimeConsumptionVasSupport called");
            EndFlow();
        }

        

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowRegisterTimeConsumptionVasSupport ");
            var result = new FlowResultRegisterTimeConsumptionVasSupport();
            result.State = FlowResultState.Ok;
            BaseModule.EndSubFlow(result);
        }

        
        public override void ExecuteState(int state)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SumAmount
{
    class ActionCommandsSumAmount
    {
        private List<ConsignmentAmount> _consignmentAmounts;
        /// <summary>
        /// method for get consignment that delivery payment pending and total amount...
        /// </summary>
        /// <param name="entityMap"></param>
        /// <param name="totalAmount"></param>
        /// <returns></returns>
        public List<ConsignmentAmount> GetOutstandingPaymentsOnConsignments(EntityMap entityMap, out decimal totalAmount)
        {
            _consignmentAmounts = new List<ConsignmentAmount>();
            totalAmount = 0;
            try
            {
                if (entityMap != null && entityMap.Values != null)
                    foreach (var consignment in entityMap.Values)
                    {
                        if (consignment != null)
                        {
                            // Add payments for scanned consignment
                            if (consignment.HasStatus(EntityStatus.Scanned) && consignment.Payments != null)
                            {
                                totalAmount += AddPaymentInList(consignment.Payments, consignment.ConsignmentId);
                            }
                            else
                            {
                                // Add payments for scanned consignment items
                                ConsignmentItemMap consignmentItemHistoryMap =
                                    entityMap.GetConsignmentItems(consignment.ConsignmentId);
                                if (consignmentItemHistoryMap != null)
                                {
                                    var hasScannedItems =
                                        consignmentItemHistoryMap.Any(
                                            c => (c.Value.EntityStatus & EntityStatus.Scanned) != 0);
                                    if (hasScannedItems)
                                    {
                                        // Add payment info from validated consignment
                                        if (consignment.Payments != null)
                                        {
                                            totalAmount += AddPaymentInList(consignment.Payments,
                                                                            consignment.ConsignmentId);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsSumAmount.GetOutstandingPaymentsOnConsignments");
            }

            return _consignmentAmounts;
        }

        
        /// <summary>
        /// add in list those payment is greater then 0 and paid is false... 
        /// </summary>
        internal decimal AddPaymentInList(IEnumerable<PaymentType> payments, string consignmentId)
        {
            decimal amount = 0;
            if (payments != null)
            {
                amount += payments.Where(payment => payment.Paid == false).Sum(payment => payment.Amount);
            }

            if (amount > 0)
            {
                var consignmentDeliveryDetail = new ConsignmentAmount
                {
                    ConsignmentNumber = consignmentId,
                    Amount = amount
                };
                if (_consignmentAmounts != null) _consignmentAmounts.Add(consignmentDeliveryDetail);
            }
            return amount;
        }
    }

    public class ConsignmentAmount
    {
        public string ConsignmentNumber { get; set; }
        public decimal Amount { get; set; }
    }

}

﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SumAmount
{
    class ViewCommandsSumAmount
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="formDataSumAmount"> </param>
        public void ShowViewSumAmount(ViewEventDelegate viewEventHandler, FormDataSumAmount formDataSumAmount)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsSumAmount.ShowViewSumAmount()");
            var formData = formDataSumAmount;
            var view = ViewCommands.ShowView<FormSumAmount>(formData);
            view.SetEventHandler(viewEventHandler);
            //view.AutoShow = false;
        }

    }
}

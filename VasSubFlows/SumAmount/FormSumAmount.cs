﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SumAmount
{
    public partial class FormSumAmount : BaseForm
    {
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private MultiButtonControl _tabButtons;
        private FormDataSumAmount _formData;
        public FormSumAmount()
        {
            InitializeComponent();
            SetStandardControlProperties(lblModuleName, lblHeading, null, null, null, null, null);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
        }
        private void SetTextToGui()
        {

            if (ModelUser.UserProfile != null) lblHeading.Text = ModelUser.UserProfile.OrgUnitName;

            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonOkName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
            lblModuleName.Text = GlobalTexts.Payments;
            textCell1.CellSource.ConstantData = GlobalTexts.ConsignmentPaymentHeading;
            textCell2.CellSource.ConstantData = GlobalTexts.AmountPaymentHeading;
        }
        public override void OnShow(IFormData formData)
        {
            OnUpdateView(formData);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            if (advancedListOperationList.DataRows.Count > 0)
                advancedListOperationList.DataRows.Clear();
            _formData = (FormDataSumAmount)iformData;
            if (_formData != null)
            {
                lbTotalAmount.Text = GlobalTexts.TotalAmount + Colon + Convert.ToString(_formData.TotalAmount);
                BindList(_formData);
            }
        }


        #region "Events"
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormSumAmount method FormSumAmount.ButtonBackClick");
                ViewEvent.Invoke(SumAmountViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSumAmount.ButtonBackClick");
            }
        }
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormSumAmount method FormSumAmount.ButtonConfirmClick");
                ViewEvent.Invoke(SumAmountViewEvents.Ok);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSumAmount.ButtonConfirmClick");
            }
        }

        #endregion
        #region "Bind advace list with entity map"
        /// <summary>
        /// method for binding advance list with entity map..
        /// </summary>
        private void BindList(FormDataSumAmount formData)
        {
            if (formData != null && formData.DeliveryAmountDetails != null)
            {
                foreach (ConsignmentAmount consignmentDelivery in formData.DeliveryAmountDetails)
                {
                    var colTextValues = new string[2];
                    colTextValues[0] = consignmentDelivery.ConsignmentNumber;
                    colTextValues[1] = Convert.ToString(consignmentDelivery.Amount);
                    var row = new Row(1, 2, colTextValues);
                    advancedListOperationList.DataRows.Add(row);
                }
                //lblAmount.Text = Convert.ToString(_formData.TotalAmount);
            }


        }
        #endregion
    }

    #region View specific classes

    public class FormDataSumAmount : BaseFormData
    {
        public List<ConsignmentAmount> DeliveryAmountDetails { get; set; }
        public decimal TotalAmount { get; set; }
    }
    #endregion

    public class SumAmountViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }
}
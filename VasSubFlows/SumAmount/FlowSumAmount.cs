﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

// US 77i: http://kp-confluence.postennorge.no/display/FOT/77i+-+Sum+amount+before+delivery
namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SumAmount
{
    public enum FlowStateSumAmount
    {
        ActionIsAnyUnpaidConsignment,
        FlowCommands,
        ViewShowSumAmount,
        //ViewUpdateSumAmount,
        ThisFlowComplete,
    }

    public class FlowResultSumAmount : BaseFlowResult
    {
    }

    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowSumAmount : BaseFlow
    {
        private List<ConsignmentAmount> _consignmentDeliveryAmountDetails;
        private decimal _totalAmount;
        private readonly FlowResultSumAmount _flowResult;
        private readonly ViewCommandsSumAmount _viewCommands;
        private readonly ActionCommandsSumAmount _actionCommands;
        private FlowDataVasProcessSupport _flowData;
        public FlowSumAmount()
        {
            _flowResult = new FlowResultSumAmount {State = FlowResultState.Ok};

            _viewCommands = new ViewCommandsSumAmount();
            _actionCommands = new ActionCommandsSumAmount();
            _flowData = new FlowDataVasProcessSupport();

        }
        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowSumAmount");
            _flowData = (FlowDataVasProcessSupport)flowData;
            ExecuteState(FlowStateSumAmount.ActionIsAnyUnpaidConsignment);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateSumAmount)state);
        }

        public void ExecuteState(FlowStateSumAmount stateAgeValidation)
        {

            if (stateAgeValidation > FlowStateSumAmount.FlowCommands)
            {
                ExecuteFlowState(stateAgeValidation);
            }
            else
            {
                ExecuteActionState(stateAgeValidation);
            }
        }

        private void ExecuteFlowState(FlowStateSumAmount state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowSumAmount;" + state);

                switch (state)
                {
                    case FlowStateSumAmount.ViewShowSumAmount:
                        var data = new FormDataSumAmount { DeliveryAmountDetails = _consignmentDeliveryAmountDetails, TotalAmount = _totalAmount };
                        _viewCommands.ShowViewSumAmount(SumAmountViewEventHandler, data);
                        break;

                    case FlowStateSumAmount.ThisFlowComplete:
                        EndFlow();
                        break;

                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowSumAmount.ExecuteFlowState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateSumAmount.ThisFlowComplete);
            }
        }




        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateSumAmount state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowSumAmount;" + state);
                    switch (state)
                    {
                        case FlowStateSumAmount.ActionIsAnyUnpaidConsignment:
                            _consignmentDeliveryAmountDetails = _actionCommands.GetOutstandingPaymentsOnConsignments(_flowData.EntityMap, out _totalAmount);
                            state = _consignmentDeliveryAmountDetails.Count > 0
                                        ? FlowStateSumAmount.ViewShowSumAmount
                                        : FlowStateSumAmount.ThisFlowComplete;
                            break;
                            
                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateSumAmount.FlowCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowSumAmount.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateSumAmount.ThisFlowComplete;
            }


            ExecuteFlowState(state);
        }



        private void SumAmountViewEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case SumAmountViewEvents.Ok:
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteFlowState(FlowStateSumAmount.ThisFlowComplete);
                    break;
                case SumAmountViewEvents.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteFlowState(FlowStateSumAmount.ThisFlowComplete);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in DeliveryToConsigneeViewEventHandler");
                    break;
            }
        }



        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowSumAmount ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

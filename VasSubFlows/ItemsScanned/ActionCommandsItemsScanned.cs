﻿using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.ItemsScanned
{
    class ActionCommandsItemsScanned
    {
        /// <summary>
        /// check the item scanned count is equal to lm/planned item count and then decide if true-all are scnned
        ///  and count is treated as equal if else then not equal..
        /// </summary>
       
        internal bool AreAllItemsScanned(EntityMap entityMap, string productCode, out string consignmentNumber, out int expectedConsignmentItemsCount, out int scannedConsignmentItemsCount)
        {
            bool isValid = true;
            consignmentNumber = string.Empty;
            expectedConsignmentItemsCount = 0;
            scannedConsignmentItemsCount = 0;
            string[] productCodeArray = ParseParam(productCode.Trim());
            if (entityMap != null && productCodeArray != null)
            {

                foreach (var consignment in entityMap.Values)
                {
                    if (consignment != null)
                    {
                        ConsignmentItemMap consignmentItemHistoryMap = entityMap.GetConsignmentItems(consignment.ConsignmentId);
                        if (consignmentItemHistoryMap != null)
                        {
                            // Check how many scanned items we have of given product-code
                            var scannedItemCount = consignmentItemHistoryMap.Count(c => c.Value != null &&
                                    ((c.Value.EntityStatus & EntityStatus.Scanned) != 0 && productCodeArray.Contains(c.Value.ProductCode)));

                            
                            // If we don't have LM count, we use the planned count
                            var plannedItemCount = consignment.ConsignmentItemCountFromLm != 0
                                                       ? consignment.ConsignmentItemCountFromLm
                                                       : consignment.ItemCount(EntityStatus.Planned, false);

                            if (scannedItemCount > 0 && scannedItemCount < plannedItemCount)
                            {
                                consignmentNumber = consignment.ConsignmentId;
                                scannedConsignmentItemsCount = scannedItemCount;
                                expectedConsignmentItemsCount = plannedItemCount;
                                isValid = false;
                                ShowErrorMessage(consignmentNumber, expectedConsignmentItemsCount, scannedConsignmentItemsCount);
                            }
                        }
                    }
                }
            }
            return isValid;
        }

        internal void ShowErrorMessage(string consignmentNumber, int expectedConsignmentItemsCount, int scannedConsignmentItemsCount)
        {
            string message = string.Format(GlobalTexts.IsAllItemsAreScanned, scannedConsignmentItemsCount, consignmentNumber, expectedConsignmentItemsCount);
            GuiCommon.ShowModalDialog(GlobalTexts.Warning, message, Severity.Warning, GlobalTexts.Ok);

        }

        // Parse param set on VAS
        internal string[] ParseParam(string param)
        {
            if (string.IsNullOrEmpty(param))
                return null;

            //remove unneessary words from param
            param = param.Replace("ProductCode(", "");
            param = param.Replace(")", "");
            return param.Split(',');
        }

    }

}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.AgeValidation;

// US 77g: http://kp-confluence.postennorge.no/display/FOT/77g+-+Are+all+items+scanned
namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.ItemsScanned
{
    public enum FlowStateItemsScanned
    {
        ActionAreAllItemsScanned,
        ActionShowMessage,
        FlowCommands,
        ThisFlowComplete,
    }

    public class FlowResultItemsScanned : BaseFlowResult
    {
    }

    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowItemsScanned : BaseFlow
    {
        private readonly CommandsHandleScannedBarcodes _commands;
        private readonly FlowResultAgeValidation _flowResult;
        private readonly ActionCommandsItemsScanned _actionCommands;
        private FlowDataVasProcessSupport _flowData;
        private string _consignmentNumber;
        private int _expectedConsignmentItemsCount;
        private int _scannedConsignmentItemsCount;
        public FlowItemsScanned()
        {
            _commands = new CommandsHandleScannedBarcodes();
            _flowResult = new FlowResultAgeValidation();
            _actionCommands = new ActionCommandsItemsScanned();
        }
        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowAgeValidation");
            _flowData = (FlowDataVasProcessSupport)flowData;

            if (_flowData != null && _flowData.CurrentConsignmentEntity != null && _flowData.EntityMap == null)
            {
                _flowData.EntityMap = new EntityMap();
                _commands.StoreScannedConsignmentEntity(_flowData.EntityMap, _flowData.CurrentConsignmentEntity);
            }

            ExecuteState(FlowStateItemsScanned.ActionAreAllItemsScanned);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateItemsScanned)state);
        }

        public void ExecuteState(FlowStateItemsScanned stateAgeValidation)
        {

            if (stateAgeValidation > FlowStateItemsScanned.FlowCommands)
            {
                ExecuteFlowState(stateAgeValidation);
            }
            else
            {
                ExecuteActionState(stateAgeValidation);
            }
        }

        private void ExecuteFlowState(FlowStateItemsScanned state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowItemsScanned;" + state);

                switch (state)
                {
                    case FlowStateItemsScanned.ThisFlowComplete:
                        EndFlow();
                        break;
                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowItemsScanned.ExecuteFlowState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateItemsScanned.ThisFlowComplete);
            }
        }

        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateItemsScanned state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowItemsScanned;" + state);
                    switch (state)
                    {
                        case FlowStateItemsScanned.ActionAreAllItemsScanned:
                            _actionCommands.AreAllItemsScanned(_flowData.EntityMap, _flowData.Param, out _consignmentNumber,
                                                                                                         out _expectedConsignmentItemsCount,
                                                                                                         out  _scannedConsignmentItemsCount);

                            //state = isAllScanned ? FlowStateItemsScanned.ThisFlowComplete : FlowStateItemsScanned.ActionShowMessage;
                            state = FlowStateItemsScanned.ThisFlowComplete;
                            _flowResult.State = FlowResultState.Ok;//isAllScanned ? FlowResultState.Ok : FlowResultState.Error;
                            break;
                        case FlowStateItemsScanned.ActionShowMessage:
                            _actionCommands.ShowErrorMessage(StringUtil.ToDisplayEntity(_consignmentNumber), _expectedConsignmentItemsCount, _scannedConsignmentItemsCount);
                            //_flowResult.State = FlowResultState.Cancel;
                            state = FlowStateItemsScanned.ThisFlowComplete;
                            break;
                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateItemsScanned.FlowCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowItemsScanned.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateItemsScanned.ThisFlowComplete;
            }
            ExecuteFlowState(state);
        }
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowItemsScanned ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SetSignPriority
{
    class ActionCommandsSetSignPriority
    {
        internal SignaturePriority GetSignPriority(EntityMap entityMap, string param)
        {
            var highestPriorityEncountered = SignaturePriority.Undefined;

            var parsedParam = ParseParam(param);

            foreach (var consignment in entityMap.Values)
            {
                // If no product code is specified we show both name and signature 
                if (consignment.IsScannedOnline)
                {
                    if (consignment.ProductCode == null)
                        return SignaturePriority.SignatureAndName;

                    var currentSignaturePriority = GetSignaturePriority(consignment.ProductCode, parsedParam);
                    if (currentSignaturePriority == SignaturePriority.SignatureAndName)
                        return SignaturePriority.SignatureAndName;
                    if (currentSignaturePriority > highestPriorityEncountered)
                        highestPriorityEncountered = currentSignaturePriority;
                }

                if (consignment.ConsignmentItemsMap != null)
                {
                    foreach (var consignmentItem in consignment.ConsignmentItemsMap)
                    {
                        if (consignmentItem.Value.IsScannedOnline)
                        {
                            if(consignmentItem.Value.ProductCode == null)
                                return SignaturePriority.SignatureAndName;

                            var currentSignaturePriority = GetSignaturePriority(consignmentItem.Value.ProductCode, parsedParam);
                            if (currentSignaturePriority == SignaturePriority.SignatureAndName)
                                return SignaturePriority.SignatureAndName;
                            if (currentSignaturePriority > highestPriorityEncountered)
                                highestPriorityEncountered = currentSignaturePriority;
                        }
                    }
                }
            }

            return highestPriorityEncountered != SignaturePriority.Undefined ? highestPriorityEncountered : SignaturePriority.SignatureAndName;
        }



        private SignaturePriority GetSignaturePriority(string productCode, ParsedParam parsedParam)
        {
            // No param, so default to dignature and name
            if (parsedParam == null)
                return SignaturePriority.SignatureAndName;

            if (parsedParam.Signature.Contains(productCode))
                return SignaturePriority.Signature;

            if (parsedParam.Skip.Contains(productCode))
                return SignaturePriority.Skip;

            return SignaturePriority.SignatureAndName;
        }

        class ParsedParam
        {
            public List<string> Signature;
            public List<string> Skip;
        }

        // Parse param set on VAS
        private ParsedParam ParseParam(string param)
        {
            // Format is: Signature(productcode1, ..., productcodeN)|Skip(productcode1, ..., productcodeN)
            // Example: Signature(0340,0341,0342,0343,0345,0348,0350,0360,1202,1207,3110,3117)|Skip(3266)

            //if param is null, then return null
            if (string.IsNullOrEmpty(param))
                return null;

            var vasParamParser = new VasParamParser(param);
            var signatureCodes = vasParamParser.GetEntry("Signature");
            var skipCodes = vasParamParser.GetEntry("Skip");

            var arrProductCodesForSignature = signatureCodes.Split(',');
            var listProductCodesForSignature = new List<string>();
            if (arrProductCodesForSignature != null)
                listProductCodesForSignature.AddRange(arrProductCodesForSignature);

            var arrProductCodesForSkipSignature = skipCodes.Split(',');
            var listProductCodesForSkip = new List<string>();
            if (arrProductCodesForSkipSignature != null)
                listProductCodesForSkip.AddRange(arrProductCodesForSkipSignature);

            var result = new ParsedParam
            {
                Signature = listProductCodesForSignature,
                Skip = listProductCodesForSkip
            };



            //var highestPriorityEncountered = new ParsedParam
            //{
            //    Signature = new List<string> { "0340", "0341", "0342", "0343", "0345", "0348", "0350", "0360", "1202", "1207", "3110", "3117" },
            //    Skip = new List<string> { "3266" }
            //};

            return result;
        }




        internal FlowStateSetSignaturePriority ValidateSignPriority(SignaturePriority signPriority)
        {
            switch (signPriority)
            {
                case SignaturePriority.SignatureAndName:
                    return FlowStateSetSignaturePriority.FlowSignatureAndName;

                case SignaturePriority.Signature:
                    return FlowStateSetSignaturePriority.FlowSignature;

                default:
                    return FlowStateSetSignaturePriority.ThisFlowComplete;

            }
        }


        /// <summary>
        /// This method, validates the highestPriorityEncountered from flow FlowReconcilliation and decides which will be the next state.
        /// </summary>
        /// <param name="subflowResult">Specifies the object for FlowResultSignature class.</param>
        /// <param name="vasSubFlowResultInfo">Fill signature to access it on calling flow </param>
        /// <returns>Returns a value from FlowStateSetSignaturePriority enum, which acts as next command to execute.</returns>
        internal FlowStateSetSignaturePriority BackFromFlowSignature(FlowResultSignature subflowResult, VasSubFlowResultInfo vasSubFlowResultInfo)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsSetSignPriority.BackFromFlowSignature()");
            vasSubFlowResultInfo.Signature = subflowResult.Signature;
            return FlowStateSetSignaturePriority.ThisFlowComplete;
        }
    }
}

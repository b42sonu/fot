﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

// http://kp-confluence.postennorge.no/display/FOT/77e+-+Sign
namespace Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SetSignPriority
{
    public enum SignaturePriority
    {
        Undefined,
        Skip,
        Signature,
        SignatureAndName,
        WithoutSignatureAndName,
    }

    public enum FlowStateSetSignaturePriority
    {
        ActionGetSignPriority,
        ActionBackFromSignature,
        ViewCommands,
        FlowSignature,
        FlowSignatureAndName,
        ThisFlowComplete
    }

    public class FlowResultSetSignPriority : BaseFlowResult
    {
    }


    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowSetSignPriority : BaseFlow
    {
        private readonly FlowResultSetSignPriority _flowResult;
        private readonly ActionCommandsSetSignPriority _actionCommands;
        private FlowDataVasProcessSupport _flowData;
        private EntityMap _entityMap;
        public FlowSetSignPriority()
        {
            _actionCommands = new ActionCommandsSetSignPriority();
            _flowResult = new FlowResultSetSignPriority();
            _entityMap = new EntityMap();
        }
        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowAgeValidation");
            if (flowData != null)
            {
                _flowData = (FlowDataVasProcessSupport)flowData;
                _entityMap = _flowData.EntityMap;
            }

            ExecuteState(FlowStateSetSignaturePriority.ActionGetSignPriority);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateSetSignaturePriority)state);
        }

        public void ExecuteState(FlowStateSetSignaturePriority state)
        {

            if (state > FlowStateSetSignaturePriority.ViewCommands)
            {
                ExecuteFlowState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteFlowState(FlowStateSetSignaturePriority state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowItemsScanned;" + state);

                switch (state)
                {
                    case FlowStateSetSignaturePriority.FlowSignatureAndName:

                        var flowDataSignatureAndName = new FlowDataSignature(_flowData.HeaderText, _entityMap, SignaturePriority.SignatureAndName)
                        {
                            RecipientName = _flowData.VasSubFlowResultInfo != null ? _flowData.VasSubFlowResultInfo.RecipientName : string.Empty,
                            ShowLoadCarrierCount = true
                        };
                        StartSubFlow<FlowSignature>(flowDataSignatureAndName, (int)FlowStateSetSignaturePriority.ActionBackFromSignature, Process.Inherit);
                        break;

                    case FlowStateSetSignaturePriority.FlowSignature:
                        var flowDataSignature = new FlowDataSignature(_flowData.HeaderText, _entityMap, SignaturePriority.Signature) { ShowLoadCarrierCount = true };
                        StartSubFlow<FlowSignature>(flowDataSignature, (int)FlowStateSetSignaturePriority.ActionBackFromSignature, Process.Inherit);
                        break;

                    case FlowStateSetSignaturePriority.ThisFlowComplete:
                        EndFlow();
                        break;

                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowItemsScanned.ExecuteFlowState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateSetSignaturePriority.ThisFlowComplete);
            }
        }

        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateSetSignaturePriority state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowItemsScanned;" + state);
                    switch (state)
                    {
                        case FlowStateSetSignaturePriority.ActionGetSignPriority:
                            var signPriority = _actionCommands.GetSignPriority(_entityMap, _flowData.Param);
                            state = _actionCommands.ValidateSignPriority(signPriority);
                            break;

                        case FlowStateSetSignaturePriority.ActionBackFromSignature:
                            _flowResult.State = SubflowResult.State;
                            state = _actionCommands.BackFromFlowSignature((FlowResultSignature)SubflowResult, _flowData.VasSubFlowResultInfo);
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateSetSignaturePriority.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowItemsScanned.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateSetSignaturePriority.ThisFlowComplete;
            }
            ExecuteFlowState(state);
        }





        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending FlowItemsScanned ");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

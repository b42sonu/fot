﻿using System;
using System.Collections.Specialized;
using Com.Bring.PMP.PreComFW.Communication;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListEvent;
using Com.Bring.PMP.PreComFW.Devices.Gps;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Push;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PMC.PreComX.Mock;
using PreCom.Core.Communication;
using Newtonsoft.Json;
using System.Net;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using PreCom.Core.Modules;
using PreCom.HttpClient;
using System.Threading;

namespace Com.Bring.PMP.PreComFW.Shared.Communication
{
    public interface ICommunicationClient
    {
        void SendGoodsEvent(GoodsEvents goodsEvent, Transaction transaction);
        void SendOperationListEvent(OperationListEvent operationListEventEvents, Transaction transaction);
        T Query<T>(object requestObject, Transaction transaction) where T : EntityBase;
        bool IsConnected();
    }

    /// <summary>
    /// Communication client class 
    /// </summary>
    public partial class CommunicationClient : ISyncQuery, ICommunicationClient
    {
        #region Public Properties

        private readonly ICommunicationBase _communication;
        private readonly ICoreCollection _modules;
        private readonly IPlatformBase _platform;
        private readonly bool _useCompression;
        private readonly bool _useSyncOverAsync;
        private static readonly AutoResetEvent SyncOverAsync = new AutoResetEvent(false);
        private static char[] _syncOverAsyncResponse;
        public bool LastQueryTimedOut { get; internal set; }

        #endregion

        public static CommunicationClient Instance
        {
            get { return Nested.CommunicationClientInstance; }
        }

        #region "Module Events"

        public delegate void PushMessageReceivedDelegate(PushMessage pushMessage);
        public event PushMessageReceivedDelegate PushMessageReceivedEvent;

        #endregion

        // ReSharper disable ClassNeverInstantiated.Local
        private class Nested
        // ReSharper restore ClassNeverInstantiated.Local
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly CommunicationClient CommunicationClientInstance = new CommunicationClient();
        }

        public CommunicationClient()
            : this(ApplicationWrapper.Wrap(ModelModules.Application))
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationClient"/> class.
        /// </summary>
        public CommunicationClient(IApplication precom)
            : this(precom.Communication, precom.Modules, precom.Platform, precom.Settings)
        { }

        public CommunicationClient(ICommunicationBase communication, ICoreCollection modules, IPlatformBase platform, ISettingsBase settings)
        {
            _communication = communication;
            _modules = modules;
            _platform = platform;
            _useCompression = settings.Read(modules.Get<HttpClientModule>(), "UseCompression", false, true);
            _useSyncOverAsync = settings.Read(modules.Get<HttpClientModule>(), "UseSyncOverAsync", false, true);

            _communication.Register<PushMessage>(OnPushMessage);
            _communication.Register(1234, SyncOverAsyncReceive);
        }

        /// <summary>
        /// This funcrion will act as entry point for receiving push messages
        /// </summary>
        /// <param name="pushMessage">Specifies the push message.</param>
        private void OnPushMessage(PushMessage pushMessage)
        {
            try
            {
                if (pushMessage != null)
                {
                    Logger.LogEvent(Severity.Debug, "Received push message; Type: {0}  User: {1}", pushMessage.Type, pushMessage.User);

                    if (PushMessageReceivedEvent != null)
                    {
                        PushMessageReceivedEvent.Invoke(pushMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogCommunicationError(Logger.MessageLoggingTypes.PushResponse, pushMessage, ex.Message);
            }
        }

        /// <summary>
        /// Sends asynchronously
        /// </summary>
        private bool SendAsynchronously(EntityBase request, Transaction transaction)
        {
            try
            {
                var sendArgs = new RequestArgs(QueuePriority.Normal, Persistence.UntilDelivered, transaction);
                _communication.Send(request, sendArgs);
                if (Logger.ShowMessages)
                    Logger.LogCommunication(Logger.MessageLoggingTypes.AsyncRequest, request);
                return true;
            }

            catch (Exception ex)
            {
                Logger.LogCommunicationError(Logger.MessageLoggingTypes.AsyncRequest, request, ex.Message);
                return false;
            }
        }

        private T SendSyncOverAsync<T>(object requestObject, QueuePriority priority) where T : EntityBase
        {
            var dataString = JsonConvert.SerializeObject(requestObject);
            var bytes = new byte[dataString.Length * sizeof(char)];
            Buffer.BlockCopy(dataString.ToCharArray(), 0, bytes, 0, bytes.Length);
            _communication.Send(bytes, 1234, new RequestArgs(priority, Persistence.ConnectionSession, new Transaction(Guid.NewGuid())));

            SyncOverAsync.Reset();
            return SyncOverAsync.WaitOne(10000, false) ? JsonConvert.DeserializeObject<T>(new string(_syncOverAsyncResponse)) : null;
        }

        private void SyncOverAsyncReceive(byte[] data)
        {
            var chars = new char[data.Length / sizeof(char)];
            Buffer.BlockCopy(data, 0, chars, 0, data.Length);
            _syncOverAsyncResponse = chars;

            SyncOverAsync.Set();
        }

        /// <summary>
        /// Queries the PreCom Server synchronously with the specified parameters.
        /// The query is executed at once and is not queued on the communication queue.
        /// </summary>
        private T ExecuteQuery<T>(object requestObject, Transaction transaction, TimeSpan timeout) where T : EntityBase
        {
            T result = null;
            string typeName = typeof(T).FullName;

            try
            {
                LastQueryTimedOut = false;
                var dataString = JsonConvert.SerializeObject(requestObject);

#pragma warning disable 618
                // Ip data for BigIp-debugging. Logged serverside.
                var ipString = "CurrentNetwork: " + PreCom.Application.Instance.Platform.CurrentNetwork.IpAddress + ";";
                foreach (var network in PreCom.Application.Instance.Platform.Networks)
                {
                     ipString += string.Format("{0}: {1};", network.Name, network.IpAddress);
                }
#pragma warning restore 618

                Logger.LogEvent(Severity.Debug, "Sending synchronous data: {0}", dataString);
                var nameValueCollection = new NameValueCollection { { "jsonParam", dataString } };
                nameValueCollection.Add("ipInfo", ipString); 
                var module = _modules.Get<IHttpClient>();
                module.RequestTimeout = timeout;

                // HACK: This is a workaround for a PreCom platform defect. 
                // Shall be removed as soon as PreCom Http Client > 3.8 have been released!
                var wh = new WebHeaderCollection { { "X-PMC-TransactionId", transaction.Id.ToString() } };
                if (_useCompression)
                {
                    wh.Add("Accept-Encoding", "gzip");
                }

                var queryArgs = new QueryArgs(transaction, nameValueCollection, wh);
                if (Logger.ShowMessages)
                    Logger.LogCommunication(Logger.MessageLoggingTypes.SyncRequest, requestObject);
                result = module.Query<T>(queryArgs);
                if (Logger.ShowMessages)
                    Logger.LogCommunication(Logger.MessageLoggingTypes.SyncResponse, result);
            }
            catch (WebException webException)
            {
                // Treat internal server errors as timeouts
                if (webException.Status == WebExceptionStatus.Timeout || IsInternalServerError(webException))
                {
                    LastQueryTimedOut = true;
                }
                Logger.LogExpectedException(webException, "CommunicationClient::ExecuteQuery");
            }

            catch (Exception ex)
            {
                Logger.LogCommunicationError(Logger.MessageLoggingTypes.AsyncRequest, requestObject, ex.Message);

                string function = string.Format("CommunicationClient::ExecuteQuery<{0}>", typeName);
                Logger.LogExpectedException(ex, function);
            }

            return result;
        }

        private bool IsInternalServerError(WebException webException)
        {
            if (webException.Status == WebExceptionStatus.ProtocolError)
            {
                // handle internal server error (http status code 500)
                if(((HttpWebResponse)webException.Response).StatusCode == HttpStatusCode.InternalServerError)
                    return true;
            }
            return false;
        }

        private Position GetPosition()
        {
            if (!SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsGpsPositioningAllowed))
                return null;

            var gpsModule = ResolveGpsModule();

            if (gpsModule == null)
            {
                Logger.LogEvent(Severity.Debug, "Could not find GPS module", null);
                return null;
            }

            // Given that my GPS is enabled and my TMS can recieve GPS positioning data when I'm registering a goods event then I want the positioning information to be sent as part of the event to LM.
            if (!gpsModule.Opened)
                return null;

            try
            {
                return gpsModule.GetPosition();
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "CommunicationClient.GetPosition");
                return null;
            }
        }

        private IGps ResolveGpsModule()
        {
            var gpsModule = _modules.Get<IGps>();

            // No custom gps module found. Check if the platform handler implements IGps interface and use it instead.
            if (gpsModule != null)
                return gpsModule;

            gpsModule = _platform as IGps;

            if (gpsModule != null)
                return gpsModule;


            var platformBaseWrapper = _platform as PlatformBaseWrapper;

            if (platformBaseWrapper != null)
                gpsModule = platformBaseWrapper.WrappedObject as IGps;

            return gpsModule;
        }


    }
}
using System;
using System.Reflection;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Communication
{
    public static class TimeoutValues
    {
        private static Dictionary<string, int> _timeoutValuesDict;
        public static Dictionary<string, int> TimeoutValuesDict
        {
            get { return _timeoutValuesDict ?? (_timeoutValuesDict = InitTimeoutValuesDict()); }
            set { _timeoutValuesDict = value; }
        }

        public static TimeSpan GetTimeOutValue(Type entityType)
        {
            var key = string.Format("TimeoutValues.{0}", entityType.Name);
            var milliSeconds = TimeoutValuesDict.ContainsKey(key) ? TimeoutValuesDict[key] : 8000;
            return TimeSpan.FromMilliseconds(milliSeconds);
        }

        public static Dictionary<string, int> InitTimeoutValuesDict()
        {
            _timeoutValuesDict = new Dictionary<string, int>();

            try
            {
                AddTimeOutValuesFromFile(SourcePath + "\\timeout.synchronous.xml");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "InitTimeoutValuesDict");
            }

            return _timeoutValuesDict;
        }

        private static void AddTimeOutValuesFromFile(string timeOutXmlFilePath)
        {
            try
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(timeOutXmlFilePath);
                var firstNode = xmlDocument.ChildNodes[1];

                foreach (XmlNode childNode in firstNode.ChildNodes)
                {
                    if (childNode.NodeType == XmlNodeType.Element)
                    {
                        var key = String.Format("{0}.{1}", firstNode.Name, childNode.Name);
                        try
                        {
                            var timeoutStr = childNode.Attributes["timeout"].Value;
                            if (!String.IsNullOrEmpty(timeoutStr))
                            {
                                var timeout = Int32.Parse(timeoutStr);
                                _timeoutValuesDict.Add(key, timeout);
                            }

                        }
                        catch (FormatException ex)
                        {
                            Logger.LogException(ex, string.Format("Could not parse timeout string for: {0}", key));
                        }
                    }
                }
            }
            catch (XmlException ex)
            {
                Logger.LogException(ex, string.Format("Problems reading xml file for rpc timeout values! File: {0}",
                    timeOutXmlFilePath));
            }
        }

        private static string SourcePath
        {
            get
            {
                return
                    Path.GetDirectoryName(
                        Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
            }
        }
    }
}
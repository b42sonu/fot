﻿using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Communication
{
    /// <summary>
    /// Callback for when a asynchronous query has completed.
    /// </summary>
    /// <typeparam name="T">Subclass of EntityBase</typeparam>
    /// <param name="entity">The entity or null in case of error.</param>
    /// <param name="state">The State which was passed when starting the query.</param>
    /// <param name="ex">The exception if something has gone wrong</param>
    public delegate void QueryCompleted<T>(T entity, object state, Exception ex)
        where T : EntityBase;

    public interface ISyncQuery
    {
        /// <summary>
        /// Queries the PreCom Server synchronously with the specified parameters. 
        /// The query is executed at once and is not queued on the communication queue.
        /// </summary>
        /// <typeparam name="T">Subclass fo EntityBase</typeparam>
        /// <returns>Instance of the entity, or null if failed.</returns>
        T Query<T>(object requestObject, Transaction transaction)
           where T : EntityBase;
    }
}

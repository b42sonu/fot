﻿using System;
using System.Globalization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.CommunicationEntity.AckOperationList;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListEvent;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListResponse;
using Com.Bring.PMP.PreComFW.CommunicationEntity.RequestActualsSummary;
using Com.Bring.PMP.PreComFW.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using CommunicationEntity.Loading;
using CommunicationEntity.LogicalLoadCarrierEvent;
using CommunicationEntity.Messaging;
using PreCom.Core.Communication;
using PreCom.MeasurementCore;

namespace Com.Bring.PMP.PreComFW.Shared.Communication
{
    partial class CommunicationClient
    {
        public bool HasAttemptedToLogInToTMS;

        /// <summary>
        /// Queries the PreCom Server synchronously with the specified parameters.
        /// The query is executed at once and is not queued on the communication queue.
        /// </summary>
        public T Query<T>(object requestObject, Transaction transaction) where T : EntityBase
        {
            T result = null;
            try
            {
                if (IsConnected())
                {
                    if (_useSyncOverAsync && requestObject is T20200_ValidateGoodsFromFOTRequest)
                    {
                        return SendSyncOverAsync<T>(requestObject, QueuePriority.Highest);
                    }

                    var timeout = TimeoutValues.GetTimeOutValue(requestObject.GetType());

                    result = ExecuteQuery<T>(requestObject, transaction, timeout);
                }
                else
                {
                    Logger.LogEvent(Severity.Debug, "Tried to execute syncronous call for type '{0}' when disconnected", requestObject.GetType().Name);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "CommunicationClient.Query");
            }

            return result;
        }

        public void SendGoodsEvent(GoodsEvents goodsEvent, Transaction transaction)
        {
            var position = GetPosition();
            if (position != null && position.IsValid)
            {
                if (goodsEvent.EventInformation == null)
                {
                    goodsEvent.EventInformation = new GoodsEventsEventInformation();
                }

                goodsEvent.EventInformation.EventCoordinatesystem = "WGS84";
                goodsEvent.EventInformation.EventCoordinateX =
                    position.Longitude.ToString(CultureInfo.InvariantCulture);
                goodsEvent.EventInformation.EventCoordinateY =
                    position.Latitude.ToString(CultureInfo.InvariantCulture);

                Logger.LogEvent(Severity.Debug, "SendGoodsEvent;EventCoordinateX {0} ,EventCoordinateY {1} ", goodsEvent.EventInformation.EventCoordinateX, goodsEvent.EventInformation.EventCoordinateY);
            }

            TransactionLogger.LogTiming<GoodsEvents>(transaction, new Timing(), TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Received, TimingComponent.User), new HttpCommunication());

            SendAsynchronously(goodsEvent, transaction);
        }



        /// <summary>
        /// method for sedn operationlist event with gps position...
        /// </summary>
        /// <param name="operationListEventEvents"></param>
        /// <param name="transaction"></param>
        public void SendOperationListEvent(OperationListEvent operationListEventEvents, Transaction transaction)
        {
            if (SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsGpsPositioningAllowed))
            {
                var position = GetPosition();
                if (position != null && position.IsValid)
                {

                    if (operationListEventEvents.EventInformation == null)
                    {
                        operationListEventEvents.EventInformation =
                            new OperationListEventEventInformation();
                    }

                    operationListEventEvents.EventInformation.EventCoordinatesystem = "WGS84";
                    operationListEventEvents.EventInformation.EventCoordinateX =
                        position.Longitude.ToString(CultureInfo.InvariantCulture);
                    operationListEventEvents.EventInformation.EventCoordinateY =
                        position.Latitude.ToString(CultureInfo.InvariantCulture);
                    operationListEventEvents.EventInformation.ConsignmentItemCount = 0;
                    operationListEventEvents.EventInformation.ConsignmentItemCountSpecified = true;
                }
            }

            SendAsynchronously(operationListEventEvents, transaction);
        }

        public void SendConsignmentNoteEvent(CreateConsignmentNote consignmentNote)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.ActualsSummaryMessage,
                                              "Consignment Note Event");

            SendAsynchronously(consignmentNote, transaction);
        }

        public void SendMessageFromDriver(MessageFromDriver messageFromDriver)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                            (byte)TransactionCategory.ActualsSummaryMessage,
                                            "Message From Driver");

            SendAsynchronously(messageFromDriver, transaction);
        }
        public void SendValidateResortedOperationlistRequest(ValidateResortedOperationListRequest validateResortedOperationlist)
        {

            var transaction = new Transaction(Guid.NewGuid(),
                                          (byte)TransactionCategory.ValidateResortedOperationlistRequest,
                                          "ValidateResortedOperationListRequest");

            SendAsynchronously(validateResortedOperationlist, transaction);
        }

        public void SendLoadCarrierEvent(LogicalLoadCarrierEvent eventInfo)
        {
            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.LoadCarrierEvent,
                                              "User: " + eventInfo.EventInformation.LogicalLoadCarrierId);
            SendAsynchronously(eventInfo, transaction);
        }

        public void SendAsyncConnectPhysicalToLogicalCarrierEvent(ConnectLoadCarriersRequest eventInfo)
        {
            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ConnectPhysicalToLogicalCarrierEvent,
                                              "User: " + eventInfo.PhysicalId + ":" + eventInfo.LogicalId);
            SendAsynchronously(eventInfo, transaction);
        }

        public T20261_ConnectLogicalToPhysicalLoadCarSyncReply SendSyncConnectPhysicalToLogicalCarrierEvent(T20261_ConnectLogicalToPhysicalLoadCarSyncRequest request)
        {
            T20261_ConnectLogicalToPhysicalLoadCarSyncReply reply = null;
            Logger.LogEvent(Severity.Debug, "Executing command CommunicationClient.SendSyncConnectPhysicalToLogicalCarrierEvent()");
            try
            {
                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ConnectPhysicalToLogicalCarrierEvent,
                                              "User: " + request.PhysicalId + ":" + request.LogicalId);
                reply = Query<T20261_ConnectLogicalToPhysicalLoadCarSyncReply>(request, transaction);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "Validation.ValidateGoodsFromFot");
            }

            return reply;
        }



        public void SendUserInformationEvent(UserInformation userInformation)
        {
            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.UserInformation,
                                              "User: " + userInformation.User.Username);

            SendAsynchronously(userInformation, transaction);
        }

        public void SendActualsSummaryEvent(string stopId, RequestActualsSummary_Type actualsSummary)
        {
            //Prepare object for transaction
            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ActualsSummaryMessage, "StopId:" + stopId);
            //Send asynchronous call for actual summary
            SendAsynchronously(actualsSummary, transaction);
        }

        public bool SendDriverAvailabilityRequest(T20210_DriverAvailabilityToTMSRequest request)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.T20210_DriverAvailabilityToTMSRequest,
                                              "UserId:" + request.UserLogonId);

            return SendAsynchronously(request, transaction);
        }

        public void SendOperationListResponse(OperationListResponse operationListResponse)
        {
            Logger.LogEvent(Severity.Debug, "CommunicationClient: SendOperationListResponse() executed");

            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.T20210_DriverAvailabilityToTMSRequest,
                                              "OperationListId:" + CommandsOperationList.GetOperationListId());


            SendAsynchronously(operationListResponse, transaction);
            LogOperationListResponse(operationListResponse);
        }

        private void LogOperationListResponse(OperationListResponse operationListResponse)
        {
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(OperationListResponse));
                var builder = new StringBuilder();
                var writer = XmlWriter.Create(builder);
                xmlSerializer.Serialize(writer, operationListResponse);

                Logger.LogEvent(Severity.Debug, builder.ToString());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "CommunicationClient.LogOperationListResponse");
            }
        }


        public void SendOperationListAcknowledgement(AckOperationList ackOperationList)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.AckOperationList,
                                              "OperationListId:" + ackOperationList.CorrelationId);

            SendAsynchronously(ackOperationList, transaction);
        }

        public void SendWorkStatusRequest(IndicateWorkstatus request)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.WorkStatusRequest,
                                              "SessionId:" + request.SessionId);

            SendAsynchronously(request, transaction);
        }

        public bool IsConnected()
        {
            return _communication.IsConnected;
        }
    }
}

﻿using System;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.CommunicationEntity
{
    public class ClientLogItem : EntityBase
    {
        public DateTime LogDateTime { get; set; }
        public string Description { get; set; }
        public string StackTrace { get; set; }
        public int Severity { get; set; }
        public string Pda { get; set; }
        public string UserId { get; set; }
        public string Hash { get; set; }
    }
}

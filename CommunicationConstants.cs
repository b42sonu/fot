﻿
namespace Com.Bring.PMP.PreComFW.Shared
{
    public enum PushMessageTypes
    {
        OperationList,
        DriverAvailabilityResponse,
    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.NavigateToStop
{

    public class FormEventAddressDetails : ViewEvent
    {
        public const int Back = 1;
        public const int Erase = 2;
        public const int Navigate = 3;

    }
    public partial class FormAddressDetail : BaseForm
    {
        private FormDataAddressDetail _formData;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();
        public FormAddressDetail()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
        }

        private void SetTextToControls()
        {
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back,ButtonBackClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Erase, ButtonEraseClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Navigate, ButtonNavigationStop));
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            if (formData != null)
            {
                _formData = (FormDataAddressDetail)formData;

                if (_formData.Street != null) txtBoxStreetName.Text = _formData.Street;
                if (_formData.StreetNumber != null) txtBoxStreetNumber.Text = _formData.StreetNumber;
                if (_formData.City != null) txtBoxCity.Text = _formData.City;
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormAddressDetail.ButtonBackClick()");
            try
            {
                 ViewEvent.Invoke(FormEventAddressDetails.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormAddressDetail.ButtonBackClick()");
            }
        }

        private void ButtonEraseClick(object sender, EventArgs e)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormAddressDetail.ButtonEraseClick()");
            try
            {
                txtBoxCity.Text = string.Empty;
                txtBoxStreetName.Text = string.Empty;
                txtBoxStreetNumber.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormAddressDetail.ButtonOkClick()");
            }
        }

        private void ButtonNavigationStop(object sender, EventArgs e)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormAddressDetail.ButtonEraseClick()");
            try
            {
                ViewEvent.Invoke(FormEventAddressDetails.Navigate);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormAddressDetail.ButtonOkClick()");
            }
        }
    }

    public class FormDataAddressDetail : BaseFormData
    {
        public string Street { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string StreetNumber { get; set; }
    }
}
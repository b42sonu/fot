﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.NavigateToStop
{
    public enum FlowStateNavigateToStop
    {
        ActionParseAddress,
        ActionStartNavigation,
        ViewCommands,

        ViewAddressDetail,
        ThisFlowComplete,
    }

    public class FlowDataNavigateToStop : BaseFlowData
    {
        public string Street { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string StreetNumber { get; set; }
    }

    public class FlowResultNavigateToStop : BaseFlowResult
    {
    }

    public class FlowNavigateToStop : BaseFlow
    {
        private readonly ActionCommandsNavigateToStop _actionCommandsNavigationToStop;
        private FlowDataNavigateToStop _flowData;
        private readonly FlowResultNavigateToStop _flowResult;
        public FlowNavigateToStop()
        {
            _flowResult = new FlowResultNavigateToStop();
            _actionCommandsNavigationToStop = new ActionCommandsNavigateToStop();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowNavigateToStop.BeginFlow");
            if (flowData != null)
            {
                _flowData = (FlowDataNavigateToStop)flowData;
            }
            ExecuteState(FlowStateNavigateToStop.ViewAddressDetail);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateNavigateToStop)state);
        }

        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowStateNavigateToStop, which acts as name for next command.</param>
        public void ExecuteState(FlowStateNavigateToStop state)
        {
            if (state > FlowStateNavigateToStop.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStateNavigateToStop state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowResultNavigateToStop;" + state);
                switch (state)
                {
                    case FlowStateNavigateToStop.ViewAddressDetail:
                        ViewCommandsNavigateToStop.ShowAddressDetails(AddressDetailsEventHandler, _flowData);
                        break;

                    case FlowStateNavigateToStop.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowStateNavigateToStop.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateNavigateToStop.ThisFlowComplete);
            }
        }
        public void ExecuteActionState(FlowStateNavigateToStop state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowResultNavigateToStop;" + state);
                    switch (state)
                    {
                        case FlowStateNavigateToStop.ActionParseAddress:
                            break;

                        case FlowStateNavigateToStop.ActionStartNavigation:
                            _actionCommandsNavigationToStop.ActionStartNavigation(_flowData);
                            state = FlowStateNavigateToStop.ViewAddressDetail;

                            break;
                    }
                } while (state < FlowStateNavigateToStop.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowStateNavigateToStop.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateNavigateToStop.ThisFlowComplete;
            }
            ExecuteViewState(state);

        }

        private void AddressDetailsEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case FormEventAddressDetails.Back:
                    ExecuteState(FlowStateNavigateToStop.ThisFlowComplete);
                    break;

                case FormEventAddressDetails.Navigate:
                    ExecuteState(FlowStateNavigateToStop.ActionStartNavigation);
                    break;
            }
        }

        /// <summary>
        /// This method ends the flow FlowStateRegisterVas.
        /// </summary>
        private
        void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowStateNavigateToStop.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}


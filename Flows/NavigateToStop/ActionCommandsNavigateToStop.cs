﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.NavigateToStop
{
    class ActionCommandsNavigateToStop
    {
        public void ActionStartNavigation(FlowDataNavigateToStop flowDataNavigateStop)
        {
#pragma warning disable 618
            var module = PreCom.Application.Instance.Modules.Get<NavigationSygicFleet.SygicFleet>();
#pragma warning restore 618
            module.NavigateToAddress("Oslo", "kongens gate", 34);
         //   module.NavigateToCoordinates(61.0000, 8.0000);
        }
    }
}

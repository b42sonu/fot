﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.NavigateToStop
{
    class ViewCommandsNavigateToStop
    {
        /// <summary>
        /// Open view to see the address details
        /// </summary>
        public static void ShowAddressDetails(ViewEventDelegate viewEventHandler,FlowDataNavigateToStop flowData)
        {
            try
            {
                var formdata = new FormDataAddressDetail
                    {
                        Street = flowData.Street,
                        StreetNumber = flowData.StreetNumber,
                        City = flowData.City,
                        Country = flowData.Country
                    };
                var view = ViewCommands.ShowView<FormAddressDetail>(formdata);
               view.SetEventHandler(viewEventHandler);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsNavigateToStop.ShowAddressDetails()");
            }
        }
    }
}

﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.NavigateToStop
{
    partial class FormAddressDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblAddressInfo = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtBoxStreetNumber = new System.Windows.Forms.TextBox();
            this.txtBoxCity = new System.Windows.Forms.TextBox();
            this.txtBoxStreetName = new System.Windows.Forms.TextBox();
            this.labelStreet = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCity = new Resco.Controls.CommonControls.TransparentLabel();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.labelCountry = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAddress = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStreet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCountry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.lblAddress);
            this.touchPanel.Controls.Add(this.lblAddressInfo);
            this.touchPanel.Controls.Add(this.txtBoxStreetNumber);
            this.touchPanel.Controls.Add(this.txtBoxCity);
            this.touchPanel.Controls.Add(this.txtBoxStreetName);
            this.touchPanel.Controls.Add(this.labelStreet);
            this.touchPanel.Controls.Add(this.labelCity);
            this.touchPanel.Controls.Add(this.comboBoxCountry);
            this.touchPanel.Controls.Add(this.labelCountry);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblAddressInfo
            // 
            this.lblAddressInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAddressInfo.AutoSize = false;
            this.lblAddressInfo.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblAddressInfo.Location = new System.Drawing.Point(128, 244);
            this.lblAddressInfo.Name = "lblAddressInfo";
            this.lblAddressInfo.Size = new System.Drawing.Size(339, 34);
            this.lblAddressInfo.Text = "Verify address and start navigation";
            // 
            // txtBoxStreetNumber
            // 
            this.txtBoxStreetNumber.Location = new System.Drawing.Point(407, 284);
            this.txtBoxStreetNumber.Name = "txtBoxStreetNumber";
            this.txtBoxStreetNumber.Size = new System.Drawing.Size(60, 41);
            this.txtBoxStreetNumber.TabIndex = 42;
            // 
            // txtBoxCity
            // 
            this.txtBoxCity.Location = new System.Drawing.Point(128, 331);
            this.txtBoxCity.Name = "txtBoxCity";
            this.txtBoxCity.Size = new System.Drawing.Size(339, 41);
            this.txtBoxCity.TabIndex = 41;
            // 
            // txtBoxStreetName
            // 
            this.txtBoxStreetName.Location = new System.Drawing.Point(128, 284);
            this.txtBoxStreetName.Name = "txtBoxStreetName";
            this.txtBoxStreetName.Size = new System.Drawing.Size(273, 41);
            this.txtBoxStreetName.TabIndex = 40;
            // 
            // labelStreet
            // 
            this.labelStreet.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelStreet.AutoSize = false;
            this.labelStreet.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelStreet.Location = new System.Drawing.Point(11, 298);
            this.labelStreet.Name = "labelStreet";
            this.labelStreet.Size = new System.Drawing.Size(100, 34);
            this.labelStreet.Text = "Street";
            this.labelStreet.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // labelCity
            // 
            this.labelCity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelCity.AutoSize = false;
            this.labelCity.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelCity.Location = new System.Drawing.Point(3, 338);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(122, 34);
            this.labelCity.Text = "City";
            this.labelCity.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.comboBoxCountry.Location = new System.Drawing.Point(128, 378);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(294, 40);
            this.comboBoxCountry.TabIndex = 33;
            //this.comboBoxCountry.SelectedIndexChanged += new System.EventHandler(this.OnChangedRole);
            // 
            // labelCountry
            // 
            this.labelCountry.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelCountry.AutoSize = false;
            this.labelCountry.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelCountry.Location = new System.Drawing.Point(0, 381);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(122, 34);
            this.labelCountry.Text = "Country";
            this.labelCountry.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 12);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 34);
            this.labelModuleName.Text = "Nvigate to stop";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // lblAddress
            // 
            this.lblAddress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAddress.AutoSize = false;
            this.lblAddress.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lblAddress.Location = new System.Drawing.Point(29, 70);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(427, 115);
            this.lblAddress.Text = "GOTO :";
            // 
            // FormChangeRole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormAddressDetail";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStreet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCountry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private ComboBox comboBoxCountry;
        private Resco.Controls.CommonControls.TransparentLabel labelCountry;
        private Resco.Controls.CommonControls.TransparentLabel labelCity;
        private Resco.Controls.CommonControls.TransparentLabel lblAddressInfo;
        private TextBox txtBoxStreetNumber;
        private TextBox txtBoxCity;
        private TextBox txtBoxStreetName;
        private Resco.Controls.CommonControls.TransparentLabel labelStreet;
        private Resco.Controls.CommonControls.TransparentLabel lblAddress;
    }
}
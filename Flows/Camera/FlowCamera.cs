﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

// US 33b: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7936861
namespace Com.Bring.PMP.PreComFW.Shared.Flows.Camera
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateCamera
    {
        ActionDeletePreviousPictureFolders,
        ActionCameraSetting,
        ActionInitializeCamera,
        ActionDeletePictureFolder,
        ActionSavePhoto,
        ViewCommands,
        ViewShowCamera,
        FlowUpdateView,
        ThisFlowComplete
    }
    public class FlowResultCamera : BaseFlowResult
    {
        public string FolderName;
    }
    public class FlowDataCamera : BaseFlowData
    {
        public string CameraFunctionalityFor;
        public string ConsignmentItemType;
        public string ConsignmentItemNumber;
        public string FolderName;
        public string ConsignmentDisplayItemNumber;
    }
    public class FlowCamera : BaseFlow
    {
        private FlowDataCamera _flowData;
        private readonly ViewCommandsCamera _viewCommandsCamera;
        private readonly FormDataCamera _formDataCamera;
        private readonly ActionCommandsCamera _actionCommandsCamera;
        private readonly FlowResultCamera _flowResult;
        private string _directoryPath;

        private bool _photoCaptured;
        private bool _isCamreaProcessCancelled;
        public FlowCamera()
        {
            _viewCommandsCamera = new ViewCommandsCamera();
            _formDataCamera = new FormDataCamera();
            _actionCommandsCamera = new ActionCommandsCamera();
            _flowResult = new FlowResultCamera();
            _directoryPath = null;
        }
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "initialized flow Camera");
            _flowData = (FlowDataCamera)flowData;
            if (_flowData != null)
            {
                _formDataCamera.ConsignmentDisplayItemNumber = _flowData.ConsignmentDisplayItemNumber;
                _formDataCamera.ConsignmentItemType = _flowData.ConsignmentItemType;
                _formDataCamera.FunctionalityFor = _flowData.CameraFunctionalityFor;
                _formDataCamera.HeaderText = _flowData.HeaderText;

                _formDataCamera.FolderName = _flowData.FolderName;
            }
            if (_flowData != null && string.IsNullOrEmpty(_flowData.FolderName))
                ExecuteState(FlowStateCamera.ActionDeletePreviousPictureFolders);
            else
                ExecuteState(FlowStateCamera.ActionCameraSetting);
        }
        public void ExecuteState(FlowStateCamera state)
        {
            if (state > FlowStateCamera.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteViewState((FlowStateCamera)state);
        }
        private void ExecuteViewState(FlowStateCamera stateCamera)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowCamera;" + stateCamera);
                switch (stateCamera)
                {
                    case FlowStateCamera.ViewShowCamera:
                        _viewCommandsCamera.ShowTakePhotoView(TakePhotoViewEventHandler, _formDataCamera);
                        break;
                    case FlowStateCamera.FlowUpdateView:
                        CurrentView.UpdateView(_formDataCamera);
                        break;
                    case FlowStateCamera.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowCamera.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateCamera.ThisFlowComplete);
            }
        }


        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateCamera state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowCamera;" + state);
                    switch (state)
                    {
                        case FlowStateCamera.ActionDeletePreviousPictureFolders:
                            _actionCommandsCamera.DeletePreviousPicturesFolders();
                            state = FlowStateCamera.ActionCameraSetting;
                            break;


                        case FlowStateCamera.ActionCameraSetting:
                            _formDataCamera.SettingPhotoCount = _actionCommandsCamera.GetSettingPhotoCount();
                            _formDataCamera.CameraResoluion = _actionCommandsCamera.GetCameraResolutionSetting();
                            state = FlowStateCamera.ViewShowCamera;
                            break;

                        case FlowStateCamera.ActionInitializeCamera:
                            BusyUtil.Reset();
                            _actionCommandsCamera.ShowCameraCaptureDialog(_formDataCamera, _directoryPath);
                            state = FlowStateCamera.FlowUpdateView;
                            break;

                        case FlowStateCamera.ActionSavePhoto:
                            _actionCommandsCamera.SaveCameraPhoto(_directoryPath, _formDataCamera);
                            state = FlowStateCamera.FlowUpdateView;
                            break;

                        case FlowStateCamera.ActionDeletePictureFolder:
                            if (!string.IsNullOrEmpty(_formDataCamera.FolderName) && _isCamreaProcessCancelled)
                                _actionCommandsCamera.DeletePicturesFolder(_formDataCamera.FolderName);
                            _flowResult.FolderName = _isCamreaProcessCancelled ? null : _formDataCamera.FolderName;//null;
                            _flowResult.State = _isCamreaProcessCancelled ? FlowResultState.Cancel : FlowResultState.Ok;
                            if (_flowResult.State == FlowResultState.Cancel)
                                _flowResult.Message = GlobalTexts.PhotoCancelled;
                            else
                                _flowResult.Message = _photoCaptured ? GlobalTexts.PhotoAdded : GlobalTexts.NoPhotoAdded;
                            state = FlowStateCamera.ThisFlowComplete;

                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateCamera.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowCamera.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateCamera.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        /// <summary>
        /// Handler which will handle all type of events from take photo view..
        /// </summary>
        private void TakePhotoViewEventHandler(int takePhotoViewEvents, params object[] data)
        {
            switch (takePhotoViewEvents)
            {
                case TakePhotoViewEvent.Camera:
                    _formDataCamera.CameraResoluion = Convert.ToString(data[0]);
                    _directoryPath = Convert.ToString(data[1]);
                    ExecuteActionState(FlowStateCamera.ActionInitializeCamera);
                    break;
                case TakePhotoViewEvent.Ok:
                    _flowResult.State = FlowResultState.Ok;
                    _flowResult.FolderName = Convert.ToString(data[0]);
                    _flowResult.Message = GlobalTexts.PhotoAdded;//Convert.ToBoolean(data[1]) ? GlobalTexts.PhotoAdded : GlobalTexts.NoPhotoAdded;
                    ExecuteViewState(FlowStateCamera.ThisFlowComplete);
                    break;
                case TakePhotoViewEvent.Save:
                    _directoryPath = Convert.ToString(data[0]);
                    _flowResult.FolderName = Convert.ToString(data[1]);
                    ExecuteActionState(FlowStateCamera.ActionSavePhoto);
                    break;
                case TakePhotoViewEvent.Cancel:
                    _formDataCamera.FolderName = Convert.ToString(data[0]);
                    _photoCaptured = Convert.ToBoolean(data[1]);
                    _isCamreaProcessCancelled = Convert.ToBoolean(data[2]);
                    ExecuteActionState(FlowStateCamera.ActionDeletePictureFolder);
                    /*_flowResult.State = FlowResultState.Cancel;
                    _flowResult.Message = GlobalTexts.PhotoCancelled;
                    ExecuteViewState(FlowStateCamera.ThisFlowComplete);*/
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in TakePhotoViewEventHandler");
                    break;
            }
        }

        /// <summary>
        /// This methos ends the camera flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowCamera");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using System.IO;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using System.Drawing;
using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Camera
{
    public partial class FormTakePhoto : BaseForm
    {
        #region "Variables/objects adn constrctor"
        private PictureBox _pictureBoxSelected;
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private const string TabButtonCameraName = "Camera";
        private MultiButtonControl _tabButtons;
        private int _capturePhotoCount;
        private int _previousPhotoCount;
        private FormDataCamera _formData;
        string _folderName;
        private bool _popUpShown;
        public FormTakePhoto()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
                touchPanelPhotos.BackgroundImage = image;
            }
            SetTextToGui();
        }
        private void SetTextToGui()
        {
            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName) { ButtonType = TabButtonType.Cancel });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.TakePhoto, ButtonCameraClick, TabButtonCameraName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Complete, ButtonConfirmClick, TabButtonOkName) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            lableThumbnail.Text = GlobalTexts.LabelThumbnail + Colon;
            lableCameraInstruction.Text = GlobalTexts.CameraUpdatedInstruction;
            lableDamageFor.Text = GlobalTexts.DamageFor;
            lableDeleteInstruction.Text = GlobalTexts.CameraDeleteInstruction;
            labelModuleName.Text = GlobalTexts.DamageRegistration;
            SetStandardControlProperties(labelModuleName, lblOrgnaisationUnitName, null, null, null, null, null);
            if (ModelUser.UserProfile.OrgUnitName != null) lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
        }
        #endregion

        #region "Events"
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method ButtonBackClick");
                bool processCancel = true;
                if (_capturePhotoCount > 0)
                {
                    var result = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.DeleteCapturePhotos, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
                    if (result == GlobalTexts.No)
                    {
                        processCancel = false;
                    }
                }

                ViewEvent.Invoke(TakePhotoViewEvent.Cancel, _folderName, _capturePhotoCount > _previousPhotoCount,
                                 processCancel);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonBackClick");
            }
        }

        private void ButtonCameraClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method ButtonCameraClick");

                TakePhoto();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonCameraClick");
            }
        }

        private void TakePhoto()
        {
            if (_folderName != null)
            {
                _formData.PhotoPath = string.Empty;
                _formData.ErrorOccured = false;
                if (_formData.SettingPhotoCount > _capturePhotoCount)
                {

                    string directoryPath = CreateDirectory(true);
                    ViewEvent.Invoke(TakePhotoViewEvent.Camera, _formData.CameraResoluion, directoryPath);
                    SavePicture();

                }
                else
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.PhotoLimitOver, Severity.Warning, GlobalTexts.Ok);
                }
            }
        }

        /// <summary>
        /// method used for created thumnail images for saved image....
        /// </summary>
        private void SavePicture()
        {
            if (_formData != null && !_formData.ErrorOccured && !string.IsNullOrEmpty(_formData.PhotoPath))
            {
                if (_formData.KeepPicture)
                {
                    if (_folderName != null)
                    {
                        string thumbFolderName = CreateDirectory(false);
                        ViewEvent.Invoke(TakePhotoViewEvent.Save, thumbFolderName, _folderName);
                    }

                    if (_formData != null && !_formData.ErrorOccured)
                    {
                        AddImage(_formData.PhotoPath);
                        _capturePhotoCount++;
                    }
                }
                ResetButtons();
            }
        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method ButtonConfirmClick");
                ViewEvent.Invoke(TakePhotoViewEvent.Ok, _folderName, _capturePhotoCount > _previousPhotoCount);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonConfirmClick");
            }
        }



        private void DeletePicture(String picturePath)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTakePhoto method BtnDiscardClick");
                /*SendBackPannel();*/

                string imagePath = picturePath;
                if (File.Exists(imagePath))
                {
                    File.Delete(imagePath);
                    if (_pictureBoxSelected != null)
                    {
                        touchPanelPhotos.Controls.Remove(_pictureBoxSelected);
                        _pictureBoxSelected = null;

                        ClearControlsAndCounter();
                        ShowAlreadyCaptureImgaes(_folderName);
                        ResetButtons();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonConfirmClick");
            }
        }
        /// <summary>
        /// method for create directory for captured phot and thumnail photos
        /// </summary>
        /// <param name="isReturnMainDirectorPath"></param>
        /// <returns></returns>
        private string CreateDirectory(bool isReturnMainDirectorPath)
        {
            string path = Path.Combine(FileUtil.GetApplicationPath(), "Photo");
            string directoryPath = string.Format("{0}\\{1}", path, _folderName); //@"\My Documents\thumb";

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            string thumbFolderName = directoryPath + "\\thumb\\";

            if (!Directory.Exists(thumbFolderName))
            {
                Directory.CreateDirectory(thumbFolderName);
            }

            if (isReturnMainDirectorPath)
                return directoryPath;
            return thumbFolderName;
        }

        #endregion

        #region "Methods"


        private void AddImage(string imagePath)
        {
            if (File.Exists(imagePath))
            {
                Image captureImage = new Bitmap(imagePath);
                int quotient = touchPanelPhotos.Controls.Count / 3;
                int remainder = touchPanelPhotos.Controls.Count % 3;

                var pointImageBox = new Point { X = remainder == 0 ? ScaleUtil.GetScaledPosition(8) : ScaleUtil.GetScaledPosition(146) * remainder, Y = quotient == 0 ? ScaleUtil.GetScaledPosition(4) : ScaleUtil.GetScaledPosition(105) * quotient };

                var imageBox = new PictureBox
                {
                    Location = pointImageBox,
                    Name = "imageBox1",
                    Size = new Size(ScaleUtil.GetScaledPosition(128), ScaleUtil.GetScaledPosition(95)),
                    TabIndex = 0,
                    Image = captureImage,
                    Tag = imagePath,
                    SizeMode = PictureBoxSizeMode.StretchImage,
                };
                imageBox.Click += ImageBoxClick;
                touchPanelPhotos.Controls.Add(imageBox);
            }
        }

        void ImageBoxClick(object sender, EventArgs e)
        {
            try
            {
                if (!_popUpShown)
                {
                _popUpShown = true;
                if (sender != null)
                {
                    _pictureBoxSelected = ((PictureBox)sender);

                    //BringToFrontPannel();
                    var result = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.CameraDeletePicture, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
                    _popUpShown = false;
                    if (result == GlobalTexts.Yes)
                    {
                        DeletePicture(Convert.ToString(_pictureBoxSelected.Tag));
                    }
                }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormTakePhoto.ImageBoxClick");
            }
        }
        public override void OnShow(IFormData formData)
        {

            ClearControlsAndCounter();
            _formData = (FormDataCamera)formData;
            labelModuleName.Text = _formData.HeaderText;

            if (_formData != null)
            {
                //_tabButtons.Enable(TabButtonCameraName, !_formData.IsMc70Device);
                //lableItemDetail.Text = _formData.PlannedConsignmentItem + " " + _formData.ConsignmentItemNumber;
                lableDamageFor.Text = _formData.FunctionalityFor + "  " + GlobalTexts.For + " " + _formData.ConsignmentItemType.ToLower() + " " + _formData.ConsignmentDisplayItemNumber;

                if (!string.IsNullOrEmpty(_formData.FolderName))
                {
                    _folderName = _formData.FolderName;
                    ShowAlreadyCaptureImgaes(_formData.FolderName);

                }
                else
                {
                    _folderName = Convert.ToString(Guid.NewGuid());
                }
            }
            else
            {
                _folderName = Convert.ToString(Guid.NewGuid());
            }
            _previousPhotoCount = _capturePhotoCount;
            ResetButtons();

        }

        private void ResetButtons()
        {

            _tabButtons.SetButtonEnabledState(TabButtonOkName, _capturePhotoCount > 0);
            _tabButtons.SetButtonEnabledState(TabButtonBackName, true);//_capturePhotoCount == 0);
            _tabButtons.SetButtonEnabledState(TabButtonCameraName, true);
            lableDeleteInstruction.Visible = _capturePhotoCount > 0;

        }

        /// <summary>
        /// 
        /// </summary>
        private void ClearControlsAndCounter()
        {
            _capturePhotoCount = 0;
            _previousPhotoCount = 0;
            touchPanelPhotos.Controls.Clear();
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataCamera)iformData;
            if (_formData != null)
            {
                if (_formData.ErrorOccured)
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, _formData.ErrorMessage, Severity.Error, GlobalTexts.Ok);
            }
        }

        public void OpenCamera()
        {
            if (_previousPhotoCount == 0)
                TakePhoto();
        }

        #endregion
        /// <summary>
        /// show already captured images if folder name provide by previous process ..
        /// and it means same process for same consignment and consignment item...
        /// </summary>
        /// <param name="folderName"></param>
        private void ShowAlreadyCaptureImgaes(string folderName)
        {
            string directoryPath =
                Path.Combine(Path.Combine(
                    Path.Combine(FileUtil.GetApplicationPath(), "Photo"),
                    folderName), "thumb");
            if (Directory.Exists(directoryPath))
            {
                string[] photosPaths = Directory.GetFiles(directoryPath, "*.jpg");

                if (photosPaths.Length > 0)
                {
                    foreach (var photoPath in photosPaths)
                    {
                        AddImage(photoPath);
                        _capturePhotoCount++;
                    }
                }
            }
        }


        

    }

    public class FormDataCamera : BaseFormData
    {
        public string FunctionalityFor;
        public string ConsignmentItemType;
        public string ConsignmentDisplayItemNumber;
        public int SettingPhotoCount;
        public string CameraResoluion;
        public bool ErrorOccured;
        public string ErrorMessage;
        public string PhotoPath;
        public bool KeepPicture;
        public string FolderName;
    }

    public class TakePhotoViewEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Ok = 1;
        public const int Save = 2;
        public const int Camera = 3;
    }

}
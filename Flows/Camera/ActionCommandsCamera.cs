﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Microsoft.WindowsMobile.Forms;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.Camera
{
    class ActionCommandsCamera
    {
        //TODO :: Write Unit Test
        /// <summary>
        /// get setting value for number of photo captured for one consignment/consignment item
        /// </summary>
        /// <returns></returns>
        internal int GetSettingPhotoCount()
        {
            //string value = "6";//SettingDataProvider.Instance.GetTmiValue<string>(TmiSettings.NumberOfPhotos);
            int result = SettingDataProvider.Instance.GetDeviceSetting(DeviceSettingNames.NumberOfPhotos, 20);//string.IsNullOrEmpty(value) ? 0 : Convert.ToInt32(value);
            return result;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// get setting value for Camera Resolution
        /// </summary>
        /// <returns></returns>
        internal string GetCameraResolutionSetting()
        {
            string value = SettingDataProvider.Instance.GetDeviceSetting(DeviceSettingNames.NumberOfPhotos, "100");
            return value;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// method for initialization camera ..
        /// </summary>
        /// <param name="formDataCamera"></param>
        /// <param name="directoryPath"> </param>
        internal void ShowCameraCaptureDialog(FormDataCamera formDataCamera, string directoryPath)
        {
            try
            {
                if (formDataCamera != null)
                {
                    if (ModelModules.Application.State == PreCom.ApplicationState.Activated)
                        ModelModules.Application.State = PreCom.ApplicationState.Deactivated;
                   
                    using (var cameraCapture = new CameraCaptureDialog())
                    {
                        cameraCapture.Owner = ViewCommands.CurrentView;
                        cameraCapture.InitialDirectory = directoryPath;
                        cameraCapture.DefaultFileName = DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".jpg";
                        cameraCapture.Title = GlobalTexts.Camera;
                        
                        cameraCapture.Mode = CameraCaptureMode.Still;
                        try
                        {
                            KeysApi.UnregisterEnterKey();
                            DialogResult diaResult = cameraCapture.ShowDialog();
                            if (DialogResult.OK == diaResult)
                            {
                                formDataCamera.PhotoPath = cameraCapture.FileName;
                                formDataCamera.ErrorOccured = false;
                                formDataCamera.KeepPicture = true;
                            }
                            else
                            {
                                formDataCamera.KeepPicture = false;
                            }
                        }
                        finally
                        {
                            KeysApi.RegisterEnterKey();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "ActionCommandsCamera.ShowCameraCaptureDialog");

                if (formDataCamera != null)
                {
                    formDataCamera.ErrorOccured = true;
                    formDataCamera.ErrorMessage = GlobalTexts.CameraInitializeError;
                }
            }

            var command = new HideKeyBoardCommand();
            command.HideAll();

            if (ModelModules.Application.State == PreCom.ApplicationState.Deactivated)
            {
                ModelModules.Application.State = PreCom.ApplicationState.Activated;
            }
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// method for capture and save photo on client side memory..
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="formDataCamera"></param>
        /// <returns></returns>
        internal bool SaveCameraPhoto(string directoryPath, FormDataCamera formDataCamera)
        {
            bool isSavePhoto = true;

            try
            {
                if (formDataCamera != null)
                {
                    string thumbFilePath = directoryPath + "\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".jpg";

                    try
                    {
                        CreateThumbnail(formDataCamera.PhotoPath, thumbFilePath);
                    }
                    catch
                    {
                        // Generally, you should try to catch an explicit
                        // exception, but I know that this can throw 2 different
                        // types of exceptions on an OOM.
                        GC.Collect();
                        CreateThumbnail(formDataCamera.PhotoPath, thumbFilePath);
                        // An exception will be thrown if still OOM
                    }

                    formDataCamera.PhotoPath = thumbFilePath;
                    formDataCamera.ErrorOccured = false;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "ActionCommandsCamera.SaveCameraPhoto");
                isSavePhoto = false;

                if (formDataCamera != null)
                {
                    formDataCamera.ErrorOccured = true;
                    formDataCamera.ErrorMessage = GlobalTexts.SavePhotoError;
                }
            }

            return isSavePhoto;
        }
        //TODO :: Write Unit Test
        private void CreateThumbnail(string originalImageFile, string destinationFile)
        {
            /*var originalImage = new CompactImage();
            originalImage.Load(originalImageFile);

            originalImage.Resize(originalImage.Size.Width);//(float)128 / originalImage.Size.Width);
            originalImage.Save(destinationFile, 100);*/
            Bitmap bmp = null;
            try
            {
                if (string.IsNullOrEmpty(originalImageFile) == false) bmp = new Bitmap(originalImageFile);
            }
            catch
            {
                // Generally, you should try to catch an explicit
                // exception, but I know that this can throw 2 different
                // types of exceptions on an OOM.
                GC.Collect();
                // An exception will be thrown if still OOM
                if (string.IsNullOrEmpty(originalImageFile) == false) bmp = new Bitmap(originalImageFile);
            }

            if (bmp != null)
            {
                var thumbnail = new Bitmap(bmp.Width, bmp.Height);

                using (Graphics g = Graphics.FromImage(thumbnail))
                {
                    var rectDestination = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    g.DrawImage(bmp, rectDestination, new Rectangle(0, 0, bmp.Width, bmp.Height), GraphicsUnit.Pixel);
                    if (string.IsNullOrEmpty(destinationFile) == false) thumbnail.Save(destinationFile, ImageFormat.Jpeg);
                    thumbnail.Dispose();
                }
            }
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// delete the folder of captured picture .....
        /// </summary>
        /// <param name="folderName">images folder name</param>
        /// <returns>if deleted successfully then true if not then false..</returns>
        internal bool DeletePicturesFolder(string folderName)
        {
            try
            {
                return GoodsEventHelper.DeletePicturesFolder(folderName);

            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsCamera.DeletePicturesFolder");
            }
            return false;
        }
        /// <summary>
        /// mehhod for deleted all previous pictures folders when new process started for clear memory ...
        /// </summary>
        /// <returns></returns>
        internal bool DeletePreviousPicturesFolders()
        {
            bool isDeleted = false;
            try
            {
                string directoryPath = Path.Combine(FileUtil.GetApplicationPath(), "Photo");
                if (Directory.Exists(directoryPath))
                {
                    Directory.Delete(directoryPath, true);
                    isDeleted = true;
                }
                return isDeleted;

            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsCamera.DeletePreviousPicturesFolders");
            }
            return isDeleted;
        }
    }
}

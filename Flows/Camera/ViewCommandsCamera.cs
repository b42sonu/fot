﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Commands;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Camera
{
    class ViewCommandsCamera
    {
        internal FormTakePhoto ShowTakePhotoView(ViewEventDelegate viewEventHandler, FormDataCamera formDataCamera)
        {
            var view = ViewCommands.ShowView<FormTakePhoto>(formDataCamera);
            view.SetEventHandler(viewEventHandler);
            view.OpenCamera();
            return view;
        }
    }
}

﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Camera
{
    partial class FormTakePhoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lableThumbnail = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanelPhotos = new Resco.Controls.CommonControls.TouchPanel();
            this.lableCameraInstruction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lableDeleteInstruction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lableDamageFor = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lableThumbnail)).BeginInit();
            this.touchPanelPhotos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lableCameraInstruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDeleteInstruction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDamageFor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgnaisationUnitName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lableThumbnail);
            this.touchPanel.Controls.Add(this.touchPanelPhotos);
            this.touchPanel.Controls.Add(this.lableCameraInstruction);
            this.touchPanel.Controls.Add(this.lableDeleteInstruction);
            this.touchPanel.Controls.Add(this.lableDamageFor);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 588);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            
            // 
            // lableThumbnail
            // 
            this.lableThumbnail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lableThumbnail.Location = new System.Drawing.Point(22, 131);
            this.lableThumbnail.Name = "lableThumbnail";
            this.lableThumbnail.Size = new System.Drawing.Size(0, 0);
            // 
            // touchPanelPhotos
            // 
            this.touchPanelPhotos.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanelPhotos.Location = new System.Drawing.Point(22, 160);
            this.touchPanelPhotos.Name = "touchPanelPhotos";
            this.touchPanelPhotos.Size = new System.Drawing.Size(436, 230);
            // 
            // lableCameraInstruction
            // 
            this.lableCameraInstruction.AutoSize = false;
            this.lableCameraInstruction.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lableCameraInstruction.Location = new System.Drawing.Point(22, 390);
            this.lableCameraInstruction.Name = "lableCameraInstruction";
            this.lableCameraInstruction.Size = new System.Drawing.Size(436, 60);
            // 
            // lableDeleteInstruction
            // 
            this.lableDeleteInstruction.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.lableDeleteInstruction.Location = new System.Drawing.Point(22, 445);
            this.lableDeleteInstruction.Name = "lableDeleteInstruction";
            this.lableDeleteInstruction.Size = new System.Drawing.Size(0, 0);
            // 
            // lableDamageFor
            // 
            this.lableDamageFor.AutoSize = false;
            this.lableDamageFor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lableDamageFor.Location = new System.Drawing.Point(21, 71);
            this.lableDamageFor.Name = "lableDamageFor";
            this.lableDamageFor.Size = new System.Drawing.Size(438, 84);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(115, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormTakePhotoNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormTakePhotoNew";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lableThumbnail)).EndInit();
            this.touchPanelPhotos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lableCameraInstruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDeleteInstruction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lableDamageFor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgnaisationUnitName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;


        //private Resco.Controls.CommonControls.TransparentLabel lableItemDetail;
        private Resco.Controls.CommonControls.TransparentLabel lableDamageFor;
        private Resco.Controls.CommonControls.TransparentLabel lableCameraInstruction;
        private Resco.Controls.CommonControls.TransparentLabel lableThumbnail;
        private Resco.Controls.CommonControls.TouchPanel touchPanelPhotos;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TransparentLabel lableDeleteInstruction;
        


    }
}
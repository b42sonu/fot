﻿using System;
using System.Text.RegularExpressions;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount
{
    public partial class FormConfirmConsignmentItemCount : BaseForm
    {
        int _consignmentItemCount;
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private MultiButtonControl _tabButtons;
        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public FormConfirmConsignmentItemCount()
        {
            InitializeComponent();
            SetTextToGui();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToGui()
        {
            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonType.Confirm) { Name = TabButtonOkName });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();


            labelModuleName.Text = GlobalTexts.Pickup;
            transparentLabel3.Text = GlobalTexts.ConfirmConsignmentCount;
            transparentLabel2.Text = GlobalTexts.ConsignmentId + ":";
            transparentLabel1.Text = GlobalTexts.InformationConsignmentScanned;

        }
        public override void OnShow(IFormData iformData)
        {
            Logger.LogAssert(iformData is FormDataConfirmConsignmentItemCount);
            var formData = (FormDataConfirmConsignmentItemCount)iformData;

            labelModuleName.Text = formData.HeaderText;
            transparentLabel2.Text = GlobalTexts.ConsignmentId + ":" + formData.Consignment.ConsignmentId;
            
            // Retrieve initital count from plan
            _consignmentItemCount = formData.Consignment.ItemCount(EntityStatus.Planned, false);

            // No plan, so retrieve initial count if known in LM
            if (_consignmentItemCount == 0)
            {
                // Retrieve number of planned consignments
                _consignmentItemCount = formData.Consignment.ConsignmentItemCountFromLm;
            }


            editConsignmentItemCount.Text = _consignmentItemCount != 0 ? Convert.ToString(_consignmentItemCount) : string.Empty;

            _messageControlControl.ShowMessage(GlobalTexts.ConsignmentItemCantZero, MessageState.Warning);
            _messageControlControl.Visible = false;

            _tabButtons.SetButtonEnabledState(TabButtonOkName, _consignmentItemCount > 0);
            
            SetKeyboardState(KeyboardState.Numeric);
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewEvent != null)
                {
                    ViewEvent.Invoke(ConfirmConsignmentItemCountViewEvent.Back);
                }

                EndForm();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonBackClick");
            }
        }

        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewEvent != null)
                {
                    ViewEvent.Invoke(ConfirmConsignmentItemCountViewEvent.Confirm, _consignmentItemCount);
                }

                EndForm();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonConfirmClick");
            }
        }

        private void EndForm()
        {
            Parent.Controls.Remove(this);
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var onScreenKeyboardProperties = new OnScreenKeyboardProperties(GlobalTexts.EnterItemCount, KeyPadTypes.Numeric);
            editConsignmentItemCount.Tag = onScreenKeyboardProperties;
            onScreenKeyboardProperties.TextChanged = ConsignmentItemTextChanged;
        }

        private void ConsignmentItemTextChanged(bool userFinishedTyping, string textentered)
        {
            var text = textentered;
            var regex = new Regex(@"^\d+$");
            if (!regex.IsMatch(text))
            {
                if (userFinishedTyping)
                    _messageControlControl.ShowMessage(GlobalTexts.InvalidInputMessage, MessageState.Error);
                editConsignmentItemCount.Text = string.Empty;
                //buttonOk.Enabled = false;
                _tabButtons.SetButtonEnabledState(TabButtonOkName, false);
            }
            else
            {
                if (string.IsNullOrEmpty(text) == false)
                {
                    _consignmentItemCount = Convert.ToInt32(text);
                    //buttonOk.Enabled = _consignmentItemCount > 0;
                    _tabButtons.SetButtonEnabledState(TabButtonOkName, _consignmentItemCount > 0);
                    _messageControlControl.Visible = !_tabButtons.IsButtonEnabled(TabButtonOkName);//!buttonOk.Enabled;
                    if (userFinishedTyping && _consignmentItemCount == 0)
                    {
                        _messageControlControl.ShowMessage(GlobalTexts.ConsignmentItemCantZero, MessageState.Warning);
                        editConsignmentItemCount.Text = "0";
                    }
                }
                else
                {
                    //buttonOk.Enabled = false;
                    _tabButtons.SetButtonEnabledState(TabButtonOkName, false);
                }
            }
            editConsignmentItemCount.Focus();
        }
    }

    public class FormDataConfirmConsignmentItemCount : BaseFormData
    {
        public Consignment Consignment;
        public EntityMap EntityMap;
    }

    public class ConfirmConsignmentItemCountViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Confirm = 1;
    }
}
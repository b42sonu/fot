﻿using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount
{
    public class FlowDataConfirmConsignmentItemCount : BaseFlowData
    {
        public Consignment Consignment;
    }

    public class FlowResultConfirmConsignmentItemCount : BaseFlowResult
    {
        public int ConsignmentItemsCount;
    }

    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowState
    {
        ActionStoreConsignmentItemCount,

        ViewCommands,
        ViewConfirmConsignmentItemCount,
        FlowCanceled,
        FlowComplete
    }

    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowConfirmConsignmentItemCount : BaseFlow
    {
        private int _consignmentItemsCount;
        private FlowDataConfirmConsignmentItemCount _flowData;
        private  readonly FlowResultConfirmConsignmentItemCount _flowResult = new FlowResultConfirmConsignmentItemCount();

        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "initialized flow FlowConfirmConsignmentItemCount");
            Logger.LogAssert(flowData != null);
            _flowData = (FlowDataConfirmConsignmentItemCount) flowData;

            SoundUtil.Instance.PlayWarningSound();
            ExecuteState(FlowState.ViewConfirmConsignmentItemCount);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowState)state);
        }

        public void ExecuteState(FlowState state)
        {
            if (state > FlowState.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowState state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowConfirmConsignmentItemCount;" + state);
                switch (state)
                {
                    case FlowState.ViewConfirmConsignmentItemCount:

                        ViewCommands.ShowPlannedConsignmentItemCount(ConfirmConsignmentItemCountViewEventHandler, _flowData);
                        break;

                    case FlowState.FlowCanceled:
                        EndFlow(false);
                        break;

                    case FlowState.FlowComplete:
                        EndFlow(true);
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "Encountered unhandled view state");
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowConfirmConsignmentItemCount.ExecuteViewState");
                _flowResult.State=FlowResultState.Exception;
                ExecuteState(FlowState.FlowComplete);
            }
        }



        /// <summary>
        /// This method executes the state for the flow
        /// </summary>
        public void ExecuteActionState(FlowState state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowConfirmConsignmentItemCount;" + state);
                    switch (state)
                    {
                        case FlowState.ActionStoreConsignmentItemCount:
                            _flowData.Consignment.ManuallyEnteredConsignmentItemCount = _consignmentItemsCount;
                            state = FlowState.FlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowState.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowConfirmConsignmentItemCount.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                 state=FlowState.FlowComplete;
            }


            ExecuteViewState(state);
        }


        /// <summary>
        /// Handler which will handle all type of events from View
        /// </summary>
        public void ConfirmConsignmentItemCountViewEventHandler(int confirmConsignmentItemCountViewEvent, object[] data)
        {
            switch (confirmConsignmentItemCountViewEvent)
            {
                case ConfirmConsignmentItemCountViewEvent.Confirm:
                    _consignmentItemsCount = (int)data[0];
                    ExecuteState(FlowState.ActionStoreConsignmentItemCount);
                    break;

                case ConfirmConsignmentItemCountViewEvent.Back:
                    ExecuteState(FlowState.FlowCanceled);
                    break;
            }
        }




        private void EndFlow(bool result)
        {
            // Remove this operation from history as we now are finished
          //  var flowResult = new FlowResultConfirmConsignmentItemCount();
            Logger.LogEvent(Severity.Debug, "Ending flow FlowVConfirmConsignmentItemCount");
            if (result)
            {
                _flowResult.State = FlowResultState.Ok;
                _flowResult.ConsignmentItemsCount = _consignmentItemsCount;
            }
            else
            {
                _flowResult.State = FlowResultState.Cancel;
                _flowResult.Message = GlobalTexts.ConsignmentItemConfirmationAborted;
            }

            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

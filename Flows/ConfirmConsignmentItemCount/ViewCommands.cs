﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount
{
    class ViewCommands
    {
        internal static FormConfirmConsignmentItemCount ShowPlannedConsignmentItemCount(ViewEventDelegate viewEventHandler, FlowDataConfirmConsignmentItemCount flowData)
        {
            var formData = new FormDataConfirmConsignmentItemCount
                {
               HeaderText = flowData.HeaderText,
               Consignment = flowData.Consignment,
           };

            var view = Commands.ViewCommands.ShowView<FormConfirmConsignmentItemCount>(formData);
            view.SetEventHandler(viewEventHandler);
            return view;
        }

    }
}

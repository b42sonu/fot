﻿using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.LoadUnloadCarrier
{
    public class ActionCommandsLoadUnloadCarrier
    {
        private static ICommunicationClient _communicationClient;

        internal ActionCommandsLoadUnloadCarrier(ICommunicationClient communicationClient)
        {
            _communicationClient = communicationClient;
        }


        internal bool IsLoadCarrierValidAndConnectedToPhysical(ConsignmentEntity loadCarrierEntity)
        {
            var isValidCarrier = ActionCommandsScanBarcode.ValidateLoadCarrier(loadCarrierEntity.CastToLoadCarrier());
            if(isValidCarrier)
            {
                isValidCarrier = IsLoadCarrierConnectedToPhysical(loadCarrierEntity);
            }
            return isValidCarrier;
        }

        internal bool IsLoadCarrierConnectedToPhysical(ConsignmentEntity loadCarrierEntity)
        {
            var loadCarrier = (LoadCarrier)loadCarrierEntity;
            if (loadCarrier.LogicalLoadCarrierDetails != null)
            {
                if (loadCarrier.LogicalLoadCarrierDetails != null)
                {
                    var logicalLoadCarrier = loadCarrier.LogicalLoadCarrierDetails[0];

                    if (logicalLoadCarrier == null)
                        return false;

                    return Convert.ToString(logicalLoadCarrier.PhysicalId) != "generisk" && Convert.ToString(logicalLoadCarrier.PhysicalLoadCarrierId) != "1";
                }
            }
            return false;
        }


        //TODO :: Write Unit Test
        internal bool ValidateLoadCarrierBeforeLoading(ConsignmentEntity loadCarrierEntity, WorkListItem currentWorkListItem)
        {
            var parentLoadCarrierId = currentWorkListItem != null ? currentWorkListItem.LoadCarrierId : string.Empty;
            
            return ActionCommandsScanBarcode.ValidateLoadCarrier(loadCarrierEntity.CastToLoadCarrier(), null, string.Empty, true, parentLoadCarrierId);
        }



        //TODO :: Write Unit Test
        internal void LoadUnloadCarrier(FlowDataLoadUnloadCarrier flowData, Process currentProcess, WorkListItem currentWorkListItem)
        {
            var eventCode = EventCodeUtil.GetEventCodeFromProcessForLoadCarrier(currentProcess);
            var eventInfo = LoadCarrierEventHelper.CreateLoadCarrierEvent(flowData.LoadCarrierEntity.LoadCarrierId, null, null, null, null, eventCode, null);
            
            if (eventInfo != null)
            {
                if (currentWorkListItem != null)
                {
                    if (currentWorkListItem.Type != WorkListItemType.Route)
                        eventInfo.EventInformation.LogicalLoadCarrierIdParent = currentWorkListItem.LoadCarrierId;

                    eventInfo.EventInformation.RouteIdAmphora = currentWorkListItem.RouteId;
                    if (!String.IsNullOrEmpty(currentWorkListItem.TripId))
                    {
                        eventInfo.EventInformation.TripIdAmphora = currentWorkListItem.TripId;
                        eventInfo.EventInformation.TripId = currentWorkListItem.TripId;    
                    }
                }
                if(CommunicationClient.Instance != null)
                CommunicationClient.Instance.SendLoadCarrierEvent(eventInfo);
            }
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// method for check is pad online or offline...
        /// </summary>
        /// <returns></returns>
        internal bool IsPdaOffline()
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsWorkList.IsSingleOperationReceived()");
            bool result = !_communicationClient.IsConnected();
            return result;
        }


        internal void ShowWarningForRegistrationWithoutValidation()
        {
            GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.EventRegisteredWithResponse, Severity.Warning, GlobalTexts.Ok);
        }

        internal bool ShowConfirmationForCreationOfWli(FlowResultLoadUnloadCarrier flowResult)
        {
            var result = GuiCommon.ShowModalDialog(GlobalTexts.Info, GlobalTexts.DoYouWantToActivateWorklist, Severity.Info, GlobalTexts.Confirm, GlobalTexts.Cancel);
            if (result != GlobalTexts.Confirm)
            {
                flowResult.State = FlowResultState.Cancel;
                return false;
            }
            return true;
        }


        internal FlowStatesLoadUnloadCarrier DecideNextStateAfterValidation(Process currentProcess)
        {
            switch (currentProcess)
            {
                case Process.UnloadLineHaul:
                case Process.LoadLineHaul:
                    return FlowStatesLoadUnloadCarrier.ViewShowActivationPopUp;
                case Process.LoadDistribTruck:
                    //If role will be driver2, then no pop will be shown it will always be loaded.
                    return ModelUser.UserProfile.Role == Role.Driver2 || ModelUser.UserProfile.Role == Role.Landpostbud
                               ? FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierAlreadyExists
                               : FlowStatesLoadUnloadCarrier.ViewShowActivationPopUp;
                
            }
            return FlowStatesLoadUnloadCarrier.ThisFlowComplete;
        }

        internal FlowStatesLoadUnloadCarrier DecideNextStateAfterNoValidation(Process currentProcess)
        {

            switch (currentProcess)
            {
                case Process.LoadDistribTruck:
                    //If role will be driver2, then no pop will be shown it will always be loaded.
                    return ModelUser.UserProfile.Role == Role.Driver2 || ModelUser.UserProfile.Role == Role.Landpostbud
                               ? FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierAlreadyExists
                               : FlowStatesLoadUnloadCarrier.ViewShowActivationPopUp;
                case Process.LoadLineHaul:
                    return FlowStatesLoadUnloadCarrier.ViewShowActivationPopUp;
                
            }
            return FlowStatesLoadUnloadCarrier.ThisFlowComplete;
        }

        internal bool IsLoadCarrierAlreadyExists(FlowDataLoadUnloadCarrier flowData, FlowResultLoadUnloadCarrier flowResult)
        {
            var result = flowData.EntityMap.IsScannedBefore(flowData.LoadCarrierEntity);
            flowResult.LoadUnloadResultType = LoadUnloadResultType.LoadCarrierAlreadyLoaded;
            flowResult.State = FlowResultState.Finished;
            return result;
        }


    }
}

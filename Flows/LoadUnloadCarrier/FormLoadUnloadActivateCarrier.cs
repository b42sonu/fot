﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.LoadUnloadCarrier
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormSelectCarrier.
    /// </summary>
    public class FormDataLoadUnloadActivateCarrier : BaseFormData
    {
        public bool IsForLoading { get; set; }
    }

    public partial class FormLoadUnloadActivateCarrier : BaseForm
    {

        #region Private, Readonly and const variables
        private FormDataLoadUnloadActivateCarrier _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonLoad = "ButtonLoad";
        private const string ButtonActivate = "ButtonActivate";
        #endregion

        /// <summary>
        /// Acts as default constructor for the form.
        /// </summary>
        public FormLoadUnloadActivateCarrier()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            
            AddButtons();
        }
        #region Methods and Functions

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormDataLoadUnloadActivateCarrier.AddButtons()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Load, ButtonLoadClick, TabButtonType.Confirm) { Name = ButtonLoad });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Activate, ButtonAtivateClick, ButtonActivate));
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadingScan.AddButtons");
            }
        }

      
        public override void OnShow(IFormData formData)
        {
            _formData = (FormDataLoadUnloadActivateCarrier)formData;
            labelModuleName.Text = _formData.HeaderText;
        }

        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnUpdateView(IFormData formData)
        {
            if (formData != null)
            {
                _formData = (FormDataLoadUnloadActivateCarrier)formData;
                _multiButtonControl.GetButton(ButtonLoad).Text = _formData.IsForLoading ? GlobalTexts.Load : GlobalTexts.Unload;
                lblMessage.Text = _formData.IsForLoading
                                      ? GlobalTexts.LoadOrActivateLoadCarrier
                                      : GlobalTexts.UnloadOrActivateLoadCarrier;
            }
        }

      
        #endregion

        #region Events

        /// <summary>
        /// This method acts as handler for click event of button btnCancel. It does following:
        /// 1) Hides the new carrier popup
        /// 2) Enables the other buttons on parent screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadUnloadActivateCarrierEvent.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadUnloadActivateCarrier.BtnBackClick");
            }
        }

        private void ButtonAtivateClick(object sender, System.EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadUnloadActivateCarrierEvent.Activate);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadUnloadActivateCarrier.BtnAtivateClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of continue button. It does the following:
        /// 1) Sends the message to flow that, user clicked continue button and passes the required data, so that flow
        /// can handle this event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLoadClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(_formData.IsForLoading ? LoadUnloadActivateCarrierEvent.Load : LoadUnloadActivateCarrierEvent.Unload);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadUnloadActivateCarrier.BtnLoadClick");
            }
        }

        #endregion

    }


    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectCarrier.
    /// </summary>
    public class LoadUnloadActivateCarrierEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Load = 1;
        public const int Unload = 2;
        public const int Activate = 3;

        public static implicit operator LoadUnloadActivateCarrierEvent(int viewEvent)
        {
            return new LoadUnloadActivateCarrierEvent { Value = viewEvent };
        }
    }
}
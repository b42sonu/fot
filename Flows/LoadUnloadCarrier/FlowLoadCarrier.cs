﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.LoadUnloadCarrier
{

    public enum LoadUnloadResultType
    {
        LoadCarrierLoaded,
        ActivateLoadCarrier,
        LoadCarrierAlreadyLoaded,
        NoUnloadingOfCarrier,
        NoLoadingOfCarrier,
        LoadCarrierUnloaded,
        LoadCarrierNotValidated
    }

    /// <summary>
    /// Enum which specifies the collection of stateId for this flow.
    /// </summary>
    public enum FlowStatesLoadUnloadCarrier
    {
        ActionValidateLoadCarrierBeforeLoading,
        ActionValidateLoadCarrierWithLm,
        ActionIsLoadCarrierConnectedToPhysical,
        ActionIsProcessLoadingOfDistributionTruck,
        ActionActivateNewWorkListItem,
        ActionLoadCarrier,
        ActionUnloadCarrier,
        ActionIsLoadCarrierAlreadyExists,
        ActionDecideNextStateAfterValidation,
        ActionDecideNextStateAfterNoValidation,
        ActionNoUnloadOfCarrier,
        ActionNoLoadOfCarrier,
        ActionLoadUnloadCarrier,
        ActionIsOfflineInFirstValidation,
        ActionIsOfflineInSecondValidation,
        ActionIsValidatedOkInFirstTimeValidation,
        ActionIsValidatedOkInSecondTimeValidation,
        ActionInValidLoadCarrier,
        ActionCheckTypeOfWli,
        ActionIsLoadCarrierValidAndConnectedToPhysical,
        ActionWarningForRegistrationWithoutValidation,
        ActionConfirmCreationOfWli,


        ViewCommands,
        ViewShowActivationPopUp,
        ThisFlowComplete
    }

    public class FlowDataLoadUnloadCarrier : BaseFlowData
    {
        public WorkListItem CurrentWorkListItem { get; set; }
        public EntityMap EntityMap { get; set; }
        public ConsignmentEntity LoadCarrierEntity { get; set; }
    }

    public class FlowResultLoadUnloadCarrier : BaseFlowResult
    {
        public LoadUnloadResultType LoadUnloadResultType { get; set; }
    }


    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowLoadCarrier : BaseFlow
    {

        private readonly FlowResultLoadUnloadCarrier _flowResult;
        private readonly ActionCommandsLoadUnloadCarrier _actionCommands;
        private FlowDataLoadUnloadCarrier _flowData;
        private bool _isValidatedOk;


        public FlowLoadCarrier()
        {
            _flowResult = new FlowResultLoadUnloadCarrier();
            _actionCommands = new ActionCommandsLoadUnloadCarrier(CommunicationClient.Instance);
        }



        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing FlowLoadUnloadCarrier");

            _flowData = (FlowDataLoadUnloadCarrier)flowData;

            ExecuteActionState(FlowStatesLoadUnloadCarrier.ActionValidateLoadCarrierWithLm);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesLoadUnloadCarrier)state);
        }

        public void ExecuteState(FlowStatesLoadUnloadCarrier state)
        {
            if (state > FlowStatesLoadUnloadCarrier.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesLoadUnloadCarrier state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowLoadUnloadCarrier;" + state);
                switch (state)
                {
                    case FlowStatesLoadUnloadCarrier.ViewShowActivationPopUp:
                        ViewCommandsLoadUnloadCarrier.ShowPopToLoadOrActivate(LoadUnloadActivateCarrierViewEventHandler, _flowData.HeaderText, CurrentProcess);
                        break;
                    case FlowStatesLoadUnloadCarrier.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowLoadUnloadCarrier.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesLoadUnloadCarrier.ThisFlowComplete);
            }
        }



        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStatesLoadUnloadCarrier state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowLoadUnloadCarrier;" + state);
                    switch (state)
                    {
                        case FlowStatesLoadUnloadCarrier.ActionValidateLoadCarrierWithLm:
                            _isValidatedOk = ActionCommandsScanBarcode.ValidateLoadCarrier(_flowData.LoadCarrierEntity.CastToLoadCarrier());
                            state = FlowStatesLoadUnloadCarrier.ActionIsOfflineInFirstValidation;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionIsOfflineInFirstValidation:
                            bool result = _actionCommands.IsPdaOffline();
                            state = result
                                        ? FlowStatesLoadUnloadCarrier.ActionDecideNextStateAfterNoValidation
                                        : FlowStatesLoadUnloadCarrier.ActionIsValidatedOkInFirstTimeValidation;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionIsValidatedOkInFirstTimeValidation:
                            state = _isValidatedOk
                                        ? FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierConnectedToPhysical
                                        : FlowStatesLoadUnloadCarrier.ActionInValidLoadCarrier;

                            break;

                        case FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierConnectedToPhysical:
                            result = _actionCommands.IsLoadCarrierConnectedToPhysical(_flowData.LoadCarrierEntity);
                            state = result
                                        ? FlowStatesLoadUnloadCarrier.ActionActivateNewWorkListItem
                                        : FlowStatesLoadUnloadCarrier.ActionDecideNextStateAfterValidation;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionInValidLoadCarrier:
                            if (_flowData.LoadCarrierEntity.IsScannedOnline == false)
                                state = FlowStatesLoadUnloadCarrier.ActionDecideNextStateAfterNoValidation;
                            else
                            {
                                _flowResult.LoadUnloadResultType = LoadUnloadResultType.LoadCarrierNotValidated;
                                _flowResult.State = FlowResultState.Finished;
                                state = FlowStatesLoadUnloadCarrier.ThisFlowComplete;    
                            }
                            
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionDecideNextStateAfterNoValidation:
                            state = _actionCommands.DecideNextStateAfterNoValidation(CurrentProcess);
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionDecideNextStateAfterValidation:
                            state = _actionCommands.DecideNextStateAfterValidation(CurrentProcess);
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionNoLoadOfCarrier:
                            _flowResult.LoadUnloadResultType = LoadUnloadResultType.NoLoadingOfCarrier;
                            _flowResult.State = FlowResultState.Finished;
                            state = FlowStatesLoadUnloadCarrier.ThisFlowComplete;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierAlreadyExists:
                            result = _actionCommands.IsLoadCarrierAlreadyExists(_flowData, _flowResult);
                            state = result ? FlowStatesLoadUnloadCarrier.ThisFlowComplete : FlowStatesLoadUnloadCarrier.ActionCheckTypeOfWli;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionCheckTypeOfWli:
                            state = _flowData.CurrentWorkListItem.Type == WorkListItemType.LoadCarrier
                                        ? FlowStatesLoadUnloadCarrier.ActionValidateLoadCarrierBeforeLoading
                                        : FlowStatesLoadUnloadCarrier.ActionLoadCarrier;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionValidateLoadCarrierBeforeLoading:
                            _isValidatedOk = _actionCommands.ValidateLoadCarrierBeforeLoading(_flowData.LoadCarrierEntity,
                                                                                      _flowData.CurrentWorkListItem);
                            state = FlowStatesLoadUnloadCarrier.ActionIsOfflineInSecondValidation;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionIsOfflineInSecondValidation:
                            result = _actionCommands.IsPdaOffline();
                            state = result
                                        ? FlowStatesLoadUnloadCarrier.ActionLoadCarrier
                                        : FlowStatesLoadUnloadCarrier.ActionIsValidatedOkInSecondTimeValidation;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionIsValidatedOkInSecondTimeValidation:
                            state = _isValidatedOk
                                        ? FlowStatesLoadUnloadCarrier.ActionLoadCarrier
                                        : FlowStatesLoadUnloadCarrier.ActionNoLoadOfCarrier;

                            break;

                        case FlowStatesLoadUnloadCarrier.ActionLoadCarrier:
                            _flowResult.LoadUnloadResultType = LoadUnloadResultType.LoadCarrierLoaded;
                            _actionCommands.LoadUnloadCarrier(_flowData, CurrentProcess, _flowData.CurrentWorkListItem);
                            state = FlowStatesLoadUnloadCarrier.ThisFlowComplete;
                            break;

                        case FlowStatesLoadUnloadCarrier.ActionActivateNewWorkListItem:
                            _flowResult.State = FlowResultState.Finished;
                            _flowResult.LoadUnloadResultType = LoadUnloadResultType.ActivateLoadCarrier;
                            state = FlowStatesLoadUnloadCarrier.ThisFlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStatesLoadUnloadCarrier.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowLoadUnloadCarrier.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStatesLoadUnloadCarrier.ThisFlowComplete;
            }


            ExecuteViewState(state);
        }

        public void LoadUnloadActivateCarrierViewEventHandler(int loadActivateEvent, object[] data)
        {

            Logger.LogEvent(Severity.Debug, "Executing FlowLoadUnloadCarrier.LoadUnloadActivateCarrierViewEventHandler");

            switch ((LoadUnloadActivateCarrierEvent)loadActivateEvent)
            {
                case LoadUnloadActivateCarrierEvent.Cancel:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteViewState(FlowStatesLoadUnloadCarrier.ThisFlowComplete);
                    break;

                case LoadUnloadActivateCarrierEvent.Load:

                    ExecuteActionState(FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierAlreadyExists);
                    break;

                case LoadUnloadActivateCarrierEvent.Unload:

                    ExecuteActionState(FlowStatesLoadUnloadCarrier.ActionIsLoadCarrierAlreadyExists);
                    break;

                case LoadUnloadActivateCarrierEvent.Activate:
                    _flowResult.LoadUnloadResultType = LoadUnloadResultType.ActivateLoadCarrier;
                    _flowResult.State = FlowResultState.Finished;
                    ExecuteViewState(FlowStatesLoadUnloadCarrier.ThisFlowComplete);
                    break;
            }
        }


        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowLoadUnloadCarrier");

            switch (_flowResult.State)
            {
                case FlowResultState.Finished:
                    switch (_flowResult.LoadUnloadResultType)
                    {
                        case LoadUnloadResultType.LoadCarrierAlreadyLoaded:
                        case LoadUnloadResultType.NoUnloadingOfCarrier:
                        case LoadUnloadResultType.NoLoadingOfCarrier:
                            SoundUtil.Instance.PlayWarningSound();
                            break;
                    }
                    break;
                case FlowResultState.Cancel:
                    SoundUtil.Instance.PlayWarningSound();
                    break;

                case FlowResultState.Error:
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;
            }


            BaseModule.EndSubFlow(_flowResult);
        }
    }

}

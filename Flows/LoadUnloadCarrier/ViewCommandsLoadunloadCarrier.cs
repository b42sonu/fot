﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Storage;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.LoadUnloadCarrier
{
    public class ViewCommandsLoadUnloadCarrier
    {

        internal static FormLoadUnloadActivateCarrier ShowPopToLoadOrActivate(ViewEventDelegate viewEventHandler, string headerText, Process process)
        {
            var formData = new FormDataLoadUnloadActivateCarrier
                               {
                                   HeaderText = headerText,
                                   IsForLoading =
                                       (process == Process.LoadLineHaul || process == Process.LoadDistribTruck)
                               };
            var view = ViewCommands.ShowView<FormLoadUnloadActivateCarrier>(formData);
            view.SetEventHandler(viewEventHandler);
            view.UpdateView(formData);
            return view;
        }
    }
}

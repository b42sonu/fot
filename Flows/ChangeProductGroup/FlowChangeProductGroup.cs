﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup
{
    /// <summary>
    /// Enum which specifies the collection of State for this flow.
    /// </summary>
    public enum FlowStateProductGroup
    {
        ActionBackFromScanBarcode,
        ActionIsConsignmentEntityScanned,
        ActionValidateGoods,
        ActionValidateScan,
        ActionPerformProcessRelatedValidation,
        ActionGetPossibleGroupChanges,
        ActionSendRegisterProductGroupChange,
        ActionBackFromRegisterWaybill,
        ActionBackFromProductGroupScan,
        ActionCreateConsignmentEntity,
        ActionProductCategoryChanged,
        ActionValidateProductCategory,

        ViewCommands,

        FlowRegisterWaybill,
        ViewChangeProductGroup,
        ViewChangeProductGroupScan,
        ViewChooseProductGroup,
        ViewUpdateProductGroupScan,
        ViewUpdateChooseProductGroup,
        FlowComplete,
        FlowError,
        FlowCanceled
    }

    public class FlowDataProductGroup : BaseFlowData
    {
        //The flow will go another path when the damage comes from correction menu
        public bool FromCorrectionMenu { get { return ConsignmentEntity != null; } }

        //This is set when entering from Correction menu
        public ConsignmentEntity ConsignmentEntity { get; set; }

        public MessageHolder MessageHolder { get; set; }
    }

    public class FlowResultProductGroup : BaseFlowResult
    {
        public MessageHolder MessageHolder { get; set; }
    }

    /// <summary>
    /// US 35 http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554926
    /// </summary>
    public class FlowChangeProductGroup : BaseFlow
    {

        private readonly ActionCommandsChangeProductGroup _actionCommandChangeProductGroup;
        private FlowDataProductGroup _flowData;
        private FotScannerOutput _scannerOutput;
        private BarcodeData _currentScanning;
        private readonly FlowResultProductGroup _flowResult;
        private readonly ActionCommandsScanBarcode _actionCommandsScanBarcode;
        private readonly EntityMap _entityMap;
        private MessageHolder _messageHolder;
        private readonly ViewCommandsChangeProductGroup _viewCommands;
        private ConsignmentEntity _currentConsignmentEntity;
        private List<ProductGroupTransition> _possibleGroupTransitions;
        private string _changedProductCategory;
        
        public FlowChangeProductGroup()
        {
            _messageHolder = new MessageHolder();
            _entityMap = new EntityMap();
            _actionCommandChangeProductGroup = new ActionCommandsChangeProductGroup(_messageHolder);
            _viewCommands = new ViewCommandsChangeProductGroup();
            _flowResult = new FlowResultProductGroup();
            _actionCommandsScanBarcode = new ActionCommandsScanBarcode(_messageHolder);
        }

        public override void BeginFlow(IFlowData flowData)
        {
            _flowData = (FlowDataProductGroup)flowData;
            _currentConsignmentEntity = _flowData.ConsignmentEntity;
            //Store in cache
            if (_flowData.FromCorrectionMenu)
                _entityMap.Store(_currentConsignmentEntity);
            ExecuteState(FlowStateProductGroup.ActionIsConsignmentEntityScanned);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateProductGroup)state);
        }

        public void ExecuteState(FlowStateProductGroup state)
        {
            if (state > FlowStateProductGroup.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteActionState(FlowStateProductGroup state)
        {
            try
            {
                do
                {
                    bool result;
                    Logger.LogEvent(Severity.Debug, "#FlowChangeProductGroup;" + state);
                    switch (state)
                    {
                        case FlowStateProductGroup.ActionIsConsignmentEntityScanned:
                            state = _actionCommandChangeProductGroup.IsFromCorrectionMenu(_flowData, ref _currentConsignmentEntity);
                            break;

                        case FlowStateProductGroup.ActionValidateGoods:
                            state = _actionCommandChangeProductGroup.RetriveProductGroup(ref _currentConsignmentEntity, _entityMap, _flowResult);
                            break;

                        case FlowStateProductGroup.ActionBackFromRegisterWaybill:
                            if (CurrentView != null) CurrentView.AutoShow = true;
                            state = _actionCommandChangeProductGroup.ValidateRegisterWaybillResult((FlowResultRegisterWaybill)SubflowResult, _flowData.FromCorrectionMenu, _currentConsignmentEntity.CastToConsignmentItem());

                            break;

                        case FlowStateProductGroup.ActionValidateScan:
                            result = _actionCommandsScanBarcode.ValidateBarcode(_scannerOutput, BarcodeType.Shipment, out _currentScanning, false);
                            state = result
                                        ? FlowStateProductGroup.ActionCreateConsignmentEntity
                                        : FlowStateProductGroup.ViewChangeProductGroupScan;
                            break;

                        case FlowStateProductGroup.ActionCreateConsignmentEntity:
                            _currentConsignmentEntity = ActionCommandsScanBarcode.CreateConsignmentEntity(_currentScanning);
                            state = FlowStateProductGroup.ActionValidateGoods;
                            break;

                        case FlowStateProductGroup.ActionGetPossibleGroupChanges:
                            _possibleGroupTransitions = _actionCommandChangeProductGroup.GetPossibleProductGroupChanges(_currentConsignmentEntity.ProductCategory, _flowResult);
                            state = _possibleGroupTransitions.Count > 0 ?
                                FlowStateProductGroup.ViewChooseProductGroup : FlowStateProductGroup.ViewChangeProductGroupScan;
                            break;

                        case FlowStateProductGroup.ActionValidateProductCategory:
                            result = _actionCommandChangeProductGroup.ValidateProductCategory(_currentConsignmentEntity.ProductCategory, _changedProductCategory, _messageHolder);
                            state = result ? FlowStateProductGroup.ActionProductCategoryChanged : FlowStateProductGroup.ViewUpdateChooseProductGroup;
                            break;

                        case FlowStateProductGroup.ActionProductCategoryChanged:
                            _actionCommandChangeProductGroup.SendRegisterProductGroupChangeEvent(_currentConsignmentEntity, _changedProductCategory, _flowResult);
                            state = _flowData.FromCorrectionMenu ? FlowStateProductGroup.FlowComplete : FlowStateProductGroup.ViewChangeProductGroupScan;
                            break;

                    }
                } while (state < FlowStateProductGroup.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Failed to execute ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateProductGroup.FlowComplete;
            }
            ExecuteViewState(state);
        }

        private void ExecuteViewState(FlowStateProductGroup state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowChangeProductGroup;" + state);

                switch (state)
                {
                    case FlowStateProductGroup.ViewChangeProductGroupScan:
                        _viewCommands.FormChangeProductGroupScan(ChangeProductGroupScanEventHandler, _currentConsignmentEntity, _flowData.FromCorrectionMenu, _messageHolder);
                        break;

                    case FlowStateProductGroup.ViewUpdateProductGroupScan:
                        IFormData formData = new FormDataChangeProductGroupScan { ConsignmentEntity = _currentConsignmentEntity, MessageHolder = _messageHolder, FromCorrectionMenu = _flowData.FromCorrectionMenu };
                        CurrentView.UpdateView(formData);
                        break;

                    case FlowStateProductGroup.ViewChooseProductGroup:
                        _viewCommands.ChooseProductGroup(ChooseProductGroupEventHandler, _flowData, _possibleGroupTransitions, _currentConsignmentEntity);
                        break;

                    case FlowStateProductGroup.ViewUpdateChooseProductGroup:
                        IFormData formDataProductGroup = new FormDataChooseProductGroup { MessageHolder = _messageHolder };
                        CurrentView.UpdateView(formDataProductGroup);
                        break;

                    case FlowStateProductGroup.FlowRegisterWaybill:
                        if (CurrentView != null) 
                            CurrentView.AutoShow = false;
                        StartSubFlow<FlowRegisterWaybill>(new FlowDataRegisterWaybill { ConsignmentItem = _currentConsignmentEntity as ConsignmentItem, HeaderText = GlobalTexts.ChangeProductGroup , IsCalledFromExternalFlow = true}, (int)FlowStateProductGroup.ActionBackFromRegisterWaybill, Process.ChangeProductGroup);
                        break;

                    case FlowStateProductGroup.FlowError:
                        _flowResult.State = FlowResultState.Error;
                        _flowResult.Message = GlobalTexts.ChangeProductGroupAborted;
                        EndFlow();
                        break;

                    case FlowStateProductGroup.FlowComplete:
                        EndFlow();
                        break;

                    case FlowStateProductGroup.FlowCanceled:
                        _flowResult.State = FlowResultState.Error;
                        _flowResult.Message = GlobalTexts.AlterProductGroup + " " + GlobalTexts.Cancelled;
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "Encountered unhandled view FlowChangeProductGroup : " + state);
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowChangeProductGroup.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateProductGroup.FlowComplete);
            }
        }


        public void ChooseProductGroupEventHandler(int viewEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing ChooseProductGroupEventHandler()");
            switch (viewEvent)
            {

                case ChooseProductGroupFormEvent.Ok:
                    _changedProductCategory = (string)data[0];
                    ExecuteState(FlowStateProductGroup.ActionValidateProductCategory);
                    break;

                case ChooseProductGroupFormEvent.Back:
                    if (_flowData.FromCorrectionMenu)
                        ExecuteState(FlowStateProductGroup.FlowCanceled);
                    else
                    {
                        _messageHolder.Update(GlobalTexts.ChangeProductGroupAborted, MessageState.Warning);
                        ExecuteState(FlowStateProductGroup.ViewChangeProductGroupScan);
                    }
                    break;

                case ChooseProductGroupFormEvent.Finished:
                    _flowResult.MessageHolder = _messageHolder;
                    ClearLocalVariables();
                    ExecuteState(FlowStateProductGroup.FlowComplete);
                    break;
            }
        }

        private void ClearLocalVariables()
        {
            _possibleGroupTransitions = null;
            _currentConsignmentEntity = null;
            _changedProductCategory = null;
            _currentConsignmentEntity = null;
        }


        private void ChangeProductGroupScanEventHandler(int viewEvents, params object[] data)
        {
            switch (viewEvents)
            {
                case ChangeProductGroupViewEvents.Ok:
                    _flowResult.State = FlowResultState.Ok;
                    _flowResult.Message = String.Format(GlobalTexts.ProductGroupChangeRegistered, _currentConsignmentEntity.ConsignmentId);
                    
                    _messageHolder.Clear();
                    ExecuteState(_flowData.FromCorrectionMenu ? FlowStateProductGroup.FlowComplete : FlowStateProductGroup.ViewChangeProductGroupScan);
                    break;

                case ChangeProductGroupViewEvents.BarcodeScanned:
                    _currentConsignmentEntity = null;
                    _scannerOutput = (FotScannerOutput)data[0];
                    ExecuteState(FlowStateProductGroup.ActionValidateScan);
                    break;

                case ScanBarcodeViewEvents.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStateProductGroup.FlowComplete);
                    break;
            }
        }


        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowChangeProductGroup");
            BaseModule.EndSubFlow(_flowResult);
        }


    }
}

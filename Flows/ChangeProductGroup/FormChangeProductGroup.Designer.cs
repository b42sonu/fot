﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup
{
    partial class FormChangeProductGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            //.cmbProductGroup = new System.Windows.Forms.ComboBox();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.labelConsignmentItem = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblCurrentProductGroup = new Resco.Controls.CommonControls.TransparentLabel();
            this.listProductCategories = new System.Windows.Forms.ListBox();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentProductGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
           // this.touchPanel.Controls.Add(this.cmbProductGroup);
            this.touchPanel.Controls.Add(this.labelTask);
            this.touchPanel.Controls.Add(this.labelConsignment);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.labelConsignmentItem);
            this.touchPanel.Controls.Add(this.lblCurrentProductGroup);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.listProductCategories);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // cmbProductGroup
            // 
            //this.cmbProductGroup.Location = new System.Drawing.Point(22, 145);
            //this.cmbProductGroup.Name = "cmbProductGroup";
            //this.cmbProductGroup.Size = new System.Drawing.Size(436, 41);
            //this.cmbProductGroup.TabIndex = 49;
            // 
            // labelTask
            // 
            this.labelTask.AutoSize = false;
            this.labelTask.Location = new System.Drawing.Point(22, 109);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(300, 30);
            // 
            // labelConsigment
            // 
            this.labelConsignment.Location = new System.Drawing.Point(22, 359);
            this.labelConsignment.Name = "labelConsigment";
            this.labelConsignment.Size = new System.Drawing.Size(425, 29);

            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 42);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // messageControlValidateScan
            // 
            this.messageControlValidateScan.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControlValidateScan.Location = new System.Drawing.Point(22, 206);
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(436, 120);
            this.messageControlValidateScan.TabIndex = 56;

            // 
            // listProductCategories
            // 
            this.listProductCategories.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.listProductCategories.Location = new System.Drawing.Point(21, 105);
            this.listProductCategories.Name = "listProductCategories";
            this.listProductCategories.Size = new System.Drawing.Size(438, 243);
            this.listProductCategories.TabIndex = 4;
            // 
            // labelConsignmentItem
            // 
            this.labelConsignmentItem.Location = new System.Drawing.Point(21, 355);
            this.labelConsignmentItem.Name = "labelConsignmentItem";
            this.labelConsignmentItem.Size = new System.Drawing.Size(438, 30);

            // 
            // lblCurrentProductGroup
            // 
            this.lblCurrentProductGroup.Location = new System.Drawing.Point(21, 399);
            this.lblCurrentProductGroup.Name = "lblCurrentProductGroup";
            this.lblCurrentProductGroup.Size = new System.Drawing.Size(425, 29);
       

            // 
            // lblModuleName 
            // 
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 3);
            this.labelModuleName.Name = "lblModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(477, 33);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter; 
          
            // 
            // FormAttemptedDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormChangeProductGroup";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        //private System.Windows.Forms.ComboBox cmbProductGroup;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignment;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentItem;
        private Resco.Controls.CommonControls.TransparentLabel lblCurrentProductGroup;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private ListBox listProductCategories;
        private MessageControl messageControlValidateScan;
    }
}

﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup
{
    class ViewCommandsChangeProductGroup
    {

        internal void ChooseProductGroup(ViewEventDelegate viewEventHandler, FlowDataProductGroup flowData, List<ProductGroupTransition> possibleGroups, ConsignmentEntity consignmentEntity)
        {
            var formDataChooseProductGroup = new FormDataChooseProductGroup
            {
                MessageHolder = flowData.MessageHolder,
                LegalProductCategoryChanges = possibleGroups,
                ConsignmentEntity = consignmentEntity,
            };

            var view = ViewCommands.ShowView<FormChangeProductGroup>(formDataChooseProductGroup);
            view.SetEventHandler(viewEventHandler);
        }

        public void FormChangeProductGroupScan(ViewEventDelegate viewEventHandler, ConsignmentEntity consignmentEntity, bool fromCorrectionMenu, MessageHolder messageHolder)
        {
            var formDataChangeProductGroupScan = new FormDataChangeProductGroupScan
            {
                ConsignmentEntity = consignmentEntity,
                FromCorrectionMenu = fromCorrectionMenu,
                MessageHolder = messageHolder
            };
            var view = ViewCommands.ShowView<FormChangeProductGroupScan>(formDataChangeProductGroupScan);
            view.SetEventHandler(viewEventHandler);
        }




    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup
{
    public partial class FormChangeProductGroupScan : BaseBarcodeScanForm
    {
        #region private vars
        private readonly MultiButtonControl _tabButtons;

        public FormChangeProductGroupScan() 
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }

        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        #endregion

        #region private methods

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.AlterProductGroup;
            lblScanHeading.Text = BaseActionCommands.ScanTextFromValidBarcodeTypes(BarcodeType.Shipment,    GlobalTexts.ScanConsignmentItemOrPdf);
            lblHeading.Text = ModelUser.UserProfile.OrgUnitName;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton) { Activation = ActivateOn.IfNotScanned });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonType.Confirm) { Activation = ActivateOn.Always, Name = OkButton });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged,
                Title = lblScanHeading.Text
            };
            txtScannedNumber.Tag = keyBoardProperty;
        }



        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _tabButtons.SetButtonEnabledState(OkButton, txtScannedNumber.Text != string.Empty);
            if (messageControlValidateScan != null)
                messageControlValidateScan.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text != string.Empty)
            {
                BarcodeEnteredManually(textentered);
            }
        }

        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(ChangeProductGroupViewEvents.BarcodeScanned, scannerOutput);
        }


        private void ClearState()
        {
            txtScannedNumber.Visible = true;
            messageControlValidateScan.ClearMessage();
            lblScanHeading.Visible = true;
            _tabButtons.SetButtonEnabledState(BackButton, true);
            _tabButtons.SetButtonEnabledState(OkButton, false);

        }

        

        #endregion

        #region Overridden methods
        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtScannedNumber.Text = "";
            ViewEvent.Invoke(ChangeProductGroupViewEvents.BarcodeScanned, scannerOutput);
        }

        public override void OnShow(IFormData iFormData)
        {
            base.OnShow(iFormData);
            ClearState();
            var formData = (FormDataChangeProductGroupScan)iFormData;
            _tabButtons.SetButtonEnabledState(OkButton, txtScannedNumber.Text != string.Empty);
            if (formData != null && formData.MessageHolder != null)
                messageControlValidateScan.ShowMessage(formData.MessageHolder);
            if (formData != null) txtScannedNumber.Visible = !formData.FromCorrectionMenu;
            if (formData != null) lblScanHeading.Visible = !formData.FromCorrectionMenu;
            if (formData != null && formData.FromCorrectionMenu)
                StopScan();
            else
            {
                StartScan();    
            }
            SetKeyboardState(KeyboardState.Numeric);

        }

        public override void OnClose()
        {
            base.OnClose();
            messageControlValidateScan.ClearMessage();
            txtScannedNumber.Text = string.Empty;
        }

        #endregion

        #region Button Click

        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtScannedNumber.Text))
                {
                    ViewEvent.Invoke(ChangeProductGroupViewEvents.BarcodeScanned, new FotScannerOutput { Code = txtScannedNumber.Text.Trim() });
                }
                else
                {
                    ViewEvent.Invoke(ChangeProductGroupViewEvents.Ok);
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeProductGroupScan.ButtonConfirmClick");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ChangeProductGroupViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeProductGroupScan.ButtonConfirmClick");
            }
        }

        #endregion
    }
    #region external classes
    public class ChangeProductGroupViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
        public const int NewDamage = 3;
        public const int TakePhoto = 4;
    }

    public class FormDataChangeProductGroupScan : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
        public bool FromCorrectionMenu { get; set; }
        public ConsignmentEntity ConsignmentEntity { get; set; }
    }

    #endregion
}

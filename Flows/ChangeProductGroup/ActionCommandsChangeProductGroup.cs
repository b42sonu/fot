﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup
{
    internal class ActionCommandsChangeProductGroup : ActionCommandsScanBarcode
    {
        private readonly MessageHolder _messageHolder;

        /// <summary>
        /// Constructor to set Message Holder
        /// </summary>
        public ActionCommandsChangeProductGroup(MessageHolder messageHolder)
            : base(messageHolder)
        {
            _messageHolder = messageHolder;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Set appropriate states if called from Correction Menu
        /// </summary>
        public FlowStateProductGroup IsFromCorrectionMenu(FlowDataProductGroup flowDataProductGroup, ref ConsignmentEntity currentConsignmentEntity)
        {
            FlowStateProductGroup state;
            if (flowDataProductGroup.FromCorrectionMenu == false)//View Scan Form 
            {
                state = FlowStateProductGroup.ViewChangeProductGroupScan;
            }
            else //From Correction menu
            {
                if (currentConsignmentEntity is Consignment)//If scanned consignment previously get product category directly as Validate Goods has been sent
                    state = FlowStateProductGroup.ActionGetPossibleGroupChanges;
                else //If scanned consignment item need to call validate goods for consignment
                {
                    var consignmentItem = flowDataProductGroup.ConsignmentEntity as ConsignmentItem;
                    if (consignmentItem != null)
                    {
                        if (consignmentItem.HasUnknownConsignment) //Unknown Consignment
                            state = FlowStateProductGroup.FlowRegisterWaybill;
                        else
                        {
                            currentConsignmentEntity = new Consignment(consignmentItem.ConsignmentId);
                            state = FlowStateProductGroup.ActionValidateGoods;
                        }
                    }
                    else
                    {
                        _messageHolder.Update(GlobalTexts.NoItemScanned, MessageState.Error);
                        state = FlowStateProductGroup.ViewChangeProductGroupScan;
                    }
                }
            }
            return state;
        }

        /// <summary>
        /// Try to retrieve product group of consignment
        /// </summary>
        public FlowStateProductGroup RetriveProductGroup(ref ConsignmentEntity consignmentEntity, EntityMap entityMap, FlowResultProductGroup flowResult)
        {
            try
            {
                // If consignment id is known, we only need to validate it
                if (consignmentEntity.HasUnknownConsignment == false)
                {
                    if (ValidateConsignment(consignmentEntity, entityMap) == false)
                        return FlowStateProductGroup.ViewChangeProductGroupScan;
                    return FlowStateProductGroup.ActionGetPossibleGroupChanges;
                }

                ValidateGoods(entityMap, consignmentEntity);
                if (consignmentEntity.IsScannedOnline == false)//If not online set error message message
                {
                    SetErrorState(GlobalTexts.OfflineOrTimeout);
                    return FlowStateProductGroup.ViewChangeProductGroupScan;
                }

                if (consignmentEntity.IsConsignmentSynthetic && consignmentEntity.HasUnknownConsignment == false)//Synthetic or Unknown 
                {
                    SetErrorState(GlobalTexts.CantProcessSyntheticConsignment);
                    return FlowStateProductGroup.ViewChangeProductGroupScan;
                }

                var consignmentItem = consignmentEntity as ConsignmentItem;
                if (consignmentItem != null)
                {
                    if (consignmentItem.HasUnknownConsignment)//If consignment item is not validated call register waybill
                        return FlowStateProductGroup.FlowRegisterWaybill;

                    if (string.IsNullOrEmpty(consignmentItem.ProductCategory))
                    {
                        if (ValidateConsignment(consignmentEntity, entityMap) == false)
                            return FlowStateProductGroup.ViewChangeProductGroupScan;
                    }
                }

                return FlowStateProductGroup.ActionGetPossibleGroupChanges;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsChangeProductGroup.RetriveProductGroup");
                SetErrorState(GlobalTexts.ConsignmentIsUnknown);
                return FlowStateProductGroup.ViewChangeProductGroupScan;
            }
        }

        /// <summary>
        /// Validate the consignment instance of scanned consignment item to fetch Product Group of Consignment
        /// </summary>
        private bool ValidateConsignment(ConsignmentEntity consignmentEntity, EntityMap entityMap)
        {
            bool result = false;
            var consignment = consignmentEntity.ConsignmentInstance;
            ValidateGoods(entityMap, consignment);

            if (consignment.IsKnownInLm)
            {
                consignmentEntity.ProductCategory = consignment.ProductCategory;
                result = true;
            }
            else
                SetErrorState(GlobalTexts.ConsignmentIsUnknown);

            return result;
        }

        /// <summary>
        /// Set Error message
        /// </summary>
        public void SetErrorState(string message)
        {
            _messageHolder.Update(message, MessageState.Error);
            SoundUtil.Instance.PlayScanErrorSound();
        }

        /// <summary>
        /// Get product group codes to which current product group can be changed from local database
        /// </summary>
        public List<ProductGroupTransition> GetPossibleProductGroupChanges(string productGroupCode, FlowResultProductGroup flowResult)
        {
            var possibleGroupTransitions = SettingDataProvider.Instance.GetProductGroupTransitions(productGroupCode,
                ModelUser.UserProfile.LanguageCode);

            if (possibleGroupTransitions.Count == 0)
            {
                flowResult.State = FlowResultState.Error;
                _messageHolder.Update(GlobalTexts.NoAvailableProductCategories, MessageState.Error);
                SoundUtil.Instance.PlayScanErrorSound();
            }

            return possibleGroupTransitions;
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// Sends goods event for Product Group Change Event
        /// </summary>
        public bool SendRegisterProductGroupChangeEvent(ConsignmentEntity consignmentEntity, string changedProductCategory, FlowResultProductGroup flowResult)
        {
            try
            {
                // Send an event to backend that we have scanned this consignment item
                var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity.ConsignmentInstance);
                goodsEvent.EventInformation.Consignment[0].ProductCategory = changedProductCategory;

                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ActualsSummaryMessage, "Register Product Group Change");

                CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);

                var message = String.Format(GlobalTexts.ProductGroupChanged, consignmentEntity.ProductCategory, changedProductCategory, consignmentEntity.ConsignmentDisplayId);
                GuiCommon.ShowModalDialog(GlobalTexts.ChangeProductGroup, message, Severity.Info, GlobalTexts.Ok);

                flowResult.Message = message;
                flowResult.State = FlowResultState.Ok;

                //Set ConsignmentEntity to changed product group
                consignmentEntity.ProductCategory = changedProductCategory;
                return true;

            }
            catch (Exception e)
            {
                Logger.LogEvent(Severity.Error, "Unexpected error in execution of function ActionCommandsChangeProductGroup.SendRegisterProductGroupChangeEvent:" + e.Message);
                return false;
            }
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// Back from Flow Register Waybill 
        /// </summary>
        public FlowStateProductGroup ValidateRegisterWaybillResult(FlowResultRegisterWaybill subflowResult, bool fromCorrectionMenu, ConsignmentEntity consignmentEntity)
        {
            bool result;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok://Waybill registration information successfully sent
                    result = true;
                    SoundUtil.Instance.PlaySuccessSound();
                    break;

                case FlowResultState.Cancel://skipped Waybill registeration 
                    result = false;
                    _messageHolder.Update(GlobalTexts.ConsignmentIsUnknown, MessageState.Error);//Register Waybill canceled
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;

                case FlowResultState.Error:
                    result = false;
                    _messageHolder.Update(subflowResult.Message, MessageState.Error);
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unknown state");
                    break;
            }

            return result ? FlowStateProductGroup.ActionValidateGoods : FlowStateProductGroup.ViewChangeProductGroupScan;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Check for empty product groups
        /// </summary>
        internal bool ValidateProductCategory(string productCategory, string changedProductCategory, MessageHolder messageHolder)
        {
            if (string.IsNullOrEmpty(changedProductCategory))
            {
                messageHolder.Update(GlobalTexts.EmptyProductCategory, MessageState.Error);
                return false;
            }

            if (String.Compare(productCategory, changedProductCategory, StringComparison.OrdinalIgnoreCase) == 0)
            {
                messageHolder.Update(string.Format(GlobalTexts.ProductCategoryIsNotChanged, productCategory), MessageState.Error);
                return false;
            }

            messageHolder.Clear();
            return true;
        }
    }


}

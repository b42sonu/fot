﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup
{
    public class ChooseProductGroupFormEvent : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int Finished = 2;
    }

    public partial class FormChangeProductGroup : BaseForm
    {
        private readonly MultiButtonControl _tabButtons;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";
        public FormChangeProductGroup()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            SetStandardControlProperties(labelModuleName, lblHeading, labelTask, null, null, lblCurrentProductGroup, labelConsignment);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
        }

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.AlterProductGroup;
            labelConsignment.Text = GlobalTexts.Consignment;
            labelConsignmentItem.Text = GlobalTexts.ConsignmentItem;
            labelTask.Text = GlobalTexts.ChooseProductGroup + ":";
            lblHeading.Text = ModelUser.UserProfile.OrgUnitName;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm){Name=OkButton});
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            var formDataChooseProductGroup = (FormDataChooseProductGroup)formData;
            SetState(formDataChooseProductGroup.MessageHolder, true);
            if (formDataChooseProductGroup.ConsignmentEntity != null)
            {
                labelConsignment.Text = GlobalTexts.Consignment + ": " + formDataChooseProductGroup.ConsignmentEntity.ConsignmentId;
                if (formDataChooseProductGroup.ConsignmentEntity is ConsignmentItem)
                    labelConsignmentItem.Text = GlobalTexts.ConsignmentItem + ": " + formDataChooseProductGroup.ConsignmentEntity.EntityId;
                else
                    labelConsignmentItem.Text = string.Empty;

                lblCurrentProductGroup.Text = GlobalTexts.ProductCategory + ": " +
                                              formDataChooseProductGroup.ConsignmentEntity.ProductCategory;
                BindProductGroups(formDataChooseProductGroup.LegalProductCategoryChanges);
            }

            
            _tabButtons.SetButtonEnabledState(OkButton, formDataChooseProductGroup.LegalProductCategoryChanges.Count > 0);

        }

        /// <summary>
        /// 
        /// </summary>
        private void BindProductGroups(List<ProductGroupTransition> productGroups)
        {
            if (productGroups != null)
            {
                listProductCategories.DataSource = productGroups;
                listProductCategories.DisplayMember = "DisplayValue";
                listProductCategories.ValueMember = "ToCode";
                

                if(productGroups.Count > 0 )
                    listProductCategories.SelectedIndex = 0;
            }
        }

        private void SetState(MessageHolder messageHolder, bool state)
        {
            listProductCategories.Visible = state;
            labelModuleName.Text = state ? GlobalTexts.AlterProductGroup : GlobalTexts.AcceptChange;
            PopulateMessage(messageHolder, state);
        }

        private void PopulateMessage(MessageHolder messageHolder, bool state)
        {
            if (messageHolder != null)
            {
                if (messageHolder.State != MessageState.Empty)
                {
                    labelConsignment.Visible = state;
                    labelConsignmentItem.Visible = state;
                }

                messageControlValidateScan.ShowMessage(messageHolder);
            }
            else
                messageControlValidateScan.ClearMessage();
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if(listProductCategories.SelectedIndex > -1)
                    ViewEvent.Invoke(ChooseProductGroupFormEvent.Ok, listProductCategories.SelectedValue);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeProductGroup");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ChooseProductGroupFormEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeProductGroup.ButtonBack");
            }
        }

        public override void OnUpdateView(IFormData iformData)
        {
            var formData = (FormDataChooseProductGroup)iformData;
            PopulateMessage(formData.MessageHolder, false);
            SetKeyboardState(KeyboardState.Numeric);
        }

    }

    #region View specific classes
    public class FormDataChooseProductGroup : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
        public List<ProductGroupTransition> LegalProductCategoryChanges { get; set; }
        public ConsignmentEntity ConsignmentEntity { get; set; }
    }
    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ControlScan
{
    public class ControlScanFlowModel : IFlowData
    {
        internal FotScannerOutput FotScannerOutput;
        internal ConsignmentEntity ConsignmentEntity;
        internal T20200_ValidateGoodsFromFOTReply ValidateGoodsFromFotReply { get; set; }
        internal string SelectedCaption = string.Empty;
        internal string SelectedCellContent = string.Empty;
        internal List<History> ReplyHistory;
        public string HeaderText { get; set; }
        public string ConsignmentId { get; set; }
        public bool FromOperationList;
    }
    internal class History
    {
        internal string Id;
        internal T20200_ValidateGoodsFromFOTReply Reply;
        internal ConsignmentEntity Entity;
    }

    public class ControlScanViewModel
    {
        internal string HeaderText;
        internal string EntityId;
        internal bool IsConsignment;
        internal static bool SynteticConsignment;
        private readonly DbTablesDataCopy _dbTablesDataCopy;
        private static readonly int[] RoleOrderForInvisible = new[] { -1, -1, -1, -1, -1, -1, -1, -1, -1 };
        private static readonly int[] RoleOrderForCons = new[] { 2, 2, 2, 2, 2, 2, 2, 2, 2 };
        private static readonly int[] RoleOrderForStop = new[] { 3, 0, 0, 0, 0, 0, 3, 3, 0 };
        private static readonly int[] RoleOrderForLastEvent = new[] { 5, 0, 3, 3, 3, 0, 3, 3, 3 };
        private static readonly int[] RoleOrderForSend = new[] { 6, 6, 6, 6, 6, 6, 6, 6, 6 };
        private static readonly int[] RoleOrderForSRecp = new[] { 7, 7, 7, 7, 7, 7, 7, 7, 7 };
        private static readonly int[] RoleOrderForWv = new[] { 8, 8, 8, 8, 8, 0, 8, 0, 8 };
        private static readonly int[] RoleOrderForBlk = new[] { 4, 0, 0, 0, 0, 0, 0, 0, 0 };
        private static readonly int[] RoleOrderForDate = new[] { 10, 10, 10, 10, 10, 10, 0, 0, 0 };
        private static readonly int[] RoleOrderForPlc = new[] { 9, 9, 9, 9, 9, 9, 9, 0, 0 };
        private static readonly int[] RoleOrderForRtrn = new[] { 0, 0, 0, 0, 0, 0, 9, 0, 0 };
        private static readonly int[] RoleOrderForTemp = new[] { 11, 3, 11, 0, 0, 0, 0, 0, 0 };
        private static readonly int[] RoleOrderForCat = new[] { 12, 4, 12, 0, 0, 0, 0, 0, 0 };
        const string SpaceInFront = "    ";

        internal ControlScanViewModel(T20200_ValidateGoodsFromFOTReply validateGoodsResponse, bool isItemScanned, DbTablesDataCopy dbTablesDataCopy)
        {
            {
                var consignment = validateGoodsResponse.Consignment;
                IsConsignment = !isItemScanned;
                _dbTablesDataCopy = dbTablesDataCopy;
                var consignmentItem = isItemScanned ? consignment.ConsignmentItem[0] : new ValidationResponseConsignmentConsignmentItem();
                EntityId = isItemScanned ? consignmentItem.ConsignmentItemNumber : consignment.ConsignmentNumber;
                SynteticConsignment = consignment.IsSynthetic;
                HeaderText = (isItemScanned ? GlobalTexts.Item : GlobalTexts.Cons) + ":     " + EntityId;

                InfoRows = new List<ViewModelRow>
                         {
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Cons,
                                     Value = ConsignmentNumber(validateGoodsResponse)+ "    " + ConsignmentItemCount(consignment),
                                     RoleOrder = isItemScanned ? RoleOrderForCons : RoleOrderForInvisible
                                 },
  
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Stop,
                                     Value = StopOrder(consignmentItem),
                                     RoleOrder = RoleOrderForStop
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.LEv,
                                     Value = isItemScanned ? LastEventForItem(consignmentItem) : LastEventForConsignment(consignment),
                                     RoleOrder = RoleOrderForLastEvent
                                 },                        
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Send,
                                     Value = Sender(consignment),
                                     RoleOrder = RoleOrderForSend
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Recp,
                                     Value = Recipient(consignment),
                                     RoleOrder = RoleOrderForSRecp
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.WidthVolume,
                                     Value = isItemScanned? WeightAndVolumeItem(consignmentItem): WeightAndVolumeCons(consignment),
                                     RoleOrder = RoleOrderForWv
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Blk,
                                     Value = PartySuspended(consignment),
                                     RoleOrder = RoleOrderForBlk
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Plc,
                                     Value = ShelfLocation(consignmentItem),
                                     RoleOrder = isItemScanned? RoleOrderForPlc:RoleOrderForInvisible
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Rtrn,
                                     Value = PlannedReturndate(consignmentItem),
                                     RoleOrder = RoleOrderForRtrn
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Date,
                                     Value = PlannedDeliverydate(consignmentItem),
                                     RoleOrder = RoleOrderForDate
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.DegrC,
                                     Value = "not implemented",
                                     RoleOrder = RoleOrderForTemp
                                 },
                             new ViewModelRow
                                 {
                                     Caption = GlobalTexts.Cat,
                                     Value = ProductCategory(consignment),
                                     RoleOrder= RoleOrderForCat
                                 }
                         };

                InfoRows.AddRange(isItemScanned
                                      ? ViewModelRowsWithEventsForItems(consignmentItem)
                                      : ViewModelRowsWithEventsForConsignment(consignment));
                InfoRows.AddRange(DetailsForSender(consignment));
                InfoRows.AddRange(DetailsForConsignee(consignment));
                if (!isItemScanned)
                {
                    InfoRows.Add(
                        new ViewModelRow
                               {
                                   Caption = GlobalTexts.Items,
                                   Value = GlobalTexts.ListOfItems + " " + ConsignmentItemCount(consignment),
                                   RoleOrder = RoleOrderForCons
                               }
                        );
                    InfoRows.AddRange(ViewModelRowsWithItemsForConsignment(consignment));
                }
            }


        }

        /// <summary>
        /// defines rows where details are available
        /// </summary>
        /// <param name="caption"></param>
        /// <returns>true if details are available</returns>
        internal static bool HasDetail(string caption)
        {

            if (caption == GlobalTexts.Cons)
            {
                return !SynteticConsignment;
            }
            
            return caption == GlobalTexts.LEv
                || caption == GlobalTexts.Send
                || caption == GlobalTexts.Recp
                || caption == GlobalTexts.Items
                || caption == GlobalTexts.ListOfItems;
        }


        /// <summary>
        /// transforms view events from validatemessage to viewdata
        /// </summary>
        /// <param name="consignmentitem">current consignment item</param>
        /// <returns>collection of viewmodel rows</returns>
        private IEnumerable<ViewModelRow> ViewModelRowsWithEventsForItems(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            var result = new List<ViewModelRow>();
            var dOrder = 1;
            if (consignmentitem.Event == null) return result;
            foreach (var itemEvent in consignmentitem.Event)
            {
                result.Add(ModelViewEventRow(dOrder, itemEvent.EventTime.ToString(GlobalTexts.DateAndTimeFormat)));
                result.Add(ModelViewEventRow(dOrder + 1, SpaceInFront + ActionCodes.GetActionCodeDescription(itemEvent.EventCode)));
                var orgName = SettingDataProvider.Instance.GetLocationName(itemEvent.EventOrgUnitId) ?? "?";
                result.Add(ModelViewEventRow(dOrder + 2, SpaceInFront + itemEvent.EventOrgUnitId + " " + orgName));
                dOrder += 3;
                if (itemEvent.StopId != null)
                {
                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + itemEvent.StopId));
                    dOrder += 1;
                }
                if (itemEvent.TripId != null)
                {
                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + itemEvent.TripId));
                    dOrder += 1;
                }
                if (itemEvent.DamageType != null)
                {
                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + itemEvent.DamageType));
                    dOrder += 1;
                }
                if (itemEvent.ReasonCode != null)
                {
                    var reasons = _dbTablesDataCopy.ReasonTypes();
                    var reasontext = reasons.FirstOrDefault(reasonType => reasonType.Code == itemEvent.ReasonCode);
                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + itemEvent.ReasonCode + " " + reasontext));
                    dOrder += 1;
                }
                if (itemEvent.ActionCode != null)
                {
                    var actionTypes = _dbTablesDataCopy.ActionTypes();
                    var actionDescription = actionTypes.FirstOrDefault(actionType => actionType.Code == itemEvent.ActionCode);

                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + itemEvent.ActionCode + " " + actionDescription));
                    dOrder += 1;
                }
            }
            return result;
        }




        private IEnumerable<ViewModelRow> ViewModelRowsWithEventsForConsignment(ValidationResponseConsignment consignment)
        {
            var result = new List<ViewModelRow>();
            var dOrder = 1;
            if (consignment.Event == null) return result;

            foreach (var consignmentEvent in consignment.Event)
            {
                result.Add(ModelViewEventRow(dOrder, consignmentEvent.EventTime.ToString(GlobalTexts.DateAndTimeFormat)));
                result.Add(ModelViewEventRow(dOrder + 1, SpaceInFront + ActionCodes.GetActionCodeDescription(consignmentEvent.EventCode)));
                var orgName = SettingDataProvider.Instance.GetLocationName(consignmentEvent.EventOrgUnitId) ?? "?";
                result.Add(ModelViewEventRow(dOrder + 2, SpaceInFront + consignmentEvent.EventOrgUnitId + " " + orgName));
                dOrder += 3;
                if (consignmentEvent.StopId != null)
                {
                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + consignmentEvent.StopId));
                    dOrder += 1;
                }
                if (consignmentEvent.TripId != null)
                {
                    result.Add(ModelViewEventRow(dOrder, SpaceInFront + consignmentEvent.TripId));
                    dOrder += 1;
                }

            }


            return result;
        }

        private static ViewModelRow ModelViewEventRow(int dOrder, string eventInfo)
        {
            return new ViewModelRow
            {
                DetailName = GlobalTexts.LEv,
                DetailOrder = dOrder,
                RoleOrder = RoleOrderForLastEvent,
                Value = eventInfo
            };
        }

        private static IEnumerable<ViewModelRow> ViewModelRowsWithItemsForConsignment(ValidationResponseConsignment consignment)
        {
            var result = new List<ViewModelRow>();
            var dOrder = 1;
            if (consignment.ConsignmentItem == null) return result;
            result.AddRange(consignment.ConsignmentItem
                .Select(item => new ViewModelRow
                {
                    Caption = GlobalTexts.ListOfItems,
                    Value = item.ConsignmentItemNumber,
                    RoleOrder = RoleOrderForCons,
                    DetailName = GlobalTexts.Items,
                    DetailOrder = dOrder += 1
                }));
            return result;
        }

        private IEnumerable<ViewModelRow> DetailsForSender(ValidationResponseConsignment consignment)
        {
            var result = new List<ViewModelRow>();
            if (consignment.Consignor == null) return result;
            if (consignment.Consignor.Name1 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignor.Name1,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Send,
                    DetailOrder = 1
                });
            }
            if (consignment.Consignor.Name2 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignor.Name2,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Send,
                    DetailOrder = 2
                });
            }
            if (consignment.Consignor.Address1 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignor.Address1,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Send,
                    DetailOrder = 3
                });
            }
            if (consignment.Consignor.Address2 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignor.Address2,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Send,
                    DetailOrder = 4
                });
            }
            if (consignment.Consignor.PostalCode != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignor.PostalCode + " " + consignment.Consignor.PostalName,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Send,
                    DetailOrder = 5
                });
            }
            if (consignment.Consignor.CountryCode != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignor.CountryCode,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Send,
                    DetailOrder = 6
                });
            }
            return result;
        }
        private IEnumerable<ViewModelRow> DetailsForConsignee(ValidationResponseConsignment consignment)
        {
            var result = new List<ViewModelRow>();
            if (consignment.Consignee == null) return result;
            if (consignment.Consignee.Name1 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignee.Name1,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Recp,
                    DetailOrder = 1
                });
            }
            if (consignment.Consignee.Name2 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignee.Name2,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Recp,
                    DetailOrder = 2
                });
            }
            if (consignment.Consignee.Address1 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignee.Address1,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Recp,
                    DetailOrder = 3
                });
            }
            if (consignment.Consignee.Address2 != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignee.Address2,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Recp,
                    DetailOrder = 4
                });
            }
            if (consignment.Consignee.PostalCode != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignee.PostalCode + " " + consignment.Consignee.PostalName,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Recp,
                    DetailOrder = 5
                });
            }
            if (consignment.Consignee.CountryCode != null)
            {
                result.Add(new ViewModelRow
                {
                    Value = consignment.Consignee.CountryCode,
                    RoleOrder = RoleOrderForSend,
                    DetailName = GlobalTexts.Recp,
                    DetailOrder = 6
                });
            }
            return result;
        }


        //Blocked actor (sperret aktør)
        private static string PartySuspended(ValidationResponseConsignment consignment)
        {
            return consignment.PartySuspended ? GlobalTexts.Yes : GlobalTexts.No;
        }

        private static string ProductCategory(ValidationResponseConsignment consignment)
        {
            return consignment.ProductCategory ?? GlobalTexts.NotAvailable;
        }

        //Weight and volume information
        private static string WeightAndVolumeCons(ValidationResponseConsignment consignment)
        {
            var result = string.Empty;
            if (consignment.WeightSpecified) result += consignment.Weight + consignment.WeightUnit.ToLower();
            if (consignment.VolumeSpecified) result += " / " + consignment.Volume + consignment.VolumeUnit.ToLower();
            return result;
        }
        private static string WeightAndVolumeItem(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            var result = string.Empty;
            if (consignmentitem.WeightSpecified) result += consignmentitem.Weight + consignmentitem.WeightUnit.ToLower();
            return result;
        }


        private static string PlannedReturndate(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            return consignmentitem.PlannedReturnDate.ToShortDateString();
        }

        private static string PlannedDeliverydate(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            return !consignmentitem.PlannedDeliveryDateSpecified ?
                GlobalTexts.NotAvailable
                : consignmentitem.PlannedDeliveryDate.ToShortDateString();
        }

        private static string ShelfLocation(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            return consignmentitem.ShelfLocation ?? GlobalTexts.NotAvailable;
        }

        private static string Recipient(ValidationResponseConsignment consignment)
        {
            if (consignment.Consignee == null)
            {
                return string.Empty;
            }
            return consignment.Consignee.Name1 + " " + consignment.Consignee.PostalCode;
        }

        private static string Sender(ValidationResponseConsignment consignment)
        {
            if (consignment.Consignor == null)
            {
                return string.Empty;
            }
            return consignment.Consignor.Name1 ?? string.Empty + " " + consignment.Consignor.PostalCode;
        }

        private static string LastEventForItem(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            var result = string.Empty;
            if (consignmentitem.Event == null) return result;
            var eventItem = consignmentitem.Event.First();
            if (eventItem == null) return result;
            result = eventItem.EventTime.ToString(GlobalTexts.DateAndTimeFormat) + " " +
                     ActionCodes.GetActionCodeDescription(eventItem.EventCode);
            return result;
        }
        private static string LastEventForConsignment(ValidationResponseConsignment consignment)
        {
            var result = string.Empty;
            if (consignment.Event == null) return result;
            var eventConsignment = consignment.Event.First();
            if (eventConsignment == null) return result;

            result = eventConsignment.EventTime.ToString(GlobalTexts.DateAndTimeFormat) + " " +
                         ActionCodes.GetActionCodeDescription(eventConsignment.EventCode);
            return result;
        }
        private static string StopOrder(ValidationResponseConsignmentConsignmentItem consignmentitem)
        {
            return consignmentitem.StopOrder ? GlobalTexts.ItemIsStoppedReturnIt : GlobalTexts.No;
        }

        private static string ConsignmentItemCount(ValidationResponseConsignment consignment)
        {
            return consignment.ConsignmentItemCount.ToString(CultureInfo.InvariantCulture);
        }

        private static string ConsignmentNumber(T20200_ValidateGoodsFromFOTReply validateGoodsResponse)
        {
            return validateGoodsResponse.GetConsignmentNumber();
        }


        internal IEnumerable<ViewListRow> OrderedListForCurrentRole(string detailName)
        {
            var rowsToOrder = new List<ViewListRow>();
            if (ModelUser.UserProfile != null)
            {
                rowsToOrder.AddRange(
                    InfoRows.Select(viewModelRow => new ViewListRow
                {
                    FieldCaption = viewModelRow.Caption,
                    FieldValue = viewModelRow.Value,
                    Order = CurrentOrderValue(viewModelRow),
                    DetailName = viewModelRow.DetailName,
                    DetailOrder = viewModelRow.DetailOrder
                })
                );
            }
            if (detailName == null)
            {
                var sortAscendingQuery =
                    from rowsToOrderRow in rowsToOrder
                    where rowsToOrderRow.Order > 0 && rowsToOrderRow.DetailName == null
                    orderby rowsToOrderRow.Order
                    select new ViewListRow { FieldCaption = rowsToOrderRow.FieldCaption, FieldValue = rowsToOrderRow.FieldValue, Order = rowsToOrderRow.Order };
                return sortAscendingQuery;

            }
            else
            {
                var sortAscendingQuery =
                    from rowsToOrderRow in rowsToOrder
                    where rowsToOrderRow.Order > 0 && rowsToOrderRow.DetailName == detailName
                    orderby rowsToOrderRow.DetailOrder
                    select new ViewListRow { FieldCaption = rowsToOrderRow.FieldCaption, FieldValue = rowsToOrderRow.FieldValue, Order = rowsToOrderRow.DetailOrder };
                return sortAscendingQuery;
            }
        }

        private static int CurrentOrderValue(ViewModelRow infoRow)
        {
            switch (ModelUser.UserProfile.Role)
            {
                case Role.Driver1:
                    return infoRow.OrderDriver1;
                case Role.Driver2:
                    return infoRow.OrderDriver2;
                case Role.HubWorker1:
                    return infoRow.OrderHubWorker1;
                case Role.Customer:
                    return infoRow.OrderCustomer;
                case Role.Landpostbud:
                    return infoRow.OrderLandPostBud;
                case Role.PostOfficeAndStore:
                    return infoRow.OrderPkPib;
                case Role.HomeDeliveryHub:
                    return infoRow.OrderHdh;
                default:
                    throw new Exception(string.Format("Cannot find ordering info for role {0}", ModelUser.UserProfile.Role));
            }
        }



        internal class ViewListRow
        {
            internal string FieldCaption;
            internal string FieldValue;
            internal int Order;
            internal string DetailName;
            internal int DetailOrder;
        }

        internal List<ViewModelRow> InfoRows;

        internal class ViewModelRow
        {

            internal string Caption;// display in first column
            internal string Value;// display in second column
            internal int OrderHubWorker1;// display in row number for role
            internal int OrderDriver1;
            internal int OrderDriver2;
            internal int OrderHdh;
            internal int OrderPkPib;
            internal int OrderLandPostBud;
            internal int OrderCustomer;
            internal IList<int> RoleOrder
            {
                set
                {
                    if (value.Count != 9)
                    {
                        throw new Exception("Orderdata in InfoRowCreator is incorrectly initalized ");
                    }
                    OrderHubWorker1 = value[0];
                    OrderDriver1 = value[2];
                    OrderDriver2 = value[3];
                    OrderHdh = value[5];
                    OrderPkPib = value[6];
                    OrderLandPostBud = value[7];
                    OrderCustomer = value[8];
                }
            }

            internal string DetailName;
            internal int DetailOrder;

        }
    }

    /// <summary>
    /// This list is obtained 1.8.13 by the help of Arne Nilsen
    /// EventCodeUtil.GetProcessFromEventCode is not complete enough for this task
    /// Todo: move into shared and get the english translation
    /// </summary>
    internal class ActionCodes
    {
        private static readonly string[][] ActionCodesWithDescription =
            {
       new []{ "A", "INNHENTET"},
       new []{ "AK", "KANSELLERING AV PICK UP"},
       new []{ "A1", "TEMPERATUR REGISTRERING"},
       new []{ "A2", "Registr. av FP-type/TR-utstyr"},
       new []{ "A3", "Reg. av vekt og volum på OT"},
       new []{ "A4", "Endre produktgruppe"},
       new []{ "B", "ANKOMMET AVG UTVEKSLINGSKONTOR"},
       new []{ "C", "AVGANG AVG UTVEKSLINGSKONTOR"},
       new []{ "D", "ANKOMMET ANK UTVEKSLINGSKONTOR"},
       new []{ "E", "STOPPET FOR IMPORTFORTOLLING"},
       new []{ "EA", "TIL LAGER"},
       new []{ "EB", "TIL LAGER - MANUELL"},
       new []{ "EC", "TIL LAGER SØR"},
       new []{ "F", "FRIGJORT FRA IMPORTTOLLEN"},
       new []{ "FB", "UTSENDT TOLLFRI"},
       new []{ "FC", "UTSENDT TOLLFRI - MANUELL"},
       new []{ "FD", "UTSENDT UFORTOLLET - MANUELL"},
       new []{ "FE", "UTSENDT FORENKLET TOLLBEHANDL"},
       new []{ "FG", "AVGIFT BEREGNET; FAKTURA SENDT"},
       new []{ "FH", "FRIGJORT FRA LAGER"},
       new []{ "FK", "FORTOLLET AV KUNDE"},
       new []{ "FL", "UTSENDT UFORTOLLET"},
       new []{ "FM", "SENDT I RETUR"},
       new []{ "FO", "UTSENDT MANUELL"},
       new []{ "FS", "FORENKLET TOLLBEH. - EGENFORT."},
       new []{ "FT", "FORTOLLET"},
       new []{ "FU", "UTSENDT"},
       new []{ "G", "ANKOMSTREGISTRERT"},
       new []{ "G1", "MOTTATT KUNDENS LAGER"},
       new []{ "H", "FORSØKT UTLEVERT"},
       new []{ "I", "UTLEVERT"},
       new []{ "I1", "UTLEVERT RETUR"},
       new []{ "I2", "UTLEVERT HOS KUNDE"},
       new []{ "I3", "UTLEVERT FRA TERMINAL"},
       new []{ "J", "ANKOMMET TRANSITT-OE"},
       new []{ "K", "AVGANG TRANSITT-OE"},
       new []{ "KB", "KONTANTBETALT"},
       new []{ "KL", "KOLLI LOSSET FRA LASTBÆRER"},
       new []{ "KO", "KOLLI OPPLASTET I LB (PROP.)"},
       new []{ "K3", "KOLLI OPPLAST. DISTRIB.(PROP.)"},
       new []{ "L", "Kolli er klargj for overlev sj"},
       new []{ "LBA", "LB-AVVIK I PRODUKSJON"},
       new []{ "LBF", "FORSØKT UTLEVERT"},
       new []{ "LBG", "ANKOMSTREG. AV LASTBÆRER"},
       new []{ "LBK", "KOBLE TIL FYSISK"},
       new []{ "LBL", "LOSSET FRA LASTBÆRER"},
       new []{ "LBM", "LASTBÆRER INNHENTET/INNLEVERT"},
       new []{ "LBO", "OPPLASTET I LASTBÆRER"},
       new []{ "LBP", "LASTBÆRER PASSERT MÅLEPUNKT"},
       new []{ "LBS", "SAMMENSTILLT I LASTBÆRER"},
       new []{ "LBU", "UTLEVERT FRA LASTBÆRER"},
       new []{ "LBV", "LASTBÆRER OVERLEVERT SJÅFØR"},
       new []{ "LBX", "ANGRE OPPRETTET LASTBÆRER"},
       new []{ "LB2", "BÆRER OVERLEVERT ANNEN ENHET"},
       new []{ "LB3", "LASTING AV BÆRER I DISTRIB.BIL"},
       new []{ "LLB", "LOSS HELE BÆRER"},
       new []{ "LVI", "LASTBÆRER IKKE UTKJ@RT"},
       new []{ "LVL", "AVVIK I LINJETRAFIKK"},
       new []{ "L1", "Klargjort skal leveres transp."},
       new []{ "M", "MOTTATT I EKSPORT FORTOLLING"},
       new []{ "ME", "Manuell ekspedering"},
       new []{ "N", "FRIGJORT FRA EKSPORTTOLLEN"},
       new []{ "N1", "Info om at sending er tollpass"},
       new []{ "O", "RETURREGISTRERT"},
       new []{ "ODL", "ORDER_DELETE"},
       new []{ "OI", "ORDER_DELIVERED"},
       new []{ "OLB", "OPPRETTE LOGISK LASTBÆRER"},
       new []{ "OSD", "ORDER_STOP_DELIVERED"},
       new []{ "OSP", "ORDER_STOP_PICKUP"},
       new []{ "OU", "ORDER INVOICED"},
       new []{ "OV", "DEVIATION"},
       new []{ "O1", "RETURREGISTRERT RESENDT"},
       new []{ "P", "INNLEVERT POSTKONTOR"},
       new []{ "P1", "INNLEVERT POSTKONTOR"},
       new []{ "Q", "ANKOMMET POSTKONTOR"},
       new []{ "QD", "DUPLIKAT FRA LAGER"},
       new []{ "QI", "Inn på utkjøringslager"},
       new []{ "QL", "ANKOMMET LAGER"},
       new []{ "QS", "ANKOMMET LAGER - MANUELL"},
       new []{ "QU", "Ut av utkjøringslager"},
       new []{ "R", "INNLEVERINGSREGISTRERT"},
       new []{ "R1", "SYNTETISK INNLEVERING"},
       new []{ "R2", "ANKOMMET TERMINAL OG OVERFØRES"},
       new []{ "R3", "INNLEVERINGSREGISTRERT RESENDT"},
       new []{ "S", "SORTERT"},
       new []{ "SA", "IKKE KLART TIL FORTOLLING"},
       new []{ "SD", "DELKOLLI TIL FORTOLLING"},
       new []{ "SF", "DOK OVERLEV TOLLVES FOR KONTR"},
       new []{ "SI", "SENDINGSINFO OPPDATERT"},
       new []{ "SL", "LAGT TIL FORTOLLING"},
       new []{ "SM", "FLERKOLLI IKKE FORTOLLINGSKLAR"},
       new []{ "SO", "OPPDATERT SENDINGSINFO"},
       new []{ "ST", "TOLLFRI"},
       new []{ "T", "FORHÅNDSMELDT"},
       new []{ "TLB", "TERMINERE LOGISK LASTBÆRER"},
       new []{ "TRA", "ANKOMMET TIL STOPP"},
       new []{ "TRD", "AVREIST FRA STOPP"},
       new []{ "TRF", "FERDIGLASTET LASTBÆRER"},
       new []{ "T1", "FORHÅNDSMELDT AV POSTEN"},
       new []{ "T2", "EDI-MELD. KORRIGERT AV POSTEN"},
       new []{ "T3", "FHM FORTOLLING"},
       new []{ "U", "FAKTURAGRUNNLAG BEREGNET"},
       new []{ "U1", "FAKTURAGR. BEREGNET MANUELT"},
       new []{ "U2", "FAKTURAGR. BEREGNET I ETTERTID"},
       new []{ "V", "AVVIK I PRODUKSJONEN"},
       new []{ "VG", "Gjenstående gods"},
       new []{ "VI", "IKKE UTKJØRT"},
       new []{ "VL", "AVVIK I LINJETRAFIKK"},
       new []{ "W", "SAMMENSTILT"},
       new []{ "WO", "OPPLASTET"},
       new []{ "W1", "OPPLASTET, FØRSTE KOLLI"},
       new []{ "W2", "OPPLASTET, SISTE KOLLI"},
       new []{ "X", "VARSEL SENDT"},
       new []{ "XXX", "REJECT-SORTERT JØNKØPING"},
       new []{ "X1", "ADVISERT"},
       new []{ "X2", "FORSØKT ADVISERT"},
       new []{ "Y", "KOLLI IKKE MOTTATT AV POSTEN"},
       new []{ "YA", "Hjemkjøring bestilt"},
       new []{ "YB", "Dato for hjemkjøring endret"},
       new []{ "YC", "Hjemkjøring kansellert"},
       new []{ "Z", "HENDELSE REGISTRERT"},
       new []{ "1", "INNLEV. AV GODS PÅ TERMINAL"},
       new []{ "2", "OVERLEVERT ANNEN ENHET"},
       new []{ "2A", "OVERLEVERT UNDERLEVERANDØR"},
       new []{ "2B", "OVERLEVERT GLS RETUR"},
       new []{ "2C", "OVERLEVERT FEDEX"},
       new []{ "2D", "OVERLEVERT Bring Cargo"},
       new []{ "2E", "OVERLEVERT TNT"},
       new []{ "3", "OPPLASTET TIL DISTRIBUSJON"},
       new []{ "3A", "OPPLASTET"},
       new []{ "4", "LASTET AV KUNDE"},
       new []{ "5", "SKADET"},
       new []{ "6", "LOSSING AV HENTEBIL"},
       new []{ "6A", "LOSSING"},
       new []{ "7", "FORSØKT INNHENTET"}
            };

        public static string GetActionCodeDescription(string actionCode)
        {
            foreach (var s in ActionCodesWithDescription.Where(s => s[0] == actionCode))
            {
                return s[1];
            }
            return "Unknown action code " + actionCode;
        }


    }

    /// <summary>
    /// helper class serving table data from PB tables used by control scan
    /// </summary>
    internal class DbTablesDataCopy
    {
        private readonly string _langCode;
        private List<ActionType> _actionTypes;
        private List<ReasonType> _reasonTypes;
        public DbTablesDataCopy(string langCode)
        {
            _langCode = langCode;
        }

        public List<ActionType> ActionTypes()
        {
            return _actionTypes ?? (_actionTypes = SettingDataProvider.Instance.GetActionTypes(_langCode));
        }

        public List<ReasonType> ReasonTypes()
        {
            return _reasonTypes ?? (_reasonTypes = SettingDataProvider.Instance.GetReasonTypes(_langCode));
        }
    }

}

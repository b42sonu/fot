using System;
using Com.Bring.PMP.PreComFW.Goods.Flows.ControlScan;
using Com.Bring.PMP.PreComFW.Shared;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.ControlScan;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Goods.Flows.ControlScan
{
    public enum FlowStateControlScan
    {


        InitAction,
        InitFromOperationList,
        ShowFormScanBarcode,
        FormControlScan,
        ValidateBarcode,
        ValidateGoods,
        PopulateViewModel,
        ShowIllegalBarcodeMessage,
        SwichToConsignment,
        SwichToConsignmentItem,
        GoBack,


        ThisFlowComplete,
        BreakLoop
    }
    public class FlowResultControlScan : BaseFlowResult
    {
    }


}

public class FlowControlScan : BaseFlow
{
    private ActionCommandsControlScan _actionCommandsControlScan;
    private FlowResultControlScan _flowResult;
    private ControlScanFlowModel _flowData;
    private bool _soundRequired;
    private bool _clearScanningText;


    public override void BeginFlow(IFlowData flowData)
    {
        var startState = FlowStateControlScan.InitAction;
        if (flowData != null && flowData.HeaderText==GlobalTexts.OperationListControlScan)
        {
            _flowData = (ControlScanFlowModel)flowData;
            startState = FlowStateControlScan.InitFromOperationList;
        }
        else
        {
            _flowData = new ControlScanFlowModel();
        }
        _actionCommandsControlScan = new ActionCommandsControlScan { FlowData = _flowData };
        _flowResult = new FlowResultControlScan();
        ExecuteActionState(startState);
    }

    public override void ExecuteState(int state)
    {
        ExecuteActionState((FlowStateControlScan)state);
    }

    internal void ExecuteActionState(FlowStateControlScan state)
    {
        try
        {
            do
            {
                Logger.LogEvent(Severity.Debug, "#FlowControlScan;" + state);

                switch (state)
                {
                    case FlowStateControlScan.InitAction:
                        _actionCommandsControlScan.ActionInitAction();
                        state = FlowStateControlScan.ShowFormScanBarcode;
                        break;

                    case FlowStateControlScan.InitFromOperationList:
                        _actionCommandsControlScan.ActionInitFromOperationList();
                        state = FlowStateControlScan.ValidateGoods;
                        break;

                    case FlowStateControlScan.ShowFormScanBarcode:
                        _actionCommandsControlScan.ActionShowFormScanBarcode(HandleEventsForFormScanbarCode, _clearScanningText);
                        _clearScanningText = false;
                        state = FlowStateControlScan.BreakLoop;
                        break;

                    case FlowStateControlScan.FormControlScan:
                        _actionCommandsControlScan.ActionFormControlScan(EventHandlerForFormControlScan);
                        state = FlowStateControlScan.BreakLoop;
                        break;

                    case FlowStateControlScan.ValidateBarcode:
                        _soundRequired = true;
                        state = _actionCommandsControlScan.ValidateBarcode() ?
                            FlowStateControlScan.ValidateGoods : FlowStateControlScan.ShowIllegalBarcodeMessage;
                        break;

                    case FlowStateControlScan.ValidateGoods:
                        state = _actionCommandsControlScan.ActionValidateGoods() ?
                        FlowStateControlScan.PopulateViewModel
                       : FlowStateControlScan.ShowIllegalBarcodeMessage;
                        break;

                    case FlowStateControlScan.PopulateViewModel:
                        _actionCommandsControlScan.ActionPopulateViewModel(_soundRequired);
                        state = FlowStateControlScan.FormControlScan;
                        break;

                    case FlowStateControlScan.ShowIllegalBarcodeMessage:
                        _actionCommandsControlScan.ActionShowIllegalbarcodeMessage();
                        state = FlowStateControlScan.ShowFormScanBarcode;
                        break;

                    case FlowStateControlScan.SwichToConsignment:
                        _soundRequired = false;
                        state = _actionCommandsControlScan.ActionSwichToConsignment() ?
                            FlowStateControlScan.ValidateGoods : FlowStateControlScan.PopulateViewModel;
                        break;

                    case FlowStateControlScan.SwichToConsignmentItem:
                        _soundRequired = false;
                        state = _actionCommandsControlScan.ActionSwichToConsignmentItem() ?
                            FlowStateControlScan.ValidateGoods : FlowStateControlScan.PopulateViewModel;
                        break;

                    case FlowStateControlScan.GoBack:
                        state = _actionCommandsControlScan.ActionGoBack() ?
                            (_flowData.FromOperationList ? FlowStateControlScan.ThisFlowComplete : FlowStateControlScan.ShowFormScanBarcode)
                            : FlowStateControlScan.PopulateViewModel;
                        break;

                    case FlowStateControlScan.ThisFlowComplete:
                        EndFlow();
                        state = FlowStateControlScan.BreakLoop;
                        break;
                }

            } while (state != FlowStateControlScan.BreakLoop);
        }
        catch (Exception ex)
        {
            Logger.LogException(ex, "FlowControlScan.ExecuteActionState");
            _flowResult.State = FlowResultState.Error;
            EndFlow();
        }
    }


    private void EndFlow()
    {
        BaseModule.EndSubFlow(_flowResult);
    }

    public void EventHandlerForFormControlScan(int scanBarcodeViewEvents, object[] data)
    {

        try
        {
            switch (scanBarcodeViewEvents)
            {
                case FormControlScan.FormControlScanViewEvents.BarcodeScanned:
                    _flowData.SelectedCaption = string.Empty;
                    _flowData.FotScannerOutput = (FotScannerOutput)data[0];
                    _clearScanningText = true;//Mottak 406
                    ExecuteActionState(FlowStateControlScan.ValidateBarcode);
                    break;

                case FormControlScan.FormControlScanViewEvents.ManualInput:
                    ExecuteActionState(FlowStateControlScan.ShowFormScanBarcode);
                    break;

                case FormControlScan.FormControlScanViewEvents.Back:
                    ExecuteActionState(FlowStateControlScan.GoBack);
                    break;

                case FormControlScan.FormControlScanViewEvents.Details:
                    _flowData.SelectedCaption = (string)data[0];
                    _flowData.SelectedCellContent = (string)data[1];
                    if (_flowData.SelectedCaption == GlobalTexts.Cons)
                        ExecuteActionState(FlowStateControlScan.SwichToConsignment);
                    else if (_flowData.SelectedCaption == GlobalTexts.ListOfItems)
                    {
                        ExecuteActionState(FlowStateControlScan.SwichToConsignmentItem);
                    }
                    else if (_flowData.SelectedCaption == GlobalTexts.Items)
                    {
                        ExecuteActionState(FlowStateControlScan.PopulateViewModel);
                    }
                    else
                        ExecuteActionState(FlowStateControlScan.FormControlScan);
                    break;
            }
        }
        catch (Exception ex)
        {
            Logger.LogException(ex, "FlowControlScan.EventHandlerForFormControlScan");
        }
    }


    private void HandleEventsForFormScanbarCode(int flowEvents, object[] data)
    {
        try
        {
            switch (flowEvents)
            {
                case ScanBarcodeViewEvents.Ok:
                case ScanBarcodeViewEvents.BarcodeScanned:
                    _flowData.SelectedCaption = string.Empty;
                    _flowData.FotScannerOutput = (FotScannerOutput)data[0];

                    ExecuteActionState(FlowStateControlScan.ValidateBarcode);
                    break;

                default:
                    ExecuteActionState(FlowStateControlScan.ThisFlowComplete);
                    break;
            }
        }
        catch (Exception ex)
        {
            Logger.LogException(ex, "FlowControlScan.HandleEventsForFormScanbarCode");
        }
    }

}


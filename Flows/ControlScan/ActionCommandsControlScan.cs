using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.MeasurementCore;
using Symbol.Barcode;
using LoggingInformationType = Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading.LoggingInformationType;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ControlScan
{
    public class ActionCommandsControlScan
    {
        internal ControlScanFlowModel FlowData;
        internal FormControlScan.FormData Formdata;
        internal DbTablesDataCopy DbTablesDataCopy;
        private string _scantxt;

        internal void ActionInitAction()
        {
            Formdata = new FormControlScan.FormData { MessageHolder = new MessageHolder(), HeaderText = GlobalTexts.ControlScan };
            FlowData.ReplyHistory = new List<History>();
            DbTablesDataCopy = new DbTablesDataCopy(ModelUser.UserProfile.LanguageCode);
        }

        internal void ActionInitFromOperationList()
        {
            Formdata = new FormControlScan.FormData
                           {
                               MessageHolder = new MessageHolder(),
                               HeaderText = FlowData.HeaderText
                           };

            FlowData.ConsignmentEntity = new Consignment(FlowData.ConsignmentId);
            FlowData.FromOperationList = true;
            FlowData.ReplyHistory = new List<History>();
            FlowData.FotScannerOutput = new FotScannerOutput
                                            {
                                                Code = FlowData.ConsignmentId,
                                                Type = DecoderTypes.CODE128,
                                            };
            if (DbTablesDataCopy == null) DbTablesDataCopy =
                  new DbTablesDataCopy(ModelUser.UserProfile.LanguageCode);
        }



        //TODO :: Write Unit Test
        // Back
        // Detail
        // ManualInput
        // FormControlScan 
        internal void ActionFormControlScan(ViewEventDelegate eventHandlerForFormScanbarcode)
        {
            var viewcontrolscan = ViewCommands.ShowView<FormControlScan>(Formdata);
            viewcontrolscan.SetEventHandler(eventHandlerForFormScanbarcode);
            viewcontrolscan.OnUpdateView(Formdata);

            TransactionLogger.LogTiming<T20200_ValidateGoodsFromFOTReply>((FlowData.FotScannerOutput).Transaction, new Timing(),
                                                              TimingMeasurement.CalculateTimingCategoryValue(
                                                                  TimingComponent.Client, TimingType.Sending,
                                                                  TimingComponent.User), new HttpCommunication());

        }

        //TODO :: Write Unit Test
        // true:    IsConsignmentScanned
        // false:   ShowIllegalBarcodeMessage
        internal Boolean ValidateBarcode()
        {
            Formdata.MessageHolder = new MessageHolder();

            BarcodeData barcodeData;
            bool result = BaseActionCommands.ValidateBarcode(FlowData.FotScannerOutput, BarcodeType.ConsignmentItemOrDeliveryCode | BarcodeType.ConsignmentNumber, out barcodeData);
            if (result)
            {
                FlowData.ReplyHistory = new List<History>();
                FlowData.ConsignmentEntity = ActionCommandsScanBarcode.CreateConsignmentEntity(barcodeData);
            }
            return result;
        }




        //TODO :: Write Unit Test
        // VewControlScanForm
        internal void ActionShowIllegalbarcodeMessage()
        {
            SoundUtil.Instance.PlayScanErrorSound();//ADDED BY SYED TO FIX QA-92 ON 9/19/2013
            if (Formdata.MessageHolder.Text.Length == 0) 
                Formdata.MessageHolder.Update(GlobalTexts.InvalidNumber + ".\r\n" + _scantxt + ".", MessageState.Error);
        }

        //TODO :: Write Unit Test
        internal Boolean ActionIsConsignmentScanned()
        {
            return (FlowData.ConsignmentEntity is Consignment);
        }

        //TODO :: Write Unit Test
        // IsConsignmentScanned
        internal void ActionPopulateViewModel(bool soundRequired)
        {
            try
            {
                Formdata.MessageHolder.Clear();
                var reply = FlowData.ValidateGoodsFromFotReply;
                var isItemScanned = FlowData.ConsignmentEntity is ConsignmentItem;
                if (!isItemScanned) Formdata.FromList = false;
                Formdata.ControlScanViewModel = new ControlScanViewModel(reply, isItemScanned, DbTablesDataCopy);
                if (soundRequired)
                    SoundUtil.Instance.PlaySuccessSound();
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                Formdata.MessageHolder.Update(ex.Message, MessageState.Error);
            }
        }

        //TODO :: Write Unit Test
        // VewControlScanForm
        internal void ActionShowFormScanBarcode(ViewEventDelegate eventHandlerForScanBarcodeView, bool clearScanningText)
        {
            _scantxt = BaseActionCommands.ScanTextFromValidBarcodeTypes(BarcodeType.Shipment, "");

            var formData = new FormScanBarcodeData
            {
                MessageHolder = Formdata.MessageHolder,
                HeaderText = Formdata.HeaderText,
                ControlScanFlow = true,
                ScanText = _scantxt,
                forceClearScanningText = clearScanningText

            };
            var view = ViewCommands.ShowView<FormScanBarcode>(formData);
            view.SetEventHandler(eventHandlerForScanBarcodeView);
        }

        // false=PopulateViewModel
        // true=ShowFormScanBarcode
        internal Boolean ActionGoBack()
        {
            var consignmentHistoryElement = FlowData.ReplyHistory.FirstOrDefault(history => history.Entity is Consignment);
            if (consignmentHistoryElement != null)
            {
                if (Formdata.FromList && FlowData.SelectedCaption == string.Empty)
                {
                    Logger.LogEvent(Severity.Debug, "ActionGoBack;vis liste");
                    FlowData.ConsignmentEntity = consignmentHistoryElement.Entity;
                    FlowData.ValidateGoodsFromFotReply = consignmentHistoryElement.Reply;
                    FlowData.SelectedCaption = GlobalTexts.Items;
                    return false;
                }
                if (FlowData.SelectedCaption == GlobalTexts.Items)
                {
                    Logger.LogEvent(Severity.Debug, "ActionGoBack;tilbake fra liste");
                    FlowData.ConsignmentEntity = consignmentHistoryElement.Entity;
                    FlowData.ValidateGoodsFromFotReply = consignmentHistoryElement.Reply;
                    FlowData.SelectedCaption = string.Empty;
                    return false;
                }
            }

            if (FlowData.ConsignmentEntity == FlowData.ReplyHistory[0].Entity && FlowData.SelectedCaption == string.Empty) return true;

            if (!Formdata.FromList)
            {
                Logger.LogEvent(Severity.Debug, "ActionGoBack;start item");
                FlowData.ConsignmentEntity = FlowData.ReplyHistory[0].Entity;
                FlowData.ValidateGoodsFromFotReply = FlowData.ReplyHistory[0].Reply;
            }
            else
            {
                Logger.LogEvent(Severity.Debug, "ActionGoBack;item selected from list");
            }
            FlowData.SelectedCaption = string.Empty;

            return false;

        }

        internal bool ActionSwichToConsignment()
        {
            var consignmentId = FlowData.ValidateGoodsFromFotReply.GetConsignmentNumber();
            FlowData.ConsignmentEntity = new Consignment(consignmentId);
            Formdata.MessageHolder = new MessageHolder();
            foreach (var entry in FlowData.ReplyHistory.Where(entry => entry.Id == consignmentId))
            {
                FlowData.ValidateGoodsFromFotReply = entry.Reply;
                FlowData.ConsignmentEntity = entry.Entity;
                return false;
            }
            return true;
        }

        internal bool ActionSwichToConsignmentItem()
        {
            Formdata.MessageHolder = new MessageHolder();
            var itemno = FlowData.SelectedCellContent;
            foreach (var entry in FlowData.ReplyHistory.Where(entry => entry.Id == itemno))
            {
                FlowData.ValidateGoodsFromFotReply = entry.Reply;
                FlowData.ConsignmentEntity = entry.Entity;
                return false;
            }

            BarcodeData barcodeData;
            FlowData.FotScannerOutput.Code = itemno;
            FlowData.FotScannerOutput.Type = DecoderTypes.CODE128;

            var validationResult =
            BaseActionCommands.ValidateBarcode(FlowData.FotScannerOutput, BarcodeType.ConsignmentItemNumber, out barcodeData);
            Logger.LogEvent(Severity.Debug, "validationResult;" + validationResult);

            if (!validationResult)
            {
                Formdata.MessageHolder.Update(GlobalTexts.ValidationFailed, MessageState.Error);
            }
            else
            {
                try
                {
                    FlowData.ConsignmentEntity = ActionCommandsScanBarcode.CreateConsignmentEntity(barcodeData);
                }
                catch (Exception ex)
                {
                    Formdata.MessageHolder.Update(ex.Message, MessageState.Error);
                }
            }

            FlowData.SelectedCaption = string.Empty;
            return true;
        }



        internal Boolean ActionValidateGoods()
        {
            var entity = FlowData.ConsignmentEntity;

            var request = CreateValidateGoodsRequestForControlScan(FlowData.ConsignmentEntity, ModelUser.UserProfile);

            request.LoggingInformation.MessageId = FlowData.FotScannerOutput.Transaction.Id.ToString();

            try
            {
                {
                    var reply = GoodsEventHelper.ValidateGoodsFromFot(CommunicationClient.Instance, request);

                    if (reply == null)
                    {
                        Formdata.MessageHolder.Update(GlobalTexts.NoReplyFromServer, MessageState.Error);
                        SoundUtil.Instance.PlayScanErrorSound();

                        return false;
                    }

                    switch (reply.ResponseInformation.ReturnState)
                    {
                        case "00":
                            break;

                        case "03":
                            var notFoundMessage = string.Format(GlobalTexts.ItemNotFound,FlowData.ConsignmentEntity.EntityDisplayId);
                            if (FlowData.ConsignmentEntity is Consignment)
                                notFoundMessage = string.Format(GlobalTexts.ConsignmentNotFound, FlowData.ConsignmentEntity.EntityDisplayId);
                           
                            Formdata.MessageHolder.Update(notFoundMessage, MessageState.Error);
                            SoundUtil.Instance.PlayScanErrorSound();
                            return false;
                        
                        default:
                            Formdata.MessageHolder.Update(reply.ResponseInformation.ReturnText, MessageState.Error);
                            SoundUtil.Instance.PlayScanErrorSound();
                            return false;
                    }


                    FlowData.ValidateGoodsFromFotReply = reply;
                    FlowData.ReplyHistory.Add(new History { Id = entity.EntityId, Reply = reply, Entity = entity });
                }
            }
            catch (Exception ex)
            {
                Formdata.MessageHolder.Update(ex.Message, MessageState.Error);
                return false;
            }

            return true;
        }


        //TODO :: Write Unit Test
        private static T20200_ValidateGoodsFromFOTRequest CreateValidateGoodsRequestForControlScan(ConsignmentEntity consignmentEntity, UserProfile userProfile)
        {
            T20200_ValidateGoodsFromFOTRequest request = null;

            try
            {
                request = new T20200_ValidateGoodsFromFOTRequest
                {
                    GetEvents = EventCodeUtil.GetEventLevelFromProcess(),
                    GetVASPayment = false,
                    GetExtended = true,
                    ValidationLevel = ValidateGoodsRequestValidationLevel.Goods,
                    GetPackageType = true,
                    LoggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationType>("Control Scan", userProfile),
                    GetAllItems = true,
                };

                if (consignmentEntity is Consignment)
                {
                    request.ValidationType = ValidateGoodsRequestValidationType.Consignment;
                    request.ConsignmentNumber = consignmentEntity.ConsignmentIdForValidateGoods;
                }
                else
                {
                    request.ValidationType = ValidateGoodsRequestValidationType.ConsignmentItem;
                    request.ConsignmentItemNumber = consignmentEntity.CastToConsignmentItem().ItemId;
                    request.DeliveryCode = consignmentEntity.CastToConsignmentItem().DeliveryCode;
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsControlScan.CreateValidateGoodsRequestForControlScan");
            }
            return request;
        }
    }
}

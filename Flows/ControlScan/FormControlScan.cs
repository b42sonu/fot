﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using PreCom.MeasurementCore;
using Resco.Controls.CommonControls;
using Resco.Controls.OutlookControls;
using Resco.Controls.SmartGrid;
using Alignment = Resco.Controls.CommonControls.Alignment;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ControlScan
{
    class FormControlScan : BaseBarcodeScanForm
    {
        private readonly TransparentLabel _lblTitle;
        private readonly TransparentLabel _lblHeader;
        private FormData _formData;
        private readonly SmartGridControlScan _smartgrid;

        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonDetails = "ButtonDetails";
        private const string ButtonManualInput = "ButtonManualInput";


        /// <summary>
        /// This constructor creates all controls for the form
        /// No designer is used
        /// </summary>
        public FormControlScan()
        {
            var touchPanel = new TouchPanel
                                 {
                                     Location = new Point(0, 0),
                                     Size = new Size(480, 552),
                                     TouchScrollBounceMode = TouchScrollBounceMode.Vertical,
                                     TouchScrollMode = TouchScrollMode.Vertical,
                                 };
            Controls.Add(touchPanel);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;

            _lblTitle = new TransparentLabel
            {
                AutoSize = false,
                Size = new Size(480, 43),
                Location = new Point(0, 2),
                TextAlignment = Alignment.MiddleCenter,
                Text = GlobalTexts.ControlScan,
                Font = new Font("Arial", 9F, FontStyle.Bold),
            };
            touchPanel.Controls.Add(_lblTitle);

            _lblHeader = new TransparentLabel
            {
                Location = new Point(20, 45),
                Font = new Font("Arial", 8F, FontStyle.Bold),
            };
            touchPanel.Controls.Add(_lblHeader);

            _multiButtonControl = new MultiButtonControl();
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonClick, ButtonBack));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Details, ButtonClick, ButtonDetails));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.ManualInput, ButtonClick, ButtonManualInput));

            _multiButtonControl.GenerateButtons();

            _smartgrid = new SmartGridControlScan(touchPanel, _multiButtonControl);

            SetStandardControlProperties(_lblTitle, null, null, null, null, null, null);
            touchPanel.Controls.Add(_multiButtonControl);
        }


        /// <summary>
        /// displays the control scan summary data
        /// 30 ms
        /// </summary>
        /// <param name="iformData"></param>
        /// 
        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormData)iformData;
            _lblTitle.Text = _formData.HeaderText;
            if (_formData.ControlScanViewModel == null)
            {
                _smartgrid.SetInVisible();
            }
            else
            {
                _smartgrid.SetVisible();
                _lblHeader.Text = _formData.ControlScanViewModel.HeaderText;
                _multiButtonControl.SetButtonEnabledState(ButtonDetails, false);
                var caption = _smartgrid.SelectedCaption();
                var mainViewSelected = caption == string.Empty || caption == GlobalTexts.Cons || caption == GlobalTexts.ListOfItems;

                if (_formData.FromList && _formData.ControlScanViewModel.IsConsignment && caption != GlobalTexts.Consignment)
                {
                    caption = GlobalTexts.Items;
                    _formData.FromList = false;
                    mainViewSelected = false;
                }
                _smartgrid.DisplayValues(_formData.ControlScanViewModel, mainViewSelected ? null : caption, ViewEvent);
                _multiButtonControl.SetButtonEnabledState(ButtonManualInput, mainViewSelected && _formData.HeaderText == GlobalTexts.ControlScan);
            }
            _smartgrid.Selectfirstrow();
        }



        /// <summary>
        /// handles click on all buttons in this form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewEvent == null) return;
                var button = (ImageButton)sender;
                Logger.LogEvent(Severity.Debug, " ButtonClick;button.Text=" + button.Text + " " + _smartgrid.SelectedCaption());
                if (button.Text == GlobalTexts.Back)
                {
                    if (_smartgrid.SelectedCaption() == GlobalTexts.ListOfItems)
                    {
                        _formData.FromList = false;
                    }
                    _smartgrid.ClearSelection();
                    ViewEvent.Invoke(FormControlScanViewEvents.Back);
                }
                else if (button.Text == GlobalTexts.ManualInput)
                {
                    _smartgrid.ClearSelection();
                    ViewEvent.Invoke(FormControlScanViewEvents.ManualInput);
                }
                else if (button.Text == GlobalTexts.Details)
                {
                    if (_smartgrid.SelectedCaption() == GlobalTexts.ListOfItems) _formData.FromList = true;
                    ViewEvent.Invoke(FormControlScanViewEvents.Details, _smartgrid.SelectedCaption(), _smartgrid.SelectedCellContent());
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ButtonClick");
            }
        }

        /// <summary>
        /// mandatory method for scanner enabled forms
        /// </summary>
        /// <param name="scannerOutput"></param>
        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {

            scannerOutput.Transaction = new Transaction(scannerOutput.Transaction.Id, (byte)TransactionCategory.ValidateGoodsFromFot, scannerOutput.Code);
            TransactionLogger.LogTiming<T20200_ValidateGoodsFromFOTReply>(scannerOutput.Transaction, new Timing(),
                                                                          TimingMeasurement.CalculateTimingCategoryValue(
                                                                              TimingComponent.Client, TimingType.Received,
                                                                              TimingComponent.User), new HttpCommunication());


            _smartgrid.ClearSelection();
            ViewEvent.Invoke(FormControlScanViewEvents.BarcodeScanned, scannerOutput);
        }

        public class FormControlScanViewEvents : ViewEvent
        {
            public const int Back = 1;
            public const int Details = 4;
            public const int ManualInput = 5;
            public const int BarcodeScanned = 8;
        }

        public class SmartGridControlScan
        {
            private readonly SmartGrid _smartGridControlScan;
            private readonly MultiButtonControl _multiButtonControl;
            private ViewEventDelegate _viewEvent;
            public SmartGridControlScan(TouchPanel touchPanel, MultiButtonControl multiButtonControl)
            {
                _multiButtonControl = multiButtonControl;
                _smartGridControlScan = new SmartGrid
                {
                    BorderSize = ScaleUtil.GetScaledPosition(2),
                    ColumnHeaderHeight = ScaleUtil.GetScaledPosition(34),
                    Location = new Point(21, 80),
                    ScrollHeight = ScaleUtil.GetScaledPosition(24),
                    ScrollWidth = ScaleUtil.GetScaledPosition(24),
                    Size = new Size(438, 415),
                    TouchScrolling = true,
                    RowHeight = ScaleUtil.GetScaledPosition(32),
                    ColumnHeadersVisible = false,
                    KeyNavigation = true,
                    FullRowSelect = true,

                };
                var columnPropertyLegend = new Column
                {
                    CellEdit = CellEditType.TextBox,
                    MinimumWidth = ScaleUtil.GetScaledPosition(60),
                    Width = ScaleUtil.GetScaledPosition(60),
                };

                _smartGridControlScan.Columns.Add(columnPropertyLegend);

                var columnPropertyValue = new Column
                {
                    CellEdit = CellEditType.TextBox,
                    HeaderText = "Value",
                    MinimumWidth = ScaleUtil.GetScaledPosition(60),
                    Width = ScaleUtil.GetScaledPosition(360),
                };
                _smartGridControlScan.Columns.Add(columnPropertyValue);
                _smartGridControlScan.KeyPress += GridKeyUp;



                touchPanel.Controls.Add(_smartGridControlScan);
                _smartGridControlScan.CellClick += SmartGridControlScanCellClick;
                _smartGridControlScan.SelectionChanged += SmartGridSelectionChanged;
            }

            private void SmartGridSelectionChanged(object sender, EventArgs e)
            {
                OnSelectedRowChanged();
            }

            private void GridKeyUp(object sender, KeyPressEventArgs e)
            {
                try
                {
                    switch ((int)e.KeyChar)
                    {
                        case 13:
                            if (ControlScanViewModel.HasDetail(SelectedCaption()))
                            {
                                _viewEvent.Invoke(FormControlScanViewEvents.Details, SelectedCaption(), SelectedCellContent());
                            }
                            break;
                        case 8:
                            _viewEvent.Invoke(FormControlScanViewEvents.Back, SelectedCaption(), SelectedCellContent());
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogEvent(Severity.Error, "GridKeyUp Error;" + ex.Message);
                }

            }

            private void SmartGridControlScanCellClick(object sender, CellEventArgs e)
            {
                OnSelectedRowChanged();
            }

            private void OnSelectedRowChanged()
            {
                try
                {
                    var buttonHasDetails = ControlScanViewModel.HasDetail(SelectedCaption());
                    _multiButtonControl.SetButtonEnabledState(ButtonDetails, buttonHasDetails);
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "Error in OnSelectedRowChanged");
                }

            }


            internal void FormatForDetail(string caption)
            {
                _smartGridControlScan.Columns[0].Width = 0;
                _smartGridControlScan.Columns[1].Width = ScaleUtil.GetScaledPosition(438);
                _smartGridControlScan.ColumnHeadersVisible = true;
                _smartGridControlScan.Columns[1].HeaderText = caption;
            }
            internal void FormatForSummary()
            {
                _smartGridControlScan.Columns[0].Width = ScaleUtil.GetScaledPosition(70);
                _smartGridControlScan.Columns[1].Width = ScaleUtil.GetScaledPosition(364);
                _smartGridControlScan.ColumnHeadersVisible = false;
            }
            internal string SelectedCaption()
            {
                if (_smartGridControlScan.Rows.Count == 0) return string.Empty;
                var i = _smartGridControlScan.ActiveRowIndex;
                return i >= 0 ? _smartGridControlScan.Cells[i, 0].Text : string.Empty;
            }
            internal void ClearSelection()
            {
                _smartGridControlScan.ActiveRowIndex = -1;
            }

            internal void Selectfirstrow()
            {
                if (_smartGridControlScan.Rows.Count <= 0) return;
                _smartGridControlScan.ActiveRowIndex = -1;
                _smartGridControlScan.ActivateCell(0, 0);
                _smartGridControlScan.EditSelectedCell();
                _smartGridControlScan.EndEdit();
            }

            internal string SelectedCellContent()
            {
                var i = _smartGridControlScan.ActiveRowIndex;
                return i >= 0 ? _smartGridControlScan.Cells[i, 1].Text : string.Empty;
            }

            public void SetVisible()
            {
                _smartGridControlScan.Visible = true;
            }

            public void SetInVisible()
            {
                _smartGridControlScan.Visible = false;
            }

            internal void DisplayValues(ControlScanViewModel controlScanViewModel, string detailCaption, ViewEventDelegate viewEvent)
            {
                _smartGridControlScan.SuspendLayout();
                _smartGridControlScan.Visible = false;
                _smartGridControlScan.Rows.Clear();
                //_smartGridControlScan.UpdateLayout();

                _viewEvent = viewEvent;


                foreach (var row in controlScanViewModel.OrderedListForCurrentRole(detailCaption)
                    .Select(viewListRow => new Row
                       {
                           StringData = new[]
                            {
                                viewListRow.FieldCaption, 
                                viewListRow.FieldValue
                            },

                           Height = ScaleUtil.GetScaledPosition(35)
                       }))

                    _smartGridControlScan.Rows.Add(row);
                if (detailCaption != null)
                {
                    FormatForDetail(detailCaption);
                }
                else
                {
                    FormatForSummary();
                }
                _smartGridControlScan.Visible = true;
                _smartGridControlScan.ResumeLayout();
                _smartGridControlScan.Refresh();
            }
        }

        public class FormData : BaseFormData
        {
            public MessageHolder MessageHolder { get; set; }
            public ControlScanViewModel ControlScanViewModel;
            internal bool FromList;
        }

    }
}


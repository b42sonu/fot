﻿using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{
    public class ActionCommandsCauseAndMeasure
    {

        /// <summary>
        /// method for get the reason and action code combination from local db on the baiss of 
        /// role and language ,event code(passed inside calling method on the basis of process)
        /// </summary>
        public List<CauseMeasure> GetReasonActionCombinations()
        {
            var causeMeasureCombinations =
                SettingDataProvider.Instance.GetDistinctCombinationForReasonActionTypes(
                    ModelUser.UserProfile.RoleName, ModelUser.UserProfile.LanguageCode);
            return causeMeasureCombinations;
        }

        /// <summary>
        /// method for get all reason from combination of reasons and actions..
        /// </summary>
        /// <param name="reasonActionCombinations"></param>
        /// <returns></returns>
        public List<CauseMeasure> GetReasons(List<CauseMeasure> reasonActionCombinations)
        {
            List<CauseMeasure> reasons = null;
            if (reasonActionCombinations != null && reasonActionCombinations.Any())
                reasons = reasonActionCombinations.GroupBy(n => n != null ? (n.CauseCode) : null).Select(o => o.First()).ToList();
            return reasons;
        }
        /// <summary>
        /// method used for get actions for selected reason ..
        /// </summary>
        public List<CauseMeasure> GetActionsForReasons(string reasonCode, List<CauseMeasure> reasonActionCombinations)
        {
            List<CauseMeasure> listCauseMeasure = null;
            if (reasonActionCombinations != null && reasonActionCombinations.Any())
                listCauseMeasure = (from n in reasonActionCombinations where n.CauseCode == reasonCode select n).ToList<CauseMeasure>();
            return listCauseMeasure;
        }
        /// <summary>
        /// this method used for this flow for decide which will be starting screen on the basis of 
        /// flowdata that passed by calling flow..
        /// </summary>
        /// <param name="selectedReasonAndActionCombination"></param>
        /// <param name="reasons"></param>
        /// <returns></returns>
        internal FlowStatesCauseAndMeasure GetDecideToShowView(CauseMeasure selectedReasonAndActionCombination, List<CauseMeasure> reasons)
        {
            var state = FlowStatesCauseAndMeasure.ViewSelectReason;

            if (selectedReasonAndActionCombination != null)
            {
                if (string.IsNullOrEmpty(selectedReasonAndActionCombination.CauseCode) == false && string.IsNullOrEmpty(selectedReasonAndActionCombination.MeasureCode) == false)
                {
                    state = FlowStatesCauseAndMeasure.ViewShowReasonAndAction;
                }
                else if (string.IsNullOrEmpty(selectedReasonAndActionCombination.CauseCode) == false && string.IsNullOrEmpty(selectedReasonAndActionCombination.MeasureCode) && reasons != null)
                {
                    var list = reasons.Where(n => n.CauseCode == selectedReasonAndActionCombination.CauseCode).ToList();
                    if (list.Any())
                    {
                        var cause = list[0];
                        selectedReasonAndActionCombination.CauseDescription = cause.CauseDescription;
                        selectedReasonAndActionCombination.CauseCodeAndCauseDescription = cause.CauseCodeAndCauseDescription;
                    }

                    state = FlowStatesCauseAndMeasure.ActionGetActionsForReason;
                }
            }

            return state;
        }

    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormShowActionAndReason.
    /// </summary>
    public class FormDataShowReasonAndAction : BaseFormData
    {
        public CauseMeasure SelectedReasonAndAction { get; set; }
        public string FreeText { get; set; }
        public string Header { get; set; }
        public bool IsShowFreeText = true;
        public bool ShowPlacement { get; set; }
        public string Placement { get; set; }
    }

    public partial class FormShowActionAndReason : BaseBarcodeScanForm
    {
        private FormDataShowReasonAndAction _formData;
        readonly MultiButtonControl _tabButtons = new MultiButtonControl();
        #region Methods and Functions

        /// <summary>
        /// 
        /// </summary>
        public FormShowActionAndReason()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetStandardControlProperties(labelModuleName, lblOrgnaisationUnitName, null, null, null, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            if (ModelUser.UserProfile.OrgUnitName != null)
            lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblReason.Text = GlobalTexts.Reason ;
            lblReasonValue.Text = GlobalTexts.SelectReasonValue;
            lblAction.Text = GlobalTexts.Action ;
            lblActionValue.Text = GlobalTexts.SelectedActionValue;
            lblFreeText.Text = GlobalTexts.FreeText + Colon;
            lblPlacement.Text = GlobalTexts.Placement + Colon;
           _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
           _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnShow(IFormData formData)
        {
            if (formData != null)
            {
                txtFreeText.Text = string.Empty;
                txtPlacement.Text = string.Empty;
                _formData = (FormDataShowReasonAndAction)formData;
                txtFreeText.Text = _formData.FreeText;
                txtPlacement.Text = _formData.Placement;
                labelModuleName.Text = _formData.HeaderText;
                if (!_formData.IsShowFreeText)
                {lblFreeText.Visible = false;
                    txtFreeText.Visible = false;
                }
                else
                {
                    lblFreeText.Visible = true;
                    txtFreeText.Visible = true; 
                }
                if (_formData.SelectedReasonAndAction != null)
                {
                    lblReasonValue.Text = _formData.SelectedReasonAndAction.CauseCodeAndCauseDescription;
                    lblActionValue.Text = _formData.SelectedReasonAndAction.MeasureCodeAndMeasureDescription;
                    
                }

                if(_formData.ShowPlacement)
                {
                    base.OnShow(formData);
                    lblPlacement.Visible = true;
                    txtPlacement.Visible = true;
                    txtFreeText.Size = new System.Drawing.Size(438, 96);
                }
                else
                {
                    lblPlacement.Visible = false;
                    txtPlacement.Visible = false;
                    txtFreeText.Size = new System.Drawing.Size(438, 170);
                }
            }
            SetKeyboardState(KeyboardState.AlphaLowerCase);
            var onScreenKeyBoardPropertiesFreeText = new OnScreenKeyboardProperties(GlobalTexts.FreeText, KeyPadTypes.Qwerty);
            txtFreeText.Tag = onScreenKeyBoardPropertiesFreeText;
            var onScreenKeyBoardPropertiesPlacement = new OnScreenKeyboardProperties(GlobalTexts.Placement, KeyPadTypes.Qwerty);
            txtPlacement.Tag = onScreenKeyBoardPropertiesPlacement;

        }


        
        #endregion

        public void ClearText()
        {
            lblReasonValue.Text = string.Empty;
            lblActionValue.Text = string.Empty;
            txtFreeText.Text = string.Empty;
            txtPlacement.Text = string.Empty;
        }

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ClearText();
                ViewEvent.Invoke(ShowReasonAndActionEvent.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormShowActionAndReason.ButtonBackClick");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ShowReasonAndActionEvent.Ok, txtFreeText.Text, txtPlacement.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormShowActionAndReason.ButtonOkClick");
            }
        }

        #endregion


        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtPlacement.Text = string.Empty;
            ViewEvent.Invoke(ShowReasonAndActionEvent.BarcodeScanned, scannerOutput,txtFreeText.Text);
        }
    }

    /// <summary>
    /// This class is having all constants for each event raised by events of FormShowActionAndReason.
    /// </summary>
    public class ShowReasonAndActionEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Ok = 1;
        public const int BarcodeScanned = 2;

        public static implicit operator ShowReasonAndActionEvent(int viewEvent)
        {
            return new ShowReasonAndActionEvent { Value = viewEvent };
        }
    }
}
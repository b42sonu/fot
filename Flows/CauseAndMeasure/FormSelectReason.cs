﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormSelectReason.
    /// </summary>
    public class FormDataSelectReason : BaseFormData
    {
        public string Header { get; set; }
        public List<CauseMeasure> Reasons { get; set; }
        public CauseMeasure SelectedReason { get; set; }
        public bool IsNewActionAndReason { get; set; }
    }
    public partial class FormSelectReason : BaseForm
    {
        private FormDataSelectReason _formData;
        private List<CauseMeasure> _reasons;
        private CauseMeasure _selectedReason;
        readonly MultiButtonControl _tabButtons = new MultiButtonControl();
        private const string ButtonOk = "buttonOk";
        private bool _selectItem;
        #region Methods and Functions

        public FormSelectReason()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetStandardControlProperties(labelModuleName,lblOrgnaisationUnitName,null,null,null,null,null);
            InitOnScreenKeyBoardProperties();
        }

        public void SelectFirstElement()
        {
            if(listReasons.Items.Count > 0)
                listReasons.SelectedIndex = 0;
            SetReasonValue(_selectedReason, (CauseMeasure)listReasons.SelectedItem);
            if (_selectedReason != null && _selectedReason.CauseCodeAndCauseDescription != null)
                txtReason.Text = _selectedReason.CauseCodeAndCauseDescription;
            txtReason.SelectAll();
            EnableDisableButtons();
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            if (ModelUser.UserProfile.OrgUnitName != null)
                lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblReason.Text = GlobalTexts.Reason + ":";
            lblHeading.Text = GlobalTexts.SelectReasonInstruction;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitOnScreenKeyBoardProperties()
        {
            var onScreenKeyBoardProperties = new OnScreenKeyboardProperties(GlobalTexts.Reason , KeyPadTypes.Qwerty);
            txtReason.Tag = onScreenKeyBoardProperties;
            onScreenKeyBoardProperties.TextChanged = TxtReasonTextChanged;
        }

        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnShow(IFormData formData)
        {
            Visible = true;
            Show(BaseModule.MainForm);
            if (formData != null)
            {
                _formData = (FormDataSelectReason)formData;
                _reasons = _formData.Reasons;
                labelModuleName.Text = _formData.HeaderText;
                _selectedReason = _formData.SelectedReason;

            }
            if (_selectedReason == null || _selectedReason.CauseCode == null)
            {
                BindReasons(_reasons);
                txtReason.Text = string.Empty;
            }
            EnableDisableButtons();
           // SetKeyboardState(KeyboardState.AlphaLowerCase);
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindReasons(List<CauseMeasure> reasons)
        {
            _selectItem = true;
            if (reasons != null)
            {
                listReasons.DisplayMember = "CauseCodeAndCauseDescription";
                listReasons.ValueMember = "CauseCode";
                listReasons.DataSource = reasons;
            }
            else
            listReasons.SelectedIndex = -1;
            EnableDisableButtons();
            txtReason.Select(txtReason.Text.Length, 0);
            _selectItem = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void EnableDisableButtons()
        {
            _tabButtons.SetButtonEnabledState(ButtonOk, (listReasons.Items.Count != 0) && listReasons.SelectedIndex >= 0);
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetReason()
        {
            txtReason.Text = _selectedReason.CauseCodeAndCauseDescription;
            ViewEvent.Invoke(SelectReasonEvent.Ok, _selectedReason);
        }

        #endregion


        #region Events

       /* public override void ShowingView()
        {
            base.ShowingView();
            listReasons.Focus();
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtReasonKeyUp(object sender, KeyEventArgs e)
        {
            try
            {

                if (_reasons == null) return;
                if (!string.IsNullOrEmpty(txtReason.Text))
                {
                    var filteredReasons = _reasons.Where(c => c != null && (c.CauseCodeAndCauseDescription != null && c.CauseCodeAndCauseDescription.ToLower().StartsWith(txtReason.Text.ToLower()))).ToList();

                    filteredReasons.AddRange((_reasons.Where(
                        c =>
                        c != null && (c.CauseCodeAndCauseDescription != null && ((c.CauseCodeAndCauseDescription.ToLower().Contains(txtReason.Text.ToLower()) &&
                                                                                  !c.CauseCodeAndCauseDescription.ToLower().StartsWith(txtReason.Text.ToLower())))))).ToList());

                    BindReasons(filteredReasons);
                }
                else
                    BindReasons(_reasons);
                SetReasonValue(_selectedReason, (CauseMeasure)listReasons.SelectedItem);
                EnableDisableButtons();

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectReason.TxtReasonKeyUp");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFinishedTyping"></param>
        /// <param name="textentered"></param>
        private void TxtReasonTextChanged(bool userFinishedTyping, string textentered)
        {
            if (!userFinishedTyping) return;
            if (_reasons != null)
            {
                var filteredReasons =
                    (from c in _reasons
                     where c.CauseCodeAndCauseDescription.ToLower().StartsWith(txtReason.Text.ToLower())
                     select c).ToList();
                filteredReasons.AddRange((from c in _reasons
                                          where
                                              c.CauseCodeAndCauseDescription.ToLower()
                                               .Contains(txtReason.Text.ToLower()) &&
                                              !c.CauseCodeAndCauseDescription.ToLower()
                                                .StartsWith(txtReason.Text.ToLower())
                                          select c).ToList());

                BindReasons(filteredReasons);
                SetReasonValue(_selectedReason, (CauseMeasure)listReasons.SelectedItem);
                EnableDisableButtons();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListReasonsSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (!_selectItem)
                {
                    SetReasonValue(_selectedReason, (CauseMeasure)listReasons.SelectedItem);
                    txtReason.Text = _selectedReason.CauseCodeAndCauseDescription;
                    txtReason.SelectAll();
                    EnableDisableButtons();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectReason.ListReasonsSelectedIndexChanged");
            }
        }

        private void SetReasonValue(CauseMeasure causeMeasure, CauseMeasure selectedCauseMeasure)
        {
            //only for attempted delivery for retaining the previous selection
            if (BaseModule.CurrentFlow != null && (BaseModule.CurrentFlow.CurrentProcess==Process.AttemptedDelivery &&  selectedCauseMeasure != null && (causeMeasure != null && causeMeasure.CauseCode != selectedCauseMeasure.CauseCode)))
            {
                causeMeasure.MeasureCode = null;
                causeMeasure.MeasureCodeAndMeasureDescription = null;
                causeMeasure.MeasureDescription = null;
            }


            if (selectedCauseMeasure != null && causeMeasure != null)
            {
                causeMeasure.CauseCode = selectedCauseMeasure.CauseCode;
                causeMeasure.CauseCodeAndCauseDescription = selectedCauseMeasure.CauseCodeAndCauseDescription;
                causeMeasure.CauseDescription = selectedCauseMeasure.CauseDescription;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(SelectReasonEvent.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectReason.ButtonBackClick");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if (_selectedReason == null) return;
                switch (BaseModule.CurrentFlow.CurrentProcess)
                {
                    case Process.GeneralDeviations:

                        if (_selectedReason.CauseCode == "24")
                        {
                            var result = GuiCommon.ShowModalDialog(GlobalTexts.Warning,
                                                                   GlobalTexts.Reson24Warning,
                                                                   Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);

                            //If no delete local file
                            if (result == GlobalTexts.No) 
                                return;
                            SetReason();
                        }
                        else
                        {
                            SetReason();
                        }
                        break;

                    default:
                        SetReason();
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectReason.ButtonOkClick");
            }
        }

        #endregion
    }

    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectReason.
    /// </summary>
    public class SelectReasonEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Ok = 1;


        public static implicit operator SelectReasonEvent(int viewEvent)
        {
            return new SelectReasonEvent { Value = viewEvent };
        }
    }
}
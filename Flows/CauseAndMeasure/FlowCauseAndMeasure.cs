﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using Symbol.Barcode;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{
    public enum FlowStatesCauseAndMeasure
    {
        ActionDecideToShowView,
        ActionDisplayErrorMessage,
        ActionGetReasonActionCombinations,
        ActionGetReasons,
        ActionGetActionsForReason,
        ViewCommands,
        ViewSelectReason,
        ViewSelectAction,
        ViewShowReasonAndAction,
        FlowScanItemOrCarrierId,
        ThisFlowComplete
    }

    public class FlowResultCauseAndMeasure : BaseFlowResult
    {
        public CauseMeasure SelectedReasonAndActionCombination;
        public string Comment;
        public string Placement;
    }

    public class FlowDataCauseAndMeasure : BaseFlowData
    {
        public CauseMeasure SelectedReasonAndActionCombination;
        public string Comment;
        public string Placement;
        public bool IsCallFromVasMatrixSubprocess;
    }

    public class FlowCauseAndMeasure : BaseFlow
    {
        private List<CauseMeasure> _reasonActionCombinations;
        private List<CauseMeasure> _reasons;
        private List<CauseMeasure> _actions;
        private readonly FlowResultCauseAndMeasure _flowResult;
        private FlowDataCauseAndMeasure _flowData;
        private readonly ActionCommandsCauseAndMeasure _actionCommandsGeneralDeviation;
        private MessageHolder _messageHolder;



        public FlowCauseAndMeasure()
        {
            _messageHolder = new MessageHolder();
            _flowData = new FlowDataCauseAndMeasure();
            _flowResult = new FlowResultCauseAndMeasure();
            _actionCommandsGeneralDeviation = new ActionCommandsCauseAndMeasure();
        }


        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowGeneralDeviations.BeginFlow");
            _flowData = (FlowDataCauseAndMeasure)flowData;
            if (_flowData.SelectedReasonAndActionCombination == null)
                _flowData.SelectedReasonAndActionCombination = new CauseMeasure();

            ExecuteState(FlowStatesCauseAndMeasure.ActionGetReasonActionCombinations);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesCauseAndMeasure)state);
        }

        public void ExecuteState(FlowStatesCauseAndMeasure state)
        {
            if (state > FlowStatesCauseAndMeasure.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesCauseAndMeasure state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowCauseAndMeasure;" + state);

                switch (state)
                {
                    case FlowStatesCauseAndMeasure.ViewSelectReason:
                        ViewCommandsCauseAndMeasure.ShowViewSelectReason(SelectReasonViewEventHandler, _reasons, _flowData.SelectedReasonAndActionCombination, _flowData.HeaderText);
                        break;
                    case FlowStatesCauseAndMeasure.ViewSelectAction:
                        ViewCommandsCauseAndMeasure.ShowViewSelectAction(SelectActionViewEventHandler, _actions, _flowData.SelectedReasonAndActionCombination, _flowData.HeaderText);
                        break;
                    case FlowStatesCauseAndMeasure.ViewShowReasonAndAction:
                        ViewCommandsCauseAndMeasure.ShowViewShowReasonAndAction(ShowActionAndReasonViewEventHandler, _flowData.SelectedReasonAndActionCombination, _flowData.Comment, _flowData.HeaderText, _flowData.Placement);
                        break;


                    case FlowStatesCauseAndMeasure.ThisFlowComplete:
                        if (_flowResult.State == FlowResultState.Ok && _flowData != null)
                        {
                            _flowResult.SelectedReasonAndActionCombination =
                                _flowData.SelectedReasonAndActionCombination;
                            _flowResult.Comment = _flowData.Comment;
                            _flowResult.Placement = _flowData.Placement;
                        }
                        EndFlow();
                        break;
                }

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowGeneralDeviations.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesCauseAndMeasure.ThisFlowComplete);
            }
        }



        private void ExecuteActionState(FlowStatesCauseAndMeasure state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowCauseAndMeasure;" + state);

                do
                {
                    switch (state)
                    {

                        case FlowStatesCauseAndMeasure.ActionGetReasonActionCombinations:
                            _reasonActionCombinations = _actionCommandsGeneralDeviation.GetReasonActionCombinations();
                            state = FlowStatesCauseAndMeasure.ActionGetReasons;
                            break;
                        case FlowStatesCauseAndMeasure.ActionGetReasons:
                            _reasons = _actionCommandsGeneralDeviation.GetReasons(_reasonActionCombinations);
                            state = FlowStatesCauseAndMeasure.ActionDecideToShowView;
                            break;

                        case FlowStatesCauseAndMeasure.ActionDecideToShowView:
                            state = _flowData != null && CurrentProcess != Process.AttemptedDelivery ? _actionCommandsGeneralDeviation.GetDecideToShowView(_flowData.SelectedReasonAndActionCombination, _reasons) : FlowStatesCauseAndMeasure.ViewSelectReason;
                            break;

                        case FlowStatesCauseAndMeasure.ActionGetActionsForReason:
                            if (_flowData != null)
                                _actions =
                                    _actionCommandsGeneralDeviation.GetActionsForReasons(
                                        _flowData.SelectedReasonAndActionCombination.CauseCode, _reasonActionCombinations);
                            state = FlowStatesCauseAndMeasure.ViewSelectAction;
                            break;


                    }
                } while (state < FlowStatesCauseAndMeasure.ViewCommands);

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowCauseAndMeasure.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesCauseAndMeasure.ThisFlowComplete);
            }
            ExecuteViewState(state);
        }


        private void SelectReasonViewEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case SelectReasonEvent.Cancel:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStatesCauseAndMeasure.ThisFlowComplete);
                    break;
                case SelectReasonEvent.Ok:
                    _flowData.SelectedReasonAndActionCombination = (CauseMeasure)data[0];
                    ExecuteState(FlowStatesCauseAndMeasure.ActionGetActionsForReason);
                    break;

            }
        }

        private void ShowActionAndReasonViewEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case ShowReasonAndActionEvent.Cancel:
                    _flowData.Comment = string.Empty;
                    _flowData.Placement = string.Empty;
                    ExecuteState(_actions == null
                                     ? FlowStatesCauseAndMeasure.ActionGetActionsForReason
                                     : FlowStatesCauseAndMeasure.ViewSelectAction);
                    break;

                case ShowReasonAndActionEvent.Ok:
                    _flowResult.State = FlowResultState.Ok;
                    _flowData.Comment = (string)data[0];
                    _flowData.Placement = (string)data[1];
                    ExecuteState(FlowStatesCauseAndMeasure.ThisFlowComplete);
                    break;

                case ShowReasonAndActionEvent.BarcodeScanned:
                    SoundUtil.Instance.PlayScanSound();
                    var scannerOutput = (FotScannerOutput)data[0];
                    _flowData.Comment = (string)data[1];
                    if (scannerOutput.Type == DecoderTypes.PDF417)
                    {
                        if (scannerOutput.Code.Substring(0, 4) == "AE01")
                        {
                            SoundUtil.Instance.PlaySuccessSound();
                            _flowData.Placement = scannerOutput.Code.Substring(24, 3);
                            ExecuteViewState(FlowStatesCauseAndMeasure.ThisFlowComplete);
                        }
                        else
                        {
                            GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.WrongBarCodeType, Severity.Error, GlobalTexts.Ok);
                            ExecuteViewState(FlowStatesCauseAndMeasure.ViewShowReasonAndAction);
                        }
                    }
                    else
                    {
                        SoundUtil.Instance.PlaySuccessSound();
                        _flowData.Placement = scannerOutput.Code;
                        ExecuteViewState(FlowStatesCauseAndMeasure.ThisFlowComplete);
                    }

                    break;

            }
        }


        private void SelectActionViewEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case SelectActionEvent.Cancel:

                    if (_flowData.IsCallFromVasMatrixSubprocess)
                    {
                        _flowResult.State = FlowResultState.Cancel;
                        ExecuteState(FlowStatesCauseAndMeasure.ThisFlowComplete);
                    }
                    else
                    {
                        _flowData.SelectedReasonAndActionCombination.MeasureCode = null;
                        _flowData.SelectedReasonAndActionCombination.MeasureCodeAndMeasureDescription = null;
                        _flowData.SelectedReasonAndActionCombination.MeasureDescription = null;
                        ExecuteState(FlowStatesCauseAndMeasure.ViewSelectReason);
                    }

                    break;

                case SelectActionEvent.Ok:
                    _messageHolder.Clear();
                    _flowData.SelectedReasonAndActionCombination = (CauseMeasure)data[0];
                    ExecuteState(FlowStatesCauseAndMeasure.ViewShowReasonAndAction);
                    break;
            }
        }

        /// <summary>
        /// This methos ends the Attempted Delivery flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowCauseAndMeasure");
            BaseModule.EndSubFlow(_flowResult);
        }


    }
}

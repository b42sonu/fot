﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{
    public class ViewCommandsCauseAndMeasure
    {

        public static void ShowViewSelectReason(ViewEventDelegate viewEventHandler, List<CauseMeasure> reasons, CauseMeasure selectedReason, string header)
        {
            try
            {
                var formData = new FormDataSelectReason() { Reasons = reasons, HeaderText = header, SelectedReason = selectedReason };
                var view = ViewCommands.ShowView<FormSelectReason>(formData);
                view.SetEventHandler(viewEventHandler);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsGeneralDeviation.ShowViewSelectReason()");
            }
        }

        public static void ShowViewSelectAction(ViewEventDelegate viewEventHandler, List<CauseMeasure> actions, CauseMeasure selectedAction, string header)
        {
            try
            {
                var formData = new FormDataSelectAction { Actions = actions, SelectedAction = selectedAction, HeaderText = header };
                var view = ViewCommands.ShowView<FormSelectAction>(formData);
                view.SetEventHandler(viewEventHandler);
                view.UpdateView(formData);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsGeneralDeviation.ShowViewSelectAction()");
            }
        }

        public static void ShowViewShowReasonAndAction(ViewEventDelegate viewEventHandler, CauseMeasure selectedReasonAndAction, string freeText, string header, string placement)
        {
            try
            {
                bool showPlacement = BaseModule.CurrentFlow.CurrentProcess == Process.RemainingGoodsAtHub;
                var formData = new FormDataShowReasonAndAction() { SelectedReasonAndAction = selectedReasonAndAction, FreeText = freeText, HeaderText = header, ShowPlacement = showPlacement,Placement = placement};
                var view = ViewCommands.ShowView<FormShowActionAndReason>(formData);
                view.SetEventHandler(viewEventHandler);


            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsGeneralDeviation.ShowViewShowReasonAndAction()");
            }
        }


    }

}


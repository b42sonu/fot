﻿using System;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{
    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormSelectAction.
    /// </summary>
    public class FormDataSelectAction : BaseFormData
    {
        public List<CauseMeasure> Actions { get; set; }
        public CauseMeasure SelectedAction { get; set; }
        public string Header { get; set; }
    }

    public partial class FormSelectAction : BaseForm
    {
        private FormDataSelectAction _formData;
        private CauseMeasure _selectedAction;
        private List<CauseMeasure> _actions;
        readonly MultiButtonControl _tabButtons = new MultiButtonControl();
        private const string ButtonOk = "buttonOk";
        private bool _selectItem;

        #region Methods and Functions

        public FormSelectAction()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
            SetStandardControlProperties(labelModuleName,lblOrgnaisationUnitName, null, null, null, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            if (ModelUser.UserProfile.OrgUnitName != null)
                lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblReason.Text = GlobalTexts.Reason;
            lblAction.Text = GlobalTexts.Action+Colon;
            lblHeading.Text = GlobalTexts.SelectActionInstruction;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm){Name=ButtonOk});
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        public void SelectFirstElement()
        {
            if (listActions.Items.Count > 0)
                listActions.SelectedIndex = 0;

            if (listActions.SelectedItem != null)
            {
                SetCauseValue(_selectedAction, (CauseMeasure)listActions.SelectedItem);
                txtAction.Text = _selectedAction.MeasureCodeAndMeasureDescription;
                txtAction.SelectAll();
            }
            EnableDisableButtons();
        }
      
        /// <summary>
        /// 
        /// </summary>
        private void InitOnScreenKeyBoardProperties()
        {
            var onScreenKeyBoardProperties = new OnScreenKeyboardProperties(GlobalTexts.Action, KeyPadTypes.Qwerty);
            txtAction.Tag = onScreenKeyBoardProperties;
            onScreenKeyBoardProperties.TextChanged = TextActionTextChanged;
        }


        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnShow(IFormData formData)
        {
            Visible = true;
            Show(BaseModule.MainForm);
            if (formData != null)
            {
                _formData = (FormDataSelectAction)formData;
                _actions = _formData.Actions;
                labelModuleName.Text = _formData.HeaderText;
                _selectedAction = _formData.SelectedAction;
                if (_formData.SelectedAction != null)
                {
                    lblReasonValue.Text = _formData.SelectedAction.CauseCodeAndCauseDescription;
                }
            }
            if (_selectedAction == null || _selectedAction.MeasureCode == null)
            {
                BindActions(_actions);
                txtAction.Text = string.Empty;
            }
            EnableDisableButtons();
            //SetKeyboardState(KeyboardState.AlphaLowerCase);
        }


        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataSelectAction)iformData;
            if (listActions.Items.Count != 1 || _formData.SelectedAction.MeasureCode != null)return;
            listActions.SelectedIndex = 0;
            SetCauseValue(_formData.SelectedAction, (CauseMeasure)listActions.SelectedItem);
            ViewEvent.Invoke(SelectReasonEvent.Ok, _formData.SelectedAction);
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindActions(List<CauseMeasure> actions)
        {
            _selectItem = true;
            if (actions != null)
            {
                actions = actions.Distinct().ToList();
                listActions.DisplayMember = "MeasureCodeAndMeasureDescription";
                listActions.ValueMember = "MeasureCode";
                listActions.DataSource = actions;
            }
            listActions.SelectedIndex = -1;
            EnableDisableButtons();
            txtAction.Select(txtAction.Text.Length, 0);
            _selectItem = false;
        }

        /// <summary>
        /// 
        /// </summary>
        private void EnableDisableButtons()
        {
            _tabButtons.SetButtonEnabledState(ButtonOk, (listActions.Items.Count != 0) && listActions.SelectedIndex >= 0);
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFinishedTyping"></param>
        /// <param name="textentered"></param>
        private void TextActionTextChanged(bool userFinishedTyping, string textentered)
        {
            if (!userFinishedTyping) return;
            var filteredActions =
                (from c in _actions
                 where c.MeasureCodeAndMeasureDescription.ToLower().StartsWith(txtAction.Text.ToLower())
                 select c).ToList();
            filteredActions.AddRange(
                        (from c in _actions
                         where c.MeasureCodeAndMeasureDescription.ToLower().Contains(txtAction.Text.ToLower()) && !c.MeasureCodeAndMeasureDescription.ToLower().StartsWith(txtAction.Text.ToLower())
                         select c).ToList());
            BindActions(filteredActions);
            SetCauseValue(_selectedAction, (CauseMeasure)listActions.SelectedItem);
            EnableDisableButtons();
        }

       /* public override void ShowingView()
        {
            base.ShowingView();
            listActions.Focus();
        }*/
        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtActionKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (_actions == null) return;
                if (!string.IsNullOrEmpty(txtAction.Text))
                {
                        var filteredActions =
                            (from c in _actions
                             where c.MeasureCodeAndMeasureDescription.ToLower().StartsWith(txtAction.Text.ToLower())
                             select c).ToList();
                        filteredActions.AddRange(
                            (from c in _actions
                             where c.MeasureCodeAndMeasureDescription.ToLower().Contains(txtAction.Text.ToLower()) && !c.MeasureCodeAndMeasureDescription.ToLower().StartsWith(txtAction.Text.ToLower())
                             select c).ToList());

                        BindActions(filteredActions);
                }
                else
                    BindActions(_actions);
                SetCauseValue(_selectedAction, (CauseMeasure)listActions.SelectedItem);
                EnableDisableButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex,"FormSelectAction.TxtActionKeyUp");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListReasonsSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!_selectItem)
                {
                    SetCauseValue(_selectedAction, (CauseMeasure)listActions.SelectedItem);
                    txtAction.Text = _selectedAction.MeasureCodeAndMeasureDescription;
                    txtAction.SelectAll();
                    EnableDisableButtons();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectAction.ListReasonsSelectedIndexChanged");
            }
           
        }

        private void SetCauseValue(CauseMeasure causeMeasure, CauseMeasure selectedCauseMeasure)
        {
            if (selectedCauseMeasure != null && causeMeasure != null)
            {
                causeMeasure.MeasureCode = selectedCauseMeasure.MeasureCode;
                causeMeasure.MeasureCodeAndMeasureDescription = selectedCauseMeasure.MeasureCodeAndMeasureDescription;
                causeMeasure.MeasureDescription = selectedCauseMeasure.MeasureDescription;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(SelectReasonEvent.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectAction.ButtonBackClick");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if (_selectedAction != null)
                {
                    txtAction.Text = _selectedAction.MeasureCodeAndMeasureDescription;
                    ViewEvent.Invoke(SelectReasonEvent.Ok, _selectedAction);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectAction.ButtonOkClick");
            }
        }

        #endregion
    }

    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectAction.
    /// </summary>
    public class SelectActionEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Ok = 1;


        public static implicit operator SelectActionEvent(int viewEvent)
        {
            return new SelectActionEvent { Value = viewEvent };
        }
    }
}
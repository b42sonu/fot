﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using PreCom.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure
{

    partial class FormShowActionAndReason
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageControlBox = new MessageControl();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReason = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReasonValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblActionValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblFreeText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPlacement = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtFreeText = new System.Windows.Forms.TextBox();
            this.txtPlacement = new System.Windows.Forms.TextBox();
            

            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblActionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFreeText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPlacement)).BeginInit();

            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.lblReason);
            this.touchPanel.Controls.Add(this.lblReasonValue);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblActionValue);
            this.touchPanel.Controls.Add(this.lblFreeText);
            this.touchPanel.Controls.Add(this.lblPlacement);
            this.touchPanel.Controls.Add(this.txtFreeText);
            this.touchPanel.Controls.Add(this.txtPlacement);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.lblAction);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.Controls.Add(this.messageControlBox);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // messageControlBox
            // 
            this.messageControlBox.Location = new System.Drawing.Point(6, 350);
            this.messageControlBox.Name = "messageControlBox";
            this.messageControlBox.Size = new System.Drawing.Size(468, 123);
            this.messageControlBox.TabIndex = 1;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // lblReason
            // 
            this.lblReason.Location = new System.Drawing.Point(21, 71);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(157, 30);
            this.lblReason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            // lblReason
            //
            this.lblReasonValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblReasonValue.Location = new System.Drawing.Point(21, 105);
            this.lblReasonValue.Name = "lblReasonValue";
            this.lblReasonValue.Size = new System.Drawing.Size(438, 60);
            this.lblReasonValue.AutoSize = false;
            // lblAction
            // 
            this.lblAction.Location =new System.Drawing.Point(21, 160);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(157, 30);
            this.lblAction.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);

            // lblActionValue
            // 
            this.lblActionValue.Location = new System.Drawing.Point(21, 194);
            this.lblActionValue.Name = "lblActionValue";
            this.lblActionValue.Size = new System.Drawing.Size(438, 60);
            this.lblActionValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblActionValue.AutoSize = false;
            // lblFreeText
            // 
            this.lblFreeText.Location = new System.Drawing.Point(21, 259);
            this.lblFreeText.Name = "lblFreeText";
            this.lblFreeText.Size = new System.Drawing.Size(157, 30);
            this.lblFreeText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblFreeText.Visible = false;

            // lblPlacement
            // 
            this.lblPlacement.Location = new System.Drawing.Point(21, 390);
            this.lblPlacement.Name = "lblPlacement";
            this.lblPlacement.Size = new System.Drawing.Size(157, 30);
            this.lblPlacement.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblPlacement.Visible = false;
            this.lblPlacement.Text = "Placement";
            // 
            // txtFreeText
            // 
            this.txtFreeText.Location = new System.Drawing.Point(21, 293);
            this.txtFreeText.Multiline = true;
            this.txtFreeText.Name = "txtEarlierEvents";
            this.txtFreeText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFreeText.Size = new System.Drawing.Size(438, 170);
            this.txtFreeText.TabIndex = 1;
            this.txtFreeText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtFreeText.BackColor = System.Drawing.Color.White;
            // 
            // txtPlacement
            // 
            this.txtPlacement.Location = new System.Drawing.Point(21, 423);
            this.txtPlacement.Multiline = false;
            this.txtPlacement.Name = "txtPlacement";
            this.txtPlacement.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtPlacement.Size = new System.Drawing.Size(438, 40);
            this.txtPlacement.MaxLength = 40;
            this.txtPlacement.TabIndex = 1;
            this.txtPlacement.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // FormShowReasonAndAction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormShowActionAndReason";//FormShowActionAndReason
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblActionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFreeText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPlacement)).EndInit();
            
            this.ResumeLayout(false);
        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblReason;
        private Resco.Controls.CommonControls.TransparentLabel lblReasonValue;
        private Resco.Controls.CommonControls.TransparentLabel lblAction;
        private Resco.Controls.CommonControls.TransparentLabel lblActionValue;
        private Resco.Controls.CommonControls.TransparentLabel lblFreeText;
        private Resco.Controls.CommonControls.TransparentLabel lblPlacement;
        private System.Windows.Forms.TextBox txtFreeText;
        private System.Windows.Forms.TextBox txtPlacement;
        private MessageControl messageControlBox;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
    }
}
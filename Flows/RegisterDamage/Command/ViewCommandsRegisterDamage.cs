﻿using System;
using System.Text;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.Command
{
    public class ViewCommandsRegisterDamage
    {
        public void FormDamageInformation(ViewEventDelegate viewEventHandler, Damage damage, ConsignmentEntity currentConsignmentEntity, bool fromCorrectionMenu,bool isprevious)
        {

            var damageInformationData = GetDamageData();
            damageInformationData.MessageHolder = new MessageHolder();
            damageInformationData.Damage = damage;
            damageInformationData.CurrentConsignmentEntity = currentConsignmentEntity;
            damageInformationData.FromCorrectionMenu = fromCorrectionMenu;
            damageInformationData.IsPrevious = isprevious;
            IsDamageInfoValid(damageInformationData);
            var view = ViewCommands.ShowView<FormDamageType>(damageInformationData);
            view.SetEventHandler(viewEventHandler);

        }
        public void FormSelectCause(ViewEventDelegate viewEventHandler, Damage damage, string headerEarlierEvents, string earlierEvents, bool isprevious)
        {
            var damageInformationData = GetDamageData();
            damageInformationData.Damage = damage;
            damageInformationData.HeaderEarlierEvents = headerEarlierEvents;
            damageInformationData.EarlierEvents = earlierEvents;
            damageInformationData.IsPrevious = isprevious;
            var view = ViewCommands.ShowView<FormCause>(damageInformationData);
            view.SetEventHandler(viewEventHandler);
            view.UpdateView(damageInformationData);

        }
        public void FormSelectMeasure(ViewEventDelegate viewEventHandler, Damage damage, string headerEarlierEvents, string earlierEvents, bool isprevious)
        {
            var damageInformationData = GetDamageData();
            damageInformationData.Damage = damage;
            damageInformationData.HeaderEarlierEvents = headerEarlierEvents;
            damageInformationData.EarlierEvents = earlierEvents;
            damageInformationData.IsPrevious = isprevious;
            var view = ViewCommands.ShowView<FormMeasure>(damageInformationData);
            view.SetEventHandler(viewEventHandler);
            view.UpdateView(damageInformationData);

        }
        public void FormEnterComments(ViewEventDelegate viewEventHandler, Damage damage, string headerEarlierEvents, string earlierEvents)
        {
            var damageInformationData = GetDamageData();
            damageInformationData.Damage = damage;
            damageInformationData.HeaderEarlierEvents = headerEarlierEvents;
            damageInformationData.EarlierEvents = earlierEvents;
            var view = ViewCommands.ShowView<FormDamageComments>(damageInformationData);
            view.SetEventHandler(viewEventHandler);

        }

        private FormDataDamageType GetDamageData()
        {
            var formDataDamageType = new FormDataDamageType
            {
                CauseMeasureList = SettingDataProvider.Instance.GetDistinctCombinationForReasonActionTypes(
                  ModelUser.UserProfile.RoleName, ModelUser.UserProfile.LanguageCode),
                DamageCodeList = SettingDataProvider.Instance.GetDamageCodes(ModelUser.UserProfile.LanguageCode)
            };
            return formDataDamageType;
        }
        private void IsDamageInfoValid(FormDataDamageType formDataDamageInformation)
        {
            var errorMessage = new StringBuilder("");
            if (formDataDamageInformation.CauseMeasureList == null || formDataDamageInformation.CauseMeasureList.Count == 0)
                errorMessage.Append(GlobalTexts.NoCauseMeasureSynced + "\n");
            if (formDataDamageInformation.DamageCodeList == null || formDataDamageInformation.DamageCodeList.Count == 0)
                errorMessage.Append(GlobalTexts.NoConsignmentCodesSynced + "\n");

            Boolean isDamageInfoValid = String.IsNullOrEmpty(errorMessage.ToString());
            if (isDamageInfoValid) return;
            Logger.LogEvent(Severity.Debug, "Damage information not synced");
            formDataDamageInformation.MessageHolder.Update(errorMessage.ToString(), MessageState.ModalMessage);
        }

        public FormDamageScan ShowFormDamageScan(ViewEventDelegate viewEventHandler, ConsignmentEntity consignmentEntity, Damage damage, bool fromCorrectionMenu, bool hasDamages, string headerEarlierEvents, string earlierEvents, int scannedEntityCount,MessageHolder messageHolder, out FormDataDamageScan formDataDamageScan)
        {
            var formdata = new FormDataDamageScan
            {
                ConsignmentEntity = consignmentEntity,
                Damage = damage,
                FromCorrectionMenu = fromCorrectionMenu,
                HasDamages = hasDamages,
                HeaderEarlierEvents = headerEarlierEvents,
                EarlierEvents = earlierEvents,
                ScannedEntityCount = scannedEntityCount,
                MessageHolder = messageHolder,
                ScanText = BaseActionCommands.ScanTextFromValidBarcodeTypes(BarcodeType.Shipment, "")

            };
            formDataDamageScan = formdata;
            var view = ViewCommands.ShowView<FormDamageScan>(formdata);
            view.SetEventHandler(viewEventHandler);
            return view;
        }



        public void FormEventVerification(ViewEventDelegate viewEventHandler, ConsignmentEntity consignmentEntity)
        {
            var view = ViewCommands.ShowView<FormEventVerification>();
            view.AutoShow = false;
            view.SetEventHandler(viewEventHandler);
            view.OnUpdateView(new FormEventVerification.FormDataEventVerification { ConsignmentEntity = consignmentEntity });
        }
    }
}

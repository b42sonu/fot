﻿using System;
using System.Text;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.Camera;
using Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount;
using Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.Command
{
    public class ActionCommandsRegisterDamage : BaseActionCommands
    {
        private readonly MessageHolder _messageHolder;
        private static ICommunicationClient _communicationClient;

        public ActionCommandsRegisterDamage(ICommunicationClient comClient, MessageHolder messageHolder)
        {
            _messageHolder = messageHolder;
            _communicationClient = comClient;
        }

        public bool ValidateDamage(Damage enteredDamage)
        {
            var errorMessage = new StringBuilder("");
            if (enteredDamage == null || enteredDamage.DamageCode == null)
                errorMessage.Append(GlobalTexts.DamageCodeNotSet + "\n");
            if (enteredDamage == null || enteredDamage.CauseMeasure == null)
                errorMessage.Append(GlobalTexts.CauseNotSet + "\n");

            bool damageInfoIsValid = String.IsNullOrEmpty(errorMessage.ToString());
            if (!damageInfoIsValid)
            {
                _messageHolder.Update(errorMessage.ToString(), MessageState.ModalMessage);
            }

            return damageInfoIsValid;
        }

        public bool StoreConsignmentItemCount(Damage damage, FlowResultConfirmConsignmentItemCount subflowResult)
        {
            bool enteredItemCountSuccess = subflowResult.State == FlowResultState.Ok;

            if (enteredItemCountSuccess)
            {
                damage.ConsignmentItemCount = subflowResult.ConsignmentItemsCount;
            }
            else
            {
                _messageHolder.Update(subflowResult.Message, MessageState.Error);
            }
            return enteredItemCountSuccess;
        }
        //TODO :: Write Unit Test
        public void StoreDamageAndItemInLocalCache(ConsignmentEntity consignmentEntity, EntityMap entityMap, Damage currentDamage)
        {
            //consginmentEntity.Damage = currentDamage.Clone();
            StoreDamageAndItemInLocalCache(consignmentEntity, entityMap);

            //if (currentDamage != null) currentDamage.ValidScan = true;
        }

        public void StoreDamageAndItemInLocalCache(ConsignmentEntity consignmentEntity, EntityMap entityMap)
        {
            consignmentEntity.EntityStatus |= EntityStatus.Scanned;
            entityMap.Store(consignmentEntity);

            consignmentEntity.Damage.ValidScan = true;

            if (consignmentEntity is ConsignmentItem)
                _messageHolder.Update(string.Format(GlobalTexts.ConsignmentItemRegistered, consignmentEntity.EntityDisplayId), MessageState.Information);
            else
                _messageHolder.Update(string.Format(GlobalTexts.ConsignmentRegistered, consignmentEntity.EntityDisplayId), MessageState.Information);

            if (consignmentEntity.IsScannedOnline == false)
            {
                _messageHolder.Update(GlobalTexts.Offline + ": " + _messageHolder.Text, MessageState.Warning);
            }
        }

        public void ConfirmDamage(Damage damage)
        {
            damage.Confirmed = true;
        }

        public bool HasVerifiedUnsignedEvents(EntityMap entityMap)
        {
            foreach (var consignmentEntity in entityMap.GetConsignmentsWithoutEntities())
            {
                if (!consignmentEntity.Damage.HasSignature() && consignmentEntity.HasStatus(EntityStatus.Scanned))
                    return true;
            }
            return false;
        }

        public bool HasMoreDamageToConfirm(EntityMap entityMap, out ConsignmentEntity damageEntity)
        {
            foreach (var consignmentEntity in entityMap.GetConsignmentsWithoutEntities())
            {
                if (consignmentEntity.Damage.Confirmed == false && consignmentEntity.Damage.CauseMeasure != null)
                {
                    damageEntity = consignmentEntity;
                    return true;
                }
            }
            damageEntity = null;
            return false;
        }

        public bool IsScannedBefore(EntityMap entityMap, ref ConsignmentEntity damageEntity)
        {
            if (entityMap.GetScannedBefore(ref damageEntity))
            {
                _messageHolder.Update(GlobalTexts.ItemAlreadyDamage, MessageState.Warning);
                return true;
            }

            return false;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Check if current user is allowed to register consignments
        /// </summary>
        /// <returns></returns>
        public bool IsConsignmentRegistrationAllowed(ConsignmentEntity currentConsignmentEntity)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsRegisterDamage.CheckConsignmentRegistrationAllowed()");
            var result = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsConsignmentRegistrationAllowed);

            if (result == false)
            {
                _messageHolder.Update(GlobalTexts.ScanningConsignmentNotAuthorized, MessageState.Error);
                //Placed sound here, as it is the special case. Before removing it talk to Rakesh Aggarwal
                SoundUtil.Instance.PlayScanErrorSound();
            }
            return result;
        }

        public bool BackFromFlowReconcilliation(FlowResultReconciliation subflowResult, ref EntityMap entityMap, ref ConsignmentEntity consignmentEntity)
        {
            bool result = subflowResult != null && subflowResult.State == FlowResultState.Ok;

            if (subflowResult != null && entityMap != null)//subflowResult.EntityMap != null)
            {
                //entityMap = subflowResult.EntityMap;
                //Consignment of scanned consignment items can also register deviation I 1914 
                bool deviationForConsignment = consignmentEntity is Consignment && entityMap.ContainsKey(consignmentEntity.EntityId);
                bool existsAsScannedInEntityMap = entityMap.GetScannedBefore(ref consignmentEntity) == false;
                if (consignmentEntity != null && existsAsScannedInEntityMap && deviationForConsignment == false)
                {
                    consignmentEntity = null;
                }
            }
            return result;
        }

        public bool BackFromSignatureForSingelEvent(ConsignmentEntity damageEntity, FlowResultSignature subflowResult)
        {
            bool success = subflowResult.State == FlowResultState.Ok;

            if (success)
                damageEntity.Damage.Signature = subflowResult.Signature;
            else
            {
                _messageHolder.Update(subflowResult.Message, MessageState.Error);
            }

            return success;
        }

        //Set signature for all items
        public bool BackFromSignatureForReconciliationList(FlowResultSignature subflowResult, EntityMap entityMap)
        {
            bool success = subflowResult.State == FlowResultState.Ok;

            if (success)
            {
                foreach (var consignmentEntity in entityMap.GetConsignmentsWithoutEntities())
                {
                    consignmentEntity.Damage.Signature = subflowResult.Signature;
                }
            }
            else
            {
                _messageHolder.Update(subflowResult.Message, MessageState.Error);
            }
            return success;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Handles the error state from validate goods
        /// </summary>
        public void SetErrorResultState(FlowResultRegisterDamage flowResult, MessageHolder messageHolder)
        {
            flowResult.State = FlowResultState.Error;
            messageHolder.Update(GlobalTexts.ErrorWhileScanning, MessageState.Error);
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Validate Goods if called from correction menu and set next state 
        /// </summary>
        public void ValidateGoodsFromCorrectionMenu(bool fromCorrectionMenu, ActionCommandsScanBarcode actionCommandsScanBarcode, EntityMap entityMap, ConsignmentEntity currentConsignmentEntity)
        {
            if (fromCorrectionMenu)
            {
                actionCommandsScanBarcode.ValidateGoods(entityMap, currentConsignmentEntity);
            }
        }



        //TODO :: Write Unit Test
        /// <summary>
        /// Validate Consignment Item and set next state
        /// </summary>
        public FlowStateRegisterDamage ValidateGoods(EntityMap entityMap, ConsignmentEntity currentConsignmentEntity, ActionCommandsScanBarcode actionCommandsScanBarcode, FlowResultRegisterDamage flowResult, FlowDataRegisterDamage flowData)
        {
            actionCommandsScanBarcode.ValidateGoods(entityMap, currentConsignmentEntity);

            if (flowData.FromCorrectionMenu == false)
                currentConsignmentEntity.Event = null;

            return FlowStateRegisterDamage.ActionPerformProcessRelatedValidation;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Call ONLY if online
        /// Returns true if we already know consignment number or if it is unknown but not required
        /// </summary>
        /// <returns></returns>
        public FlowStateRegisterDamage CheckOnlineConsignmentNumberKnownOrNotRequired(ConsignmentEntity consignmentEntity)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.CheckOnlineConsignmentNumberKnownOrNotRequired()");

            var resultState = FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu;
            var consignmentItem = consignmentEntity as ConsignmentItem;
            if (consignmentItem == null)
            {
                //this is a consignment, so id must be known
                return FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu;
            }

            // Check if consignment number is required
            var value = SettingDataProvider.Instance.GetTmiValue<String>(TmiSettings.IsOnlineConsignmentNumberRequired);
            switch (value.ToLower())
            {
                case "true":
                case "yes":
                    if (consignmentItem.HasUnknownConsignment == false)
                         resultState = FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu;
                    break;

                case "false":
                case "no":
                   resultState = FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu;
                    break;

                case "optional":
                    resultState = consignmentItem.HasUnknownConsignment == false ? 
                        FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu : FlowStateRegisterDamage.FlowRegisterWaybill;
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Unknown state for TMIValue 'IsOnlineConsignmentNumberRequired': {0}", value);
                    break;
            }
            return resultState;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Call ONLY if offline
        /// Returns true if we already know consignment number or if it is unknown but not required
        /// </summary>
        /// <returns></returns>
        public FlowStateRegisterDamage CheckOfflineConsignmentNumberKnownOrNotRequired(ConsignmentEntity consignmentEntity)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanbarcode.CheckConsignmentNumberMissingAndRequired()");

            var resultState = FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu;
            var consignmentItem = consignmentEntity as ConsignmentItem;
            if (consignmentItem != null && consignmentItem.HasUnknownConsignment)
            {
                //result = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsOfflineConsignmentNumberRequired) == false;
                var value = SettingDataProvider.Instance.GetTmiValue<string>(TmiSettings.IsOfflineConsignmentNumberRequired);
                switch (value.ToLower())
                {
                    case "true":
                    case "yes":
                        resultState = FlowStateRegisterDamage.FlowScanConsignment;
                        break;

                    case "false":
                    case "no":
                        resultState = FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu;
                        break;

                    case "optional":
                        resultState = FlowStateRegisterDamage.FlowRegisterWaybill;
                        break;

                    default:
                        resultState = FlowStateRegisterDamage.FlowComplete;
                        Logger.LogEvent(Severity.Error, "Unknown state for TMIValue 'IsOfflineConsignmentNumberRequired': {0}", value);
                        break;
                }
               
            }
             return resultState;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Validate the result from sub flow where consignment were scanned. If empty means scan was cancelled, else scan was valid
        /// </summary>
        internal bool ValidateScanConsignmentResult(FlowResultScanConsignment subFlowResult, ConsignmentItem consignmentItem)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.ValidateScanConsignmentResult()");
            bool result;

            switch (subFlowResult.State)
            {
                case FlowResultState.Ok:
                    _messageHolder.Update(string.Format(GlobalTexts.ConsignmentAddedToItem, consignmentItem.ConsignmentDisplayId, consignmentItem.ItemDisplayId), MessageState.Information);
                    result = true;
                    break;

                case FlowResultState.Cancel:
                    result = false;
                    _messageHolder.Update(subFlowResult.Message, MessageState.Error);
                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unknown state");
                    break;
            }

            return result;
        }





        //TODO :: Write Unit Test
        /// <summary>
        /// If a consigment was scanned, give feedback and the flow is over
        /// If consignment item, let flow continue so user can enter consignment if required
        /// </summary>
        /// <returns>If process is finished or not</returns>
        public FlowStateRegisterDamage PerformProcessRelatedValidation(ConsignmentEntity currentConsignmentEntity)
        {
            FlowStateRegisterDamage result;
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.IsConsignmentNumberForItemKnown()");
            if (currentConsignmentEntity.IsScannedOnline)
            {
                if (currentConsignmentEntity is ConsignmentItem)
                    result = FlowStateRegisterDamage.ActionCheckOnlineConsignmentNumberMissingAndRequired;
                else
                    result = FlowStateRegisterDamage.ActionTypeOfObjectScanned;
            }
            else
            {
                if (currentConsignmentEntity is ConsignmentItem)
                    result = FlowStateRegisterDamage.ActionCheckOfflineConsignmentNumberMissingAndRequired;
                else
                    result = FlowStateRegisterDamage.ActionTypeOfObjectScanned;
            }

            return result;
        }



        public void RemoveDamage(ConsignmentEntity damageEntity, EntityMap entityMap)
        {
            entityMap.RemoveEntity(damageEntity);
        }

        public void SendGoodsEvent(ConsignmentEntity consignmentEntity)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsRegisterDamage.SendGoodsEventMessage()");
            var goodsEvent = ConvertDamageToGoodsEvent(consignmentEntity);
            SendGoodsEvent(goodsEvent);
        }
        //TODO :: Write Unit Test
        public void SendGoodsEvents(FlowDataRegisterDamage flowData, EntityMap entityMap)
        {
            foreach (var consignmentEntity in entityMap.GetConsignmentsWithoutEntities())
            {
                if (consignmentEntity.HasStatus(EntityStatus.Scanned))
                    SendGoodsEvent(consignmentEntity);
            }
        }

        private static void SendGoodsEvent(GoodsEvents goodsEvent)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.GoodsEventsEventInformation,
                                              GlobalTexts.RegisterDamage);
            _communicationClient.SendGoodsEvent(goodsEvent, transaction);
        }

        private GoodsEvents ConvertDamageToGoodsEvent(ConsignmentEntity damageEntity)
        {
            var goodsEvent = CreateGoodsEvents(damageEntity);
            ConvertDamageToEventInformation(damageEntity.Damage, goodsEvent.EventInformation);

            return goodsEvent;
        }

        private static GoodsEvents CreateGoodsEvents(ConsignmentEntity damageEntity)
        {
            var goodsEvent = GoodsEventHelper.CreateGoodsEvent(damageEntity);

            if (damageEntity.Damage.HasImages())
            {
                goodsEvent = GoodsEventHelper.AddPhotoInGoodsEvent(goodsEvent, damageEntity, damageEntity.Damage.FolderName);
                GoodsEventHelper.DeletePicturesFolder(damageEntity.Damage.FolderName);
            }

            return goodsEvent;
        }

        private void ConvertDamageToEventInformation(Damage damage, GoodsEventsEventInformation eventInformation)
        {
            if (damage.ConsignmentItemCount > 0)
            {
                eventInformation.Consignment[0].ConsignmentItemsHandled = (short)damage.ConsignmentItemCount;
                eventInformation.Consignment[0].ConsignmentItemsHandledSpecified = true;
            }
            eventInformation.DamageCode = damage.DamageCode != null ? damage.DamageCode.Code : null;
            eventInformation.CauseCode = damage.CauseMeasure != null ? damage.CauseMeasure.CauseCode : null;
            eventInformation.ActionCode = damage.CauseMeasure != null ? damage.CauseMeasure.MeasureCode : null;
            eventInformation.EventComment = damage.Comment;

            if (damage.Signature != null)
            {
                eventInformation.Signature = CreateSignature(damage.Signature.PrintedName, damage.Signature.Sketch);

            }
        }

        private static GoodsEventsEventInformationSignature CreateSignature(string printedName, byte[] sketch)
        {
            var signature = new GoodsEventsEventInformationSignature
                {
                    RecipientName = printedName,
                    Signature = sketch,
                    SignatureId = Guid.NewGuid().ToString(),
                    SignatureTime = DateTime.Now
                };
            return signature;
        }


        public void BackFromFlowCamera(FlowResultCamera subflowResult, Damage damage)
        {
            bool flowSuccess = subflowResult.State == FlowResultState.Ok;
            _messageHolder.Clear();

            if (flowSuccess)
            {
                damage.FolderName = subflowResult.FolderName;
                _messageHolder.Update(subflowResult.Message, MessageState.Information);
            }
            else
            {
                _messageHolder.Update(subflowResult.Message, MessageState.Warning);
            }
        }

        public bool ValidateRegisterWaybillResult(FlowResultRegisterWaybill subFlowResult, ConsignmentItem consignmentItem, ref MessageHolder messageHolder)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.ValidateRegisterWaybill");
            bool result;
            switch (subFlowResult.State)
            {
                case FlowResultState.Ok://Waybill registration information successfully sent
                    messageHolder.Update(string.Format(GlobalTexts.ConsignmentAddedToItem, consignmentItem.ConsignmentDisplayId, consignmentItem.ItemDisplayId), MessageState.Information);
                    result = true;
                    break;

                case FlowResultState.Cancel://skipped Waybill registration 
                    result = true;
                    messageHolder.Update(subFlowResult.Message, MessageState.Error);
                    break;

                case FlowResultState.Error:
                    result = false;
                    messageHolder.Update(subFlowResult.Message, MessageState.Error);
                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unknown state");
                    break;
            }
            return result;
        }
    }
}
﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormEventVerification : BaseForm
    {
        #region Private Variables and Properties
        private MultiButtonControl _multiButtonControl;
        private const string ButtonConfirmName = "btnConfirm";
        private const string ButtonSignatureName = "btnSignature";
        private const string ButtonBackName = "btnBack";
        public FormEventVerification()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
        }

        private void SetTextToGui()
        {
            _multiButtonControl = new MultiButtonControl();
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBackName });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Confirm, ButtonConfirmClick, TabButtonType.Confirm) { Name = ButtonConfirmName });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Signature, ButtonSignatureClick, ButtonSignatureName));
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();

            lblConfirmItem.Text = GlobalTexts.ConfirmDamageForItem;
            lblMeasure.Text = GlobalTexts.Measure + " : 22 - " + GlobalTexts.Destruert;
            lblCause.Text = GlobalTexts.Cause + " : 64 - " + GlobalTexts.FellOutOfCar;
            lblType.Text = GlobalTexts.DamageType + " : " + GlobalTexts.APackageDamage;
            lblComments.Text = GlobalTexts.Comment + ": " + GlobalTexts.PackageTypeDamaged;
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            lblConfirmMessage.Text = GlobalTexts.ConfirmSignature;
           
        }


        #endregion


        #region Methods and Funcions
        public override void OnUpdateView(IFormData formData)
        {
            var formDataEventVerification = (FormDataEventVerification)formData;
            PopulateLabels(formDataEventVerification.ConsignmentEntity);
        }

        private void PopulateLabels(ConsignmentEntity damageEntity)
        {
            string itemId = damageEntity.EntityId;
            string itemType = damageEntity is ConsignmentItem
                           ? GlobalTexts.ConsignmentItem
                           : GlobalTexts.Consignment;

            lblConfirmItem.Text = String.Format("{0} {1} {2}", GlobalTexts.ConfirmDamageFor, itemType, itemId);
            lblType.Text = String.Format("{0} {1}: {2}", GlobalTexts.Damage, GlobalTexts.Type.ToLower(), damageEntity.Damage.DamageCode);
            lblCause.Text = String.Format("{0}: {1}", GlobalTexts.Cause, damageEntity.Damage.CauseMeasure.CauseCodeAndCauseDescription);
            lblMeasure.Text = String.Format("{0}: {1}", GlobalTexts.Measure, damageEntity.Damage.CauseMeasure.MeasureCodeAndMeasureDescription);
            //lblComments.Text = String.Format("{0}: {1}", GlobalTexts.Comment, damageEntity.Damage.Comment);
            lblComments.Text = GlobalTexts.Comment;
            txtComments.Text = damageEntity.Damage.Comment;
        }
        #endregion

        #region Events

        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(EventVerificationviewEvent.Confirm);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEventVerification.ButtonConfirmClick");
            }
        }

        private void ButtonSignatureClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(EventVerificationviewEvent.Signature);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEventVerification.ButtonSignatureClick");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(EventVerificationviewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEventVerification.ButtonBackClick");
            }
        }

        public class EventVerificationviewEvent : ViewEvent
        {
            public const int Back = 1;
            public const int Confirm = 2;
            public const int Signature = 3;
        }
        #endregion

        #region View specific classes

        public class FormDataEventVerification : BaseFormData
        {
            public ConsignmentEntity ConsignmentEntity { get; set; }
        }

        #endregion
    }
}
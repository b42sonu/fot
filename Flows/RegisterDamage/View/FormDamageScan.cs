﻿using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using System;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormDamageScan : BaseBarcodeScanForm
    {
        #region Private Variables and Properties
        private const string ButtonTakePhotoName = "btnTakePhoto";
        private const string ButtonOkName = "btnOk";
        private const string ButtonBackName = "btnBack";
        private const string ButtonNewDamageName = "btnNewDamage";
        private readonly MultiButtonControl _multiButtonControl;
        private bool _hasDamages;
        private readonly TransparentLabel _lblItemCount;

        public FormDamageScan()
        {
            InitializeComponent();
            var lblHeading = AddOrgUnitControl();
            _lblItemCount = AddItemCount();
            SetStandardControlProperties(labelModuleName, lblHeading, lblScanHeading, txtScannedNumber, messageControlValidateScan, _lblItemCount, null);
            lblType.Location = ScaleUtil.GetScaledPoint(21, 339 - 79);
            lblCause.Location = ScaleUtil.GetScaledPoint(21, 368 - 79);
            lblMeasure.Location = ScaleUtil.GetScaledPoint(21, 397 - 79);
            lblComments.Location = ScaleUtil.GetScaledPoint(21, 426 - 79);
            txtComments.Location = ScaleUtil.GetScaledPoint(140, 426 - 79);
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
        }

        private void SetTextToGui()
        {
            lblScanHeading.Text = GlobalTexts.ScanOrEnterBarcode + Colon;
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBackName });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.TakePhoto, ButtonTakePhoto, ButtonTakePhotoName));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonType.Confirm) { Name = ButtonOkName });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.NewDamage, ButtonNewDamageClick, ButtonNewDamageName));
            _multiButtonControl.GenerateButtons();
        }


        private TransparentLabel AddOrgUnitControl()
        {
            var lblOrgUnit = new TransparentLabel
            {
                AutoSize = false,
                Size = new Size(480, 33),
                Location = ScaleUtil.GetScaledPoint(0, 34),//new Point(0, 34),
                Text = ModelUser.UserProfile.OrgUnitName,
                TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter,
                Font = new Font("Arial", 9F, FontStyle.Bold),
            };
            touchPanel.Controls.Add(lblOrgUnit);
            return lblOrgUnit;
        }
        private TransparentLabel AddItemCount()
        {
            var itemCount = new TransparentLabel
            {
                AutoSize = false,
                Size = new Size(438, 30),
                Location = new Point(21, 415),
                Font = new Font("Arial", 9F, FontStyle.Regular),
            };
            touchPanel.Controls.Add(itemCount);
            return itemCount;
        }
        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOkName, txtScannedNumber.Text != string.Empty || _hasDamages);
            if (messageControlValidateScan != null)
                messageControlValidateScan.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text != string.Empty)
                BarcodeEnteredManually(textentered);
        }

        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(DamageScanViewEvents.BarcodeScanned, scannerOutput);
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtScannedNumber.Text = string.Empty;
            ViewEvent.Invoke(DamageScanViewEvents.BarcodeScanned, scannerOutput);
        }

        #endregion

        #region Methods and Funcions

        public override void OnShow(IFormData formData)
        {
            var formDataDamageScan = (FormDataDamageScan)formData;

            if (formDataDamageScan == null) return;

            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged,
                Title = formDataDamageScan.ScanText
            };
            txtScannedNumber.Tag = keyBoardProperty;

            lblScanHeading.Text = formDataDamageScan.ScanText + Colon;
            _multiButtonControl.SetButtonEnabledState(ButtonOkName, formDataDamageScan.HasDamages || txtScannedNumber.Text != string.Empty);
            _multiButtonControl.SetButtonEnabledState(ButtonNewDamageName, true);
            _multiButtonControl.SetButtonEnabledState(ButtonTakePhotoName, formDataDamageScan.ConsignmentEntity != null && formDataDamageScan.ConsignmentEntity.Damage.ValidScan);
            lblType.Location = ScaleUtil.GetScaledPoint(21, 260);
            lblTypeValue.Location = ScaleUtil.GetScaledPoint(77, 260);
            lblCause.Location = ScaleUtil.GetScaledPoint(21, 289);
            lblCauseValue.Location = ScaleUtil.GetScaledPoint(77, 289);
            lblMeasure.Location = ScaleUtil.GetScaledPoint(21, 318);
            lblMeasureValue.Location = ScaleUtil.GetScaledPoint(77, 318);
            lblComments.Location = ScaleUtil.GetScaledPoint(21, 350);
            txtComments.Location = ScaleUtil.GetScaledPoint(140, 350);

            OnUpdateView(formDataDamageScan);
        }
        public override void OnUpdateView(IFormData iformData)
        {
            base.OnUpdateView(iformData);
            var formDataDamageScan = (FormDataDamageScan)iformData;

            if (formDataDamageScan != null)
            {
                _hasDamages = formDataDamageScan.HasDamages;
                if(formDataDamageScan.FromCorrectionMenu == false) 
                    _multiButtonControl.SetButtonEnabledState(ButtonBackName, !formDataDamageScan.HasDamages);
                _multiButtonControl.SetButtonEnabledState(ButtonOkName, formDataDamageScan.HasDamages || txtScannedNumber.Text != string.Empty);
                _multiButtonControl.SetButtonEnabledState(ButtonNewDamageName, true);
                _multiButtonControl.SetButtonEnabledState(ButtonTakePhotoName, formDataDamageScan.ConsignmentEntity != null && formDataDamageScan.ConsignmentEntity.Damage.ValidScan);

                PopulateLabels(formDataDamageScan.Damage);
                if (formDataDamageScan.FromCorrectionMenu == false)
                    PopulateScannedNumber(formDataDamageScan.ConsignmentEntity);

                if (formDataDamageScan.MessageHolder != null && formDataDamageScan.MessageHolder.State != MessageState.Error)
                {
                    txtScannedNumber.Text = string.Empty;
                }

                PopulateMessage(formDataDamageScan.MessageHolder);
                SetGuiBasedOnWhereTheUserComeFrom(formDataDamageScan);
                _lblItemCount.Text = string.Format("{0}: {1}", GlobalTexts.NoOfItems, formDataDamageScan.ScannedEntityCount);
                
            }

            SetKeyboardState(KeyboardState.Numeric);
        }

        public override void OnClose()
        {
            base.OnClose();
            messageControlValidateScan.ClearMessage();
            txtScannedNumber.Text = string.Empty;
        }

        private void SetGuiBasedOnWhereTheUserComeFrom(FormDataDamageScan formData)
        {
            lblScanHeading.Visible = !formData.FromCorrectionMenu;
            txtScannedNumber.Visible = !formData.FromCorrectionMenu;
            lblEarlierEvents.Visible = formData.FromCorrectionMenu;
            txtEarlierEvents.Visible = formData.FromCorrectionMenu;
            _multiButtonControl.SetButtonEnabledState(ButtonNewDamageName, !formData.FromCorrectionMenu);
            txtEarlierEvents.Location = new Point(21, 150);
            txtEarlierEvents.BringToFront();
            if (formData.FromCorrectionMenu)
            {
                lblEarlierEvents.Visible = !messageControlValidateScan.Visible;
                messageControlValidateScan.BringToFront();
                txtEarlierEvents.Location = new Point(21, 105);
                messageControlValidateScan.Location = new Point(21, 105);
                lblEarlierEvents.Text = formData.HeaderEarlierEvents;
                txtEarlierEvents.Text = formData.EarlierEvents;
                if (formData.ConsignmentEntity != null)
                {
                    //Read only view when consignment entity comes from correction menu
                    //TODO:: Confirm this comment
                    // buttonTakePhoto.Location = new System.Drawing.Point(56, 288); 
                    //messageControlValidateScan.Location = new Point(ScaleUtil.GetScaledPosition(25), ScaleUtil.GetScaledPosition(316));

                    if (formData.ConsignmentEntity != null)
                    {
                        lblScanHeading.Text = string.Empty;
                    }
                }


                lblScanHeading.Font = new Font("Tahoma", 9F, FontStyle.Regular);
                StopScan();
            }

            else
            {

                //TODO:: Confirm this comment
                //buttonTakePhoto.Location = new System.Drawing.Point(242, 447);
                messageControlValidateScan.Location = new Point(21, 156 - 8);
                lblScanHeading.Font = new Font("Tahoma", 9F, FontStyle.Regular);
                //  messageControlValidateScan.Location = new Point(21, 105);
            }


            if (formData.FromCorrectionMenu)
            {
                _multiButtonControl.SetButtonEnabledState(ButtonOkName, true);
                _multiButtonControl.SetButtonEnabledState(ButtonTakePhotoName, true);
            }
        }

        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
            {
                switch (messageHolder.State)
                {
                    case MessageState.Information:
                        SoundUtil.Instance.PlaySuccessSound();
                        break;

                    case MessageState.Warning:
                        SoundUtil.Instance.PlayWarningSound();
                        break;
                }
                messageControlValidateScan.ShowMessage(messageHolder);
            }
            else
                messageControlValidateScan.ClearMessage();
        }

        private void PopulateScannedNumber(ConsignmentEntity damageEntity)
        {
            if (damageEntity != null && !damageEntity.Damage.ValidScan)
                txtScannedNumber.Text = damageEntity.EntityId;
        }

        private void PopulateLabels(Damage enteredDamageInformation)
        {
            if (enteredDamageInformation != null)
            {
                if (enteredDamageInformation.CauseMeasure != null)
                {


                    lblType.Text = enteredDamageInformation.DamageCode.Code;
                    lblTypeValue.Text = enteredDamageInformation.DamageCode.Description;
                    lblType.Text += enteredDamageInformation.DamageCode.Code == string.Empty ? string.Empty : Colon + " ";
                    lblCause.Text = enteredDamageInformation.CauseMeasure.CauseCode;
                    lblCause.Text += enteredDamageInformation.CauseMeasure.CauseCode == string.Empty ? string.Empty : Colon + " ";
                    lblMeasure.Text = enteredDamageInformation.CauseMeasure.MeasureCode;
                    lblMeasure.Text += enteredDamageInformation.CauseMeasure.MeasureCode == string.Empty ? string.Empty : Colon + " ";
                    lblCauseValue.Text = ReturnCommonFormattedTest(enteredDamageInformation.CauseMeasure.CauseDescription, 35);
                    lblMeasureValue.Text = ReturnCommonFormattedTest(enteredDamageInformation.CauseMeasure.MeasureDescription.Trim(), 35);
                }
                lblComments.Text = GlobalTexts.Comment + ":";
                txtComments.Text = enteredDamageInformation.Comment;
            }
        }


        /// <summary>
        /// Return similar length string if string exceeds space
        /// </summary>
        private string ReturnCommonFormattedTest(string text, double maxLimit)
        {
            double weightage = 0;
            string finalString = string.Empty;
            foreach (char c in text)
            {
                weightage += GetWeightage(c);
                if (weightage < maxLimit)
                    finalString += c;
                else
                {
                    finalString += "...";
                    break;
                }
            }
            return finalString;
        }

        /// <summary>
        /// Assign weightage as per width chars take
        /// </summary>
        private double GetWeightage(char c)
        {
            if (Char.IsLower(c) == false)
                return 1.8;

            switch (c)
            {
                case 'w':
                case 'm':
                    return 1.8;
                case 'i':
                    return .45;
                case 'j':
                    return .6;
                case 't':
                    return .84;
                default:
                    return 1;
            }

        }
        #endregion

        #region Events

        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtScannedNumber.Text))
                    ViewEvent.Invoke(DamageScanViewEvents.Ok);
                else
                    BarcodeEnteredManually(txtScannedNumber.Text.Trim());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ShowFormDamageScan.ButtonConfirmClick");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(DamageScanViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ShowFormDamageScan.ButtonBackClick");
            }
        }

        private void ButtonNewDamageClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(DamageScanViewEvents.NewDamage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ShowFormDamageScan.ButtonNewDamageClick");
            }
        }

        private void ButtonTakePhoto(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(DamageScanViewEvents.TakePhoto);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ShowFormDamageScan.ButtonNewDamageClick");
            }
        }

        public class DamageScanViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
            public const int BarcodeScanned = 2;
            public const int NewDamage = 3;
            public const int TakePhoto = 4;
        }

        #endregion
    }

    public class FormDataDamageScan : BaseFormData
    {
        public string HeaderEarlierEvents { get; set; }
        public string EarlierEvents { get; set; }
        public bool HasDamages { get; set; }
        public ConsignmentEntity ConsignmentEntity { get; set; }
        public Damage Damage { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public bool FromCorrectionMenu { get; set; }
        public int ScannedEntityCount { get; set; }
        public string ScanText { get; set; }

    }
}
﻿using PreCom.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormDamageComments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code


        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblDamageType = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblComment = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMeasureValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblCauseText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDamageTypeValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblEarlierEvents = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtEarlierEvents = new System.Windows.Forms.TextBox();
            this.txtFreeText = new PreCom.Controls.PreComInput2();
            this.LabelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblDamageType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasureValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCauseText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDamageTypeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelOrgUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.LabelOrgUnit);
            this.touchPanel.Controls.Add(this.lblDamageType);
            this.touchPanel.Controls.Add(this.lblComment);
            this.touchPanel.Controls.Add(this.lblMeasureValue);
            this.touchPanel.Controls.Add(this.lblMeasure);
            this.touchPanel.Controls.Add(this.lblCauseText);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblDamageTypeValue);
            this.touchPanel.Controls.Add(this.lblEarlierEvents);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtEarlierEvents);
            this.touchPanel.Controls.Add(this.txtFreeText);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;

            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            // 
            // lblEarlierEvents
            // 
            this.lblEarlierEvents.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblEarlierEvents.Location = new System.Drawing.Point(21, 70);
            this.lblEarlierEvents.Name = "lblEarlierEvents";
            this.lblEarlierEvents.Size = new System.Drawing.Size(170, 27);


            // 
            // txtEarlierEvents
            // 
            this.txtEarlierEvents.BackColor = System.Drawing.Color.White;
            this.txtEarlierEvents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEarlierEvents.Location = new System.Drawing.Point(21, 105);
            this.txtEarlierEvents.Multiline = true;
            this.txtEarlierEvents.Name = "txtEarlierEvents";
            this.txtEarlierEvents.ReadOnly = true;
            this.txtEarlierEvents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEarlierEvents.Size = new System.Drawing.Size(438, 70);
            this.txtEarlierEvents.TabIndex = 1;

            // 
            // lblDamageTypeValue
            // 
            this.lblDamageTypeValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDamageTypeValue.Location = new System.Drawing.Point(21, 214);
            this.lblDamageTypeValue.Name = "lblDamageTypeValue";
            this.lblDamageTypeValue.Size = new System.Drawing.Size(97, 27);

            // 
            // lblCause
            // 
            this.lblCause.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblCause.Location = new System.Drawing.Point(21, 245);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(70, 27);
            // 
            // lblCauseText
            // 
            this.lblCauseText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblCauseText.Location = new System.Drawing.Point(21, 276);
            this.lblCauseText.Name = "lblLabelCauseText";
            this.lblCauseText.Size = new System.Drawing.Size(138, 27);

            // 
            // lblMeasure
            //
            this.lblMeasure.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblMeasure.Location = new System.Drawing.Point(21, 309);
            this.lblMeasure.Name = "lblLabelMeasure";
            this.lblMeasure.Size = new System.Drawing.Size(98, 27);
            
            // 
            // lblType
            // 
            this.lblDamageType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblDamageType.Location = new System.Drawing.Point(21, 183);
            this.lblDamageType.Name = "lblType";
            this.lblDamageType.Size = new System.Drawing.Size(186, 27);
            // 
            // lblMeasureValue
            // 
            this.lblMeasureValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblMeasureValue.Location = new System.Drawing.Point(21, 341);
            this.lblMeasureValue.Name = "lblMeasureValue";
            this.lblMeasureValue.Size = new System.Drawing.Size(438, 41);
            // 
            // lblComment
            // 
            this.lblComment.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblComment.Location = new System.Drawing.Point(21, 377);
            this.lblComment.Name = "lblComment";
            this.lblComment.Size = new System.Drawing.Size(111, 27);

            
            // 
            // txtFreeText
            // 
            this.txtFreeText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtFreeText.Location = new System.Drawing.Point(21, 410);
            this.txtFreeText.Multiline = true;
            this.txtFreeText.Name = "txtFreeText";
            this.txtFreeText.Size = new System.Drawing.Size(438, 79);
            this.txtFreeText.TabIndex = 0;
            this.txtFreeText.TextTranslation = true;
            // 
            // LabelOrgUnit
            // 
            this.LabelOrgUnit.AutoSize = false;
            this.LabelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LabelOrgUnit.Location = new System.Drawing.Point(0, 42);
            this.LabelOrgUnit.Name = "transparentLabel1";
            this.LabelOrgUnit.Size = new System.Drawing.Size(480, 33);
            this.LabelOrgUnit.Text = "Module Name";
            this.LabelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormOperationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 588);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDamageComments";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblDamageType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasureValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCauseText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDamageTypeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelOrgUnit)).EndInit();
            this.ResumeLayout(false);

        }

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblEarlierEvents;
        private System.Windows.Forms.TextBox txtEarlierEvents;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblDamageTypeValue;
        private Resco.Controls.CommonControls.TransparentLabel lblCause;
        private Resco.Controls.CommonControls.TransparentLabel lblCauseText;
        private Resco.Controls.CommonControls.TransparentLabel lblMeasure;
        private Resco.Controls.CommonControls.TransparentLabel lblMeasureValue;
        private Resco.Controls.CommonControls.TransparentLabel lblComment;
        private PreCom.Controls.PreComInput2 txtFreeText;
        private Resco.Controls.CommonControls.TransparentLabel lblDamageType;
        #endregion
        private Resco.Controls.CommonControls.TransparentLabel LabelOrgUnit;
    
    }
}
﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormCause
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCause));
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDamageTypeValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtBoxCause = new System.Windows.Forms.TextBox();
            this.lblEarlierEvents = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblType = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtEarlierEvents = new System.Windows.Forms.TextBox();
            this.listCause = new System.Windows.Forms.ListBox();
            this.LabelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelInstructions = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDamageTypeValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.LabelOrgUnit);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblDamageTypeValue);
            this.touchPanel.Controls.Add(this.txtBoxCause);
            this.touchPanel.Controls.Add(this.lblEarlierEvents);
            this.touchPanel.Controls.Add(this.lblType);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtEarlierEvents);
            this.touchPanel.Controls.Add(this.listCause);
            this.touchPanel.Controls.Add(this.labelInstructions);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;

            // 
            // labelInstructions
            // 
            this.labelInstructions.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelInstructions.Location = new System.Drawing.Point(21, 466);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(407, 27);

            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            // 
            // LabelOrgUnit
            // 
            this.LabelOrgUnit.AutoSize = false;
            this.LabelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LabelOrgUnit.Location = new System.Drawing.Point(0, 42);
            this.LabelOrgUnit.Name = "transparentLabel1";
            this.LabelOrgUnit.Size = new System.Drawing.Size(480, 33);
            this.LabelOrgUnit.Text = "Module Name";
            this.LabelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            // lblEarlierEvents
            // 
            this.lblEarlierEvents.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblEarlierEvents.Location = new System.Drawing.Point(21, 70);
            this.lblEarlierEvents.Name = "lblEarlierEvents";
            this.lblEarlierEvents.Size = new System.Drawing.Size(170, 27);

            // 
            // txtEarlierEvents
            // 
            this.txtEarlierEvents.BackColor = System.Drawing.Color.White;
            this.txtEarlierEvents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEarlierEvents.Location = new System.Drawing.Point(21, 105);
            this.txtEarlierEvents.Multiline = true;
            this.txtEarlierEvents.Name = "txtEarlierEvents";
            this.txtEarlierEvents.ReadOnly = true;
            this.txtEarlierEvents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEarlierEvents.Size = new System.Drawing.Size(438, 70);
            this.txtEarlierEvents.TabIndex = 1;

            // 
            // lblType
            // 
            this.lblType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblType.Location = new System.Drawing.Point(21, 183);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(186, 27);

            // 
            // lblDamageTypeValue
            // 
            this.lblDamageTypeValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDamageTypeValue.Location = new System.Drawing.Point(21, 214);
            this.lblDamageTypeValue.Name = "lblDamageTypeValue";
            this.lblDamageTypeValue.Size = new System.Drawing.Size(97, 27);

            // 
            // lblCause
            // 
            this.lblCause.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblCause.Location = new System.Drawing.Point(21, 245);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(70, 27);

            // 
            // txtBoxCause
            // 
            this.txtBoxCause.Location = new System.Drawing.Point(21, 278);
            this.txtBoxCause.Name = "txtBoxCause";
            this.txtBoxCause.Size = new System.Drawing.Size(438, 41);
            this.txtBoxCause.TabIndex = 6;
            this.txtBoxCause.KeyUp += TxtCauseKeyUp;

            // 
            // listCause
            // 
            this.listCause.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.listCause.Location = new System.Drawing.Point(21, 314);
            this.listCause.Name = "listCause";
            this.listCause.Size = new System.Drawing.Size(438, 159);
            this.listCause.TabIndex = 10;
            this.listCause.SelectedIndexChanged += ListCauseTypeSelectedIndexChanged;
            
            
            // 
            // FormOperationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormCause";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDamageTypeValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelOrgUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).EndInit();
            this.ResumeLayout(false);

        }



        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblType;
        private Resco.Controls.CommonControls.TransparentLabel lblEarlierEvents;
        private System.Windows.Forms.TextBox txtEarlierEvents;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private TextBox txtBoxCause;
        private ListBox listCause;
        private Resco.Controls.CommonControls.TransparentLabel lblDamageTypeValue;
        private Resco.Controls.CommonControls.TransparentLabel lblCause;
        private Resco.Controls.CommonControls.TransparentLabel LabelOrgUnit;
        private Resco.Controls.CommonControls.TransparentLabel labelInstructions;
    }
}
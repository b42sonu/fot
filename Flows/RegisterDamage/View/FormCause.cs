﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormCause : BaseForm
    {
        private FormDataDamageType _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonOkName = "buttonOk";
        private CauseMeasure _selectedCause;
        private List<CauseMeasure> _causeMeasureTypeFilterlist;
        private bool _selectItem;
        public FormCause()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetStandardControlProperties(labelModuleName,LabelOrgUnit,null,null,null,null,null);
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            lblType.Text = GlobalTexts.DamageType;// "Type of Damage";
            lblCause.Text = GlobalTexts.Cause+Colon;//"Cause"
            labelInstructions.Text = GlobalTexts.SelectReasonInstruction;
            LabelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonCancelClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm){Name= ButtonOkName});
            _multiButtonControl.GenerateButtons();
        }
        private void InitOnScreenKeyBoardProperties()
        {
            var onScreenKeyBoardProperties = new OnScreenKeyboardProperties(GlobalTexts.Cause, KeyPadTypes.Qwerty);
            txtBoxCause.Tag = onScreenKeyBoardProperties;
            onScreenKeyBoardProperties.TextChanged = TxtReasonTextChanged;
        }
        public override void OnShow(IFormData formData)
        {
            Visible = true;
            Show(BaseModule.MainForm);
            _formData = (FormDataDamageType)formData;

            //Only set globale formDataDamageInformation when containing data.
            if (_formData.Damage.DamageCode.Description != null)
                lblDamageTypeValue.Text = _formData.Damage.DamageCode.Code+ " " +_formData.Damage.DamageCode.Description;
           
            lblEarlierEvents.Text = _formData.HeaderEarlierEvents;
            txtEarlierEvents.Text = _formData.EarlierEvents;
          
            BindListCause();
            EnableDiableButtons();
            SelectFirstElement();
          
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataDamageType)iformData;
            if (listCause.Items.Count != 1 || _formData.Damage.CauseMeasure.CauseCode != null)
            {
                return;
            }
         
            listCause.SelectedIndex = 0;
            SetCauseValue(_formData.Damage.CauseMeasure, (CauseMeasure)listCause.SelectedItem);
            ViewEvent.Invoke(SelectCauseViewEvents.Ok, _formData.Damage);
        }

        private void SelectFirstElement()
        {
            if (listCause.SelectedItem != null)
            {
                _selectedCause = (CauseMeasure)listCause.SelectedItem;
                txtBoxCause.Text = _selectedCause.CauseCodeAndCauseDescription;
                EnableDiableButtons();
            }
            else
            {
                if (_formData.IsPrevious && _formData.Damage.CauseMeasure.CauseCode != null)
                    listCause.SelectedValue = _formData.Damage.CauseMeasure.CauseCode;
            }
        }

        public override void ShowingView()
        {
            base.ShowingView();
            txtBoxCause.Focus();
        }

        private void BindListCause()
        {
            if (_formData.Damage.CauseMeasure.CauseCode == null) 
            {
                _selectItem = true;
                _causeMeasureTypeFilterlist =
                    _formData.CauseMeasureList.GroupBy(n => n.CauseCode)
                             .Select(o => o.First())
                             .ToList();

                listCause.ValueMember = "CauseCode";
                listCause.DisplayMember = "CauseCodeAndCauseDescription";
                listCause.DataSource = _causeMeasureTypeFilterlist;
                listCause.SelectedIndex = -1;
                txtBoxCause.Text = string.Empty;
                _selectItem = false;
            }
        }

        private void EnableDiableButtons()
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOkName, (listCause.Items.Count != 0) && listCause.SelectedIndex >= 0);
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                _formData.Damage.CauseMeasure.CauseCode = null;
                _formData.Damage.CauseMeasure.CauseCodeAndCauseDescription = null;
                _formData.Damage.CauseMeasure.CauseDescription = null;
                listCause.SelectedIndex = -1;
                ViewEvent.Invoke(SelectCauseViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonCancelClick");
            }
        }
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                SetCauseValue(_formData.Damage.CauseMeasure, (CauseMeasure)listCause.SelectedItem);
                ViewEvent.Invoke(SelectCauseViewEvents.Ok, _formData.Damage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonOkClick");
            }
        }

        private void ListCauseTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!_selectItem)
                {
                    if (listCause.SelectedItem != null)
                    {
                        _selectedCause = (CauseMeasure)listCause.SelectedItem;
                        txtBoxCause.Text = _selectedCause.CauseCodeAndCauseDescription;
                        EnableDiableButtons();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormCause.ListCauseTypeSelectedIndexChanged");
            }
        }

        private static void SetCauseValue(CauseMeasure causeMeasure, CauseMeasure selectedCauseMeasure)
        {
            if (selectedCauseMeasure == null || causeMeasure == null) return;
            causeMeasure.CauseCode = selectedCauseMeasure.CauseCode;
            causeMeasure.CauseCodeAndCauseDescription = selectedCauseMeasure.CauseCodeAndCauseDescription;
            causeMeasure.CauseDescription = selectedCauseMeasure.CauseDescription;
        }

        private void TxtCauseKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                FilterList();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormCause.TxtCauseKeyUp");
            }
        }
        private void TxtReasonTextChanged(bool userFinishedTyping, string textentered)
        {
            try
            {
                if (!userFinishedTyping) return;
                FilterList();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.TxtBoxDamageTypeKeyUp");
            }
        }

        private void FilterList()
        {
            if (!string.IsNullOrEmpty(txtBoxCause.Text.Trim()))
            {
                {
                    var enteredText = txtBoxCause.Text.ToLower();
                    var filteredActions =
                        (from c in _causeMeasureTypeFilterlist
                         where c.CauseCodeAndCauseDescription.ToLower().StartsWith(enteredText)
                         select c).ToList();

                    filteredActions.AddRange(
                      (from c in _causeMeasureTypeFilterlist
                       where ((c.CauseCodeAndCauseDescription.ToLower().Contains(enteredText)) && !(c.CauseCodeAndCauseDescription.ToLower().StartsWith(enteredText)))
                       select c).ToList());

                    listCause.SelectedIndexChanged -= ListCauseTypeSelectedIndexChanged;
                    listCause.DataSource = filteredActions;
                    listCause.SelectedIndex = -1;
                    EnableDiableButtons();
                    listCause.SelectedIndexChanged += ListCauseTypeSelectedIndexChanged;
                }
            }
            else
            {
                BindListCause();
                EnableDiableButtons();
            }
            txtBoxCause.Select(txtBoxCause.Text.Length, 0);
        }

        public class SelectCauseViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
        }
    }

}
﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormEventVerification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConfirmMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConfirmItem = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMeasure = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblCause = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblType = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblComments = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtComments = new TextBox();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirmMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirmItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComments)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblConfirmMessage);
            this.touchPanel.Controls.Add(this.lblConfirmItem);
            this.touchPanel.Controls.Add(this.lblMeasure);
            this.touchPanel.Controls.Add(this.lblCause);
            this.touchPanel.Controls.Add(this.lblType);
            this.touchPanel.Controls.Add(this.lblComments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtComments);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblConfirmMessage
            // 
            this.lblConfirmMessage.Location = new System.Drawing.Point(21, 399);
            this.lblConfirmMessage.Name = "lblConfirmMessage";
            this.lblConfirmMessage.Size = new System.Drawing.Size(438, 65);
   
            // 
            // lblConfirmItem
            // 
            this.lblConfirmItem.Location = new System.Drawing.Point(21, 62);
            this.lblConfirmItem.Name = "lblConfirmItem";
            this.lblConfirmItem.Size = new System.Drawing.Size(438, 65);

            this.lblConfirmItem.AutoSize = false;
            // 
            // lblMeasure
            // 
            this.lblMeasure.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblMeasure.Location = new System.Drawing.Point(21, 243);
            this.lblMeasure.Name = "lblMeasure";
            this.lblMeasure.Size = new System.Drawing.Size(438, 27);

            // 
            // lblCause
            // 
            this.lblCause.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblCause.Location = new System.Drawing.Point(21, 192);
            this.lblCause.Name = "lblCause";
            this.lblCause.Size = new System.Drawing.Size(438, 30);

            // 
            // lblType
            // 
            this.lblType.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblType.Location = new System.Drawing.Point(21, 145);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(438, 26);

            // 
            // lblComments
            // 
            this.lblComments.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular);
            this.lblComments.Location = new System.Drawing.Point(21, 289);
            this.lblComments.Name = "lblComments";
            this.lblComments.Size = new System.Drawing.Size(438, 26);//81);
            this.lblComments.AutoSize = false;
            // 
            // txtComments
            // 
            this.txtComments.Location = new System.Drawing.Point(21, 317);
            this.txtComments.Multiline = true;
            this.txtComments.Name = "txtComments";
            this.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComments.Size = new System.Drawing.Size(438, 70);
            this.txtComments.BorderStyle = BorderStyle.None;
            this.txtComments.BackColor = System.Drawing.Color.White;//System.Drawing.Color.FromArgb(216, 217, 218);
            this.txtComments.ReadOnly = true;
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(132, 17);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(197, 27);

    
            // 
            // FormEventVerification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480,552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormEventVerification";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirmItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeasure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCause)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConfirmMessage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblConfirmItem;
        private Resco.Controls.CommonControls.TransparentLabel lblMeasure;
        private Resco.Controls.CommonControls.TransparentLabel lblCause;
        private Resco.Controls.CommonControls.TransparentLabel lblType;
        private Resco.Controls.CommonControls.TransparentLabel lblComments;
        private Resco.Controls.CommonControls.TransparentLabel lblConfirmMessage;
        private System.Windows.Forms.TextBox txtComments;
    }
}
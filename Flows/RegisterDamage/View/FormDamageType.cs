﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormDamageType : BaseForm
    {
        private FormDataDamageType _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private DamageCode _selectedDamageType;
        private bool _selectItem;
        private const string ButtonOkName = "buttonOk";

        public FormDamageType()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            SetTextToGui();
            SetStandardControlProperties(labelModuleName, LabelOrgUnit, null, null, null, null, null);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            InitOnScreenKeyBoardProperties();
        }
        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            lblType.Text = GlobalTexts.DamageType + Colon;//"Type of Damage";
            LabelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            labelInstructions.Text = GlobalTexts.SelectTypeInstruction;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonCancelClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOkName });
            _multiButtonControl.GenerateButtons();
        }

        public override void OnShow(IFormData formData)
        {
            Visible = true;
            Show(BaseModule.MainForm);

            _formData = (FormDataDamageType)formData;
            if (_formData.Damage.DamageCode == null||_formData.IsPrevious)
            {
                _selectItem = true;
                listDamageType.DataSource = _formData.DamageCodeList;
                listDamageType.DisplayMember = "CodeAndDescription";
                listDamageType.ValueMember = "Code";
                listDamageType.SelectedIndex = -1;
                txtBoxDamageType.Text = string.Empty;
                _selectItem = false;
            }

            SetEarlierEvents(_formData);
            EnableDiableButtons();
            SelectFirstElement();
        }

        private void SelectFirstElement()
        {
            if (listDamageType.SelectedItem != null)
            {
                _selectedDamageType = (DamageCode)listDamageType.SelectedItem;
                txtBoxDamageType.Text = _selectedDamageType.Code + " " + _selectedDamageType.Description;
                EnableDiableButtons();
            }
            else
            {
                if (_formData.Damage.DamageCode != null && (_formData.IsPrevious && _formData.Damage.DamageCode.Code != null))
                    listDamageType.SelectedValue = _formData.Damage.DamageCode.Code;
            }
        }

        public override void ShowingView()
        {
            base.ShowingView();
            txtBoxDamageType.Focus();
        }

        private void BindingList(List<DamageCode> damageCodeList)
        {
            _selectItem = true;
            if (damageCodeList != null)
            {
                listDamageType.DataSource = damageCodeList;
                listDamageType.DisplayMember = "CodeAndDescription";
                listDamageType.ValueMember = "Code";
            }
            listDamageType.SelectedIndex = -1;
            EnableDiableButtons();
            txtBoxDamageType.Select(txtBoxDamageType.Text.Length, 0);
            _selectItem = false;
        }

        private void EnableDiableButtons()
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOkName, (listDamageType.Items.Count != 0) && listDamageType.SelectedIndex >= 0);
        }

        public void SetEarlierEvents(FormDataDamageType formDataDamageType)
        {
            bool hasEarlierEvents = _formData.CurrentConsignmentEntity != null && _formData.CurrentConsignmentEntity.Event != null && _formData.CurrentConsignmentEntity.Event.Length > 0;

            if (!hasEarlierEvents)
                txtEarlierEvents.Text = !_formData.FromCorrectionMenu ? GlobalTexts.NoInformationAboutItemAvailable : GlobalTexts.NoEarlierDamageAvailable;
            if (hasEarlierEvents)
            {
                lblEarlierEvents.Text = GlobalTexts.EarlierEventsFor + ": " + StringUtil.GetTextForConsignmentEntity(_formData.CurrentConsignmentEntity.EntityDisplayId, false);
                var earlierEvents = new StringBuilder();

                if (_formData.CurrentConsignmentEntity.Event != null)
                {
                    //get unique cause codes and its description
                    var causelist = _formData.CauseMeasureList.GroupBy(n => n.CauseCode)
                                          .Select(o => o.First())
                                          .ToList();
                    foreach (var earlierEvent in _formData.CurrentConsignmentEntity.Event)
                    {
                        if (Convert.ToString(earlierEvents) != string.Empty)
                            earlierEvents.Append(Environment.NewLine + "--------------------------" + Environment.NewLine);



                        //filter cause code and description for current event's cause code
                        var causeCodeAndDesc =
                            causelist.SingleOrDefault(c => c.CauseCode.Equals(earlierEvent.ReasonCode));

                        //filter measure code and description for current event's cause code
                        var measureCodeAndDesc = (from n in _formData.CauseMeasureList where n.CauseCode == earlierEvent.ReasonCode && n.MeasureCode == earlierEvent.ActionCode select n).SingleOrDefault();

                        //TODO: implement functionality for getting damage code and its description, when damage code will be implemented on LM

                        //Get damage code and its description
                        var damageCodeAndDesc = earlierEvent.DamageType;
                        earlierEvents.Append(Convert.ToString(earlierEvent.EventTime) + Environment.NewLine);


                        //If damage code not empty then append damage code
                        if (string.IsNullOrEmpty(damageCodeAndDesc) == false)
                            earlierEvents.Append(damageCodeAndDesc);

                        if (causeCodeAndDesc != null && !string.IsNullOrEmpty(causeCodeAndDesc.CauseCodeAndCauseDescription))
                            earlierEvents.Append(!string.IsNullOrEmpty(damageCodeAndDesc)
                                                     ? Environment.NewLine + causeCodeAndDesc.CauseCodeAndCauseDescription
                                                     : causeCodeAndDesc.CauseCodeAndCauseDescription);

                        if (measureCodeAndDesc != null)
                            earlierEvents.Append((!string.IsNullOrEmpty(damageCodeAndDesc) || (causeCodeAndDesc != null && !string.IsNullOrEmpty(causeCodeAndDesc.CauseCodeAndCauseDescription)) ? Environment.NewLine + measureCodeAndDesc.MeasureCodeAndMeasureDescription : string.Empty + measureCodeAndDesc.MeasureCodeAndMeasureDescription));
                    }
                }
                txtEarlierEvents.Text = earlierEvents.ToString();
            }
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                _formData.Damage.DamageCode = null;
                lblEarlierEvents.Text = string.Empty;
                txtEarlierEvents.Text = string.Empty;
                ViewEvent.Invoke(DamageInfoViewEvents.Back);
                //listDamageType.DataSource = null;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonCancelClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                _formData.Damage.DamageCode = (DamageCode)listDamageType.SelectedItem;
                ViewEvent.Invoke(DamageInfoViewEvents.Ok, _formData.Damage, lblEarlierEvents.Text, txtEarlierEvents.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonOkClick");
            }
        }

        private void ListDamageTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                if (!_selectItem)
                {
                    _selectedDamageType = (DamageCode)listDamageType.SelectedItem;
                    txtBoxDamageType.Text = _selectedDamageType.Code + " " + _selectedDamageType.Description;
                    EnableDiableButtons();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ListDamageTypeSelectedIndexChanged");
            }
        }
        private void InitOnScreenKeyBoardProperties()
        {
            var onScreenKeyBoardProperties = new OnScreenKeyboardProperties(GlobalTexts.DamageType, KeyPadTypes.Qwerty);
            txtBoxDamageType.Tag = onScreenKeyBoardProperties;
            onScreenKeyBoardProperties.TextChanged = TxtReasonTextChanged;
        }
        private void TxtBoxDamageTypeKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                FilterList();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.TxtBoxDamageTypeKeyUp");
            }
        }

        private void FilterList()
        {
            if (_formData.DamageCodeList == null) return ;
            var damageType = _formData.DamageCodeList;
            string damageTypeHint = txtBoxDamageType.Text.ToLower();
            if (!string.IsNullOrEmpty(damageTypeHint))
            {
                var filteredReasons =
                    (from c in damageType
                     where (c.Code+" "+c.Description).ToLower().StartsWith(damageTypeHint)
                     select c).ToList();

                filteredReasons.AddRange((from c in damageType
                                          where
                                              (c.Code+c.Description).ToLower().Contains(damageTypeHint) &&
                                              !(c.Code + " " + c.Description).ToLower().StartsWith(damageTypeHint)
                                          select c).ToList());

                BindingList(filteredReasons);
            }
            else
                BindingList(_formData.DamageCodeList);
            _selectedDamageType = (DamageCode) listDamageType.SelectedItem;
            EnableDiableButtons();
        }

        private void TxtReasonTextChanged(bool userFinishedTyping, string textentered)
        {
            try
            {
                if (!userFinishedTyping) return;
                FilterList();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.TxtBoxDamageTypeKeyUp");
            }
        }

        public class DamageInfoViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
        }
    }
    public class FormDataDamageType : BaseFormData
    {
        public bool FromCorrectionMenu { get; set; }
        public string HeaderEarlierEvents { get; set; }
        public string EarlierEvents { get; set; }
        public ConsignmentEntity CurrentConsignmentEntity { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public Damage Damage { get; set; }
        public List<DamageCode> DamageCodeList { get; set; }
        public List<CauseMeasure> CauseMeasureList { get; set; }
        public bool IsPrevious { get; set; }
    }
}
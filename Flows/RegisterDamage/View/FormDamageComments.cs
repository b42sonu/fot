﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormDamageComments : BaseForm
    {
        private FormDataDamageType _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonOkName = "buttonOk";

        public FormDamageComments()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetStandardControlProperties(labelModuleName, LabelOrgUnit, null, null, null, null, null);
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            lblDamageType.Text = GlobalTexts.DamageType;
            lblCause.Text = GlobalTexts.Cause;
            lblComment.Text = GlobalTexts.Comment+Colon;
            lblMeasure.Text = GlobalTexts.Measure;
            LabelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonCancelClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOkName });
            _multiButtonControl.GenerateButtons();
        }
        private void InitOnScreenKeyBoardProperties()
        {
            var onScreenKeyBoardProperties = new OnScreenKeyboardProperties(GlobalTexts.Comment, KeyPadTypes.Qwerty);
            txtFreeText.Tag = onScreenKeyBoardProperties;
        }
        public override void OnShow(IFormData formData)
        {
            _formData = (FormDataDamageType)formData;
            if(_formData != null)
            {
                if(_formData.Damage != null)
                {
                    if (_formData.Damage.DamageCode.Description != null)
                        lblCauseText.Text = _formData.Damage.CauseMeasure.CauseCodeAndCauseDescription;

                    lblDamageTypeValue.Text = _formData.Damage.DamageCode.Code + " " + _formData.Damage.DamageCode.Description;
                    lblMeasureValue.Text = _formData.Damage.CauseMeasure.MeasureCodeAndMeasureDescription.Trim();

                    txtFreeText.Text = _formData.Damage.Comment ?? string.Empty;
                }

                lblEarlierEvents.Text = _formData.HeaderEarlierEvents;
                txtEarlierEvents.Text = _formData.EarlierEvents;
            }
           
           
            SetKeyboardState(KeyboardState.Alpha);
        }


        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                txtFreeText.Text = "";
                _formData.Damage.Comment = string.Empty;
                ViewEvent.Invoke(EnterCommentsViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonCancelClick");
            }
        }
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                _formData.Damage.Comment = txtFreeText.Text;
                ViewEvent.Invoke(EnterCommentsViewEvents.Ok, _formData.Damage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonOkClick");
            }
        }

        public class EnterCommentsViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
        }
    }
}
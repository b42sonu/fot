﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    partial class FormDamageType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.txtBoxDamageType = new System.Windows.Forms.TextBox();
            this.lblEarlierEvents = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblType = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtEarlierEvents = new System.Windows.Forms.TextBox();
            this.listDamageType = new System.Windows.Forms.ListBox();
            this.LabelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelInstructions = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.LabelOrgUnit);
            this.touchPanel.Controls.Add(this.txtBoxDamageType);
            this.touchPanel.Controls.Add(this.lblEarlierEvents);
            this.touchPanel.Controls.Add(this.lblType);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtEarlierEvents);
            this.touchPanel.Controls.Add(this.listDamageType);
            this.touchPanel.Controls.Add(this.labelInstructions);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;

            // 
            // labelInstructions
            // 
            this.labelInstructions.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelInstructions.Location = new System.Drawing.Point(21, 466);
            this.labelInstructions.Name = "labelInstructions";
            this.labelInstructions.Size = new System.Drawing.Size(407, 27);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            // 
            // transparentLabel1
            // 
            this.LabelOrgUnit.AutoSize = false;
            this.LabelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.LabelOrgUnit.Location = new System.Drawing.Point(0, 42);
            this.LabelOrgUnit.Name = "transparentLabel1";
            this.LabelOrgUnit.Size = new System.Drawing.Size(480, 33);
            this.LabelOrgUnit.Text = "Module Name";
            this.LabelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            // lblEarlierEvents
            // 
            this.lblEarlierEvents.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblEarlierEvents.Location = new System.Drawing.Point(21, 70);
            this.lblEarlierEvents.Name = "lblEarlierEvents";
            this.lblEarlierEvents.Size = new System.Drawing.Size(170, 27);

            // 
            // txtEarlierEvents
            // 
            this.txtEarlierEvents.BackColor = System.Drawing.Color.White;
            this.txtEarlierEvents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEarlierEvents.Location = new System.Drawing.Point(21, 105);
            this.txtEarlierEvents.Multiline = true;
            this.txtEarlierEvents.Name = "txtEarlierEvents";
            this.txtEarlierEvents.ReadOnly = true;
            this.txtEarlierEvents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEarlierEvents.Size = new System.Drawing.Size(438, 70);
            this.txtEarlierEvents.TabIndex = 1;
            // 
            // lblType
            // 
            this.lblType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblType.Location = new System.Drawing.Point(21, 183);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(186, 27);
            // 
            // txtBoxDamageType
            // 
            this.txtBoxDamageType.Location = new System.Drawing.Point(21, 218);
            this.txtBoxDamageType.Name = "txtBoxDamageType";
            this.txtBoxDamageType.Size = new System.Drawing.Size(438, 41);
            this.txtBoxDamageType.TabIndex = 6;
            this.txtBoxDamageType.KeyUp += TxtBoxDamageTypeKeyUp;
            // 
            // listDamageType
            // 
            this.listDamageType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.listDamageType.Location = new System.Drawing.Point(21, 256);
            this.listDamageType.Name = "listDamageType";
            this.listDamageType.Size = new System.Drawing.Size(438, 219);
            this.listDamageType.TabIndex = 10;
            this.listDamageType.SelectedIndexChanged += ListDamageTypeSelectedIndexChanged;
            
            // 
            // FormOperationDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDamageType";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblEarlierEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LabelOrgUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelInstructions)).EndInit();
            this.ResumeLayout(false);

        }



        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblType;
        private Resco.Controls.CommonControls.TransparentLabel lblEarlierEvents;
        private System.Windows.Forms.TextBox txtEarlierEvents;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private TextBox txtBoxDamageType;
        private ListBox listDamageType;
        private Resco.Controls.CommonControls.TransparentLabel LabelOrgUnit;
        private Resco.Controls.CommonControls.TransparentLabel labelInstructions;
    }
}
﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View
{
    public partial class FormMeasure : BaseForm
    {
        private FormDataDamageType _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonOkName = "buttonOk";
        private CauseMeasure _selectedMeasure;
        private List<CauseMeasure> _causeMeasureTypeFilterlist;

        public FormMeasure()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            SetStandardControlProperties(labelModuleName, LabelOrgUnit, null, null, null, null, null);
        }

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.RegisterDamage;
            lblLabelMeasure.Text = GlobalTexts.Measure+Colon;
            lblLabelCause.Text = GlobalTexts.Cause;
            lblType.Text = GlobalTexts.DamageType;
            LabelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            labelInstructions.Text = GlobalTexts.SelectActionInstruction;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonCancelClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOkName });
            _multiButtonControl.GenerateButtons();
        }

        public override void OnShow(IFormData formData)
        {
            Visible = true;
            Show(BaseModule.MainForm);
            _formData = (FormDataDamageType)formData;
            if (_formData.Damage.DamageCode.Description != null)
            {
                lblLabelCauseText.Text = _formData.Damage.CauseMeasure.CauseCodeAndCauseDescription;
                lblDamageTypeValue.Text = _formData.Damage.DamageCode.Code + " " + _formData.Damage.DamageCode.Description; 
                lblEarlierEvents.Text = _formData.HeaderEarlierEvents;
                txtEarlierEvents.Text = _formData.EarlierEvents;
                BindMeasures();
                EnableDiableButtons();
                SelectFirstElement();

            }
            //SetKeyboardState(KeyboardState.Alpha);
        }
        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataDamageType)iformData;
            if (listMeasure.Items.Count != 1 || _formData.Damage.CauseMeasure.MeasureCode != null) return;
            listMeasure.SelectedIndex = 0;
            SetCauseValue(_formData.Damage.CauseMeasure, (CauseMeasure)listMeasure.SelectedItem);
            ViewEvent.Invoke(SelectdMeasureViewEvents.Ok, _formData.Damage);
        }

        private void SelectFirstElement()
        {
            if (listMeasure.SelectedItem == null)
            {
                _selectedMeasure = (CauseMeasure) listMeasure.SelectedItem;
                if (_selectedMeasure != null) txtBoxMeasure.Text = _selectedMeasure.MeasureCodeAndMeasureDescription;
                listMeasure.SelectedIndex = 0;
                EnableDiableButtons();
            }
            else
            {
                if (_formData.IsPrevious && !string.IsNullOrEmpty(_formData.Damage.CauseMeasure.MeasureCode))
                {
                    listMeasure.SelectedValue = _formData.Damage.CauseMeasure.MeasureCode;
                }
                else
                {
                    if (string.IsNullOrEmpty(_formData.Damage.CauseMeasure.MeasureCode) && !string.IsNullOrEmpty(_formData.Damage.CauseMeasure.MeasureCodeAndMeasureDescription) )
                        listMeasure.SelectedIndex = 0;
                }
            }
        }

        public override void ShowingView()
        {
            base.ShowingView();
            listMeasure.Focus();
        }

        private void EnableDiableButtons()
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOkName, (listMeasure.Items.Count != 0) && listMeasure.SelectedIndex >= 0);
        }

        private void BindMeasures()
        {
            if (_formData.Damage.CauseMeasure.MeasureCodeAndMeasureDescription == null){

                 _causeMeasureTypeFilterlist =
                    (from n in _formData.CauseMeasureList where n.CauseCode == _formData.Damage.CauseMeasure.CauseCode select n)
                        .ToList<CauseMeasure>();
                listMeasure.ValueMember = "MeasureCode";
                listMeasure.DisplayMember = "MeasureCodeAndMeasureDescription"; 
                listMeasure.DataSource = _causeMeasureTypeFilterlist;
                txtBoxMeasure.Text = string.Empty;
               listMeasure.SelectedIndex = -1;
            }
            
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                _formData.Damage.CauseMeasure.MeasureCodeAndMeasureDescription = null;
                _formData.Damage.CauseMeasure.MeasureCode = null;
                _formData.Damage.CauseMeasure.MeasureDescription = null;
                listMeasure.SelectedIndex = -1;
                ViewEvent.Invoke(SelectdMeasureViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonCancelClick");
            }
        }
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                SetCauseValue(_formData.Damage.CauseMeasure, (CauseMeasure)listMeasure.SelectedItem);
                ViewEvent.Invoke(SelectdMeasureViewEvents.Ok, _formData.Damage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDamageType.ButtonOkClick");
            }
        }
        private static void SetCauseValue(CauseMeasure causeMeasure, CauseMeasure selectedCauseMeasure)
        {
            if (selectedCauseMeasure == null || causeMeasure == null) return;
            causeMeasure.MeasureCode = selectedCauseMeasure.MeasureCode;
            causeMeasure.MeasureCodeAndMeasureDescription = selectedCauseMeasure.MeasureCodeAndMeasureDescription;
            causeMeasure.MeasureDescription = selectedCauseMeasure.MeasureDescription;
        }

        private void ListMeasureSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (listMeasure.SelectedItem == null) return;
                _selectedMeasure = (CauseMeasure)listMeasure.SelectedItem;
                txtBoxMeasure.Text = _selectedMeasure.MeasureCodeAndMeasureDescription;
                EnableDiableButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMeasure.ListMeasureSelectedIndexChanged");
            }
        }

        private void TxtMeasureKeyUp(object sender, KeyEventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(txtBoxMeasure.Text.Trim()))
                {
                    {
                        var enteredText = txtBoxMeasure.Text.Trim().ToLower();
                        var filteredActions =
                            (from c in _causeMeasureTypeFilterlist
                             where c.MeasureCodeAndMeasureDescription.ToLower().StartsWith(enteredText)
                             select c).ToList();
                        filteredActions.AddRange(
                            (from c in _causeMeasureTypeFilterlist
                             where
                             ((c.MeasureCodeAndMeasureDescription.ToLower().Contains(enteredText)) && !(c.MeasureCodeAndMeasureDescription.ToLower().StartsWith(enteredText)))
                             select c).ToList()
                            );
                        listMeasure.SelectedIndexChanged -= ListMeasureSelectedIndexChanged;
                        listMeasure.DataSource = filteredActions;
                        listMeasure.SelectedIndex = -1;
                        EnableDiableButtons();
                        listMeasure.SelectedIndexChanged += ListMeasureSelectedIndexChanged;
                    }
                }
                else
                {
                    BindMeasures();
                    EnableDiableButtons();
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMeasure.TxtMeasureKEyUp");
            }
        }

        public class SelectdMeasureViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
        }
    }
}
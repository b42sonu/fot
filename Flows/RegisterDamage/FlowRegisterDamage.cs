﻿using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.Camera;
using Com.Bring.PMP.PreComFW.Shared.Flows.ConfirmConsignmentItemCount;
using Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.Command;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SetSignPriority;
using Process = Com.Bring.PMP.PreComFW.Shared.Storage.Process;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage
{


    public enum FlowStateRegisterDamage
    {
        ActionValidateGoodsToGetEarlierEvents,
        ActionValidateEnteredDamageInfomation,
        ActionValidateScan,
        ActionIsItemScannedBefore,
        ActionIsCurrentScanningConsignmentItem,
        ActionIsSingleEventVerificationRequired,
        ActionNewDamage,
        ActionConfirmConsignment,
        ActionConfirmNextConsignment,
        ActionVerifiedUnsignedEvents,
        ActionSendGoodsEventForSingleEvent,
        ActionSendGoodsEventForAllEvents,
        ActionRemoveConfirmedAndSignedDamage,
        ActionBackFromFlowConfirmConsignmentItemCount,
        ActionBackFromSignatureForSingelEvent,
        ActionBackFromSignatureForReconciliationList,
        ActionBackFromFlowReconcilliation,
        ActionBackFromCameraFlow,
        ActionStoreItemInCacheFromGoodsMenu,
        ActionIsDamageSignatureRequired,
        ActionCreateConsignmentEntity,
        ActionIsConsignmentRegistrationAllowed,
        ActionPerformProcessRelatedValidation,
        ActionCheckOnlineConsignmentNumberMissingAndRequired,
        ActionCheckOfflineConsignmentNumberMissingAndRequired,
        ActionBackFromFlowScanConsignment,
        ActionTypeOfObjectScanned,
        ActionValidateGoods,
        ActionBackFromFlowRegisterWaybill,
        ViewCommands,
        ViewUpdateFormDamageInformation,
        ViewUpdateFormDamageScan,
        ViewRegisterDamageInformation,
        ViewShowRegisterDamageScan,
        ViewEventVerification,
        ViewSelectCause,
        ViewSelectMeasure,
        ViewEnterComments,
        FlowSignatureForSingelEvent,
        FlowSignatureForAllEvent,
        FlowConfirmConsignmentItemCount,
        FlowReconcilliation,
        FlowBackToReconcilliation,
        FlowRegisterWaybill,
        FlowCamera,
        FlowScanConsignment,
        FlowCanceled,
        FlowComplete,


    }

    public class FlowDataRegisterDamage : BaseFlowData
    {
        //The flow will go another path when the damage comes from correction menu
        public bool FromCorrectionMenu { get { return DamageEntity != null; } }

        //This is set when entering from Correction menu
        public ConsignmentEntity DamageEntity { get; set; }

        public EntityMap EntityMap { get; set; }
    }

    public class FlowResultRegisterDamage : BaseFlowResult
    { }

    // US 33 - http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7932739
    public class FlowRegisterDamage : BaseFlow
    {
        private readonly ViewCommandsRegisterDamage _viewCommand;
        private readonly ActionCommandsRegisterDamage _actionCommand;
        private readonly ActionCommandsScanBarcode _actionCommandsScanBarcode;
        private readonly FlowResultRegisterDamage _flowResult;

        private FormDamageScan _formDamageScan;
        private MessageHolder _messageHolder;
        private FlowDataRegisterDamage _flowData;
        private string _headerEarlierEvents;
        private string _earlierEvents;
        //Two current damage incase user click back and want to continue register consignment entity for damage
        // For adding data to damage
        private ConsignmentEntity _currentConsignmentEntity;
        // For confirming damage
        private ConsignmentEntity _currentConfirmedConsignmentEntity;

        private FotScannerOutput _scannerOutput;
        private EntityMap _entityMap;
        private readonly String _flowName = GlobalTexts.RegisterDamage;

        private BarcodeData _currentScanning;
        private Damage _currentDamage;
        private readonly CauseMeasure _causeMeasure;

        private FormDataDamageScan _currentFormDataDamageScan;
        private FormDataDamageScan _previousFormData;
        private EntityMap _previousEntityMap;
        private FormDataDamageScan _previousFormDataDamageScan;
        private bool _hasDamages, _isPrevious;
        public FlowRegisterDamage()
        {
            _messageHolder = new MessageHolder();
            _viewCommand = new ViewCommandsRegisterDamage();
            _actionCommand = new ActionCommandsRegisterDamage(CommunicationClient.Instance, _messageHolder);
            _actionCommandsScanBarcode = new ActionCommandsScanBarcode(_messageHolder);
            _entityMap = new EntityMap();
            _flowResult = new FlowResultRegisterDamage();
            _currentDamage = new Damage();
            _causeMeasure = new CauseMeasure();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing FlowRegisterDamage");
            _flowData = (FlowDataRegisterDamage)flowData;
            _currentDamage.CauseMeasure = _causeMeasure;
            if (_flowData.DamageEntity != null)
            {
                var consignmentItem1 = _flowData.DamageEntity as ConsignmentItem;
                if (consignmentItem1 != null)
                {
                    _currentConsignmentEntity = consignmentItem1.Clone();
                    _currentConsignmentEntity.ConsignmentInstance.EntityStatus = EntityStatus.Unknown;
                }
                var consignment = _flowData.DamageEntity as Consignment;
                if (consignment != null)
                {
                    _currentConsignmentEntity = consignment.Clone();
                    foreach (var consignmentItem in _currentConsignmentEntity.CastToConsignment().ConsignmentItemsMap)
                    {
                        consignmentItem.Value.EntityStatus = EntityStatus.Unknown;
                    }
                }

            }

            var initialState = _flowData.FromCorrectionMenu
                                   ? FlowStateRegisterDamage.ActionValidateGoodsToGetEarlierEvents
                                   : FlowStateRegisterDamage.ViewRegisterDamageInformation;
            ExecuteState(initialState);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateRegisterDamage)state);
        }

        public void ExecuteState(FlowStateRegisterDamage state)
        {

            if (state > FlowStateRegisterDamage.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);

        }

        private void ExecuteViewState(FlowStateRegisterDamage state)
        {

            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowStateRegisterDamage;" + state.ToString());
                _hasDamages = _entityMap.Values.Count != 0;
                switch (state)
                {
                    case FlowStateRegisterDamage.FlowScanConsignment:
                        var flowDataScanConsignment = new FlowDataScanConsignment
                        {
                            HeaderText = GlobalTexts.RegisterDamage,
                            ConsignmentItem = _currentConsignmentEntity.CastToConsignmentItem()
                        };

                        StartSubFlow<FlowScanConsignment>(flowDataScanConsignment, (int)FlowStateRegisterDamage.ActionBackFromFlowScanConsignment, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.ViewRegisterDamageInformation:
                        _viewCommand.FormDamageInformation(DamageTypeViewEventHandler, _currentDamage, _currentConsignmentEntity, _flowData.FromCorrectionMenu, _isPrevious);
                        break;

                    case FlowStateRegisterDamage.ViewSelectCause:
                        _viewCommand.FormSelectCause(SelectCauseEventHandler, _currentDamage, _headerEarlierEvents, _earlierEvents, _isPrevious);
                        break;

                    case FlowStateRegisterDamage.ViewSelectMeasure:
                        _viewCommand.FormSelectMeasure(SelectMeasureEventHandler, _currentDamage, _headerEarlierEvents, _earlierEvents,_isPrevious);
                        break;

                    case FlowStateRegisterDamage.ViewEnterComments:
                        _viewCommand.FormEnterComments(EnterCommentsEventHandler, _currentDamage, _headerEarlierEvents, _earlierEvents);
                        break;

                    case FlowStateRegisterDamage.ViewShowRegisterDamageScan:
                        _formDamageScan = _viewCommand.ShowFormDamageScan(DamageScanEventHandler, _currentConsignmentEntity, _currentDamage,
                            _flowData.FromCorrectionMenu, _hasDamages, _headerEarlierEvents, _earlierEvents, _entityMap.ScannedConsignmentItemsTotalCount, _messageHolder, out _currentFormDataDamageScan);
                        break;

                    case FlowStateRegisterDamage.ViewUpdateFormDamageScan:
                        IFormData formData = new FormDataDamageScan
                        {
                            ConsignmentEntity = _currentConsignmentEntity,
                            Damage = _currentDamage,
                            MessageHolder = _messageHolder,
                            FromCorrectionMenu = _flowData.FromCorrectionMenu,
                            HasDamages = _hasDamages,
                            HeaderEarlierEvents = _headerEarlierEvents,
                            EarlierEvents = _earlierEvents,
                            ScannedEntityCount = _entityMap.ScannedConsignmentItemsTotalCount,
                        };
                        _currentFormDataDamageScan = (FormDataDamageScan)formData;
                        _formDamageScan.UpdateView(formData);
                        break;

                    case FlowStateRegisterDamage.ViewEventVerification:
                        _viewCommand.FormEventVerification(EventVerificationEventHandler, _currentConfirmedConsignmentEntity);
                        break;

                    case FlowStateRegisterDamage.FlowSignatureForSingelEvent:
                        var flowDataSignature = new FlowDataSignature(_flowName, _currentConfirmedConsignmentEntity.Damage.CountForSingelDamage())
                        {
                            SignatureOwnerTitle = GlobalTexts.Name,
                        };
                        StartSubFlow<FlowSignature>(flowDataSignature, (int)FlowStateRegisterDamage.ActionBackFromSignatureForSingelEvent, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowSignatureForAllEvent:
                        flowDataSignature = new FlowDataSignature(_flowName, _entityMap, SignaturePriority.SignatureAndName)
                        {
                            SignatureOwnerTitle = GlobalTexts.Name
                        };
                        MakeSureSignatureViewIsShowingWhileSendingGoodsEvent();
                        StartSubFlow<FlowSignature>(flowDataSignature, (int)FlowStateRegisterDamage.ActionBackFromSignatureForReconciliationList, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowConfirmConsignmentItemCount:
                        var flowDataConfirmConsignmentItemCount = new FlowDataConfirmConsignmentItemCount
                        {
                            HeaderText = _flowName,
                            Consignment = _currentConsignmentEntity.CastToConsignment(),
                        };
                        StartSubFlow<FlowConfirmConsignmentItemCount>(flowDataConfirmConsignmentItemCount, (int)FlowStateRegisterDamage.ActionBackFromFlowConfirmConsignmentItemCount, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowReconcilliation:
                        var flowDataReconciliation = new FlowDataReconciliation
                        {
                            EntityMap = _entityMap,
                            HeaderText = _flowName,
                            OkButtonDisableWhenNoItemsInList = true,
                        };
                        StartSubFlow<FlowReconciliation>(flowDataReconciliation, (int)FlowStateRegisterDamage.ActionBackFromFlowReconcilliation, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowBackToReconcilliation:
                        flowDataReconciliation = new FlowDataReconciliation
                        {
                            EntityMap = _entityMap,
                            HeaderText = _flowName,
                            OkButtonDisableWhenNoItemsInList = true,
                            IsReturningToReconcilliation = true
                        };
                        StartSubFlow<FlowReconciliation>(flowDataReconciliation, (int)FlowStateRegisterDamage.ActionBackFromFlowReconcilliation, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowRegisterWaybill:
                        var flowDataRegisterWaybill = new FlowDataRegisterWaybill
                        {
                            ConsignmentItem = _currentConsignmentEntity as ConsignmentItem,
                            HeaderText = _flowName,
                            IsCalledFromExternalFlow = true
                        };
                        StartSubFlow<FlowRegisterWaybill>(flowDataRegisterWaybill, (int)FlowStateRegisterDamage.ActionBackFromFlowRegisterWaybill, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowCamera:
                        var flowData = new FlowDataCamera
                        {
                            HeaderText = _flowName,
                            FolderName = _currentConsignmentEntity.Damage.FolderName,
                            CameraFunctionalityFor = GlobalTexts.Damage,
                            ConsignmentItemNumber = _currentConsignmentEntity.EntityId,
                            ConsignmentItemType = _currentConsignmentEntity is ConsignmentItem ? GlobalTexts.ConsignmentItem : GlobalTexts.Consignment,
                            ConsignmentDisplayItemNumber = StringUtil.GetTextForConsignmentEntity(_currentConsignmentEntity.EntityDisplayId, false)
                        };
                        StartSubFlow<FlowCamera>(flowData, (int)FlowStateRegisterDamage.ActionBackFromCameraFlow, Process.Inherit);
                        break;

                    case FlowStateRegisterDamage.FlowCanceled:
                        _flowResult.State = FlowResultState.Error;
                        _flowResult.Message = GlobalTexts.RegisterDamageCancelled;
                        _headerEarlierEvents = string.Empty;
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    case FlowStateRegisterDamage.FlowComplete:
                        _flowResult.State = FlowResultState.Ok;
                        if (_currentConsignmentEntity != null)
                            _flowResult.Message = string.Format(GlobalTexts.DamageRegisteredForItem, _currentConsignmentEntity.EntityDisplayId);

                        _headerEarlierEvents = string.Empty;
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "FlowRegisterDamage.ExecuteViewState: Encountered unhandled state");
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowRegisterDamage.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateRegisterDamage.FlowComplete);
            }
        }

        private void MakeSureSignatureViewIsShowingWhileSendingGoodsEvent()
        {
            CurrentView.AutoShow = false;
        }


        public void ExecuteActionState(FlowStateRegisterDamage state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowStateRegisterDamage;" + state.ToString());
                    switch (state)
                    {
                        case FlowStateRegisterDamage.ActionValidateGoodsToGetEarlierEvents:
                            _actionCommand.ValidateGoodsFromCorrectionMenu(_flowData.FromCorrectionMenu, _actionCommandsScanBarcode, _entityMap, _currentConsignmentEntity);
                            state = FlowStateRegisterDamage.ViewRegisterDamageInformation;
                            break;

                        case FlowStateRegisterDamage.ActionValidateEnteredDamageInfomation:
                            bool result = _actionCommand.ValidateDamage(_currentDamage);
                            state = result ? FlowStateRegisterDamage.ViewShowRegisterDamageScan : FlowStateRegisterDamage.ViewUpdateFormDamageInformation;
                            break;

                        //States that are part of user story 33

                        case FlowStateRegisterDamage.ActionValidateScan:
                            result = _actionCommandsScanBarcode.ValidateBarcode(_scannerOutput, BarcodeType.Shipment, out _currentScanning, false);
                            state = result
                                        ? FlowStateRegisterDamage.ActionCreateConsignmentEntity
                                        : FlowStateRegisterDamage.ViewUpdateFormDamageScan;
                            break;

                        case FlowStateRegisterDamage.ActionCreateConsignmentEntity:
                            _currentConsignmentEntity =
                             ActionCommandsScanBarcode.CreateConsignmentEntity(_currentScanning);
                            _currentConsignmentEntity.Damage = _currentDamage.Clone();
                            state = FlowStateRegisterDamage.ActionIsItemScannedBefore;
                            break;

                        case FlowStateRegisterDamage.ActionIsItemScannedBefore:
                            result = _actionCommand.IsScannedBefore(_entityMap, ref _currentConsignmentEntity);
                            state = result
                                        ? FlowStateRegisterDamage.ViewUpdateFormDamageScan
                                        : FlowStateRegisterDamage.ActionIsCurrentScanningConsignmentItem;
                            break;

                        case FlowStateRegisterDamage.ActionIsCurrentScanningConsignmentItem:
                            result = _currentConsignmentEntity is ConsignmentItem;
                            state = result
                                        ? FlowStateRegisterDamage.ActionValidateGoods
                                        : FlowStateRegisterDamage.ActionIsConsignmentRegistrationAllowed;
                            break;

                        case FlowStateRegisterDamage.ActionIsConsignmentRegistrationAllowed:
                            result = _actionCommand.IsConsignmentRegistrationAllowed(_currentConsignmentEntity);
                            state = result ? FlowStateRegisterDamage.ActionValidateGoods : FlowStateRegisterDamage.ViewUpdateFormDamageScan;
                            break;

                        case FlowStateRegisterDamage.ActionValidateGoods:
                            state = _actionCommand.ValidateGoods(_entityMap, _currentConsignmentEntity, _actionCommandsScanBarcode, _flowResult, _flowData);

                            break;

                        case FlowStateRegisterDamage.ActionPerformProcessRelatedValidation:
                            state = _actionCommand.PerformProcessRelatedValidation(_currentConsignmentEntity);
                            break;

                        case FlowStateRegisterDamage.ActionCheckOnlineConsignmentNumberMissingAndRequired:
                            state = _actionCommand.CheckOnlineConsignmentNumberKnownOrNotRequired(_currentConsignmentEntity);
                            //state = result ? FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu : FlowStateRegisterDamage.FlowScanConsignment;
                            break;

                        case FlowStateRegisterDamage.ActionCheckOfflineConsignmentNumberMissingAndRequired:
                            //  result = _actionCommand.CheckConsignmentNumberRequired(_currentConsignmentEntity);
                            //  state = result ? FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu : FlowStateRegisterDamage.FlowScanConsignment;
                            state = _actionCommand.CheckOfflineConsignmentNumberKnownOrNotRequired(_currentConsignmentEntity);
                            break;


                        case FlowStateRegisterDamage.ActionTypeOfObjectScanned:
                            state = _currentConsignmentEntity is ConsignmentItem
                                        ? FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu
                                        : FlowStateRegisterDamage.FlowConfirmConsignmentItemCount;
                            break;

                        case FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu:
                            _actionCommand.StoreDamageAndItemInLocalCache(_currentConsignmentEntity, _entityMap, _currentDamage);
                            state = FlowStateRegisterDamage.ViewUpdateFormDamageScan;
                            break;

                        case FlowStateRegisterDamage.ActionIsSingleEventVerificationRequired:
                            result = SettingDataProvider.Instance.GetTmiValue<bool>((TmiSettings.IsSingleEventVerificationRequired));
                            state = result
                                        ? FlowStateRegisterDamage.ActionConfirmNextConsignment
                                        : FlowStateRegisterDamage.ActionIsDamageSignatureRequired;
                            break;

                        case FlowStateRegisterDamage.ActionIsDamageSignatureRequired:
                            result = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsDamageSignatureRequired);
                            state = result
                                        ? FlowStateRegisterDamage.FlowReconcilliation
                                        : FlowStateRegisterDamage.ActionSendGoodsEventForAllEvents;
                            break;

                        case FlowStateRegisterDamage.ActionNewDamage:
                            _currentDamage = new Damage { CauseMeasure = new CauseMeasure() };
                            //_entityMap.Clear();
                            state = FlowStateRegisterDamage.ViewRegisterDamageInformation;
                            break;

                        case FlowStateRegisterDamage.ActionConfirmConsignment:
                            _actionCommand.ConfirmDamage(_currentConfirmedConsignmentEntity.Damage);
                            state = FlowStateRegisterDamage.ActionConfirmNextConsignment;
                            break;

                        case FlowStateRegisterDamage.ActionConfirmNextConsignment:
                            result = _actionCommand.HasMoreDamageToConfirm(_entityMap, out _currentConfirmedConsignmentEntity);
                            state = result ? FlowStateRegisterDamage.ViewEventVerification : FlowStateRegisterDamage.ActionVerifiedUnsignedEvents;
                            break;

                        case FlowStateRegisterDamage.ActionVerifiedUnsignedEvents:
                            result = _actionCommand.HasVerifiedUnsignedEvents(_entityMap);
                            state = result ? FlowStateRegisterDamage.FlowReconcilliation : FlowStateRegisterDamage.FlowComplete;
                            break;

                        case FlowStateRegisterDamage.ActionSendGoodsEventForSingleEvent:
                            _actionCommand.SendGoodsEvent(_currentConfirmedConsignmentEntity);
                            state = FlowStateRegisterDamage.ActionRemoveConfirmedAndSignedDamage;
                            break;

                        case FlowStateRegisterDamage.ActionRemoveConfirmedAndSignedDamage:
                            _actionCommand.RemoveDamage(_currentConfirmedConsignmentEntity, _entityMap);
                            state = FlowStateRegisterDamage.ActionConfirmConsignment;
                            break;

                        case FlowStateRegisterDamage.ActionSendGoodsEventForAllEvents:
                            if (_flowData.FromCorrectionMenu)
                            {
                                _actionCommand.SendGoodsEvent(_currentConsignmentEntity);
                            }
                            else
                            {
                                _actionCommand.SendGoodsEvents(_flowData, _entityMap);
                            }
                            state = FlowStateRegisterDamage.FlowComplete;
                            break;

                        case FlowStateRegisterDamage.ActionBackFromFlowReconcilliation:
                            result = _actionCommand.BackFromFlowReconcilliation((FlowResultReconciliation)SubflowResult, ref _entityMap, ref _currentConsignmentEntity);
                            ClearMessageHolder();
                            if (_flowData.FromCorrectionMenu)
                            {
                                if (result == false)
                                {
                                    state = FlowStateRegisterDamage.ViewShowRegisterDamageScan;
                                }
                                else if (_currentConsignmentEntity != null && result)
                                {
                                    state = FlowStateRegisterDamage.FlowSignatureForAllEvent;
                                }
                                else
                                    state = FlowStateRegisterDamage.FlowComplete;
                               
                            }
                            else
                            {
                                state = result == false? FlowStateRegisterDamage.ViewShowRegisterDamageScan
                                            : (_entityMap.HasScannedEntities? FlowStateRegisterDamage.FlowSignatureForAllEvent: FlowStateRegisterDamage.FlowComplete);
                            }
                            break;

                        case FlowStateRegisterDamage.ActionBackFromCameraFlow:
                            _actionCommand.BackFromFlowCamera((FlowResultCamera)SubflowResult, _currentConsignmentEntity.Damage);
                            state = FlowStateRegisterDamage.ViewShowRegisterDamageScan;//ViewUpdateFormDamageScan;
                            break;

                        case FlowStateRegisterDamage.ActionBackFromSignatureForSingelEvent:
                            result = _actionCommand.BackFromSignatureForSingelEvent(_currentConfirmedConsignmentEntity, (FlowResultSignature)SubflowResult);
                            state = result ? FlowStateRegisterDamage.ActionSendGoodsEventForSingleEvent : FlowStateRegisterDamage.ViewEventVerification;
                            break;

                        case FlowStateRegisterDamage.ActionBackFromSignatureForReconciliationList:
                            result = _actionCommand.BackFromSignatureForReconciliationList((FlowResultSignature)SubflowResult, _entityMap);
                            state = result ? FlowStateRegisterDamage.ActionSendGoodsEventForAllEvents : FlowStateRegisterDamage.FlowBackToReconcilliation;
                            CurrentView.AutoShow = true;
                            break;

                        case FlowStateRegisterDamage.ActionBackFromFlowConfirmConsignmentItemCount:
                            result = _actionCommand.StoreConsignmentItemCount(_currentConsignmentEntity.Damage, (FlowResultConfirmConsignmentItemCount)SubflowResult);
                            if (result == false)
                                _currentConsignmentEntity = null;
                            state = result ? FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu : FlowStateRegisterDamage.ViewUpdateFormDamageScan;
                            break;

                        case FlowStateRegisterDamage.ActionBackFromFlowScanConsignment:
                            result = _actionCommand.ValidateScanConsignmentResult((FlowResultScanConsignment)SubflowResult, _currentConsignmentEntity.CastToConsignmentItem());
                            _flowResult.State = SubflowResult.State;
                            state = result ? FlowStateRegisterDamage.ActionTypeOfObjectScanned : FlowStateRegisterDamage.ViewUpdateFormDamageScan;
                            break;

                        case FlowStateRegisterDamage.ActionBackFromFlowRegisterWaybill:
                            result = _actionCommand.ValidateRegisterWaybillResult((FlowResultRegisterWaybill)SubflowResult,
                                    _currentConsignmentEntity.CastToConsignmentItem(), ref  _messageHolder);//, ref isRegisterWaybillSkippedForPickup);

                            state = result ? FlowStateRegisterDamage.ActionStoreItemInCacheFromGoodsMenu : FlowStateRegisterDamage.FlowScanConsignment;
                            break;

                        default:
                            Logger.LogEvent(Severity.Error, "FlowRegisterDamage.ExecuteActionState: Encountered unhandled state");
                            break;
                    }
                } while (state < FlowStateRegisterDamage.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Failed to execute ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateRegisterDamage.FlowComplete;
            }
            ExecuteViewState(state);
        }
        private void ClearMessageHolder()
        {
            _messageHolder.Clear();
        }
        private void CloneCurrentDamageForNewConsignmentEntity()
        {
            _currentDamage = _currentDamage.Clone();
            _currentDamage.ResetDamageForNewScan();
        }

        #region EventHandlers

        private void DamageTypeViewEventHandler(int viewEvents, params object[] data)
        {
            switch (viewEvents)
            {
                case FormDamageType.DamageInfoViewEvents.Back:
                    if (_previousFormData != null)
                    {
                        _currentConsignmentEntity = _previousFormData.ConsignmentEntity;
                        _currentDamage = _previousFormData.Damage;
                        _hasDamages = _previousFormData.HasDamages;
                        _headerEarlierEvents = _previousFormData.HeaderEarlierEvents;
                        _earlierEvents = _previousFormData.EarlierEvents;
                        _entityMap = _previousEntityMap;
                        _messageHolder.Update(_previousFormData.MessageHolder.Text, _previousFormData.MessageHolder.State);
                        _currentFormDataDamageScan = _previousFormDataDamageScan;
                        _isPrevious = true;
                        _previousFormData = null;
                        ExecuteState(FlowStateRegisterDamage.ViewShowRegisterDamageScan);
                    }
                    else
                    {
                        _currentDamage.DamageCode = null;
                        _currentDamage.CauseMeasure = null;
                        ExecuteState(FlowStateRegisterDamage.FlowCanceled);
                    }
                    break;

                case FormDamageType.DamageInfoViewEvents.Ok:
                    _currentDamage = (Damage)data[0];
                    _headerEarlierEvents = Convert.ToString(data[1]);
                    _earlierEvents = Convert.ToString(data[2]);
                    if (_earlierEvents == "")
                        _earlierEvents = !_flowData.FromCorrectionMenu ? GlobalTexts.NoInformationAboutItemAvailable : GlobalTexts.NoEarlierDamageAvailable;
                    ExecuteState(FlowStateRegisterDamage.ViewSelectCause);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "FlowRegisterDamage.DamageInformationViewEventHandler: Encountered unhandled state");
                    break;
            }
        }

        private void DamageScanEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case FormDamageScan.DamageScanViewEvents.Back:
                    if (!_flowData.FromCorrectionMenu)
                    {
                        //_currentConsignmentEntity = null;
                        CloneCurrentDamageForNewConsignmentEntity();
                    }
                    ClearMessageHolder();
                    ExecuteState(FlowStateRegisterDamage.ViewEnterComments);
                    break;

                case FormDamageScan.DamageScanViewEvents.Ok:
                    if (_flowData.FromCorrectionMenu)
                    {
                        ExecuteState(FlowStateRegisterDamage.ActionIsSingleEventVerificationRequired);
                    }
                    else
                    {
                        CloneCurrentDamageForNewConsignmentEntity();
                        ExecuteState(FlowStateRegisterDamage.ActionIsSingleEventVerificationRequired);
                    }

                    break;

                case FormDamageScan.DamageScanViewEvents.BarcodeScanned:
                    if (_flowData.FromCorrectionMenu == false)
                        CloneCurrentDamageForNewConsignmentEntity();
                    _scannerOutput = (FotScannerOutput)data[0];
                    ExecuteState(FlowStateRegisterDamage.ActionValidateScan);
                    break;

                case FormDamageScan.DamageScanViewEvents.NewDamage:
                    _previousFormData = new FormDataDamageScan
                                        {
                                            ConsignmentEntity = _currentFormDataDamageScan.ConsignmentEntity,
                                            Damage = _currentFormDataDamageScan.Damage.Clone(),
                                            FromCorrectionMenu = _currentFormDataDamageScan.FromCorrectionMenu,
                                            HasDamages = _currentFormDataDamageScan.HasDamages,
                                            HeaderEarlierEvents = _currentFormDataDamageScan.HeaderEarlierEvents,
                                            EarlierEvents = _currentFormDataDamageScan.EarlierEvents,
                                            ScannedEntityCount = _currentFormDataDamageScan.ScannedEntityCount,
                                            MessageHolder = new MessageHolder(_currentFormDataDamageScan.MessageHolder.Text, _currentFormDataDamageScan.MessageHolder.State),
                                            ScanText = _currentFormDataDamageScan.ScanText
                                        };
                    _previousEntityMap = _entityMap.Clone();
                    _previousFormDataDamageScan = _currentFormDataDamageScan;
                    _currentConsignmentEntity = null;
                    ClearMessageHolder();
                    ExecuteState(FlowStateRegisterDamage.ActionNewDamage);
                    break;

                case FormDamageScan.DamageScanViewEvents.TakePhoto:
                    ExecuteState(FlowStateRegisterDamage.FlowCamera);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "FlowRegisterDamage.DamageScanEventHandler: Encountered unhandled state");
                    break;
            }
        }

        private void SelectCauseEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case FormCause.SelectCauseViewEvents.Back:
                    ExecuteState(FlowStateRegisterDamage.ViewRegisterDamageInformation);
                    break;

                case FormCause.SelectCauseViewEvents.Ok:
                    _currentDamage = (Damage)data[0];
                    ExecuteState(FlowStateRegisterDamage.ViewSelectMeasure);
                    break;
            }
        }
        private void SelectMeasureEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case FormCause.SelectCauseViewEvents.Back:
                    ExecuteState(FlowStateRegisterDamage.ViewSelectCause);
                    break;

                case FormCause.SelectCauseViewEvents.Ok:
                    _currentDamage = (Damage)data[0];
                    ExecuteState(FlowStateRegisterDamage.ViewEnterComments);
                    break;
            }
        }
        private void EnterCommentsEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case FormDamageComments.EnterCommentsViewEvents.Back:
                    ExecuteState(FlowStateRegisterDamage.ViewSelectMeasure);
                    break;

                case FormDamageComments.EnterCommentsViewEvents.Ok:
                    _currentDamage = (Damage)data[0];
                    //Set damage because consignment entity is already scanned
                    if (_flowData.FromCorrectionMenu)
                    {
                        _currentConsignmentEntity.Damage = _currentDamage;
                        //Store in cache right away as  the entityNumber is already valid
                        _entityMap.Store(_currentConsignmentEntity);
                        if (_currentConsignmentEntity is Consignment)
                        {
                            _currentConsignmentEntity.Damage.ConsignmentItemCount = _flowData.EntityMap.GetItemCountFromConsignment(_currentConsignmentEntity.ConsignmentId);
                        }
                    }
                    ExecuteState(FlowStateRegisterDamage.ActionValidateEnteredDamageInfomation);
                    break;
            }
        }

        private void EventVerificationEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case FormEventVerification.EventVerificationviewEvent.Back:
                    ExecuteState(FlowStateRegisterDamage.ViewShowRegisterDamageScan);
                    break;

                case FormEventVerification.EventVerificationviewEvent.Confirm:
                    ExecuteState(FlowStateRegisterDamage.ActionConfirmConsignment);
                    break;

                case FormEventVerification.EventVerificationviewEvent.Signature:
                    ExecuteState(FlowStateRegisterDamage.FlowSignatureForSingelEvent);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "FlowRegisterDamage.EventVerificationEventHandler: Encountered unhandled state");
                    break;
            }
        }
        #endregion


    }
}

﻿using System.Windows.Forms;
using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.StopDetailsAmphora
{
    partial class FormOperationDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOperationDetail));
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.webBrowser = new FotWebBrowser();
            this.advancedList = new Resco.Controls.AdvancedList.AdvancedList();
            this.labelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.textCellConsignment = new Resco.Controls.AdvancedList.TextCell();
            this.textCellPI = new Resco.Controls.AdvancedList.TextCell();
            this.SelectedTemplate = new Resco.Controls.AdvancedList.RowTemplate();
            this.TextCell3 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell4 = new Resco.Controls.AdvancedList.TextCell();
            this.UnselectedTemplate = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell5 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedList.TextCell();
            this.RowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeading)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.webBrowser);
            this.touchPanel.Controls.Add(this.advancedList);
            this.touchPanel.Controls.Add(this.labelOrgUnit);
            this.touchPanel.Controls.Add(this.labelHeading);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(21, 215);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(438, 232);
            // 
            // advancedList
            // 
            this.advancedList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedList.BackColor = System.Drawing.SystemColors.ControlLight;
            this.advancedList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedList.DataRows.Clear();
            this.advancedList.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] { "", "" });
            this.advancedList.Location = new System.Drawing.Point(21, 74);
            this.advancedList.Name = "advancedList";
            this.advancedList.ScrollbarSmallChange = 32;
            this.advancedList.ScrollbarWidth = 26;
            this.advancedList.SelectedTemplateIndex = 2;
            this.advancedList.ShowHeader = true;
            this.advancedList.Size = new System.Drawing.Size(438, 136);
            this.advancedList.TabIndex = 8;
            this.advancedList.TemplateIndex = 1;
            this.advancedList.Templates.Add(this.SelectedTemplate);
            this.advancedList.Templates.Add(this.UnselectedTemplate);
            this.advancedList.Templates.Add(this.RowTemplate1);
            this.advancedList.TouchScrolling = true;
            this.advancedList.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.AdvancedListRowSelect);
            // 
            // labelOrgUnit
            // 
            this.labelOrgUnit.AutoSize = false;
            this.labelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelOrgUnit.Location = new System.Drawing.Point(3, 38);
            this.labelOrgUnit.Name = "labelOrgUnit";
            this.labelOrgUnit.Size = new System.Drawing.Size(474, 30);
            this.labelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // labelHeading
            // 
            this.labelHeading.AutoSize = false;
            this.labelHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeading.Location = new System.Drawing.Point(0, 2);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(480, 30);
            this.labelHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // textCell1
            // 
            this.textCellConsignment.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.textCellConsignment.CellSource.ColumnIndex = 0;
            this.textCellConsignment.DesignName = "textCell1";
            this.textCellConsignment.Location = new System.Drawing.Point(0, 0);
            this.textCellConsignment.Size = new System.Drawing.Size(355, 30);
            // 
            // textCell2
            // 
            this.textCellPI.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.textCellPI.CellSource.ColumnIndex = 1;
            this.textCellPI.DesignName = "textCell2";
            this.textCellPI.Location = new System.Drawing.Point(360, 0);
            this.textCellPI.Size = new System.Drawing.Size(50, 30);
            // 
            // SelectedTemplate
            // 
            this.SelectedTemplate.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.SelectedTemplate.CellTemplates.Add(this.textCellConsignment);
            this.SelectedTemplate.CellTemplates.Add(this.textCellPI);
            this.SelectedTemplate.Height = 32;
            this.SelectedTemplate.Name = "SelectedTemplate";
            // 
            // TextCell3
            // 
            this.TextCell3.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.TextCell3.CellSource.ColumnIndex = 0;
            this.TextCell3.DesignName = "TextCell3";
            this.TextCell3.Location = new System.Drawing.Point(0, 0);
            this.TextCell3.Size = new System.Drawing.Size(355, 30);
            this.TextCell3.ForeColor = Color.White;
            // 
            // TextCell4
            // 
            this.TextCell4.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.TextCell4.CellSource.ColumnIndex = 1;
            this.TextCell4.DesignName = "TextCell4";
            this.TextCell4.Location = new System.Drawing.Point(360, 0);
            this.TextCell4.Size = new System.Drawing.Size(50, 30);
            this.TextCell4.ForeColor = Color.White;
            // 
            // UnselectedTemplate
            // 
            this.UnselectedTemplate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(191)))), ((int)(((byte)(74)))));
            this.UnselectedTemplate.CellTemplates.Add(this.TextCell3);
            this.UnselectedTemplate.CellTemplates.Add(this.TextCell4);
            this.UnselectedTemplate.Height = 32;
            this.UnselectedTemplate.Name = "UnselectedTemplate";
            // 
            // TextCell5
            // 
            this.textCell5.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.textCell5.CellSource.ColumnIndex = 0;
            this.textCell5.DesignName = "TextCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(355, 30);
            // 
            // TextCell6
            // 
            this.textCell6.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.textCell6.CellSource.ColumnIndex = 1;
            this.textCell6.DesignName = "TextCell6";
            this.textCell6.Location = new System.Drawing.Point(360, 0);
            this.textCell6.Size = new System.Drawing.Size(50, 30);
            // 
            // RowTemplate1
            // 
            this.RowTemplate1.CellTemplates.Add(this.textCell5);
            this.RowTemplate1.CellTemplates.Add(this.textCell6);
            this.RowTemplate1.Height = 30;
            this.RowTemplate1.Name = "RowTemplate1";
            // 
            // FormAttemptedDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormOperationDetail";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeading)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelHeading;
        private Resco.Controls.CommonControls.TransparentLabel labelOrgUnit;
        private Resco.Controls.AdvancedList.AdvancedList advancedList;
        private FotWebBrowser webBrowser;
        private Resco.Controls.AdvancedList.RowTemplate SelectedTemplate;
        private Resco.Controls.AdvancedList.TextCell textCellConsignment;
        private Resco.Controls.AdvancedList.TextCell textCellPI;
        private Resco.Controls.AdvancedList.RowTemplate UnselectedTemplate;
        private Resco.Controls.AdvancedList.TextCell TextCell3;
        private Resco.Controls.AdvancedList.TextCell TextCell4;
        private Resco.Controls.AdvancedList.RowTemplate RowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell5;
        private Resco.Controls.AdvancedList.TextCell textCell6;
    }
}
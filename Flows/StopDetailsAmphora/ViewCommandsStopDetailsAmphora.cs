﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Views;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.StopDetailsAmphora
{
    class ViewCommandsStopDetailsAmphora
    {
        public FormOperationDetail ShowViewOperationList(ViewEventDelegate viewEventHandler, FormDataOperationDetail formConsignmentDetailData)
        {
            var view = ViewCommands.ShowView<FormOperationDetail>(formConsignmentDetailData);
            view.SetEventHandler(viewEventHandler);
            return view;
        }

        internal void ShowDeliveryInfo(ViewEventDelegate viewEventHandler, PlannedConsignmentsType selectedConsignment, string selectedTabName, bool isStaredFromScan, DeliveryOperationType selectedOperationType)
        {
            var formData = new FormDataDeliveryInfo { Consignment = selectedConsignment, SelectedTabName = selectedTabName, IsStaredFromScan = isStaredFromScan, SelectedOperationType = selectedOperationType };
            var view = ViewCommands.ShowView<FormDeliveryInfo>(formData);
            view.SetEventHandler(viewEventHandler);
            view.OnShow(formData);
        }
    }
}

﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.StopDetailsAmphora
{
    public class ActionCommandsStopDetailsAmphora
    {
        public PlannedOperationType GetPlannedOperation(ref OperationListStop operationListStop, string stopId,
                                                        string operationId, bool startedFromPushOperationList)
        {
            PlannedOperationType plannedOperationType = null;
            if (string.IsNullOrEmpty(stopId) == false)
            {
                if (string.IsNullOrEmpty(operationId) == false)
                    plannedOperationType = startedFromPushOperationList
                                               ? ModelOperationList.UpdatedOperationListAmphora.GetPlannedOperation(stopId,
                                                                                                             operationId)
                                               : ModelOperationList.OperationList.GetPlannedOperation(stopId, operationId);

                operationListStop = startedFromPushOperationList
                                               ? ModelOperationList.UpdatedOperationListAmphora.GetStop(stopId)
                                               : ModelOperationList.OperationList.GetStop(stopId);
            }
            return plannedOperationType;
        }
        internal PlannedConsignmentsType GetDeliveryOperation(ref DeliveryOperationType selectedOperationType)
        {
            List<DeliveryOperation> operationsFromLoadlist = CommandsOperationList.GetDeliveryOperationOnTabBase(OperationListTab.All);
            DeliveryOperation operation = null;
            if (operationsFromLoadlist != null)
            {
                foreach (var deliveryOperation in operationsFromLoadlist)
                {
                    if (deliveryOperation != null && deliveryOperation.Consignment != null && deliveryOperation.Consignment.ConsignmentNumber == ModelMain.SelectedOperationProcess.StopId)
                    {
                        operation = deliveryOperation;
                        selectedOperationType = deliveryOperation.OperationType;
                        break;
                    }
                }
            }
            return operation != null ? operation.Consignment : null;
        }
        #region "Methods from back from sub flows"
        internal bool BackFromFlowAttemptedDelivery(FlowResultAttemptedDelivery subflowResult)
        {
            if (subflowResult == null)
                return false;
            bool verifiedSuccessfully = subflowResult.State == FlowResultState.Ok;
            return verifiedSuccessfully;
        }

        #endregion
    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery;
using Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration;
using Com.Bring.PMP.PreComFW.Shared.Flows.ControlScan;
using Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Views;

// US : http://kp-confluence.postennorge.no/display/FOT/GUI+-+Operation+list-V2
namespace Com.Bring.PMP.PreComFW.Shared.Flows.StopDetailsAmphora
{
    public enum FlowStateFlowStopDetailsAmphora
    {
        ActionIsloadListOperation,
        ActionBackFromFlowDeparture,
        ActionBackFromFlowAttemptedDelivery,
        ActionValidateFlowResultBulkRegistration,
        ActionBackFromFlowControlScan,
        ActionGetPlannedOperation,
        ActionGetDeliveryDetail,
        ViewCommands,
        ViewShowOperationDetail,
        ViewUpdateOperationDetail,
        ViewShowDeliveryInfo,
        FlowControlScan,
        FlowBulkRegistration,
        FlowAttemptedDelivery,
        FlowDeparture,
        ThisFlowComplete
    }
    public class FlowDataStopDetailsAmphora : BaseFlowData
    {
        public string StopId { get; set; }
        public string OperationId { get; set; }
        public bool IsStaredFromScan { get; set; }
        public bool IsloadListOperation { get; set; }
    }
    public class FlowResultStopDetailsAmphora : BaseFlowResult
    {
        public bool StartScanFlows { get; set; }
    }
    public class FlowStopDetailsAmphora : BaseFlow
    {
        private string _consignmentId;
        private readonly FlowResultStopDetailsAmphora _flowResult;
        private FlowDataStopDetailsAmphora _flowData;
        private PlannedOperationType _plannedOperationType;
        private OperationListStop _operationListStop;
        private readonly ActionCommandsStopDetailsAmphora _commands;
        private readonly ViewCommandsStopDetailsAmphora _viewCommandsStopDetailsAmphora;
        private PlannedConsignmentsType _deliveryConsignment;
        private DeliveryOperationType _selectedOperationType;
        public FlowStopDetailsAmphora()
        {
            _commands = new ActionCommandsStopDetailsAmphora();
            _flowResult = new FlowResultStopDetailsAmphora();
            _viewCommandsStopDetailsAmphora = new ViewCommandsStopDetailsAmphora();
        }
        public override void BeginFlow(IFlowData flowData)
        {
            _flowData = (FlowDataStopDetailsAmphora)flowData;
            ExecuteState(FlowStateFlowStopDetailsAmphora.ActionIsloadListOperation);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateFlowStopDetailsAmphora)state);
        }

        protected void ExecuteState(FlowStateFlowStopDetailsAmphora state)
        {
            if (state > FlowStateFlowStopDetailsAmphora.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }
        void ExecuteViewState(FlowStateFlowStopDetailsAmphora state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowStopDetailsAmphora;" + state);
                switch (state)
                {
                    case FlowStateFlowStopDetailsAmphora.ViewShowDeliveryInfo:
                        _viewCommandsStopDetailsAmphora.ShowDeliveryInfo(LoadListViewEventHandler, _deliveryConsignment, null, _flowData.IsStaredFromScan, _selectedOperationType);
                        break;
                    case FlowStateFlowStopDetailsAmphora.ViewShowOperationDetail:
                        var formDataOperationDetail = new FormDataOperationDetail { PlannedOperation = _plannedOperationType, OperationListStop = _operationListStop, StaredFromScan = _flowData.IsStaredFromScan };
                        _viewCommandsStopDetailsAmphora.ShowViewOperationList(OperationDetailAmphoraEventHandler,
                                                                              formDataOperationDetail);
                        break;
                    case FlowStateFlowStopDetailsAmphora.ViewUpdateOperationDetail:
                        var formData = new FormDataOperationDetail { PlannedOperation = _plannedOperationType, StaredFromScan = _flowData.IsStaredFromScan };
                        CurrentView.OnUpdateView(formData);
                        break;
                    case FlowStateFlowStopDetailsAmphora.FlowControlScan:
                        var flowDataControlScan = new ControlScanFlowModel { HeaderText = GlobalTexts.OperationListControlScan, ConsignmentId = _consignmentId };
                        StartSubFlow<FlowControlScan>(flowDataControlScan, (int)FlowStateFlowStopDetailsAmphora.ActionBackFromFlowControlScan, Process.Inherit);
                        break;
                    case FlowStateFlowStopDetailsAmphora.FlowBulkRegistration:
                        var flowDataBulkRegistration = new FlowDataBulkRegistration
                        {
                            ConsignmentEntity = new Consignment(_consignmentId),
                            HeaderText = GlobalTexts.BulkRegistration, //_flowData.HeaderText
                            CalledFromOperationList = true
                        };
                        StartSubFlow<FlowBulkRegistration>(flowDataBulkRegistration, (int)FlowStateFlowStopDetailsAmphora.ActionValidateFlowResultBulkRegistration, Process.Inherit);
                        break;
                    case FlowStateFlowStopDetailsAmphora.FlowAttemptedDelivery:
                        var flowDataAttemptedDelivery = new FlowDataAttemptedDelivery
                        {
                            EntityMap = null,
                            IsDirectFromOperationList = true
                        };
                        StartSubFlow<FlowAttemptedDelivery>(flowDataAttemptedDelivery, (int)FlowStateFlowStopDetailsAmphora.ActionBackFromFlowAttemptedDelivery, Process.AttemptedDelivery);
                        break;
                    case FlowStateFlowStopDetailsAmphora.FlowDeparture:
                        StartSubFlow<FlowDeparture>(null, (int)FlowStateFlowStopDetailsAmphora.ActionBackFromFlowDeparture, Process.Inherit);
                        break;
                    case FlowStateFlowStopDetailsAmphora.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowStopDetailsAmphora.ExecuteViewState");
                ExecuteState(FlowStateFlowStopDetailsAmphora.ThisFlowComplete);
            }
        }
        void ExecuteActionState(FlowStateFlowStopDetailsAmphora state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowStopDetailsAmphora;" + state);

                switch (state)
                {
                    case FlowStateFlowStopDetailsAmphora.ActionIsloadListOperation:
                        state = _flowData != null && _flowData.IsloadListOperation
                                    ? FlowStateFlowStopDetailsAmphora.ActionGetDeliveryDetail
                                    : FlowStateFlowStopDetailsAmphora.ActionGetPlannedOperation;
                        break;
                    case FlowStateFlowStopDetailsAmphora.ActionGetDeliveryDetail:
                        _deliveryConsignment = _commands.GetDeliveryOperation(ref _selectedOperationType);
                        state = FlowStateFlowStopDetailsAmphora.ViewShowDeliveryInfo;
                        break;
                    case FlowStateFlowStopDetailsAmphora.ActionGetPlannedOperation:
                        _plannedOperationType = _commands.GetPlannedOperation(ref _operationListStop, _flowData.StopId, _flowData.OperationId,
                                                                              false);
                        state = FlowStateFlowStopDetailsAmphora.ViewShowOperationDetail;
                        break;
                    case FlowStateFlowStopDetailsAmphora.ActionBackFromFlowControlScan:
                        state = FlowStateFlowStopDetailsAmphora.ViewUpdateOperationDetail;
                        break;
                    case FlowStateFlowStopDetailsAmphora.ActionValidateFlowResultBulkRegistration:
                        //state = _actionCommandsNotInTerminal.ValidateFlowBulkRegistration(SubflowResult, out _correctItems, _messageHolder, ref _consignmentEntity, _isDuplicateConsignment, _currentConsignmentEntity);
                        state = FlowStateFlowStopDetailsAmphora.ViewUpdateOperationDetail;
                        break;
                    case FlowStateFlowStopDetailsAmphora.ActionBackFromFlowAttemptedDelivery:
                        _commands.BackFromFlowAttemptedDelivery((FlowResultAttemptedDelivery)SubflowResult);
                        state = FlowStateFlowStopDetailsAmphora.ThisFlowComplete;
                        break;
                    case FlowStateFlowStopDetailsAmphora.ActionBackFromFlowDeparture:
                        state = FlowStateFlowStopDetailsAmphora.ViewUpdateOperationDetail;
                        break;
                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
                ExecuteState(state);
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowStopDetailsAmphora.ExecuteViewState");
                ExecuteState(FlowStateFlowStopDetailsAmphora.ThisFlowComplete);
            }
        }



        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowStopDetailsAmphora.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }

        private void OperationDetailAmphoraEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case OperationDetailFormEvent.Back:
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStateFlowStopDetailsAmphora.ThisFlowComplete);
                    break;
                case OperationDetailFormEvent.BulkRegistration:
                    _consignmentId = Convert.ToString(data[0]);
                    ExecuteState(FlowStateFlowStopDetailsAmphora.FlowBulkRegistration);
                    break;
                case OperationDetailFormEvent.Departure:
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStateFlowStopDetailsAmphora.FlowDeparture);
                    break;
                case OperationDetailFormEvent.ItemDetails:
                    _consignmentId = Convert.ToString(data[0]);
                    ExecuteState(FlowStateFlowStopDetailsAmphora.FlowControlScan);
                    break;
                case OperationDetailFormEvent.Scan:
                    _flowResult.State = FlowResultState.Ok;
                    _flowResult.StartScanFlows = true;
                    ExecuteState(FlowStateFlowStopDetailsAmphora.ThisFlowComplete);
                    break;

            }
        }

        /// <summary>
        /// This method acts as common event handler for, all evets raised from loadlist and error message 
        /// screen.
        /// </summary>

        public void LoadListViewEventHandler(int deliveryInfoEvent, object[] data)
        {

            switch ((DeliveryInfoEvent)deliveryInfoEvent)
            {

                case DeliveryInfoEvent.Scan:
                    _flowResult.State = FlowResultState.Ok;
                    _flowResult.StartScanFlows = true;
                    ExecuteState(FlowStateFlowStopDetailsAmphora.ThisFlowComplete);
                    break;

                case DeliveryInfoEvent.Back:
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStateFlowStopDetailsAmphora.ThisFlowComplete);
                    break;
            }
        }
    }
}

﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.StopDetailsAmphora
{
    public class OperationDetailFormEvent : ViewEvent
    {
        public const int Departure = 0;
        public const int Scan = 1;
        public const int Back = 2;
        public const int ItemDetails = 3;
        public const int BulkRegistration = 4;
    }

    public partial class FormOperationDetail : BaseConsignmentEntityDetailForm
    {
        #region "Private Variables"
        string _heading = string.Empty;
        private string _consignmentNumber;
        private FormDataOperationDetail _formData;
        private MultiButtonControl _tabButtons;
        private OperationListStop _operationListStop;
        private PlannedOperationType _plannedOperationType;
        private const string ButtonBackName = "ButtonBack";
        private const string ButtonDepartureName = "ButtonDeparture";
        private const string ButtonScanName = "ButtonScan";
        private const string ButtonItemDetailsName = "ButtonItemDetails";
        private const string ButtonBulkRegistrationName = "ButtonBulkRegistration";
        #endregion

        #region "Constructor"
        public FormOperationDetail()
        {
            Logger.LogEvent(Severity.Debug, "Calling the Constructor of FormConsignmentDetail");
            InitializeComponent();
            SetTextToGui();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
        }

        private void SetTextToGui()
        {
            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, TabButtonType.Cancel) { Name = ButtonBackName });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Departure, BtnDepartureClick, ButtonDepartureName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Scan, BtnScanClick, TabButtonType.Confirm) { Name = ButtonScanName });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.ItemDetails, BtnItemDetailsClick, ButtonItemDetailsName));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.BulkRegistration, BtnBulkRegistrationClick, ButtonBulkRegistrationName));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
            labelHeading.Text = GlobalTexts.TaskDetails;
        }

        #endregion

        #region "Methods"

        public override void OnShow(IFormData formData)
        {
            _formData = (FormDataOperationDetail)formData;

            if (_formData != null)
            {
                _plannedOperationType = _formData.PlannedOperation;
                _operationListStop = _formData.OperationListStop;
                advancedList.Focus();//I 2069 hack
                if (_plannedOperationType != null)
                {
                    BindConsignmentsToControl();
                    ShowDetail();
                    labelOrgUnit.Text = SetHeading();
                }
                _tabButtons.BeginUpdate();
                if (_formData.StartedFromPushOperationList)
                {
                    _tabButtons.SetButtonEnabledState(ButtonBulkRegistrationName, false);
                    _tabButtons.SetButtonEnabledState(ButtonScanName, false);
                    _tabButtons.SetButtonEnabledState(ButtonItemDetailsName, false);
                    _tabButtons.SetButtonEnabledState(ButtonDepartureName, false);
                }
                else
                {
                    _tabButtons.SetButtonEnabledState(ButtonBulkRegistrationName, _formData.StaredFromScan && advancedList.DataRows.Count > 0);
                    _tabButtons.SetButtonEnabledState(ButtonBackName, !_formData.StaredFromScan);
                    _tabButtons.SetButtonEnabledState(ButtonItemDetailsName, advancedList.DataRows.Count > 0);
                    _tabButtons.SetButtonEnabledState(ButtonDepartureName, !_formData.StaredFromScan);
                }
                _tabButtons.EndUpdate();
            }
        }


        /// <summary>
        /// Binding the PlannedConsignments No's to the ListView Control
        /// </summary>
        private void BindConsignmentsToControl()
        {
            advancedList.DataRows.Clear();
            advancedList.HeaderRow[0] = GlobalTexts.ConsignmentNumbers;
            advancedList.HeaderRow[1] = "PI";
            var colTextValues = new string[2];
            if (_plannedOperationType != null && _plannedOperationType.PlannedConsignments != null)
            {
                foreach (PlannedConsignmentsType consignment in _plannedOperationType.PlannedConsignments)
                {
                    colTextValues[0] = consignment.ConsignmentNumber;
                    colTextValues[1] = consignment.ConsignmentItem != null
                                           ? Convert.ToString(consignment.ConsignmentItem.Length)
                                           : "0";
                    var insertRow = new Row(2, 1, colTextValues);
                    advancedList.DataRows.Add(insertRow);
                }

                if (advancedList.DataRows.Count > 0)
                {
                    advancedList.DataRows[0].Selected = true;
                    _consignmentNumber = advancedList.DataRows[0].StringData[0];
                }

            }
        }
        private string SetHeading()
        {
            switch (_plannedOperationType.OperationType)
            {
                case PlannedOperationTypeOperationType.DELIVERY:
                    _heading = GlobalTexts.Delivery;
                    break;
                case PlannedOperationTypeOperationType.PICKUP:
                    _heading = GlobalTexts.Pickup;
                    break;
                case PlannedOperationTypeOperationType.LOAD:
                    _heading = GlobalTexts.Load;
                    break;
                case PlannedOperationTypeOperationType.LOAD_DISTRIBUTION:
                    _heading = GlobalTexts.LoadDistributionTruck;
                    break;
                case PlannedOperationTypeOperationType.LOAD_LINE_HAUL:
                    _heading = GlobalTexts.LoadLineHaul;
                    break;
                case PlannedOperationTypeOperationType.RETURNEDDELIVERY:
                    _heading = GlobalTexts.Delivery;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD:
                    _heading = GlobalTexts.Unload;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL:
                    _heading = GlobalTexts.UnloadLineHaul;
                    break;
                case PlannedOperationTypeOperationType.UNLOAD_PICKUP:
                    _heading = GlobalTexts.Pickup;
                    break;

            }
            return _heading;
        }

        void ShowDetail()
        {
            var html = new HtmlUtil();
            html.StartTable();
            switch (_plannedOperationType.OperationType)
            {
                case PlannedOperationTypeOperationType.DELIVERY:
                    ShowDetailForDelivery(ref html, true);
                    break;
                case PlannedOperationTypeOperationType.PICKUP:
                    ShowDetailForDelivery(ref html, false);
                    break;

                default:
                    ShowCommonDetail(ref html);
                    break;
            }
            html.EndTable();
            webBrowser.DocumentText = html.GetResult();
        }

        private void ShowDetailForDelivery(ref HtmlUtil html, bool isDelivery)
        {
            var consignee = _plannedOperationType.PlannedConsignments == null ||
                              _plannedOperationType.PlannedConsignments.Length == 0
                                  ? null
                                  : _plannedOperationType.PlannedConsignments[0].Consignee;
            var stopInformation = (_operationListStop == null ? null : _operationListStop.StopInformation);
            ShowCommonDetail(ref html);

            if (stopInformation != null && string.IsNullOrEmpty(stopInformation.CustomerContact) == false)
            {
                html.StartRow();
                html.Append(GlobalTexts.Name + ":");
                html.EndRow();
                html.StartRow();
                html.Append(_operationListStop.StopInformation.CustomerContact);
                html.EndRow();
            }
            else if (isDelivery && consignee != null && string.IsNullOrEmpty(consignee.ContactPerson) == false)
            //if it is delivery operation and stopInformation.CustomerContact is empty
            {
                html.StartRow();
                html.Append(GlobalTexts.Name + ":");
                html.EndRow();
                html.StartRow();
                html.Append(consignee.ContactPerson);
                html.EndRow();
            }
            if (stopInformation != null && string.IsNullOrEmpty(stopInformation.PhoneNumber) == false)
            {
                html.StartRow();
                html.Append(GlobalTexts.TelephoneNumber + ":");
                html.EndRow();
                html.StartRow();
                html.Append(_operationListStop.StopInformation.PhoneNumber);
                html.EndRow();
            }
            else if (isDelivery && consignee != null && string.IsNullOrEmpty(consignee.PhoneNumber) == false)
            //if it is delivery operation and stopInformation.CustomerContact is empty
            {
                html.StartRow();
                html.Append(GlobalTexts.TelephoneNumber + ":");
                html.EndRow();
                html.StartRow();
                html.Append(consignee.PhoneNumber);
                html.EndRow();
            }

            html.StartRow();
            html.EndRow();
            if (stopInformation != null && string.IsNullOrEmpty(stopInformation.AdditionalInfo) == false)
            {
                html.StartRow();
                html.Append(stopInformation.AdditionalInfo);
                html.EndRow();
            }


            if (string.IsNullOrEmpty(_plannedOperationType.PickupOrderNumber) == false)
            {
                html.StartRow();
                html.EndRow();
                html.StartRow();
                html.Append(GlobalTexts.OrderNo + ": " + _plannedOperationType.PickupOrderNumber);
                html.EndRow();
            }
        }


        void ShowCommonDetail(ref HtmlUtil html)
        {

            var stopInformation = (_operationListStop == null ? null : _operationListStop.StopInformation);

            if (_plannedOperationType != null && (_plannedOperationType.EarliestStartTime != DateTime.MinValue ||
                                                  _plannedOperationType.LatestStartTime != DateTime.MinValue))
            {
                html.StartRow();
                html.Append(CommandsOperationList.GetConcatenateTime(_plannedOperationType.EarliestStartTime, _plannedOperationType.LatestStartTime));
                html.EndRow();
            }


            else if (stopInformation != null && (stopInformation.EarliestStopTime != DateTime.MinValue ||
                                                 stopInformation.LatestStopTime != DateTime.MinValue))
            {
                html.StartRow();
                html.Append(CommandsOperationList.GetConcatenateTime(stopInformation.EarliestStopTime, stopInformation.LatestStopTime));
                html.EndRow();
            }
            if (stopInformation != null)
            {
                if (!string.IsNullOrEmpty(stopInformation.StopName))
                {
                    html.StartRow();
                    html.Append(stopInformation.StopName);
                    html.EndRow();
                }
                if (stopInformation.Address != null)
                {
                    string addressPart1 = string.Empty;
                    string addressPart2 = string.Empty;
                    if (!string.IsNullOrEmpty(stopInformation.Address.StreetName))
                    {
                        addressPart1 = stopInformation.Address.StreetName;
                    }
                    if (!string.IsNullOrEmpty(stopInformation.Address.StreetNumber))
                    {
                        addressPart1 = " " + stopInformation.Address.StreetNumber;
                    }

                    if (!string.IsNullOrEmpty(stopInformation.Address.PostalCode))
                    {
                        addressPart2 = stopInformation.Address.PostalCode;
                    }
                    if (!string.IsNullOrEmpty(stopInformation.Address.PostalCode))
                    {
                        addressPart2 += " " + stopInformation.Address.PostalName;
                    }
                    if (!string.IsNullOrEmpty(addressPart1) || !string.IsNullOrEmpty(addressPart2))
                    {
                        html.StartRow();
                        if (!string.IsNullOrEmpty(addressPart1) && !string.IsNullOrEmpty(addressPart2))
                            html.Append(addressPart1 + ", " + addressPart2);
                        else
                            html.Append(addressPart1 + addressPart2);
                        html.EndRow();
                    }
                }

                html.StartRow();
                html.EndRow();
                if (_plannedOperationType != null)
                {

                    if (_plannedOperationType.ConsignmentItemCount > 0)
                    {
                        html.StartRow();
                        html.Append(GlobalTexts.NoOfItems + ": " +
                                    _plannedOperationType.ConsignmentItemCount);
                        html.EndRow();
                    }

                    var weight = _plannedOperationType.GetFormattedWeight();
                    var volume = _plannedOperationType.GetFormattedVolume();
                    if (string.IsNullOrEmpty(weight) == false || string.IsNullOrEmpty(volume) == false)
                    {
                        html.StartRow();
                        if (weight != null && volume != null)
                            html.Append(GlobalTexts.Weight + ": " + weight + " - " + GlobalTexts.Volume + ": " + volume);
                        else
                            html.Append(GlobalTexts.Weight + ": " + (weight ?? volume));
                        html.EndRow();
                    }
                }
                html.StartRow();
                html.EndRow();


            }
        }
        #endregion


        #region "Events"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdvancedListRowSelect(object sender, RowEventArgs e)
        {
            _consignmentNumber = Convert.ToString(e.DataRow[0]);
            Logger.LogEvent(Severity.Debug, "AdvancedListRowSelect _consignmentNumber=;" + _consignmentNumber);
        }
        /// <summary>
        /// This event will get fired whenever we will click on the back button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Back Link Event Fired on the FormConsignment");
                if (_formData != null && _formData.StartedFromPushOperationList)
                    Hide();
                else
                    ViewEvent.Invoke(OperationDetailFormEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConsignmentDetail.BtnBackClick");
            }
        }

        /// <summary>
        /// This event will get fired whenever we will click on the departure button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDepartureClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Departure Button Event Fired on the FormConsignment");
                ViewEvent.Invoke(OperationDetailFormEvent.Departure);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConsignmentDetail.BtnDepartureClick");
            }
        }

        /// <summary>
        /// This event will get fired whenever we will click on the scan button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnItemDetailsClick(object sender, EventArgs e)
        {
            try
            {
                _consignmentNumber = advancedList.SelectedRow.StringData[0];
                Logger.LogEvent(Severity.Debug, "BtnItemDetailsClick;_consignmentNumber='" + _consignmentNumber + "'");
                if (string.IsNullOrEmpty(_consignmentNumber)) return;
                ViewEvent.Invoke(OperationDetailFormEvent.ItemDetails, _consignmentNumber);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConsignmentDetail.BtnItemDetailsClick");
            }
        }

        /// <summary>
        /// This event will get fired whenever we will click on the scan button on the consignment detail screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnScanClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Scan Button Event Fired on the FormConsignment");

                if (_formData != null && _formData.StaredFromScan)
                    ViewEvent.Invoke(OperationDetailFormEvent.Back);
                else
                    ViewEvent.Invoke(OperationDetailFormEvent.Scan);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConsignmentDetail.BtnScanClick");
            }
        }
        private void BtnBulkRegistrationClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Scan Button Event Fired on the FormConsignment");
                ViewEvent.Invoke(OperationDetailFormEvent.BulkRegistration, _consignmentNumber);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConsignmentDetail.BtnBulkRegistrationClick");
            }
        }

        #endregion
    }

    public class FormDataOperationDetail : BaseFormData
    {
        public OperationListStop OperationListStop { get; set; }
        public PlannedOperationType PlannedOperation { get; set; }
        public bool StaredFromScan { get; set; }
        public bool StartedFromPushOperationList { get; set; }
    }
}





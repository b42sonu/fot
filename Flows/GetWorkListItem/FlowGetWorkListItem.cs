﻿using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateGetWorkListItem
    {
        ActionIsPowerUnitKnown,
        ActionIsPdaOffline,
        ActionValidateRbt,
        ActionIsOperationReceived,
        ActionIsSingleOperationReceived,
        ActionIsOfflineInValidateRbt,
        ActionGetWorkListItem,
        ActionIsOfflineInValidateLoadCarrier,
        ActionIsOfflineInValidatePhysicalLoadCarrier,
        ActionValidateLoadCarrier,
        ActionIsConnectedToPhysicalCarier,
        ActionValidatePhysicalLoadCarrier,
        ActionPhyLoadCarrierConnectedToDifferentLogicalLoad,
        ActionConnectPhysicalAndLogicalCarrier,
        ActionValidateRouteId,
        ActionCreateStopItem,
        ActionDecideNextStateBasedOnScan,
        ActionIsPowerUnitEntered,
        ActionProcessStopList,
        ActionDisplayWarningInOffline,
        ActionDisplayWarningInOfflineInRbtScan,
        ActionDoesPhysicalCarrierExists,
        ActionIsRbtOfTypeLineHaulOrDistribution,
        ActionIsOfflineFirstTimeForRbtValidation,
        ActionBackFromFlowVerifyPowerUnit,
        ActionBackFromFlowDisplayStopList,
        ActionBackFromFlowScanPhysicalCarrier,
        ActionCreateManualRbt,
        ActionDisplayWarningInOfflineForPhysicalLoadCarrier,
        ActionCopyValuesFromSelectedStopInToRbt,
        

        ViewCommands,
        ViewEnterRbtManual,
        ViewDisplayWaringMessage,
        ViewLoadLineStopList,
        ViewEnterPhysicalCarrier,
        ViewConnectionstatus,
        ViewShowOfflineMessage,

        FlowVerifyPowerUnit,
        FlowShowDeliveryOperations,

        ThisFlowComplete
    }

    public enum LoadCombinationTripType
    {
        LoadDistribution,
        LoadLineHaul
    }

    public enum GetWorkListItemResultTypes
    {
        WorkListItemCreated,
        OfflineWorkListItemCreated,
        LoadCarrierNotValidated,
        LoadCarrierNotConnectedToPhysical,
        CorrectRbtNotScanned
    }

    public class FlowResultGetWorkListItem : BaseFlowResult
    {
        public WorkListItem WorkListItem { get; set; }
        public GetWorkListItemResultTypes GetWorkListItemResultType { get; set; }

    }
    /// <summary>
    /// flow data that needed for this flow when any flow call this flow...
    /// </summary>
    public class FlowDataGetWorkListItem : BaseFlowData
    {
        private string _powerUnitId;
        private string _loadCarrierId;
        private string _tripId;
        private string _stopId;
        public BarcodeRouteCarrierTrip Rbt;
        public string PowerUnitId
        {
            get { return _powerUnitId; }
            set {
                _powerUnitId = string.IsNullOrEmpty(value) ? string.Empty : value.ToUpper();
            }
        }

        public string LoadCarrierId
        {
            get { return _loadCarrierId; }
            set
            {
                _loadCarrierId = string.IsNullOrEmpty(value) ? string.Empty : value.ToUpper();
            }
        }

        public string TripId
        {
            get { return _tripId; }
            set
            {
                _tripId = string.IsNullOrEmpty(value) ? string.Empty : value.ToUpper();
            }
        }

        public string StopId
        {
            get { return _stopId; }
            set
            {
                _stopId = string.IsNullOrEmpty(value) ? string.Empty : value.ToUpper();
            }
        }
        public string RouteId;
        public BarcodeType BarcodeType;
        public LocationElement CurrentLocation;
        public bool IsPowerUnitEntered;
        public Entity.EntityMap.LoadCarrier LoadCarrierEntity;
        public bool IsForCombinationTrip;
        public LoadCombinationTripType TripType;
    }
    public class FlowGetWorkListItem : BaseFlow
    {
        private bool _isLoadingLineHaulProcess;
        private readonly MessageHolder _messageHolder;
        private Stop _selectedStop;
        private T20240_GetGoodsListFromFOTReply _responseFromLm;
        private bool _isOfflineWorkListItem;
        private bool _offlineHitSecondTime;
        private readonly ViewCommandsGetWorkListItem _viewCommandsGetWorkListItem;
        private FlowDataGetWorkListItem _flowData;
        private readonly FlowResultGetWorkListItem _flowResult;
        private readonly ActionCommandsGetWorkListItem _actionCommandsWorkList;
        private List<Stop> _stops;
        private Entity.EntityMap.LoadCarrier _loadCarrierEntity;
        private Entity.EntityMap.LoadCarrier _physicalLoadCarrierEntity;

        public FlowGetWorkListItem()
        {
            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultGetWorkListItem();
            _actionCommandsWorkList = new ActionCommandsGetWorkListItem(CommunicationClient.Instance);
            _viewCommandsGetWorkListItem = new ViewCommandsGetWorkListItem();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowGetWorkListItem.BeginFlow(IFlowData flowData)");
            _flowData = (FlowDataGetWorkListItem)flowData;

            if (_flowData != null && _flowData.BarcodeType == BarcodeType.LoadCarrier)
            {
                _loadCarrierEntity = _flowData.LoadCarrierEntity;
            }

            _isLoadingLineHaulProcess = Process.LoadLineHaul == CurrentProcess;
            ExecuteState(FlowStateGetWorkListItem.ActionDecideNextStateBasedOnScan);
        }

        public void ExecuteState(FlowStateGetWorkListItem state)
        {
            if (state > FlowStateGetWorkListItem.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateGetWorkListItem)state);
        }
        private void ExecuteViewState(FlowStateGetWorkListItem stateAttemptedDelivery)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItem;" + stateAttemptedDelivery);
                switch (stateAttemptedDelivery)
                {
                    case FlowStateGetWorkListItem.ViewDisplayWaringMessage:
                        _viewCommandsGetWorkListItem.ShowValidationErrorView(ValidationErrorViewEventHandler, _messageHolder, _flowData);
                        break;

                    case FlowStateGetWorkListItem.ViewEnterPhysicalCarrier:
                        _viewCommandsGetWorkListItem.ShowScanPhysicalLoadCarrierView(ScanPhysicalLoadCarrierViewEventHandler, _flowData, _loadCarrierEntity, _messageHolder);
                        break;

                    case FlowStateGetWorkListItem.ViewLoadLineStopList:
                        var flowDataDisplayStopList = new FlowDataDisplayStopList
                        {
                            HeaderText = _flowData.HeaderText,
                            Stops = _stops,
                            OperationType = GetOperationTypeFromProcess()
                        };
                        StartSubFlow<FlowDisplayStopList>(flowDataDisplayStopList, (int)FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList, Process.Inherit);
                        break;

                    case FlowStateGetWorkListItem.ViewConnectionstatus:
                        _viewCommandsGetWorkListItem.ShowConnectionStatusView(ConnectionStatusViewEventHandler, _flowData, _loadCarrierEntity, _physicalLoadCarrierEntity);
                        break;

                    case FlowStateGetWorkListItem.ViewEnterRbtManual:
                        _viewCommandsGetWorkListItem.ShowManualRbtView(_flowData, ManualRbtViewEventHandler, CurrentProcess == Process.LoadLineHaul ? OperationType.LoadLineHaulTruck : OperationType.LoadingDistributionTruck);
                        break;

                    case FlowStateGetWorkListItem.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowGetWorkListItem.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateGetWorkListItem.ThisFlowComplete);
            }
        }


        public OperationType GetOperationTypeFromProcess()
        {
            switch(CurrentProcess)
            {
                case Process.LoadLineHaul:
                    return OperationType.LoadLineHaulTruck;
                case Process.LoadDistribTruck:
                    return OperationType.LoadingDistributionTruck;
                case Process.LoadCombinationTrip:
                    return _flowData.Rbt.TripType == TripType.LineHaul
                               ? OperationType.LoadLineHaulTruck
                               : OperationType.LoadingDistributionTruck;
            }
            return OperationType.LoadLineHaulTruck;
        }


        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateGetWorkListItem state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItem;" + state);
                    bool result;
                    switch (state)
                    {
                        case FlowStateGetWorkListItem.ActionIsPowerUnitEntered:
                            state = _flowData.IsPowerUnitEntered
                                        ? FlowStateGetWorkListItem.ViewEnterRbtManual
                                        : FlowStateGetWorkListItem.ActionDecideNextStateBasedOnScan;
                            break;

                        case FlowStateGetWorkListItem.ActionDecideNextStateBasedOnScan:
                            state = _actionCommandsWorkList.DecideNextStateBasedOnScan(_flowData, CurrentProcess); break;

                        case FlowStateGetWorkListItem.ActionValidateRbt:
                            CommandsOperations.GetStopListForRbt(_flowData.Rbt, GetGoodsListRequestRequestType.StopListAmphora, out _responseFromLm,_flowData.IsForCombinationTrip,_flowData.TripType);
                            state = FlowStateGetWorkListItem.ActionProcessStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionProcessStopList:
                            _stops = CommandsOperations.ProcessGoodsListReplyToGetStops(_responseFromLm, GetOperationTypeFromProcess());
                            state = FlowStateGetWorkListItem.ActionIsOperationReceived;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOperationReceived:
                            result = CommandsOperations.IsTripsAndStopsReturned(_stops);
                            state = result ? FlowStateGetWorkListItem.ActionIsSingleOperationReceived : FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt;
                            break;

                        case FlowStateGetWorkListItem.ActionIsSingleOperationReceived:
                            result = _actionCommandsWorkList.IsSingleOperationReceived(_stops, out _selectedStop);
                            state = result ? FlowStateGetWorkListItem.ActionCopyValuesFromSelectedStopInToRbt : FlowStateGetWorkListItem.ViewLoadLineStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionCopyValuesFromSelectedStopInToRbt:
                            _actionCommandsWorkList.CopyValuesFromSelectedStopInToRbt(_flowData.Rbt,_selectedStop);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;

                        case FlowStateGetWorkListItem.ActionIsRbtOfTypeLineHaulOrDistribution:
                            result = _actionCommandsWorkList.IsRbtOfTypeLineHaulOrDistribution(_flowData, _flowResult, BaseModule.CurrentFlow.CurrentProcess);
                            state = !result
                                        ? FlowStateGetWorkListItem.ThisFlowComplete
                                        : FlowStateGetWorkListItem.ActionValidateRbt;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt:
                            result = _actionCommandsWorkList.IsPdaOffline(_messageHolder, _flowData.BarcodeType);
                            // If coming first time and pda is online, then validate
                            state = result
                                        ? FlowStateGetWorkListItem.ActionIsOfflineFirstTimeForRbtValidation
                                        : FlowStateGetWorkListItem.ActionDisplayWarningInOfflineInRbtScan;
                            break;

                        case FlowStateGetWorkListItem.ActionDisplayWarningInOfflineInRbtScan:
                            result = _actionCommandsWorkList.DisplayWarningForStopsNotAvailable();
                            if (result == false)
                            {
                                _flowResult.State = FlowResultState.Cancel;
                                _flowResult.Message = "";
                            }
                            state = result
                                        ? FlowStateGetWorkListItem.ActionCreateStopItem
                                        : FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineFirstTimeForRbtValidation:
                            state = _offlineHitSecondTime
                                        ? FlowStateGetWorkListItem.ActionDisplayWarningInOfflineInRbtScan
                                        : FlowStateGetWorkListItem.ViewDisplayWaringMessage;

                            _offlineHitSecondTime = !_offlineHitSecondTime;
                            break;

                        case FlowStateGetWorkListItem.ActionDisplayWarningInOffline:
                            result = _actionCommandsWorkList.DisplayWarningMessage();
                            if (result == false)
                            {
                                _flowResult.State = FlowResultState.Cancel;
                                _flowResult.Message = "";
                            }
                            else
                                _isOfflineWorkListItem = true;

                            state = result
                                        ? FlowStateGetWorkListItem.ViewEnterPhysicalCarrier
                                        : FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionDisplayWarningInOfflineForPhysicalLoadCarrier:
                            result = _actionCommandsWorkList.DisplayOfflineWarningMessageForPhysicalLoadCarrier(_messageHolder);
                            _isOfflineWorkListItem = result;
                            state = result
                                        ? FlowStateGetWorkListItem.ActionConnectPhysicalAndLogicalCarrier
                                        : FlowStateGetWorkListItem.ViewEnterPhysicalCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineInValidateLoadCarrier:
                            result = _actionCommandsWorkList.IsPdaOffline(_messageHolder, _flowData.BarcodeType);
                            state = result
                                        ? FlowStateGetWorkListItem.ActionDisplayWarningInOffline
                                        : FlowStateGetWorkListItem.ActionValidateLoadCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineInValidatePhysicalLoadCarrier:
                            result = _actionCommandsWorkList.IsPdaOffline(_messageHolder, _flowData.BarcodeType);
                            state = result
                                        ? FlowStateGetWorkListItem.ActionDisplayWarningInOfflineForPhysicalLoadCarrier
                                        : FlowStateGetWorkListItem.ActionValidatePhysicalLoadCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionValidateLoadCarrier:
                            result = _actionCommandsWorkList.ValidateLoadCarrier(_loadCarrierEntity, _flowResult, false);
                            state = result ? FlowStateGetWorkListItem.ActionIsConnectedToPhysicalCarier : FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionValidatePhysicalLoadCarrier:
                            result = _actionCommandsWorkList.ValidatePhysicalLoadCarrierId(_physicalLoadCarrierEntity, _messageHolder);
                            state = result
                                ? (_physicalLoadCarrierEntity.IsScannedOnline ? FlowStateGetWorkListItem.ActionPhyLoadCarrierConnectedToDifferentLogicalLoad 
                                        : FlowStateGetWorkListItem.ActionDisplayWarningInOfflineForPhysicalLoadCarrier)
                                  : FlowStateGetWorkListItem.ViewEnterPhysicalCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionIsConnectedToPhysicalCarier:
                            result = _actionCommandsWorkList.IsConnectedToPhysicalCarier(_loadCarrierEntity, _flowResult);
                            state = result ? FlowStateGetWorkListItem.ActionGetWorkListItem : FlowStateGetWorkListItem.ViewEnterPhysicalCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionPhyLoadCarrierConnectedToDifferentLogicalLoad:
                            result = _actionCommandsWorkList.IsPhyLoadCarrierConnectedToDifferentLogicalLoad(_physicalLoadCarrierEntity, _loadCarrierEntity);
                            state = result
                                        ? FlowStateGetWorkListItem.ViewConnectionstatus
                                        : FlowStateGetWorkListItem.ActionConnectPhysicalAndLogicalCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionConnectPhysicalAndLogicalCarrier:
                            _actionCommandsWorkList.ConnectPhysicalToLogicalCarrier(_flowData, _physicalLoadCarrierEntity, _loadCarrierEntity);
                            state = FlowStateGetWorkListItem.ActionCreateStopItem;
                            break;

                        case FlowStateGetWorkListItem.ActionCreateStopItem:
                            var operationType = _isLoadingLineHaulProcess
                                                ? OperationType.LoadLineHaulTruck
                                                : OperationType.LoadingDistributionTruck;
                            _selectedStop = _actionCommandsWorkList.CreateStop(_flowData.StopId, _flowData.TripId, operationType);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;

                        case FlowStateGetWorkListItem.ActionGetWorkListItem:
                            _flowResult.WorkListItem = _actionCommandsWorkList.GetWorkListItem(_selectedStop, _flowData, CurrentProcess, _loadCarrierEntity,_flowData.CurrentLocation);
                            _flowResult.State = FlowResultState.Ok;
                            _flowResult.GetWorkListItemResultType = _isOfflineWorkListItem ? GetWorkListItemResultTypes.OfflineWorkListItemCreated : GetWorkListItemResultTypes.WorkListItemCreated;
                            state = FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList:
                            Stop selectedStop;
                            _flowResult.State = SubflowResult.State;
                            state = _actionCommandsWorkList.BackFromFlowShowOperations((FlowResultDisplayStopList)SubflowResult, out selectedStop);
                            _selectedStop = selectedStop;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateGetWorkListItem.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowGetWorkListItem.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateGetWorkListItem.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }
        /// <summary>
        /// Handler which will handle all type of events from manual rbt view..
        /// </summary>
        private void ManualRbtViewEventHandler(int manualRbtViewEvents, params object[] data)
        {
            switch (manualRbtViewEvents)
            {
                case ManualRbtViewEvent.Ok:
                    _flowData.BarcodeType = BarcodeType.Rbt;

                    var loadCarrierId = string.IsNullOrEmpty(Convert.ToString(data[1]))
                                                  ? Convert.ToString(data[0])
                                                  : Convert.ToString(data[1]);

                    var rbt = new BarcodeRouteCarrierTrip(Convert.ToString(data[2]), Convert.ToString(data[3]),
                                                   Convert.ToString(data[0]), loadCarrierId);
                    _flowData.Rbt = rbt;
                    _flowData.PowerUnitId = Convert.ToString(data[0]);
                    _flowData.LoadCarrierId = loadCarrierId;
                    _flowData.RouteId = Convert.ToString(data[2]);
                    _flowData.TripId = Convert.ToString(data[3]);
                    CurrentView.AutoShow = false;
                    ExecuteActionState(FlowStateGetWorkListItem.ActionValidateRbt);
                    break;
                case ManualRbtViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteViewState(FlowStateGetWorkListItem.ThisFlowComplete);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ManualRbtViewEventHandler");
                    break;
            }
        }
        /// <summary>
        /// Handler which will handle all type of events from Validation Error view..
        /// </summary>
        private void ValidationErrorViewEventHandler(int validationErrorViewEvents, params object[] data)
        {
            switch (validationErrorViewEvents)
            {
                case ValidationErrorViewEvent.TryAgain:
                    ExecuteViewState(FlowStateGetWorkListItem.ViewEnterRbtManual);
                    break;
                case ValidationErrorViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    _flowResult.Message = "";
                    ExecuteViewState(FlowStateGetWorkListItem.ThisFlowComplete);
                    break;
                case ValidationErrorViewEvent.Continue:
                    _isOfflineWorkListItem = true;
                    ExecuteActionState(FlowStateGetWorkListItem.ActionCreateStopItem);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ManualRbtViewEventHandler");
                    break;
            }
        }

        /// <summary>
        /// Handler which will handle all type of events from connection Status view..
        /// </summary>
        private void ConnectionStatusViewEventHandler(int connectionStatusViewEvents, params object[] data)
        {
            switch (connectionStatusViewEvents)
            {
                case ConnectionStatusViewEvent.KeepOld:
                    _messageHolder.Update(string.Format(GlobalTexts.OldConnectionToPhysicalPreserved, _physicalLoadCarrierEntity.LoadCarrierId), MessageState.Information);
                    _physicalLoadCarrierEntity = null;
                    ExecuteViewState(FlowStateGetWorkListItem.ViewEnterPhysicalCarrier);
                    break;
                case ConnectionStatusViewEvent.KeepNew:
                    ExecuteActionState(FlowStateGetWorkListItem.ActionConnectPhysicalAndLogicalCarrier);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ConnectionStatusViewEventHandler");
                    break;
            }
        }

        /// <summary>
        /// Handler which will handle all type of events from manual rbt view..
        /// </summary>
        private void ScanPhysicalLoadCarrierViewEventHandler(int scanPhysicalLoadCarrierViewEvents, params object[] data)
        {
            switch (scanPhysicalLoadCarrierViewEvents)
            {
                case ScanPhysicalLoadCarrierViewEvent.BarcodeScanned:
                case ScanPhysicalLoadCarrierViewEvent.Ok:
                    var scannerOutput = (FotScannerOutput)data[0];
                    var physicalCarrier = string.Empty;
                    if (scannerOutput != null && !string.IsNullOrEmpty(scannerOutput.Code))
                        physicalCarrier = scannerOutput.Code.ToUpper();
                    _physicalLoadCarrierEntity = new Entity.EntityMap.LoadCarrier(physicalCarrier) { LoadCarrierId = physicalCarrier };
                    ExecuteState(FlowStateGetWorkListItem.ActionIsOfflineInValidatePhysicalLoadCarrier);
                    break;
                case ScanPhysicalLoadCarrierViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    _flowResult.Message = "";
                    ExecuteViewState(FlowStateGetWorkListItem.ThisFlowComplete);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ScanPhysicalLoadCarrierViewEventHandler");
                    break;
            }
        }


        /// <summary>
        /// This methos ends the WorkList flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowGetWorkListItem");

            switch (_flowResult.State)
            {
                case FlowResultState.Ok:
                    if (_flowData.BarcodeType != BarcodeType.LoadCarrier)
                        SoundUtil.Instance.PlaySuccessSound();
                    break;
                case FlowResultState.Duplicate:
                case FlowResultState.Cancel:
                case FlowResultState.Warning:
                    SoundUtil.Instance.PlayWarningSound();
                    break;

                case FlowResultState.Error:
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;
            }

            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

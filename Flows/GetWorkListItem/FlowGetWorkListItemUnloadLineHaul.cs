﻿using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    
   public class FlowGetWorkListItemUnloadLineHaul : BaseFlow
    {
        private readonly MessageHolder _messageHolder;
        private Stop _selectedStop;
        private T20240_GetGoodsListFromFOTReply _responseFromLm;
        private bool _isOfflineWorkListItem;
        private bool _offlineHitSecondTime;
        private readonly ViewCommandsGetWorkListItem _viewCommandsGetWorkListItem;
        private FlowDataGetWorkListItem _flowData;
        private readonly FlowResultGetWorkListItem _flowResult;
        private readonly ActionCommandsGetWorkListItem _actionCommandsWorkList;
        private List<Stop> _stops;
        private Entity.EntityMap.LoadCarrier _loadCarrierEntity;
        

        public FlowGetWorkListItemUnloadLineHaul()
        {
            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultGetWorkListItem();
            _actionCommandsWorkList = new ActionCommandsGetWorkListItem(CommunicationClient.Instance);
            _viewCommandsGetWorkListItem = new ViewCommandsGetWorkListItem();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowGetWorkListItem.BeginFlow(IFlowData flowData)");
            _flowData = (FlowDataGetWorkListItem)flowData;

            if (_flowData != null)
            {
                if (_flowData.BarcodeType == BarcodeType.LoadCarrier)
                {
                    _loadCarrierEntity = _flowData.LoadCarrierEntity;
                }

            }
            ExecuteState(FlowStateGetWorkListItem.ActionDecideNextStateBasedOnScan);
        }

        public void ExecuteState(FlowStateGetWorkListItem state)
        {
            if (state > FlowStateGetWorkListItem.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateGetWorkListItem)state);
        }
        private void ExecuteViewState(FlowStateGetWorkListItem stateAttemptedDelivery)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItemUnloadLineHaul;" + stateAttemptedDelivery);
                switch (stateAttemptedDelivery)
                {
                    case FlowStateGetWorkListItem.ViewDisplayWaringMessage:
                        _viewCommandsGetWorkListItem.ShowValidationErrorView(ValidationErrorViewEventHandler, _messageHolder, _flowData);
                        break;

                    case FlowStateGetWorkListItem.ViewLoadLineStopList:
                        var flowDataDisplayStopList = new FlowDataDisplayStopList
                        {
                            HeaderText = _flowData.HeaderText,
                            Stops = _stops,
                            OperationType = CurrentProcess == Process.LoadLineHaul ? OperationType.LoadLineHaulTruck : OperationType.LoadingDistributionTruck
                        };
                        StartSubFlow<FlowDisplayStopList>(flowDataDisplayStopList, (int)FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList, Process.Inherit);
                        break;
                    
                    case FlowStateGetWorkListItem.ViewEnterRbtManual:
                        _viewCommandsGetWorkListItem.ShowManualRbtView(_flowData, ManualRbtViewEventHandler, CurrentProcess == Process.LoadLineHaul ? OperationType.LoadLineHaulTruck : OperationType.LoadingDistributionTruck);
                        break;

                    case FlowStateGetWorkListItem.ViewShowOfflineMessage:
                        _viewCommandsGetWorkListItem.ShowValidationErrorView(ValidationErrorViewEventHandler,
                                                                             _messageHolder, _flowData);
                        break;

                    case FlowStateGetWorkListItem.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowGetWorkListItem.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateGetWorkListItem.ThisFlowComplete);
            }
        }
        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateGetWorkListItem state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItemUnloadLineHaul;" + state);
                    bool result;
                    switch (state)
                    {
                        case FlowStateGetWorkListItem.ActionIsPowerUnitEntered:
                            state = _flowData.IsPowerUnitEntered
                                        ? FlowStateGetWorkListItem.ViewEnterRbtManual
                                        : FlowStateGetWorkListItem.ActionDecideNextStateBasedOnScan;
                            break;

                        case FlowStateGetWorkListItem.ActionDecideNextStateBasedOnScan:
                            state = _actionCommandsWorkList.DecideNextStateBasedOnScan(_flowData, CurrentProcess);
                            break;

                        case FlowStateGetWorkListItem.ActionValidateRbt:
                            CommandsOperations.GetStopListForRbt(_flowData.Rbt, GetGoodsListRequestRequestType.StopListAmphora, out _responseFromLm,_flowData.IsForCombinationTrip,_flowData.TripType);
                            state = FlowStateGetWorkListItem.ActionProcessStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionProcessStopList:
                            _stops = CommandsOperations.ProcessGoodsListReplyToGetStops(_responseFromLm, OperationType.UnloadLineHaulTruck);
                            state = FlowStateGetWorkListItem.ActionIsOperationReceived;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOperationReceived:
                            result = CommandsOperations.IsTripsAndStopsReturned(_stops);
                            state = result ? FlowStateGetWorkListItem.ActionIsSingleOperationReceived : FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt;
                            break;

                        case FlowStateGetWorkListItem.ActionIsSingleOperationReceived:
                            result = _actionCommandsWorkList.IsSingleOperationReceived(_stops, out _selectedStop);
                            state = result ? FlowStateGetWorkListItem.ActionCopyValuesFromSelectedStopInToRbt : FlowStateGetWorkListItem.ViewLoadLineStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionCopyValuesFromSelectedStopInToRbt:
                            _actionCommandsWorkList.CopyValuesFromSelectedStopInToRbt(_flowData.Rbt, _selectedStop);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;

                        case FlowStateGetWorkListItem.ActionIsRbtOfTypeLineHaulOrDistribution:
                            result = _actionCommandsWorkList.IsRbtOfTypeLineHaulOrDistribution(_flowData, _flowResult,BaseModule.CurrentFlow.CurrentProcess);
                            state = !result
                                        ? FlowStateGetWorkListItem.ThisFlowComplete
                                        : FlowStateGetWorkListItem.ActionValidateRbt;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt:
                            result = _actionCommandsWorkList.IsPdaOffline(_messageHolder,_flowData.BarcodeType);
                            // If coming first time and pda is online, then validate
                            state = result
                                        ? FlowStateGetWorkListItem.ActionIsOfflineFirstTimeForRbtValidation
                                        : FlowStateGetWorkListItem.ActionDisplayWarningInOfflineInRbtScan;
                            break;

                        case FlowStateGetWorkListItem.ActionDisplayWarningInOfflineInRbtScan:
                            result = _actionCommandsWorkList.DisplayWarningForStopsNotAvailable();
                            if (result == false)
                            {
                                _flowResult.State = FlowResultState.Cancel;
                                _flowResult.Message = "";
                            }
                            state = result
                                        ? FlowStateGetWorkListItem.ActionCreateStopItem
                                        : FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineFirstTimeForRbtValidation:
                            state = _offlineHitSecondTime
                                        ? FlowStateGetWorkListItem.ActionDisplayWarningInOffline
                                        : FlowStateGetWorkListItem.ViewDisplayWaringMessage;
                            _offlineHitSecondTime = !_offlineHitSecondTime;
                            break;

                        case FlowStateGetWorkListItem.ActionDisplayWarningInOffline:
                            result = _actionCommandsWorkList.DisplayWarningMessage();
                            if (result == false)
                            {
                                _flowResult.State = FlowResultState.Cancel;
                                _flowResult.Message = "";
                            }
                            else
                                _isOfflineWorkListItem = true;

                            state = result
                                        ? FlowStateGetWorkListItem.ActionCreateStopItem
                                        : FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOfflineInValidateLoadCarrier:
                            result = _actionCommandsWorkList.IsPdaOffline(_messageHolder,_flowData.BarcodeType);
                            state = result
                                        ? FlowStateGetWorkListItem.ActionDisplayWarningInOffline
                                        : FlowStateGetWorkListItem.ActionValidateLoadCarrier;
                            break;

                        case FlowStateGetWorkListItem.ActionValidateLoadCarrier:
                            result = _actionCommandsWorkList.ValidateLoadCarrier(_loadCarrierEntity, _flowResult,false);
                            state = result ? FlowStateGetWorkListItem.ActionIsConnectedToPhysicalCarier : FlowStateGetWorkListItem.ActionCreateStopItem;
                            break;

                        case FlowStateGetWorkListItem.ActionIsConnectedToPhysicalCarier:
                            result = _actionCommandsWorkList.IsConnectedToPhysicalCarier(_loadCarrierEntity, _flowResult);
                            
                            state = result ? FlowStateGetWorkListItem.ActionCreateStopItem : FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        case FlowStateGetWorkListItem.ActionCreateStopItem:
                            _selectedStop = _actionCommandsWorkList.CreateStop(_flowData.StopId, _flowData.TripId, OperationType.UnloadLineHaulTruck);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;


                        case FlowStateGetWorkListItem.ActionGetWorkListItem:
                            _flowResult.WorkListItem = _actionCommandsWorkList.GetWorkListItem(_selectedStop, _flowData, CurrentProcess, _loadCarrierEntity,_flowData.CurrentLocation);
                            _flowResult.State = FlowResultState.Ok;
                            _flowResult.GetWorkListItemResultType = _isOfflineWorkListItem ? GetWorkListItemResultTypes.OfflineWorkListItemCreated : GetWorkListItemResultTypes.WorkListItemCreated;
                            state = FlowStateGetWorkListItem.ThisFlowComplete;
                            break;

                        
                        case FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList:
                            Stop selectedStop;
                            _flowResult.State = SubflowResult.State;
                            state = _actionCommandsWorkList.BackFromFlowShowOperations((FlowResultDisplayStopList)SubflowResult, out selectedStop);
                            _selectedStop = selectedStop;
                            break;

                      

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateGetWorkListItem.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowGetWorkListItem.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateGetWorkListItem.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }
        /// <summary>
        /// Handler which will handle all type of events from manual rbt view..
        /// </summary>
        private void ManualRbtViewEventHandler(int manualRbtViewEvents, params object[] data)
        {
            switch (manualRbtViewEvents)
            {
                case ManualRbtViewEvent.Ok:
                    _flowData.BarcodeType = BarcodeType.Rbt;

                    var loadCarrierId = string.IsNullOrEmpty(Convert.ToString(data[1]))
                                                  ? Convert.ToString(data[0])
                                                  : Convert.ToString(data[1]);

                    var rbt = new BarcodeRouteCarrierTrip(Convert.ToString(data[2]), Convert.ToString(data[3]),
                                                   Convert.ToString(data[0]), loadCarrierId);
                    _flowData.Rbt = rbt;
                    _flowData.Rbt.TripType = TripType.LineHaul;
                    _flowData.PowerUnitId = Convert.ToString(data[0]);
                    _flowData.LoadCarrierId = loadCarrierId;
                    _flowData.RouteId = Convert.ToString(data[2]);
                    _flowData.TripId = Convert.ToString(data[3]);
                    CurrentView.AutoShow = false;
                    ExecuteActionState(FlowStateGetWorkListItem.ActionValidateRbt);
                    break;
                case ManualRbtViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteViewState(FlowStateGetWorkListItem.ThisFlowComplete);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ManualRbtViewEventHandler");
                    break;
            }
        }
        /// <summary>
        /// Handler which will handle all type of events from Validation Error view..
        /// </summary>
        private void ValidationErrorViewEventHandler(int validationErrorViewEvents, params object[] data)
        {
            switch (validationErrorViewEvents)
            {
                case ValidationErrorViewEvent.TryAgain:
                    ExecuteViewState(FlowStateGetWorkListItem.ViewEnterRbtManual);
                    break;
                case ValidationErrorViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    _flowResult.Message = "";
                    ExecuteViewState(FlowStateGetWorkListItem.ThisFlowComplete);
                    break;
                case ValidationErrorViewEvent.Continue:
                    ExecuteActionState(FlowStateGetWorkListItem.ActionCreateStopItem);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ManualRbtViewEventHandler");
                    break;
            }
        }


        /// <summary>
        /// This methos ends the WorkList flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowGetWorkListItem");

            switch (_flowResult.State)
            {
                case FlowResultState.Ok:
                    if (_flowData.BarcodeType != BarcodeType.LoadCarrier)
                    SoundUtil.Instance.PlaySuccessSound();
                    break;
                case FlowResultState.Duplicate:
                case FlowResultState.Cancel:
                case FlowResultState.Warning:
                    SoundUtil.Instance.PlayWarningSound();
                    break;

                case FlowResultState.Error:
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;
            }

            BaseModule.EndSubFlow(_flowResult);
        }
    }

}

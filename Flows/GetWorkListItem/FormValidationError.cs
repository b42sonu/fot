﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    public partial class FormValidationError : BaseForm
    {
        private FormDataValidationError _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        private const string ButtonTryAgain = "ButtonTryAgain";
        public FormValidationError()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            AddButtons();
        }

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.AddButtonsAccordintToProcess()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Continue, ButtonContinueClick, TabButtonType.Confirm) { Name = ButtonOk });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.TryAgain, ButtonTryAgainClick, ButtonTryAgain));
                
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.SetTextToGui");
            }
        }

        public override void OnShow(IFormData iformData)
        {
            OnUpdateView(iformData);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataValidationError) iformData;
            if(_formData!=null && _formData.MessageHolder!=null)
            {
                lblOrgUnit.Text = _formData.OrgUnitName;
                labelModuleName.Text = _formData.HeaderText;
                messageControl.ShowMessage(_formData.MessageHolder.Text, _formData.MessageHolder.State);
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormValidationError method FormValidationError.ButtonBackClick");
                ViewEvent.Invoke(ValidationErrorViewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormValidationError.ButtonBackClick");
            }
        }

        private void ButtonContinueClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormValidationError method FormValidationError.ButtonConfirmClick");
                ViewEvent.Invoke(ValidationErrorViewEvent.Continue);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormValidationError.ButtonConfirmClick");
            }
        }

        private void ButtonTryAgainClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormValidationError method FormValidationError.ButtonBackClick");
                ViewEvent.Invoke(ValidationErrorViewEvent.TryAgain);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormValidationError.ButtonBackClick");
            }
        }
    }

    public class FormDataValidationError : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
        public string PowerUnitId { get; set; }
        public string LoadCarrierId { get; set; }
        public string RouteId { get; set; }
        public string TripId { get; set; }
        public string StopId { get; set; }
    }
    public class ValidationErrorViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Continue = 1;
        public const int TryAgain = 2;
    }
}
﻿using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.VerifyPowerUnit;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    internal class ActionCommandsGetWorkListItem
    {
        //RBT  -> Route LoadCarrier Trip
        private static ICommunicationClient _communicationClient;

        public ActionCommandsGetWorkListItem(ICommunicationClient comClient)
        {
            _communicationClient = comClient;
        }



        //TODO :: Write Unit Test
        /// <summary>
        /// method for check if operation is single or multiple received....
        /// </summary>
        /// <returns></returns>
        internal bool IsSingleOperationReceived(List<Stop> listOfStops, out Stop stop)
        {
            bool result = listOfStops != null && listOfStops.Any() && listOfStops.Count() == 1;
            stop = result ? listOfStops[0] : null;
            if (stop != null)
            {
                ModelMain.SelectedOperationProcess.StopId = stop.StopId;
                ModelMain.SelectedOperationProcess.TripId = stop.TripId;
                ModelMain.SelectedOperationProcess.OperationId = string.Format("{0}_{1}", stop.StopId, stop.TripId);
                ModelMain.SelectedOperationProcess.Location = stop.Location;
                ModelMain.SelectedOperationProcess.RouteId = stop.RouteId;
                ModelMain.SelectedOperationProcess.PowerUnitId = stop.PowerUnitId;
                ModelMain.SelectedOperationProcess.LogicalLoadCarrierId = stop.LoadCarrierId;
            }

            return result;
        }


        internal void CopyValuesFromSelectedStopInToRbt(BarcodeRouteCarrierTrip rbt, Stop stop)
        {
            rbt.PowerUnitId = stop.PowerUnitId;
            rbt.LoadCarrierId = stop.LoadCarrierId;
            rbt.RouteId = stop.RouteId;
            rbt.TripId = stop.TripId;
        }


        //TODO :: Write Unit Test
        internal FlowStateGetWorkListItem DecideNextStateBasedOnScan(FlowDataGetWorkListItem flowData, Process currentProcess)
        {
            switch (flowData.BarcodeType)
            {
                case BarcodeType.Rbt:
                    if (currentProcess == Process.LoadCombinationTrip)
                        return FlowStateGetWorkListItem.ActionValidateRbt;
                    return flowData.IsPowerUnitEntered
                                   ? FlowStateGetWorkListItem.ViewEnterRbtManual
                                   : FlowStateGetWorkListItem.ActionIsRbtOfTypeLineHaulOrDistribution;
                case BarcodeType.LoadCarrier:
                    return FlowStateGetWorkListItem.ActionIsOfflineInValidateLoadCarrier;
                case BarcodeType.RouteId:
                    return FlowStateGetWorkListItem.ActionCreateStopItem;
            }
            return FlowStateGetWorkListItem.ThisFlowComplete;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// method for check is pad online or offline...
        /// </summary>
        /// <returns></returns>
        internal bool IsPdaOffline(MessageHolder messageHolder, BarcodeType scannedBarcodeType)
        {
            if (messageHolder != null) 
                messageHolder.Clear();

            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsWorkList.IsSingleOperationReceived()");
            bool result = !_communicationClient.IsConnected();
            if (result && messageHolder != null && scannedBarcodeType != BarcodeType.LoadCarrier)
            {
                messageHolder.Update(GlobalTexts.CancelTryOrProcessOffline, MessageState.Error);
            }
            return result;
        }

        //TODO :: Write Unit Test
        internal bool IsRbtOfTypeLineHaulOrDistribution(FlowDataGetWorkListItem flowData, FlowResultGetWorkListItem flowResult, Process process)
        {
            if (flowData.Rbt != null)
            {
                var result = process == Process.LoadLineHaul || process == Process.UnloadLineHaul
                                 ? flowData.Rbt.TripType == TripType.LineHaul
                                 : flowData.Rbt.TripType == TripType.Local;

                if (!result)
                    flowResult.GetWorkListItemResultType = GetWorkListItemResultTypes.CorrectRbtNotScanned;

                return result;
            }

            return false;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// method for get particular work list item by selecting any of  operation received or another combination...
        /// </summary>
        internal WorkListItem GetWorkListItem(Stop selectedStop, FlowDataGetWorkListItem flowData, Process currentProcess, Entity.EntityMap.LoadCarrier loadCarrierEntity, LocationElement location)
        {
            var workListItem = new WorkListItem();

            switch (flowData.BarcodeType)
            {
                case BarcodeType.Rbt:
                    workListItem.Type = WorkListItemType.RouteCarrierTrip;
                    workListItem.LoadCarrierId = flowData.Rbt != null ? flowData.Rbt.LoadCarrierId : string.Empty;
                    workListItem.PhysicalLoadCarrierId = selectedStop != null &&
                                                         string.IsNullOrEmpty(selectedStop.LoadCarrierId) == false
                                                             ? selectedStop.LoadCarrierId
                                                             : flowData.Rbt != null
                                                                   ? flowData.Rbt.LoadCarrierId
                                                                   : string.Empty;

                    workListItem.Rbt = flowData.Rbt;
                    workListItem.RouteId = flowData.Rbt != null ? flowData.Rbt.RouteId : string.Empty;
                    workListItem.PowerUnitId = flowData.Rbt != null ? flowData.Rbt.PowerUnitId : string.Empty;
                    workListItem.StopId = selectedStop != null ? selectedStop.StopId : string.Empty;
                    workListItem.TripId = selectedStop != null && string.IsNullOrEmpty(selectedStop.TripId) == false ? selectedStop.TripId : flowData.Rbt != null ? flowData.Rbt.TripId : string.Empty;
                    workListItem.ExternalTripId = selectedStop != null ? selectedStop.ExternalTripId : string.Empty;
                    break;
                case BarcodeType.LoadCarrier:
                    workListItem.Type = WorkListItemType.LoadCarrier;
                    workListItem.LoadCarrierId = flowData.LoadCarrierId;
                    workListItem.PhysicalLoadCarrierId = selectedStop != null &&
                                                         string.IsNullOrEmpty(selectedStop.LoadCarrierId) == false
                                                             ? selectedStop.LoadCarrierId
                                                             : flowData.LoadCarrierId;
                    workListItem.RouteId = string.Empty;
                    workListItem.PowerUnitId = string.Empty;
                    workListItem.StopId = string.Empty;
                    workListItem.TripId = string.Empty;
                    workListItem.ExternalTripId = selectedStop != null ? selectedStop.ExternalTripId : string.Empty;

                    //If load carrier details are available
                    if (loadCarrierEntity.LogicalLoadCarrierDetails != null)
                    {
                        var loadCarrierDetails = loadCarrierEntity.LogicalLoadCarrierDetails;
                        if (loadCarrierDetails != null)
                            workListItem.LoadCarrierDetails = loadCarrierDetails[0];
                    }

                    //If physical logical load detaills are available
                    if (loadCarrierEntity.PhysicalLoadCarrier != null)
                    {
                        workListItem.PhysicalLoadCarrierDetails = loadCarrierEntity.PhysicalLoadCarrier;
                    }

                    workListItem.IsLoadCarrierKnownInLm = loadCarrierEntity.IsKnownInLm;

                    break;
                case BarcodeType.RouteId:
                    workListItem.Type = WorkListItemType.Route;
                    workListItem.LoadCarrierId = string.Empty;
                    workListItem.PhysicalLoadCarrierId = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora &&
                                           ModelUser.UserProfile.Role == Role.Driver1
                                               ? ModelUser.UserProfile.LoadCarrier
                                               : string.Empty;
                    workListItem.RouteId = flowData.RouteId;
                    workListItem.PowerUnitId = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora &&
                                               ModelUser.UserProfile.Role == Role.Driver1
                                                   ? ModelUser.UserProfile.PowerUnit
                                                   : string.Empty;
                    workListItem.StopId = string.Empty;
                    workListItem.TripId = string.Empty;
                    workListItem.ExternalTripId = string.Empty;
                    break;
            }

            if (flowData.IsForCombinationTrip)
                workListItem.CombinationTripType = flowData.TripType == LoadCombinationTripType.LoadLineHaul
                                            ? OperationType.LoadLineHaulTruck
                                            : OperationType.LoadingDistributionTruck;

            workListItem.Location = location != null ? location.Clone() : null;
            workListItem.StopType = CommandsOperations.GetStopTypeFromProcess(currentProcess);
            return workListItem;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// method for validate load carrier id number from LM...
        /// </summary>
        /// <param name="loadCarrierEntity"></param>
        /// <param name="flowResult"> </param>
        /// <param name="isToSetEventType"> </param>
        /// <returns></returns>
        internal bool ValidateLoadCarrier(Entity.EntityMap.LoadCarrier loadCarrierEntity, FlowResultGetWorkListItem flowResult, bool isToSetEventType)
        {
            var validationResult = true;

            var result = ActionCommandsScanBarcode.ValidateLoadCarrier(loadCarrierEntity);
            if (result == false && loadCarrierEntity.IsScannedOnline)
            {
                validationResult = false;
                if (BaseModule.CurrentFlow.CurrentProcess == Process.LoadLineHaul)
                {
                    flowResult.GetWorkListItemResultType = GetWorkListItemResultTypes.LoadCarrierNotValidated;
                    flowResult.State = FlowResultState.Error;
                }
                else
                    flowResult.State = FlowResultState.Ok;
            }
            return validationResult;
        }


        internal bool ValidatePhysicalLoadCarrierId(Entity.EntityMap.LoadCarrier loadCarrierEntity, MessageHolder messageHolder)
        {
            var validationResult = true;

            var result = ActionCommandsScanBarcode.ValidateLoadCarrier(loadCarrierEntity, null, "Vekselbeholder", false, string.Empty);

            if (result == false && loadCarrierEntity.IsScannedOnline && !loadCarrierEntity.IsKnownInLm)
            {
                validationResult = false;
                SoundUtil.Instance.PlayWarningSound();
                messageHolder.Update(string.Format(GlobalTexts.PhyCarrierDoesNotExists, loadCarrierEntity.LoadCarrierId), MessageState.Warning);
            }

            return validationResult;
        }


        //TODO :: Write Unit Test
        /// <summary>
        ///  method for check that is logical load carrier is connected with Physical load carrier....
        /// </summary>
        /// <returns></returns>
        internal bool IsConnectedToPhysicalCarier(Entity.EntityMap.LoadCarrier loadCarrierEntity, FlowResultGetWorkListItem flowResult)
        {
            var result = !(loadCarrierEntity.LogicalLoadCarrierDetails == null || loadCarrierEntity.PhysicalLoadCarrier == null);

            if (loadCarrierEntity.LogicalLoadCarrierDetails != null)
            {
                //if (loadCarrierEntity.LogicalLoadCarrierDetails != null)
                //{
                var logicalLoadCarrier = loadCarrierEntity.LogicalLoadCarrierDetails[0];

                if (logicalLoadCarrier == null)
                    return false;

                result = Convert.ToString(logicalLoadCarrier.PhysicalId) != "generisk" && Convert.ToString(logicalLoadCarrier.PhysicalLoadCarrierId) != "1";
                //}
            }

            if (!result)
            {
                flowResult.GetWorkListItemResultType = GetWorkListItemResultTypes.LoadCarrierNotConnectedToPhysical;
                flowResult.State = FlowResultState.Warning;
            }


            return result;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// method for check that is physical Load already Connected to logicalLoad......
        /// </summary>
        /// <returns></returns>
        internal bool IsPhyLoadCarrierConnectedToDifferentLogicalLoad(Entity.EntityMap.LoadCarrier physicalLoadCarrier, Entity.EntityMap.LoadCarrier logicalLoadCarrier)
        {
            var loadCarrierDetails = physicalLoadCarrier.LogicalLoadCarrierDetails;

            if (loadCarrierDetails != null && loadCarrierDetails.Length > 0)
                return true;
            return false;
        }







        //TODO :: Write Unit Test
        /// <summary>
        /// method for create single peration ....
        /// </summary>
        /// <param name="stopId"></param>
        /// <param name="tripId"></param>
        /// <param name="operationType"> </param>
        /// <returns></returns>
        internal Stop CreateStop(string stopId, string tripId, OperationType operationType)
        {
            var stop = new Stop
                {
                    StopId = string.IsNullOrEmpty(stopId) ? string.Empty : stopId,
                    Status = CommandsOperations.GetLoadingOperationStatus(StopOperationType.New),
                    TripId = string.IsNullOrEmpty(tripId) ? string.Empty : tripId,
                    OperationDetail = string.Empty,
                    StopType = operationType,

                };
            return stop;
        }

        public void ConnectPhysicalToLogicalCarrier(FlowDataGetWorkListItem flowData, Entity.EntityMap.LoadCarrier physicalLoadCarrierEntity, Entity.EntityMap.LoadCarrier logicalLoadCarrierEntity)
        {

            if (physicalLoadCarrierEntity != null)
            {
                var physicalLoadCarrier = physicalLoadCarrierEntity.LoadCarrierId;
                var logicalLoadCarrier = logicalLoadCarrierEntity.LoadCarrierId;

                //send synchronous request
                var status = ConnectPhysicalToLogicalLoadCarrierSynchronously(flowData, physicalLoadCarrier,
                                                                              logicalLoadCarrier);
                if (!status)
                    ConnectPhysicalToLogicalLoadCarrierAsynchronously(flowData, physicalLoadCarrier,
                                                                      logicalLoadCarrier);
            }
        }


        private void ConnectPhysicalToLogicalLoadCarrierAsynchronously(FlowDataGetWorkListItem flowData, string physicalLoadCarrier, string logicalLoadCarrier)
        {
            var orgUnitId = flowData.CurrentLocation != null ? flowData.CurrentLocation.Number : string.Empty;
            var eventInfo = LoadCarrierEventHelper.CreateConnectLoadCarriersAsyncRequest(physicalLoadCarrier, logicalLoadCarrier);

            if (eventInfo != null && CommunicationClient.Instance != null)
            {
                eventInfo.NameOfType = "Vekselbeholder";
                if (!string.IsNullOrEmpty(orgUnitId))
                    eventInfo.OrgUnitId = orgUnitId;
                CommunicationClient.Instance.SendAsyncConnectPhysicalToLogicalCarrierEvent(eventInfo);
            }
        }

        private bool ConnectPhysicalToLogicalLoadCarrierSynchronously(FlowDataGetWorkListItem flowData, string physicalLoadCarrier, string logicalLoadCarrier)
        {
            var orgUnitId = flowData.CurrentLocation != null ? flowData.CurrentLocation.Number : "";
            var request = LoadCarrierEventHelper.CreateConnectLoadCarriersSyncRequest(physicalLoadCarrier, logicalLoadCarrier);

            if (request != null && CommunicationClient.Instance != null)
            {
                request.NameOfType = "Vekselbeholder";
                if (!string.IsNullOrEmpty(orgUnitId))
                    request.OrgUnitId = orgUnitId;
                var reply = CommunicationClient.Instance.SendSyncConnectPhysicalToLogicalCarrierEvent(request);

                if (reply != null)
                    return true;
            }
            return false;
        }

        //TODO :: Write Unit Test
        internal FlowStateGetWorkListItem BackFromFlowShowOperations(FlowResultDisplayStopList subflowResult, out Stop selectedStop)
        {
            var flowStateGetWorkListItem = FlowStateGetWorkListItem.ThisFlowComplete;
            selectedStop = null;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    if (subflowResult.LoadWithOutStop)
                        flowStateGetWorkListItem = FlowStateGetWorkListItem.ViewEnterRbtManual;
                    else
                    {
                        flowStateGetWorkListItem = FlowStateGetWorkListItem.ActionCopyValuesFromSelectedStopInToRbt;
                        selectedStop = new Stop
                                           {
                                               TripId = ModelMain.SelectedOperationProcess.TripId,
                                               OperationDetail = ModelMain.SelectedOperationProcess.OperationId,
                                               StopId = ModelMain.SelectedOperationProcess.StopId,
                                               ExternalTripId = ModelMain.SelectedOperationProcess.ExternalTripId,
                                               RouteId = ModelMain.SelectedOperationProcess.RouteId,
                                               PowerUnitId = ModelMain.SelectedOperationProcess.PowerUnitId,
                                               LoadCarrierId = ModelMain.SelectedOperationProcess.LogicalLoadCarrierId
                                           };
                    }

                    break;
            }
            return flowStateGetWorkListItem;
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// This method, validates the result from flow FlowVerifyPowerUnit and decides which will be the next state.
        /// </summary>
        /// <returns>Returns next state to execute.</returns>
        public FlowStateGetWorkListItem ValidateFlowVerifyPowerUnitResult(FlowResultVerifyPowerUnit subflowResult, ref string powerUnitId)
        {
            FlowStateGetWorkListItem resultState;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    powerUnitId = subflowResult.PowerUnitId;
                    resultState = FlowStateGetWorkListItem.ActionIsPdaOffline;
                    break;
                case FlowResultState.Cancel:
                    resultState = FlowStateGetWorkListItem.ThisFlowComplete;
                    break;
                default:
                    resultState = FlowStateGetWorkListItem.ThisFlowComplete;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            return resultState;
        }



        //TODO :: Write Unit Test
        /// <summary>
        /// method used when no stop list available ....
        /// </summary>
        /// <returns></returns>
        internal bool DisplayWarningMessage()
        {
            var modalMessageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.ContinueIfOnline, Severity.Warning, GlobalTexts.Activate, GlobalTexts.Cancel);
            return modalMessageBoxResult == GlobalTexts.Activate;
        }

        internal bool DisplayOfflineWarningMessageForPhysicalLoadCarrier(MessageHolder messageHolder)
        {
            var modalMessageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.ContinueIfOnline, Severity.Warning, GlobalTexts.Continue, GlobalTexts.Cancel);
            if (modalMessageBoxResult == GlobalTexts.Continue)
                return true;
            messageHolder.Update(GlobalTexts.ScanningPhysicalCarrierAborted, MessageState.Warning);
            return false;
        }



        //TODO :: Write Unit Test
        /// <summary>
        /// method used when no stop list available ....
        /// </summary>
        /// <returns></returns>
        internal bool DisplayWarningForStopsNotAvailable()
        {
            var modalMessageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.ContinueIfStopsNotAvailable, Severity.Warning, GlobalTexts.Activate, GlobalTexts.Cancel);
            return modalMessageBoxResult == GlobalTexts.Activate;
        }

    }
}

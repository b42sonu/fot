﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    partial class FormScanPhysicalLoadCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblRegisterLoadCarrier = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblPwr = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDestination = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblScanLoadCarrier = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPhysLoadCarrier = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtScannedNumber = new System.Windows.Forms.TextBox();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegisterLoadCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPwr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanLoadCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhysLoadCarrier)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.lblPhysLoadCarrier);
            this.touchPanel.Controls.Add(this.lblScanLoadCarrier);
            this.touchPanel.Controls.Add(this.lblDestination);
            this.touchPanel.Controls.Add(this.lblPwr);
            
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblRegisterLoadCarrier);
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480,552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 2);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblRegisterLoadCarrier
            // 
            this.lblRegisterLoadCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblRegisterLoadCarrier.Location = new System.Drawing.Point(21, 58);
            this.lblRegisterLoadCarrier.Name = "lblRegisterLoadCarrier";
            this.lblRegisterLoadCarrier.Size = new System.Drawing.Size(170, 15);
            

            // 
            // lblDestination
            // 
            this.lblDestination.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblDestination.Location = new System.Drawing.Point(21, 100);
            this.lblDestination.Name = "lblDestination";
            this.lblDestination.AutoSize = false;
            this.lblDestination.Size = new System.Drawing.Size(480 - 21 - 10, 35);


            // 
            // lblPwr
            // 
            this.lblPwr.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblPwr.Location = new System.Drawing.Point(21, 140);
            this.lblPwr.Name = "lblPwr";
            this.lblPwr.Size = new System.Drawing.Size(42, 15);

            // 
            // lblPhysLoadCarrier
            // 
            this.lblPhysLoadCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblPhysLoadCarrier.Location = new System.Drawing.Point(21, 220);
            this.lblPhysLoadCarrier.Name = "lblPhysLoadCarrier";
            this.lblPhysLoadCarrier.AutoSize = false;
            this.lblPhysLoadCarrier.Size = new System.Drawing.Size(480-21-10, 35);
            
            // 
            // lblScanLoadCarrier
            // 
            this.lblScanLoadCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblScanLoadCarrier.Location = new System.Drawing.Point(21, 254);
            this.lblScanLoadCarrier.Name = "lblScanLoadCarrier";
            this.lblScanLoadCarrier.AutoSize = false;
            this.lblScanLoadCarrier.Size = new System.Drawing.Size(480 - 21 - 10, 35);

            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(21, 305);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(436, 20);
            this.txtScannedNumber.TabIndex = 69;


            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(21, 331);
            this.messageControl.MessageText = "";
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(436, 120);
            this.messageControl.TabIndex = 32;



            
            
            
            // 
            // FormValidationError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormScanPhysicalLoadCarrier";
            this.Text = "FormScanPhysicalLoadCarrier";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegisterLoadCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPwr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanLoadCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhysLoadCarrier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private Resco.Controls.CommonControls.TransparentLabel lblRegisterLoadCarrier;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        
        private Resco.Controls.CommonControls.TransparentLabel lblPwr;
        private Resco.Controls.CommonControls.TransparentLabel lblPhysLoadCarrier;
        private Resco.Controls.CommonControls.TransparentLabel lblScanLoadCarrier;
        private Resco.Controls.CommonControls.TransparentLabel lblDestination;
        private TextBox txtScannedNumber;

    }
}
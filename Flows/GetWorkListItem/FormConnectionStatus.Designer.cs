﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    partial class FormConnectionStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPhysicalLoadConnected = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOldConnection = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOldDestination = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOldPower = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNewConnection = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNewPwr = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNewDestination = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhysicalLoadConnected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldConnection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewConnection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewPwr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewDestination)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblNewPwr);
            this.touchPanel.Controls.Add(this.lblNewDestination);
            this.touchPanel.Controls.Add(this.lblNewConnection);
            this.touchPanel.Controls.Add(this.lblOldPower);
            this.touchPanel.Controls.Add(this.lblOldDestination);
            this.touchPanel.Controls.Add(this.lblMessage);
            this.touchPanel.Controls.Add(this.lblOldConnection);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblPhysicalLoadConnected);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
           
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 2);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            // lblPhysicalLoadConnected
            // 
            this.lblPhysicalLoadConnected.AutoSize = false;
            this.lblPhysicalLoadConnected.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblPhysicalLoadConnected.Location = new System.Drawing.Point(22, 88);
            this.lblPhysicalLoadConnected.Name = "lblPhysicalLoadConnected";
            this.lblPhysicalLoadConnected.Size = new System.Drawing.Size(424, 60);
            

           
            // 
            // lblMessage
            // 
            this.lblMessage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblMessage.Location = new System.Drawing.Point(21, 176);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(380, 75);
            this.lblMessage.AutoSize = false;


            // 
            // transparentLabel1
            // 
            this.lblOldConnection.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOldConnection.Location = new System.Drawing.Point(21, 279);
            this.lblOldConnection.Name = "lblOldConnection";
            this.lblOldConnection.Size = new System.Drawing.Size(90, 14);

            // 
            // lblOldDestination
            // 
            this.lblOldDestination.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblOldDestination.Location = new System.Drawing.Point(21, 309);
            this.lblOldDestination.Name = "lblOldDestination";
            this.lblOldDestination.AutoSize = false;
            this.lblOldDestination.Size = new System.Drawing.Size(480 - 21 - 10, 35);


            // 
            // lblOldPower
            // 
            this.lblOldPower.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblOldPower.Location = new System.Drawing.Point(21, 339);
            this.lblOldPower.Name = "lblOldPower";
            this.lblOldPower.Size = new System.Drawing.Size(39, 14);

            // 
            // lblNewConnection
            // 
            this.lblNewConnection.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblNewConnection.Location = new System.Drawing.Point(21, 379);
            this.lblNewConnection.Name = "lblNewConnection";
            this.lblNewConnection.Size = new System.Drawing.Size(95, 14);

            // 
            // lblNewDestination
            // 
            this.lblNewDestination.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNewDestination.Location = new System.Drawing.Point(21, 411);
            this.lblNewDestination.Name = "lblNewDestination";
            this.lblNewDestination.AutoSize = false;
            this.lblNewDestination.Size = new System.Drawing.Size(480 - 21 - 10, 35);

            // 
            // lblNewPwr
            // 
            this.lblNewPwr.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNewPwr.Location = new System.Drawing.Point(21, 441);
            this.lblNewPwr.Name = "lblNewPwr";
            this.lblNewPwr.Size = new System.Drawing.Size(39, 14);

            
            

            // 
            // FormConnectionSatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormConnectionStatus";
            this.Text = "FormConnectionStatus";
            this.touchPanel.ResumeLayout(false);
            
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPhysicalLoadConnected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldConnection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOldPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewConnection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewPwr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNewDestination)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblPhysicalLoadConnected;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblOldPower;
        private Resco.Controls.CommonControls.TransparentLabel lblOldDestination;
        private Resco.Controls.CommonControls.TransparentLabel lblMessage;
        private Resco.Controls.CommonControls.TransparentLabel lblOldConnection;
        private Resco.Controls.CommonControls.TransparentLabel lblNewPwr;
        private Resco.Controls.CommonControls.TransparentLabel lblNewDestination;
        private Resco.Controls.CommonControls.TransparentLabel lblNewConnection;

    }
}
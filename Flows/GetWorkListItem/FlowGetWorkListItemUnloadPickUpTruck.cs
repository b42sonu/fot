﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{

    public class FlowGetWorkListItemUnloadPickUpTruck : BaseFlow
    {
        private bool _isOfflineWorkListItem;
        private Stop _selectedStop;
        private List<Stop> _stops;
        private T20240_GetGoodsListFromFOTReply _responseFromLm;
        private readonly ViewCommandsGetWorkListItem _viewCommandsGetWorkListItem;
        private FlowDataGetWorkListItem _flowData;
        private readonly FlowResultGetWorkListItem _flowResult;
        private readonly ActionCommandsGetWorkListItem _actionCommandsWorkList;
        private readonly MessageHolder _messageHolder;
        public FlowGetWorkListItemUnloadPickUpTruck()
        {
            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultGetWorkListItem();
            _actionCommandsWorkList = new ActionCommandsGetWorkListItem(CommunicationClient.Instance);
            _viewCommandsGetWorkListItem = new ViewCommandsGetWorkListItem();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowWorkList.BeginFlow(IFlowData flowData)");
            _flowData = (FlowDataGetWorkListItem)flowData;

            ExecuteActionState(_flowData.Rbt == null
                                   ? FlowStateGetWorkListItem.ActionCreateManualRbt
                                   : FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt);
        }

        public void ExecuteState(FlowStateGetWorkListItem state)
        {
            if (state > FlowStateGetWorkListItem.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateGetWorkListItem)state);
        }
        private void ExecuteViewState(FlowStateGetWorkListItem stateAttemptedDelivery)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItemUnloadPickUpTruck;" + stateAttemptedDelivery);
                switch (stateAttemptedDelivery)
                {
                    case FlowStateGetWorkListItem.ViewEnterRbtManual:
                        _viewCommandsGetWorkListItem.ShowManualRbtView(_flowData, ManualRbtViewEventHandler, OperationType.UnloadPickUpTruck);
                        break;
                    
                    case FlowStateGetWorkListItem.ViewLoadLineStopList:
                        var flowDataDisplayStopList = new FlowDataDisplayStopList
                        {
                            HeaderText = _flowData.HeaderText,
                            Stops = _stops,
                            OperationType = OperationType.UnloadPickUpTruck
                        };
                        StartSubFlow<FlowDisplayStopList>(flowDataDisplayStopList, (int)FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList, Process.Inherit);
                        break;
                    case FlowStateGetWorkListItem.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowWorkListItemPickUpTruck.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateGetWorkListItem.ThisFlowComplete);
            }
        }
        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateGetWorkListItem state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItemUnloadPickUpTruck;" + state );
                    switch (state)
                    {
                        case FlowStateGetWorkListItem.ActionCreateManualRbt:
                            CreateManualRbt(_flowData.PowerUnitId,"","","");
                            state = FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt;
                            break;
                        case FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt:
                            _isOfflineWorkListItem =  _actionCommandsWorkList.IsPdaOffline(_messageHolder,_flowData.BarcodeType);
                            state = _isOfflineWorkListItem
                                        ? FlowStateGetWorkListItem.ViewEnterRbtManual
                                        : FlowStateGetWorkListItem.ActionValidateRbt;
                            break;

                        case FlowStateGetWorkListItem.ActionValidateRbt:
                            CommandsOperations.GetStopListForRbt(_flowData.Rbt, GetGoodsListRequestRequestType.StopListAmphora, out _responseFromLm,_flowData.IsForCombinationTrip,_flowData.TripType);
                            state = FlowStateGetWorkListItem.ActionProcessStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionProcessStopList:
                            _stops = CommandsOperations.ProcessGoodsListReplyToGetStops(_responseFromLm, OperationType.UnloadPickUpTruck);
                            state = FlowStateGetWorkListItem.ActionIsOperationReceived;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOperationReceived:
                            bool result = CommandsOperations.IsTripsAndStopsReturned(_stops);
                            state = result ? FlowStateGetWorkListItem.ViewLoadLineStopList : FlowStateGetWorkListItem.ViewEnterRbtManual;
                            break;

                        case FlowStateGetWorkListItem.ActionCreateStopItem:
                            _selectedStop = _actionCommandsWorkList.CreateStop(_flowData.StopId, _flowData.TripId, OperationType.UnloadPickUpTruck);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;

                        case FlowStateGetWorkListItem.ActionCopyValuesFromSelectedStopInToRbt:
                            _actionCommandsWorkList.CopyValuesFromSelectedStopInToRbt(_flowData.Rbt, _selectedStop);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;

                        case FlowStateGetWorkListItem.ActionGetWorkListItem:
                            _flowResult.WorkListItem = _actionCommandsWorkList.GetWorkListItem(_selectedStop, _flowData, CurrentProcess, null,_flowData.CurrentLocation);
                            _flowResult.State = FlowResultState.Ok;
                            _flowResult.GetWorkListItemResultType = _isOfflineWorkListItem ? GetWorkListItemResultTypes.OfflineWorkListItemCreated : GetWorkListItemResultTypes.WorkListItemCreated;
                            state = FlowStateGetWorkListItem.ThisFlowComplete;
                            break;
                      
                        case FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList:
                            Stop selectedStop;
                            _flowResult.State = SubflowResult.State;
                            state = _actionCommandsWorkList.BackFromFlowShowOperations((FlowResultDisplayStopList)SubflowResult, out selectedStop);
                            _selectedStop = selectedStop;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateGetWorkListItem.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowWorkListItemPickUpTruck.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateGetWorkListItem.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        private void CreateManualRbt(string powerUnit, string loadCarrier, string route, string tripId)
        {
            _flowData.BarcodeType = BarcodeType.Rbt;

            //If rbt is null then it means user entered the power unit id.
            if(_flowData.Rbt == null)
            {
                var loadCarrierId = _flowData.Rbt != null ? _flowData.Rbt.PowerUnitId : _flowData.PowerUnitId;

                var rbt = new BarcodeRouteCarrierTrip(route, tripId,
                                               powerUnit, loadCarrierId);

                _flowData.Rbt = rbt;
                _flowData.PowerUnitId = powerUnit;
                _flowData.LoadCarrierId = loadCarrierId;
                _flowData.RouteId = route;
                _flowData.TripId = tripId;
            }
            else
            {
                _flowData.TripId = tripId;
            }

            
        
        }


        /// <summary>
        /// Handler which will handle all type of events from manual rbt view..
        /// </summary>
        private void ManualRbtViewEventHandler(int manualRbtViewEvents, params object[] data)
        {
            switch (manualRbtViewEvents)
            {
                case ManualRbtViewEvent.Skip:
                case ManualRbtViewEvent.Ok:
                    CreateManualRbt(Convert.ToString(data[0]), Convert.ToString(data[1]), Convert.ToString(data[2]),
                                    Convert.ToString(data[3]));
                    CurrentView.AutoShow = false;
                    ExecuteActionState(FlowStateGetWorkListItem.ActionCreateStopItem);
                    break;
                
                case ManualRbtViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteViewState(FlowStateGetWorkListItem.ThisFlowComplete);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ManualRbtViewEventHandler");
                    break;
            }
        }
        

        /// <summary>
        /// This methos ends the Flow WorkListItemPickUpTruck .
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowWorkListItemPickUpTruck");

            switch (_flowResult.State)
            {
                case FlowResultState.Ok:
                    if (_flowData.BarcodeType != BarcodeType.LoadCarrier)
                    {
                        if (_flowResult.GetWorkListItemResultType == GetWorkListItemResultTypes.OfflineWorkListItemCreated)
                            SoundUtil.Instance.PlayWarningSound();
                        else
                            SoundUtil.Instance.PlaySuccessSound();    
                    }
                    
                    break;
                case FlowResultState.Duplicate:
                case FlowResultState.Cancel:
                case FlowResultState.Warning:
                    SoundUtil.Instance.PlayWarningSound();
                    break;

                case FlowResultState.Error:
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;
            }

            BaseModule.EndSubFlow(_flowResult);
        }
    }
}


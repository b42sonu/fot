﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    class ViewCommandsGetWorkListItem
    {
        /// <summary>
        /// method for show Manual Rbt view...
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="flowData"></param>
        /// <param name="operationType"> </param>
        /// <returns></returns>
        internal void ShowManualRbtView(FlowDataGetWorkListItem flowData, ViewEventDelegate viewEventHandler, OperationType operationType)
        {
            var formDataManualRbt = new FormDataManualRbt
                                        {
                                            PowerUnitId = string.IsNullOrEmpty(flowData.PowerUnitId) ? "" : flowData.PowerUnitId,
                                            TripId = string.IsNullOrEmpty(flowData.TripId) ? "" : flowData.TripId,
                                            OperationType = operationType,
                                            HeaderText = flowData.HeaderText,
                                            OrgUnitName = flowData.CurrentLocation != null ? flowData.CurrentLocation.Name : ""
                                        };
            var view = ViewCommands.ShowView<FormManualRbt>();
            view.SetEventHandler(viewEventHandler);
            view.UpdateView(formDataManualRbt);    

        }


        

        /// <summary>
        /// method for show Validation Error view...
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="messageHolder"> </param>
        /// <param name="flowData"> </param>
        /// <returns></returns>
        internal FormValidationError ShowValidationErrorView(ViewEventDelegate viewEventHandler, MessageHolder messageHolder, FlowDataGetWorkListItem flowData)
        {
           var formDataValidationError = new FormDataValidationError
                                              {
                                                  MessageHolder = messageHolder,
                                                  OrgUnitName = flowData.CurrentLocation != null ? flowData.CurrentLocation.Name : "",
                                                  HeaderText = flowData.HeaderText
                                              };
            var view = ViewCommands.ShowView<FormValidationError>(formDataValidationError);
            view.AutoShow = true;
            view.SetEventHandler(viewEventHandler);
            return view;
        }

        /// <summary>
        /// method for show connection Status view...
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="flowData"> </param>
        /// <param name="loadCarrierEntity"> </param>
        /// <param name="physicalLoadCarrierEntity"> </param>
        /// <returns></returns>
        internal FormConnectionStatus ShowConnectionStatusView(ViewEventDelegate viewEventHandler, FlowDataGetWorkListItem flowData, Entity.EntityMap.LoadCarrier loadCarrierEntity, Entity.EntityMap.LoadCarrier physicalLoadCarrierEntity)
        {
           //New connection details
            LogicalLoadCarrierType newLogicalLoadCarrier = null;
            if (loadCarrierEntity.LogicalLoadCarrierDetails != null && loadCarrierEntity.LogicalLoadCarrierDetails != null)
                newLogicalLoadCarrier = loadCarrierEntity.LogicalLoadCarrierDetails[0];

            //Old connection details
            LogicalLoadCarrierType oldLogicalLoadCarrier = null;
            if (physicalLoadCarrierEntity.LogicalLoadCarrierDetails != null && physicalLoadCarrierEntity.LogicalLoadCarrierDetails != null)
                oldLogicalLoadCarrier = physicalLoadCarrierEntity.LogicalLoadCarrierDetails[0];


            var formDataConnectionStatus = new FormDataConnectionStatus
                                               {
                                                   HeaderText = flowData.HeaderText,
                                                   OrgUnitName = flowData.CurrentLocation != null ? flowData.CurrentLocation.Name : "",
                                                   NewDestination = newLogicalLoadCarrier != null ? newLogicalLoadCarrier.DestinationName1 + " " + newLogicalLoadCarrier.DestinationName2 : "",
                                                   NewLoadCarrier = newLogicalLoadCarrier != null ? newLogicalLoadCarrier.LogicalLoadCarrierId : "",
                                                   OldDestination = oldLogicalLoadCarrier != null ? oldLogicalLoadCarrier.DestinationName1 + " " + oldLogicalLoadCarrier.DestinationName2 : "",
                                                   OldLoadCarrier = oldLogicalLoadCarrier != null ? oldLogicalLoadCarrier.LogicalLoadCarrierId : ""

                                               };
            var view = ViewCommands.ShowView<FormConnectionStatus>(formDataConnectionStatus);
            view.SetEventHandler(viewEventHandler);
            return view;
        }

        /// <summary>
        /// method for show Scan Physical LoadCarrier view...
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="flowData"> </param>
        /// <param name="loadCarrierEntity"> </param>
        /// <param name="messageHolder"> </param>
        /// <returns></returns>
        internal FormScanPhysicalLoadCarrier ShowScanPhysicalLoadCarrierView(ViewEventDelegate viewEventHandler, FlowDataGetWorkListItem flowData, Entity.EntityMap.LoadCarrier loadCarrierEntity, MessageHolder messageHolder)
        {
            LogicalLoadCarrierType logicalLoadCarrier = null;
            if (loadCarrierEntity.LogicalLoadCarrierDetails != null && loadCarrierEntity.LogicalLoadCarrierDetails != null)
                logicalLoadCarrier = loadCarrierEntity.LogicalLoadCarrierDetails[0];

            var formDataScanPhysicalLoadCarrier = new FormDataScanPhysicalLoadCarrier
                                                      {
                                                          HeaderText = flowData.HeaderText,
                                                          OrgUnitName = flowData.CurrentLocation != null ? flowData.CurrentLocation.Name : "",
                                                          LoadCarrierId = logicalLoadCarrier != null ? logicalLoadCarrier.LogicalLoadCarrierId : "",
                                                          Destination = logicalLoadCarrier != null ? logicalLoadCarrier.DestinationName1 + " " + logicalLoadCarrier.DestinationName2 : "",
                                                          MessageHolder = messageHolder
                                                      };

            var view = ViewCommands.ShowView<FormScanPhysicalLoadCarrier>(formDataScanPhysicalLoadCarrier);
            view.SetEventHandler(viewEventHandler);
            return view;
        }
    }
}

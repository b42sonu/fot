﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    public partial class FormScanPhysicalLoadCarrier : BaseBarcodeScanForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        
        public FormScanPhysicalLoadCarrier()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            InitOnScreenKeyBoardProperties();
            AddButtons();
        }

        private void SetTextToControls()
        {
            lblRegisterLoadCarrier.Text = GlobalTexts.LogLoadCarrierRegistered;
            lblScanLoadCarrier.Text = GlobalTexts.ScanOrEnterPhyLoadCarrier;
            lblPhysLoadCarrier.Text = GlobalTexts.PhyLoadCarrier;
        }

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.AddButtonsAccordintToProcess()");
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.SetTextToGui");
            }
        }

        public override void OnShow(IFormData formData)
        {
            //Enable Scanner
            base.OnShow(formData);
            messageControl.ClearMessage();
            SetKeyboardState(KeyboardState.Numeric);
            OnUpdateView(formData);

        }

        public override void OnUpdateView(IFormData iformData)
        {
            var formData = (FormDataScanPhysicalLoadCarrier)iformData;
            if (formData != null)
            {
                lblPwr.Text = string.Format(GlobalTexts.Pwr, formData.LoadCarrierId);
                var destinationText = string.Format(GlobalTexts.Destination, formData.Destination) ;
                if (destinationText.Length>35)
                    destinationText = destinationText.Substring(0, 30) + "....";
                lblDestination.Text = destinationText;
                lblModuleName.Text = formData.HeaderText;
                if (formData.MessageHolder.State != MessageState.Error && formData.MessageHolder.State != MessageState.Warning)
                    txtScannedNumber.Text = "";

                PopulateMessage(formData.MessageHolder);

                _multiButtonControl.SetButtonEnabledState(ButtonOk, txtScannedNumber.Text != string.Empty);
            }
        }



        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };

            txtScannedNumber.Tag = keyBoardProperty;
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOk, txtScannedNumber.Text.Trim() != string.Empty);
            if (messageControl != null)
                messageControl.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text.Trim() != string.Empty)
            {
                BarcodeEnteredManually(textentered);
            }

        }

        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(ScanPhysicalLoadCarrierViewEvent.Ok, scannerOutput);
        }

       
        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
                messageControl.ShowMessage(messageHolder);
            else
                messageControl.ClearMessage();
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            ViewEvent.Invoke(ScanPhysicalLoadCarrierViewEvent.BarcodeScanned, scannerOutput);
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormScanPhysicalLoadCarrier method FormScanPhysicalLoadCarrier.ButtonBackClick");
                ViewEvent.Invoke(ScanPhysicalLoadCarrierViewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanPhysicalLoadCarrier.ButtonBackClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormScanPhysicalLoadCarrier method FormScanPhysicalLoadCarrier.ButtonOkClick");
                BarcodeEnteredManually(txtScannedNumber.Text);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanPhysicalLoadCarrier.ButtonOkClick");
            }
        }
    }

    public class FormDataScanPhysicalLoadCarrier : BaseFormData
    {
        public string LoadCarrierId { get; set; }
        public string Destination { get; set; }
        public MessageHolder MessageHolder { get; set; }
    }
    public class ScanPhysicalLoadCarrierViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;
        public const int BarcodeScanned = 2;
    }
}
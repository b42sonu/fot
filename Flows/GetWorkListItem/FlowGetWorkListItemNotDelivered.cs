﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    class FlowGetWorkListItemNotDelivered : BaseFlow
    {
        
        private Stop _selectedStop;
        private List<Stop> _stops;
        private T20240_GetGoodsListFromFOTReply _responseFromLm;
        private FlowDataGetWorkListItem _flowData;
        private readonly FlowResultGetWorkListItem _flowResult;
        private readonly ActionCommandsGetWorkListItem _actionCommandsWorkList;
        private readonly MessageHolder _messageHolder;

        public FlowGetWorkListItemNotDelivered()
        {
            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultGetWorkListItem();
            _actionCommandsWorkList = new ActionCommandsGetWorkListItem(CommunicationClient.Instance);
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowGetWorkListItemNotDelivered.BeginFlow(IFlowData flowData)");
            _flowData = (FlowDataGetWorkListItem)flowData;
            ExecuteActionState(FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt);
        }

        public void ExecuteState(FlowStateGetWorkListItem state)
        {
            if (state > FlowStateGetWorkListItem.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateGetWorkListItem)state);
        }

        private void ExecuteViewState(FlowStateGetWorkListItem stateAttemptedDelivery)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItemNotDelivered;" + stateAttemptedDelivery); 
                switch (stateAttemptedDelivery)
                {
                    
                    case FlowStateGetWorkListItem.ViewLoadLineStopList:
                        var flowDataDisplayStopList = new FlowDataDisplayStopList
                        {
                            HeaderText = _flowData.HeaderText,
                            Stops = _stops,
                            OperationType = OperationType.UnloadPickUpTruck
                        };
                        StartSubFlow<FlowDisplayStopList>(flowDataDisplayStopList, (int)FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList, Process.Inherit);
                        break;
                    case FlowStateGetWorkListItem.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowGetWorkListItemNotDelivered.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateGetWorkListItem.ThisFlowComplete);
            }
        }
        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateGetWorkListItem state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowGetWorkListItemNotDelivered;" + state);
                    bool result;
                    switch (state)
                    {
                        case FlowStateGetWorkListItem.ActionIsOfflineInValidateRbt:
                              _actionCommandsWorkList.IsPdaOffline(_messageHolder,_flowData.BarcodeType);
                            state =  FlowStateGetWorkListItem.ActionValidateRbt;
                            break;

                        case FlowStateGetWorkListItem.ActionValidateRbt:
                            CommandsOperations.GetStopListForPowerUnit(_flowData.PowerUnitId, GetGoodsListRequestRequestType.StopListAmphora, out _responseFromLm, string.Empty);
                            state = FlowStateGetWorkListItem.ActionProcessStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionProcessStopList:
                            _stops = CommandsOperations.ProcessGoodsListReplyToGetStops(_responseFromLm, OperationType.UnloadPickUpTruck);
                            state = FlowStateGetWorkListItem.ActionIsOperationReceived;
                            break;

                        case FlowStateGetWorkListItem.ActionIsOperationReceived:
                            result = CommandsOperations.IsTripsAndStopsReturned(_stops);
                            if (!result)
                            {
                                _flowResult.State = FlowResultState.Error;
                                _flowResult.Message = GlobalTexts.NoReplyFromServer;
                            }
                            state = result ? FlowStateGetWorkListItem.ActionIsSingleOperationReceived : FlowStateGetWorkListItem.ThisFlowComplete;//complete if no operations
                            break;

                        case FlowStateGetWorkListItem.ActionIsSingleOperationReceived:
                            result = _actionCommandsWorkList.IsSingleOperationReceived(_stops, out _selectedStop);
                            state = result ? FlowStateGetWorkListItem.ActionGetWorkListItem : FlowStateGetWorkListItem.ViewLoadLineStopList;
                            break;

                        case FlowStateGetWorkListItem.ActionCreateStopItem:
                            _selectedStop = _actionCommandsWorkList.CreateStop(_flowData.StopId, _flowData.TripId, OperationType.UnloadPickUpTruck);
                            state = FlowStateGetWorkListItem.ActionGetWorkListItem;
                            break;

                        case FlowStateGetWorkListItem.ActionGetWorkListItem:
                            _flowResult.WorkListItem = _actionCommandsWorkList.GetWorkListItem(_selectedStop, _flowData, CurrentProcess, null,_flowData.CurrentLocation);
                            _flowResult.State = FlowResultState.Ok;
                            _flowResult.GetWorkListItemResultType = GetWorkListItemResultTypes.WorkListItemCreated;
                            state = FlowStateGetWorkListItem.ThisFlowComplete;
                            break;
                      
                        case FlowStateGetWorkListItem.ActionBackFromFlowDisplayStopList:
                            Stop selectedStop;
                            _flowResult.State = SubflowResult.State;
                            state = _actionCommandsWorkList.BackFromFlowShowOperations((FlowResultDisplayStopList)SubflowResult, out selectedStop);
                            _selectedStop = selectedStop;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateGetWorkListItem.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowGetWorkListItemNotDelivered.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateGetWorkListItem.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }
        
        

        /// <summary>
        /// This methos ends the Flow WorkListItemPickUpTruck .
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowGetWorkListItemNotDelivered");

            switch (_flowResult.State)
            {
                case FlowResultState.Ok:
                    SoundUtil.Instance.PlaySuccessSound();
                    break;
                case FlowResultState.Duplicate:
                case FlowResultState.Cancel:
                case FlowResultState.Warning:
                    SoundUtil.Instance.PlayWarningSound();
                    break;

                case FlowResultState.Error:
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;
            }

            BaseModule.EndSubFlow(_flowResult);
        }

    }
}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    partial class FormValidationError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnit)).BeginInit();
            
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblOrgUnit);
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            
            // 
            // lblHeading
            // 
            this.lblOrgUnit.AutoSize = false;
            this.lblOrgUnit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgUnit.Location = new System.Drawing.Point(3, 42);
            this.lblOrgUnit.Name = "lblOrgUnit";
            this.lblOrgUnit.Size = new System.Drawing.Size(477, 33);
            this.lblOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;   
            
            
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(135, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(22, 130);
            this.messageControl.MessageText = "";
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(436, 120);
            this.messageControl.TabIndex = 32;
            // 
            // FormValidationError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormValidationError";
            this.Text = "FormValidationError";
            this.touchPanel.ResumeLayout(false);
            
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgUnit;
        
        
    }
}
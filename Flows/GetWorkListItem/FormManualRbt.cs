﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using System;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    public partial class FormManualRbt : BaseForm
    {
        private MultiButtonControl _tabButtons;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        private const string ButtonSkip = "ButtonSkip";
        private FormDataManualRbt _formData;
        public FormManualRbt()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
            AddButtons();
        }

        private void AddButtons()
        {
            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Skip, ButtonSkipClick, ButtonSkip));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                Title = txtBoxPowerUnit.Text,
                TextChanged = PowerUnitTextChanged
            };
            txtBoxPowerUnit.Tag = keyBoardProperty;
        }

        private void PowerUnitTextChanged(bool userFinishedTyping, string textentered)
        {
            if (string.IsNullOrEmpty(txtBoxPowerUnit.Text))
                _tabButtons.SetButtonEnabledState(ButtonOk, false);
            else
                _tabButtons.SetButtonEnabledState(ButtonOk, true);
        }

        private void SetTextToGui()
        {
            if (ModelUser.UserProfile.OrgUnitName != null) lblOrgUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblTrip.Text = GlobalTexts.TripId;
            lblRoute.Text = GlobalTexts.RouteId;
            lblLoadCarrierId.Text = GlobalTexts.LoadCarrier;
            lblPowerUnit.Text = GlobalTexts.PowerUnitId;
            lblEnterTripInfo.Text = GlobalTexts.EnterTripInformation;

            var onScreenKeyBoardPropertiesTrip = new OnScreenKeyboardProperties(GlobalTexts.TripId, KeyPadTypes.QwertyUpperCase);
            txtBoxTrip.Tag = onScreenKeyBoardPropertiesTrip;
            var onScreenKeyBoardPropertiesLoadCarrier = new OnScreenKeyboardProperties(GlobalTexts.LoadCarrier, KeyPadTypes.QwertyUpperCase);
            txtBoxLoadCarrierId.Tag = onScreenKeyBoardPropertiesLoadCarrier;
            var onScreenKeyBoardPropertiesPowerUnit = new OnScreenKeyboardProperties(GlobalTexts.PowerUnitId, KeyPadTypes.QwertyUpperCase);
            txtBoxPowerUnit.Tag = onScreenKeyBoardPropertiesPowerUnit;
            var onScreenKeyBoardPropertiesRoute = new OnScreenKeyboardProperties(GlobalTexts.RouteId, KeyPadTypes.QwertyUpperCase);
            txtBoxRoute.Tag = onScreenKeyBoardPropertiesRoute;
        }


        public override void OnShow(IFormData iformData)
        {
            SetKeyboardState(KeyboardState.AlphaUpperCase);
            OnUpdateView(iformData);
        }
        public override void OnUpdateView(IFormData iformData)
        {

            _formData = (FormDataManualRbt)iformData;

            if (_formData != null)
            {
                _tabButtons.SetButtonEnabledState(ButtonSkip, BaseModule.CurrentFlow.CurrentProcess == Process.UnloadPickUpTruck);
                lblModuleName.Text = _formData.HeaderText;
                lblOrgUnitName.Text = _formData.OrgUnitName;
                ResetControl(_formData.OperationType);
                if (string.IsNullOrEmpty(_formData.LoadCarrierId))
                    _formData.LoadCarrierId = string.Empty;
                if (string.IsNullOrEmpty(_formData.PowerUnitId))
                    _formData.PowerUnitId = string.Empty;
                if (string.IsNullOrEmpty(_formData.RouteId))
                    _formData.RouteId = string.Empty;
                if (string.IsNullOrEmpty(_formData.TripId))
                    _formData.TripId = string.Empty;

                txtBoxLoadCarrierId.Text = _formData.LoadCarrierId.ToUpper();
                txtBoxPowerUnit.Text = _formData.PowerUnitId.ToUpper();
                txtBoxRoute.Text = _formData.RouteId.ToUpper();
                txtBoxTrip.Text = _formData.TripId.ToUpper();
            }
            else
            {
                txtBoxLoadCarrierId.Text = string.Empty;
                txtBoxPowerUnit.Text = string.Empty;
                txtBoxRoute.Text = string.Empty;
                txtBoxTrip.Text = string.Empty;
            }

            if (string.IsNullOrEmpty(txtBoxPowerUnit.Text))
                _tabButtons.SetButtonEnabledState(ButtonOk, false);
            else
                _tabButtons.SetButtonEnabledState(ButtonOk, true);

            if (_formData != null && _formData.OperationType == OperationType.UnloadPickUpTruck)
                txtBoxTrip.Focus();
            else
                txtBoxPowerUnit.Focus();
        }


        private void ResetControl(OperationType operationType)
        {
            switch (operationType)
            {
                case OperationType.UnloadPickUpTruck:
                    lblLoadCarrierId.Visible = false;
                    lblPowerUnit.Visible = false;
                    lblRoute.Visible = false;
                    txtBoxLoadCarrierId.Visible = false;
                    txtBoxPowerUnit.Visible = false;
                    txtBoxRoute.Visible = false;
                    lblTrip.Location = lblPowerUnit.Location;
                    txtBoxTrip.Location = txtBoxPowerUnit.Location;
                    txtBoxTrip.Focus();
                    break;
                default:
                    lblLoadCarrierId.Visible = true;
                    lblPowerUnit.Visible = true;
                    lblRoute.Visible = true;
                    lblTrip.Visible = true;

                    txtBoxLoadCarrierId.Visible = true;
                    txtBoxPowerUnit.Visible = true;
                    txtBoxRoute.Visible = true;
                    txtBoxTrip.Visible = true;
                    lblTrip.Location = new System.Drawing.Point(15, 275);
                    txtBoxTrip.Location = new System.Drawing.Point(176, 275);
                    break;
            }
        }


        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormManualRbt method FormManualRbt.ButtonBackClick");
                ViewEvent.Invoke(ManualRbtViewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.ButtonBackClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormManualRbt method FormManualRbt.ButtonConfirmClick");


                //If user entered the values then convert values in field to upper case
                var powerUnitId = string.IsNullOrEmpty(txtBoxPowerUnit.Text)
                                      ? string.Empty
                                      : txtBoxPowerUnit.Text.ToUpper();
                var loadCarrierId = string.IsNullOrEmpty(txtBoxLoadCarrierId.Text)
                                      ? string.Empty
                                      : txtBoxLoadCarrierId.Text.ToUpper();

                var tripId = string.IsNullOrEmpty(txtBoxTrip.Text)
                                      ? string.Empty
                                      : txtBoxTrip.Text.ToUpper();

                var routeId = string.IsNullOrEmpty(txtBoxRoute.Text)
                                      ? string.Empty
                                      : txtBoxRoute.Text.ToUpper();

                ViewEvent.Invoke(ManualRbtViewEvent.Ok, powerUnitId, loadCarrierId, routeId, tripId);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.ButtonConfirmClick");
            }
        }

        private void ButtonSkipClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormManualRbt method FormManualRbt.ButtonSkip");
                ViewEvent.Invoke(ManualRbtViewEvent.Skip, txtBoxPowerUnit.Text, txtBoxLoadCarrierId.Text, txtBoxRoute.Text, txtBoxTrip.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.ButtonSkip");
            }
        }

        private void TxtBoxPowerUnitKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    txtBoxLoadCarrierId.Focus();

                if (string.IsNullOrEmpty(txtBoxPowerUnit.Text))
                    _tabButtons.SetButtonEnabledState(ButtonOk, false);
                else
                    _tabButtons.SetButtonEnabledState(ButtonOk, true);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.TxtBoxPowerUnitKeyUp");
            }
        }

        private void TxtBoxLoadCarrierIdKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    txtBoxRoute.Focus();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.TxtBoxLoadCarrierIdKeyUp");
            }
        }


        private void TxtBoxRouteKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    txtBoxTrip.Focus();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.TxtBoxRouteKeyUp");
            }
        }


        private void TxtBoxTripKeyUp(object sender, KeyEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormManualRbt.TxtBoxTripKeyUp");
            }
        }

    }
    public class FormDataManualRbt : BaseFormData
    {
        public string PowerUnitId { get; set; }
        public string LoadCarrierId { get; set; }
        public string RouteId { get; set; }
        public string TripId { get; set; }
        public OperationType OperationType { get; set; }
    }
    public class ManualRbtViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;
        public const int Skip = 2;
    }
}
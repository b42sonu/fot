﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem
{
    public partial class FormConnectionStatus : BaseForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        public FormConnectionStatus()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            AddButtons();
        }

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.AddButtonsAccordintToProcess()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.KeepOld, ButtonKeepOldClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.NewConn, ButtonKeepNewClick, TabButtonType.Confirm) { Name = ButtonOk });
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.AddButtons");
            }
        }


        private void SetTextToControls()
        {
            lblPhysicalLoadConnected.Text = GlobalTexts.PhysicalCarrierAlreadyConnected;
            lblMessage.Text =String.Format(GlobalTexts.WishToKeepExistingConnection , Environment.NewLine);
            lblOldConnection.Text = GlobalTexts.OldConnection;
            lblNewConnection.Text = GlobalTexts.NewConnection;
            
        }

        public override void OnShow(IFormData iformData)
        {
            OnUpdateView(iformData);
        }
        public override void OnUpdateView(IFormData iformData)
        {
            var formData = (FormDataConnectionStatus) iformData;
            if (formData != null)
            {
                var oldDestinationText= String.Format(GlobalTexts.Destination, formData.OldDestination);
                if (oldDestinationText.Length > 35)
                    oldDestinationText = oldDestinationText.Substring(0, 30) + "....";
                lblOldDestination.Text = oldDestinationText;

                 lblOldPower.Text = String.Format(GlobalTexts.Pwr, formData.OldLoadCarrier);

               var newDestinationText = String.Format(GlobalTexts.Destination, formData.NewDestination);
                if (newDestinationText.Length > 35)
                    newDestinationText = newDestinationText.Substring(0, 30) + "....";
                lblNewDestination.Text = newDestinationText;

                lblNewPwr.Text = String.Format(GlobalTexts.Pwr, formData.NewLoadCarrier);
                lblModuleName.Text = formData.HeaderText;
            }
        }

        private void ButtonKeepOldClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormConnectionStatus method FormConnectionStatus.ButtonKeepOldClick");
                ViewEvent.Invoke(ConnectionStatusViewEvent.KeepOld);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConnectionStatus.ButtonBackClick");
            }
        }

        private void ButtonKeepNewClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormConnectionStatus method FormConnectionStatus.ButtonKeepNewClick");
                ViewEvent.Invoke(ConnectionStatusViewEvent.KeepNew);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormConnectionStatus.ButtonConfirmClick");
            }
        }
    }

    public class FormDataConnectionStatus : BaseFormData
    {
        public string OldDestination { get; set; }
        public string OldLoadCarrier { get; set; }
        public string NewDestination { get; set; }
        public string NewLoadCarrier { get; set; }
    }
    public class ConnectionStatusViewEvent : ViewEvent
    {
        public const int KeepOld = 0;
        public const int KeepNew = 1;
    }
}
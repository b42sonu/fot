﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList
{
    /// <summary>
    /// Enum which specifies the collection of state for flow FlowDisplayStopList.
    /// </summary>
    public enum FlowStateDisplayStopList
    {
        ActionRefreshBindingData,

        ViewCommands,
        ViewShowLoadingStops,

        ThisFlowComplete
    }

    /// <summary>
    /// This class acts as initial data for flow, FlowDisplayStopList.
    /// </summary>
    public class FlowDataDisplayStopList : BaseFlowData
    {
        #region IForm Members

        public OperationType OperationType;
        public List<Stop> Stops;

        #endregion
    }

    /// <summary>
    /// This class acts as , result from flow FlowDisplayStopList when it will come back to parent flow which
    /// initialized this flow.
    /// </summary>
    public class FlowResultDisplayStopList : BaseFlowResult
    {
        public bool LoadWithOutStop { get; set; }
    }

    /// <summary>
    /// This class acts as a flow for displaying stop list for loading or unloading
    /// </summary>
    public class FlowDisplayStopList : BaseFlow
    {

        #region Private, readonly and constant variables

        private readonly FlowResultDisplayStopList _flowResult;
        private FlowDataDisplayStopList _flowData;

        #endregion

        #region Methods and Functions
        /// <summary>
        /// This is the default constructor for flow FlowDisplayStopList.
        /// </summary>
        public FlowDisplayStopList()
        {
            _flowResult = new FlowResultDisplayStopList();
        }


        /// <summary>
        /// This method initializes the flow with required initial data.
        /// </summary>
        /// <param name="flowData">Specifies the flow data supplied by parent flow.</param>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "initializing flow FlowDisplayStopList.");

            _flowData = (FlowDataDisplayStopList)flowData;

            ExecuteState(FlowStateDisplayStopList.ViewShowLoadingStops);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateDisplayStopList)state);
        }


        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowStateLoading, which acts as name for next command.</param>
        public void ExecuteState(FlowStateDisplayStopList state)
        {
            if (state > FlowStateDisplayStopList.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        /// <summary>
        /// This method have responsibility to execute, view commands, flow commands.
        /// </summary>
        /// <param name="state">Specifies the state/command that need to execute.</param>
        private void ExecuteViewState(FlowStateDisplayStopList state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowDisplayStopList;" + state); 
                switch (state)
                {
                    case FlowStateDisplayStopList.ViewShowLoadingStops:
                        ViewCommandsLoadingStops.ShowStopList(LoadingStopListViewEventHandler, _flowData.Stops, _flowData.OperationType);
                        break;

                    case FlowStateDisplayStopList.ThisFlowComplete:
                        EndFlow();
                        break;

                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowDisplayStopList.ExecuteViewState");
                _flowResult.State=FlowResultState.Exception;
                ExecuteState(FlowStateDisplayStopList.ThisFlowComplete);
            }
        }



        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateDisplayStopList state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowDisplayStopList;" + state); 
                    switch (state)
                    {

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;

                    }
                } while (state < FlowStateDisplayStopList.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowDisplayStopList.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state=FlowStateDisplayStopList.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        /// <summary>
        /// This method acts as common event handler for, all evets raised from FormDisplayStopList. 
        /// screen.
        /// </summary>
        /// <param name="loadingStopListEvent">Specifies the value from enum LoadingStopListEvent which corresponds
        /// to event raised on screen.</param>
        /// <param name="data">Specifies list of parameters as supplied from, form.</param>
        public void LoadingStopListViewEventHandler(int loadingStopListEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDisplayStopList.LoadingStopListViewEventHandler");
            switch ((LoadingStopListEvent)loadingStopListEvent)
            {
                case LoadingStopListEvent.LoadWithoutStop:
                    ModelMain.SelectedOperationProcess.OperationId = "Unknown";
                    ModelMain.SelectedOperationProcess.StopId = string.Empty;
                    ModelMain.SelectedOperationProcess.TripId = string.Empty;
                    _flowResult.LoadWithOutStop = true;
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStateDisplayStopList.ThisFlowComplete);
                    break;

                case LoadingStopListEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStateDisplayStopList.ThisFlowComplete);
                    break;

                case LoadingStopListEvent.Scan:
                    ModelMain.SelectedOperationProcess.StopId = data[0].ToString();
                    ModelMain.SelectedOperationProcess.TripId = data[1].ToString();
                    ModelMain.SelectedOperationProcess.OperationId = string.Format("{0}_{1}", data[0], data[1]);
                    ModelMain.SelectedOperationProcess.Location = data[2].ToString();
                    ModelMain.SelectedOperationProcess.RouteId = data[3].ToString();
                    ModelMain.SelectedOperationProcess.PowerUnitId = data[4].ToString();
                    ModelMain.SelectedOperationProcess.LogicalLoadCarrierId = data[5].ToString();
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStateDisplayStopList.ThisFlowComplete);
                    break;
            }
        }


        /// <summary>
        /// This method, does cleanup action for the flow.
        /// </summary>
        public override void CleanupFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDisplayStopList.CleanupFlow");
        }

        /// <summary>
        /// This method ends the FlowLoadList.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDisplayStopList.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }

        #endregion
    }

}

﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList
{
    /// <summary>
    /// This class acts as initial data for screen FormDisplayStopList.
    /// </summary>
    public class FormData : BaseFormData
    {
        public List<Stop> OperationStops;
        public OperationType OperationType;
    }

    /// <summary>
    /// This class, shows the list of stops returned from LM.
    /// </summary>
    public partial class FormDisplayStopList : BaseForm
    {

        #region Private, Readonly and Constant variables
        private const string ButtonBack = "Back";
        private const string ButtonScan = "Scan";
        private const string ButtonLoadWithoutStop = "Stop";
        private MultiButtonControl _tabButtons;
        private string _selectedStopId = string.Empty;
        private string _selectedTripId = string.Empty;
        private string _location = string.Empty;
        private string _routeId = string.Empty;
        private string _powerUnitId = string.Empty;
        private string _loadCarrierId = string.Empty;
        #endregion


        #region Methods and Functions

        /// <summary>
        /// Default constructor for class.
        /// </summary>
        public FormDisplayStopList()
        {
            InitializeComponent();
            SetTextToGui();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            AddButtons();
        }

        private void AddButtons()
        {

            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, ButtonBack) { ButtonType = TabButtonType.Cancel });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.WithoutStop, ButtonLoadWithOutStopClick, ButtonLoadWithoutStop) { Visible = false });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonScanClick, ButtonScan) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        private void SetTextToGui()
        {
            lblStopIdHeading.Text = GlobalTexts.StopId + ":";
            labelModuleName.Text = GlobalTexts.Stops;
            lblTripIdHead.Text = GlobalTexts.TripId + ":";
        }

        /// <summary>
        /// This method takes object of FormData class as parameter, and does following.
        /// 1) Hides the btnLoadWithoutStop, if screen is not opening for loading stops.
        /// 2) Calls OnUpdateView method by passing object of Formdata to it.
        /// </summary>
        /// <param name="iformData">Specifies the object of FormData.</param>
        public override void OnShow(IFormData iformData)
        {
            if (iformData == null) return;
            OnUpdateView(iformData);
        }

        public override void ShowingView()
        {
            base.ShowingView();
            listOperations.Focus();
        }

        /// <summary>
        /// This method refreshes the existing list of operation, with specified list of operations
        /// </summary>
        public override void OnUpdateView(IFormData iformData)
        {
            if (iformData == null) return;
            var formData = (FormData)iformData;
            if (formData.OperationStops == null || formData.OperationStops.Count == 0)
                ClearOperationDetail();

            BindOperations(formData.OperationStops);
            SetButtonTextAndVisibility(formData);
        }

        private void SetButtonTextAndVisibility(FormData formData)
        {
            _tabButtons.SetButtonEnabledState(ButtonLoadWithoutStop, false);


            if (formData != null)
            {
                _tabButtons.SetButtonEnabledState(ButtonScan, formData.OperationStops.Count > 0);
                switch (formData.OperationType)
                {
                    case OperationType.UnloadPickUpTruck:
                        _tabButtons.SetButtonEnabledState(ButtonLoadWithoutStop, true);
                        _tabButtons.GetButton(ButtonLoadWithoutStop).Text = GlobalTexts.NewStop;
                        break;
                    case OperationType.Loading:
                        _tabButtons.SetButtonEnabledState(ButtonLoadWithoutStop, true);
                        break;
                }
            }
        }

        /// <summary>
        /// This method clears the TripId and StopId details at the bottom.
        /// </summary>
        private void ClearOperationDetail()
        {
            lblTripId.Text = string.Empty;
            lblStopId.Text = string.Empty;
        }

        /// <summary>
        /// This method shows the TripId and StopId for selected operation.
        /// </summary>
        /// <param name="e"></param>
        private void ShowOperationDetail(RowEventArgs e)
        {
            lblTripId.Text = Convert.ToString(e.DataRow["TripId"]);
            lblStopId.Text = Convert.ToString(e.DataRow["StopId"]);
        }

        /// <summary>
        /// This method runs when user clicks any operation in list of operations. it deselects
        /// all rows in list of operations.
        /// </summary>
        private void DeSelectOtherRows()
        {
            for (var i = 0; i <= listOperations.DataRows.Count - 1; i++)
            {
                var row = listOperations.DataRows[i];
                row.Selected = false;
            }
        }

        /// <summary>
        /// This method binds the specified list of operations to advance list
        /// </summary>
        private void BindOperations(List<Stop> loadingOperations)
        {
            listOperations.BeginUpdate();
            listOperations.DataSource = loadingOperations;
            listOperations.EndUpdate();
        }

        /// <summary>
        /// This method does following:
        /// 1) Deselect all rows in lstOperations.
        /// 2) Selects the current row.
        /// 3) Get the stopId and tripId from selected row.
        /// 4) Shows the operation details at the bottom.
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSelectedRow(RowEventArgs e)
        {
            DeSelectOtherRows();
            e.DataRow.Selected = true;
            _selectedStopId = Convert.ToString(e.DataRow["StopId"]);
            _selectedTripId = Convert.ToString(e.DataRow["TripId"]);
            _location = Convert.ToString(e.DataRow["Location"]);
            _routeId = Convert.ToString(e.DataRow["RouteId"]);
            _powerUnitId = Convert.ToString(e.DataRow["PowerUnitId"]);
            _loadCarrierId = Convert.ToString(e.DataRow["LoadCarrierId"]);
            ShowOperationDetail(e);
        }

        #endregion

        #region Events

        /// <summary>
        /// This event runs every time user selects any row in list of operations, and it processes the selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                switch (e.DataRow.CurrentTemplateIndex)
                {
                    case 1: // The master row has to be collapsed
                        _tabButtons.SetButtonEnabledState(ButtonScan, false);
                        break;
                    case 2: // The master row has to be expanded  
                        ProcessSelectedRow(e);
                        _tabButtons.SetButtonEnabledState(ButtonScan, true);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayStopList.ListOperationRowSelect");
            }
        }

        /// <summary>
        /// This method acts as event handler for back button and send message to flow that user clicked
        /// back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadingStopListEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayStopList.ButtonBackClick");
            }
        }

        /// <summary>
        /// This method acts as event handler for scan button and send message to flow that user clicked
        /// scan button. It also passses the required parameters with message that will be used by
        /// flow to handle this event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonScanClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadingStopListEvent.Scan, _selectedStopId, _selectedTripId, _location,_routeId,_powerUnitId,_loadCarrierId);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayStopList.ButtonScanClick");
            }
        }

        /// <summary>
        /// This method acts as event handler for click event for LoadWithOutStop button and send message to
        /// flow that user clicked on this LoadWithoutStop button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLoadWithOutStopClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadingStopListEvent.LoadWithoutStop);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayStopList.ButtonLoadWithoutStopClick");
            }
        }

        #endregion
    }

    /// <summary>
    /// This class is having list of constants, corresponding to each event that can be raised on this
    /// screen, these constants will be used by flow to check which type of event occured on screen.
    /// </summary>
    public class LoadingStopListEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Scan = 1;
        public const int LoadWithoutStop = 2;

        public static implicit operator LoadingStopListEvent(int viewEvent)
        {
            return new LoadingStopListEvent { Value = viewEvent };
        }
    }
}
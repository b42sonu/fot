﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList
{
    /// <summary>
    /// This class is having all view commands only.
    /// </summary>
    class ViewCommandsLoadingStops
    {
        /// <summary>
        /// This method shows FormDisplayStopList, having list of available planned stops.
        /// </summary>
        /// <param name="viewEventHandler">Specifies the handler for all events that will be raised, from screen.</param>
        /// <param name="operationStops">Specifies the list of stops, that need to show on screen.</param>
        /// <param name="operationType">Specifies the operation type (Loading or unloading) for which this screen is opening.</param>
        /// <returns></returns>
        internal static FormDisplayStopList ShowStopList(ViewEventDelegate viewEventHandler, List<Stop> operationStops, OperationType operationType)
        {
            var formData = new FormData
                {
                    OperationStops = operationStops,
                    OperationType = operationType
                };

            var view = ViewCommands.ShowView<FormDisplayStopList>(formData);
            view.SetEventHandler(viewEventHandler);
            return view;
        }
    }
}

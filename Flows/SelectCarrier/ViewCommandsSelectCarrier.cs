﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier
{
    class ViewCommandsSelectCarrier
    {

        internal static FormSelectCarrier SelectCarrier(ViewEventDelegate viewEventHandler)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsSelectCarrier.SelectCarrier()");
            var view = ViewCommands.ShowView<FormSelectCarrier>(null);
            view.SetEventHandler(viewEventHandler);
            var formData = new FormDataWorkList() { WorkListItems = ModelMain.WorkListItems };
            view.OnUpdateView(formData);
            return view;
        }

        internal static FormSelectCarrier SelectNewCarrier(ViewEventDelegate viewEventHandler, string loadCarrierId)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsSelectCarrier.SelectNewCarrier()");
            var view = ViewCommands.ShowView<FormSelectCarrier>(null);
            view.SetEventHandler(viewEventHandler);
            var formData = new FormDataWorkList() { IsToShowPopForNewCarrier = true, LoadCarrierId = loadCarrierId };
            view.OnUpdateView(formData);
            return view;
        }
    }
}

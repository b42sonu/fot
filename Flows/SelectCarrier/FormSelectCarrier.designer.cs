﻿using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier
{
    partial class FormSelectCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelectCarrier));

            #region Work list pop up related
            this.pnlConfirmMessage = new Panel();
            this.btnUnload = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.btnLoad = new Resco.Controls.OutlookControls.ImageButton();
            this.lblConfirmInfo = new System.Windows.Forms.Label();
            this.lblConfirmInfoHeading = new System.Windows.Forms.Label();
            this.picConfirmInfo = new System.Windows.Forms.PictureBox();
            this.lblNewCarrier = new System.Windows.Forms.Label();
            this.txtNewCarrier = new TextBox();
            #endregion

            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationListIcons = new System.Windows.Forms.ImageList() { ImageSize = new Size(40, 40) };//
            this.btnNewCarrier = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancelWorkList = new Resco.Controls.OutlookControls.ImageButton();
            this.btnContinue = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.listCarriers = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellLoadCarrierId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationTripId = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateLoadCarrierId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateOperationTripId = new Resco.Controls.AdvancedList.TextCell();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.btnNewCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnContinue)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            this.imageListOperationListIcons.Images.Clear();
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.Loading());
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.Unloading());
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.Delivery());
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.LoadingLineHaulTruck());
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.LoadDistributionTruck());


            // 
            // lblMessage
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Location = new System.Drawing.Point(10, 40);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(446, 80);
            this.labelModuleName.Font = new System.Drawing.Font("Arial",9F, System.Drawing.FontStyle.Regular);


            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(10,8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            
            // 
            // btnDeparture
            // 
            this.btnNewCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnNewCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnNewCarrier.ForeColor = System.Drawing.Color.White;
            this.btnNewCarrier.Location = new System.Drawing.Point(161, 501);
            this.btnNewCarrier.Name = "btnNewCarrier";
            this.btnNewCarrier.Size = new System.Drawing.Size(158, 50);
            this.btnNewCarrier.TabIndex = 8;
            
            this.btnNewCarrier.Click += new System.EventHandler(BtnNewCarrierClick);
            
            // 
            // btnBack
            // 
            this.btnCancelWorkList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancelWorkList.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancelWorkList.ForeColor = System.Drawing.Color.White;
            this.btnCancelWorkList.Location = new System.Drawing.Point(0,501);
            this.btnCancelWorkList.Name = "btnCancelWorkList";
            this.btnCancelWorkList.Size = new System.Drawing.Size(158, 50);
            this.btnCancelWorkList.TabIndex = 7;
            
            this.btnCancelWorkList.Click += new System.EventHandler(BtnCancelWorkListClick);
            
            // 
            // btnScan
            // 
            this.btnContinue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnContinue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnContinue.ForeColor = System.Drawing.Color.White;
            this.btnContinue.Location = new System.Drawing.Point(322, 501);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(158, 50);
            this.btnContinue.TabIndex = 9;
            this.btnContinue.Click += new System.EventHandler(BtnContinueClick);
            
           
           
            
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.btnContinue);
            this.touchPanel.Controls.Add(this.btnCancelWorkList);
            this.touchPanel.Controls.Add(this.btnNewCarrier);
            this.touchPanel.Controls.Add(this.listCarriers);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.pnlConfirmMessage);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            // 
            // advancedListOperationList
            // 
            this.listCarriers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listCarriers.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listCarriers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listCarriers.DataRows.Clear();
            //this.listOperations.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            //resources.GetString("advancedListOperationList.HeaderRow")});
            this.listCarriers.Location = new System.Drawing.Point(0, 120);
            this.listCarriers.MultiSelect = true;
            this.listCarriers.Name = "listCarriers";
            this.listCarriers.ScrollbarSmallChange = 32;
            this.listCarriers.ScrollbarWidth = 26;
            this.listCarriers.SelectedTemplateIndex = 2;
            this.listCarriers.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listCarriers.ShowHeader = true;
            this.listCarriers.Size = new System.Drawing.Size(480, 340);
            this.listCarriers.TabIndex = 2;
            this.listCarriers.TemplateIndex = 1;
            this.listCarriers.Templates.Add(this.rowTemplateHeader);
            this.listCarriers.Templates.Add(this.rowTemplatePlannedOp);
            this.listCarriers.Templates.Add(this.rowTemplatePlannedOpAlternateTemp);
            this.listCarriers.TouchScrolling = true;
            this.listCarriers.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListCarriersRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.rowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rowTemplateHeader.CellTemplates.Add(this.textCellHeaderTemplate);
            this.rowTemplateHeader.Height = 0;
            this.rowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.textCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.textCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.textCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.textCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.textCellHeaderTemplate.Visible = false;
            
            // 
            // RowTemplatePlannedOp
            // 
            this.rowTemplatePlannedOp.CellTemplates.Add(this.imageCellOperationType);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellLoadCarrierId);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationStopId);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationTripId);
            this.rowTemplatePlannedOp.Height = 64;
            this.rowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.imageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellOperationType.AutoResize = true;
            this.imageCellOperationType.CellSource.ColumnName = "StopType";
            this.imageCellOperationType.DesignName = "ImageCellOperationType";
            this.imageCellOperationType.ImageList = this.imageListOperationListIcons;
            this.imageCellOperationType.Location = new System.Drawing.Point(15, 10);
            this.imageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellLoadCarrierId.CellSource.ColumnName = "Detail";
            this.textCellLoadCarrierId.DesignName = "textCellLoadCarrierId";
            this.textCellLoadCarrierId.Location = new System.Drawing.Point(57, 0);
            this.textCellLoadCarrierId.Size = new System.Drawing.Size(370, 64);
            
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationStopId.Visible = false;
            // 
            // TextCellOperationOrderNumber
            // 
            this.textCellOperationTripId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationTripId.CellSource.ColumnName = "TripId";
            this.textCellOperationTripId.DesignName = "textCellOperationTripId";
            this.textCellOperationTripId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationTripId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationTripId.Visible = false;
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.rowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.imageCellAlternateOperationType);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateLoadCarrierId);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationStopId);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationTripId);
            this.rowTemplatePlannedOpAlternateTemp.Height = 64;
            this.rowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.imageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellAlternateOperationType.AutoResize = true;
            this.imageCellAlternateOperationType.CellSource.ColumnName = "StopType";
            this.imageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.imageCellAlternateOperationType.ImageList = this.imageListOperationListIcons;
            this.imageCellAlternateOperationType.Location = new System.Drawing.Point(15, 10);
            this.imageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateLoadCarrierId.CellSource.ColumnName = "Detail";
            this.textCellAlternateLoadCarrierId.DesignName = "textCellAlternateLoadCarrierId";
            this.textCellAlternateLoadCarrierId.Location = new System.Drawing.Point(57, 0);
            this.textCellAlternateLoadCarrierId.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateLoadCarrierId.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateLoadCarrierId.Size = new System.Drawing.Size(370, 64);
            
            
            // 
            // TextCellAlternateOperationStopId
            // 
            this.textCellAlternateOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellAlternateOperationStopId.DesignName = "TextCellAlternateOperationStopId";
            this.textCellAlternateOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationStopId.Visible = false;
            // 
            // TextCellAlternateOperationOrderNumber
            // 
            this.textCellAlternateOperationTripId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationTripId.CellSource.ColumnName = "TripId";
            this.textCellAlternateOperationTripId.DesignName = "textCellAlternateOperationTripId";
            this.textCellAlternateOperationTripId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationTripId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationTripId.Visible = false;

            #region Work list pop up

            // 
            // pnlConfirmMessage
            // 
            this.pnlConfirmMessage.BackColor = System.Drawing.Color.White;
            this.pnlConfirmMessage.Location = new System.Drawing.Point(20, 20);
            this.pnlConfirmMessage.Name = "pnlConfirmMessage";
            this.pnlConfirmMessage.Controls.Add(this.btnUnload);
            this.pnlConfirmMessage.Controls.Add(this.btnCancel);
            this.pnlConfirmMessage.Controls.Add(this.btnLoad);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfoHeading);
            this.pnlConfirmMessage.Controls.Add(this.picConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblNewCarrier);
            this.pnlConfirmMessage.Controls.Add(this.txtNewCarrier);
            this.pnlConfirmMessage.Size = new System.Drawing.Size(440, 380);
            this.pnlConfirmMessage.Visible = false;
            this.pnlConfirmMessage.Paint += new PaintEventHandler(PnlConfirmMessagePaint);


            //btnConfirm
            this.btnUnload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnUnload.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnUnload.ForeColor = System.Drawing.Color.White;
            this.btnUnload.Location = new System.Drawing.Point(310, 300);
            this.btnUnload.Name = "btnUnload";
            this.btnUnload.Size = new System.Drawing.Size(100, 50);
            this.btnUnload.TabIndex = 13;
            
            this.btnUnload.Click += new System.EventHandler(BtnUnloadClick);

            //btnCancel
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(20, 300);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 50);
            this.btnCancel.TabIndex = 11;
            
            this.btnCancel.Click += new System.EventHandler(BtnCancelClick);

            //btnAttempt
            this.btnLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnLoad.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnLoad.ForeColor = System.Drawing.Color.White;
            this.btnLoad.Location = new System.Drawing.Point(140, 300);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(150, 50);
            this.btnLoad.TabIndex = 12;
            
            this.btnLoad.Click += new System.EventHandler(BtnLoadClick);

            // 
            // lblConfirmInfo
            // 
            this.lblConfirmInfo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblConfirmInfo.Location = new System.Drawing.Point(25, 130);
            this.lblConfirmInfo.Name = "lblConfirmInfo";
            this.lblConfirmInfo.Size = new System.Drawing.Size(370, 60);
            
            this.lblConfirmInfo.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            // 
            // lblConfirmInfoHeading
            // 
            this.lblConfirmInfoHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblConfirmInfoHeading.Location = new System.Drawing.Point(25, 100);
            this.lblConfirmInfoHeading.Name = "lblConfirmInfoHeading";
            this.lblConfirmInfoHeading.Size = new System.Drawing.Size(236, 30);
            
            this.lblConfirmInfoHeading.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            //  
            // picConfirmInfo
            // 
            this.picConfirmInfo.Image = GuiCommon.Instance.Attention();
            this.picConfirmInfo.Location = new System.Drawing.Point(195, 20);
            this.picConfirmInfo.Name = "picConfirmInfo";
            this.picConfirmInfo.Size = new System.Drawing.Size(50, 50);
            this.picConfirmInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

            // 
            // lblNewCarrier
            // 
            this.lblNewCarrier.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.lblNewCarrier.Location = new System.Drawing.Point(25, 220);
            this.lblNewCarrier.Name = "lblNewCarrier";
            this.lblNewCarrier.Size = new System.Drawing.Size(150, 30);
            
            this.lblNewCarrier.TextAlign = System.Drawing.ContentAlignment.TopLeft;

            //
            // txtNewCarrier
            //
            this.txtNewCarrier.Location = new System.Drawing.Point(200, 210);
            this.txtNewCarrier.Name = "lblNewCarrier";
            this.txtNewCarrier.Size = new System.Drawing.Size(200, 30);
            this.txtNewCarrier.Text = string.Empty;
            this.txtNewCarrier.TabIndex = 10;
            

            #endregion


            
            // 
            // FormOperationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormSelectCarrier";
            this.Size = new System.Drawing.Size(480, 552);
            ((System.ComponentModel.ISupportInitialize)(this.btnNewCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnContinue)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }
        

        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList imageListOperationListIcons;
       
        private Resco.Controls.OutlookControls.ImageButton btnNewCarrier;
        private Resco.Controls.OutlookControls.ImageButton btnCancelWorkList;
        private Resco.Controls.OutlookControls.ImageButton btnContinue;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listCarriers;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell textCellHeaderTemplate;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell imageCellOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellLoadCarrierId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationTripId;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell imageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateLoadCarrierId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationTripId;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;


        private Panel pnlConfirmMessage;
        private PictureBox picConfirmInfo;
        private Label lblConfirmInfo;
        private Label lblConfirmInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnUnload;
        private Resco.Controls.OutlookControls.ImageButton btnCancel;
        private Resco.Controls.OutlookControls.ImageButton btnLoad;
        private Label lblNewCarrier;
        private TextBox txtNewCarrier;

    }
}
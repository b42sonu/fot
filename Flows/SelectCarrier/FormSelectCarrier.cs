﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormSelectCarrier.
    /// </summary>
    public class FormDataWorkList : BaseFormData
    {
        public List<WorkListItem> WorkListItems { get; set; }
        public bool IsToShowPopForNewCarrier { get; set; }
        public string LoadCarrierId { get; set; }
    }

    public partial class FormSelectCarrier : BaseForm
    {

        #region Private, Readonly and const variables
        private FormDataWorkList _formData;
        private List<WorkListItem> _workListItems;
        private bool _isToShowPopForNewCarrier;
        private string _loadCarrierId;
        private OperationType _stopType;
        private string _stopId = string.Empty;
        private string _tripId = string.Empty;
        #endregion

        /// <summary>
        /// Acts as default constructor for the form.
        /// </summary>
        public FormSelectCarrier()
        {
            InitializeComponent();
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToGui()
        {
            lblNewCarrier.Text = GlobalTexts.NewCarrier + ":";
            lblHeading.Text = GlobalTexts.SelectAddCarrier;
            labelModuleName.Text = GlobalTexts.WorkList;
            btnNewCarrier.Text = GlobalTexts.NewCarrier;
            btnCancelWorkList.Text = GlobalTexts.Cancel;
            btnContinue.Text = GlobalTexts.Continue;
            btnUnload.Text = GlobalTexts.Unload;
            btnCancel.Text = GlobalTexts.Cancel;
            btnLoad.Text = GlobalTexts.Load;
            lblConfirmInfo.Text = GlobalTexts.AddCarrierAndSelectOperation;
            lblConfirmInfoHeading.Text = GlobalTexts.NewCarrier;
        }

        #region Methods and Functions

        /// <summary>
        /// This method initializes the keyboard for text field named txtNewCarrier.
        /// </summary>
        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterLoadCarrier, KeyPadTypes.QwertyUpperCase, KeyPadTypes.Numeric);
            txtNewCarrier.Tag = keyBoardProperty;
        }

        /// <summary>
        /// This method enabled or disables the buttons on the basis of boolean value specified.
        /// </summary>
        /// <param name="status"></param>
        private void EnableDisableButtons(bool status)
        {
            btnCancelWorkList.Enabled = status;
            btnNewCarrier.Enabled = status;
            btnContinue.Enabled = status;

        }

        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnUpdateView(IFormData formData)
        {
            if (formData != null)
            {
                _formData = (FormDataWorkList)formData;
                _workListItems = _formData.WorkListItems;
                _isToShowPopForNewCarrier = _formData.IsToShowPopForNewCarrier;
            }

            if (_isToShowPopForNewCarrier)
                ShowHidePopUpForNewCarrier(true);
            BindWorkList();
        }

        /// <summary>
        /// This method takes load carrier as parameter and validate it against regular expression.
        /// </summary>
        /// <param name="loadCarrier">Specifies the load carrier that need to validate.</param>
        /// <returns>Returns bool result of validation, that it is validated or not.</returns>
        private bool IsValidLoadCarrier(string loadCarrier)
        {
            return Regex.IsMatch(loadCarrier, @"^[A-Z]{3}[0-9]{3}$");
        }

        /// <summary>
        /// This method, validate the load carrier specified by user and shows proper message to user if not
        /// valdated correctly.
        /// </summary>
        /// <returns>Returns bool status that, load carrier validated or not.</returns>
        private bool ValidateLoadCarrier()
        {
            var validationResult = IsValidLoadCarrier(txtNewCarrier.Text);
            lblConfirmInfo.Text = validationResult ? String.Empty : GlobalTexts.WrongCarrierFormat;
            return validationResult;
        }


        /// <summary>
        /// This method deselects all rows in listOperations, if any rows are there.
        /// </summary>
        private void UnSelectOtherRows()
        {
            if (listCarriers == null || listCarriers.DataRows.Count == 0) return;
            for (var i = 0; i <= listCarriers.DataRows.Count - 1; i++)
            {
                var row = listCarriers.DataRows[i];
                row.Selected = false;
            }

        }

        /// <summary>
        /// This method binds the list of carriers to listCarriers advance list.
        /// </summary>
        private void BindWorkList()
        {
            listCarriers.BeginUpdate();
            listCarriers.DataSource = _workListItems;
            listCarriers.EndUpdate();
        }


        /// <summary>
        /// This method shows or hides new carrier pop up on the basis of specified value of status.
        /// </summary>
        /// <param name="status">Specifies the bool status, which tels if wants to show/hide, new carrier popup.</param>
        private void ShowHidePopUpForNewCarrier(bool status)
        {
            pnlConfirmMessage.Visible = status;
            if (!status) return;
            lblConfirmInfo.Text = GlobalTexts.AddCarrierAndSelectOperation;
            txtNewCarrier.Text = string.Empty;
            pnlConfirmMessage.BringToFront();
        }


        /// <summary>
        /// This method executes every time, we select a row in listCarriers. Ans performs following tasks.
        /// 1) Deselect all rows
        /// 2) Selects the clicked row
        /// 3) Get values for loadCarrier, Operation Type (loading/unloading) and stopId/tripId from selected row
        /// so that it can be used later on.
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSelectedWorkListItem(RowEventArgs e)
        {
            UnSelectOtherRows();
            e.DataRow.Selected = true;
            _loadCarrierId = Convert.ToString(e.DataRow["LoadCarrierId"]);
            _stopType = (OperationType)e.DataRow["StopType"];
            _stopId = (string)e.DataRow["StopId"];
            _tripId = (string)e.DataRow["TripId"];
        }

        #endregion

        #region Events

        /// <summary>
        /// This method acts as handler for Paint event for pnlConfirmMessage and paints the black border for new carrier pop up.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void PnlConfirmMessagePaint(object sender, PaintEventArgs e)
        {
            try
            {
                using (var pen = new Pen(Color.FromArgb(113, 112, 116), 2))
                {
                    var graphics = e.Graphics;
                    graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlConfirmMessage.Width - 3, pnlConfirmMessage.Height - 3));
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.PnlConfirmMessagePaint");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of button btnNewCarrier. It shows the new carrier pop
        /// and disable other button on parent screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNewCarrierClick(object sender, EventArgs e)
        {
            try
            {
                ShowHidePopUpForNewCarrier(true);
                EnableDisableButtons(false);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnNewCarrierClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of button btnLoad. It does following:
        /// 1) Enables other butttons on screen having work list.
        /// 2) Validates the load carrier specified by user.
        /// 3) Hides the pop up for new carrier.
        /// 4) Send message to flow, that user clicked on load button. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLoadClick(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateLoadCarrier())
                    return;
                EnableDisableButtons(true);
                pnlConfirmMessage.Visible = false;
                ViewEvent.Invoke(SelectCarrierEvent.Load, txtNewCarrier.Text, _stopId, _tripId);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnLoadClick");                
            }
        }

        /// <summary>
        /// This method acts as handler for click event of button btnUnload. It does following:
        /// 1) Enables other butttons on screen having work list.
        /// 2) Validates the load carrier specified by user.
        /// 3) Hides the pop up for new carrier.
        /// 4) Send message to flow, that user clicked on Unload button. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnUnloadClick(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateLoadCarrier())
                    return;
                EnableDisableButtons(true);
                pnlConfirmMessage.Visible = false;
                ViewEvent.Invoke(SelectCarrierEvent.Unload, txtNewCarrier.Text, _stopId, _tripId);    
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnUnloadClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of button btnCancel. It does following:
        /// 1) Hides the new carrier popup
        /// 2) Enables the other buttons on parent screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            try
            {
                ShowHidePopUpForNewCarrier(false);
                EnableDisableButtons(true);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnCancelClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of continue button. It does the following:
        /// 1) Sends the message to flow that, user clicked continue button and passes the required data, so that flow
        /// can handle this event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnContinueClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(SelectCarrierEvent.Continue, _loadCarrierId, (int)_stopType, listCarriers.DataRows.Count, _stopId, _tripId);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnContinueClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of cancel button and send the message to flow that, user clicked
        /// on cancel button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancelWorkListClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(SelectCarrierEvent.CancelWorkList);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnCancelWorkListClick");                
            }
        }

        /// <summary>
        /// This method acts as handler for RowSelect event for advance list listCarriers. it process the selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListCarriersRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                switch (e.DataRow.CurrentTemplateIndex)
                {
                    case 1: // The master row has to be collapsed
                        btnContinue.Enabled = false;
                        break;
                    case 2: // The master row has to be expanded  
                        ProcessSelectedWorkListItem(e);
                        btnContinue.Enabled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.ListOperationRowSelect");
            }
        }

        #endregion

    }


    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectCarrier.
    /// </summary>
    public class SelectCarrierEvent : ViewEvent
    {
        public const int CancelWorkList = 0;
        public const int NewCarrier = 1;
        public const int Continue = 2;
        public const int CancelNewCarrier = 3;
        public const int Load = 4;
        public const int Unload = 5;

        public static implicit operator SelectCarrierEvent(int viewEvent)
        {
            return new SelectCarrierEvent { Value = viewEvent };
        }
    }
}
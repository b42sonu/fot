﻿using System.Linq;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier
{
    public class ActionCommandsSelectCarrier
    {
        /// <summary>
        /// This function returns the list of objects of type WorkListItem.
        /// </summary>
        /// <returns>Returns list of objects of type WorkListItem.</returns>
        internal List<WorkListItem> GetWorkListItems()
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsSelectCarrier.GetWorkListItems()");
            return ModelMain.WorkListItems;
        }

        /// <summary>
        /// This method stores, load carrier id, stop id and trip id in worklist.
        /// </summary>
        /// <param name="tripId"></param>
        /// <param name="loadCarrier">Specifies the load carrier that needs to store in work list.</param>
        /// <param name="process">Specifies the current process.</param>
        /// <param name="stopId"></param>
        internal void StoreLoadCarrierInWorkList(string stopId, string tripId, string loadCarrier, Process process)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsSelectCarrier.StoreLoadCarrierInWorkList()");
            if (string.IsNullOrEmpty(loadCarrier) || (process != Process.Loading && process != Process.Unloading))
                return;

            var workListItem = new WorkListItem
            {
                LoadCarrierId = loadCarrier,
                StopId = string.IsNullOrEmpty(stopId) ? string.Empty : stopId,
                TripId = string.IsNullOrEmpty(tripId) ? string.Empty : tripId,
                StopType = process == Process.Loading ? OperationType.Loading : OperationType.Unloading
            };

            if (ModelMain.WorkListItems == null)
                ModelMain.WorkListItems = new List<WorkListItem>();

            ModelMain.WorkListItems.Add(workListItem);
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// This method stores, load carrier id, stop id and trip id in worklist.
        /// </summary>
        /// <param name="workListItem">worklistitem that needs to add into list of worlist items.</param>
        public void StoreWorkListItem(WorkListItem workListItem)
        {
            //TODO:: Unit test for this method
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsSelectCarrier.StoreWorkListItem()");
            if (ModelMain.WorkListItems == null)
                ModelMain.WorkListItems = new List<WorkListItem>();
            ModelMain.WorkListItems.Add(workListItem.Clone());
        }
        //TODO :: Write Unit Test

        public void PreserveStateOfWorkListItem(EntityMap entityMap, WorkListItem currentWorkListItem)
        {
            var existingWorkListItem = GetExistingWorkListItem(currentWorkListItem);
            if (existingWorkListItem != null)
                existingWorkListItem.EntityMap = entityMap.DeepClone();
        }

        public void MarkCurrentWorkListItemAsCompleted(WorkListItem currentWorkListItem)
        {
            var existingWorkListItem = GetExistingWorkListItem(currentWorkListItem);
            existingWorkListItem.Status = WorkListItemStatus.Completed;
        }

        //TODO :: Write Unit Test
        public WorkListItem GetExistingWorkListItem(WorkListItem workListItem)
        {
            WorkListItem result = null;

            if (workListItem == null || ModelMain.WorkListItems == null)
                return null;
            //Get worklistitem for required type
            switch (workListItem.Type)
            {
                case WorkListItemType.LoadCarrier:
                    result = ModelMain.WorkListItems.SingleOrDefault(c => c.StopId.Equals(workListItem.StopId) && c.TripId.Equals(workListItem.TripId) && c.LoadCarrierId.Equals(workListItem.LoadCarrierId) &&
                             c.StopType == workListItem.StopType && c.Type == WorkListItemType.LoadCarrier);
                    break;

                case WorkListItemType.RouteCarrierTrip:
                    result = ModelMain.WorkListItems.SingleOrDefault(c => c.Rbt != null && c.Rbt.RouteId == workListItem.Rbt.RouteId 
                        && c.StopId == workListItem.StopId && c.TripId == workListItem.TripId && c.Rbt.LoadCarrierId == workListItem.Rbt.LoadCarrierId &&
                             c.StopType == workListItem.StopType && c.Type == WorkListItemType.RouteCarrierTrip);
                    break;

                case WorkListItemType.Route:
                    result = ModelMain.WorkListItems.SingleOrDefault(c => !string.IsNullOrEmpty(c.RouteId) && c.RouteId.Equals(workListItem.RouteId) && c.StopId.Equals(workListItem.StopId) && c.TripId.Equals(workListItem.TripId) && c.LoadCarrierId.Equals(workListItem.LoadCarrierId) &&
                                c.StopType == workListItem.StopType && c.Type == WorkListItemType.Route);
                    break;
            }
            return result;
        }

        //TODO :: Write Unit Test
        public bool IsWorkListItemAlreadyExists(WorkListItem workListItem)
        {
            //check for null
            if (ModelMain.WorkListItems == null || workListItem == null)
                return false;

            WorkListItem result = GetExistingWorkListItem(workListItem);

            return result != null;
        }

        public bool IsWorkListItemRelatedToCombination(WorkListItem workListItem)
        {
            //check for null
            if (ModelMain.WorkListItems == null || workListItem == null)
                return false;

            WorkListItem result = GetExistingWorkListItem(workListItem);

            return result.IsForCombinationTrip;
        }



        //TODO :: Write Unit Test
        /// <summary>
        /// This function checks if the specified combination of load carrier, stop id, trip and operation type
        /// exists in, work list or not.
        /// </summary>
        /// <param name="loadCarrier">Specifies the loadCarrier that needs to check for.</param>
        /// <param name="operationType">Specifies the operation type, that needs to check for.</param>
        /// <returns>Returns true, if combination exists, otherwise returns false.</returns>
        public WorkListItem GetExistingLoadCarrierWorkListItem(string loadCarrier, OperationType operationType)
        {
            //TODO:: Unit test cases
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsSelectCarrier.GetExistingLoadCarrierWorkListItem()");
            WorkListItem item = null;
            if (ModelMain.WorkListItems != null && string.IsNullOrEmpty(loadCarrier) == false)
            {
                item = (from c in ModelMain.WorkListItems
                        where string.IsNullOrEmpty(c.LoadCarrierId) == false && c.LoadCarrierId.Equals(loadCarrier) &&
                                           c.StopType == operationType && c.Type == WorkListItemType.LoadCarrier
                        select c).SingleOrDefault();
            }
            return item;
        }

        public bool IsWorkListItemAlreadyActive(WorkListItem currentWorkListItem, WorkListItem existingWorkListItem, MessageHolder messageHolder)
        {
            var isAlreadyExists = false;
            if (currentWorkListItem == null || currentWorkListItem.Type != existingWorkListItem.Type)
                return false;

            switch (existingWorkListItem.Type)
            {
                case WorkListItemType.LoadCarrier:
                    isAlreadyExists = existingWorkListItem.StopType == currentWorkListItem.StopType &&
                                     existingWorkListItem.LoadCarrierId == currentWorkListItem.LoadCarrierId &&
                                     existingWorkListItem.StopId == currentWorkListItem.StopId &&
                                     existingWorkListItem.TripId == currentWorkListItem.TripId;
                    break;

                case WorkListItemType.RouteCarrierTrip:
                    isAlreadyExists = existingWorkListItem.StopType == currentWorkListItem.StopType &&
                                     existingWorkListItem.LoadCarrierId == currentWorkListItem.LoadCarrierId &&
                                     existingWorkListItem.Rbt.RouteId == currentWorkListItem.Rbt.RouteId &&
                                     existingWorkListItem.StopId == currentWorkListItem.StopId &&
                                     existingWorkListItem.TripId == currentWorkListItem.TripId;
                    break;

                case WorkListItemType.Route:
                    isAlreadyExists = existingWorkListItem.StopType == currentWorkListItem.StopType &&
                                    existingWorkListItem.LoadCarrierId == currentWorkListItem.LoadCarrierId &&
                                    existingWorkListItem.RouteId == currentWorkListItem.RouteId &&
                                     existingWorkListItem.StopId == currentWorkListItem.StopId &&
                                     existingWorkListItem.TripId == currentWorkListItem.TripId;
                    break;

            }

            if (isAlreadyExists)
            {
                switch (existingWorkListItem.Type)
                {
                    case WorkListItemType.RouteCarrierTrip:
                    case WorkListItemType.LoadCarrier:
                        messageHolder.Update(GlobalTexts.LoadCarrierAlreadyActivate, MessageState.Warning);
                        break;
                    case WorkListItemType.Route:
                        messageHolder.Update(GlobalTexts.RouteAlreadyActive, MessageState.Warning);
                        break;
                }
            }

            return isAlreadyExists;
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// This function checks if the specified combination of load carrier, stop id, trip and operation type
        /// exists in, work list or not.
        /// </summary>
        /// <param name="rbt">Specifies the loadCarrier that needs to check for.</param>
        /// <param name="operationType">Specifies the operation type, that needs to check for.</param>
        /// <returns>Returns true, if combination exists, otherwise returns false.</returns>
        public WorkListItem GetExistingRbtWorkListItem(BarcodeRouteCarrierTrip rbt, OperationType operationType)
        {
            //TODO:: Unit Test cases
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsSelectCarrier.GetExistingRbtWorkListItem()");

            WorkListItem item = null;

            if (ModelMain.WorkListItems != null && rbt != null)
            {
                item = (from c in ModelMain.WorkListItems
                        where c.LoadCarrierId.Equals(rbt.LoadCarrierId) &&
                              c.RouteId.Equals(rbt.RouteId) &&
                              c.TripId.Equals(rbt.TripId) &&
                              c.StopType == operationType && c.Type == WorkListItemType.RouteCarrierTrip
                        select c).SingleOrDefault();
            }
            return item;
        }

        //TODO :: Write Unit Test
        public WorkListItem GetDefaultWorkListItem(OperationProcess selectedOperationProcess, out string routeId, Process currentProcess)
        {
            var stopType = CommandsOperations.GetStopTypeFromProcess(currentProcess);
            routeId = string.IsNullOrEmpty(selectedOperationProcess.RouteId) == false
                              ? selectedOperationProcess.RouteId
                              : ModelUser.UserProfile.RouteId;

            var tripId = string.IsNullOrEmpty(selectedOperationProcess.TripId)
                             ? string.Empty
                             : selectedOperationProcess.TripId;

            var rbt = new BarcodeRouteCarrierTrip(routeId, tripId, selectedOperationProcess.PowerUnitId, selectedOperationProcess.LogicalLoadCarrierId);
            var workListItem = new WorkListItem
            {
                Rbt = rbt,
                StopId = selectedOperationProcess.StopId,
                TripId = tripId,
                LoadCarrierId = selectedOperationProcess.LogicalLoadCarrierId,
                Type = WorkListItemType.RouteCarrierTrip,
                RouteId = routeId,
                StopType = stopType,
                IsDefaultWli = true,
                PhysicalLoadCarrierId = selectedOperationProcess.PhysicalLoadCarrierId,
                ExternalTripId = selectedOperationProcess.ExternalTripId,
                OperationId = selectedOperationProcess.OperationId
            };
            return workListItem;
        }

        //TODO :: Write Unit Test
        public bool HasActiveWorkListItems()
        {
            return ModelMain.WorkListItems != null && ModelMain.WorkListItems.Count > 0;
        }

    }
}

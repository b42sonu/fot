﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using Process = Com.Bring.PMP.PreComFW.Shared.Storage.Process;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStatesSelectCarrier
    {
        ActionValidateLoadCarrierId,
        ActionGetWorkListItems,
        ActionStoreLoadCarrierWhenUnloading,
        ActionStoreLoadCarrierWhenLoading,

        ViewCommands,
        ViewShowSelectCarrier,
       
        ThisFlowComplete
    }

    public class FlowDataSelectCarrier : BaseFlowData
    {
        public List<WorkListItem> WorkListItems { get; set; }
    }

    public class FlowResultSelectCarrier : BaseFlowResult
    {
        public string LoadCarrierId;
        public bool StartLoading { get; set; }
        public bool StartUnloading { get; set; }
        public bool ContinueWhenWorkListEmpty { get; set; }
    }


    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowSelectCarrier : BaseFlow
    {

        //private FlowDataSelectCarrier _flowData;
        private readonly FlowResultSelectCarrier _flowResult;
        //private readonly string _loadCarrierId = string.Empty;
        private readonly ActionCommandsSelectCarrier _actionCommands;
        private string _loadCarrierId;


        public FlowSelectCarrier()
        {
            _flowResult = new FlowResultSelectCarrier();
            _actionCommands = new ActionCommandsSelectCarrier();
        }



        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing FlowSelectCarrier");

            //_flowData = (FlowDataSelectCarrier)flowData;

            ExecuteViewState(FlowStatesSelectCarrier.ViewShowSelectCarrier);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesSelectCarrier)state);
        }

        public void ExecuteState(FlowStatesSelectCarrier state)
        {
            if (state > FlowStatesSelectCarrier.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesSelectCarrier state)
        {
            try
            {
                switch (state)
                {
                    case FlowStatesSelectCarrier.ViewShowSelectCarrier:
                        ViewCommandsSelectCarrier.SelectCarrier(ShowWorkListViewEventHandler);
                        break;
                    case FlowStatesSelectCarrier.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowSelectCarrier.ExecuteViewState");
            _flowResult.State=FlowResultState.Exception;
                ExecuteState(FlowStatesSelectCarrier.ThisFlowComplete);
            }
        }



        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStatesSelectCarrier state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "Executing state " + state + "in flow FlowSelectCarrier");
                    switch (state)
                    {
                        case FlowStatesSelectCarrier.ActionGetWorkListItems:
                            _actionCommands.GetWorkListItems();
                            state = FlowStatesSelectCarrier.ViewShowSelectCarrier;
                            break;

                        case FlowStatesSelectCarrier.ActionStoreLoadCarrierWhenUnloading:
                            _actionCommands.StoreLoadCarrierInWorkList(ModelMain.SelectedOperationProcess.StopId, ModelMain.SelectedOperationProcess.TripId,
                                                                       _loadCarrierId, Process.Unloading);

                            state = FlowStatesSelectCarrier.ThisFlowComplete;
                            break;

                        case FlowStatesSelectCarrier.ActionStoreLoadCarrierWhenLoading:
                            _actionCommands.StoreLoadCarrierInWorkList(ModelMain.SelectedOperationProcess.StopId, ModelMain.SelectedOperationProcess.TripId,
                                                                       _loadCarrierId, Process.Loading);

                            state = FlowStatesSelectCarrier.ThisFlowComplete;
                            break;
                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStatesSelectCarrier.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowSelectCarrier.ExecuteActionState");
                _flowResult.State=FlowResultState.Exception;
                state=FlowStatesSelectCarrier.ThisFlowComplete;
            }


            ExecuteViewState(state);
        }


        public void ShowWorkListViewEventHandler(int workListEvent, object[] data)
        {

            Logger.LogEvent(Severity.Debug, "Executing FlowSelectCarrier.ShowOperationsViewEventHandler");

            switch ((SelectCarrierEvent)workListEvent)
            {
                case SelectCarrierEvent.CancelWorkList:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStatesSelectCarrier.ThisFlowComplete);
                    break;

                case SelectCarrierEvent.Continue:

                    _flowResult.LoadCarrierId = (string)data[0];
                    //Check type of operation, for selected work list item and decide, which operation to start.

                    if ((int) data[2] == 0) // if Number of operations 0
                    {
                        _flowResult.ContinueWhenWorkListEmpty = true;
                        if (BaseModule.CurrentFlow.CurrentProcess == Process.Loading)
                            _flowResult.StartLoading = true;
                        if (BaseModule.CurrentFlow.CurrentProcess == Process.Unloading)
                            _flowResult.StartUnloading = true;
                    }
                    else //other wise check type of work list item selected.
                    {
                        if ((OperationType)data[1] == OperationType.Unloading)
                            _flowResult.StartUnloading = true;
                        if ((OperationType)data[1] == OperationType.Loading)
                            _flowResult.StartLoading = true;
                    }
                    ModelMain.SelectedOperationProcess.StopId =  (string)data[3];
                    ModelMain.SelectedOperationProcess.TripId =  (string)data[4];
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStatesSelectCarrier.ThisFlowComplete);
                    break;

                case SelectCarrierEvent.Load:
                    _flowResult.LoadCarrierId = _loadCarrierId = (string)data[0];
                    _flowResult.StartLoading = true;
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStatesSelectCarrier.ActionStoreLoadCarrierWhenLoading);
                    break;

                case SelectCarrierEvent.Unload:
                    _flowResult.State = FlowResultState.Ok;
                    _flowResult.LoadCarrierId = _loadCarrierId = (string)data[0];
                    _flowResult.StartUnloading = true;
                    ExecuteState(FlowStatesSelectCarrier.ActionStoreLoadCarrierWhenUnloading);
                    break;

            }
        }
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowShowOperations");
            BaseModule.EndSubFlow(_flowResult);
        }
    }

}

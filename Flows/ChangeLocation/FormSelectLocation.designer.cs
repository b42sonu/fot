﻿using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{
    partial class FormSelectLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelectLocation));

            #region Work list pop up related
            this.pnlConfirmMessage = new Panel();
            this.btnUnload = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.btnLoad = new Resco.Controls.OutlookControls.ImageButton();
            this.lblConfirmInfo = new System.Windows.Forms.Label();
            this.lblConfirmInfoHeading = new System.Windows.Forms.Label();
            this.picConfirmInfo = new System.Windows.Forms.PictureBox();
            this.lblNewCarrier = new System.Windows.Forms.Label();
            this.txtNewCarrier = new TextBox();
            #endregion

            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationListIcons = new System.Windows.Forms.ImageList() { ImageSize = new Size(40, 40) };//
            this.btnNewCarrier = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCancelWorkList = new Resco.Controls.OutlookControls.ImageButton();
            this.btnContinue = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.listLocations = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellLoadCarrierId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationTripId = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateLoadCarrierId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateOperationTripId = new Resco.Controls.AdvancedList.TextCell();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.btnNewCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnContinue)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();
            this.imageListOperationListIcons.Images.Clear();
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.Loading());
            this.imageListOperationListIcons.Images.Add(GuiCommon.Instance.Unloading());


            // 
            // lblMessage
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Location = new System.Drawing.Point(10, 40);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(446, 80);
            this.labelModuleName.Font = new System.Drawing.Font("Arial",9F, System.Drawing.FontStyle.Regular);


            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(10,8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            
            
            // 
            // btnBack
            // 
            this.btnCancelWorkList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCancelWorkList.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCancelWorkList.ForeColor = System.Drawing.Color.White;
            this.btnCancelWorkList.Location = new System.Drawing.Point(0,501);
            this.btnCancelWorkList.Name = "btnCancelWorkList";
            this.btnCancelWorkList.Size = new System.Drawing.Size(158, 50);
            this.btnCancelWorkList.TabIndex = 7;
            
            this.btnCancelWorkList.Click += new System.EventHandler(BtnCancelClick);
            
            // 
            // btnScan
            // 
            this.btnContinue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnContinue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnContinue.ForeColor = System.Drawing.Color.White;
            this.btnContinue.Location = new System.Drawing.Point(322,501);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(158, 50);
            this.btnContinue.TabIndex = 9;
            this.btnContinue.Click += new System.EventHandler(BtnContinueClick);
            
           
           
            
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.btnContinue);
            this.touchPanel.Controls.Add(this.btnCancelWorkList);
            this.touchPanel.Controls.Add(this.btnNewCarrier);
            this.touchPanel.Controls.Add(this.listLocations);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.pnlConfirmMessage);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480,552);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            // 
            // advancedListOperationList
            // 
            this.listLocations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listLocations.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listLocations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listLocations.DataRows.Clear();
            //this.listOperations.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            //resources.GetString("advancedListOperationList.HeaderRow")});
            this.listLocations.Location = new System.Drawing.Point(0, 120);
            this.listLocations.MultiSelect = true;
            this.listLocations.Name = "listLocations";
            this.listLocations.ScrollbarSmallChange = 32;
            this.listLocations.ScrollbarWidth = 26;
            this.listLocations.SelectedTemplateIndex = 2;
            this.listLocations.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.listLocations.ShowHeader = true;
            this.listLocations.Size = new System.Drawing.Size(480, 340);
            this.listLocations.TabIndex = 2;
            this.listLocations.TemplateIndex = 1;
            this.listLocations.Templates.Add(this.rowTemplateHeader);
            this.listLocations.Templates.Add(this.rowTemplatePlannedOp);
            this.listLocations.Templates.Add(this.rowTemplatePlannedOpAlternateTemp);
            this.listLocations.TouchScrolling = true;
            this.listLocations.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListLocationsRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.rowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rowTemplateHeader.CellTemplates.Add(this.textCellHeaderTemplate);
            this.rowTemplateHeader.Height = 0;
            this.rowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.textCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.textCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.textCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.textCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.textCellHeaderTemplate.Visible = false;
            
            // 
            // RowTemplatePlannedOp
            // 
            this.rowTemplatePlannedOp.CellTemplates.Add(this.imageCellOperationType);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellLoadCarrierId);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationStopId);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationTripId);
            this.rowTemplatePlannedOp.Height = 64;
            this.rowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.imageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellOperationType.AutoResize = true;
            this.imageCellOperationType.CellSource.ColumnName = "StopType";
            this.imageCellOperationType.DesignName = "ImageCellOperationType";
            this.imageCellOperationType.ImageList = this.imageListOperationListIcons;
            this.imageCellOperationType.Location = new System.Drawing.Point(15, 10);
            this.imageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellLoadCarrierId.CellSource.ColumnName = "Detail";
            this.textCellLoadCarrierId.DesignName = "textCellLoadCarrierId";
            this.textCellLoadCarrierId.Location = new System.Drawing.Point(57, 0);
            this.textCellLoadCarrierId.Size = new System.Drawing.Size(370, 64);
            
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationStopId.Visible = false;
            // 
            // TextCellOperationOrderNumber
            // 
            this.textCellOperationTripId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationTripId.CellSource.ColumnName = "TripId";
            this.textCellOperationTripId.DesignName = "textCellOperationTripId";
            this.textCellOperationTripId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationTripId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationTripId.Visible = false;
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.rowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.imageCellAlternateOperationType);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateLoadCarrierId);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationStopId);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationTripId);
            this.rowTemplatePlannedOpAlternateTemp.Height = 64;
            this.rowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.imageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellAlternateOperationType.AutoResize = true;
            this.imageCellAlternateOperationType.CellSource.ColumnName = "StopType";
            this.imageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.imageCellAlternateOperationType.ImageList = this.imageListOperationListIcons;
            this.imageCellAlternateOperationType.Location = new System.Drawing.Point(15, 10);
            this.imageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateLoadCarrierId.CellSource.ColumnName = "Detail";
            this.textCellAlternateLoadCarrierId.DesignName = "textCellAlternateLoadCarrierId";
            this.textCellAlternateLoadCarrierId.Location = new System.Drawing.Point(57, 0);
            this.textCellAlternateLoadCarrierId.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateLoadCarrierId.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateLoadCarrierId.Size = new System.Drawing.Size(370, 64);
            
            
            // 
            // TextCellAlternateOperationStopId
            // 
            this.textCellAlternateOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellAlternateOperationStopId.DesignName = "TextCellAlternateOperationStopId";
            this.textCellAlternateOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationStopId.Visible = false;
            // 
            // TextCellAlternateOperationOrderNumber
            // 
            this.textCellAlternateOperationTripId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationTripId.CellSource.ColumnName = "TripId";
            this.textCellAlternateOperationTripId.DesignName = "textCellAlternateOperationTripId";
            this.textCellAlternateOperationTripId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationTripId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationTripId.Visible = false;
            // 
            // FormSelectLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormSelectLocation";
            this.Size = new System.Drawing.Size(480, 552);
            ((System.ComponentModel.ISupportInitialize)(this.btnNewCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnContinue)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }
        

        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList imageListOperationListIcons;
       
        private Resco.Controls.OutlookControls.ImageButton btnNewCarrier;
        private Resco.Controls.OutlookControls.ImageButton btnCancelWorkList;
        private Resco.Controls.OutlookControls.ImageButton btnContinue;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listLocations;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell textCellHeaderTemplate;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell imageCellOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellLoadCarrierId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationTripId;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell imageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateLoadCarrierId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationTripId;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;


        private Panel pnlConfirmMessage;
        private PictureBox picConfirmInfo;
        private Label lblConfirmInfo;
        private Label lblConfirmInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton btnUnload;
        private Resco.Controls.OutlookControls.ImageButton btnCancel;
        private Resco.Controls.OutlookControls.ImageButton btnLoad;
        private Label lblNewCarrier;
        private TextBox txtNewCarrier;

    }
}
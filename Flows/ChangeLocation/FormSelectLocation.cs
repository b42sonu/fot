﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormSelectCarrier.
    /// </summary>
    public class FormDataSelectLocation : BaseFormData
    {
        public List<LocationElement> Locations { get; set; }
       
    }

    public partial class FormSelectLocation : BaseForm
    {

        #region Private, Readonly and const variables
        private FormDataSelectLocation _formData;
        private List<LocationElement> _locations;
        private string _selectedLocation;
        #endregion

        /// <summary>
        /// Acts as default constructor for the form.
        /// </summary>
        public FormSelectLocation()
        {
            InitializeComponent();
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToGui()
        {
            lblHeading.Text = GlobalTexts.SelectAddCarrier;
            labelModuleName.Text = GlobalTexts.WorkList;
            btnCancelWorkList.Text = GlobalTexts.Cancel;
            btnContinue.Text = GlobalTexts.Continue;
        }

        #region Methods and Functions

        /// <summary>
        /// This method initializes the keyboard for text field named txtNewCarrier.
        /// </summary>
        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterLoadCarrier, KeyPadTypes.QwertyUpperCase, KeyPadTypes.Numeric);
            txtNewCarrier.Tag = keyBoardProperty;
        }

        /// <summary>
        /// This method enabled or disables the buttons on the basis of boolean value specified.
        /// </summary>
        /// <param name="status"></param>
        private void EnableDisableButtons(bool status)
        {
            btnCancelWorkList.Enabled = status;
            btnNewCarrier.Enabled = status;
            btnContinue.Enabled = status;

        }

        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnUpdateView(IFormData formData)
        {
            if (formData != null)
            {
                _formData = (FormDataSelectLocation)formData;
                _locations = _formData.Locations;
            }

            BindLocations();
        }

        /// <summary>
        /// This method deselects all rows in listOperations, if any rows are there.
        /// </summary>
        private void UnSelectOtherRows()
        {
            if (listLocations == null || listLocations.DataRows.Count == 0) return;
            for (var i = 0; i <= listLocations.DataRows.Count - 1; i++)
            {
                var row = listLocations.DataRows[i];
                row.Selected = false;
            }

        }

        /// <summary>
        /// This method binds the list of carriers to listLocations advance list.
        /// </summary>
        private void BindLocations()
        {
            listLocations.BeginUpdate();
            listLocations.DataSource = _locations;
            listLocations.EndUpdate();
        }


        /// <summary>
        /// This method executes every time, we select a row in listLocations. Ans performs following tasks.
        /// 1) Deselect all rows
        /// 2) Selects the clicked row
        /// 3) Get values for loadCarrier, Operation Type (loading/unloading) and stopId/tripId from selected row
        /// so that it can be used later on.
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSelectedLocation(RowEventArgs e)
        {
            UnSelectOtherRows();
            e.DataRow.Selected = true;
            _selectedLocation = Convert.ToString(e.DataRow["LocationName"]);
        }

        #endregion

        #region Events

       
        /// <summary>
        /// This method acts as handler for click event of continue button. It does the following:
        /// 1) Sends the message to flow that, user clicked continue button and passes the required data, so that flow
        /// can handle this event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnContinueClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(SelectLocationEvent.Continue, _selectedLocation);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectLocation.BtnContinueClick");
            }
        }

        /// <summary>
        /// This method acts as handler for click event of cancel button and send the message to flow that, user clicked
        /// on cancel button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(SelectLocationEvent.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectLocation.BtnCancelClick");                
            }
        }

        /// <summary>
        /// This method acts as handler for RowSelect event for advance list listLocations. it process the selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListLocationsRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                switch (e.DataRow.CurrentTemplateIndex)
                {
                    case 1: // The master row has to be collapsed
                        btnContinue.Enabled = false;
                        break;
                    case 2: // The master row has to be expanded  
                        ProcessSelectedLocation(e);
                        btnContinue.Enabled = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectLocation.ListLocationsRowSelect");
            }
        }

        #endregion

    }


    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectCarrier.
    /// </summary>
    public class SelectLocationEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Continue = 2;

        public static implicit operator SelectLocationEvent(int viewEvent)
        {
            return new SelectLocationEvent { Value = viewEvent };
        }
    }
}
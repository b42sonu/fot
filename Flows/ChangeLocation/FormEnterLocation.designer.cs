﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{
    partial class FormEnterLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.listOrgUnits = new System.Windows.Forms.ListBox();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChooseOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtLocation = new PreCom.Controls.PreComInput2();

            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChooseOrgUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblOrgUnit);
            this.touchPanel.Controls.Add(this.lblChooseOrgUnit);
            this.touchPanel.Controls.Add(this.listOrgUnits);
            this.touchPanel.Controls.Add(this.txtLocation);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 2);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            // lblOrgUnit
            // 
            this.lblOrgUnit.AutoSize = false;
            this.lblOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgUnit.Location = new System.Drawing.Point(3, 42);
            this.lblOrgUnit.Name = "lblOrgUnit";
            this.lblOrgUnit.Size = new System.Drawing.Size(477, 33);
            this.lblOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            // lblChooseOrgUnit
            // 
            this.lblChooseOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblChooseOrgUnit.Location = new System.Drawing.Point(13, 105);
            this.lblChooseOrgUnit.Name = "lblChooseOrgUnit";
            this.lblChooseOrgUnit.Size = new System.Drawing.Size(203, 40);

            // 
            // listActions
            // 
            this.listOrgUnits.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.listOrgUnits.Location = new System.Drawing.Point(21, 150);
            this.listOrgUnits.Name = "listOrgUnits";
            this.listOrgUnits.Size = new System.Drawing.Size(438, 210);
            this.listOrgUnits.TabIndex = 4;
            
            
            // 
            // txtLocation
            // 
            this.txtLocation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtLocation.Location = new System.Drawing.Point(21, 105);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(438, 70);
            this.txtLocation.TabIndex = 39;
            this.txtLocation.TextTranslation = false;
            this.txtLocation.KeyUp += new KeyEventHandler(TxtLocationKeyUp);
            
            // 
            // messageControl
            // 
            this.messageControl.Location = new System.Drawing.Point(21, 227);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438, 110);
            this.messageControl.TabIndex = 47;
            // 
            // FormValidationError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormEnterLocation";
            this.Text = "FormEnterLocation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgUnit)).EndInit();
            this.ResumeLayout(false);

        }

        

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblChooseOrgUnit;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgUnit;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private PreCom.Controls.PreComInput2 txtLocation;
        private ListBox listOrgUnits;

    }
}
﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{

    /// <summary>
    /// This class acts as initial data, supplied from flows/commands to FormSelectCarrier.
    /// </summary>
    public class FormDataEnterLocation : BaseFormData
    {
        public List<LocationElement> Locations { get; set; }
        public MessageHolder StopMessage { get; set; }
        public string ScannedOrgUnit { get; set; }
    }

    public partial class FormEnterLocation : BaseBarcodeScanForm
    {

        #region Private, Readonly and const variables
        private FormDataEnterLocation _formData;
        private LocationElement _selectedLocation;
        private bool _bindingList;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        #endregion

        /// <summary>
        /// Acts as default constructor for the form.
        /// </summary>
        public FormEnterLocation()
        {
            InitializeComponent();
            SetStandardControlProperties(lblModuleName, lblOrgUnit, lblChooseOrgUnit, null, null, null, null);
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
            AddButtons();
            InitOnScreenKeyBoardProperties();
        }

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormEnterLocation.AddButtons()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEnterLocation.AddButtons");
            }
        }

        private void SetTextToGui()
        {
            lblChooseOrgUnit.Text = GlobalTexts.ChooseOrgUnit + ":";
        }

        #region Methods and Functions


        public override void BarcodeScanned(Com.Bring.PMP.PreComFW.Shared.Devices.FotScannerOutput scannerOutput)
        {
            txtLocation.Text = "";
            ViewEvent.Invoke(EnterLocationEvent.BarcodeScanned, scannerOutput);
        }


        /// <summary>
        /// This method refreshes the existing list of load carriers, by getting work list from
        /// form data.
        /// </summary>
        /// <param name="formData">Specifies the object of FormDataWorkList, as supplied from Action Command.</param>
        public override void OnUpdateView(IFormData formData)
        {
            base.OnUpdateView(formData);
            listOrgUnits.SelectedIndexChanged += ListOrgSelectedIndexChanged;
            if (formData != null)
            {
                BusyUtil.Activate();
                _formData = (FormDataEnterLocation)formData;
                lblModuleName.Text = _formData.HeaderText;
                lblOrgUnit.Text = _formData.OrgUnitName;

                if (_formData.StopMessage != null)
                    PopulateMessage(_formData.StopMessage);
                if (_formData.Locations.Count > 0)
                    BindLocations(_formData.Locations);

                if (string.IsNullOrEmpty(_formData.ScannedOrgUnit) == false)
                {
                    listOrgUnits.SelectedValue = _formData.ScannedOrgUnit;
                    _selectedLocation = (LocationElement)listOrgUnits.SelectedItem;
                    if (_selectedLocation != null)
                    {
                        txtLocation.Text = _selectedLocation.NameNumberCode;
                        txtLocation.SelectAll();
                        _multiButtonControl.SetButtonEnabledState(ButtonOk, true);
                    }
                }

                
                _multiButtonControl.SetButtonEnabledState(ButtonOk, _selectedLocation != null);
                BusyUtil.Reset();
            }
        }

        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
            {
                switch (messageHolder.State)
                {
                    case MessageState.Information:
                        SoundUtil.Instance.PlaySuccessSound();
                        break;

                    case MessageState.Warning:
                        SoundUtil.Instance.PlayWarningSound();
                        break;
                }
                messageControl.ShowMessage(messageHolder);
            }
            else
                messageControl.ClearMessage();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitOnScreenKeyBoardProperties()
        {
            var locationProperty = new OnScreenKeyboardProperties(GlobalTexts.Locations, KeyPadTypes.Qwerty,
                                                                  KeyPadTypes.Numeric) { TextChanged = TxtReasonTextChanged };
            txtLocation.Tag = locationProperty;
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindLocations(List<LocationElement> locations)
        {
            _bindingList = true;
            if (locations != null)
            {
                locations = locations.Distinct().ToList();
                listOrgUnits.DisplayMember = "NameNumberCode";
                listOrgUnits.ValueMember = "Number";
                listOrgUnits.DataSource = locations;
            }
            listOrgUnits.SelectedIndex = -1;
            txtLocation.Select(txtLocation.Text.Length, 0);
            _bindingList = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFinishedTyping"></param>
        /// <param name="textentered"></param>
        private void TxtReasonTextChanged(bool userFinishedTyping, string textentered)
        {
            if (!userFinishedTyping) return;
            try
            {
                if (_formData.Locations != null)
                {
                    var filteredActions =
                        (from c in _formData.Locations
                         where c.Name.ToLower().Contains(txtLocation.Text.ToLower()) || c.Number.ToLower().Contains(txtLocation.Text.ToLower())
                         select c).ToList();

                    BindLocations(filteredActions);
                }
                
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectAction.TxtActionKeyUp");
            }
        }

        private void TxtLocationKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (_formData.Locations == null) return;
                if (!string.IsNullOrEmpty(txtLocation.Text))
                {
                    var filteredActions =
                        (from c in _formData.Locations
                         where c.Name.ToLower().Contains(txtLocation.Text.ToLower()) || c.Number.ToLower().Contains(txtLocation.Text.ToLower())
                         select c).ToList();

                    BindLocations(filteredActions);
                }
                else
                    BindLocations(_formData.Locations);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectAction.TxtActionKeyUp");
            }
        }


        #endregion

        #region Events


        /// <summary>
        /// This method acts as handler for click event of button btnCancel. It does following:
        /// 1) Hides the new carrier popup
        /// 2) Enables the other buttons on parent screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                listOrgUnits.SelectedIndexChanged -= ListOrgSelectedIndexChanged;
                ClearDataSource();
                ViewEvent.Invoke(EnterLocationEvent.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnCancelClick");
            }
        }

        private void ClearDataSource()
        {
            listOrgUnits.BeginUpdate();
            listOrgUnits.DataSource = null;
            listOrgUnits.EndUpdate();
        }

        /// <summary>
        /// This method acts as handler for click event of continue button. It does the following:
        /// 1) Sends the message to flow that, user clicked continue button and passes the required data, so that flow
        /// can handle this event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {

                ViewEvent.Invoke(EnterLocationEvent.Continue, _selectedLocation);
                ClearDataSource();//hack clear data source so that it does not take much time when module ends

                listOrgUnits.SelectedIndexChanged -= ListOrgSelectedIndexChanged;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectCarrier.BtnContinueClick");
            }
        }

        private void ListOrgSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (_bindingList == false)
                {
                    _selectedLocation = (LocationElement)listOrgUnits.SelectedItem;
                    if (_selectedLocation != null)
                    {
                        txtLocation.Text = _selectedLocation.NameNumberCode;
                        txtLocation.SelectAll();
                        _multiButtonControl.SetButtonEnabledState(ButtonOk, true);
                    }

                }
                else
                {
                    _selectedLocation = null;
                    _multiButtonControl.SetButtonEnabledState(ButtonOk, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSelectAction.ListReasonsSelectedIndexChanged");
            }

        }

        #endregion
    }


    /// <summary>
    /// This class is having all constants for each event raised by events of FormSelectCarrier.
    /// </summary>
    public class EnterLocationEvent : ViewEvent
    {
        public const int Cancel = 0;
        public const int Continue = 1;
        public const int ScanRbt = 2;
        public const int BarcodeScanned = 3;
        public static implicit operator EnterLocationEvent(int viewEvent)
        {
            return new EnterLocationEvent { Value = viewEvent };
        }
    }
}
﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStatesChangeLocation
    {
        ActionIsCargoDriverOrTermWorker,
        ActionGetExistingLocations,
        ActionEmptyLocalCacheWhenScanningCancelled,
        ActionRequestStopList,
        ActionProcessStopList,
        ActionIsLocationsFromLmReturned,
        ActionBackFromRbtScan,
        ActionBackFromFlowDisplayStopList,
        ViewCommands,
        ViewEnterLocation,
        ViewShowMessage,
        ViewSelectLocation,
        FlowDisplayLocationList,
        ThisFlowComplete
    }

    public class FlowDataChangeLocation : BaseFlowData
    {
        public LocationElement CurrentLocation;
    }

    public class FlowResultChangeLocation : BaseFlowResult
    {
        public LocationElement Location { get; set; }
    }


    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowChangeLocation : BaseFlow
    {

        private FlowResultChangeLocation _flowResult;
        private readonly ActionCommandsChangeLocation _actionCommands;
        private List<LocationElement> _locations;
        private LocationElement _selectedLocation;
        private FlowDataChangeLocation _flowData;
        private readonly EntityMap _entityMap;
        private FlowScanBarcode _flowScanBarcodeInstance;
        private MessageHolder _messageHolder;
        private T20240_GetGoodsListFromFOTReply _goodsListReplyForStopList;
        private List<LocationElement> _locationfromLm;
        private BarcodeRouteCarrierTrip _scannedRbt;
        private string _scannedOrgUnit = string.Empty;
        public FlowChangeLocation()
        {
            _messageHolder = new MessageHolder();
            _entityMap = new EntityMap();
            _flowResult = new FlowResultChangeLocation();
            _actionCommands = new ActionCommandsChangeLocation();
        }



        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing FlowChangeLocation");
            if (flowData != null)
                _flowData = (FlowDataChangeLocation)flowData;

            ExecuteActionState(FlowStatesChangeLocation.ActionIsCargoDriverOrTermWorker);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesChangeLocation)state);
        }

        public void ExecuteState(FlowStatesChangeLocation state)
        {
            if (state > FlowStatesChangeLocation.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesChangeLocation state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowChangeLocation;" + state);
                switch (state)
                {

                    case FlowStatesChangeLocation.ViewEnterLocation:
                        ViewCommandsChangeLocation.EnterLocation(EnterLocationViewEventHandler, _locations, _flowData, _messageHolder);
                        break;
                    
                    case FlowStatesChangeLocation.FlowDisplayLocationList:
                        ViewCommandsChangeLocation.ShowLocationsLM(SelectLocationViewEventHandler, _locationfromLm, OperationType.Loading);
                        break;
                    case FlowStatesChangeLocation.ViewShowMessage:
                        var formData = new FormDataEnterLocation
                                       {
                                           HeaderText = _flowData.HeaderText,
                                           OrgUnitName = _flowData.CurrentLocation.Name,
                                           StopMessage = _messageHolder,
                                           Locations = _locations
                                       };
                        CurrentView.UpdateView(formData);
                        break;

                    case FlowStatesChangeLocation.ViewSelectLocation:
                        var formDataViewSelectLocation = new FormDataEnterLocation
                        {
                            HeaderText = _flowData.HeaderText,
                            OrgUnitName = _flowData.CurrentLocation.Name,
                            Locations = _locations,
                            ScannedOrgUnit = _scannedOrgUnit
                        };
                        CurrentView.UpdateView(formDataViewSelectLocation);
                        _scannedOrgUnit = string.Empty;
                        break;

                    case FlowStatesChangeLocation.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowChangeLocation.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesChangeLocation.ThisFlowComplete);
            }
        }



        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStatesChangeLocation state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowChangeLocation;" + state);
                    switch (state)
                    {
                        case FlowStatesChangeLocation.ActionIsCargoDriverOrTermWorker:
                            bool result = ModelUser.UserProfile.IsDriverOrTerminalWorker;
                            state = result
                                        ? FlowStatesChangeLocation.ActionGetExistingLocations
                                        : FlowStatesChangeLocation.ThisFlowComplete;
                            break;

                        case FlowStatesChangeLocation.ActionGetExistingLocations:
                            _locations = _actionCommands.GetExistingLocations();
                            state = FlowStatesChangeLocation.ViewEnterLocation;
                            break;
                        
                        case FlowStatesChangeLocation.ActionRequestStopList:
                            CommandsOperations.GetStopListForPowerUnit(_scannedRbt.PowerUnitId, GetGoodsListRequestRequestType.StopListAmphora, out _goodsListReplyForStopList, string.Empty);
                            state = FlowStatesChangeLocation.ActionProcessStopList;
                            break;
                        case FlowStatesChangeLocation.ActionProcessStopList:
                            _locationfromLm = CommandsOperations.ProcessGoodsListReplyToGetLocations(_goodsListReplyForStopList, OperationType.Loading);
                            state = FlowStatesChangeLocation.ActionIsLocationsFromLmReturned;
                            break;
                        case FlowStatesChangeLocation.ActionIsLocationsFromLmReturned:
                            if (_locationfromLm == null || _locationfromLm.Count == 0)
                            {
                                _messageHolder.Update(GlobalTexts.NoStopsFound, MessageState.Warning);
                                state = FlowStatesChangeLocation.ViewShowMessage;
                            }
                            else
                            {
                                state = FlowStatesChangeLocation.FlowDisplayLocationList;
                            }

                            break;

                        case FlowStatesChangeLocation.ActionBackFromRbtScan:
                            state = _actionCommands.ValidateFlowLoadingScan((FlowResultScanBarcode)SubflowResult, ref _scannedRbt, _messageHolder);
                            break;

                        case FlowStatesChangeLocation.ActionBackFromFlowDisplayStopList:
                            state = _actionCommands.ValidateFlowDisplayStopListResult((FlowResultDisplayStopList)SubflowResult, ref _messageHolder, ref _flowResult);
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStatesChangeLocation.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowChangeLocation.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStatesChangeLocation.ThisFlowComplete;
            }


            ExecuteViewState(state);
        }


        public void EnterLocationViewEventHandler(int enterLocationEvent, object[] data)
        {

            Logger.LogEvent(Severity.Debug, "Executing FlowChangeLocation.EnterLocationViewEventHandler");

            switch ((EnterLocationEvent)enterLocationEvent)
            {
                    
                case EnterLocationEvent.Cancel:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStatesChangeLocation.ThisFlowComplete);
                    break;

                case EnterLocationEvent.Continue:
                    _selectedLocation = (LocationElement)data[0];
                    _flowResult.Location = _selectedLocation;
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStatesChangeLocation.ThisFlowComplete);
                    break;

                case EnterLocationEvent.BarcodeScanned:
                    var scannerOutput = (FotScannerOutput)data[0];
                    var result = BaseActionCommands.ValidateBarcode(scannerOutput,
                                                                       BarcodeType.OrgUnitId, false);
                    if (result)
                    {
                        _scannedOrgUnit = scannerOutput.Code;
                        ExecuteState(FlowStatesChangeLocation.ViewSelectLocation);
                    }
                    else
                    {
                        GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.IllegalBarcode, Severity.Warning, GlobalTexts.Ok);
                        ExecuteState(FlowStatesChangeLocation.ViewEnterLocation);
                    }
                    break;
            }
        }

        public void SelectLocationViewEventHandler(int selectLocationEvent, object[] data)
        {

            Logger.LogEvent(Severity.Debug, "Executing FlowChangeLocation.EnterLocationViewEventHandler");

            switch ((EnterLocationEvent)selectLocationEvent)
            {

                case EnterLocationEvent.Cancel:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStatesChangeLocation.ViewEnterLocation);
                    break;

                case EnterLocationEvent.Continue:
                    _selectedLocation = _locationfromLm.Find(a => a.Name.Equals(data[0]));
                    _flowResult.Location = _selectedLocation;
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStatesChangeLocation.ThisFlowComplete);
                    break;
            }
        }
        private void GoToFlowLoadingScan()
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.LoadLineHaul, _messageHolder, BarcodeType.Rbt, _entityMap)
            {
                HaveCustomForm = true,
                NoValidationOfLoadCarrierFromServer = true
            };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)FlowStatesChangeLocation.ActionBackFromRbtScan, Process.Inherit);
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowChangeLocation");
            BaseModule.EndSubFlow(_flowResult);
        }
    }

}

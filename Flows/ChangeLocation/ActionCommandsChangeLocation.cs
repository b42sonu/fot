﻿using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{
    public class ActionCommandsChangeLocation
    {

        //TODO :: Write Unit Test
        internal List<LocationElement> GetExistingLocations()
        {
            return SettingDataProvider.Instance.GetLocations();
        }
        /// <summary>
        /// This method, validates the result from flow FlowShowOperations and decides which will be the next stateId.
        /// </summary>
        /// <returns>Returns next stateId to execute.</returns>
        public FlowStatesChangeLocation ValidateFlowLoadingScan(FlowResultScanBarcode subflowResult, ref BarcodeRouteCarrierTrip rbt, MessageHolder messageHolder)//, ref string tempLoadCarrierId, ref ConsignmentEntity consignmentEntity, MessageHolder messageHolder, WorkListItem currentWorkListItem, ref BarcodeRouteCarrierTrip rbt)
        {
            FlowStatesChangeLocation resultState;

            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    rbt = subflowResult.BarcodeRouteCarrierTrip;
                    resultState = FlowStatesChangeLocation.ActionRequestStopList;
                    break;

                case FlowResultState.Warning:
                case FlowResultState.Error:
                    resultState = FlowStatesChangeLocation.ViewEnterLocation;
                    break;

                case FlowResultState.Cancel:
                    resultState = FlowStatesChangeLocation.ActionEmptyLocalCacheWhenScanningCancelled;
                    break;
                default:
                    resultState = FlowStatesChangeLocation.ThisFlowComplete;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            return resultState;
        }


        internal FlowStatesChangeLocation ValidateFlowDisplayStopListResult(FlowResultDisplayStopList subflowResult, ref MessageHolder messageHolder, ref FlowResultChangeLocation flowResult)
        {
            var resultState = FlowStatesChangeLocation.ThisFlowComplete;

            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    if (flowResult.Location == null)
                        flowResult.Location = new LocationElement();
                    flowResult.Location.Name = ModelMain.SelectedOperationProcess.Location;
                    flowResult.State = FlowResultState.Ok;
                    resultState = FlowStatesChangeLocation.ThisFlowComplete;
                    break;
                case FlowResultState.Cancel:
                    messageHolder.Update(GlobalTexts.OrganizationUnitAborted, MessageState.Information);
                    resultState = FlowStatesChangeLocation.ViewShowMessage;
                    break;
            }
            return resultState;
        }
    }
}

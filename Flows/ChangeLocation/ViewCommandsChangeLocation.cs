﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Flows.DisplayStopList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeLocation
{
    public class ViewCommandsChangeLocation
    {

        internal static FormEnterLocation EnterLocation(ViewEventDelegate viewEventHandler, List<LocationElement> locations, FlowDataChangeLocation flowData, MessageHolder messageHolder)
        {
             var view = ViewCommands.ShowView<FormEnterLocation>(null);
            view.SetEventHandler(viewEventHandler);
            var formData = new FormDataEnterLocation { Locations = locations, HeaderText = flowData.HeaderText, OrgUnitName = flowData.CurrentLocation.Name, StopMessage = messageHolder };
            view.OnUpdateView(formData);
            return view;
        }

        internal static FormSelectLocation SelectLocation(ViewEventDelegate viewEventHandler, List<LocationElement> locations)
        {
             var view = ViewCommands.ShowView<FormSelectLocation>(null);
            view.SetEventHandler(viewEventHandler);
            var formData = new FormDataSelectLocation(){Locations = locations};
            view.OnUpdateView(formData);
            return view;
        }

        internal static FormDisplayLocations ShowLocationsLM(ViewEventDelegate viewEventHandler, List<LocationElement> operationLocations, OperationType operationType)
        {
            var formData = new FormLocationData
            {
                OperationLocations = operationLocations,
                OperationType = operationType
            };

            var view = ViewCommands.ShowView<FormDisplayLocations>(formData);
            view.SetEventHandler(viewEventHandler);
            return view;
        }
    }
}

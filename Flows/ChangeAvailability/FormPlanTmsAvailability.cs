﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    public partial class FormPlanTmsAvailability : BaseForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        const string ButtonBackName = "ButtonBack";
        const string ButtonOkName = "ButtonOk";
        public FormPlanTmsAvailability()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            SetTextToGui();
        }

        private void SetTextToGui()
        {
            labelHeader.Text = GlobalTexts.PlanTmsAvailability;
            labelDate.Text = GlobalTexts.Date;
            labelDateValue.Text = DateTime.Now.ToShortDateString();
            labelPowerUnit.Text = GlobalTexts.PowerUnit;
            labelPowerUnitValue.Text = ModelUser.UserProfile.PowerUnit;
            labelShortCuts.Text = GlobalTexts.DurationShortCuts;
            labelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            labelTms.Text = GlobalTexts.TmsAffiliation;
            labelTmsValue.Text = ModelUser.UserProfile.TmsAffiliation;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonCancelClick, TabButtonType.Cancel) { Name = ButtonBackName });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOkName });
            _multiButtonControl.GenerateButtons();
            button30Min.Text = GlobalTexts.Minutes30;
            button1Hour.Text = GlobalTexts.Hour1;
            button2Hour.Text = GlobalTexts.Hour2;
            button4Hour.Text = GlobalTexts.Hour4;
            button8Hour.Text = GlobalTexts.Hour8;

            labelStart.Text = GlobalTexts.Activate;
            labelStop.Text = GlobalTexts.Stop;
        }

        public override void OnShow(IFormData formData)
        {
            labelDate.Text = GlobalTexts.Date;
            labelDateValue.Text = DateTime.Now.ToShortDateString();
            timePickerStart.Value = DateTime.Now; //.AddMinutes(1);
            timePickerStop.Value = DateTime.Now;//.AddMinutes(2);
        }

        private void Button30MinClick(object sender, EventArgs e)
        {
            try
            {
                SetDuration(30);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.Button30MinClick");
            }
        }

        private void Button1HourClick(object sender, EventArgs e)
        {
            try
            {
                SetDuration(60);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.Button1HourClick");
            }
        }

        private void Button2HourClick(object sender, EventArgs e)
        {
            try
            {
                SetDuration(120);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.Button2HourClick");
            }
        }

        private void Button4HourClick(object sender, EventArgs e)
        {
            try
            {
                SetDuration(240);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.Button4HourClick");
            }
        }

        private void Button8HourClick(object sender, EventArgs e)
        {
            try
            {
                SetDuration(480);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.Button8HourClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormChangeTmsAvailability.ButtonOkClick");

                if (timePickerStart.Value >= timePickerStop.Value)
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.ScheduledPauseIllegalStart, Severity.Error, GlobalTexts.Ok);
                }
                else if (timePickerStop.Value < DateTime.Now)
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.ScheduledPauseIllegalEnd, Severity.Error, GlobalTexts.Ok);
                }
                else
                {
                    ViewEvent.Invoke(PlanTmsAvailabilityViewEvent.Ok, timePickerStart.Value, timePickerStop.Value);
                    Visible = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.ButtonOkClick");
            }
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormChangeTmsAvailability.ButtonCancelClick");
                ViewEvent.Invoke(PlanTmsAvailabilityViewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPlanTmsAvailability.ButtonCancelClick");
            }
        }

        private void SetDuration(int minutes)
        {
            DateTime startTime = timePickerStart.Value;

            DateTime endTime = startTime.AddMinutes(minutes);

            timePickerStop.Value = endTime;
        }
    }

    public class PlanTmsAvailabilityViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;
    }
}
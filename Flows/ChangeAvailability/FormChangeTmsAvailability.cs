﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    public partial class FormChangeTmsAvailability : BaseForm
    {
        private FormDataTmsAvailability _formDataTmsAvailability;
        private readonly MultiButtonControl _multiButtonControl;
        private Action _actionChangeLoggedInStatus;
        private Action _actionChangePauseStatus;
        const string NameButtonBack = "ButtonBack";
        const string NameButtonPlanning = "ButtonPlanning";
        const string NameButtonStartStopPause = "ButtonStartStopBreak";
        const string NameButtonLoginOut = "ButtonLoginOut";


        private void InitGui()
        {
            labelHeader.Text = GlobalTexts.ChangeTmsAvailability;
            labelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            labelHeader.Text = GlobalTexts.PlanTmsAvailability;
            labelDate.Text = GlobalTexts.Date;
            labelDateValue.Text = DateTime.Now.ToShortDateString();
            labelPowerUnit.Text = GlobalTexts.PowerUnit;
            labelPowerUnitValue.Text = ModelUser.UserProfile.PowerUnit;
            labelOrgUnit.Text = ModelUser.UserProfile.OrgUnitName;
            labelTms.Text = GlobalTexts.TmsAffiliation;
            labelTmsValue.Text = ModelUser.UserProfile.TmsAffiliation;

            _actionChangeLoggedInStatus = ButtonLoginClick;
            _actionChangePauseStatus = ButtonStartPauseClick;

            AddTabButtons();
        }

        private void AddTabButtons()
        {
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonCancelClick, TabButtonType.Cancel) { Name = NameButtonBack });
            if (CommunicationClient.Instance.IsConnected() && ModelUser.IsLoggedInToTms)
            {
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Planning, ButtonPlanningClick, NameButtonPlanning));
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.StartPause, ButtonPauseClick, NameButtonStartStopPause));
            }

            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Login, ButtonLogInOutClick, NameButtonLoginOut));
            _multiButtonControl.GenerateButtons();
        }

        public FormChangeTmsAvailability()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);

            InitGui();

        }

        internal void OnUpdateGui()
        {
            try
            {
                if (InvokeRequired)
                {
                    BeginInvoke(new Action(OnUpdateGui));
                    return;
                }

                _multiButtonControl.Clear();
                AddTabButtons();
                if (ModelUser.UserProfile.TmsAffiliation != null)
                {
                    labelPowerUnitValue.Visible = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora;
                    labelPowerUnit.Visible = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora;
                }
                labelDate.Text = GlobalTexts.Date;
                labelDateValue.Text = DateTime.Now.ToShortDateString();
                labelStatus.Text = GlobalTexts.Status;
                labelStatusValue.Text = ActionCommandsChangeTmsAvailability.Instance.GetStatusText();

                var loginOutButton = _multiButtonControl.GetButton(NameButtonLoginOut);
                var pauseButton = _multiButtonControl.GetButton(NameButtonStartStopPause);

                if (ModelUser.IsLoggedInToTms)
                {
                    _multiButtonControl.SetButtonEnabledState(NameButtonStartStopPause, true);
#if TestNewFeature
                    _multiButtonControl.SetButtonEnabledState(NameButtonLoginOut, true);
                    loginOutButton.Text = GlobalTexts.Logout;
                    _actionChangeLoggedInStatus = ButtonLogoutClick;
#else
                    _multiButtonControl.SetButtonVisibleState(NameButtonLoginOut, false);
#endif
                }
                else
                {
                    if (CommunicationClient.Instance.IsConnected())
                    {
                        _multiButtonControl.SetButtonEnabledState(NameButtonLoginOut, true);
                        loginOutButton.Text = GlobalTexts.Login;
                        _actionChangeLoggedInStatus = ButtonLoginClick;
                    }
                    else
                        _multiButtonControl.SetButtonEnabledState(NameButtonLoginOut, false);
                }

                _multiButtonControl.SetButtonEnabledState(NameButtonPlanning, ActionCommandsChangeTmsAvailability.Instance.IsStatusPaused == false && ModelUser.IsLoggedInToTms);

                if (CommunicationClient.Instance.IsConnected() && ModelUser.IsLoggedInToTms)
                {
                    if (ActionCommandsChangeTmsAvailability.Instance.IsStatusPaused)
                    {
                        pauseButton.Text = GlobalTexts.StopPause;
                        _actionChangePauseStatus = ButtonStopPauseClick;
                    }
                    else
                    {
                        pauseButton.Text = GlobalTexts.StartPause;
                        _actionChangePauseStatus = ButtonStartPauseClick;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailability.UpdateGui");
            }
            BusyUtil.Reset();
        }

        internal void OnShowGuiMessage(MessageState messageState, string message)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing function FormChangeTmsAvailability.OnShowGuiMessage");
                messageControl.ShowMessage(message, messageState);
                OnUpdateGui();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailability.OnShowGuiMessage");
            }
        }

        public override void OnShow(IFormData formData)
        {
            _formDataTmsAvailability = (FormDataTmsAvailability)formData;
            ActionCommandsChangeTmsAvailability.Instance.StartPlan(_formDataTmsAvailability.StartBreak, _formDataTmsAvailability.EndBreak);

            messageControl.ClearMessage();
            OnUpdateGui();
        }

        private void ButtonLogInOutClick(object param, EventArgs ea)
        {
            _actionChangeLoggedInStatus();
        }


        private void ButtonLoginClick()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing event FormChangeTmsAvailability.ButtonLoginClick");
                BusyUtil.Activate();
                _multiButtonControl.SetButtonEnabledState(NameButtonLoginOut, false);
                messageControl.ClearMessage();
                ViewEvent.Invoke(ChangeTmsAvailabilityViewEvent.Login);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailability.ButtonLoginClick");
            }

            finally
            {
                BusyUtil.Reset();
            }
        }



#if TestNewFeature
        private void ButtonLogoutClick()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing event FormChangeTmsAvailability.ButtonLoginClick");

                _multiButtonControl.SetButtonEnabledState(NameButtonLoginOut, false);
                messageControl.ClearMessage();
                ViewEvent.Invoke(ChangeTmsAvailabilityViewEvent.Logout);
            }


            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailability.ButtonLogoutClick");
            }

            finally
            {
                BusyUtil.Reset();
            }
        }
#endif

        private void ButtonPauseClick(object param, EventArgs ea)
        {
            messageControl.ClearMessage();
            _actionChangePauseStatus();
        }

        private void ButtonStartPauseClick()
        {
            ViewEvent.Invoke(ChangeTmsAvailabilityViewEvent.StartPause);
            OnUpdateGui();
        }

        private void ButtonStopPauseClick()
        {
            try
            {
                ViewEvent.Invoke(ChangeTmsAvailabilityViewEvent.StopPause);
                OnUpdateGui();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailavility.ButtonStopBreak");
            }
        }


        private void ButtonPlanningClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormTmsAvailability.ButtonPlanningClick");
                messageControl.ClearMessage();
                ViewEvent.Invoke(ChangeTmsAvailabilityViewEvent.Planning);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailability.ButtonPlanningClick");
            }
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing event FormTmsAvailability.BtnBackClick");
                ViewEvent.Invoke(ChangeTmsAvailabilityViewEvent.Back);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormTMSAvailability.BtnBackClick");
            }
        }


    }

    public class ChangeTmsAvailabilityViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Planning = 1;
        public const int Login = 2;
        public const int Logout = 3;
        public const int StartPause = 4;
        public const int StopPause = 5;
    }
    public class FormDataTmsAvailability : BaseFormData
    {
        public DateTime? StartBreak { get; set; }
        public DateTime? EndBreak { get; set; }
    }

}
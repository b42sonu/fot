﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Model;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    class ViewCommandsChangeAvailability
    {
        internal FormChangeTmsAvailability ShowChangeTmsAvailabilityView(ViewEventDelegate viewEventHandler, FormDataTmsAvailability formDataTmsAvailability,
            ref DriverAvailability driverAvailability)
        {
            var view = ViewCommands.ShowView<FormChangeTmsAvailability>(formDataTmsAvailability);
            view.SetEventHandler(viewEventHandler);

            if (driverAvailability == null)
                driverAvailability = new DriverAvailability(ModelUser.UserProfile, view.OnShowGuiMessage);
            else
                DriverAvailability.ShowGuiMessageHandler = view.OnShowGuiMessage;

            return view;
        }

        internal FormPlanTmsAvailability ShowPlanTmsAvailabilityView(ViewEventDelegate viewEventHandler)
        {
            var view = ViewCommands.ShowView<FormPlanTmsAvailability>();
            view.SetEventHandler(viewEventHandler);
            return view;
        }
    }
}

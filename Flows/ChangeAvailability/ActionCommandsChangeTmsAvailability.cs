﻿using System;
using System.Threading;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    public class ActionCommandsChangeTmsAvailability
    {

        private static ActionCommandsChangeTmsAvailability _instance;
        public static ActionCommandsChangeTmsAvailability Instance
        {
            get { return _instance ?? (_instance = new ActionCommandsChangeTmsAvailability()); }
        }

        private Timer _pauseTimer;
        private Timer PauseTimer
        {
            get { return _pauseTimer ?? (_pauseTimer = new Timer(OnBreakTimer, null, Timeout.Infinite, Timeout.Infinite)); }
        }

        private DateTime? _startPause;
        private DateTime? _stopPause;

        private TimerTask _timerTask;

        private enum TimerTask
        {
            StartPause,
            EndPause
        }

        private enum PauseIndicator
        {
            Start,
            Stop
        }

        private bool IsStatusPlannedPause
        {
            get
            {
                if (_startPause != null && _stopPause != null)
                {
                    if (_startPause != DateTime.MinValue && _stopPause != DateTime.MaxValue)
                    {
                        if (_startPause < DateTime.Now && _stopPause > DateTime.Now)
                            return true;
                    }
                }

                return false;
            }
        }

        public bool IsStatusPaused
        {
            get
            {
                if (_startPause != null && _stopPause != null)
                {
                    if (_startPause < DateTime.Now && _stopPause > DateTime.Now)
                        return true;
                }

                return false;
            }
        }

        internal string GetStatusText()
        {
            if (CommunicationClient.Instance.IsConnected() == false)
                return GlobalTexts.Offline;

            if (ModelUser.IsLoggedInToTms == false)
                return GlobalTexts.Unavailable;

            if (IsStatusPlannedPause)
                return GlobalTexts.ScheduledPause;

            if (IsStatusPaused)
                return GlobalTexts.Paused;

            return GlobalTexts.Available;
        }

        private void OnBreakTimer(object argument)
        {
            // We are now starting a break
            if (_timerTask == TimerTask.StartPause)
            {
                _timerTask = TimerTask.EndPause;
                if (_stopPause != null && _stopPause > DateTime.Now)
                {
                    var diff = (DateTime)_stopPause - DateTime.Now;
                    StartPause(Convert.ToInt32(diff.TotalSeconds));
                    ActivateEndPauseTimer();
                }
            }
            else
            {
                // We are now ending a break
                StopPause(false);
            }
            UpdateGui();
        }

        private static void UpdateGui()
        {
            var form = ViewCommands.GetView<FormChangeTmsAvailability>();
            if (form != null)
            {
                form.OnUpdateGui();
            }
        }

        internal void StartPause()
        {
            StartPause(-1);
        }

        private void StartPause(int duration)
        {
            try
            {
                var workStatus = CreateWorkStatus(PauseIndicator.Start, duration);
                CommunicationClient.Instance.SendWorkStatusRequest(workStatus);
                // Turn off timer
                PauseTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormChangeTmsAvailability.ButtonStartPauseClick");
            }
        }


        private void ActivateEndPauseTimer()
        {
            if (_stopPause != null && _stopPause >= DateTime.Now)
            {
                _timerTask = TimerTask.EndPause;
                // PauseIndicator is started. Calculate when it is ending
                TimeSpan period = (DateTime)_stopPause - DateTime.Now;
                PauseTimer.Change(period, TimeSpan.FromMilliseconds(Timeout.Infinite));
            }
        }

        internal void StopPause(bool isManualStop)
        {
            PauseTimer.Change(Timeout.Infinite, Timeout.Infinite);
            _startPause = _stopPause = null;

            if (isManualStop || ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra)
            {
                var workStatus = CreateWorkStatus(PauseIndicator.Stop);
                CommunicationClient.Instance.SendWorkStatusRequest(workStatus); // Async
            }
        }


        internal void StartManualPause()
        {
            _startPause = DateTime.MinValue;
            _stopPause = DateTime.MaxValue;
            StartPause();
        }



        internal void StartPlan(DateTime? startPause, DateTime? stopPause)
        {
            if (startPause != null && stopPause != null)
            {
                _startPause = startPause;
                _stopPause = stopPause;
                _timerTask = TimerTask.StartPause;

                // User scheduled a break that has already started
                if (_startPause <= DateTime.Now)
                {
                    var diff = (DateTime)_stopPause - DateTime.Now;
                    StartPause((Convert.ToInt32(diff.TotalSeconds)));
                    ActivateEndPauseTimer();
                }
                // Start a pause in the future
                else if (_startPause > DateTime.Now)
                {
                    // Calculate when PauseIndicator is starting
                    var period = (DateTime)_startPause - DateTime.Now;
                    PauseTimer.Change(period, TimeSpan.FromMilliseconds(Timeout.Infinite));
                }
            }
        }

        private IndicateWorkstatus CreateWorkStatus(PauseIndicator pauseIndicator)
        {
            return CreateWorkStatus(pauseIndicator, -1);
        }

        private IndicateWorkstatus CreateWorkStatus(PauseIndicator pauseIndicator, int duration)
        {
            if (duration == -1)
                duration = 24 * 60 * 60; // 24 hours

            var indicateWorkstatus = new IndicateWorkstatus
            {
                UserLogonId = ModelUser.UserProfile.UserId,
                CaptureEquipmentId = ModelModules.Platform.Hardware.Tag,
                SessionId = ModelUser.SessionId,
                LocationId = ModelUser.UserProfile.OrgUnitId,
                MessageTime = DateTime.Now,
            };

            if (ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra)
            {
                indicateWorkstatus.TMS_Affiliation = TMS_Affiliation.ALYST_BEX;
                indicateWorkstatus.UnitId = ModelUser.UserProfile.UserId;
                indicateWorkstatus.PowerUnitId = string.Empty;
                indicateWorkstatus.LoadCarrierId = string.Empty;
                indicateWorkstatus.Status = pauseIndicator == PauseIndicator.Start ? Status.PAUSED : Status.ACTIVE;
            }
            else
            {
                indicateWorkstatus.TMS_Affiliation = TMS_Affiliation.AMPHORA;
                indicateWorkstatus.UnitId = string.Empty;
                indicateWorkstatus.PowerUnitId = ModelUser.UserProfile.PowerUnit;
                indicateWorkstatus.LoadCarrierId = ModelUser.UserProfile.LoadCarrier;
                if (pauseIndicator == PauseIndicator.Start)
                {
                    indicateWorkstatus.Status = Status.Start;
                    indicateWorkstatus.Duration = duration;
                    indicateWorkstatus.DurationSpecified = true;
                }
                else
                {
                    indicateWorkstatus.Status = Status.Stopp;
                    indicateWorkstatus.Duration = duration;
                    indicateWorkstatus.DurationSpecified = true;
                }
            }

            return indicateWorkstatus;
        }

        internal void Logout()
        {
            if (IsStatusPaused)
                StopPause(true);

            _startPause = _stopPause = null;
        }
    }
}

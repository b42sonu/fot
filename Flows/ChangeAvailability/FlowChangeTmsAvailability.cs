﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateChangeAvailability
    {
        ViewCommands,

        ViewShowChangeTmsAvailability,
        ViewShowPlanTmsAvailability,
        ThisFlowComplete
    }

    public class FlowResultChangeAvailability : BaseFlowResult{}

    // US 48: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554935
    public class FlowChangeTmsAvailability : BaseFlow
    {
        private DateTime? _startBreak;
        private DateTime? _endBreak;
        private readonly ViewCommandsChangeAvailability _viewCommands;
        private readonly FlowResultChangeAvailability _flowResult;
        private DriverAvailability _driverAvailability;
        public FlowChangeTmsAvailability()
        {
            _viewCommands = new ViewCommandsChangeAvailability();
            _flowResult = new FlowResultChangeAvailability();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowChangeTmsAvailability.BeginFlow(IFlowData flowData)");
            ExecuteState(FlowStateChangeAvailability.ViewShowChangeTmsAvailability);
        }

        public void ExecuteState(FlowStateChangeAvailability state)
        {
            if (state > FlowStateChangeAvailability.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateChangeAvailability)state);
        }

        private void ExecuteViewState(FlowStateChangeAvailability stateChangeAvailability)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowChangeTmsAvailability;" + stateChangeAvailability);
                switch (stateChangeAvailability)
                {
                    case FlowStateChangeAvailability.ViewShowChangeTmsAvailability:
                        var formDataTmsAvailability = new FormDataTmsAvailability { StartBreak = _startBreak, EndBreak = _endBreak };
                        _viewCommands.ShowChangeTmsAvailabilityView(ChangeTmsAvailabilityViewEventHandler, formDataTmsAvailability, ref _driverAvailability);
                        break;

                    case FlowStateChangeAvailability.ViewShowPlanTmsAvailability:
                        _viewCommands.ShowPlanTmsAvailabilityView(PlanTmsAvailabilityViewEventHandler);
                        break;

                    case FlowStateChangeAvailability.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowChangeTmsAvailability.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateChangeAvailability.ThisFlowComplete);
            }
        }
        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateChangeAvailability state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowChangeTmsAvailability;" + state);
                    switch (state)
                    {
                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateChangeAvailability.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowChangeTmsAvailability.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateChangeAvailability.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }


        /// <summary>
        /// Handler which will handle all type of events from Change Tms Availability view..
        /// </summary>
        private void ChangeTmsAvailabilityViewEventHandler(int changeTmsAvailabilityViewEvents, params object[] data)
        {
            switch (changeTmsAvailabilityViewEvents)
            {
                case ChangeTmsAvailabilityViewEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteViewState(FlowStateChangeAvailability.ThisFlowComplete);
                    break;

                case ChangeTmsAvailabilityViewEvent.Login:
                    _driverAvailability.Login();
                    break;

                case ChangeTmsAvailabilityViewEvent.Logout:
                    ActionCommandsChangeTmsAvailability.Instance.Logout();
                    _driverAvailability.Logout(true);
                    break;

                case ChangeTmsAvailabilityViewEvent.Planning:
                    ExecuteViewState(FlowStateChangeAvailability.ViewShowPlanTmsAvailability);
                    break;

                case ChangeTmsAvailabilityViewEvent.StartPause:
                    ActionCommandsChangeTmsAvailability.Instance.StartManualPause();
                    break;

                case ChangeTmsAvailabilityViewEvent.StopPause:
                    ActionCommandsChangeTmsAvailability.Instance.StopPause(true);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ChangeTmsAvailabilityViewEventHandler");
                    break;
            }
        }

        private void PlanTmsAvailabilityViewEventHandler(int planTmsAvailabilityViewEvents, params object[] data)
        {
            switch (planTmsAvailabilityViewEvents)
            {
                case PlanTmsAvailabilityViewEvent.Back:
                    _startBreak = null;
                    _endBreak = null;
                    ExecuteViewState(FlowStateChangeAvailability.ViewShowChangeTmsAvailability);
                    break;

                case PlanTmsAvailabilityViewEvent.Ok:
                    _startBreak = Convert.ToDateTime(data[0]);
                    _endBreak = Convert.ToDateTime(data[1]);
                    ExecuteViewState(FlowStateChangeAvailability.ViewShowChangeTmsAvailability);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in PlanTmsAvailabilityViewEventHandler");
                    break;
            }
        }

        /// <summary>
        /// This methos ends the Change Availability flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowChangeTmsAvailability");
            if (_driverAvailability != null)
            {
                _driverAvailability.Dispose();
                _driverAvailability = null;
            }
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

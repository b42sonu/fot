﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    partial class FormChangeTmsAvailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelDateValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelStatusValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelPowerUnitValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelTmsValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelTms = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelPowerUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelStatus = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelDate = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelHeader = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelDateValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStatusValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnitValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTmsValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeader)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.labelDateValue);
            this.touchPanel.Controls.Add(this.labelStatusValue);
            this.touchPanel.Controls.Add(this.labelPowerUnitValue);
            this.touchPanel.Controls.Add(this.labelTmsValue);
            this.touchPanel.Controls.Add(this.labelTms);
            this.touchPanel.Controls.Add(this.labelPowerUnit);
            this.touchPanel.Controls.Add(this.labelStatus);
            this.touchPanel.Controls.Add(this.labelDate);
            this.touchPanel.Controls.Add(this.labelOrgUnit);
            this.touchPanel.Controls.Add(this.labelHeader);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // labelDateValue
            // 
            this.labelDateValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelDateValue.Location = new System.Drawing.Point(225, 116);
            this.labelDateValue.Name = "labelDateValue";
            this.labelDateValue.Size = new System.Drawing.Size(119, 27);
            this.labelDateValue.Text = "TMS Value";
            // 
            // labelStatusValue
            // 
            this.labelStatusValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelStatusValue.Location = new System.Drawing.Point(225, 157);
            this.labelStatusValue.Name = "labelStatusValue";
            this.labelStatusValue.Size = new System.Drawing.Size(119, 27);
            this.labelStatusValue.Text = "TMS Value";
            // 
            // labelPowerUnitValue
            // 
            this.labelPowerUnitValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelPowerUnitValue.Location = new System.Drawing.Point(225, 239);
            this.labelPowerUnitValue.Name = "labelPowerUnitValue";
            this.labelPowerUnitValue.Size = new System.Drawing.Size(119, 27);
            this.labelPowerUnitValue.Text = "TMS Value";
            // 
            // labelTmsValue
            // 
            this.labelTmsValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelTmsValue.Location = new System.Drawing.Point(225, 198);
            this.labelTmsValue.Name = "labelTmsValue";
            this.labelTmsValue.Size = new System.Drawing.Size(119, 27);
            this.labelTmsValue.Text = "TMS Value";
            // 
            // labelTms
            // 
            this.labelTms.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelTms.Location = new System.Drawing.Point(11,198);
            this.labelTms.Name = "labelTms";
            this.labelTms.Size = new System.Drawing.Size(168, 27);
            this.labelTms.Text = "TMS Affiliation";
            // 
            // labelPowerUnit
            // 
            this.labelPowerUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelPowerUnit.Location = new System.Drawing.Point(11, 239);
            this.labelPowerUnit.Name = "labelPowerUnit";
            this.labelPowerUnit.Size = new System.Drawing.Size(126, 27);
            this.labelPowerUnit.Text = "Power Unit";
            // 
            // labelStatus
            // 
            this.labelStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelStatus.Location = new System.Drawing.Point(11, 157);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(74, 27);
            this.labelStatus.Text = "Status";
            // 
            // labelDate
            // 
            this.labelDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDate.Location = new System.Drawing.Point(11, 116);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(53, 27);
            this.labelDate.Text = "Date";
            // 
            // labelOrgUnit
            // 
            this.labelOrgUnit.AutoSize = false;
            this.labelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelOrgUnit.Location = new System.Drawing.Point(6, 52);
            this.labelOrgUnit.Name = "labelOrgUnit";
            this.labelOrgUnit.Size = new System.Drawing.Size(459, 34);
            this.labelOrgUnit.Text = "Org Unit";
            this.labelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // labelHeader
            // 
            this.labelHeader.AutoSize = false;
            this.labelHeader.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeader.Location = new System.Drawing.Point(11, 12);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(459, 34);
            this.labelHeader.Text = "Change TMS Availability";
            this.labelHeader.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(21, 337);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438, 110);
            this.messageControl.TabIndex = 57;
            // 
            // FormChangeTmsAvailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormChangeTmsAvailability";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelDateValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStatusValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnitValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTmsValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeader)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelHeader;
        private Resco.Controls.CommonControls.TransparentLabel labelDate;
        private Resco.Controls.CommonControls.TransparentLabel labelPowerUnit;
        private Resco.Controls.CommonControls.TransparentLabel labelStatus;
        private Resco.Controls.CommonControls.TransparentLabel labelTmsValue;
        private Resco.Controls.CommonControls.TransparentLabel labelDateValue;
        private Resco.Controls.CommonControls.TransparentLabel labelStatusValue;
        private Resco.Controls.CommonControls.TransparentLabel labelPowerUnitValue;
        private Resco.Controls.CommonControls.TransparentLabel labelTms;
        private Resco.Controls.CommonControls.TransparentLabel labelOrgUnit;
        private MessageControl messageControl;
    }
}

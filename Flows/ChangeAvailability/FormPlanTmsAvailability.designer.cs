﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.ChangeAvailability
{
    partial class FormPlanTmsAvailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelTms = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelOrgUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelPowerUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelDate = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelDateValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelTmsValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelPowerUnitValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.button8Hour = new Resco.Controls.OutlookControls.ImageButton();
            this.button4Hour = new Resco.Controls.OutlookControls.ImageButton();
            this.button2Hour = new Resco.Controls.OutlookControls.ImageButton();
            this.button1Hour = new Resco.Controls.OutlookControls.ImageButton();
            this.timePickerStop = new System.Windows.Forms.DateTimePicker();
            this.timePickerStart = new System.Windows.Forms.DateTimePicker();
            this.button30Min = new Resco.Controls.OutlookControls.ImageButton();
            this.labelShortCuts = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelHeader = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelStop = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelStart = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelTms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDateValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTmsValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnitValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button8Hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button4Hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button2Hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button1Hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.button30Min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelShortCuts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStart)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.Controls.Add(this.labelTms);
            this.touchPanel.Controls.Add(this.labelOrgUnit);
            this.touchPanel.Controls.Add(this.labelPowerUnit);
            this.touchPanel.Controls.Add(this.labelDate);
            this.touchPanel.Controls.Add(this.labelDateValue);
            this.touchPanel.Controls.Add(this.labelTmsValue);
            this.touchPanel.Controls.Add(this.labelPowerUnitValue);
            this.touchPanel.Controls.Add(this.button8Hour);
            this.touchPanel.Controls.Add(this.button4Hour);
            this.touchPanel.Controls.Add(this.button2Hour);
            this.touchPanel.Controls.Add(this.button1Hour);
            this.touchPanel.Controls.Add(this.timePickerStop);
            this.touchPanel.Controls.Add(this.timePickerStart);
            this.touchPanel.Controls.Add(this.button30Min);
            this.touchPanel.Controls.Add(this.labelShortCuts);
            this.touchPanel.Controls.Add(this.labelHeader);
            this.touchPanel.Controls.Add(this.labelStop);
            this.touchPanel.Controls.Add(this.labelStart);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // labelTms
            // 
            this.labelTms.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelTms.Location = new System.Drawing.Point(11, 198);
            this.labelTms.Name = "labelTms";
            this.labelTms.Size = new System.Drawing.Size(168, 27);
            this.labelTms.Text = "TMS Affiliation";
            // 
            // labelOrgUnit
            // 
            this.labelOrgUnit.AutoSize = false;
            this.labelOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelOrgUnit.Location = new System.Drawing.Point(6, 52);
            this.labelOrgUnit.Name = "labelOrgUnit";
            this.labelOrgUnit.Size = new System.Drawing.Size(459, 34);
            this.labelOrgUnit.Text = "Org Unit";
            this.labelOrgUnit.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // labelPowerUnit
            // 
            this.labelPowerUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelPowerUnit.Location = new System.Drawing.Point(11, 157);
            this.labelPowerUnit.Name = "labelPowerUnit";
            this.labelPowerUnit.Size = new System.Drawing.Size(126, 27);
            this.labelPowerUnit.Text = "Power Unit";
            // 
            // labelDate
            // 
            this.labelDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelDate.Location = new System.Drawing.Point(11, 116);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(53, 27);
            this.labelDate.Text = "Date";
            // 
            // labelDateValue
            // 
            this.labelDateValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelDateValue.Location = new System.Drawing.Point(200, 116);
            this.labelDateValue.Name = "labelDateValue";
            this.labelDateValue.Size = new System.Drawing.Size(119, 27);
            this.labelDateValue.Text = "TMS Value";
            // 
            // labelTmsValue
            // 
            this.labelTmsValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelTmsValue.Location = new System.Drawing.Point(200, 198);
            this.labelTmsValue.Name = "labelTmsValue";
            this.labelTmsValue.Size = new System.Drawing.Size(119, 27);
            this.labelTmsValue.Text = "TMS Value";
            // 
            // labelPowerUnitValue
            // 
            this.labelPowerUnitValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelPowerUnitValue.Location = new System.Drawing.Point(200, 157);
            this.labelPowerUnitValue.Name = "labelPowerUnitValue";
            this.labelPowerUnitValue.Size = new System.Drawing.Size(119, 27);
            this.labelPowerUnitValue.Text = "TMS Value";
            // 
            // button8Hour
            // 
            this.button8Hour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.button8Hour.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.button8Hour.ForeColor = System.Drawing.Color.White;
            this.button8Hour.Location = new System.Drawing.Point(383, 304);
            this.button8Hour.Name = "button8Hour";
            this.button8Hour.Size = new System.Drawing.Size(85, 34);
            this.button8Hour.TabIndex = 67;
            this.button8Hour.Text = "8 Hour";
            this.button8Hour.Click += new System.EventHandler(this.Button8HourClick);
            // 
            // button4Hour
            // 
            this.button4Hour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.button4Hour.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.button4Hour.ForeColor = System.Drawing.Color.White;
            this.button4Hour.Location = new System.Drawing.Point(290, 304);
            this.button4Hour.Name = "button4Hour";
            this.button4Hour.Size = new System.Drawing.Size(85, 34);
            this.button4Hour.TabIndex = 66;
            this.button4Hour.Text = "4 Hour";
            this.button4Hour.Click += new System.EventHandler(this.Button4HourClick);
            // 
            // button2Hour
            // 
            this.button2Hour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.button2Hour.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.button2Hour.ForeColor = System.Drawing.Color.White;
            this.button2Hour.Location = new System.Drawing.Point(197, 304);
            this.button2Hour.Name = "button2Hour";
            this.button2Hour.Size = new System.Drawing.Size(85, 34);
            this.button2Hour.TabIndex = 65;
            this.button2Hour.Text = "2 Hour";
            this.button2Hour.Click += new System.EventHandler(this.Button2HourClick);
            // 
            // button1Hour
            // 
            this.button1Hour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.button1Hour.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.button1Hour.ForeColor = System.Drawing.Color.White;
            this.button1Hour.Location = new System.Drawing.Point(104, 304);
            this.button1Hour.Name = "button1Hour";
            this.button1Hour.Size = new System.Drawing.Size(85, 34);
            this.button1Hour.TabIndex = 64;
            this.button1Hour.Text = "1Hour";
            this.button1Hour.Click += new System.EventHandler(this.Button1HourClick);
            // 
            // timePickerStop
            // 
            this.timePickerStop.CustomFormat = "HH:mm";
            this.timePickerStop.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.timePickerStop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerStop.Location = new System.Drawing.Point(342, 389);
            this.timePickerStop.Name = "timePickerStop";
            this.timePickerStop.ShowUpDown = true;
            this.timePickerStop.Size = new System.Drawing.Size(126, 35);
            this.timePickerStop.TabIndex = 62;
            // 
            // timePickerStart
            // 
            this.timePickerStart.CustomFormat = "HH:mm";
            this.timePickerStart.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.timePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerStart.Location = new System.Drawing.Point(11, 389);
            this.timePickerStart.Name = "timePickerStart";
            this.timePickerStart.ShowUpDown = true;
            this.timePickerStart.Size = new System.Drawing.Size(126, 35);
            this.timePickerStart.TabIndex = 59;
            // 
            // button30Min
            // 
            this.button30Min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.button30Min.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.button30Min.ForeColor = System.Drawing.Color.White;
            this.button30Min.Location = new System.Drawing.Point(11, 304);
            this.button30Min.Name = "button30Min";
            this.button30Min.Size = new System.Drawing.Size(85, 34);
            this.button30Min.TabIndex = 54;
            this.button30Min.Text = "30 min";
            this.button30Min.Click += new System.EventHandler(this.Button30MinClick);
            // 
            // labelShortCuts
            // 
            this.labelShortCuts.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelShortCuts.Location = new System.Drawing.Point(11, 274);
            this.labelShortCuts.Name = "labelShortCuts";
            this.labelShortCuts.Size = new System.Drawing.Size(180, 24);
            this.labelShortCuts.Text = "Duration short cuts";
            // 
            // labelHeader
            // 
            this.labelHeader.AutoSize = false;
            this.labelHeader.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelHeader.Location = new System.Drawing.Point(11, 12);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(459, 34);
            this.labelHeader.Text = "Change TMS Availability";
            this.labelHeader.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // labelStop
            // 
            this.labelStop.Location = new System.Drawing.Point(342, 354);
            this.labelStop.Name = "labelStop";
            this.labelStop.Size = new System.Drawing.Size(48, 29);
            this.labelStop.Text = "Stop";
            // 
            // labelStart
            // 
            this.labelStart.Location = new System.Drawing.Point(11, 354);
            this.labelStart.Name = "labelStart";
            this.labelStart.Size = new System.Drawing.Size(51, 29);
            this.labelStart.Text = "Activate";
            // 
            // FormPlanTmsAvailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 0);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Name = "FormPlanTmsAvailability";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelTms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelOrgUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelDateValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTmsValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPowerUnitValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button8Hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button4Hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button2Hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button1Hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.button30Min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelShortCuts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelHeader;
        private Resco.Controls.OutlookControls.ImageButton button30Min;
        private System.Windows.Forms.DateTimePicker timePickerStart;
        private System.Windows.Forms.DateTimePicker timePickerStop;
        private Resco.Controls.CommonControls.TransparentLabel labelStop;
        private Resco.Controls.CommonControls.TransparentLabel labelStart;
        private Resco.Controls.OutlookControls.ImageButton button8Hour;
        private Resco.Controls.OutlookControls.ImageButton button4Hour;
        private Resco.Controls.OutlookControls.ImageButton button2Hour;
        private Resco.Controls.OutlookControls.ImageButton button1Hour;
        private Resco.Controls.CommonControls.TransparentLabel labelShortCuts;
        private Resco.Controls.CommonControls.TransparentLabel labelDateValue;
        private Resco.Controls.CommonControls.TransparentLabel labelTmsValue;
        private Resco.Controls.CommonControls.TransparentLabel labelPowerUnitValue;
        private Resco.Controls.CommonControls.TransparentLabel labelDate;
        private Resco.Controls.CommonControls.TransparentLabel labelPowerUnit;
        private Resco.Controls.CommonControls.TransparentLabel labelOrgUnit;
        private Resco.Controls.CommonControls.TransparentLabel labelTms;
    }
}

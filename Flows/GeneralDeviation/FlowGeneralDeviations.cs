﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

// US 36 :http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554927
namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    public enum FlowStatesGeneralDeviations
    {
        ActionBackFromFlowCauseAndMeasure,
        ActionBackFromFlowExecuteVasMatrixOnLoadCarrierScan,
        ActionBackFromFlowVasMatrix,
        ActionIsProcessStartFromDoubleScan,
        ActionBackFromFlowMainScan,
        ActionCreateLoadCarrierEntity,
        ActionIsLoadCarrierAlreadyExists,
        ActionStoreLoadCarrier,
        ActionSendLoadCarrierEvent,
        ActionGetItemCount,
        ActionBackFromFlowScanCorrectionMenu,
        ActionRegisterGeneralDeviation,
        ActionStoreConsignmentItem,
        ActionBackFromFlowScanItemOrCarrierId,
        ActionDisplayErrorMessage,
        ActionValidateScanIsConsignmentItem,
        ActionValidateScanIsLoadCarrier,
        ActionCreateConsignmentEntity,
        ActionCretaeEntityForLoadCarrier,
        ActionValidateLoadCarrier,
        ActionValidateConsignmentItem,
        ActionIsItemScannedBefore,
        ActionStoreItemInCache,
        ViewCommands,

        ViewDeviationDetail,
        FlowScanCorrectionMenu,
        FlowExecuteVasMatrix,
        FlowExecuteVasMatrixOnLoadCarrierScan,
        FlowCauseAndMeasure,
        FlowScanItemOrCarrierId,
        ThisFlowCanceled,
        ThisFlowComplete
    }

    public class FlowResultGeneralDeviations : BaseFlowResult
    {
    }

    public class FlowDataGeneralDeviations : BaseFlowData
    {
        #region IForm Members

        public bool IsStartedFromDoubleScan { get; set; }
        public ConsignmentEntity CurrentConsignmentEntity { get; set; }
        #endregion
    }

    public class FlowGeneralDeviations : BaseFlow
    {
        private CauseMeasure _selectedReasonAndActionCombination;
        private string _freeText;
        private int _numberOfItems;
        private int _numberOfCarriers;
        private readonly FlowResultGeneralDeviations _flowResult;
        private FlowDataGeneralDeviations _flowData;
        private readonly ActionCommandsGeneralDeviation _actionCommandsGeneralDeviation;
        private MessageHolder _messageHolder;
        private FotScannerOutput _scannerOutput;
        private ConsignmentEntity _currentConsignmentEntity;
        private readonly string _operationId = string.Empty;
        private readonly string _stopId = string.Empty;
        private readonly string _tripId = string.Empty;
        private string _loadcarrier;
        private readonly EntityMap _entityMap;
        public readonly BarcodeType BarcodeType;
        private FlowScanBarcode _flowScanBarcodeInstance;

        public override WorkProcess WorkProcess
        {
            get
            {
                return WorkProcess.Deviation;
            }
        }


        public FlowGeneralDeviations()
        {
            _messageHolder = new MessageHolder();
            _flowData = new FlowDataGeneralDeviations();
            _flowResult = new FlowResultGeneralDeviations();
            _actionCommandsGeneralDeviation = new ActionCommandsGeneralDeviation(CommunicationClient.Instance);
            _entityMap = new EntityMap();
            BarcodeType = BarcodeType.LoadCarrier | BarcodeType.ConsignmentItemNumber;
            _selectedReasonAndActionCombination = new CauseMeasure();
        }


        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowGeneralDeviations.BeginFlow");
            _flowData = (FlowDataGeneralDeviations)flowData;
            ExecuteState(FlowStatesGeneralDeviations.FlowCauseAndMeasure);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesGeneralDeviations)state);
        }

        public void ExecuteState(FlowStatesGeneralDeviations state)
        {
            if (state > FlowStatesGeneralDeviations.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesGeneralDeviations state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowGeneralDeviations;" + state); 

                switch (state)
                {
                    case FlowStatesGeneralDeviations.FlowCauseAndMeasure:
                        var flowDataCauseAndMeasure = new FlowDataCauseAndMeasure
                        {
                            Comment = _freeText,
                            SelectedReasonAndActionCombination = _selectedReasonAndActionCombination,
                            HeaderText = _flowData.HeaderText
                        };
                        StartSubFlow<FlowCauseAndMeasure>(flowDataCauseAndMeasure, (int)FlowStatesGeneralDeviations.ActionBackFromFlowCauseAndMeasure, Process.Inherit);
                        break;

                    case FlowStatesGeneralDeviations.FlowScanCorrectionMenu:
                        var flowData = new FlowDataCorrectionMenu(_flowData.HeaderText, _currentConsignmentEntity,
                                                                  _entityMap);
                        
                        StartSubFlow<FlowCorrectionMenu>(flowData, (int)FlowStatesGeneralDeviations.ActionBackFromFlowScanCorrectionMenu, Process.Inherit);
                        break;

                    case FlowStatesGeneralDeviations.FlowScanItemOrCarrierId:
                        ViewCommandsGeneralDeviation.ShowFormScanItemOrCarrieId(ScanItemOrCarrierIdEventHandler, _selectedReasonAndActionCombination, _freeText, _numberOfItems, _numberOfCarriers,_messageHolder, _flowData.HeaderText);
                        break;

                    case FlowStatesGeneralDeviations.ViewDeviationDetail:
                        ViewCommandsGeneralDeviation.ShowDviationDetail(DeviationDetailEventHandler, _selectedReasonAndActionCombination, _freeText, _numberOfItems, _messageHolder, _flowData.HeaderText);
                        break;

                    case FlowStatesGeneralDeviations.ThisFlowComplete:
                        _flowResult.State = FlowResultState.Ok;
                        if (_flowData.CurrentConsignmentEntity != null)
                            _flowResult.Message = GlobalTexts.DeviationRegistered + " " + _flowData.CurrentConsignmentEntity.CastToConsignmentItem().ItemDisplayId;
                        BaseModule.EndSubFlow(_flowResult);
                        break;
                    case FlowStatesGeneralDeviations.FlowExecuteVasMatrixOnLoadCarrierScan:
                        ExecuteVasMatrix(FlowStatesGeneralDeviations.ActionBackFromFlowExecuteVasMatrixOnLoadCarrierScan);
                        break;
                    case FlowStatesGeneralDeviations.FlowExecuteVasMatrix:
                        ExecuteVasMatrix(FlowStatesGeneralDeviations.ActionBackFromFlowVasMatrix);
                        break;

                    case FlowStatesGeneralDeviations.ThisFlowCanceled:
                        _flowResult.State = FlowResultState.Cancel;
                        BaseModule.EndSubFlow(_flowResult);
                        break;
                }

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowGeneralDeviations.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesGeneralDeviations.ThisFlowComplete);
            }
        }

        private void ExecuteVasMatrix(FlowStatesGeneralDeviations flowStatesGeneralDeviations)
        {
            var flowDataVasMatrix = new FlowDataVasMatrix(_currentConsignmentEntity, this);
            StartSubFlow<FlowVasMatrix>(flowDataVasMatrix, (int)flowStatesGeneralDeviations, Process.Inherit);
        }

        private void ExecuteActionState(FlowStatesGeneralDeviations state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowGeneralDeviations;" + state); 

                do
                {
                    switch (state)
                    {
                        case FlowStatesGeneralDeviations.ActionBackFromFlowMainScan:
                            state = _actionCommandsGeneralDeviation.ValidateFlowScanBarcode((FlowResultScanBarcode)SubflowResult, ref _currentConsignmentEntity, _messageHolder, ref _loadcarrier, _scannerOutput);
                            break;

                        case FlowStatesGeneralDeviations.ActionStoreLoadCarrier:
                            _loadcarrier = _currentConsignmentEntity.ConsignmentId;
                            _actionCommandsGeneralDeviation.StoreLoadCarrierEntity(_entityMap, _currentConsignmentEntity);
                            state = FlowStatesGeneralDeviations.ActionSendLoadCarrierEvent;
                            break;

                        case FlowStatesGeneralDeviations.ActionSendLoadCarrierEvent:
                            _actionCommandsGeneralDeviation.SendLoadCarrierEvent(_loadcarrier, "LBA", _selectedReasonAndActionCombination.CauseCode, _selectedReasonAndActionCombination.MeasureCode, _freeText);
                            state = FlowStatesGeneralDeviations.ActionGetItemCount;
                            break;

                        case FlowStatesGeneralDeviations.ActionGetItemCount:
                            _numberOfItems = _entityMap.ScannedConsignmentItemsTotalCount;
                            _numberOfCarriers = _entityMap.ScannedLoadCarrierTotalCount;
                            state = FlowStatesGeneralDeviations.FlowScanItemOrCarrierId;
                            break;


                        case FlowStatesGeneralDeviations.ActionRegisterGeneralDeviation:
                            OperationProcess operationProcess;
                            if (_flowData.IsStartedFromDoubleScan == false)
                            {
                                operationProcess = new OperationProcess { StopId = _stopId, TripId = _tripId, OperationId = _operationId };
                            }
                            else
                            {
                                operationProcess = ModelMain.SelectedOperationProcess;
                                _currentConsignmentEntity = _flowData.CurrentConsignmentEntity;
                            }
                            _actionCommandsGeneralDeviation.RegisterGeneralDeviation(_currentConsignmentEntity, operationProcess, _selectedReasonAndActionCombination.CauseCode, _selectedReasonAndActionCombination.MeasureCode, _freeText, "V", _loadcarrier, _flowData.IsStartedFromDoubleScan);
                            state = _flowData.IsStartedFromDoubleScan ? FlowStatesGeneralDeviations.ThisFlowComplete : FlowStatesGeneralDeviations.ActionStoreConsignmentItem;
                            break;

                        case FlowStatesGeneralDeviations.ActionStoreConsignmentItem:
                            _actionCommandsGeneralDeviation.StoreConsignmentItemEntity(_entityMap, _currentConsignmentEntity);
                            state = FlowStatesGeneralDeviations.ActionGetItemCount;
                            break;

                        case FlowStatesGeneralDeviations.ActionBackFromFlowScanCorrectionMenu:
                            _actionCommandsGeneralDeviation.ValidateFlowScanCorrectionMenuResult(SubflowResult, ref _messageHolder, ref _currentConsignmentEntity, _entityMap);
                            state = FlowStatesGeneralDeviations.ActionGetItemCount;
                            break;


                        case FlowStatesGeneralDeviations.ActionIsProcessStartFromDoubleScan:
                            bool result = _actionCommandsGeneralDeviation.IsProcessStartedFromDoubleScan(_flowData.IsStartedFromDoubleScan);
                            state = result
                                        ? FlowStatesGeneralDeviations.ViewDeviationDetail
                                        : FlowStatesGeneralDeviations.FlowScanItemOrCarrierId;
                            break;

                        case FlowStatesGeneralDeviations.ActionBackFromFlowVasMatrix:
                            state = _actionCommandsGeneralDeviation.ValidateFlowVasMatrixResult((FlowResultVasMatrix)SubflowResult, ref _messageHolder, false);
                            break;

                        case FlowStatesGeneralDeviations.ActionBackFromFlowExecuteVasMatrixOnLoadCarrierScan:
                            state = _actionCommandsGeneralDeviation.ValidateFlowVasMatrixResult((FlowResultVasMatrix)SubflowResult, ref _messageHolder, true);
                            break;

                        case FlowStatesGeneralDeviations.ActionBackFromFlowCauseAndMeasure:
                            bool isValid = _actionCommandsGeneralDeviation.VerifyFlowResultCauseAndMeasure((FlowResultCauseAndMeasure)SubflowResult, ref _selectedReasonAndActionCombination, out _freeText, _flowResult);

                            state = isValid ? FlowStatesGeneralDeviations.ActionIsProcessStartFromDoubleScan : FlowStatesGeneralDeviations.ThisFlowCanceled;
                            break;

                    }
                } while (state < FlowStatesGeneralDeviations.ViewCommands);

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowGeneralDeviations.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesGeneralDeviations.ThisFlowComplete);
            }
            ExecuteViewState(state);
        }

        private void ScanItemOrCarrierIdEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case ScanItemOrCarrierIdViewEvents.Back:
                    _messageHolder.Clear();
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStatesGeneralDeviations.FlowCauseAndMeasure);
                    break;

                case ScanItemOrCarrierIdViewEvents.BarcodeScanned:
                    GoToFlowScanBarcodeForMainScan();
                    _scannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                    _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
                    break;

                case ScanItemOrCarrierIdViewEvents.Deviation:
                    if (_entityMap != null && !_entityMap.IsScannedBefore(_currentConsignmentEntity))
                        _currentConsignmentEntity = null;
                    if (_currentConsignmentEntity != null)
                        if (_currentConsignmentEntity is LoadCarrier)
                        {
                            _messageHolder.Update(GlobalTexts.CanNotOpenDeviationForLoadCarrier, MessageState.Warning);
                            ExecuteState(FlowStatesGeneralDeviations.FlowScanItemOrCarrierId);
                        }
                        else
                        {
                            ExecuteViewState(FlowStatesGeneralDeviations.FlowScanCorrectionMenu);
                        }

                    else
                    {
                        _messageHolder.Update(GlobalTexts.NoItemScanned, MessageState.Warning);
                        ExecuteState(FlowStatesGeneralDeviations.FlowScanItemOrCarrierId);
                    }
                    break;

                case ScanItemOrCarrierIdViewEvents.Ok:
                    ExecuteState(FlowStatesGeneralDeviations.ThisFlowComplete);
                    break;

                case ScanItemOrCarrierIdViewEvents.Finish:
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStatesGeneralDeviations.ThisFlowComplete);
                    break;
            }
        }

        private void DeviationDetailEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case ScanItemOrCarrierIdViewEvents.Back:
                    ExecuteState(FlowStatesGeneralDeviations.FlowCauseAndMeasure);
                    break;

                case ScanItemOrCarrierIdViewEvents.Ok:
                    ExecuteState(FlowStatesGeneralDeviations.ActionRegisterGeneralDeviation);
                    break;
            }
        }
        private void GoToFlowScanBarcodeForMainScan()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowArrivalRegistration.GoToFlowScanBarcodeForMainScan");
            var flowData = new FlowDataScanBarcode(GlobalTexts.GeneralDeviations, _messageHolder, BarcodeType.ConsignmentItemNumber | BarcodeType.LoadCarrier, _entityMap) { HaveCustomForm = true, PlaySuccesSound = false};
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)FlowStatesGeneralDeviations.ActionBackFromFlowMainScan, Process.Inherit);
        }
    }
}

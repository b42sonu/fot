﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    public class ViewCommandsGeneralDeviation
    {
        public static void ShowFormScanItemOrCarrieId(ViewEventDelegate viewEventHandler, CauseMeasure selectedReasonAndAction, string freeText, int numberOfItems, int numberOfCarriers, MessageHolder messageHolder, string header)
        {
            var formDataScanItemOrCarrierId = new FormDataScanItemOrCarrierId();

            formDataScanItemOrCarrierId.SelectedReasonAndAction = selectedReasonAndAction;
            formDataScanItemOrCarrierId.FreeText = freeText;
            formDataScanItemOrCarrierId.NumberOfItems = numberOfItems;
            formDataScanItemOrCarrierId.NumberOfLoadCarriers = numberOfCarriers;
            formDataScanItemOrCarrierId.MessageHolder = messageHolder;
            formDataScanItemOrCarrierId.Header = header;
            var view = ViewCommands.ShowView<FormScanItemOrCarrieId>(formDataScanItemOrCarrierId);
            view.SetEventHandler(viewEventHandler);

        }

        public static void ShowDviationDetail(ViewEventDelegate viewEventHandler, CauseMeasure selectedReasonAndAction, string freeText, int numberOfItems, MessageHolder messageHolder, string header)
        {
            var formDataScanItemOrCarrierId = new FormDataDeviationDetail
                {
                    SelectedReasonAndAction = selectedReasonAndAction,
                    FreeText = freeText,
                    NumberOfItems = numberOfItems,
                    Header = header
                };
            var view = ViewCommands.ShowView<FormDeviationDetail>(formDataScanItemOrCarrierId);
            view.SetEventHandler(viewEventHandler);
        }
    }

}


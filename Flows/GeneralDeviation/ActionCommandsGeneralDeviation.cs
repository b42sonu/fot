﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    public class ActionCommandsGeneralDeviation
    {
        private static ICommunicationClient _communicationClient;
        public ActionCommandsGeneralDeviation(ICommunicationClient comClient)
        {
            _communicationClient = comClient;
        }
        public ActionCommandsGeneralDeviation()
        {
        }
        //TODO:: Write Test Case
        /// <summary>
        /// This method, validates the result from flow FlowShowOperations and decides which will be the next state.
        /// </summary>
        /// <returns>Returns next state to execute.</returns>
        public FlowStatesGeneralDeviations ValidateFlowScanBarcode(FlowResultScanBarcode subflowResult, ref ConsignmentEntity consignmentEntity, MessageHolder messageHolder, ref string loadCarrierId, FotScannerOutput scannerOutput)
        {
            FlowStatesGeneralDeviations resultState;

            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    switch (subflowResult.BarcodeType)
                    {
                        case BarcodeType.LoadCarrier:
                            consignmentEntity = subflowResult.CurrentConsignmentEntity;
                            loadCarrierId = subflowResult.CurrentConsignmentEntity.LoadCarrierId;
                            consignmentEntity.CastToLoadCarrier().GetMessageForRegistrationOfCarrier(messageHolder, null);
                  
                           /* if (consignmentEntity.IsScannedOnline)
                            {
                                messageHolder.Update(string.Format(GlobalTexts.LoadCarrierIsRegistered, consignmentEntity.EntityDisplayId);
                                messageHolder.State = MessageState.Information;
                            }
                            else
                            {
                                messageHolder.Update(string.Format(GlobalTexts.OfflineLoadCarrierIsRegistered, consignmentEntity.EntityDisplayId);
                                messageHolder.State = MessageState.Warning;
                            }*/
                            resultState = FlowStatesGeneralDeviations.ActionStoreLoadCarrier;
                            break;

                        case BarcodeType.ConsignmentItemNumber:
                            consignmentEntity = subflowResult.CurrentConsignmentEntity;
                            var consignmentItem = (ConsignmentItem)consignmentEntity;
                            if (consignmentItem != null)
                                if (consignmentItem.IsScannedOnline)
                                {
                                    messageHolder.Update(string.Format(GlobalTexts.ConsignmentItemRegistered, consignmentItem.ItemDisplayId), MessageState.Information);
                                }
                                else
                                {
                                    messageHolder.Update(string.Format(GlobalTexts.OfflineConsignmentItemRegistered, consignmentItem.ItemDisplayId), MessageState.Warning);
                                }
                            resultState = FlowStatesGeneralDeviations.FlowExecuteVasMatrix;

                            break;
                        default:
                            resultState = FlowStatesGeneralDeviations.FlowScanItemOrCarrierId;
                            break;
                    }
                    break;

                case FlowResultState.Duplicate:

                    switch (subflowResult.BarcodeType)
                    {
                        case BarcodeType.LoadCarrier:
                            messageHolder.Update(GlobalTexts.LoadCarrierAlreadyScanned, MessageState.Warning);
                            resultState = FlowStatesGeneralDeviations.FlowScanItemOrCarrierId;
                            break;

                        case BarcodeType.ConsignmentItemNumber:
                            consignmentEntity = subflowResult.CurrentConsignmentEntity;
                            resultState = FlowStatesGeneralDeviations.FlowScanCorrectionMenu;
                            break;

                        default:
                            resultState = FlowStatesGeneralDeviations.ThisFlowComplete;
                            break;
                    }
                    break;

                case FlowResultState.Warning:
                case FlowResultState.Error:
                    resultState = FlowStatesGeneralDeviations.FlowScanItemOrCarrierId;

                    break;
                case FlowResultState.Cancel:
                case FlowResultState.Finished:
                    resultState = FlowStatesGeneralDeviations.ThisFlowComplete;
                    break;
                default:
                    resultState = FlowStatesGeneralDeviations.ThisFlowComplete;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            return resultState;
        }
       
        
        //TODO:: Write Test Case
        public void StoreLoadCarrierEntity(EntityMap entityMap, ConsignmentEntity consignmentEntity)
        {
            StoreConsignmentItemEntity(entityMap, consignmentEntity);
        }
        //TODO:: Write Test Case
        public void StoreConsignmentItemEntity(EntityMap entityMap, ConsignmentEntity consignmentEntity)
        {
            consignmentEntity.EntityStatus |= EntityStatus.Scanned;
            entityMap.Store(consignmentEntity);
        }
        //TODO:: Write Test Case
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadcarrier"></param>
        /// <param name="eventCode"></param>
        /// <param name="reasonCode"></param>
        /// <param name="actionCode"></param>
        /// <param name="comment"></param>
        public void SendLoadCarrierEvent(string loadcarrier, string eventCode, string reasonCode, string actionCode, string comment)
        {
            if (string.IsNullOrEmpty(loadcarrier))
            {
                return;
            }
            var eventInfo = LoadCarrierEventHelper.CreateLoadCarrierEvent(loadcarrier, null, null, null, null, eventCode, null);
            if (eventInfo != null)
            {
                eventInfo.EventInformation.ActionCode = actionCode;
                eventInfo.EventInformation.ReasonCode = reasonCode;
                eventInfo.EventInformation.Comment = comment;
                if (CommunicationClient.Instance != null)
                {
                    CommunicationClient.Instance.SendLoadCarrierEvent(eventInfo);
                }
            }
        }
        //TODO:: Write Test Case
        public void StoreConsignmentItemEntityIntoEntityMap(EntityMap entityMap, ConsignmentEntity consignmentEntity)
        {
            consignmentEntity.EntityStatus |= EntityStatus.Scanned;
            entityMap.Store(consignmentEntity);
        }
        //TODO:: Write Test Case
        /// <summary>
        /// This method, validates the result from flow FlowCorrectionMenu and decides which will be the next state.
        /// </summary>
        /// <param name="subflowResult">Specifies the result from sub flow.</param>
        /// <param name="messageHolder">Specifies the message holder.</param>
        /// <param name="consignmentEntity"></param>
        /// <param name="entityMap"> </param>
        public void ValidateFlowScanCorrectionMenuResult(IFlowResult subflowResult, ref MessageHolder messageHolder, ref ConsignmentEntity consignmentEntity, EntityMap entityMap)
        {
            if (messageHolder == null)
                throw new ArgumentNullException("messageHolder");

            var flowResultScanCorrectionMenu = (FlowResultCorrectionMenu)subflowResult;
            messageHolder = flowResultScanCorrectionMenu.MessageHolder;
            switch (flowResultScanCorrectionMenu.State)
            {
                case FlowResultState.Ok:
                    if (!entityMap.IsScannedBefore(consignmentEntity))
                        consignmentEntity = null;
                    break;
                case FlowResultState.Cancel:
                    break;
            }
        }


        public bool IsProcessStartedFromDoubleScan(bool isprocessStartsFromDoubleScan)
        {
            return isprocessStartsFromDoubleScan;
        }


        #region "Goods event detail"

        /// <summary>
        ///  This method will send send goods event for  process General deviation with the information of reason code , Action Code , Event Code etc
        /// </summary>
        public bool RegisterGeneralDeviation(ConsignmentEntity consignmentEntity, OperationProcess operationProcess, string reasonCode, string actionCode, string comment, string eventCode, string loadCarrier, bool isStartedFromDoubleScan)
        {
            bool result = false;

            try
            {

                if ((consignmentEntity is ConsignmentItem))
                {
                    PreComFW.CommunicationEntity.GoodsEvents.GoodsEvents goodsEvent =
                        GoodsEventHelper.CreateGoodsEvent(consignmentEntity, eventCode);

                    if (goodsEvent != null)
                    {
                        //set temerature data and comment in event information...
                        if (goodsEvent.EventInformation != null)
                        {
                            goodsEvent.EventInformation.CauseCode = reasonCode;
                            goodsEvent.EventInformation.ActionCode = actionCode;
                            goodsEvent.EventInformation.EventComment = comment;
                            goodsEvent.EventInformation.EventCode = eventCode;
                        }
                        if (isStartedFromDoubleScan == false)
                        consignmentEntity.GoodsEvent = goodsEvent;
                        var transaction = new Transaction(Guid.NewGuid(),
                                                          (byte)TransactionCategory.GoodsEventsEventInformation,
                                                          consignmentEntity.OrderNumber);

                        if (_communicationClient != null)
                        {
                            _communicationClient.SendGoodsEvent(goodsEvent, transaction);
                            result = true;
                        }

                    }
                }
                else
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsGeneralDeviation.RegisterGeneralDeviation");
            }
            return result;
        }

        #endregion

        /// <summary>
        /// This method, validates the result from flow FlowCorrectionMenu and decides which will be the next state.
        /// </summary>
        /// <param name="subflowResult">Specifies the result from sub flow.</param>
        /// <param name="messageHolder">Specifies the message holder.</param>
        /// <param name="isScanLoadCarrier"></param>
        public FlowStatesGeneralDeviations ValidateFlowVasMatrixResult(FlowResultVasMatrix subflowResult, ref MessageHolder messageHolder, bool isScanLoadCarrier)
        {
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    {
                        if (subflowResult.MessageIsFromVasMatrix)
                        {
                            messageHolder.Update(subflowResult.Message, MessageState.Warning);
                        }
                        return isScanLoadCarrier ? FlowStatesGeneralDeviations.ActionStoreLoadCarrier : FlowStatesGeneralDeviations.ActionRegisterGeneralDeviation;
                    }

                case FlowResultState.Cancel:
                    messageHolder.Update(subflowResult.Message, MessageState.Warning);
                    return FlowStatesGeneralDeviations.FlowScanItemOrCarrierId;
            }
            return FlowStatesGeneralDeviations.ThisFlowComplete;
        }

        /// <summary>
        /// method for verify flow result when back from departure from stop flow..
        /// </summary>
        /// <param name="flowResultCauseAndMeasure"> </param>
        /// <param name="causeMeasure"> </param>
        /// <param name="freeText"> </param>
        /// <param name="flowresult"></param>
        public bool VerifyFlowResultCauseAndMeasure(FlowResultCauseAndMeasure flowResultCauseAndMeasure, ref CauseMeasure causeMeasure, out string freeText, FlowResultGeneralDeviations flowresult)
        {
            bool isValid = flowResultCauseAndMeasure != null && flowResultCauseAndMeasure.State == FlowResultState.Ok;
            freeText = string.Empty;
            if (isValid)
            {
                causeMeasure = flowResultCauseAndMeasure.SelectedReasonAndActionCombination;
                freeText = flowResultCauseAndMeasure.Comment;
            }
            else
            {
                flowresult.State = FlowResultState.Cancel;
            }
            return isValid;
        }
    }
}

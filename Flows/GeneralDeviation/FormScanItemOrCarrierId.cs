﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    public class FormDataScanItemOrCarrierId : BaseFormData
    {
        public CauseMeasure SelectedReasonAndAction { get; set; }
        public string FreeText { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public int NumberOfItems { get; set; }
        public string Header { get; set; }
        public int NumberOfLoadCarriers { get; set; }
    }

    public partial class FormScanItemOrCarrieId : BaseBarcodeScanForm
    {

        private FormDataScanItemOrCarrierId _formData;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        private const string ButtonDeviation = "ButtonDeviation";

        public FormScanItemOrCarrieId()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            SetStandardControlProperties(labelModuleName, lblOrgnaisationUnitName, lblScanHeading, txtScannedNumber, messageControl, lblNumberOfitems, lblNoOfLoadCarriers);
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToControls()
        {
            lblScanHeading.Text = GlobalTexts.ScanOrEnterItemOrCarrier + Colon;
            lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Deviation, ButtonDeviationClick) { Name = ButtonDeviation, Activation = ActivateOn.IfScanned });

            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };
            txtScannedNumber.Tag = keyBoardProperty;
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOk, txtScannedNumber.Text != string.Empty || _formData.NumberOfItems + _formData.NumberOfLoadCarriers > 0);
            if (messageControl != null)
                messageControl.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text != string.Empty)
                BarcodeEnteredManually(textentered);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="textentered"></param>
        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(FormDamageScan.DamageScanViewEvents.BarcodeScanned, scannerOutput);
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            messageControl.ClearMessage();
            OnUpdateView(formData);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            if (iformData != null)
            {
                _formData = (FormDataScanItemOrCarrierId)iformData;
                if (_formData.SelectedReasonAndAction != null)
                {
                    lblReason.Text = _formData.SelectedReasonAndAction.CauseCode ;
                    lblAction.Text = _formData.SelectedReasonAndAction.MeasureCode ;
                    lblReason.Text += _formData.SelectedReasonAndAction.CauseCode == string.Empty ? string.Empty : Colon + " ";
                    lblAction.Text += _formData.SelectedReasonAndAction.MeasureCode == string.Empty ? string.Empty : Colon + " ";
                    lblReasonValue.Text = StringUtil.ReturnCommonFormattedText( _formData.SelectedReasonAndAction.CauseDescription,35);
                    lblActionValue.Text = StringUtil.ReturnCommonFormattedText(_formData.SelectedReasonAndAction.MeasureDescription, 35);
                    lblFreeText.Text = GlobalTexts.Comment + ":";
                    txtFreeTextDescription.Text = _formData.FreeText;
                    lblNumberOfitems.Text = GlobalTexts.NoOfItems + ": " + _formData.NumberOfItems;
                    lblNoOfLoadCarriers.Text = GlobalTexts.NoOfCarriers + ": " + _formData.NumberOfLoadCarriers;
                    labelModuleName.Text = _formData.Header;
                    _multiButtonControl.ScannedItemCount = _formData.NumberOfItems + _formData.NumberOfLoadCarriers;
                    _multiButtonControl.SetButtonEnabledState(ButtonBack, _formData.NumberOfItems + _formData.NumberOfLoadCarriers == 0);
                    _multiButtonControl.SetButtonEnabledState(ButtonOk, _formData.NumberOfItems + _formData.NumberOfLoadCarriers > 0 || txtScannedNumber.Text != string.Empty);
                }


                if (_formData.MessageHolder.State != MessageState.Error)
                    txtScannedNumber.Text = string.Empty;

                PopulateMessage(_formData.MessageHolder);

            }
            SetKeyboardState(KeyboardState.Numeric);

        }
        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null && string.IsNullOrEmpty(messageHolder.Text) == false)
                messageControl.ShowMessage(messageHolder);
            else
                messageControl.ClearMessage();
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtScannedNumber.Text = string.Empty;
            ViewEvent.Invoke(FormDamageScan.DamageScanViewEvents.BarcodeScanned, scannerOutput);
        }

        

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ScanItemOrCarrierIdViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDataScanItemOrCarrierId.ButtonBackClick");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDeviationClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ScanItemOrCarrierIdViewEvents.Deviation);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanArrivalRegistration.ButtonDeviationClick");
            }
        }


        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtScannedNumber.Text))
                    ViewEvent.Invoke(ScanItemOrCarrierIdViewEvents.Finish);
                else
                    BarcodeEnteredManually(txtScannedNumber.Text.Trim());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDataScanItemOrCarrierId.ButtonBackClick");
            }
        }
    }
    #region View specific classes


    #endregion

    public class ScanItemOrCarrierIdViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
        public const int Finish = 3;
        public const int Deviation = 4;
    }
}

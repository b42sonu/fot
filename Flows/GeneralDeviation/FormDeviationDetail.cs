﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    public class FormDataDeviationDetail : BaseFormData
    {
        public CauseMeasure SelectedReasonAndAction { get; set; }
        public string FreeText { get; set; }
        public int NumberOfItems { get; set; }
        public string Header { get; set; }
    }

    public partial class FormDeviationDetail : BaseForm
    {
        private FormDataDeviationDetail _formData;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();

        public FormDeviationDetail()
        {
            InitializeComponent();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            SetStandardControlProperties(labelModuleName, lblOrgnaisationUnitName, null, null, null, null, null);
        }
        private void SetTextToControls()
        {
           
            lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm));
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
        }
       

       public override void OnShow(IFormData formData)
        {
            if (formData != null)
            {
                _formData = (FormDataDeviationDetail)formData;
                if (_formData.SelectedReasonAndAction != null)
                {
                    lblReasonCode.Text = _formData.SelectedReasonAndAction.CauseCode + Colon;
                    lblReason.Text = _formData.SelectedReasonAndAction.CauseDescription ;
                    lblActionCode.Text = _formData.SelectedReasonAndAction.MeasureCode+Colon;
                    lblAction.Text =_formData.SelectedReasonAndAction.MeasureDescription;
                    lblFreeText.Text = GlobalTexts.FreeText + Colon;
                    txtFreeTextDescription.Text = _formData.FreeText;
                    labelModuleName.Text = _formData.Header;
                }
                
            }
            
        }

       private void ButtonBackClick(object sender, EventArgs e)
       {
           try
           {
               ViewEvent.Invoke(DeviationDetailViewEvents.Back);
           }
           catch (Exception ex)
           {
               Logger.LogException(ex, "FormDeviationDetail.ButtonBackClick");
           }
       }

       private void ButtonOkClick(object sender, EventArgs e)
       {
           try
           {
               ViewEvent.Invoke(DeviationDetailViewEvents.Ok);
           }
           catch (Exception ex)
           {
               Logger.LogException(ex, "FormDeviationDetail.ButtonBackClick");
           }
       }
    }

    public class DeviationDetailViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }
}
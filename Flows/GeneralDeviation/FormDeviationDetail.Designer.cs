﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    partial class FormDeviationDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.txtFreeTextDescription = new System.Windows.Forms.TextBox();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReason = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblActionCode = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReasonCode = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblFreeText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblActionCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFreeText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgnaisationUnitName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.txtFreeTextDescription);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblAction);
            this.touchPanel.Controls.Add(this.lblReason);
            this.touchPanel.Controls.Add(this.lblActionCode);
            this.touchPanel.Controls.Add(this.lblReasonCode);
            this.touchPanel.Controls.Add(this.lblFreeText);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // txtFreeTextDescription
            // 
            this.txtFreeTextDescription.BackColor = System.Drawing.Color.White;
            this.txtFreeTextDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFreeTextDescription.Location = new System.Drawing.Point(140, 212);
            this.txtFreeTextDescription.Multiline = true;
            this.txtFreeTextDescription.Name = "txtFreeTextDescription";
            this.txtFreeTextDescription.ReadOnly = true;
            this.txtFreeTextDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtFreeTextDescription.Size = new System.Drawing.Size(320, 60);
            this.txtFreeTextDescription.TabIndex = 1;
            // 
            // labelModuleName
            // 
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 4);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(477, 35);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblAction
            // 
            this.lblAction.AutoSize = false;
            this.lblAction.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblAction.Location = new System.Drawing.Point(73, 147);
            this.lblAction.Name = "lblAction";
            this.lblAction.Size = new System.Drawing.Size(385, 60);
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = false;
            this.lblReason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblReason.Location = new System.Drawing.Point(73, 82);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(385, 60);
            // 
            // lblActionCode
            // 
            this.lblActionCode.AutoSize = false;
            this.lblActionCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblActionCode.Location = new System.Drawing.Point(21, 147);
            this.lblActionCode.Name = "lblActionCode";
            this.lblActionCode.Size = new System.Drawing.Size(50, 30);
            this.lblActionCode.TextAlignment = Resco.Controls.CommonControls.Alignment.TopRight;
            // 
            // lblReasonCode
            // 
            this.lblReasonCode.AutoSize = false;
            this.lblReasonCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblReasonCode.Location = new System.Drawing.Point(21, 82);
            this.lblReasonCode.Name = "lblReasonCode";
            this.lblReasonCode.Size = new System.Drawing.Size(50, 30);
            this.lblReasonCode.TextAlignment = Resco.Controls.CommonControls.Alignment.TopRight;
            // 
            // lblFreeText
            //
            
            this.lblFreeText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblFreeText.Location = new System.Drawing.Point(21, 212);
            this.lblFreeText.Name = "lblFreeText";

            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 42);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormAttemptedPickup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDeviationDetail";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblActionCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFreeText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgnaisationUnitName)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblReason;
        private Resco.Controls.CommonControls.TransparentLabel lblAction;
        private Resco.Controls.CommonControls.TransparentLabel lblFreeText;
        private Resco.Controls.CommonControls.TransparentLabel lblReasonCode;
        private Resco.Controls.CommonControls.TransparentLabel lblActionCode;
        private System.Windows.Forms.TextBox txtFreeTextDescription;
    }
}
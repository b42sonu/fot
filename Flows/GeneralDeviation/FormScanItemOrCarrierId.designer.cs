﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation
{
    partial class FormScanItemOrCarrieId
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblScanHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtScannedNumber = new PreCom.Controls.PreComInput2();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblActionValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReasonValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblFreeText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNumberOfitems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtFreeTextDescription = new System.Windows.Forms.TextBox();
            this.lblReason = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAction = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNoOfLoadCarriers = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblActionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFreeText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfitems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).BeginInit();
            this.touchPanel.Controls.Add(this.txtFreeTextDescription);
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgnaisationUnitName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.lblScanHeading);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblActionValue);
            this.touchPanel.Controls.Add(this.lblReasonValue);
            this.touchPanel.Controls.Add(this.lblNoOfLoadCarriers);
            this.touchPanel.Controls.Add(this.lblFreeText);
            this.touchPanel.Controls.Add(this.lblNumberOfitems);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.lblReason);
            this.touchPanel.Controls.Add(this.lblAction);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(10, 155);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(465, 110);
            this.messageControl.TabIndex = 56;

            // 
            // lblScanHeading
            // 
            this.lblScanHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblScanHeading.Location = new System.Drawing.Point(21, 71);
            this.lblScanHeading.Name = "lblScanHeading";
            this.lblScanHeading.Size = new System.Drawing.Size(55, 27);
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(21, 105);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(465, 40);
            this.txtScannedNumber.TabIndex = 37;

            this.lblReason.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblReason.Location = new System.Drawing.Point(21, 259);
            this.lblReason.Name = "lblMeasure";
            this.lblReason.Size = new System.Drawing.Size(50, 30);
            this.lblReason.AutoSize = false;
            this.lblReason.TextAlignment = Resco.Controls.CommonControls.Alignment.TopRight;
            // 
            // lblCause
            // 
            this.lblAction.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblAction.Location = new System.Drawing.Point(21, 295);
            this.lblAction.Name = "lblCause";
            this.lblAction.Size = new System.Drawing.Size(50, 30);
            this.lblAction.AutoSize = false;
            this.lblAction.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomRight;

            // 
            // lblNoOfLoadCarriers
            // 
            this.lblNoOfLoadCarriers.Location = new System.Drawing.Point(161 + 19, 348);
            this.lblNoOfLoadCarriers.Name = "lblNoOfLoadCarriers";
            this.lblNoOfLoadCarriers.Size = new System.Drawing.Size(14, 29);
            // 
            // labelModuleName
            // 
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 4);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(477, 35);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblReasonValue
            // 
            this.lblReasonValue.Size = new System.Drawing.Size(385, 30);
            this.lblReasonValue.Location = new System.Drawing.Point(73, 259);
            this.lblReasonValue.Name = "lblReasonValue";
            this.lblReasonValue.AutoSize = false;
            // 
            // lblActionValue
            // 
            this.lblActionValue.Location = new System.Drawing.Point(73, 295);
            this.lblActionValue.Name = "lblActionValue";
            this.lblActionValue.Size = new System.Drawing.Size(385, 30);
            this.lblActionValue.AutoSize = false;
            // 
            // lblFreeText
            // 
            this.lblFreeText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblFreeText.AutoSize = false;
            this.lblFreeText.Location = new System.Drawing.Point(21, 331);
            this.lblFreeText.Name = "lblFreeText";
            this.lblFreeText.Size = new System.Drawing.Size(115, 26);
            // 
            // txtFreeTextDescription
            // 
            this.txtFreeTextDescription.Location = new System.Drawing.Point(140, 331);
            this.txtFreeTextDescription.Multiline = true;
            this.txtFreeTextDescription.Name = "txtFreeTextDescription";
            this.txtFreeTextDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFreeTextDescription.Size = new System.Drawing.Size(320, 56);
            this.txtFreeTextDescription.BorderStyle = BorderStyle.None;
            this.txtFreeTextDescription.BackColor = System.Drawing.Color.White;
            this.txtFreeTextDescription.ReadOnly = true;
            // 
            // lblNumberOfitems
            // 
            this.lblNumberOfitems.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNumberOfitems.Location = new System.Drawing.Point(10 + 11, 455);
            this.lblNumberOfitems.Name = "lblNumberOfitems";
            this.lblNumberOfitems.Size = new System.Drawing.Size(210, 27);
            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormScanItemOrCarrieId
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormScanItemOrCarrieId";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblActionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReasonValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFreeText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfitems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgnaisationUnitName)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private PreCom.Controls.PreComInput2 txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblScanHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblReasonValue;
        private Resco.Controls.CommonControls.TransparentLabel lblActionValue;
        private Resco.Controls.CommonControls.TransparentLabel lblFreeText;
        private Resco.Controls.CommonControls.TransparentLabel lblNumberOfitems;
        private MessageControl messageControl;
        private System.Windows.Forms.TextBox txtFreeTextDescription;
        private Resco.Controls.CommonControls.TransparentLabel lblReason;
        private Resco.Controls.CommonControls.TransparentLabel lblAction;
        private TransparentLabel lblNoOfLoadCarriers;
        #endregion
    }
}

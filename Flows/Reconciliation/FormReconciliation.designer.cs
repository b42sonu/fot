﻿using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation
{
    partial class FormReconciliation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.ListConsignments = new Resco.Controls.AdvancedList.AdvancedList();
            this.templateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellHeaderCol0 = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderBottomBorder = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellSelectedConsignmentStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.imageListStatus = new System.Windows.Forms.ImageList() { ImageSize = new Size(20, 20) };//
            this.cellSelectedConsignmentId = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedActualConsignmentId = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedConsignmentStatusCount = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowUnselected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellUnselectedConsignmentStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.cellUnselectedConsignmentId = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedActualConsignmentId = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedConsignmentStatusCount = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelectedConsItems = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellItemSelectedConsItemId = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemSelectedActualConsItemId = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemSelectedConsItemStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.templateRowUnselectedConsItems = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellItemUnselectedConsItemId = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemUnselectedActualConsItemId = new Resco.Controls.AdvancedList.TextCell();
            this.cellItemUnselectedConsItemStatusSymbol = new Resco.Controls.AdvancedList.ImageCell();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.ListConsignments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblOrgUnitName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            
            // 
            // ListConsignments
            // 
            this.ListConsignments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.ListConsignments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListConsignments.DataRows.Clear();
            this.ListConsignments.GridColor = System.Drawing.Color.Black;
            this.ListConsignments.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.ListConsignments.Location = new System.Drawing.Point(21, 90);
            this.ListConsignments.Name = "ListConsignments";
            this.ListConsignments.ScrollbarSmallChange = 32;
            this.ListConsignments.ScrollbarWidth = 26;
            this.ListConsignments.ShowHeader = true;
            this.ListConsignments.Size = new System.Drawing.Size(438, 295);
            this.ListConsignments.TabIndex = 3;
            this.ListConsignments.Templates.Add(this.templateHeader);
            this.ListConsignments.Templates.Add(this.templateRowSelected);
            this.ListConsignments.Templates.Add(this.templateRowUnselected);
            this.ListConsignments.Templates.Add(this.templateRowSelectedConsItems);
            this.ListConsignments.Templates.Add(this.templateRowUnselectedConsItems);
            this.ListConsignments.KeyNavigation = true;
            this.ListConsignments.MultiSelect = false;
            this.ListConsignments.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            // 
            // templateHeader
            // 
            this.templateHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol0);
            this.templateHeader.CellTemplates.Add(this.cellHeaderBottomBorder);
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol1);
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol2);
            this.templateHeader.Height = 33;
            this.templateHeader.Name = "templateHeader";
            // 
            // cellHeaderCol0
            // 
            this.cellHeaderCol0.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderCol0.CellSource.ConstantData = "S";
            this.cellHeaderCol0.DesignName = "cellHeaderCol0";
            this.cellHeaderCol0.Location = new System.Drawing.Point(20, 0);
            this.cellHeaderCol0.Name = "cellHeaderCol0";
            this.cellHeaderCol0.Size = new System.Drawing.Size(-1, 28);
            this.cellHeaderCol0.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellHeaderCol1
            // 
            this.cellHeaderCol1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellHeaderCol1.CellSource.ConstantData = "Consignments";
            this.cellHeaderCol1.DesignName = "cellHeaderCol1";
            this.cellHeaderCol1.Location = new System.Drawing.Point(0, 0);
            this.cellHeaderCol1.Name = "cellHeaderCol1";
            this.cellHeaderCol1.Size = new System.Drawing.Size(270, 30);
            this.cellHeaderCol1.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellHeaderCol2
            // 
            this.cellHeaderCol2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderCol2.CellSource.ConstantData = "Actual/Planned";
            this.cellHeaderCol2.DesignName = "cellHeaderCol2";
            this.cellHeaderCol2.Location = new System.Drawing.Point(320, 0);
            this.cellHeaderCol2.Name = "cellHeaderCol2";
            this.cellHeaderCol2.Size = new System.Drawing.Size(150, 30);
            this.cellHeaderCol2.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellHeaderBottomBorder
            // 
            this.cellHeaderBottomBorder.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderBottomBorder.BackColor = System.Drawing.Color.Black;
            this.cellHeaderBottomBorder.DesignName = "cellHeaderBottomBorder";
            this.cellHeaderBottomBorder.Location = new System.Drawing.Point(0, 30);
            this.cellHeaderBottomBorder.Name = "cellHeaderBottomBorder";
            this.cellHeaderBottomBorder.Size = new System.Drawing.Size(438, 5);
            // 
            // templateRowSelected
            // 
            this.templateRowSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(190)))), ((int)(((byte)(74)))));
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedConsignmentStatusSymbol);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedConsignmentId);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedConsignmentStatusCount);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedActualConsignmentId);
            this.templateRowSelected.Height = 30;
            this.templateRowSelected.Name = "templateRowSelected";
            // 
            // cellSelectedConsignmentStatusSymbol
            // 
            this.cellSelectedConsignmentStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellSelectedConsignmentStatusSymbol.AutoTransparent = true;
            this.cellSelectedConsignmentStatusSymbol.CellSource.ColumnIndex = 0;
            this.cellSelectedConsignmentStatusSymbol.DesignName = "cellSelectedConsignmentStatusSymbol";
            this.cellSelectedConsignmentStatusSymbol.ImageList = this.imageListStatus;
            this.cellSelectedConsignmentStatusSymbol.Location = new System.Drawing.Point(5, 0);
            this.cellSelectedConsignmentStatusSymbol.Size = new System.Drawing.Size(20, 30);
            // 
            // cellSelectedConsignmentId
            // 
            this.cellSelectedConsignmentId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellSelectedConsignmentId.CellSource.ColumnIndex = 1;
            this.cellSelectedConsignmentId.DesignName = "cellSelectedConsignmentId";
            this.cellSelectedConsignmentId.Location = new System.Drawing.Point(25, 0);
            this.cellSelectedConsignmentId.Size = new System.Drawing.Size(280, 30);
            this.cellSelectedConsignmentId.ForeColor = System.Drawing.Color.White;
            this.cellSelectedConsignmentId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellSelectedActualConsignmentId
            // 
            this.cellSelectedActualConsignmentId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellSelectedActualConsignmentId.CellSource.ColumnIndex = 4;
            this.cellSelectedActualConsignmentId.DesignName = "cellSelectedActualConsignmentId";
            this.cellSelectedActualConsignmentId.ForeColor = System.Drawing.Color.White;
            this.cellSelectedActualConsignmentId.Visible = false;
            this.cellSelectedActualConsignmentId.TextFont = new System.Drawing.Font("Arial", 9F,
                                                                                    System.Drawing.FontStyle.Regular);

            // 
            // cellSelectedConsignmentStatusCount
            // 
            this.cellSelectedConsignmentStatusCount.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellSelectedConsignmentStatusCount.CellSource.ColumnIndex = 2;
            this.cellSelectedConsignmentStatusCount.DesignName = "cellSelectedConsignmentStatusCount";
            this.cellSelectedConsignmentStatusCount.Location = new System.Drawing.Point(320, 0);
            this.cellSelectedConsignmentStatusCount.Size = new System.Drawing.Size(-1, 30);
            this.cellSelectedConsignmentStatusCount.ForeColor = System.Drawing.Color.White;
            this.cellSelectedConsignmentStatusCount.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // templateRowUnselected
            // 
            //   this.templateRowUnselected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowUnselected.BackColor = System.Drawing.Color.White;
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedConsignmentStatusSymbol);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedConsignmentId);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedConsignmentStatusCount);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedActualConsignmentId);
            this.templateRowUnselected.Height = 30;
            this.templateRowUnselected.Name = "templateRowUnselected";
            // 
            // cellUnselectedConsignmentStatusSymbol
            // 
            this.cellUnselectedConsignmentStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellUnselectedConsignmentStatusSymbol.AutoTransparent = true;
            this.cellUnselectedConsignmentStatusSymbol.CellSource.ColumnIndex = 0;
            this.cellUnselectedConsignmentStatusSymbol.DesignName = "cellUnselectedConsignmentStatusSymbol";
            this.cellUnselectedConsignmentStatusSymbol.ImageList = this.imageListStatus;
            this.cellUnselectedConsignmentStatusSymbol.Location = new System.Drawing.Point(5, 0);
            this.cellUnselectedConsignmentStatusSymbol.Size = new System.Drawing.Size(-1, 30);
            // 
            // cellUnselectedConsignmentId
            // 
            this.cellUnselectedConsignmentId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellUnselectedConsignmentId.CellSource.ColumnIndex = 1;
            this.cellUnselectedConsignmentId.DesignName = "cellUnselectedConsignmentId";
            this.cellUnselectedConsignmentId.Location = new System.Drawing.Point(25, 0);
            this.cellUnselectedConsignmentId.Size = new System.Drawing.Size(280, 30);
            this.cellUnselectedConsignmentId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);

            // 
            // cellUnselectedActualConsignmentId
            // 
            this.cellUnselectedActualConsignmentId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellUnselectedActualConsignmentId.CellSource.ColumnIndex = 4;
            this.cellUnselectedActualConsignmentId.DesignName = "cellUnselectedActualConsignmentId";
            this.cellUnselectedActualConsignmentId.Location = new System.Drawing.Point(25, 0);
            this.cellUnselectedActualConsignmentId.Visible = false;
            this.cellUnselectedActualConsignmentId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellUnselectedConsignmentStatusCount
            // 
            this.cellUnselectedConsignmentStatusCount.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellUnselectedConsignmentStatusCount.CellSource.ColumnIndex = 2;
            this.cellUnselectedConsignmentStatusCount.DesignName = "cellUnselectedConsignmentStatusCount";
            this.cellUnselectedConsignmentStatusCount.Location = new System.Drawing.Point(320, 0);
            this.cellUnselectedConsignmentStatusCount.Size = new System.Drawing.Size(-1, 30);
            this.cellUnselectedConsignmentStatusCount.TextFont = new System.Drawing.Font("Arial", 9f, System.Drawing.FontStyle.Regular);
            // 
            // templateRowSelectedConsItems
            // 
            //   this.templateRowSelectedConsItems.BackColor = System.Drawing.Color.DarkGray;
            this.templateRowSelectedConsItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(190)))), ((int)(((byte)(74)))));
            this.templateRowSelectedConsItems.CellTemplates.Add(this.cellItemSelectedConsItemId);
            this.templateRowSelectedConsItems.CellTemplates.Add(this.cellItemSelectedConsItemStatusSymbol);
            this.templateRowSelectedConsItems.CellTemplates.Add(this.cellItemSelectedActualConsItemId);
            this.templateRowSelectedConsItems.Height = 30;
            this.templateRowSelectedConsItems.Name = "templateRowSelectedConsItems";
            // 
            // cellItemSelectedConsItemId
            // 
            this.cellItemSelectedConsItemId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellItemSelectedConsItemId.DesignName = "cellItemSelectedConsItemId";
            this.cellItemSelectedConsItemId.Location = new System.Drawing.Point(0, 0);
            this.cellItemSelectedConsItemId.Size = new System.Drawing.Size(280, 30);
            this.cellItemSelectedConsItemId.ForeColor = System.Drawing.Color.White;
            this.cellItemSelectedConsItemId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);

            // 
            // cellItemSelectedActualConsItemId
            // 
            this.cellItemSelectedActualConsItemId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellItemSelectedActualConsItemId.DesignName = "cellItemSelectedActualConsItemId";
            this.cellItemSelectedActualConsItemId.Location = new System.Drawing.Point(0, 0);
            this.cellItemSelectedActualConsItemId.Visible = false;
            this.cellItemSelectedActualConsItemId.ForeColor = System.Drawing.Color.White;
            this.cellItemSelectedActualConsItemId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellItemSelectedConsItemStatusSymbol
            // 
            this.cellItemSelectedConsItemStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellItemSelectedConsItemStatusSymbol.AutoTransparent = true;
            this.cellItemSelectedConsItemStatusSymbol.CellSource.ColumnIndex = 1;
            this.cellItemSelectedConsItemStatusSymbol.DesignName = "cellItemSelectedConsItemStatusSymbol";
            this.cellItemSelectedConsItemStatusSymbol.ImageList = this.imageListStatus;
            this.cellItemSelectedConsItemStatusSymbol.Location = new System.Drawing.Point(320, 0);
            this.cellItemSelectedConsItemStatusSymbol.Size = new System.Drawing.Size(-1, 30);
            // 
            // templateRowUnselectedConsItems
            // 
            // this.templateRowUnselectedConsItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(230)))), ((int)(((byte)(231)))));
            this.templateRowUnselectedConsItems.BackColor = System.Drawing.Color.White;
            this.templateRowUnselectedConsItems.CellTemplates.Add(this.cellItemUnselectedConsItemId);
            this.templateRowUnselectedConsItems.CellTemplates.Add(this.cellItemUnselectedConsItemStatusSymbol);
            this.templateRowUnselectedConsItems.CellTemplates.Add(this.cellItemUnselectedActualConsItemId);
            this.templateRowUnselectedConsItems.Height = 30;
            this.templateRowUnselectedConsItems.Name = "templateRowUnselectedConsItems";
            // 
            // cellItemUnselectedConsItemId
            // 
            this.cellItemUnselectedConsItemId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellItemUnselectedConsItemId.DesignName = "cellItemUnselectedConsItemId";
            this.cellItemUnselectedConsItemId.Location = new System.Drawing.Point(0, 0);
            this.cellItemUnselectedConsItemId.Size = new System.Drawing.Size(280, 30);
            this.cellItemUnselectedConsItemId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);

            // 
            // cellItemUnselectedActualConsItemId
            // 
            this.cellItemUnselectedActualConsItemId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellItemUnselectedActualConsItemId.DesignName = "cellItemUnselectedActualConsItemId";
            this.cellItemUnselectedActualConsItemId.Location = new System.Drawing.Point(0, 0);
            this.cellItemUnselectedActualConsItemId.Visible = false;
            this.cellItemUnselectedActualConsItemId.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellItemUnselectedConsItemStatusSymbol
            // 
            this.cellItemUnselectedConsItemStatusSymbol.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellItemUnselectedConsItemStatusSymbol.AutoTransparent = true;
            this.cellItemUnselectedConsItemStatusSymbol.CellSource.ColumnIndex = 1;
            this.cellItemUnselectedConsItemStatusSymbol.DesignName = "cellItemUnselectedConsItemStatusSymbol";
            this.cellItemUnselectedConsItemStatusSymbol.ImageList = this.imageListStatus;
            this.cellItemUnselectedConsItemStatusSymbol.Location = new System.Drawing.Point(320, 0);
            this.cellItemUnselectedConsItemStatusSymbol.Size = new System.Drawing.Size(-1, 30);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 5);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 30);
            this.labelModuleName.Text = "Header";
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            //
            // lblOrgUnitName
            // 
            this.lblOrgUnitName.AutoSize = false;
            this.lblOrgUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgUnitName.Location = new System.Drawing.Point(3, 36);
            this.lblOrgUnitName.Name = "lblOrgUnitName";
            this.lblOrgUnitName.Size = new System.Drawing.Size(477, 30);
            this.lblOrgUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            //
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblHeading.Location = new System.Drawing.Point(21, 71);
            this.lblHeading.Name = "lblOrgUnitName";
            this.lblHeading.Size = new System.Drawing.Size(438, 30);
            //this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // FormReconcillation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Name = "FormReconciliation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        public Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgUnitName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        public Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        public Resco.Controls.AdvancedList.AdvancedList ListConsignments;
        private Resco.Controls.AdvancedList.RowTemplate templateHeader;
        private Resco.Controls.AdvancedList.TextCell cellHeaderBottomBorder;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol0;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol1;
        private Resco.Controls.AdvancedList.ImageCell cellSelectedConsignmentStatusSymbol;
        public Resco.Controls.AdvancedList.TextCell cellSelectedConsignmentId;
        public Resco.Controls.AdvancedList.TextCell cellSelectedActualConsignmentId;
        private Resco.Controls.AdvancedList.TextCell cellSelectedConsignmentStatusCount;
        private Resco.Controls.AdvancedList.ImageCell cellUnselectedConsignmentStatusSymbol;
        public Resco.Controls.AdvancedList.TextCell cellUnselectedConsignmentId;
        public Resco.Controls.AdvancedList.TextCell cellUnselectedActualConsignmentId;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedConsignmentStatusCount;
        public Resco.Controls.AdvancedList.TextCell cellHeaderCol2;
        private System.Windows.Forms.ImageList imageListStatus;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselectedConsItems;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelectedConsItems;
        private Resco.Controls.AdvancedList.TextCell cellItemUnselectedConsItemId;
        private Resco.Controls.AdvancedList.TextCell cellItemUnselectedActualConsItemId;
        private Resco.Controls.AdvancedList.TextCell cellItemSelectedConsItemId;
        private Resco.Controls.AdvancedList.TextCell cellItemSelectedActualConsItemId;
        private Resco.Controls.AdvancedList.ImageCell cellItemUnselectedConsItemStatusSymbol;
        private Resco.Controls.AdvancedList.ImageCell cellItemSelectedConsItemStatusSymbol;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelected;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselected;
    }
}
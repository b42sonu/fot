﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Properties;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation
{
    public partial class FormReconciliation : BaseConsignmentEntityDetailForm
    {
        public bool IsShowingConsignments { get; set; }

        private FormDataReconcillitaion _formDataReconcillitaion;
        private string _headerText;
        public Consignment CurrentConsignment;
        public ConsignmentItem CurrentConsignmentItem;
        private List<Consignment> _consignmentsSortedByDeviations;
        public MultiButtonControl MultiButtonControl;
        public const string ButtonBack = "ButtonBack";
        public const string ButtonItems = "ButtonItems";
        public const string ButtonOk = "ButtonOk";
        public const string ButtonDetail = "ButtonDetail";
        public const string ButtonDeviation = "ButtonDeviation";
        public const string ButtonPrint = "ButtonPrint";
        public const string ButtonDelete = "ButtonDelete";
        private Process _currentProcess;
        private readonly DetailsPanel _detailsPanel;
        private bool _enablePrintButton;
        private readonly CounterLabels _counterLabels;
        private AdvancedList _itemAdvanceList;
        private bool _isComingFromItemsBack;

        public FormReconciliation()
        {
            Logger.LogEvent(Severity.Debug, "Executing Reconciliation.FormReconciliation()");

            InitializeComponent();
            MultiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(MultiButtonControl);
            _detailsPanel = new DetailsPanel
                {
                    Location = new Point(21, 90),
                    Size = new Size(438, 295)
                };

            touchPanel.Controls.Add(_detailsPanel);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            _detailsPanel.Visible = false;
            imageListStatus.Images.Add(Resources.Ok);
            imageListStatus.Images.Add(Resources.Plus);
            imageListStatus.Images.Add(Resources.Minus);

            AddButton();
            lblHeading.Visible = false;

            SetStandardControlProperties(labelModuleName, lblOrgUnitName, null, null, null, null, null);
            _counterLabels = new CounterLabels(touchPanel);
            ItemGrid();
            Logger.LogEvent(Severity.Debug, "Executed FormReconciliation()");
        }

        private void AddButton()
        {
            try
            {
                lblOrgUnitName.Text = ModelUser.UserProfile.OrgUnitName;
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Deviation, ButtonDeviationClick, ButtonDeviation));
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Details, ButtonDetailClick, ButtonDetail));
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, ButtonDelete) );
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Items, ButtonItemsClick, ButtonItems));
                MultiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Print, ButtonPrintClick, ButtonPrint) { Enabled = false });
                MultiButtonControl.GenerateButtons();
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.AddButton");
            }
        }


        public override void OnShow(IFormData formData)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing OnShow()");
                if (formData != null)
                {
                    //_detailsPanel.Controls.Clear();
                    MultiButtonControl.BeginUpdate();
                    ListConsignments.BringToFront();
                    _detailsPanel.Location = new Point(21, 90);
                    _detailsPanel.Size = new Size(438, 295);
                    _formDataReconcillitaion = (FormDataReconcillitaion)formData;
                    lblOrgUnitName.Text = _formDataReconcillitaion.OrgUnitName;
                    _headerText = formData.HeaderText;
                    if (_formDataReconcillitaion.IsOpeningFirstTime)
                        IsShowingConsignments = true;

                    //If opening first time in current flow then by default open consignment list
                    if (_currentProcess == Process.Unknown)
                        _currentProcess = BaseModule.CurrentFlow.CurrentProcess;
                    else
                    {
                        if (_currentProcess != BaseModule.CurrentFlow.CurrentProcess)
                        {
                            IsShowingConsignments = true;
                            _currentProcess = BaseModule.CurrentFlow.CurrentProcess;
                        }
                    }

                    _isComingFromItemsBack = false;
                    if (IsShowingConsignments || _formDataReconcillitaion.ReconciledEntityMap.ScannedConsignmentAndItemsTotalCount == 0)//when last item deletes user should be shown only one empty reconcilliation
                        ShowConsignments(true);
                    else
                        ShowConsignmentItems();
                    HideUnhidePrinterButton();
                    MultiButtonControl.SetButtonEnabledState(ButtonOk, _formDataReconcillitaion.OkButtonVisible);
                    MultiButtonControl.EndUpdate();
                    SetCurrentConsignmentEntity(0);
                    ShowCounterValues(_formDataReconcillitaion);
                    Logger.LogEvent(Severity.Debug, "Executed OnShow()");
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.OnShow");
            }
        }
        private void HideUnhidePrinterButton()
        {
            _enablePrintButton = false;
            switch (BaseModule.CurrentFlow.CurrentProcess)
            {
                case Process.BuildLoadCarrier:
                case Process.LoadDistribTruck:
                case Process.LoadLineHaul:
                    if (_formDataReconcillitaion.WorklistItem != null && string.IsNullOrEmpty(_formDataReconcillitaion.WorklistItem.LoadCarrierId) == false)
                    {
                        if (_formDataReconcillitaion.OriginalEntityMap.Count >= 1)
                            _enablePrintButton = true;
                        MultiButtonControl.SetButtonEnabledState(ButtonPrint, _enablePrintButton);
                    }
                    break;

                case Process.IntoHoldingAtHub:
                case Process.UnloadLineHaul:
                case Process.UnloadPickUpTruck:
                case Process.RemainingGoodsAtHub:
                    if (_formDataReconcillitaion.OriginalEntityMap.Count >= 1)
                        _enablePrintButton = true;
                    MultiButtonControl.SetButtonEnabledState(ButtonPrint, _enablePrintButton);
                    break;

                default:
                    MultiButtonControl.SetButtonEnabledState(ButtonPrint, false);
                    break;
            }

        }

        private void ItemGrid()
        {
            _itemAdvanceList = new AdvancedList {Size = new Size(438, 170), ScrollbarWidth = 26, ShowHeader = true};
            //Data Row
            var template = new RowTemplate { Height = 30 };
            var cellText = new TextCell
                               {
                                   Size = new Size(240, 30),
                                   TextFont =
                                       new Font("Arial", 9F,
                                                FontStyle.Regular),
                                   Location = new Point(25, 0),
                                   CellSource = {ColumnIndex = 0}
                               };
            //Prepare Header
            var cellSymbol = new ImageCell
                                 {
                                     Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter,
                                     AutoTransparent = true,
                                     CellSource = {ColumnIndex = 1},
                                     ImageList = imageListStatus,
                                     Location = new Point(320, 0),
                                     Size = new Size(-1, 30)
                                 };

           
            _itemAdvanceList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

            template.CellTemplates.Add(cellSymbol);
            template.CellTemplates.Add(cellText);

           //Prepare Header Template
            var headerTemplte = new RowTemplate
                                    {BackColor = SystemColors.ControlDark,
                                    Height = 33};
            var cellHeader = new TextCell
                                 {
                                     CellSource = {ConstantData = GlobalTexts.ConsignmentItem},
                                     Size = new Size(438, 30),
                                     Location = new Point(25, 0)
                                 };
            var cell2Header = new TextCell()
                                  {
                                    CellSource  = {ConstantData = GlobalTexts.Status},
                                    Location = new Point(320, 0),
                                    Size = new Size(150, 30),
                                    TextFont = new Font("Arial", 9F, FontStyle.Regular)
                                  };

            headerTemplte.CellTemplates.Add(cellHeader);
            headerTemplte.CellTemplates.Add(cell2Header);
            headerTemplte.Height = 33;


            _itemAdvanceList.SelectedTemplateIndex = 1;
            _itemAdvanceList.Templates.Add(headerTemplte);
            _itemAdvanceList.Templates.Add(template);
            _itemAdvanceList.BackColor = Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            touchPanel.Controls.Add(_itemAdvanceList);
            _itemAdvanceList.Visible = false;
        }

        public override void OnUpdateView(IFormData formData)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing OnUpdateView()");

                var formDataReconcillitaion = (FormDataReconcillitaion)formData;
                lblOrgUnitName.Text = _formDataReconcillitaion.OrgUnitName;
                var entity = formDataReconcillitaion.CurrentConsignmentEntity as Consignment;
                if (entity != null)
                    CurrentConsignment = entity;
                else
                    CurrentConsignmentItem = (ConsignmentItem)formDataReconcillitaion.CurrentConsignmentEntity;
                ShowDetailData();

                ShowCounterValues(formDataReconcillitaion);

                Logger.LogEvent(Severity.Debug, "Executed OnUpdateView()");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.OnUpdateView");
            }
        }

        private void ShowCounterValues(FormDataReconcillitaion formDataReconcillitaion)
        {
            var reconciledEntityMap = formDataReconcillitaion.ReconciledEntityMap;
            if (reconciledEntityMap == null)
                return;
            _counterLabels.SetValues(reconciledEntityMap);
        }

        public override void ShowingView()
        {
            base.ShowingView();
            ListConsignments.Focus();
        }

        public virtual void ShowConsignments(bool selectFirstItem)
        {
            try
            {
                _detailsPanel.Visible = false;
                IsShowingConsignments = true;
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ShowConsignments()");
                SetButtonsForConsignment();
                labelModuleName.Text = _headerText; //+Environment.NewLine + GlobalTexts.GoodsHandled;
                lblHeading.Text = string.Empty;
                var colTextValues = new string[4];

                ListConsignments.BringToFront();
                _detailsPanel.Location = new Point(21, 90);
                _detailsPanel.Size = new Size(438, 295);

                ListConsignments.BeginUpdate();
                ListConsignments.DataRows.Clear();
                cellHeaderCol0.CellSource.ConstantData = "";
                cellHeaderCol1.CellSource.ConstantData = GlobalTexts.Consignments;
                SetHeaderAccordingToProcess(BaseModule.CurrentFlow.CurrentProcess);
                _consignmentsSortedByDeviations = ActionCommandsReconciliation.ConsignmentSortedByDeviationStatus(_formDataReconcillitaion.ReconciledEntityMap);
                foreach (var consignment in _consignmentsSortedByDeviations)
                {
                    string countSummary;
                    colTextValues[0] = ActionCommandsReconciliation.DeviationStatus(consignment, out countSummary);
                    colTextValues[1] = consignment.EntityId;//ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ? consignment.EntityId : StringUtil.GetTextForConsignmentEntity(consignment.EntityDisplayId,false);
                    colTextValues[2] = countSummary;
                    colTextValues[3] = consignment.EntityId;

                    var insertRow = new Row(2, 1, colTextValues);
                    ListConsignments.DataRows.Add(insertRow);
                }

                if (_consignmentsSortedByDeviations.Count > 0)
                {
                    if (CurrentConsignment != null)
                        SelectCurrentConsignmentEntity(CurrentConsignment.ConsignmentId, true);
                    else if (selectFirstItem)
                        ListConsignments.ActiveRowIndex = 0;
                }
                else if (_consignmentsSortedByDeviations.Count == 0)
                    MultiButtonControl.SetButtonEnabledState(ButtonOk, !_formDataReconcillitaion.OkButtonDisableWhenNoItemsInList && IsOkButtonVisible());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.ShowConsignments");
            }

            ListConsignments.EndUpdate();
        }
        // After binding we try to activate the previous selected item
        private void SelectCurrentConsignmentEntity(string currentCosignmentItemId, bool isConsignement)
        {

            int columnIndex = isConsignement ? 1 : 0;

            for (int i = 0; i < ListConsignments.DataRows.Count; i++)
            {
                var consignmentItemId = (string)ListConsignments.DataRows[i][columnIndex];
                if (currentCosignmentItemId == consignmentItemId)
                {
                    ListConsignments.ActiveRowIndex = i;
                    return;
                }
            }
            ListConsignments.ActiveRowIndex = 0;
        }
        private bool IsOkButtonVisible()
        {
            if (_formDataReconcillitaion != null)
                return _formDataReconcillitaion.OkButtonVisible;
            return true;
        }

        private void SetHeaderAccordingToProcess(Process currentProcess)
        {
            switch (currentProcess)
            {
                case Process.InTerminal:
                    cellHeaderCol2.CellSource.ConstantData = GlobalTexts.Reg;
                    break;
                case Process.OutOfHoldingAtHub:
                    cellHeaderCol2.CellSource.ConstantData = GlobalTexts.Sedi;
                    break;
                default:
                    cellHeaderCol2.CellSource.ConstantData = GlobalTexts.ReconcilliationListHeading;
                    break;
            }

        }

        public virtual void ShowConsignmentItems()
        {
            try
            {

                _detailsPanel.Visible = false;
                IsShowingConsignments = false;
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ShowConsignmentItems()");

                SetButtonsForConsignmentItems();
                ListConsignments.BringToFront();

                var consignmentItemHistoryMap = ActionCommandsReconciliation.GetConsignmentItemListSortedByDeviations(_formDataReconcillitaion.ReconciledEntityMap, CurrentConsignment.ConsignmentId);

                //SetConsignmentHeader(CurrentConsignment, BaseModule.CurrentFlow.CurrentProcess);

                var colTextValues = new string[3];

                ListConsignments.BeginUpdate();
                ListConsignments.DataRows.Clear();
                cellHeaderCol0.CellSource.ConstantData = "";
                cellHeaderCol1.CellSource.ConstantData = GlobalTexts.ConsignmentItems;
                SetHeaderForStautsAccordingToProcess(BaseModule.CurrentFlow.CurrentProcess);

                if (consignmentItemHistoryMap != null && consignmentItemHistoryMap.Any())
                {
                    foreach (var consignmentItem in consignmentItemHistoryMap)
                    {
                        if (consignmentItem != null)
                        {
                            colTextValues[0] = consignmentItem.ItemId;//ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ? consignmentItem.ItemId : StringUtil.GetTextForConsignmentEntity(consignmentItem.ItemDisplayId, false);//FOT 116
                            var status = ActionCommandsReconciliation.DeviationStatusForConsignmentItem(consignmentItem);
                            colTextValues[1] = status.ToString(CultureInfo.InvariantCulture);
                            colTextValues[2] = consignmentItem.ItemId;

                            var insertRow = new Row(4, 3, colTextValues);
                            ListConsignments.DataRows.Add(insertRow);
                        }
                    }

                    if (consignmentItemHistoryMap.Count > 0)
                    {
                        if (CurrentConsignmentItem != null)
                            SelectCurrentConsignmentEntity(CurrentConsignmentItem.ItemId, false);
                        else
                            ListConsignments.ActiveRowIndex = 0;
                    }

                    else
                        MultiButtonControl.SetButtonEnabledState(ButtonOk, !_formDataReconcillitaion.OkButtonDisableWhenNoItemsInList && IsOkButtonVisible());
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.ShowConsignmentItems");
            }


            ListConsignments.EndUpdate();
        }

        private void SetHeaderForStautsAccordingToProcess(Process currentProcess)
        {
            switch (currentProcess)
            {
                case Process.OutOfHoldingAtHub:
                case Process.InTerminal:
                    cellHeaderCol2.CellSource.ConstantData = string.Empty;
                    break;
                default:
                    cellHeaderCol2.CellSource.ConstantData = GlobalTexts.Status;
                    break;
            }
        }

        //private void SetConsignmentHeader(Consignment consignment, Process currentProcess)
        //{
        //    try
        //    {
        //        Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.SetConsignmentHeader()");
        //        int scannedConsignmentItemCount = consignment.ItemCount(EntityStatus.Scanned, false);
        //        int plannedConsignmentItemCount = consignment.ItemCount(EntityStatus.Planned, false);

        //        switch (currentProcess)
        //        {
        //            case Process.InTerminal:
        //                labelModuleName.Text = String.Format("{0}", GlobalTexts.InTerminal); //+ Environment.NewLine,ModelUser.UserProfile.OrgUnitName);
        //                break;

        //            default:
        //                lblHeading.Text = String.Format("{0} {1} ({2}/{3})", GlobalTexts.Consignment + Colon,
        //                                 consignment.ConsignmentId, scannedConsignmentItemCount, plannedConsignmentItemCount);
        //                break;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LogException(ex, "FormReconciliation.SetConsignmentHeader");
        //    }
        //}


        public virtual void ShowDetailData()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing Shared FormReconciliation.ShowDetailData()");

                if (_detailsPanel != null) _detailsPanel.Visible = true;
                MultiButtonControl.BeginUpdate();
                if (IsShowingConsignments)
                {
                    if (CurrentConsignment is LoadCarrier)
                    {
                        SetButtonsForLoadCarriers();
                        var castToLoadCarrier = CurrentConsignment.CastToLoadCarrier();
                        var prepareInfoList = ItemDetailsUtil.PrepareInfoList(castToLoadCarrier);
                        if (_detailsPanel != null) _detailsPanel.AddFormattedSections(prepareInfoList);
                    }
                    else
                    {
                        SetButtonsForConsignmentDetails();
                        if (CurrentConsignment != null && _formDataReconcillitaion != null)
                        {
                            var prepareInfoList = ItemDetailsUtil.PrepareInfoList(CurrentConsignment.PlannedConsignment, _formDataReconcillitaion.StopInformation);
                            //Bind Items

                            /*_itemAdvanceList.Visible = true;
                            _itemAdvanceList.Location = new Point(21, 90);

                            _itemAdvanceList.BringToFront();
                            _itemAdvanceList.DataRows.Clear();
                            if (CurrentConsignment.PlannedConsignment != null && CurrentConsignment.PlannedConsignment.ConsignmentItem != null)
                            {
                                
                                foreach (var info in CurrentConsignment.PlannedConsignment.ConsignmentItem)
                                {
                                    bool itemExist = false;
                                   var item = _formDataReconcillitaion.ReconciledEntityMap.GetConsignmentItem(CurrentConsignment.ConsignmentId,
                                        info.ConsignmentItemNumber);
                                   if (item != null)
                                       itemExist = true;
                                    var colTextValues = new string[2];
                                    colTextValues[0] = info.ConsignmentItemNumber;
                                    colTextValues[1] = itemExist ? Convert.ToString(Convert.ToInt32(EntityMap.DeviationStatus.AcceptedStatus)) : Convert.ToString(Convert.ToInt32(EntityMap.DeviationStatus.MissingStatus));
                                    var insertRow = new Row(1, 1, colTextValues);
                                    _itemAdvanceList.DataRows.Add(insertRow);
                                }
                           
                            }

                            if (CurrentConsignment.ConsignmentId == "NPR")
                            {
                                foreach (var item in CurrentConsignment.ConsignmentItemsMap)
                                {
                                    var colTextValues = new string[2];
                                    colTextValues[0] = item.Value.ItemId;
                                    colTextValues[1] = Convert.ToString(Convert.ToInt32(EntityMap.DeviationStatus.AcceptedStatus));
                                    var insertRow = new Row(1, 1, colTextValues);
                                    _itemAdvanceList.DataRows.Add(insertRow);
                                }
                               
                            }*/
                                //listItems.DataSource = CurrentConsignment.PlannedConsignment.ConsignmentItem.ToList();

                            if (_detailsPanel != null)
                            {
                                //_detailsPanel.Location = new Point(21, 260);
                                //_detailsPanel.Size = new Size(438, 180);


                                if (_detailsPanel != null) _detailsPanel.AddFormattedSections(prepareInfoList);
                            }
                        }
                    }
                }
                else
                {
                    _itemAdvanceList.Visible = false;
                    if (_detailsPanel != null)
                    {
                        _detailsPanel.Location = new Point(21, 90);
                        _detailsPanel.Size = new Size(438, 295);
                    }
                    SetButtonsForConsignmentItemDetails();
                    var prepareInfoList = ItemDetailsUtil.PrepareInfoList(CurrentConsignmentItem);
                    if (_detailsPanel != null) _detailsPanel.AddSections(prepareInfoList);
                }
                MultiButtonControl.EndUpdate();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.ShowDetailData");
            }
        }



        private ConsignmentEntity GetCurrentConsignmentEntity()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.GetCurrentConsignmentEntity()");
                if (CurrentConsignmentItem == null)
                    return CurrentConsignment;

                return CurrentConsignmentItem;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.GetCurrentConsignmentEntity");
            }
            return null;
        }

        public virtual void SetCurrentConsignmentEntity(int rowIndex)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing Reconciliation.FormReconciliation.SetCurrentConsignmentEntity()");

                if (rowIndex >= ListConsignments.DataRows.Count)
                    return;

                if (IsShowingConsignments)
                {
                    var consignmentId = (string)ListConsignments.DataRows[rowIndex][3];
                    CurrentConsignment = _formDataReconcillitaion.ReconciledEntityMap.GetConsignment(consignmentId);
                    CurrentConsignmentItem = null;
                    //Disable deviation button in case when the status of selected consignment in active.
                    if (Convert.ToInt16(ListConsignments.DataRows[rowIndex][0]) == 2)
                    {
                        MultiButtonControl.SetButtonEnabledState(ButtonDeviation, false);
                    }
                }
                else
                {
                    var consignmentItemId = (string)ListConsignments.DataRows[rowIndex][2];
                    CurrentConsignmentItem = CurrentConsignment.GetConsignmentItem(consignmentItemId);
                    //Disable deviation button in case when the status of selected consignment item in active.
                    if (Convert.ToInt16(ListConsignments.DataRows[rowIndex][1]) == 2)
                    {
                        MultiButtonControl.SetButtonEnabledState(ButtonDeviation, false);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.SetCurrentConsignmentEntity");
            }
        }

        /// <summary>
        /// maintain scroll postion when back from consignment item to consignment list...
        /// </summary>
        private void SetScrollPosition()
        {
            if (ListConsignments.SelectedRows != null && ListConsignments.SelectedRows.Any())
            {
                var row = ListConsignments.SelectedRows.Count() == 2
                              ? ListConsignments.SelectedRows[1]
                              : ListConsignments.SelectedRows[0];


                if (row.Index > 7)  // we are using 7 beacuse we are showing 8 stop visible in one show if user want to see 5 then user need to use scroll bar for this...
                    ListConsignments.ScrollPos = row.Index * row.Height;
            }
        }

        #region Events
        public virtual void OnActiveRowChanged(object sender, EventArgs e)
        {
            try
            {

                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.OnActiveRowChanged()");
                MultiButtonControl.BeginUpdate();
                bool enable = ListConsignments.ActiveRowIndex != -1;

                if (enable)
                    SetCurrentConsignmentEntity(ListConsignments.ActiveRowIndex);


                if (IsShowingConsignments)
                {
                    MultiButtonControl.SetButtonEnabledState(ButtonDetail, enable);
                    MultiButtonControl.SetButtonEnabledState(ButtonItems, enable && !(CurrentConsignment is LoadCarrier));
                    var deviationStatus = ActionCommandsReconciliation.DeviationStatus(CurrentConsignment);
                    MultiButtonControl.SetButtonEnabledState(ButtonDeviation, deviationStatus != (int)EntityMap.DeviationStatus.MissingStatus && !(CurrentConsignment is LoadCarrier));
                    MultiButtonControl.SetButtonEnabledState(ButtonDelete, IsConsignmentEntityScannedByUser());
                }
                else
                {
                    MultiButtonControl.SetButtonEnabledState(ButtonDetail, enable);
                    var deviationStatus = ActionCommandsReconciliation.DeviationStatusForConsignmentItem(CurrentConsignmentItem);
                    MultiButtonControl.SetButtonEnabledState(ButtonDeviation, deviationStatus != (int)EntityMap.DeviationStatus.MissingStatus);
                    MultiButtonControl.SetButtonEnabledState(ButtonDelete, IsConsignmentEntityScannedByUser());
                }
                MultiButtonControl.EndUpdate();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.OnActiveRowChanged");
            }
        }

        private bool IsConsignmentEntityScannedByUser()
        {
            bool isScannedByUser = false;

            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.IsConsignmentEntityScannedByUser()");
                var currentConsignmentEntity = GetCurrentConsignmentEntity();
                isScannedByUser = _formDataReconcillitaion.OriginalEntityMap.GetScannedBefore(ref currentConsignmentEntity);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.IsConsignmentEntityScannedByUser");
            }
            return isScannedByUser;
        }

        public void ButtonDeviationClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonDeviationClick()");

                ViewEvent.Invoke(ViewEventsReconcilliation.Deviation, GetCurrentConsignmentEntity(), _detailsPanel.Visible);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonDeviationClick");
            }
        }

        public void ButtonItemsClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonItemsClick()");
                //ShowConsignmentItems();
                IsShowingConsignments = false;
                ViewEvent.Invoke(ViewEventsReconcilliation.Items, GetCurrentConsignmentEntity());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonItemsClick");
            }
        }

        public void ButtonDeleteClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonDeleteClick()");

                string deleteMessage = String.Format(GlobalTexts.ConfirmDelWantToDelete, GetCurrentConsignmentEntity().EntityDisplayId);
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonDeleteClick();" + deleteMessage);

                var modalMessageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Delete, deleteMessage, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);

                bool shouldDelete = modalMessageBoxResult == GlobalTexts.Yes;
                if (shouldDelete)
                    ViewEvent.Invoke(ViewEventsReconcilliation.Delete, GetCurrentConsignmentEntity());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonDeleteClick");
            }
        }

        public void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonBackClick()");

                Boolean detailViewIsShowing = _detailsPanel.Visible;


                if (detailViewIsShowing)
                {
                    _isComingFromItemsBack = false;
                    if (IsShowingConsignments)
                        ShowConsignments(false);
                    else
                        ShowConsignmentItems();
                }
                else if (IsShowingConsignments == false)
                {
                    // Go back from details 
                    _isComingFromItemsBack = true;
                    ShowConsignments(false);
                    SetScrollPosition();
                }
                else
                {
                    // Go back to previous view
                    _isComingFromItemsBack = false;
                    ViewEvent.Invoke(ViewEventsReconcilliation.Cancel, null);
                    CurrentConsignment = null;
                    CurrentConsignmentItem = null;
                }
                //if (ListConsignments.DataRows.Count > 0)
                //    ListConsignments.SelectedRow = ListConsignments.DataRows[0];
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonBackClick");
            }
        }

        public void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonOkClick()");

                //Reset the view to show Consignment list, so that the next process will not show wrong view
                //_viewStateWhenGoingToDeviationMenu = ViewStateValues.ConsignmentList;
                ViewEvent.Invoke(ViewEventsReconcilliation.Ok, _consignmentsSortedByDeviations);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonOkClick");
            }
        }

        public void ButtonDetailClick(object sender, EventArgs e)
        {
            try
            {
                // var deleteEnabled = MultiButtonControl.IsButtonEnabled(ButtonDelete);
                //var deviationEnabled = MultiButtonControl.IsButtonEnabled(ButtonDeviation);
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonDetailClick()");
                ViewEvent.Invoke(ViewEventsReconcilliation.Detail, GetCurrentConsignmentEntity());
                MultiButtonControl.BeginUpdate();
                MultiButtonControl.SetButtonEnabledState(ButtonDelete, false);
                MultiButtonControl.SetButtonEnabledState(ButtonDeviation, false);
                MultiButtonControl.SetButtonEnabledState(ButtonDetail, false);
                MultiButtonControl.SetButtonEnabledState(ButtonOk, false);
                //MultiButtonControl.SetButtonEnabledState(ButtonItems, false);
                MultiButtonControl.SetButtonEnabledState(ButtonPrint, false);

                MultiButtonControl.EndUpdate();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonDetailClick");
            }
        }

        public void ButtonPrintClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.ButtonDetailClick()");
                ViewEvent.Invoke(ViewEventsReconcilliation.Print, _consignmentsSortedByDeviations);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.ButtonDetailClick");
            }
        }

        #endregion

        #region Enable button methods

        private void HideAndDisableAllButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.HideAndDisableAllButtons()");
                MultiButtonControl.SetButtonEnabledState(ButtonBack, true);
                MultiButtonControl.SetButtonEnabledState(ButtonOk, IsOkButtonVisible());
                MultiButtonControl.SetButtonVisibleState(ButtonDetail, false);
                MultiButtonControl.SetButtonVisibleState(ButtonItems, false);
                MultiButtonControl.SetButtonVisibleState(ButtonDeviation, false);
                MultiButtonControl.SetButtonVisibleState(ButtonDelete, false);
                MultiButtonControl.SetButtonVisibleState(ButtonPrint, false);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.HideAndDisableAllButtons");
            }
        }

        private void SetButtonsForConsignment()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.SetButtonsForConsignment()");
                //  HideAndDisableAllButtonsForConsignment();
                //if(MultiButtonControl._buttonPrevious.te)
                if (!_isComingFromItemsBack)
                {
                    MultiButtonControl.SetButtonEnabledState(ButtonPrint, _enablePrintButton);
                    HideAndDisableAllButtons();
                    MultiButtonControl.SetButtonEnabledState(ButtonBack, _formDataReconcillitaion.CancelButtonVisible);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.SetButtonsForConsignment");
            }
        }

        private void SetButtonsForConsignmentItems()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.SetButtonsForConsignmentItems()");
                //MultiButtonControl.BeginUpdate();
                HideAndDisableAllButtons();
                //MultiButtonControl.EndUpdate();
                MultiButtonControl.SetButtonEnabledState(ButtonPrint, _enablePrintButton);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.SetButtonsForConsignmentItems");
            }
        }

        public void SetButtonsForLoadCarriers()
        {
            HideAndDisableAllButtons();
            MultiButtonControl.SetButtonEnabledState(ButtonItems, false);
            MultiButtonControl.SetButtonEnabledState(ButtonDeviation, false);
        }

        private void SetButtonsForConsignmentDetails()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.SetButtonsForConsignmentDetails()");
                HideAndDisableAllButtons();
                MultiButtonControl.SetButtonEnabledState(ButtonItems, true);
                MultiButtonControl.SetButtonEnabledState(ButtonDeviation, true);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.SetButtonsForConsignmentDetails");
            }
        }

        private void SetButtonsForConsignmentItemDetails()
        {
            try
            {

                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.SetButtonsForConsignmentItemDetails()");
                HideAndDisableAllButtons();
                MultiButtonControl.SetButtonEnabledState(ButtonDetail, false);
                MultiButtonControl.SetButtonEnabledState(ButtonDeviation, true);
                MultiButtonControl.SetButtonEnabledState(ButtonDelete, true);
                MultiButtonControl.SetButtonEnabledState(ButtonItems, false);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcillation.SetButtonsForConsignmentItemDetails");
            }
        }
        #endregion


    }



    public class ViewEventsReconcilliation : ViewEvent
    {
        public const int Ok = 0;
        public const int Cancel = 1;
        public const int Deviation = 2;
        public const int Detail = 3;
        public const int Delete = 4;
        public const int Print = 5;
        public const int Sign = 6;
        public const int Items = 7;
    }

    public class FormDataReconcillitaion : BaseFormData
    {
        //This is the original map + combined with data from GetGoodsList (what has been scanned before on this trip/stop)
        public EntityMap ReconciledEntityMap { get; set; }
        //Use original map to check if a scan is done by the user
        public EntityMap OriginalEntityMap { get; set; }

        public bool CancelButtonVisible { get; set; }
        public StopType StopInformation { get; set; }
        public ConsignmentEntity CurrentConsignmentEntity { get; set; }
        public bool OkButtonVisible { get; set; }
        public bool OkButtonDisableWhenNoItemsInList { get; set; }  //this property created when no consignment/item  in list .Then any flow is set false only that case ok button disabled other wise it remain enabled..
        public WorkListItem WorklistItem { get; set; }
        public bool IsOpeningFirstTime { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using PreCom.MeasurementCore;
using Consignment = Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap.Consignment;
using LoggingInformationType = Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading.LoggingInformationType;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation
{
    public class ActionCommandsReconciliation : BaseActionCommands
    {
        private readonly ICommunicationClient _communicationClient;

        public ActionCommandsReconciliation(ICommunicationClient communicationClient)
        {
            _communicationClient = communicationClient;
        }


        //public void BackFromCorrectionMenu(FlowResultCorrectionMenu subflowResult, ref EntityMap entityMap)
        public void BackFromCorrectionMenu(FlowResultCorrectionMenu subflowResult, ref EntityMap entityMap)//SYED REPLACED ABOVE METHOD SIGNATURE BY THIS TO FIX DEFECT 116
        {
            try
            {
                if (subflowResult.EntityMap != null)
                {
                    entityMap = subflowResult.EntityMap.Clone();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.BackFromCorrectionMenu");
            }
        }

        public bool ShouldGetGoodsList(OperationProcess operationProcess, FlowDataReconciliation flowData)
        {
            bool result = false;
            try
            {
                if (flowData.WorklistItem != null && (flowData.WorklistItem.Type == WorkListItemType.RouteCarrierTrip || flowData.WorklistItem.Type == WorkListItemType.Route))
                {
                    result = flowData.GoodsCategoryCode != string.Empty;
                }
                if (result)
                    Logger.LogEvent(Severity.Debug, "Using StopId {0} and TripId for getting goodslist", operationProcess.StopId, operationProcess.TripId);
                else
                    Logger.LogEvent(Severity.Debug, "Does not retrieve goodslist from LM for process {0}", BaseModule.CurrentFlow.CurrentProcess);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.ShouldGetGoodsList");
            }
            return result;
        }

        //TODO :: Write Unit Test
        private static T20240_GetGoodsListFromFOTRequest CreateGoodsListRequestForStop(FlowDataReconciliation flowData, OperationProcess operationProcess, UserProfile userProfile)
        {
            T20240_GetGoodsListFromFOTRequest request = null;
            try
            {
                request = new T20240_GetGoodsListFromFOTRequest
                              {
                                  TMSAffiliationId = userProfile.TmsAffiliation,
                                  LocationInformation = new GetGoodsListRequestLocationInformation
                                                            {
                                                                ActionType = flowData.GoodsCategoryCode,
                                                                LoadCarrierId = operationProcess.LogicalLoadCarrierId,
                                                                NumberOfHours = 12,
                                                                OrgUnitId = userProfile.OrgUnitId,
                                                                Route = operationProcess.RouteId,
                                                                TripId = operationProcess.TripId,
                                                                PowerUnitId = operationProcess.PowerUnitId,
                                                                CountryCode = userProfile.CountryCode,
                                                                PostalCode = userProfile.PostalCode,
                                                                ExternalTripId = operationProcess.ExternalTripId,
                                                                EventCode = EventCodeUtil.GetEventCodeFromProcess(),
                                                            },
                                  LoggingInformation =
                                      LoggingInformation.GetLoggingInformation<LoggingInformationType>(
                                          "Request goods list", userProfile),

                                  PlannedOperationInformation = new GetGoodsListRequestPlannedOperationInformation
                                                                    {
                                                                        ActionType = flowData.GoodsCategoryCode,
                                                                        ExternalTripId = operationProcess.ExternalTripId,
                                                                        Route = operationProcess.RouteId,
                                                                        TripId = operationProcess.TripId,
                                                                        StopId = string.Empty
                                                                    },
                                  RequestType = flowData.WorklistItem != null &&
                                                flowData.WorklistItem.Type == WorkListItemType.Route
                                                    ? GetGoodsListRequestRequestType.LoadList
                                                    : GetGoodsListRequestRequestType.Route
                              };
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.CreateGoodsListRequestForStop");
            }
            return request;
        }





        //TODO :: Write Unit Test
        /// <summary>
        /// Retrieves the list of planned loading/unloading operations for a stop on a trip
        /// </summary>
        /// <param name="flowData"></param>
        /// <param name="flowData.actionType">LO = Unloading, LA = loading</param>
        /// <param name="flowData.stopId">What stop to retrieve info for (mandatory)</param>
        /// <param name="flowData.tripId">What trip to retrieve info for (mandatory)</param>
        /// <param name="flowData.externalTripId">Retrieved from operation list. Can be blank</param>
        /// <param name="flowData.routeId">Retrieved from operation list. Can be blank</param>
        /// <param name="flowData.powerUnitId"></param>
        /// <param name="operationProcess"></param>
        /// <param name="userProfile">Current user</param>
        /// <returns></returns>
        public T20240_GetGoodsListFromFOTReply RequestGoodsList(FlowDataReconciliation flowData, OperationProcess operationProcess, UserProfile userProfile)
        {
            T20240_GetGoodsListFromFOTReply reply = null;
            try
            {
                var request = CreateGoodsListRequestForStop(flowData, operationProcess, userProfile);

                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ValidateGoodsFromFot, "description");
                reply = _communicationClient.Query<T20240_GetGoodsListFromFOTReply>(request, transaction);

                if (reply != null && reply.ResponseInformation.returnState != "00")
                {
                    ShowReturnMessageFromGetGoodsListResponse(reply.ResponseInformation.returnState);
                    reply = null;
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.RequestGoodsList");
            }
            return reply;
        }


        public void ShowReturnMessageFromGetGoodsListResponse(string returnState)
        {
            var message = string.Empty;
            switch(returnState)
            {
                case "01":
                    message = GlobalTexts.MissingInformationInRequest;
                    break;
                case "02":
                    message = GlobalTexts.UnexpectedDatabaseError;
                    break;
                case "03":
                    message = GlobalTexts.InformationNotFound;
                    break;
            }

            if (string.IsNullOrEmpty(message) == false)
                GuiCommon.ShowModalDialog(GlobalTexts.Error, message, Severity.Error, GlobalTexts.Ok);
        }
       

        //TODO :: Write Unit Test
        /// <summary>
        /// Combine what is scanned by current user, with what has been scanned by all users for this stop and trip
        /// </summary>
        /// <param name="entityMap">What has been planned together what current user has unloaded</param>
        /// <param name="goodsList">List of all planned and unloaded</param>
        /// <param name="stopId">Used for logging</param>
        /// <returns>Data about scannings from GetGoodsList</returns>
        internal void ReconcileScannedWithGoodsList(EntityMap entityMap, T20240_GetGoodsListFromFOTReply goodsList, string stopId)
        {
            try
            {
                if (goodsList != null && goodsList.Operation != null && goodsList.Operation.consignment != null)
                {
                    // Add (un)loaded for all users
                    foreach (var goodsListConsignment in goodsList.Operation.consignment)
                    {
                        var consignment = AddConsignorInfoToPlannedConsignment(entityMap, goodsListConsignment);


                        if (goodsListConsignment.consignmentItem != null)
                        {
                            // Add consignmentEntity items if known
                            foreach (var goodslistConsignmentItem in goodsListConsignment.consignmentItem)
                            {
                                // Neither loaded nor unloaded, means just planned
                                var status = goodslistConsignmentItem.consignmentItemIsLoaded ? EntityStatus.Scanned : EntityStatus.Planned;
                                status |= goodslistConsignmentItem.consignmentItemIsUnloaded ? EntityStatus.Scanned : EntityStatus.Planned;

                                ConsignmentItem consignmentItem = entityMap.GetConsignmentItem(goodsListConsignment.consignmentNumber, goodslistConsignmentItem.consignmentItemNumber);
                                if (consignmentItem != null)
                                {
                                    // Add this status what we already have
                                    consignmentItem.EntityStatus |= status;
                                }
                                else
                                {
                                    // Add an item scanned already (possibly by another person)
                                    consignmentItem = new ConsignmentItem
                                    {
                                        ItemId = goodslistConsignmentItem.consignmentItemNumber,
                                        ConsignmentInstance = new Consignment(goodsListConsignment.consignmentNumber),
                                        EntityStatus = status
                                    };
                                    entityMap.Store(consignmentItem);
                                }
                            }
                        }
                        else
                        {
                            EntityStatus status = goodsListConsignment.consignmentIsLoaded ? EntityStatus.Scanned : EntityStatus.Planned;
                            status |= goodsListConsignment.consignmentIsUnloaded ? EntityStatus.Scanned : EntityStatus.Planned;

                            if (consignment == null)
                            {
                                consignment = new Consignment(goodsListConsignment.consignmentNumber, status, null);
                                entityMap.Store(consignment);
                            }

                            // No known consignmentEntities, set manual consignmentEntity item count
                            consignment.ManuallyEnteredConsignmentItemCount = goodsListConsignment.consignmentItemCount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.ReconcileScannedWithGoodsList");
            }
        }


        //TODO :: Write Unit Test
        private static Consignment AddConsignorInfoToPlannedConsignment(EntityMap entityMap, GetGoodsListResponseOperationConsignment goodsListConsignment)
        {
            //get current consignment from cache
            Consignment consignment = entityMap.GetConsignment(goodsListConsignment.consignmentNumber);

            if (consignment != null)
            {
                //If consignment's planned consignment field is null
                if (consignment.PlannedConsignment == null)
                {
                    //Assign new object of planned consignment, with consignee info
                    consignment.PlannedConsignment = new PlannedConsignmentsType
                                                         {
                                                             Consignee = ConvertConsignee(goodsListConsignment.consignee),
                                                             ConsignmentItemCount = goodsListConsignment.consignmentItemCount
                                                         };
                }
                else if (consignment.PlannedConsignment.Consignee == null) // if consignee info null, then fill the same
                {
                    consignment.PlannedConsignment.Consignee = ConvertConsignee(goodsListConsignment.consignee);
                }
            }
            return consignment;
        }

        // Casts from T20240_GetGoodsListFromFOTReply.Consigneetype to OpertationList.ConsigneeType
        public static ConsigneeType ConvertConsignee(ConsigneeTypeLoading consignee)
        {
            if (consignee == null) return null;

            var ret = new ConsigneeType
                          {
                              Name1 = consignee.name1,
                              Name2 = consignee.name2,
                              Address1 = consignee.address1,
                              Address2 = consignee.address2,
                              PostalCode = consignee.postalCode,
                              PostalName = consignee.postalName,
                              CountryCode = consignee.countryCode,
                              DeliveryAddress1 = consignee.deliveryAddress1,
                              DeliveryAddress2 = consignee.deliveryAddress2,
                              DeliveryPostalCode = consignee.deliveryPostalCode,
                              DeliveryPostalName = consignee.deliveryPostalName,
                              DeliveryCountryCode = consignee.deliveryCountryCode,
                              PhoneNumber = consignee.phoneNumber
                          };
            return ret;
        }


        public void ValidateGoodsAndSetInfoToConsignmentEntity(ConsignmentEntity consignmentEntity, bool isLoading, UserProfile userProfile)
        {
            try
            {
                if (consignmentEntity != null)
                {
                    var loadCarrier = consignmentEntity as LoadCarrier;
                    if (loadCarrier != null)
                    {
                        ActionCommandsScanBarcode.ValidateLoadCarrier(loadCarrier);
                    }
                    else
                    {
                        var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ValidateGoodsFromFot,
                                                          consignmentEntity.EntityId);

                        TransactionLogger.LogTiming<T20200_ValidateGoodsFromFOTReply>(transaction, new Timing(),
                            TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Received, TimingComponent.User),
                            new HttpCommunication());

                        if (_communicationClient.IsConnected())
                        {
                            var request = CreateValidateGoodsRequest(consignmentEntity, userProfile);
                            request.LoggingInformation.MessageId = transaction.Id.ToString();
                            var reply = GoodsEventHelper.ValidateGoodsFromFot(_communicationClient, request);

                            if (reply != null)
                            {
                                if (reply.Consignment != null)
                                {
                                    if (consignmentEntity is Consignment)
                                        SetValidateGoodsReplyToConsignment(consignmentEntity, reply);
                                    else
                                        SetValidateGoodsReplyToConsignmentItem(consignmentEntity, reply);
                                }
                            }
                        }

                        TransactionLogger.LogTiming<T20200_ValidateGoodsFromFOTReply>(transaction, new Timing(),
                            TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client,
                            TimingType.Sending, TimingComponent.User), new HttpCommunication());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.ValidateGoodsAndSetInfoToConsignmentEntity");
            }
        }

        private static void SetValidateGoodsReplyToConsignmentItem(ConsignmentEntity consignmentEntity, T20200_ValidateGoodsFromFOTReply reply)
        {
            try
            {
                if (reply.Consignment.ConsignmentItem != null && reply.Consignment.ConsignmentItem.Length > 0)
                {
                    foreach (var replyConsignmentItem in reply.Consignment.ConsignmentItem)
                    {
                        bool isCorrectConsignmentItem = replyConsignmentItem != null && replyConsignmentItem.ConsignmentItemNumber != null &&
                            replyConsignmentItem.ConsignmentItemNumber.Equals(consignmentEntity.EntityId);
                        if (isCorrectConsignmentItem)
                        {
                            var consignmentItem = (ConsignmentItem)consignmentEntity;

                            if (consignmentItem.PlannedConsignmentItem == null)
                            {
                                consignmentItem.PlannedConsignmentItem = new ConsignmentItemType();
                            }
                            consignmentItem.PlannedConsignmentItem.SetDataFromValidateGoodsResponse(replyConsignmentItem);
                            GetConsigneeAndConsignorDetails(reply, consignmentEntity.PlannedConsignment);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.SetValidateGoodsReplyToConsignmentItem");
            }
        }

        public static void GetConsigneeAndConsignorDetails(T20200_ValidateGoodsFromFOTReply validateGoodsReply, PlannedConsignmentsType plannedConsignment)
        {
            try
            {
                if (validateGoodsReply != null && plannedConsignment != null && validateGoodsReply.Consignment != null)
                {
                    var consignee = validateGoodsReply.Consignment.Consignee;
                    if (consignee != null)
                    {
                        plannedConsignment.Consignee = new ConsigneeType
                            {
                                Address1 = consignee.Address1,
                                Address2 = consignee.Address2,
                                PostalCode = consignee.PostalCode,
                                PostalName = consignee.PostalName,
                                CountryCode = consignee.CountryCode,
                                DeliveryAddress1 = consignee.Address1,
                                DeliveryAddress2 = consignee.Address2,
                                DeliveryCountryCode = consignee.CountryCode,
                                DeliveryPostalCode = consignee.PostalCode,
                                DeliveryPostalName = consignee.PostalName,
                                Name1 = consignee.Name1,
                                Name2 = consignee.Name2,
                                PhoneNumber = consignee.PhoneNumber
                            };
                    }


                    var consignor = validateGoodsReply.Consignment.Consignor;
                    if (consignor != null)
                    {
                        plannedConsignment.Consignor = new ConsignorType
                            {
                                Name1 = consignor.Name1,
                                Name2 = consignor.Name2,
                                Address1 = consignor.Address1,
                                Address2 = consignor.Address2,
                                CountryCode = consignor.CountryCode,
                                PostalCode = consignor.PostalCode,
                                PostalName = consignor.PostalName
                            };

                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogEvent(Severity.Error, "Unexpected error in execution of function GetConsigneeAndConsignorDetails: " + e.Message);
            }
        }

        private static void SetValidateGoodsReplyToConsignment(ConsignmentEntity consignmentEntity, T20200_ValidateGoodsFromFOTReply reply)
        {
            try
            {
                var consignment = (Consignment)consignmentEntity;
                var consignmentDetail = consignment.PlannedConsignment ?? new PlannedConsignmentsType();

                consignmentDetail.SetDataFromValidateGoodsResponse(reply.Consignment);
                consignment.PlannedConsignment = consignmentDetail;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.SetValidateGoodsReplyToConsignment");
            }
        }

        private static T20200_ValidateGoodsFromFOTRequest CreateValidateGoodsRequest(ConsignmentEntity consignmentEntity, UserProfile userProfile)
        {
            T20200_ValidateGoodsFromFOTRequest request = null;
            try
            {
                request = new T20200_ValidateGoodsFromFOTRequest
                  {
                      GetVASPayment = false,
                      GetEvents = "3",
                      ValidationLevel = ValidateGoodsRequestValidationLevel.Goods,
                      GetPackageType = false,
                      LoggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationType>("Reconcile scan goods", userProfile),
                      GetExtended = true,
                      GetAllItems = true
                  };

                if (consignmentEntity is Consignment)
                {
                    request.ValidationType = ValidateGoodsRequestValidationType.Consignment;
                    request.ConsignmentNumber = consignmentEntity.ConsignmentIdForValidateGoods;
                    request.GetAllItems = true;
                }
                else
                {
                    request.ValidationType = ValidateGoodsRequestValidationType.ConsignmentItem;
                    request.ConsignmentItemNumber = consignmentEntity.CastToConsignmentItem().ItemId;
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.CreateValidateGoodsRequest");
            }
            return request;
        }

        //TODO :: Write Unit Test
        //Deletes it from the cloned map and add the item to a list, so that it can be deleted from the original map later
        public void DeleteConsignmentEntity(ConsignmentEntity consignmentEntity, EntityMap entityMap, EntityMap clonedEntityMap)
        {
            try
            {
                EventReportHelper.SendItemDeleteEvent(consignmentEntity);
                if (entityMap.ContainsKey(consignmentEntity.ConsignmentId))
                    entityMap.RemoveEntity(consignmentEntity);
                if (clonedEntityMap != null && clonedEntityMap.ContainsKey(consignmentEntity.ConsignmentId))
                    clonedEntityMap.RemoveEntity(consignmentEntity);

                // Remove consignment from VAS history, so ShowOnlyOnce settings will show again
                BaseFlow parentWorkProcessFlow = BaseModule.ParentWorkProcessFlow;
                if (parentWorkProcessFlow != null)
                {
                    parentWorkProcessFlow.VasHistory.DeleteProcessHistoryForConsignmentEntity(consignmentEntity);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.DeleteConsignmentEntity");
            }
        }


        //TODO :: Write Unit Test

        static internal List<ConsignmentItem> GetConsignmentItemListSortedByDeviations(EntityMap entityMap, string consignmentId)
        {
            try
            {
                if (entityMap != null)
                    return (from entry in entityMap.GetConsignmentItems(consignmentId).Values orderby DeviationStatusForConsignmentItem(entry) descending select entry).ToList();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.GetConsignmentItemListSortedByDeviations");
            }
            return null;
        }




        //TODO :: Write Unit Test
        static internal int DeviationStatusForConsignmentItem(ConsignmentItem consignmentItem)
        {
            try
            {
                switch (BaseModule.CurrentFlow.CurrentProcess)
                {
                    case Process.InTerminal:
                        return 4;

                    default:
                        var status = consignmentItem.EntityStatus;
                        if (consignmentItem.IsConsignmentSynthetic)//I 2232 sythetic should have accepted status
                            return 0;
                        switch (status)
                        {
                            case EntityStatus.False:
                            case EntityStatus.ScannedPlanned:
                                return 0;
                            case EntityStatus.Scanned:
                                return 1;
                            case EntityStatus.Planned:
                                return 2;
                            default:
                                Logger.LogEvent(Severity.Error, "Unhandled status" + status);
                                break;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.DeviationStatusForConsignmentItem");
            }
            return 0;
        }

        //TODO :: Write Unit Test
        static internal List<Consignment> ConsignmentSortedByDeviationStatus(EntityMap entityMap)
        {
            try
            {
                return (from consignment in entityMap.Values orderby DeviationStatus(consignment) descending select consignment)
                   .ToList();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.ConsignmentSortedByDeviationStatus");
            }
            return null;
        }
        //TODO :: Write Unit Test
        static internal int DeviationStatus(Consignment consignment)
        {
            if (consignment is LoadCarrier)
            {
                return (int)EntityMap.DeviationStatus.AcceptedStatus;
            }

            int scannedConsignmentItemCount = consignment.ItemCount(EntityStatus.Scanned, false);
            int plannedConsignmentItemCount = consignment.ItemCount(EntityStatus.Planned, false);
            int plannedAndScannedConsignmentItemCount = consignment.ItemCount(EntityStatus.ScannedPlanned, true);
            int consignmentItemCountFromLm = consignment.ConsignmentItemCountFromLm;
            return DeviationStatusForScanCount(plannedAndScannedConsignmentItemCount, plannedConsignmentItemCount, scannedConsignmentItemCount, consignmentItemCountFromLm);
        }

        //TODO :: Write Unit Test
        static internal string DeviationStatus(Consignment consignment, out string countSummary)
        {
            string statusString;
            if (consignment is LoadCarrier)
            {
                countSummary = BaseModule.CurrentFlow.CurrentProcess == Process.OutOfHoldingAtHub ? "1/1" : "1/1/1";
                return "3";
            }

            int scannedConsignmentItemCount = consignment.ItemCount(EntityStatus.Scanned, false);
            int plannedConsignmentItemCount = consignment.ItemCount(EntityStatus.Planned, false);
            int plannedAndScannedConsignmentItemCount = consignment.ItemCount(EntityStatus.ScannedPlanned, true);
            int consignmentItemCountFromLm = consignment.ConsignmentItemCountFromLm;
            switch (BaseModule.CurrentFlow.CurrentProcess)
            {
                case Process.InTerminal:
                    statusString = Convert.ToString((int)EntityMap.DeviationStatus.Empty);
                    break;
                case Process.OutOfHoldingAtHub:
                    statusString = Convert.ToString(DeviationStatusForScanCount(plannedAndScannedConsignmentItemCount, plannedConsignmentItemCount, scannedConsignmentItemCount, consignmentItemCountFromLm));
                    break;
                default:
                    statusString = Convert.ToString(DeviationStatusForScanCount(plannedAndScannedConsignmentItemCount, plannedConsignmentItemCount, scannedConsignmentItemCount, consignmentItemCountFromLm));
                    break;
            }

            countSummary = GetCountSummaryAccordingToProcess(BaseModule.CurrentFlow.CurrentProcess, scannedConsignmentItemCount, plannedConsignmentItemCount, consignment);

            return statusString;
        }
        //TODO :: Write Unit Test
        private static string GetCountSummaryAccordingToProcess(Process currentProcess, int scannedConsignmentItemCount, int plannedConsignmentItemCount, Consignment consignment)
        {

            string countSummary;
            switch (currentProcess)
            {

                case Process.InTerminal:
                    countSummary = string.Format("{0}", consignment.ManuallyEnteredConsignmentItemCount > 0 ? consignment.ManuallyEnteredConsignmentItemCount : scannedConsignmentItemCount);
                    break;
                case Process.OutOfHoldingAtHub:
                    countSummary = string.Format("{0}/{1}", scannedConsignmentItemCount, consignment.ConsignmentItemCountFromLm);
                    break;

                default:
                    countSummary = string.Format("{0}/{1}/{2}", scannedConsignmentItemCount, plannedConsignmentItemCount, consignment.ConsignmentItemCountFromLm);
                    break;
            }
            return countSummary;
        }
        //TODO :: Write Unit Test
        private static int DeviationStatusForScanCount(int plannedAndScannedConsignmentItemCount, int plannedConsignmentItemCount, int scannedConsignmentItemCount, int consignmentItemCountFromLm)
        {
            try
            {
                if (plannedAndScannedConsignmentItemCount == plannedConsignmentItemCount && plannedAndScannedConsignmentItemCount == scannedConsignmentItemCount)
                    return (int)EntityMap.DeviationStatus.AcceptedStatus;

                if (plannedAndScannedConsignmentItemCount < plannedConsignmentItemCount || scannedConsignmentItemCount < consignmentItemCountFromLm)
                    return (int)EntityMap.DeviationStatus.MissingStatus;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsReconciliation.DeviationStatusForScanCount");
            }
            return (int)EntityMap.DeviationStatus.AdditionalStatus;
        }

    }
}

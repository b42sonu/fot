﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation
{
    public class ViewCommandsReconciliation
    {
        internal void ShowReconciliationForm(ViewEventDelegate viewEventHandler, FlowDataReconciliation flowData, EntityMap reconciledEntityMap, bool isOpeningFirstTime)
        {
            try
            {
                var view = ViewCommands.ShowView<FormReconciliation>(new FormDataReconcillitaion
                {
                    ReconciledEntityMap = reconciledEntityMap,
                    OriginalEntityMap = flowData.EntityMap,
                    CancelButtonVisible = flowData.CancelButtonVisible,
                    OkButtonVisible = flowData.OkButtonVisible,
                    OkButtonDisableWhenNoItemsInList = flowData.OkButtonDisableWhenNoItemsInList,
                    HeaderText = flowData.HeaderText,
                    WorklistItem = flowData.WorklistItem,
                    StopInformation = ModelMain.SelectedOperationProcess != null ? CommandsOperationList.GetStopType(ModelMain.SelectedOperationProcess.StopId) : null,
                    IsOpeningFirstTime = isOpeningFirstTime,
                    OrgUnitName = flowData.location == null ? ModelUser.UserProfile.OrgUnitName : flowData.location.Name
                });
                view.SetEventHandler(viewEventHandler);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsReconciliation.ShowReconciliationForm()");
            }
        }


        internal void ShowReconciliationFirstTime(ViewEventDelegate viewEventHandler, FlowDataReconciliation flowData, EntityMap reconciledEntityMap)
        {
            try
            {
                ShowReconciliationForm(viewEventHandler, flowData, reconciledEntityMap, true);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsReconciliation.ShowReconciliationForm()");
            }
        }


    }
}

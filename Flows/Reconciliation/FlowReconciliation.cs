﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu;
using Com.Bring.PMP.PreComFW.Shared.Flows.Printer;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;

// US 76j: http://kp-confluence.postennorge.no/display/FOT/76j+-+Reconciliation+List
namespace Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation
{
    public enum FlowState
    {
        ActionValidateGoodsAndSetInfoToConsignmentEntityOnItemButton,
        ActionBackFromCorrectionMenu,
        ActionValidateGoodsAndSetInfoToConsignmentEntity,
        ActionReconcileScannedWithGoodsList,
        ActionShouldRequestGoodsList,
        ActionIsReturningBackToReconcilliation,
        ActionDeleteConsignmentEntity,
        ActionSendGoodsDeleteEvents,
        ActionBackFromSelectPrinter,
        ActionGetWorkList,
        ViewCommands,
        ViewUpdateView,
        ViewShowReconciliationForm,
        ViewShowReconciliationFormFirstTime,
        ViewShowPrinterListForm,
        FlowGotoScanCorrectionMenu,
        FlowComplete,
        FlowCanceled
    }

    public class FlowDataReconciliation : BaseFlowData
    {
        public FlowDataReconciliation()
        {
            OkButtonVisible = true;
            CancelButtonVisible = true;
            PowerUnitId = ModelUser.UserProfile != null ? ModelUser.UserProfile.PowerUnit : string.Empty;
        }

        public EntityMap EntityMap;
        public WorkListItem WorklistItem;
        public LocationElement location { get; set; }
        public string PowerUnitId { get; set; }
        public bool OkButtonDisableWhenNoItemsInList { get; set; }  //this property created when no consignment/item  in list .Then any flow is set false only that case ok button disabled other wise it remain enabled..
        public bool OkButtonVisible { get; set; }
        public bool CancelButtonVisible { get; set; } // Set to true if flow can be canceled. If false the cancel button is hidden
        public bool ShowingConsignments { get; set; }
        public string PlacementId { get; set; }
        public bool IsReturningToReconcilliation;
        public ScanningProcessStartedForm ScanningStartedFrom { get; set; }
        public string GoodsCategoryCode
        {
            get
            {
                bool? isLoading = EventCodeUtil.IsLoading;

                if (isLoading != null)
                {
                    if (isLoading == true)
                        return "LA"; //Unloading
                    return "LO"; //Loading
                }
                return string.Empty;
            }
        }
    }

    public class FlowResultReconciliation : BaseFlowResult
    {
        public EntityMap EntityMap;
    }

    public class FlowReconciliation : BaseFlow
    {
        private FlowDataReconciliation _flowData;
        private readonly ActionCommandsReconciliation _actionCommandsReconciliation;
        private readonly ViewCommandsReconciliation _viewCommandsReconciliation;
        private ConsignmentEntity _currentConsignmentEntity;
        private readonly FlowResultReconciliation _flowResult = new FlowResultReconciliation();
        private EntityMap _entityMapCloned; // Used as working data so we don't alter the local data, with entries from back-end

        public FlowReconciliation()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Excecuting FlowReconciliation()");
                _actionCommandsReconciliation = new ActionCommandsReconciliation(CommunicationClient.Instance);
                _viewCommandsReconciliation = new ViewCommandsReconciliation();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Execption in  FlowReconciliation()");
            }
        }

        public override void BeginFlow(IFlowData flowData)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Initializing flow FlowReconciliation");
                _flowData = (FlowDataReconciliation)flowData;
                _entityMapCloned = _flowData.EntityMap.Clone();
                ExecuteState(FlowState.ActionShouldRequestGoodsList);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Execption in flow FlowReconciliation.BeginFlow");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowState.FlowComplete);
            }
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowState)state);
        }

        public void ExecuteState(FlowState state)
        {
            if (state > FlowState.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        private void ExecuteViewState(FlowState state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug , "#FlowReconciliation;" + state);

                switch (state)
                {
                    case FlowState.ViewShowReconciliationForm:
                        _viewCommandsReconciliation.ShowReconciliationForm(ReconciliationViewEventHandler, _flowData, _entityMapCloned,false);
                        break;

                    case FlowState.ViewShowReconciliationFormFirstTime:
                        _viewCommandsReconciliation.ShowReconciliationFirstTime(ReconciliationViewEventHandler, _flowData, _entityMapCloned);
                        break;

                    case FlowState.FlowGotoScanCorrectionMenu:
                        var flowData = new FlowDataCorrectionMenu(_flowData.HeaderText, _currentConsignmentEntity, _flowData.EntityMap)
                        {
                            DisableDeleteButton = _currentConsignmentEntity != null && !_currentConsignmentEntity.HasStatus(EntityStatus.Scanned) //false
                        };
                        StartSubFlow<FlowCorrectionMenu>(flowData, (int)FlowState.ActionBackFromCorrectionMenu, Process.Inherit);
                        break;

                    case FlowState.ViewShowPrinterListForm:
                        var flowDataPrinter = new FlowDataPrintLoadList
                            {
                                WorkListItem = _flowData.WorklistItem,
                                IsProcessStartedFromReconcilition = true,
                                PlacementId = _flowData.PlacementId,
                            };
                        StartSubFlow<FlowPrintLoadList>(flowDataPrinter, (int)FlowState.ActionBackFromSelectPrinter, Process.Inherit);
                        break;

                    case FlowState.ViewUpdateView:
                        CurrentView.UpdateView(new FormDataReconcillitaion { CurrentConsignmentEntity = _currentConsignmentEntity });
                        break;


                    case FlowState.FlowCanceled:
                        EndFlow(false);
                        break;

                    case FlowState.FlowComplete:
                        EndFlow(true);
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "FlowDataReconciliation.ExecuteViewState: Encountered unhandled view state");
                        break;
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "Execption in " + state + "in flow FlowReconciliation");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowState.FlowComplete);
            }
        }

        public void ExecuteActionState(FlowState state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug , "#FlowReconciliation;" + state);

                    switch (state)
                    {
                        case FlowState.ActionShouldRequestGoodsList:
                            bool isTripAndStopKnown = _actionCommandsReconciliation.ShouldGetGoodsList(ModelMain.SelectedOperationProcess, _flowData);
                            state = isTripAndStopKnown ? FlowState.ActionReconcileScannedWithGoodsList : FlowState.ActionIsReturningBackToReconcilliation;
                            break;

                        case FlowState.ActionIsReturningBackToReconcilliation:
                            state = _flowData.IsReturningToReconcilliation ? FlowState.ViewShowReconciliationForm : FlowState.ViewShowReconciliationFormFirstTime;
                            break;

                        case FlowState.ActionReconcileScannedWithGoodsList:
                            var goodsList = _actionCommandsReconciliation.RequestGoodsList(_flowData, ModelMain.SelectedOperationProcess, ModelUser.UserProfile);
                            var stopId = ModelMain.SelectedOperationProcess != null ? ModelMain.SelectedOperationProcess.StopId : null;
                            _actionCommandsReconciliation.ReconcileScannedWithGoodsList(_flowData.EntityMap, goodsList, stopId);
                            _entityMapCloned = _flowData.EntityMap.Clone();
                            state = FlowState.ViewShowReconciliationFormFirstTime;
                            break;

                        case FlowState.ActionBackFromCorrectionMenu:
                             _actionCommandsReconciliation.BackFromCorrectionMenu((FlowResultCorrectionMenu)SubflowResult, ref _entityMapCloned);
                            state =  FlowState.ViewShowReconciliationForm;
                            break;

                        case FlowState.ActionValidateGoodsAndSetInfoToConsignmentEntity:
                            _actionCommandsReconciliation.ValidateGoodsAndSetInfoToConsignmentEntity(_currentConsignmentEntity, true, ModelUser.UserProfile);
                            state = FlowState.ViewUpdateView;
                            break;
                        case FlowState.ActionValidateGoodsAndSetInfoToConsignmentEntityOnItemButton:
                            _actionCommandsReconciliation.ValidateGoodsAndSetInfoToConsignmentEntity(_currentConsignmentEntity, true, ModelUser.UserProfile);
                            state = FlowState.ViewShowReconciliationForm;
                            break;
                        case FlowState.ActionDeleteConsignmentEntity:
                            _actionCommandsReconciliation.DeleteConsignmentEntity(_currentConsignmentEntity, _flowData.EntityMap, _entityMapCloned);
                            state = FlowState.ViewShowReconciliationForm;
                            break;

                        case FlowState.ActionBackFromSelectPrinter:
                            state = FlowState.ViewShowReconciliationForm;
                            break;

                        default:
                            Logger.LogEvent(Severity.Error, "FlowDataReconciliation.ExecuteActionState: Encountered unhandled view state");
                            break;
                    }
                } while (state < FlowState.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Execption in " + state + "in flow FlowReconciliation");
                _flowResult.State = FlowResultState.Exception;
                state = FlowState.FlowComplete;
            }
            ExecuteViewState(state);
        }

        private void ReconciliationViewEventHandler(int viewEvents, params object[] data)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Excecuting FlowReconciliation.ReconciliationViewEventHandler");
                switch (viewEvents)
                {
                    case ViewEventsReconcilliation.Ok:
                        ExecuteState(FlowState.FlowComplete);
                        break;

                    case ViewEventsReconcilliation.Cancel:
                        ExecuteState(FlowState.FlowCanceled);
                        break;

                    case ViewEventsReconcilliation.Deviation:
                        _currentConsignmentEntity = (ConsignmentEntity)data[0];
                        ExecuteState(FlowState.FlowGotoScanCorrectionMenu);
                        break;

                    case ViewEventsReconcilliation.Detail:
                        _currentConsignmentEntity = (ConsignmentEntity)data[0];
                        ExecuteState(FlowState.ActionValidateGoodsAndSetInfoToConsignmentEntity);
                        break;

                    case ViewEventsReconcilliation.Delete:
                        _currentConsignmentEntity = (ConsignmentEntity)data[0];
                        ExecuteState(FlowState.ActionDeleteConsignmentEntity);
                        break;

                    case ViewEventsReconcilliation.Print:
                        ExecuteState(FlowState.ViewShowPrinterListForm);
                        break;

                    case ViewEventsReconcilliation.Sign:
                        ExecuteState(FlowState.FlowComplete);
                        break;
                    case ViewEventsReconcilliation.Items:
                        _currentConsignmentEntity = (ConsignmentEntity)data[0];
                        ExecuteState(FlowState.ActionValidateGoodsAndSetInfoToConsignmentEntityOnItemButton);
                        break;
                    default:
                        Logger.LogEvent(Severity.Error, "FlowDataReconciliation.ReconciliationViewEventHandler: Encountered unhandled state");
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Execption in flow FlowReconciliation.ReconciliationViewEventHandler");
            }
        }

        private void EndFlow(bool result)
        {
            try
            {
                //var flowResult = new FlowResultReconciliation() { State = FlowResultState.Ok, ReconciledEntityMap = _flowData.ReconciledEntityMap };
                Logger.LogEvent(Severity.Debug, "Ending flow FlowReconciliation");
                if (result == false)
                {
                    _flowResult.State = FlowResultState.Cancel;
                    _flowResult.Message = GlobalTexts.ReconcilliationCancelled;
                }
                BaseModule.EndSubFlow(_flowResult);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Execption in flow FlowReconciliation.EndFlow");
            }
        }
    }
}

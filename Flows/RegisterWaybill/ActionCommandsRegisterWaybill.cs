﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill
{
    public class ActionCommandsRegisterWaybill
    {
        private readonly MessageHolder _messageHolder;
        //TODO :: Write Unit Test
        public ActionCommandsRegisterWaybill(MessageHolder messageHolder)
        {
            _messageHolder = messageHolder;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Send Async Register Waybill message
        /// </summary>
        /// <returns></returns>
        internal bool SendAsyncMessageForRegisterWaybill(CreateConsignmentNote consignmentNote)
        {
            bool sent = false;
            if (consignmentNote != null)
            {
                CommunicationClient.Instance.SendConsignmentNoteEvent(consignmentNote);
                sent = true;
            }
            return sent;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Create object for Consignment note Event
        /// </summary>
        internal CreateConsignmentNote CreateConsignmentNoteEvent(FotScannerOutput scannerOutput)
        {
            var consignmentNote = new CreateConsignmentNote
            {
                PDF417Raw = scannerOutput.Code
            };
            return consignmentNote;
        }

        //TODO :: Write Unit Test
        internal bool SendGoodsEventForRegisterWaybill(ConsignmentEntity consignmentEntity, string eventCode, string actionCode, string causeCode)
        {
            try
            {
                var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity);
                goodsEvent.EventInformation.ActionCode = actionCode;
                goodsEvent.EventInformation.CauseCode = causeCode;
                goodsEvent.EventInformation.EventCode = eventCode;
                var transaction = new Transaction(Guid.NewGuid(),
                                                  (byte)TransactionCategory.GoodsEventsEventInformation,
                                                  consignmentEntity.ConsignmentId);
                CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);
                return true;
            }
            catch (PreCom.Core.NotLoggedInExeption ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsRegisterWaybill.SendGoodsEventForRegisterWaybill");
                return false;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsRegisterWaybill.SendGoodsEventForRegisterWaybill");
                return false;
            }
        }




        internal FlowStateRegisterWaybill ValidateScanConsignmentResult(FlowResultScanConsignment subFlowResult, ConsignmentItem consignmentItem, ref BarcodeData currentScanning, ref FlowResultRegisterWaybill flowResult, ref FotScannerOutput fotScannerOutput, bool calledFromExternalFlow)
        {
            bool result;
            FlowStateRegisterWaybill returnState;

            switch (subFlowResult.State)
            {
                case FlowResultState.Ok:
                    _messageHolder.Update(string.Format(GlobalTexts.ConsignmentAddedToItem, consignmentItem.ConsignmentDisplayId, consignmentItem.ItemDisplayId), MessageState.Information);
                    result = true;
                    break;

                case FlowResultState.Error:
                case FlowResultState.Cancel://Skipped
                    result = false;
                    currentScanning = null;
                    flowResult.IsSkippedOnceForPickup = subFlowResult.State == FlowResultState.Cancel;
                    _messageHolder.Update(subFlowResult.Message, MessageState.Warning);

                    flowResult.Message = subFlowResult.Message;
                    flowResult.State = subFlowResult.State == FlowResultState.Error
                                           ? FlowResultState.Error
                                           : calledFromExternalFlow ? subFlowResult.State : FlowResultState.Ok;

                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unknown state");
                    break;
            }

            if (result)
            {
                fotScannerOutput = subFlowResult.ScannerOutput;
                returnState = (subFlowResult.CurrentScanning != null) &&
                    subFlowResult.CurrentScanning.BarcodeSubType == BarcodeSubType.Pdf ?
                    FlowStateRegisterWaybill.ActionCreateAndSendConsignmentNoteEvent : FlowStateRegisterWaybill.ActionSendGoodsEvent;
            }
            else
            {
                returnState = FlowStateRegisterWaybill.FlowCancelled;
            }
            return returnState;
        }
    }
}

﻿
using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill
{
    public enum FlowStateRegisterWaybill
    {
        ActionValidateBarcode,
        ActionCreateAndSendConsignmentNoteEvent,
        ActionBackFromScanConsignment,
        ActionSendGoodsEvent,
        ViewCommands,
        ViewScanPdf,
        FlowScanConsignment,
        ThisFlowComplete,
        FlowCancelled
    }

    #region External Classes

    public class FlowDataRegisterWaybill : BaseFlowData
    {
        public ConsignmentItem ConsignmentItem { get; set; }
        public bool IsCalledFromExternalFlow { get; set; }
    }

    public class FlowResultRegisterWaybill : BaseFlowResult
    {
        public bool IsSkippedOnceForPickup { get; set; }
        public ConsignmentItem ConsignmentItem { get; set; }
    }

    #endregion

    /// <summary>
    /// US 19 http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554914
    /// </summary>
    public class FlowRegisterWaybill : BaseFlow
    {
        #region Private Vars
        private readonly MessageHolder _messageHolder;
        private BarcodeData _currentScanning;
        private FlowResultRegisterWaybill _flowResult;
        public FotScannerOutput FotScannerOutput;
        private FlowDataRegisterWaybill _flowData;
        private readonly ActionCommandsRegisterWaybill _actionCommands;
        #endregion

        #region Constructor

        public FlowRegisterWaybill()
        {
            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultRegisterWaybill();
            _actionCommands = new ActionCommandsRegisterWaybill(_messageHolder);
        }

        #endregion

        #region Overriden Methods

        public override void BeginFlow(IFlowData flowData)
        {
            _flowData = (FlowDataRegisterWaybill)flowData;
            _messageHolder.Update(string.Format(GlobalTexts.UnknownConsignmentScanPdf, _flowData.ConsignmentItem.ItemDisplayId), MessageState.Warning);
            ExecuteViewState(FlowStateRegisterWaybill.FlowScanConsignment);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateRegisterWaybill)state);
        }

        #endregion


        private void ExecuteState(FlowStateRegisterWaybill state)
        {
            if (state > FlowStateRegisterWaybill.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        public void ExecuteActionState(FlowStateRegisterWaybill state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowRegisterWaybill;" + state);
                    //check the State and execute the same
                    switch (state)
                    {
                        case FlowStateRegisterWaybill.ActionBackFromScanConsignment:
                            state = _actionCommands.ValidateScanConsignmentResult((FlowResultScanConsignment)SubflowResult, _flowData.ConsignmentItem, ref _currentScanning, ref _flowResult, ref FotScannerOutput, _flowData.IsCalledFromExternalFlow);
                            break;


                        case FlowStateRegisterWaybill.ActionCreateAndSendConsignmentNoteEvent:
                            NoteEventHelper.CreateAndSendConsignmentNoteEvent(FotScannerOutput);
                            state = FlowStateRegisterWaybill.ThisFlowComplete;
                            break;

                        case FlowStateRegisterWaybill.ActionSendGoodsEvent:
                            _actionCommands.SendGoodsEventForRegisterWaybill(_flowData.ConsignmentItem, "V", "14", "AN");//TODO : get these parameters
                            state = FlowStateRegisterWaybill.ThisFlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateRegisterWaybill.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowRegisterWaybill.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateRegisterWaybill.ThisFlowComplete;
            }


            ExecuteViewState(state);
        }

        private void ExecuteViewState(FlowStateRegisterWaybill state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowRegisterWaybill;" + state);
            switch (state)
            {
                case FlowStateRegisterWaybill.FlowScanConsignment:
                    var flowDataScanConsignment = new FlowDataScanConsignment
                    {
                        HeaderText = _flowData.HeaderText,
                        ConsignmentItem = _flowData.ConsignmentItem,
                        FromRegisterWaybill = true,
                        ScanText = GlobalTexts.ScanPdf417
                                                
                    };
                    StartSubFlow<FlowScanConsignment>(flowDataScanConsignment, (int)FlowStateRegisterWaybill.ActionBackFromScanConsignment, Process.Inherit);

                    break;

                case FlowStateRegisterWaybill.ThisFlowComplete:
                    _flowResult.State = FlowResultState.Ok;
                    _flowResult.ConsignmentItem = _flowData.ConsignmentItem;
                    EndFlow();
                    break;

                case FlowStateRegisterWaybill.FlowCancelled:
                    _flowResult.ConsignmentItem = _flowData.ConsignmentItem;
                    EndFlow();
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled view State");
                    return;
            }
        }

        private void EndFlow()
        {
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

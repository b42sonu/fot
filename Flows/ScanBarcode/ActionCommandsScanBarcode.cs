﻿using System;
using System.Linq;
using Com.Bring.PMP.CSharp;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using PreCom.Core.Communication;
using PreCom.MeasurementCore;
using LoggingInformationType = Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Loading.LoggingInformationType;
using LoggingInformationTypeForLoadCarrier = Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.ValidationLC.LoggingInformationType;


// US 76G : http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7933803
namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    public class ActionCommandsScanBarcode
    {
        private readonly MessageHolder _messageHolder;
        public CombinationTrip CombinationTrip { get; set; }


        public MessageHolder MessageHolder
        {
            get { return _messageHolder; }
        }

        public ActionCommandsScanBarcode(MessageHolder messageHolder)
        {
            _messageHolder = messageHolder;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Validate Result after back from FlowRegisterWaybill
        /// </summary>
        public bool ValidateRegisterWaybillResult(FlowResultRegisterWaybill subFlowResult, ConsignmentItem consignmentItem, ref bool isSkippedOnceForPickup)
        {
            bool result;
            switch (subFlowResult.State)
            {
                case FlowResultState.Ok://Waybill registration information successfully sent
                    _messageHolder.Update(string.Format(GlobalTexts.ConsignmentAddedToItem, consignmentItem.ConsignmentDisplayId, consignmentItem.ItemDisplayId),
                        MessageState.Information);
                    result = true;
                    break;

                //Skipped Waybill registration 
                case FlowResultState.Cancel:
                    result = true;
                    if (subFlowResult.IsSkippedOnceForPickup)
                        isSkippedOnceForPickup = true;
                    _messageHolder.Update(subFlowResult.Message, MessageState.Error);
                    break;

                case FlowResultState.Error:
                    result = true;
                    _messageHolder.Update(subFlowResult.Message, MessageState.Error);
                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unknown state");
                    break;
            }
            return result;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Validate the result from sub flow where consignment were scanned. If empty means scan was cancelled, else scan was valid
        /// </summary>
        internal bool ValidateScanConsignmentResult(FlowResultScanConsignment subFlowResult, ConsignmentItem consignmentItem, ref BarcodeData currentScanning)
        {
            bool result;

            switch (subFlowResult.State)
            {
                case FlowResultState.Ok:
                    _messageHolder.Update(string.Format(GlobalTexts.ConsignmentAddedToItem, consignmentItem.ConsignmentDisplayId, consignmentItem.ItemDisplayId), MessageState.Information);
                    result = true;
                    break;

                case FlowResultState.Cancel:
                    result = false;
                    currentScanning = null;
                    _messageHolder.Update(subFlowResult.Message, MessageState.Warning);
                    break;

                default:
                    result = false;
                    Logger.LogEvent(Severity.Error, "Unknown state");
                    break;
            }

            return result;
        }


        /// <summary>
        /// Takes output from scanner, decodes it and returns decoded data if entityNumber was of expected type
        /// </summary>
        public bool ValidateBarcode(FotScannerOutput scannerOutput, BarcodeType validTypes, out BarcodeData barcodeData, bool haveCustomForm)
        {
            bool result = BaseActionCommands.ValidateBarcode(scannerOutput, validTypes, out barcodeData);
            if (result == false)
            {
                if (!haveCustomForm)
                    SoundUtil.Instance.PlayScanErrorSound();
                var scantext = BaseActionCommands.ScanTextFromValidBarcodeTypes(validTypes, GlobalTexts.ScanOrEnterBarcode);
                _messageHolder.Update(GlobalTexts.InvalidNumber + ".\r\n" + scantext + ".", MessageState.Error);
            }

            return result;
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// Takes output from scanner, decodes it and returns decoded data if entityNumber was of expected type
        /// </summary>
        public FlowStateScanBarcode DecideStateBasedOnScan(BarcodeType actualType, Process currentProcess)
        {
            var nextState = FlowStateScanBarcode.ThisFlowComplete;

            do
            {
                if (actualType == BarcodeType.LoadCarrier || actualType == BarcodeType.TransportCarrier)
                {
                    switch (currentProcess)
                    {
                        case Process.Loading:
                        case Process.Unloading:
                            nextState = FlowStateScanBarcode.ActionGetExistingLoadCarrierWorkListItem;
                            break;
                        default:
                            nextState = FlowStateScanBarcode.ActionCreateConsignmentEntity;
                            break;
                    }
                    break;
                }

                if ((actualType & BarcodeType.Shipment) != BarcodeType.Unknown || (actualType & BarcodeType.DeliveryCode) == BarcodeType.DeliveryCode)
                {
                    nextState = FlowStateScanBarcode.ActionCreateConsignmentEntity;
                    break;
                }



                if (actualType == BarcodeType.Rbt || actualType == BarcodeType.Shelf || actualType == BarcodeType.RouteId ||
                    actualType == BarcodeType.OrgUnitId || actualType == BarcodeType.DriversLicence)
                {
                    nextState = FlowStateScanBarcode.ActionGetFlowResultForScanning;
                    break;
                }


                if (actualType == BarcodeType.Unknown || actualType == BarcodeType.TransportCarrier)
                {
                    nextState = FlowStateScanBarcode.ThisFlowComplete;
                    break;
                }

                Logger.LogEvent(Severity.Error, "Encountered unhandled entityNumber '{0}' protocol in FlowScanBarcode", actualType);

            } while (false);

            return nextState;
        }
        //TODO :: Write Unit Test
        public void GetFlowResultForScanning(FlowResultScanBarcode flowResult, BarcodeData currentScanning, BarcodeType barcodeType)
        {
            //TODO:: Unit tests

            if (currentScanning != null)
            {
                flowResult.State = FlowResultState.Ok;
                switch (currentScanning.BarcodeType)
                {
                    case BarcodeType.Rbt:
                        flowResult.BarcodeRouteCarrierTrip = currentScanning.BarcodeRouteCarrierTrip;
                        break;

                    case BarcodeType.LoadCarrier:
                        flowResult.LoadCarrierId = currentScanning.BarcodeLoadCarrier.LoadCarrierId;
                        break;

                    case BarcodeType.Unknown:
                        flowResult.State = FlowResultState.Error;
                        flowResult.ErrorInfo = ErrorInfo.IllegalBarcode;
                        break;

                    case BarcodeType.ConsignmentNumber:
                    case BarcodeType.ConsignmentItemNumber:
                        {
                            if (_messageHolder.State == MessageState.Error)
                            {
                                flowResult.State = FlowResultState.Error;
                                flowResult.ErrorInfo = ErrorInfo.NotAuthorised;
                            }
                            break;
                        }

                    case BarcodeType.Shelf:
                        flowResult.ShelfId = currentScanning.ShelfId;
                        break;

                    case BarcodeType.OrgUnitId:
                        flowResult.OrgUnitId = currentScanning.OrgUnitId;
                        break;

                    case BarcodeType.RouteId:
                        flowResult.RouteId = currentScanning.RouteId;
                        break;

                    case BarcodeType.DeliveryCode:
                        flowResult.DeliveryCode = currentScanning.DeliveryCode;
                        break;

                    case BarcodeType.DriversLicence:
                        flowResult.LicenceId = currentScanning.LicenceNumber;
                        break;
                }

                flowResult.BarcodeType = currentScanning.BarcodeType;
                flowResult.BarcodeSubType = currentScanning.BarcodeSubType;

                if ((currentScanning.BarcodeType & barcodeType) == BarcodeType.Unknown)
                {
                    flowResult.State = FlowResultState.Error;
                    flowResult.ErrorInfo = ErrorInfo.IllegalBarcode;
                }
            }
            else
            {
                flowResult.State = _messageHolder.State == MessageState.Error ? FlowResultState.Error : FlowResultState.Warning;
            }
            flowResult.MessageHolder = _messageHolder;
        }



        /// <summary>
        /// Checks if entityNumber has already been scanned 
        /// </summary>
        internal bool IsScannedBefore(EntityMap entityMap, ref ConsignmentEntity consignmentEntity)
        {
            return entityMap != null && entityMap.GetScannedBefore(ref consignmentEntity);
        }

        // Return true if entityNumber is of type PruTracking
        private bool IsBarcodePruTracking(ConsignmentEntity consignmentEntity)
        {
            bool result = consignmentEntity.IsPruScanning;
            if (result)
            {
                // No online validation of PRU Tracking codes, but we dont want it to be marked as scanned offline
                consignmentEntity.IsScannedOnline = true;
            }

            return result;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Check if consignment number for a consignment is known
        /// </summary>
        /// <returns>If process is finished or not</returns>
        public bool IsConsignmentNumberForItemKnown(ConsignmentEntity consignmentEntity)
        {
            // For pru scannings, the consignment number will always be unknown
            return consignmentEntity.IsPruScanning || (consignmentEntity.ConsignmentId != ConsignmentEntity.UndefinedConsignmentId && consignmentEntity.ConsignmentId != Consignment.SyntheticConsignmentIdWarning);
        }

        /// <summary>
        /// Check if current user is allowed to register consignments
        /// </summary>
        /// <returns></returns>
        public void CheckConsignmentRegistrationAllowed(Consignment consignment, FlowResultScanBarcode flowResult, FlowDataScanBarcode flowData)
        {
            // According to ChangeOrder 27, if online we check if scanning consignemnt is allowed, as a property on the consignment
            bool isAllowed = consignment.IsScannedOnline ?
                consignment.IsConsignmentEventRegAllowed :
                SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsConsignmentRegistrationAllowed);

            if (isAllowed == false)
            {
                _messageHolder.Update(GlobalTexts.ScanningConsignmentNotAuthorized, MessageState.Error);

                flowResult.State = FlowResultState.Error;
            }
        }

        public void ValidateGoods(EntityMap entityMap, ConsignmentEntity consignmentEntity)
        {
            ValidateGoods(entityMap, consignmentEntity, null, null);
        }

        internal Boolean ValidateConsignment(Consignment consignment)
        {
            var request = new T20200_ValidateGoodsFromFOTRequest
            {
                GetEvents = EventCodeUtil.GetEventLevelFromProcess(),
                GetVASPayment = false,
                GetExtended = true,
                ValidationLevel = ValidateGoodsRequestValidationLevel.Goods,
                GetPackageType = true,
                LoggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationType>("", ModelUser.UserProfile),
                GetAllItems = true,
                ValidationType = ValidateGoodsRequestValidationType.Consignment,
                ConsignmentNumber = consignment.ConsignmentIdForValidateGoods,
            };

            try
            {
                var reply = GoodsEventHelper.ValidateGoodsFromFot(CommunicationClient.Instance, request);
                if (reply == null)
                    return false;

                switch (reply.ResponseInformation.ReturnState)
                {
                    case "00":
                        return true;
                    default:
                        return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool GetStatusOfExtendedCall()
        {
            return BaseModule.CurrentFlow.CurrentProcess == Process.LoadDistribTruck;
        }


        private void SetValuesForRoutingValidationLevel(T20200_ValidateGoodsFromFOTRequest request)
        {
            request.StopId = ModelMain.SelectedOperationProcess.StopId;
            request.ExternalTripId = ModelMain.SelectedOperationProcess.TripId;
            request.TMSAffiliationId = ModelUser.UserProfile.TmsAffiliation;
            request.RouteId = ModelMain.SelectedOperationProcess.RouteId;
            request.PowerUnitId = ModelMain.SelectedOperationProcess.PowerUnitId;
            request.OrgUnitIdForAction = ModelUser.UserProfile.OrgUnitId;
        }

        private string GetActionTypeForValidateGoods(T20200_ValidateGoodsFromFOTRequest request)
        {

            if (request.ValidationLevel == ValidateGoodsRequestValidationLevel.Routing)
            {
                switch (BaseModule.CurrentFlow.CurrentProcess)
                {
                    case Process.LoadLineHaul:
                    case Process.LoadDistribTruck:
                        return "LA";
                    case Process.UnloadPickUpTruck:
                    case Process.UnloadLineHaul:
                    case Process.DeliveredOtherLoc:
                        return "LO";
                }
            }
            else
            {
                if (BaseModule.CurrentFlow.CurrentProcess == Process.Pickup)
                    return "LA";
                if (BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer)
                    return "LO";
            }

            return string.Empty;
        }




        //TODO :: Write Unit Test
        /// <summary>
        /// Validate scanning against backend. If system offline or call fails we flag it as offline or unknown in LM
        /// </summary>
        public void ValidateGoods(EntityMap entityMap, ConsignmentEntity consignmentEntity, string eventCode, string loadCarrier)
        {
            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ValidateGoodsFromFot, consignmentEntity.EntityId);
            TransactionLogger.LogTiming<T20200_ValidateGoodsFromFOTReply>(transaction, new Timing(), TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Received, TimingComponent.User), new HttpCommunication());
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsScanBarcode.ValidateGoods()");
                Logger.LogAssert(entityMap != null, "EntityMap is null in ValidateGoods");

                if (entityMap != null)
                {
                    var request = new T20200_ValidateGoodsFromFOTRequest
                    {
                        GetEvents = EventCodeUtil.GetEventLevelFromProcess(),
                        EventCode = string.IsNullOrEmpty(eventCode) ? EventCodeUtil.GetEventCodeFromProcess() : eventCode,
                        GetVASPayment = true,
                        GetExtended = GetStatusOfExtendedCall(),
                        ValidationLevel = GetValidationLevel(),
                        LoadCarrierId = loadCarrier,
                        LoggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationType>("Scan entity-number", ModelUser.UserProfile)
                    };

                    request.ActionType = GetActionTypeForValidateGoods(request);

                    if (request.ValidationLevel == ValidateGoodsRequestValidationLevel.Routing)
                        SetValuesForRoutingValidationLevel(request);

                    request.LoggingInformation.MessageId = transaction.Id.ToString();

                    //Set process specific params
                    SetRequestParametersForProcess(request, consignmentEntity);

                    var consignmentItem = consignmentEntity as ConsignmentItem;
                    if (consignmentItem != null)
                    {
                        request.ConsignmentItemNumber = consignmentItem.ItemId;
                        request.ValidationType = ValidateGoodsRequestValidationType.ConsignmentItem;
                    }
                    else
                    {
                        request.ConsignmentNumber = consignmentEntity.ConsignmentIdForValidateGoods;
                        request.ValidationType = ValidateGoodsRequestValidationType.Consignment;
                    }

                    // TODO: If validating goods item for combination trip, then set following properties here
                    if (CombinationTrip != null)
                        SetRequestParametersForLoadCombinationTrip(request);

                    T20200_ValidateGoodsFromFOTReply validateGoodsResponse = null;
                    if (IsBarcodePruTracking(consignmentEntity) == false && CommunicationClient.Instance.IsConnected())
                    {
                        validateGoodsResponse = GoodsEventHelper.ValidateGoodsFromFot(CommunicationClient.Instance,
                            request);
                    }

                    if (validateGoodsResponse != null && validateGoodsResponse.Consignment != null)
                    {
                        //If consignment is coming but, consignment number is missing then
                        //set default consignment number. It is the rare case, but today it is happening
                        //in few cases so adding this check.
                        if (string.IsNullOrEmpty(validateGoodsResponse.Consignment.ConsignmentNumber))
                        {
                            validateGoodsResponse.Consignment.ConsignmentNumber = ConsignmentEntity.UndefinedConsignmentId;
                        }
                        else
                        {
                            //Trim consignment number
                            validateGoodsResponse.Consignment.ConsignmentNumber = validateGoodsResponse.Consignment.ConsignmentNumber.Trim();

                            consignmentEntity.IsKnownInLm = true;
                            consignmentEntity.IsScannedOnline = true;
                        }
                    }
                    else
                    {
                        if (consignmentEntity is ConsignmentItem)
                        {
                            var consignment = entityMap.GetConsignment(consignmentItem.ConsignmentId) ??
                                              Consignment.CreateSyntheticConsignment(consignmentItem);

                            consignmentItem.ConsignmentInstance = consignment;

                            // If goods is totally unknown, then null can be returned even if we are online
                            // Set product code accordingly
                            if (CommunicationClient.Instance.IsConnected() &&
                                CommunicationClient.Instance.LastQueryTimedOut == false)
                            {
                                consignmentEntity.ProductCode = ConsignmentEntity.UnknownProduct;
                                consignmentEntity.IsScannedOnline = true;
                            }
                        }
                    }

                    if (consignmentEntity.IsKnownInLm)
                    {
                        SetConsignmentEntityProperties(entityMap, consignmentEntity, validateGoodsResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsScanBarcode.Validate");
            }

            TransactionLogger.LogTiming<T20200_ValidateGoodsFromFOTReply>(transaction, new Timing(), TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Sending, TimingComponent.User), new HttpCommunication());
        }

        private bool SetConsignmentEntityProperties(EntityMap entityMap, ConsignmentEntity consignmentEntity, T20200_ValidateGoodsFromFOTReply validateGoodsResponse)
        {
            SetConsignmentEntityBaseProperties(consignmentEntity, validateGoodsResponse);

            //Logger.LogEvent(Severity.Debug, "ReturnState;" + validateGoodsResponse.ResponseInformation.ReturnState + " " + validateGoodsResponse.ResponseInformation.ReturnText);

            if (consignmentEntity is Consignment)
            {
                if (SetConsignmentProperties(consignmentEntity.CastToConsignment(), validateGoodsResponse) == false)
                    return false;
            }
            else if (consignmentEntity is DeliveryCode)
            {
                if (SetDeliveryCodeProperties(consignmentEntity as DeliveryCode, validateGoodsResponse, entityMap) == false)
                    return false;
            }
            else if (consignmentEntity is ConsignmentItem)
            {
                if (SetConsignmentItemProperties(consignmentEntity.CastToConsignmentItem(), validateGoodsResponse, entityMap) == false)
                    return false;
            }


            return true;
        }

        private void SetRequestParametersForLoadCombinationTrip(T20200_ValidateGoodsFromFOTRequest request)
        {
            request.ActionType = "LA";
            request.EventCode = "2T";
            request.ValidationLevel = ValidateGoodsRequestValidationLevel.Routing;
            request.OrgUnitIdForAction = ModelUser.UserProfile.OrgUnitId;
            if (CombinationTrip.LoadLineHaulWli != null)
            {
                request.ExternalTripId = CombinationTrip.LoadLineHaulWli.TripId;
                request.RouteId = CombinationTrip.LoadLineHaulWli.RouteId;
                request.PowerUnitId = CombinationTrip.LoadLineHaulWli.PowerUnitId;
            }

            if (CombinationTrip.LoadDistribWli != null)
            {
                request.CombinationTripInfo = new ValidateGoodsRequestCombinationTripInfo
                                                  {
                                                      ExternalTripId = CombinationTrip.LoadDistribWli.TripId,
                                                      RouteId = CombinationTrip.LoadDistribWli.RouteId,
                                                  };
            }
        }


        /// <summary>
        /// Set request process specific issue 
        /// </summary>
        private void SetRequestParametersForProcess(T20200_ValidateGoodsFromFOTRequest request, ConsignmentEntity consignmentEntity)
        {
            switch (BaseModule.CurrentFlow.CurrentProcess)
            {
                case Process.DelToCustomerAtPo:
                    request.GetExtended = true;
                    if (consignmentEntity is DeliveryCode)
                    {
                        request.DeliveryCode = (consignmentEntity as ConsignmentItem).DeliveryCode;
                    }
                    break;
            }
        }

        private void SetConsignmentEntityBaseProperties(ConsignmentEntity consignmentEntity, T20200_ValidateGoodsFromFOTReply reply)
        {
            consignmentEntity.IsMassRegistrationAllowed = reply.Consignment.IsMassRegistrationAllowed;
            consignmentEntity.PlannedConsignment = GetPlannedConsignmentsFromValidationReply(reply);
            consignmentEntity.ConsignmentItemCountFromLm = reply.Consignment.ConsignmentItemCount;

            //BookingNo and CustomerNumber
            consignmentEntity.OrderNumber = reply.Consignment.OrderNumber;
            consignmentEntity.CustomerId = reply.Consignment.CustomerNumber;
            consignmentEntity.ProductCategory = reply.Consignment.ProductCategory;
            consignmentEntity.ValidationCode = reply.ResponseInformation.ValidationCode;

            if (reply.ValidationStatuses != null)
            {
                consignmentEntity.ValidationEventCode = reply.ValidationStatuses.ValidationEventCode;
                consignmentEntity.ValidationCodeRouting = reply.ValidationStatuses.ValidationCodeRouting;
            }

        }

        private bool SetConsignmentProperties(Consignment consignment, T20200_ValidateGoodsFromFOTReply validateGoodsResponse)
        {
            bool result = false;

            if (IsConsignmentBlocked(validateGoodsResponse))
                consignment.IsBlocked = true;
            else
            {
                // Earlier events for consignment
                consignment.Event = validateGoodsResponse.Consignment.Event;
                consignment.Measures = new Measures();

                if (validateGoodsResponse.Consignment.VolumeSpecified)
                {

                    if (validateGoodsResponse.Consignment.VolumeUnit == VolumeUnitType.DM3.ToString())
                        consignment.Measures.VolumeDm3 = (int)validateGoodsResponse.Consignment.Volume;
                    else
                        Logger.LogEvent(Severity.Error, "Unhandled VolumeUnit in SetConsignmentProperties;" + validateGoodsResponse.Consignment.VolumeUnit);
                }
                if (validateGoodsResponse.Consignment.WeightSpecified)
                {

                    if (validateGoodsResponse.Consignment.WeightUnit == WeightUnitType.KG.ToString())
                        consignment.Measures.WeightKg = (int)validateGoodsResponse.Consignment.Weight;
                    else
                        Logger.LogEvent(Severity.Error,
                                        "Unhandled WeightUnit SetConsignmentProperties;" + validateGoodsResponse.Consignment.WeightUnit);
                }

                consignment.Vas = validateGoodsResponse.Consignment.VAS;
                consignment.ProductCode = validateGoodsResponse.Consignment.ProductCode;

                consignment.IsConsignmentEventRegAllowed = validateGoodsResponse.Consignment.IsConsignmentEventRegAllowed;
                result = true;
            }

            return result;
        }

        private bool SetDeliveryCodeProperties(DeliveryCode deliveryCode, T20200_ValidateGoodsFromFOTReply validateGoodsResponse, EntityMap entityMap)
        {
            Logger.LogEvent(Severity.Debug, "SetDeliveryCodeProperties executing");

            deliveryCode.UpdateConsignmentId(validateGoodsResponse);
            deliveryCode.ItemId = validateGoodsResponse.Consignment.ConsignmentItem[0].ConsignmentItemNumber;

            return SetConsignmentItemProperties(deliveryCode, validateGoodsResponse, entityMap);
        }


        bool SetConsignmentItemProperties(ConsignmentItem consignmentItem, T20200_ValidateGoodsFromFOTReply validateGoodsResponse, EntityMap entityMap)
        {
            Logger.LogEvent(Severity.Debug, "SetConsignmentItemProperties executing");

            //Set consignment instance in consignmentitem, which will be used while storing the
            //consignment item in entity map
            ConnectConsignment(consignmentItem, validateGoodsResponse, entityMap);

            if (validateGoodsResponse.Consignment.ConsignmentItem == null)
            {
                // This was scanned online, but was unknown
                consignmentItem.ProductCode = ConsignmentEntity.UnknownProduct;
            }
            else
            {
                var consignmentItemReply = validateGoodsResponse.Consignment.ConsignmentItem[0];

                //Case of blockage for actor
                if (IsConsignmentItemBlocked(consignmentItemReply))
                {
                    consignmentItem.IsBlocked = true;
                    return false;
                }

                consignmentItem.Shelf = string.IsNullOrEmpty(consignmentItemReply.ShelfLocation)
                                            ? string.Empty
                                            : consignmentItemReply.ShelfLocation.Trim();
                consignmentItem.ShelfLocationOrgUnit = string.IsNullOrEmpty(consignmentItemReply.ShelfLocationOrgUnitId)
                                                           ? string.Empty
                                                           : consignmentItemReply.ShelfLocationOrgUnitId.Trim();

                // Earlier events for consignment item
                consignmentItem.Event = consignmentItemReply.Event;
                consignmentItem.PlannedDeliveryDate = consignmentItemReply.PlannedDeliveryDate;
                consignmentItem.Measures = new Measures();

                if (consignmentItemReply.VolumeSpecified)
                {
                    if (consignmentItemReply.VolumeUnit == VolumeUnitType.DM3.ToString())
                        consignmentItem.Measures.VolumeDm3 = (int)consignmentItemReply.Volume;
                    else
                        Logger.LogEvent(Severity.Error, "Unhandled VolumeUnit in SetConsignmentItemProperties;" + consignmentItemReply.VolumeUnit);
                }
                if (consignmentItemReply.WeightSpecified)
                {
                    if (consignmentItemReply.WeightUnit == WeightUnitType.KG.ToString())
                        consignmentItem.Measures.WeightKg = (int)consignmentItemReply.Weight;
                    else
                        Logger.LogEvent(Severity.Error, "Unhandled WeightUnit in SetConsignmentItemProperties;" + consignmentItemReply.WeightUnit);
                }

                if (consignmentItemReply.HLWUnit == HLWUnitType.CM.ToString())
                {
                    if (consignmentItemReply.HeightSpecified)
                        consignmentItem.Measures.HeightCm = (int)consignmentItemReply.Height;
                    if (consignmentItemReply.LengthSpecified)
                        consignmentItem.Measures.LengthCm = (int)consignmentItemReply.Length;
                    if (consignmentItemReply.WidthSpecified)
                        consignmentItem.Measures.WidthCm = (int)consignmentItemReply.Width;
                }
                else if (consignmentItemReply.HLWUnit == HLWUnitType.DM.ToString())
                {
                    if (consignmentItemReply.HeightSpecified)
                        consignmentItem.Measures.HeightCm = (int)(consignmentItemReply.Height * 10);
                    if (consignmentItemReply.LengthSpecified)
                        consignmentItem.Measures.LengthCm = (int)(consignmentItemReply.Length * 10);
                    if (consignmentItemReply.WidthSpecified)
                        consignmentItem.Measures.WidthCm = (int)(consignmentItemReply.Width * 10);
                }
                else
                {
                    Logger.LogEvent(Severity.Error, "Unhandled HLWUnit in SetConsignmentItemProperties;" + consignmentItemReply.HLWUnit);
                }

                consignmentItem.ProductCode = consignmentItemReply.ProductCode;
                consignmentItem.Vas = consignmentItemReply.VAS;
                consignmentItem.Weight = consignmentItemReply.Weight;
                consignmentItem.Volume = consignmentItemReply.Volume;
            }


            return true;
        }

        private bool IsConsignmentBlocked(T20200_ValidateGoodsFromFOTReply validateGoodsResponse)
        {
            if (validateGoodsResponse.Consignment.IsActorBlocked)
            {
                _messageHolder.Update(string.Format(GlobalTexts.ActorBlocked, validateGoodsResponse.GetConsignmentNumber(), validateGoodsResponse.Consignment.ActorNumber), MessageState.Error);
            }
            return validateGoodsResponse.Consignment.IsActorBlocked;
        }

        private bool IsConsignmentItemBlocked(ValidationResponseConsignmentConsignmentItem consignmentItemResponse)
        {
            bool result = false;
            if (consignmentItemResponse.StopCode == "01" && IsStopCodeActiveForProcess())
            {
                _messageHolder.ErrorInfo = ErrorInfo.ShipmentStopped;
                _messageHolder.Update(string.Format(GlobalTexts.ScannedItemStopped, consignmentItemResponse.ConsignmentItemNumber,
                    consignmentItemResponse.StopCode), MessageState.Error);
                result = true;
            }
            return result;
        }

        // Stop code should only abort scanning for certain processes
        private bool IsStopCodeActiveForProcess()
        {
            var parentFlow = BaseModule.ParentFlow;
            if (parentFlow != null)
                return parentFlow.IsStopCodeActiveForProcess();
            return false;
        }


        public PlannedConsignmentsType GetPlannedConsignmentsFromValidationReply(T20200_ValidateGoodsFromFOTReply validationReply)
        {
            Logger.LogEvent(Severity.Debug, "Executing command CommandsHandleScannedBarcodes.GetPlannedConsignmentsFromValidationReply()");
            try
            {
                ConsignmentItemType[] consignmentItems = null;

                if (validationReply.Consignment.ConsignmentItem != null)
                {
                    consignmentItems = validationReply.Consignment.ConsignmentItem.Select(consignmentItem => new ConsignmentItemType
                    {
                        ConsignmentItemNumber = consignmentItem.ConsignmentItemNumber
                    }).ToArray();
                }

                var plannedConsignment = new PlannedConsignmentsType
                {
                    ConsignmentNumber = validationReply.Consignment.ConsignmentNumber,
                    ConsignmentItemCount = validationReply.Consignment.ConsignmentItemCount,

                    OrderNumber = validationReply.Consignment.OrderNumber,
                    CustomerId = validationReply.Consignment.CustomerNumber,
                    ConsignmentItem = consignmentItems
                };

                GetConsigneeAndConsignorDetails(validationReply, plannedConsignment);
                return plannedConsignment;
            }

            catch (Exception e)
            {
                Logger.LogEvent(Severity.Error, "Unexpected error in execution of function GetPlannedConsignmentsFromValidationReply: " + e.Message);
                return null;
            }
        }

        private void GetConsigneeAndConsignorDetails(T20200_ValidateGoodsFromFOTReply validateGoodsReply, PlannedConsignmentsType plannedConsignment)
        {
            try
            {
                var consignee = validateGoodsReply.Consignment.Consignee;
                if (consignee != null)
                {
                    plannedConsignment.Consignee = new ConsigneeType
                    {
                        Address1 = consignee.Address1,
                        Address2 = consignee.Address2,
                        PostalCode = consignee.PostalCode,
                        PostalName = consignee.PostalName,
                        CountryCode = consignee.CountryCode,
                        DeliveryAddress1 = consignee.DeliveryAddress != null ? consignee.DeliveryAddress.Address1 : string.Empty,
                        DeliveryAddress2 = consignee.DeliveryAddress != null ? consignee.DeliveryAddress.Address2 : string.Empty,
                        DeliveryCountryCode = consignee.DeliveryAddress != null ? consignee.DeliveryAddress.CountryCode : string.Empty,
                        DeliveryPostalCode = consignee.DeliveryAddress != null ? consignee.DeliveryAddress.PostalCode : string.Empty,
                        DeliveryPostalName = consignee.DeliveryAddress != null ? consignee.DeliveryAddress.PostalName : string.Empty,
                        Name1 = consignee.Name1,
                        Name2 = consignee.Name2,
                        PhoneNumber = consignee.PhoneNumber
                    };
                }

                var consignor = validateGoodsReply.Consignment.Consignor;
                if (consignor != null)
                {
                    plannedConsignment.Consignor = new ConsignorType
                    {
                        Name1 = consignor.Name1,
                        Name2 = consignor.Name2,
                        Address1 = consignor.Address1,
                        Address2 = consignor.Address2,
                        CountryCode = consignor.CountryCode,
                        PostalCode = consignor.PostalCode,
                        PostalName = consignor.PostalName
                    };
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "ActionCommandsScanBarcode.GetConsigneeAndConsignorDetails");
            }
        }

        /// <summary>
        /// If trip and stop is known, we can retrieve extra information by using validation level Plan
        /// </summary>
        public static ValidateGoodsRequestValidationLevel GetValidationLevel()
        {
            var validationLevel = ModelMain.SelectedOperationProcess.WorkListItemType == WorkListItemType.RouteCarrierTrip ?
                ValidateGoodsRequestValidationLevel.Routing : ValidateGoodsRequestValidationLevel.Goods;

            return validationLevel;
        }

        private void ConnectConsignment(ConsignmentItem consignmentItem, T20200_ValidateGoodsFromFOTReply validateGoodsReply, EntityMap entityMap)
        {
            Logger.LogEvent(Severity.Debug, "ConnectConsignment '{0}'", consignmentItem.ItemId);

            var consignment = entityMap.GetConsignment(validateGoodsReply.Consignment.ConsignmentNumber);
            if (consignment == null)
            {
                consignment = new Consignment(validateGoodsReply)
                {
                    ConsignmentItemCountFromLm = consignmentItem.ConsignmentItemCountFromLm,
                    Vas = validateGoodsReply.Consignment.VAS,
                    ConsignmentItemsMap = new ConsignmentItemMap(consignmentItem),
                    EntityCreatedOn = DateTime.Now

                };
            }
            consignmentItem.ConsignmentInstance = consignment;
        }



        /// <summary>
        /// Display warning sound 
        /// </summary>
        /// <param name="consignmentEntity"></param>
        private static void DisplayWarningMessage(ConsignmentEntity consignmentEntity)
        {
            GuiCommon.ShowModalDialog(GlobalTexts.ConsignmentItem,
                String.Format(GlobalTexts.ConsignmentItemRegisteredWithoutConsignment, consignmentEntity.EntityDisplayId),
                Severity.Info, GlobalTexts.Ok);
        }

        //TODO :: Write Unit Test
        /// <summary>
        ///  Returns true if we consignment number is required
        /// </summary>
        /// <returns></returns>
        public FlowStateScanBarcode CheckConsignmentNumberRequired(ConsignmentEntity consignmentEntity, bool isSkippedOnceForPickup)
        {
            var result = FlowStateScanBarcode.ThisFlowComplete;

            var consignmentItem = consignmentEntity.CastToConsignmentItem();

            var tmiSetting = consignmentItem.IsScannedOnline
                                 ? TmiSettings.IsOnlineConsignmentNumberRequired
                                 : TmiSettings.IsOfflineConsignmentNumberRequired;

            var value = SettingDataProvider.Instance.GetTmiValue<string>(tmiSetting) ?? string.Empty;
            switch (value.ToLower())
            {
                case "true":
                case "yes":
                    result = FlowStateScanBarcode.FlowScanConsignment;
                    break;

                case "false":
                case "no":
                    // Check if consignment number is missing
                    break;

                case "optional":
                    if (isSkippedOnceForPickup)
                    {
                        //Play Warning Sound and Message
                        SoundUtil.Instance.PlayWarningSound();
                        DisplayWarningMessage(consignmentItem);
                    }
                    else
                    {
                        result = FlowStateScanBarcode.FlowScanWaybill;
                    }
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Unknown state for TMIValue 'IsOfflineConsignmentNumberRequired': {0}", value);
                    break;
            }
            return result;
        }


        public static bool ValidateLoadCarrier(LoadCarrier loadCarrier)
        {
            return ValidateLoadCarrier(loadCarrier, null, string.Empty, false, string.Empty);
        }

        public static bool ValidateLoadCarrier(LoadCarrier loadCarrier, FlowResultScanBarcode flowResult, string logicalLoadCarrierType, bool doSetEventType, string parentLoadCarrierId)
        {
            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.ValidateGoodsFromFot, loadCarrier.LoadCarrierId);
            TransactionLogger.LogTiming<T20277_ValidateLoadCarrierFromFOTReply>(transaction, new Timing(), TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Received, TimingComponent.User), new HttpCommunication());

            bool result = false;
            if (CommunicationClient.Instance.IsConnected())
            {

                var eventType = new CommunicationEntity.ValidationLC.EventType
                {
                    EventType1 = doSetEventType ? EventCodeUtil.GetEventCodeFromProcessForLoadCarrier() : string.Empty,
                    LoadCarrierParentId = parentLoadCarrierId,
                    OrgUnitIdForEvent = ModelUser.UserProfile.OrgUnitId,
                    Timestamp = DateTime.Now
                };

                var loggingInformation = LoggingInformation.GetLoggingInformation<LoggingInformationTypeForLoadCarrier>("Vaidate loadcarrier", ModelUser.UserProfile);
                loggingInformation.ProcessName = "ValidateLoadCarrier";

                var request = new T20277_ValidateLoadCarrierFromFOTRequest
                {
                    LoadCarrierId = loadCarrier.LoadCarrierId,
                    RequestType = ReqRespType.LMK,
                    Event = doSetEventType ? eventType : null,
                    LoggingInformation = loggingInformation,
                    LogicalLoadCarrierType = string.IsNullOrEmpty(logicalLoadCarrierType) ? "" : logicalLoadCarrierType,
                    PowerUnitId = ""
                };

                request.LoggingInformation.MessageId = transaction.Id.ToString();
                T20277_ValidateLoadCarrierFromFOTReply reply = GoodsEventHelper.ValidateLoadCarrierFromFot(CommunicationClient.Instance, request);
                if (reply == null)
                {
                    if (flowResult != null)
                        flowResult.State = FlowResultState.Ok;
                }
                else
                {
                    loadCarrier.IsScannedOnline = true;

                    if (reply.LogicalLoadCarrierDetails != null)
                        loadCarrier.LogicalLoadCarrierDetails = reply.LogicalLoadCarrierDetails;
                    loadCarrier.PhysicalLoadCarrier = reply.PhysicalLoadCarrier;
                    loadCarrier.IsKnownInLm = reply.DidValidateOk;

                    result = reply.DidValidateOk;
                }
            }


            TransactionLogger.LogTiming<T20277_ValidateLoadCarrierFromFOTReply>(transaction, new Timing(), TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Sending, TimingComponent.User), new HttpCommunication());

            return result;
        }


        //TODO :: Write Unit Test
        public static ConsignmentEntity CreateConsignmentEntity(BarcodeData barcodeData)
        {
            ConsignmentEntity result;

            if (barcodeData.BarcodeType == BarcodeType.LoadCarrier)
            {
                result = new LoadCarrier(barcodeData.BarcodeLoadCarrier.LoadCarrierId)
                {
                    LoadCarrierId = barcodeData.BarcodeLoadCarrier.LoadCarrierId

                };
                return result;
            }

            if (barcodeData.BarcodeType == BarcodeType.TransportCarrier)
            {
                result = new LoadCarrier(barcodeData.BarcodeTransportCarrier.TransportCarrierId)
                {
                    LoadCarrierId = barcodeData.BarcodeTransportCarrier.TransportCarrierId
                };
                return result;
            }

            if (barcodeData.BarcodeType == BarcodeType.DeliveryCode)
            {
                result = new DeliveryCode
                {
                    DeliveryCode = barcodeData.DeliveryCode,
                    ConsignmentInstance = new Consignment(ConsignmentEntity.UndefinedConsignmentId)
                };
            }
            else if ((barcodeData.BarcodeType & BarcodeType.ConsignmentNumber) != BarcodeType.Unknown)
            {
                result = new Consignment(barcodeData.ConsignmentId);
            }
            else
            {
                result = new ConsignmentItem
                {
                    ItemId = barcodeData.ConsignmentItemId,
                    ConsignmentInstance = new Consignment(barcodeData.ConsignmentId)
                };
            }

            result.BarcodeSubType = barcodeData.BarcodeSubType;

            return result;
        }

        internal bool IsConsignment(ConsignmentEntity consignmentEntity)
        {
            return consignmentEntity is Consignment;
        }

        internal bool IsScanningStopped(ConsignmentEntity consignmentEntity, FlowResultScanBarcode flowResult)
        {
            bool result = false;
            if (consignmentEntity.IsBlocked)
            {
                flowResult.State = FlowResultState.Error;
                flowResult.ErrorInfo = ErrorInfo.ShipmentStopped;
                result = true;
            }
            return result;
        }

        public FlowStateScanBarcode GetNextStateForConsignmentEntity(ConsignmentEntity currentConsignmentEntity)
        {
            if (currentConsignmentEntity is LoadCarrier)
                return FlowStateScanBarcode.ActionIsLoadCarrierScannedBefore;

            if (currentConsignmentEntity is DeliveryCode)
                return FlowStateScanBarcode.ValidateDeliveryCode;

            return FlowStateScanBarcode.ActionIsScannedBefore;
        }

        /// <summary>
        /// method used for check scan consignment item number with pdf consignment item  number..
        /// </summary>
        internal bool IsScanConsignmentItemNumberSameInPdf(ConsignmentItem consignmentItem, BarcodeData currentScanning)
        {
            bool isValid = false;
            if (consignmentItem != null && currentScanning != null &&
                (consignmentItem.ItemId != null && currentScanning.ConsignmentItemId != null))
                isValid = consignmentItem.ItemId == currentScanning.ConsignmentItemId;

            return isValid;
        }

        internal void ShowWarningForConsignmentNotSameInPdf(string text, Severity severity)
        {
            GuiCommon.ShowModalDialog(GlobalTexts.ConsignmentItem, text, severity, GlobalTexts.Ok);
        }
    }
}

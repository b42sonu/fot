﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls;
using Resco.Controls.CommonControls;
using Resco.Controls.OutlookControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    partial class FormScanBarcode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtScannedNumber = new PreCom.Controls.PreComInput2();
            this.pnlConfirmMessage = new System.Windows.Forms.Panel();
            this.buttonUnloadChangeCarrier = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonCancelChangeCarrier = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonLoadChangeCarrier = new Resco.Controls.OutlookControls.ImageButton();
            this.lblConfirmInfo = new System.Windows.Forms.Label();
            this.lblConfirmInfoHeading = new System.Windows.Forms.Label();
            this.picConfirmInfo = new System.Windows.Forms.PictureBox();
            this.lblNewCarrier = new System.Windows.Forms.Label();
            this.txtNewCarrier = new System.Windows.Forms.TextBox();
            this.labelTask = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblNumberOfItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelItemCounter = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblNoOfLoadCarriers = new Resco.Controls.CommonControls.TransparentLabel();
            this._messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblShelfLocation = new Resco.Controls.CommonControls.TransparentLabel();

            this.pnlConfirmMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonUnloadChangeCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancelChangeCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonLoadChangeCarrier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelItemCounter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShelfLocation)).BeginInit();

            this.SuspendLayout();
            
            // 
            // pnlConfirmMessage
            // 
            this.pnlConfirmMessage.BackColor = System.Drawing.Color.White;
            this.pnlConfirmMessage.Controls.Add(this.buttonUnloadChangeCarrier);
            this.pnlConfirmMessage.Controls.Add(this.buttonCancelChangeCarrier);
            this.pnlConfirmMessage.Controls.Add(this.buttonLoadChangeCarrier);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblConfirmInfoHeading);
            this.pnlConfirmMessage.Controls.Add(this.picConfirmInfo);
            this.pnlConfirmMessage.Controls.Add(this.lblNewCarrier);
            this.pnlConfirmMessage.Controls.Add(this.txtNewCarrier);
            this.pnlConfirmMessage.Location = new System.Drawing.Point(20, 20);
            this.pnlConfirmMessage.Name = "pnlConfirmMessage";
            this.pnlConfirmMessage.Size = new System.Drawing.Size(440, 380);
            this.pnlConfirmMessage.Visible = false;
            this.pnlConfirmMessage.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlConfirmMessagePaint);
            // 
            // buttonUnloadChangeCarrier
            // 
            this.buttonUnloadChangeCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonUnloadChangeCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonUnloadChangeCarrier.ForeColor = System.Drawing.Color.White;
            this.buttonUnloadChangeCarrier.Location = new System.Drawing.Point(310, 300);
            this.buttonUnloadChangeCarrier.Name = "buttonUnloadChangeCarrier";
            this.buttonUnloadChangeCarrier.Size = new System.Drawing.Size(100, 50);
            this.buttonUnloadChangeCarrier.TabIndex = 30;
            
            this.buttonUnloadChangeCarrier.Click += new System.EventHandler(this.BtnUnloadClick);
            // 
            // buttonCancelChangeCarrier
            // 
            this.buttonCancelChangeCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancelChangeCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancelChangeCarrier.ForeColor = System.Drawing.Color.White;
            this.buttonCancelChangeCarrier.Location = new System.Drawing.Point(20, 300);
            this.buttonCancelChangeCarrier.Name = "buttonCancelChangeCarrier";
            this.buttonCancelChangeCarrier.Size = new System.Drawing.Size(100, 50);
            this.buttonCancelChangeCarrier.TabIndex = 30;
            
            this.buttonCancelChangeCarrier.Click += new System.EventHandler(this.ClickCancelChangeCarrier);
            // 
            // buttonLoadChangeCarrier
            // 
            this.buttonLoadChangeCarrier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonLoadChangeCarrier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonLoadChangeCarrier.ForeColor = System.Drawing.Color.White;
            this.buttonLoadChangeCarrier.Location = new System.Drawing.Point(140, 300);
            this.buttonLoadChangeCarrier.Name = "buttonLoadChangeCarrier";
            this.buttonLoadChangeCarrier.Size = new System.Drawing.Size(150, 50);
            this.buttonLoadChangeCarrier.TabIndex = 30;
            
            this.buttonLoadChangeCarrier.Click += new System.EventHandler(this.BtnLoadClick);
            // 
            // lblConfirmInfo
            // 
            this.lblConfirmInfo.Location = new System.Drawing.Point(25, 130);
            this.lblConfirmInfo.Name = "lblConfirmInfo";
            this.lblConfirmInfo.Size = new System.Drawing.Size(370, 60);
            this.lblConfirmInfo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            // 
            // lblConfirmInfoHeading
            // 
            this.lblConfirmInfoHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConfirmInfoHeading.Location = new System.Drawing.Point(25, 100);
            this.lblConfirmInfoHeading.Name = "lblConfirmInfoHeading";
            this.lblConfirmInfoHeading.Size = new System.Drawing.Size(236, 30);
            
            // 
            // picConfirmInfo
            // 
            this.picConfirmInfo.Location = new System.Drawing.Point(195, 20);
            this.picConfirmInfo.Name = "picConfirmInfo";
            this.picConfirmInfo.Size = new System.Drawing.Size(50, 50);
            this.picConfirmInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblNewCarrier
            // 
            this.lblNewCarrier.Location = new System.Drawing.Point(25, 220);
            this.lblNewCarrier.Name = "lblNewCarrier";
            this.lblNewCarrier.Size = new System.Drawing.Size(150, 30);
            
            // 
            // txtNewCarrier
            // 
            this.txtNewCarrier.Location = new System.Drawing.Point(200, 210);
            this.txtNewCarrier.Name = "txtNewCarrier";
            this.txtNewCarrier.Size = new System.Drawing.Size(200, 41);
            this.txtNewCarrier.TabIndex = 35;
            
            
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelTask);
            this.touchPanel.Controls.Add(this.labelItemCounter);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.pnlConfirmMessage);
            this.touchPanel.Controls.Add(this.lblNoOfLoadCarriers);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this._messageControl);
            this.touchPanel.Controls.Add(this.lblNumberOfItems);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelItemCounter
            // 
            this.labelItemCounter.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelItemCounter.Location = new System.Drawing.Point(400, 46);
            this.labelItemCounter.Name = "labelItemCounter";
            this.labelItemCounter.Size = new System.Drawing.Size(0, 0);
            this.labelItemCounter.Visible = false;
     
     this.lblShelfLocation.Location = new System.Drawing.Point(131+19, 348);
            this.lblShelfLocation.Name = "lblShelfLocation";
            this.lblShelfLocation.Size = new System.Drawing.Size(14, 29);
        

            this.touchPanel.Controls.Add(this.lblShelfLocation);

            
            // 
            // labelModuleName
            // 
            //this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            // 
            // labelTask
            // 
            this.labelTask.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelTask.Location = new System.Drawing.Point(22, 46);
            this.labelTask.Name = "labelTask";
            this.labelTask.Size = new System.Drawing.Size(375, 27);

            // 
            // editConsignmentNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(22, 78);
            this.txtScannedNumber.Name = "editConsignmentNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(436, 30);
            this.txtScannedNumber.TabIndex = 23;
            this.txtScannedNumber.TextTranslation = false;

            // 
            // lblNoOfLoadCarriers
            // 
            this.lblNoOfLoadCarriers.Location = new System.Drawing.Point(161 + 19, 348);
            this.lblNoOfLoadCarriers.Name = "lblNoOfLoadCarriers";
            this.lblNoOfLoadCarriers.Size = new System.Drawing.Size(14, 29);
            
            // 
            // _messageControl
            // 
            this._messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._messageControl.Location = new System.Drawing.Point(22, 165);
            this._messageControl.Name = "_messageControl";
            this._messageControl.Size = new System.Drawing.Size(438, 110);
            this._messageControl.TabIndex = 3;

            //lblNumberOfitems
            // 
            this.lblNumberOfItems.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblNumberOfItems.Location = new System.Drawing.Point(21, 380);
            this.lblNumberOfItems.Name = "lblNumberOfitems";
            this.lblNumberOfItems.Size = new System.Drawing.Size(245, 30);


            // 
            // FormScanBarcode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormScanBarcode";
            this.pnlConfirmMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonUnloadChangeCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancelChangeCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonLoadChangeCarrier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTask)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelItemCounter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblShelfLocation)).EndInit();

            this.ResumeLayout(false);

        }

        #endregion

        private MessageControl _messageControl;
        private PreCom.Controls.PreComInput2 txtScannedNumber;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelTask;
        private Resco.Controls.CommonControls.TransparentLabel labelItemCounter;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblNumberOfItems;
        private TransparentLabel lblNoOfLoadCarriers;
        private TransparentLabel lblShelfLocation;

        #region Work List Pop related

        private Panel pnlConfirmMessage;
        private PictureBox picConfirmInfo;
        private Label lblConfirmInfo;
        private Label lblConfirmInfoHeading;
        private Resco.Controls.OutlookControls.ImageButton buttonUnloadChangeCarrier;
        private Resco.Controls.OutlookControls.ImageButton buttonCancelChangeCarrier;
        private Resco.Controls.OutlookControls.ImageButton buttonLoadChangeCarrier;
        private Label lblNewCarrier;
        private TextBox txtNewCarrier;

        #endregion
    }

    // So Show function will create a separate instance
    //public class FormScanConsignment : FormScanBarcode
    //{
        
    //}
}
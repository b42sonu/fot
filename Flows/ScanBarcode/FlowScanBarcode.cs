﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterWaybill;
using Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Barcode;

using Process = Com.Bring.PMP.PreComFW.Shared.Storage.Process;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    public enum FlowStateScanBarcode
    {
        ActionIsScannedBefore,
        ActionIsLoadCarrierScannedBefore,
        ActionIsConsignmentRegistrationAllowed,
        ActionIsValidConsignmentNumber,
        ActionValidateBarcode,
        ActionCreateConsignmentEntity,
        ActionWasScannedOffline,
        ActionIsConsignment,
        ActionIsConsignmentNumberForItemKnown,
        ActionCheckConsignmentNumberRequired,
        ActionValidateGoods,
        ActionIsScanningStopped,
        ActionBackFromFlowScanConsignment,
        ActionBackFromRegisterWaybill,
        ActionBackFromFlowLoadUnloadCarrier,
        ActionValidateLoadCarrier,
        ActionIsWorkListRequired,
        ActionStoreLoadCarrierWhenLoading,
        ActionStoreLoadCarrierWhenUnloading,
        ActionGetExistingRbtWorkListItem,
        ActionGetExistingLoadCarrierWorkListItem,
        ActionDecideNextStateBasedOnScan,
        ActionGetFlowResultForScanning,
        ActionShowPopUpForScannedLoadCarrier,
        ActionDecideViewScanBarcode,
        ActionValidateLoadCarrierFromServer,
        ValidateDeliveryCode,

        ViewCommands,
        ViewScanBarcode,
        ViewShowNewCarrierPopUp,

        FlowScanConsignment,
        FlowScanWaybill,
        FlowLoadUnloadCarrier,
        ThisFlowComplete,
    }

    public class FlowDataScanBarcode : BaseFlowData
    {
        private bool _cancelButtonDisabledWhenScanned;

        public FlowDataScanBarcode(string flowName, MessageHolder messageHolder, BarcodeType validTypes, EntityMap entityMap)
        {
            HeaderText = flowName;
            MessageHolder = messageHolder;
            EntityMap = entityMap;
            ValidBarcodeTypes = validTypes;
            PlaySuccesSound = true;
        }

        public string EventCode { get; set; }
        public string LoadCarrier { get; set; }
        public bool PlaySuccesSound { get; set; }
        public bool SkipReconcilliation { get; set; }
        public string ScanText { get; set; }

        public bool HaveCustomForm { get; set; }

        public MessageHolder MessageHolder { get; private set; }
        public EntityMap EntityMap { get; private set; }
        public bool IsWorkListRequired { get; set; }
        public bool IsRegisterWaybillSkippedForPickup;
        public bool NoValidationOfLoadCarrierFromServer { get; set; }
        public BarcodeType ValidBarcodeTypes { get; private set; }
        public bool ShowShelf { get; set; }
        public bool ShowNoOfItems { get; set; }
        public bool ShowNoOfLoadCarriers { get; set; }
        public bool ShowDeviationButton { get; set; }
        public ConsignmentEntity LastScanning { get; set; }
        public CombinationTrip CurrentCombinationTrip { get; set; }
        public bool StartedFromOperationList { get; set; }
        public bool IsLoadCarrierDeviationAllowed { get; set; }
        public bool WantLoadCarrierDetails;
        public bool CancelButtonDisabledWhenScanned
        {
            get { return _cancelButtonDisabledWhenScanned && EntityMap.ScannedConsignmentItemsTotalCount > 0; }
            set { _cancelButtonDisabledWhenScanned = value; }
        }

        public bool SkipValidateGoods;  //this is used when scanning content only validated client side no validate goods required for server side validation 
        public bool AlwaysValidateGoods;  //this is used when scanning content always should be validated (event hough it is scanned before and is in entitymap, ie. duplicate)
    }

    public class FlowResultScanBarcode : BaseFlowResult
    {
        public ConsignmentEntity CurrentConsignmentEntity { get; set; }
        public bool ChangeCarrier { get; set; }
        public string LoadCarrierId { get; set; }
        public BarcodeType BarcodeType { get; set; }
        public BarcodeSubType BarcodeSubType { get; set; }
        public bool StartLoading { get; set; }
        public bool StartUnloading { get; set; }
        public bool IsSkippedOnceForPickup { get; set; }
        public BarcodeRouteCarrierTrip BarcodeRouteCarrierTrip { get; set; }
        public bool LoadCarrerIntoWorkListItem { get; set; }
        public bool ActivateWorkListItem { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public string ShelfId { get; set; }
        public string OrgUnitId { get; set; }
        public string RouteId { get; set; }
        public string DeliveryCode { get; set; }
        public string LicenceId { get; set; }
        public bool ShowTaskDetail { get; set; }  //this varibale if true then start FlowStopDetailsAmphora
        public ErrorInfo ErrorInfo { get; set; }
    }

    // US 76g: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7933803
    public class FlowScanBarcode : BaseFlow
    {
        public FotScannerOutput FotScannerOutput;
        private ActionCommandsScanBarcode _actionCommands;
        private readonly ViewCommandsScanBarcode _viewCommands;
        public readonly FlowResultScanBarcode FlowResult;
        private FlowDataScanBarcode _flowData;
        private BarcodeData _currentScanning;
        private ActionCommandsSelectCarrier _actionCommandSelectCarrier;
        private ConsignmentEntity _currentConsignmentEntity;
        public bool IsMainScanningBackClicked { get; set; }

        public FlowScanBarcode()
        {
            _viewCommands = new ViewCommandsScanBarcode();
            FlowResult = new FlowResultScanBarcode();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            IsMainScanningBackClicked = false;
            Logger.LogEvent(Severity.Debug, "Initializing FlowScanBarcode");
            _flowData = (FlowDataScanBarcode)flowData;

            _actionCommands = new ActionCommandsScanBarcode(_flowData.MessageHolder) { CombinationTrip = _flowData.CurrentCombinationTrip };

            _actionCommandSelectCarrier = new ActionCommandsSelectCarrier();
            ExecuteState(FlowStateScanBarcode.ViewScanBarcode);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateScanBarcode)state);
        }

        public void ExecuteState(FlowStateScanBarcode stateScanBarcode)
        {
            if (stateScanBarcode > FlowStateScanBarcode.ViewCommands)
            {
                ExecuteViewState(stateScanBarcode);
            }
            else
            {
                ExecuteActionState(stateScanBarcode);
            }
        }

        private void ExecuteViewState(FlowStateScanBarcode state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowScanBarcode;" + state);
                switch (state)
                {
                    case FlowStateScanBarcode.ViewScanBarcode:
                        _viewCommands.ScanBarcode(ScanViewEventHandler, ref _flowData, _currentScanning, false);
                        break;

                    case FlowStateScanBarcode.ViewShowNewCarrierPopUp:
                        _flowData.ShowNoOfItems = false;
                        _flowData.ShowNoOfLoadCarriers = false;
                        _viewCommands.ScanBarcode(ScanViewEventHandler, ref _flowData, _currentScanning, true);
                        break;

                    case FlowStateScanBarcode.FlowScanConsignment:
                        var flowDataScanConsignment = new FlowDataScanConsignment
                        {
                            HeaderText = _flowData.HeaderText,
                            ConsignmentItem = _currentConsignmentEntity.CastToConsignmentItem(),
                            ScanText = _flowData.ScanText
                        };

                        StartSubFlow<FlowScanConsignment>(flowDataScanConsignment, (int)FlowStateScanBarcode.ActionBackFromFlowScanConsignment, Process.Inherit);
                        break;

                    case FlowStateScanBarcode.FlowScanWaybill:
                        var flowDataRegisterWaybill = new FlowDataRegisterWaybill
                        {
                            HeaderText = _flowData.HeaderText,
                            ConsignmentItem = _currentConsignmentEntity.CastToConsignmentItem(),

                        };
                        StartSubFlow<FlowRegisterWaybill>(flowDataRegisterWaybill, (int)FlowStateScanBarcode.ActionBackFromRegisterWaybill, Process.Inherit);
                        break;
                    case FlowStateScanBarcode.ThisFlowComplete:
                        EndFlow();
                        BusyUtil.Reset();
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "Encountered unhandled state {0} in {1}", state, GetType().Name);
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowScanBarcode.ExecuteViewState");
                FlowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
            }
        }



        /// <summary>
        /// This method executes the stateReconcilliation for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateScanBarcode state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowScanBarcode;" + state);
                    bool result;
                    switch (state)
                    {
                        case FlowStateScanBarcode.ActionValidateBarcode:
                            result = _actionCommands.ValidateBarcode(FotScannerOutput, _flowData.ValidBarcodeTypes, out _currentScanning, _flowData.HaveCustomForm);
                            state = result ? FlowStateScanBarcode.ActionDecideNextStateBasedOnScan : FlowStateScanBarcode.ActionDecideViewScanBarcode;
                            break;

                        case FlowStateScanBarcode.ActionDecideViewScanBarcode:

                            state = _flowData.HaveCustomForm
                                        ? FlowStateScanBarcode.ActionGetFlowResultForScanning
                                        : FlowStateScanBarcode.ViewScanBarcode;
                            break;

                        case FlowStateScanBarcode.ActionDecideNextStateBasedOnScan:
                            state = _actionCommands.DecideStateBasedOnScan(_currentScanning.BarcodeType, CurrentProcess);
                            break;

                        case FlowStateScanBarcode.ActionGetFlowResultForScanning:
                            _actionCommands.GetFlowResultForScanning(FlowResult, _currentScanning, _flowData.ValidBarcodeTypes);
                            state = FlowStateScanBarcode.ThisFlowComplete;
                            break;

                        case FlowStateScanBarcode.ActionCreateConsignmentEntity:
                            _currentConsignmentEntity = ActionCommandsScanBarcode.CreateConsignmentEntity(_currentScanning);

                            if (_flowData != null && _flowData.SkipValidateGoods)
                            {
                                state = FlowStateScanBarcode.ThisFlowComplete;
                                FlowResult.State = FlowResultState.Ok;
                            }
                            else
                                state = _actionCommands.GetNextStateForConsignmentEntity(_currentConsignmentEntity);
                            break;

                        case FlowStateScanBarcode.ActionIsScannedBefore:
                            result = _flowData.AlwaysValidateGoods == false &&
                                _actionCommands.IsScannedBefore(_flowData.EntityMap, ref _currentConsignmentEntity);
                            if (result)
                            {
                                state = FlowStateScanBarcode.ThisFlowComplete;
                                FlowResult.State = FlowResultState.Duplicate;
                            }
                            else
                                state = FlowStateScanBarcode.ActionValidateGoods;
                            break;

                        case FlowStateScanBarcode.ValidateDeliveryCode:
                            _actionCommands.ValidateGoods(_flowData.EntityMap, _currentConsignmentEntity, _flowData.EventCode, _flowData.LoadCarrier);
                            state = FlowStateScanBarcode.ThisFlowComplete;
                            break;

                        case FlowStateScanBarcode.ActionValidateGoods:
                            // Validate and retrieve missing info 
                            _actionCommands.ValidateGoods(_flowData.EntityMap, _currentConsignmentEntity, _flowData.EventCode, _flowData.LoadCarrier);
                            state = FlowStateScanBarcode.ActionIsScanningStopped;
                            break;

                        case FlowStateScanBarcode.ActionIsScanningStopped:
                            result = _actionCommands.IsScanningStopped(_currentConsignmentEntity, FlowResult);
                            state = result
                                        ? FlowStateScanBarcode.ThisFlowComplete
                                        : FlowStateScanBarcode.ActionIsConsignment;
                            break;

                        case FlowStateScanBarcode.ActionIsConsignment:
                            result = _actionCommands.IsConsignment(_currentConsignmentEntity);
                            state = result ? FlowStateScanBarcode.ActionIsConsignmentRegistrationAllowed :
                                FlowStateScanBarcode.ActionIsConsignmentNumberForItemKnown;
                            break;

                        case FlowStateScanBarcode.ActionIsConsignmentRegistrationAllowed:
                            _actionCommands.CheckConsignmentRegistrationAllowed(_currentConsignmentEntity.CastToConsignment(), FlowResult, _flowData);
                            state = FlowStateScanBarcode.ThisFlowComplete;
                            break;

                        case FlowStateScanBarcode.ActionIsConsignmentNumberForItemKnown:
                            result = _actionCommands.IsConsignmentNumberForItemKnown(_currentConsignmentEntity);
                            state = result ? FlowStateScanBarcode.ThisFlowComplete
                                : FlowStateScanBarcode.ActionCheckConsignmentNumberRequired;
                            break;

                        case FlowStateScanBarcode.ActionCheckConsignmentNumberRequired:
                            state = _actionCommands.CheckConsignmentNumberRequired(_currentConsignmentEntity, _flowData.IsRegisterWaybillSkippedForPickup);
                            break;

                        case FlowStateScanBarcode.ActionBackFromRegisterWaybill:
                            result = _actionCommands.ValidateRegisterWaybillResult((FlowResultRegisterWaybill)SubflowResult,
                                    _currentConsignmentEntity.CastToConsignmentItem(), ref _flowData.IsRegisterWaybillSkippedForPickup);
                            FlowResult.State = SubflowResult.State;
                            if (_flowData.IsRegisterWaybillSkippedForPickup)
                            {
                                FlowResult.IsSkippedOnceForPickup = true; //has been skipped once for Pickup process
                            }
                            state = result ? FlowStateScanBarcode.ThisFlowComplete : FlowStateScanBarcode.ActionDecideViewScanBarcode;
                            break;

                        case FlowStateScanBarcode.ActionBackFromFlowScanConsignment:
                            result = _actionCommands.ValidateScanConsignmentResult((FlowResultScanConsignment)SubflowResult, _currentConsignmentEntity.CastToConsignmentItem(), ref _currentScanning);
                            FlowResult.State = SubflowResult.State;
                            state = result ? FlowStateScanBarcode.ThisFlowComplete : FlowStateScanBarcode.ActionDecideViewScanBarcode;
                            break;

                        #region HandleLoadCarrier
                        case FlowStateScanBarcode.ActionIsLoadCarrierScannedBefore:
                            result = _actionCommands.IsScannedBefore(_flowData.EntityMap, ref _currentConsignmentEntity);
                            state = result
                                        ? FlowStateScanBarcode.ThisFlowComplete
                                        : FlowStateScanBarcode.ActionValidateLoadCarrierFromServer;
                            if (result)
                                FlowResult.State = FlowResultState.Duplicate;
                            break;

                        case FlowStateScanBarcode.ActionValidateLoadCarrierFromServer:
                            state = _flowData.NoValidationOfLoadCarrierFromServer
                                        ? FlowStateScanBarcode.ThisFlowComplete
                                        : FlowStateScanBarcode.ActionValidateLoadCarrier;
                            break;

                        case FlowStateScanBarcode.ActionValidateLoadCarrier:
                            ActionCommandsScanBarcode.ValidateLoadCarrier(_currentConsignmentEntity.CastToLoadCarrier(), FlowResult, _flowData.WantLoadCarrierDetails ? string.Empty : "Vekselbeholder", !_flowData.WantLoadCarrierDetails, string.Empty);
                            state = FlowStateScanBarcode.ThisFlowComplete;
                            break;

                        case FlowStateScanBarcode.ActionGetExistingLoadCarrierWorkListItem:
                            var workListItem = _actionCommandSelectCarrier.GetExistingLoadCarrierWorkListItem(_currentScanning.BarcodeLoadCarrier.LoadCarrierId, CurrentProcess == Process.Loading ? OperationType.Loading : OperationType.Unloading);
                            state = workListItem != null ? FlowStateScanBarcode.ActionGetFlowResultForScanning : FlowStateScanBarcode.ViewShowNewCarrierPopUp;
                            break;


                        case FlowStateScanBarcode.ActionStoreLoadCarrierWhenUnloading:
                            _actionCommandSelectCarrier.StoreLoadCarrierInWorkList(ModelMain.SelectedOperationProcess.StopId, ModelMain.SelectedOperationProcess.TripId,
                                                                       _currentScanning.BarcodeLoadCarrier.LoadCarrierId, CurrentProcess);

                            state = BaseModule.CurrentFlow.CurrentProcess == Process.Unloading
                                ? FlowStateScanBarcode.ActionDecideViewScanBarcode : FlowStateScanBarcode.ThisFlowComplete;
                            break;


                        case FlowStateScanBarcode.ActionStoreLoadCarrierWhenLoading:
                            _actionCommandSelectCarrier.StoreLoadCarrierInWorkList(ModelMain.SelectedOperationProcess.StopId, ModelMain.SelectedOperationProcess.TripId,
                                                                       _currentScanning.BarcodeLoadCarrier.LoadCarrierId, CurrentProcess);
                            state = BaseModule.CurrentFlow.CurrentProcess == Process.Loading
                                ? FlowStateScanBarcode.ActionDecideViewScanBarcode : FlowStateScanBarcode.ThisFlowComplete;
                            break;


                        #endregion

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }

                } while (state < FlowStateScanBarcode.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowScanBarcode.ExecuteActionState");
                FlowResult.State = FlowResultState.Exception;
                state = FlowStateScanBarcode.ThisFlowComplete;
            }


            ExecuteViewState(state);
        }


        /// <summary>
        /// Handler which will handle all type of events from Scan-view
        /// </summary>
        private void ScanViewEventHandler(int scanBarcodeViewEvents, params object[] data)
        {

            if (scanBarcodeViewEvents != ScanBarcodeViewEvents.BarcodeScanned && scanBarcodeViewEvents != ScanBarcodeViewEvents.Deviation)
                IsMainScanningBackClicked = true;
            switch (scanBarcodeViewEvents)
            {
                case ScanBarcodeViewEvents.Ok:
                    FlowResult.State = FlowResultState.Finished;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                case ScanBarcodeViewEvents.ChangeCarrier:
                    FlowResult.State = FlowResultState.Finished;
                    FlowResult.ChangeCarrier = true;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                case ScanBarcodeViewEvents.Back:
                    FlowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                case ScanBarcodeViewEvents.BarcodeScanned:
                    FlowResult.State = FlowResultState.Ok;
                    FotScannerOutput = (FotScannerOutput)data[0];
                    ExecuteState(FlowStateScanBarcode.ActionValidateBarcode);
                    break;

                case ScanBarcodeViewEvents.Load:
                    FlowResult.LoadCarrierId = _currentScanning.BarcodeLoadCarrier.LoadCarrierId;
                    if (BaseModule.CurrentFlow.CurrentProcess != Process.Loading)
                    {
                        FlowResult.ChangeCarrier = true;
                        FlowResult.StartLoading = true;
                        FlowResult.State = FlowResultState.Finished;
                    }
                    ExecuteState(FlowStateScanBarcode.ActionStoreLoadCarrierWhenLoading);
                    break;

                case ScanBarcodeViewEvents.Unload:
                    FlowResult.LoadCarrierId = _currentScanning.BarcodeLoadCarrier.LoadCarrierId;
                    if (BaseModule.CurrentFlow.CurrentProcess != Process.Unloading)
                    {
                        FlowResult.ChangeCarrier = true;
                        FlowResult.StartUnloading = true;
                        FlowResult.State = FlowResultState.Finished;
                    }
                    ExecuteState(FlowStateScanBarcode.ActionStoreLoadCarrierWhenUnloading);
                    break;

                case ScanBarcodeViewEvents.AttemptedPickup:
                    FlowResult.State = FlowResultState.AttemptedPickup;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                case ScanBarcodeViewEvents.AttemptedDelivery:
                    FlowResult.State = FlowResultState.AttemptedDelivery;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                case ScanBarcodeViewEvents.Reconcilliation:
                    FlowResult.State = FlowResultState.Reconcilliation;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                case ScanBarcodeViewEvents.Deviation:
                    if (!_flowData.EntityMap.IsScannedBefore(_flowData.LastScanning))
                        _flowData.LastScanning = null;
                    if (_flowData.LastScanning != null && !(_flowData.LastScanning is LoadCarrier))
                    {
                        FlowResult.BarcodeType = BarcodeType.ConsignmentItemNumber;
                        FlowResult.State = FlowResultState.Duplicate;
                        ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    }
                    else
                    {
                        if (_flowData.LastScanning != null && _flowData.IsLoadCarrierDeviationAllowed)//Special case US 24
                        {
                            FlowResult.BarcodeType = BarcodeType.ConsignmentItemNumber;
                            FlowResult.State = FlowResultState.Duplicate;
                            ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                        }
                        else
                        {
                            _flowData.MessageHolder.Update(GlobalTexts.NoItemScanned, MessageState.Warning);
                            ExecuteState(FlowStateScanBarcode.ViewScanBarcode);
                        }
                    }

                    break;

                case ScanBarcodeViewEvents.TaskDetails:
                    FlowResult.ShowTaskDetail = true;
                    FlowResult.State = FlowResultState.Finished;
                    ExecuteState(FlowStateScanBarcode.ThisFlowComplete);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ScanViewEventHandler");
                    break;
            }
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowScanBarcode");
            using (ProfilingMeasurement.MeasureFunction("FlowScanBarcode.EndFlow"))
            {
                FlowResult.CurrentConsignmentEntity = _currentConsignmentEntity;
                if (_currentScanning != null)
                {
                    FlowResult.BarcodeType = _currentScanning.BarcodeType;
                    FlowResult.BarcodeSubType = _currentScanning.BarcodeSubType;
                }

                if (IsMainScanningBackClicked == false)
                {
                    switch (FlowResult.State)
                    {
                        case FlowResultState.Ok:
                            switch (FlowResult.BarcodeType)
                            {
                                case BarcodeType.LoadCarrier:
                                    PlaySuccessSound();
                                    break;

                                case BarcodeType.Shelf:
                                case BarcodeType.OrgUnitId:
                                    SoundUtil.Instance.PlaySuccessSound();
                                    break;
                            }
                            break;

                        case FlowResultState.Duplicate:
                        case FlowResultState.Cancel:
                        case FlowResultState.Warning:
                            SoundUtil.Instance.PlayWarningSound();
                            break;

                        case FlowResultState.Error:
                            SoundUtil.Instance.PlayScanErrorSound();
                            break;
                    }
                }

                BaseModule.EndSubFlow(FlowResult);
            }
        }

        private void PlaySuccessSound()
        {
            if (_flowData.PlaySuccesSound)
            {
                if (FlowResult.CurrentConsignmentEntity.IsScannedOnline)
                {
                    if (FlowResult.CurrentConsignmentEntity.IsKnownInLm)
                        SoundUtil.Instance.PlaySuccessSound();
                    else
                        SoundUtil.Instance.PlayWarningSound();
                }
                else // case of offline with ok
                    SoundUtil.Instance.PlayWarningSound();
            }
        }
    }
}

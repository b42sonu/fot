﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Barcode;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    class ViewCommandsScanBarcode
    {

        internal void ScanBarcode(ViewEventDelegate viewEventHandler,ref FlowDataScanBarcode flowData, BarcodeData currentScanning, bool showNewCarrierPopUp)
        {
            var scantext = flowData.ScanText;
            scantext = BaseActionCommands.ScanTextFromValidBarcodeTypes(flowData.ValidBarcodeTypes, scantext);
            var formScanBarcodeData = new FormScanBarcodeData
                {
                    MessageHolder = flowData.MessageHolder,
                    CancelButtonDisabledWhenScanning = flowData.CancelButtonDisabledWhenScanned,
                    HeaderText = flowData.HeaderText,
                    EntityMap = flowData.EntityMap,
                    HasChangeCarrierButton = flowData.IsWorkListRequired,
                    ShowNewCarrierPopUp = showNewCarrierPopUp,
                    ScanText = string.IsNullOrEmpty(scantext) ? "" : scantext + ":",
                    ScanPdf417Only = false,
                    SkipReconcilliation = flowData.SkipReconcilliation,
                    ShowNoOfItems = flowData.ShowNoOfItems,
                    ShowShelf = flowData.ShowShelf,
                    ShowNoOfLoadCarriers = flowData.ShowNoOfLoadCarriers,
                    ShowDeviationButton = flowData.ShowDeviationButton,//(currentScanning != null && flowData.ShowDeviationButton),
                    IsStartedFromOperationList = flowData.StartedFromOperationList
                };
            flowData.ScanText = formScanBarcodeData.ScanText;
            if (currentScanning != null && currentScanning.IsLoadCarrier)
            {
                formScanBarcodeData.LoadCarrierId = currentScanning.BarcodeLoadCarrier.LoadCarrierId;
            }

            if (flowData.HaveCustomForm == false)
            {
                var view = ViewCommands.ShowView<FormScanBarcode>(formScanBarcodeData);
                view.SetEventHandler(viewEventHandler);
            }
        }



        /// <summary>
        /// Scan PDF 
        /// </summary>
        internal void ScanPdf417<T>(ViewEventDelegate viewEventHandler, FlowDataScanBarcode flowData, BarcodeData currentScanning) where T : BaseForm, new()
        {

            var formScanBarcodeData = new FormScanBarcodeData
                {
                    MessageHolder = flowData.MessageHolder,
                    CancelButtonDisabledWhenScanning = flowData.CancelButtonDisabledWhenScanned,
                    HeaderText = flowData.HeaderText,
                    EntityMap = flowData.EntityMap,
                    HasChangeCarrierButton = flowData.IsWorkListRequired,
                    ScanPdf417Only = true
                };

            if (currentScanning != null && currentScanning.IsLoadCarrier)
            {
                formScanBarcodeData.LoadCarrierId = currentScanning.BarcodeLoadCarrier.LoadCarrierId;
            }

            var view = ViewCommands.ShowView<T>(formScanBarcodeData);
            view.SetEventHandler(viewEventHandler);
        }
    }
}

﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using System;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    /// <summary>
    /// Enum which specifies the collection of State for this flow.
    /// </summary>
    public enum FlowStateConsignment
    {
        ActionCheckConsignmentItemNumber,
        ActionIsPdfScan,
        ActionValidateBarcode,
        ActionAddConsignmentId,
        ActionValidateConsignment,

        ViewCommands,
        ViewScanConsignment,

        ThisFlowComplete
    }

    public class FlowDataScanConsignment : BaseFlowData
    {
        public ConsignmentItem ConsignmentItem { get; set; }
        public bool FromRegisterWaybill { get; set; }
        public string ScanText { get; set; }
    }

    public class FlowResultScanConsignment : BaseFlowResult
    {
        public BarcodeData CurrentScanning;
        public FotScannerOutput ScannerOutput;
    }


    /// <summary>
    /// This class acts as a flow for scanning of entityNumber.
    /// </summary>
    public class FlowScanConsignment : BaseFlow
    {
        private readonly ActionCommandsScanBarcode _actionCommands;
        private const BarcodeType ValidProtocols = BarcodeType.ConsignmentNumber;
        private BarcodeData _currentScanning;
        private FotScannerOutput _scannerOutput;
        private readonly MessageHolder _messageHolder;
        private readonly FlowResultScanConsignment _flowResult;
        private FlowDataScanConsignment _flowData;

        public FlowScanConsignment()
        {
            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultScanConsignment();
            _actionCommands = new ActionCommandsScanBarcode(_messageHolder);
        }

        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            SoundUtil.Instance.PlayWarningSound();
            _flowData = (FlowDataScanConsignment)flowData;

            _messageHolder.Update(CommunicationClient.Instance.IsConnected() ?
                (_flowData.ConsignmentItem.IsKnownInLm ? GlobalTexts.ScanConsignment : GlobalTexts.ConsignmentItemUnknownInLm)
                : GlobalTexts.ScanConsignment, MessageState.Warning);

            if (_flowData.FromRegisterWaybill)
                _messageHolder.Update(string.Format(GlobalTexts.UnknownConsignmentScanPdf, _flowData.ConsignmentItem.ItemDisplayId), MessageState.Warning);

            ShowScanView();
        }

        internal FormScanBarcode ShowScanView()
        {
            var formData = new FormScanBarcodeData
            {
                MessageHolder = _messageHolder,
                IsScanningConsignment = true,
                HeaderText = _flowData.HeaderText,
                ScanPdf417Only = _flowData.FromRegisterWaybill,
                ScanText = _flowData.ScanText
            };

            var view = ViewCommands.ShowView<FormScanBarcode>(formData);
            view.SetEventHandler(ViewEventHandler);
            return view;
        }



        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateConsignment)state);
        }

        public void ExecuteState(FlowStateConsignment state)
        {
            if (state > FlowStateConsignment.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStateConsignment state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowScanConsignment;" + state);
            if (BaseModule.MainForm.InvokeRequired)
            {
                BaseModule.MainForm.Invoke(new Action<FlowStateConsignment>(ExecuteViewState), state);
            }
            else
            {
                switch (state)
                {
                    case FlowStateConsignment.ViewScanConsignment:
                        ShowScanView();
                        break;

                    case FlowStateConsignment.ThisFlowComplete:
                        //_flowResult.State = FlowResultState.Ok;
                        EndFlow();
                        return;

                    default:
                        Logger.LogEvent(Severity.Error, "Encountered unhandled view State");
                        return;
                }
            }
        }


        /// <summary>
        /// This method executes the State for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateConsignment state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowScanConsignment;" + state);
                    //check the State and execute the same
                    bool result;
                    switch (state)
                    {
                        case FlowStateConsignment.ActionValidateBarcode:
                            result = BaseActionCommands.ValidateBarcode(_scannerOutput, ValidProtocols, out _currentScanning);
                            if (result)
                                state = FlowStateConsignment.ActionIsPdfScan;
                            else
                            {
                                _messageHolder.Update(string.Format(GlobalTexts.IllegalConsignment, _scannerOutput.Code), MessageState.Error);
                                state = FlowStateConsignment.ViewScanConsignment;
                            }
                            break;

                        case FlowStateConsignment.ActionIsPdfScan:
                            if (_currentScanning != null && _currentScanning.BarcodeSubType == BarcodeSubType.Pdf)
                                state = FlowStateConsignment.ActionCheckConsignmentItemNumber;
                            else
                                state = BaseModule.CurrentFlow.CurrentProcess == Process.LoadDistribTruck//CO 118 for US 10
                                            ? FlowStateConsignment.ActionValidateConsignment
                                            : FlowStateConsignment.ActionAddConsignmentId;
                            break;

                        case FlowStateConsignment.ActionValidateConsignment:
                            var consignment = new Consignment(_currentScanning.ConsignmentId);
                            if (_actionCommands.ValidateConsignment(consignment))
                                state = FlowStateConsignment.ActionAddConsignmentId;
                            else
                            {
                                _flowResult.State = FlowResultState.Error;
                                _flowResult.Message = GlobalTexts.LoadingOfItemsWithoutKnownConsignmentNotAllowed;
                                state = FlowStateConsignment.ThisFlowComplete;
                            }

                            break;

                        case FlowStateConsignment.ActionCheckConsignmentItemNumber:
                            result = _actionCommands.IsScanConsignmentItemNumberSameInPdf(_flowData.ConsignmentItem,
                                                                                          _currentScanning);
                            if (result)
                                state = FlowStateConsignment.ActionAddConsignmentId;
                            else
                            {
                                if (_currentScanning != null)
                                {
                                    string messageText = string.Format(GlobalTexts.ConsigmentItemNotSameWithPdfItemNumber,
                                        _currentScanning.ConsignmentItemId, _flowData.ConsignmentItem.ItemId);

                                    _actionCommands.ShowWarningForConsignmentNotSameInPdf(messageText, Severity.Warning);
                                }
                                _flowResult.State = FlowResultState.Error;
                                state = FlowStateConsignment.ViewScanConsignment; // Try again
                            }
                            break;

                        case FlowStateConsignment.ActionAddConsignmentId:
                            _flowResult.State = FlowResultState.Ok;
                            _flowData.ConsignmentItem.ConsignmentInstance = new Consignment(_currentScanning.ConsignmentId);
                            _flowData.ConsignmentItem.IsConsignmentSynthetic = false;
                            _flowResult.CurrentScanning = _currentScanning;
                            _flowResult.ScannerOutput = _scannerOutput;
                            state = FlowStateConsignment.ThisFlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateConsignment.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowScanConsignment.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateConsignment.ThisFlowComplete;
            }

            ExecuteViewState(state);
        }


        /// <summary>
        /// Handler which will handle all type of events from Scan View
        /// </summary>
        public void ViewEventHandler(int flowEvents, object[] data)
        {
            switch (flowEvents)
            {
                case ScanBarcodeViewEvents.Ok:
                    break;

                case ScanBarcodeViewEvents.Back:
                    CurrentView.EventResult.State = EventResultState.Ok;
                    _flowResult.Message = GlobalTexts.ScanningConsignmentAborted;
                    _flowResult.State = FlowResultState.Cancel;
                    EndFlow();
                    break;

                case ScanBarcodeViewEvents.Cancel:
                    CurrentView.EventResult.State = EventResultState.Error;
                    _flowResult.Message = GlobalTexts.ScanningConsignmentAborted;
                    _flowResult.State = FlowResultState.Error;
                    EndFlow();
                    break;

                case ScanBarcodeViewEvents.Skip:
                    CurrentView.EventResult.State = EventResultState.Ok;
                    _flowResult.Message = String.Format(GlobalTexts.ConsignmentItemRegisteredWithoutConsignment, _flowData.ConsignmentItem.ItemDisplayId);
                    _flowResult.State = FlowResultState.Cancel;
                    EndFlow();
                    break;

                case ScanBarcodeViewEvents.BarcodeScanned:
                    _scannerOutput = (FotScannerOutput)data[0];
                    ExecuteActionState(FlowStateConsignment.ActionValidateBarcode);
                    break;
            }
        }

        private void EndFlow()
        {
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

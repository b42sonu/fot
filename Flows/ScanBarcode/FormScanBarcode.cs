﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.SelectCarrier;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.CommonControls;
using Resco.Controls.OutlookControls;
using Process = Com.Bring.PMP.PreComFW.Shared.Storage.Process;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode
{
    public partial class FormScanBarcode : BaseBarcodeScanForm
    {
        private FormScanBarcodeData _formData;
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        private const string ButtonAttemptedPickUp = "ButtonAttemptedPickUp";
        private const string ButtonAttemptedDelivery = "ButtonAttemptedDelivery";
        private const string ButtonSkip = "ButtonSkip";
        private const string ButtonReconciliation = "ButtonReconciliation";
        private const string ButtonChangeCarrier = "ButtonChangeCarrier";
        private const string ButtonDeviation = "ButtonDeviation";
        private const string ButtonTaskDetails = "ButtonTaskDetails";
        private bool _hasScannedEntities;


        /// <summary>
        /// Initializes a new instance
        /// </summary>
        public FormScanBarcode()
        {
            InitializeComponent();
            var lblHeading = AddOrgUnitControl();
            SetStandardControlProperties(labelModuleName, lblHeading, labelTask, txtScannedNumber, _messageControl, labelItemCounter, lblNoOfLoadCarriers);

            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            SetTextToGui();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }


        }

        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormScanBarcode.AddButtons();" + BaseModule.CurrentFlow.CurrentProcess);

                _multiButtonControl.Clear();
                _multiButtonControl.ListButtons.Clear();

                

                if (_formData != null && _formData.ScanPdf417Only)
                {
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Cancel, ButtonCancelClick, TabButtonType.Cancel) { Name = ButtonBack });
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Skip, ButtonSkipClick, ButtonSkip));
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk, Enabled = false });
                    return;
                }

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });


                if (_formData != null && _formData.IsScanningConsignment)
                {
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                    return;
                }

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                if (_formData != null && _formData.ShowDeviationButton)
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Deviation, ButtonDeviationClick, ButtonDeviation));

                if (_formData != null && _formData.IsStartedFromOperationList && ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora)
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.TaskDetail, ButtonTaskDetailsClick, ButtonTaskDetails));

                if (BaseModule.CurrentFlow.CurrentProcess == Process.CorrectWeightVolume || BaseModule.CurrentFlow.CurrentProcess == Process.ControlScan)
                    return;

                if (_formData != null && _formData.HasChangeCarrierButton)
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.ChgCarrier, ButtonChangeCarrierClick, ButtonChangeCarrier));


                //if (_formData != null && !_formData.SkipReconcilliation)
                //    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Reconcillation, ButtonReconcilliationClick, ButtonReconciliation));


                if (BaseModule.CurrentFlow.CurrentProcess == Process.Pickup || BaseModule.CurrentFlow.CurrentProcess == Process.UnplannedPickup)
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.AttemptedPickUp, ButtonAttemptedPickupClick, ButtonAttemptedPickUp));
                if (BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer)
                    _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.AttemptedDelivery, ButtonAttemptedDeliveryClick, ButtonAttemptedDelivery));



            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.SetTextToGui");
            }
        }


        private void SetTextToGui()
        {
            buttonUnloadChangeCarrier.Text = GlobalTexts.Unload;
            buttonCancelChangeCarrier.Text = GlobalTexts.Cancel;
            buttonLoadChangeCarrier.Text = GlobalTexts.Load;
            lblConfirmInfo.Text = GlobalTexts.ConfirmInfo;
            lblConfirmInfoHeading.Text = GlobalTexts.NewCarrier;
            lblNewCarrier.Text = GlobalTexts.NewCarrier + Colon;
            labelTask.Text = GlobalTexts.ScanOrEnterBarcode + Colon;
            labelModuleName.Text = GlobalTexts.Pickup;
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
                                       {
                                           TextChanged = ConsignmentNumberTextChanged,
                                           Title = (_formData != null && string.IsNullOrEmpty(_formData.ScanText) == false) ? _formData.ScanText : ""
                                       };
            txtScannedNumber.Tag = keyBoardProperty;

            txtNewCarrier.Tag = new OnScreenKeyboardProperties(GlobalTexts.EnterLoadCarrier, KeyPadTypes.QwertyUpperCase, KeyPadTypes.Numeric);
        }

        private void ConsignmentNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOk, txtScannedNumber.Text != string.Empty || _hasScannedEntities);
            if (_messageControl != null)
                _messageControl.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text != string.Empty)
            {
                BarCodeEnteredManually(textentered);
            }
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            _formData = (FormScanBarcodeData)formData;
            InitOnScreenKeyBoardProperties();

            AddButtons();
            _multiButtonControl.BeginUpdate();
            _multiButtonControl.GenerateButtons();

            if (_formData == null) return;
            if (_formData.CorrectWVflow) ApplyFormatForCorrectWVflow();
            if (_formData.ControlScanFlow) ApplyFormatForControlScan();
            if (_formData.ScanPdf417Only) ApplyFormatForRegisterWaybill();

            OnUpdateView(formData);
            _multiButtonControl.EndUpdate();
        }

        private TransparentLabel AddOrgUnitControl()
        {
            var lblOrgUnit = new TransparentLabel
             {
                 AutoSize = false,
                 Size = new Size(480, 33),
                 Location = new Point(0, 42),
                 Text = ModelUser.UserProfile.OrgUnitName,
                 TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter,
                 Font = new Font("Arial", 9F, FontStyle.Bold),
             };
            touchPanel.Controls.Add(lblOrgUnit);
            return lblOrgUnit;
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtScannedNumber.Text = string.Empty;
            ViewEvent.Invoke(ScanBarcodeViewEvents.BarcodeScanned, scannerOutput);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            try
            {
                if (iformData != null)
                {
                    _formData = (FormScanBarcodeData)iformData;

                    if (_formData.MessageHolder != null && (_formData.MessageHolder.State != MessageState.Error || (_formData.MessageHolder.State == MessageState.Error && _formData.MessageHolder.ErrorInfo == ErrorInfo.ShipmentStopped)))
                    {
                        txtScannedNumber.Text = "";
                    }

                    labelModuleName.Text = _formData.HeaderText;
                    _hasScannedEntities = _formData.EntityMap != null && _formData.EntityMap.HasScannedEntities;


                    //If new carrier pop is to open then disable all buttons and show popup
                    //Otherwise hide / unhide buttons on the basis of different conditions
                    if (_formData.ShowNewCarrierPopUp)
                    {
                        EnableAllButtons(false);
                        ShowPopupForNewCarrier(true);
                    }
                    else
                        UpdateButtonsStatus();

                    if (_formData.EntityMap != null)
                    {
                        labelItemCounter.Visible = _formData.ShowNoOfItems;
                        lblShelfLocation.Text = GlobalTexts.Placement + Colon;
                        lblShelfLocation.Visible = _formData.ShowShelf;
                        lblNoOfLoadCarriers.Visible = _formData.ShowNoOfLoadCarriers;
                        labelItemCounter.Text = GlobalTexts.NoOfItems + ":" + String.Format(" {0}", _formData.EntityMap.ScannedConsignmentItemsTotalCount);
                        lblNoOfLoadCarriers.Text = GlobalTexts.NoOfCarriers + ":" +
                                                   String.Format(" {0}", _formData.EntityMap.ScannedLoadCarrierTotalCount);
                    }
                    else
                    {
                        labelItemCounter.Visible = false;
                        lblNoOfLoadCarriers.Visible = false;
                    }



                    //Set message holder
                    if (_formData.MessageHolder != null)
                        _messageControl.ShowMessage(_formData.MessageHolder);

                    //Set text of label above text field
                    labelTask.Text = _formData.ScanPdf417Only ? GlobalTexts.ScanPdf417 : GlobalTexts.ScanOrEnterBarcode;
                    labelTask.Text += Colon;
                    if (!string.IsNullOrEmpty(_formData.ScanText))
                    {
                        Logger.LogEvent(Severity.Debug, "_formData.ScanText;" + _formData.ScanText);
                        labelTask.Text = _formData.ScanText;

                    }
                    if (_formData.forceClearScanningText)//special case Control scan 
                        txtScannedNumber.Text = string.Empty;

                    if (_formData.CorrectWVflow) ApplyFormatForCorrectWVflow();
                    else if (_formData.ControlScanFlow) ApplyFormatForControlScan();
                    else if (_formData.ScanPdf417Only) ApplyFormatForRegisterWaybill();
                    else
                    {
                        SetDefaultMessageHolderPosition();
                    }
                    if (_multiButtonControl.Exists(ButtonReconciliation))
                        _multiButtonControl.SetButtonEnabledState(ButtonReconciliation, _formData.SkipReconcilliation);

                    _multiButtonControl.SetButtonEnabledState(ButtonOk, _hasScannedEntities || txtScannedNumber.Text != string.Empty);
                    //Set key state
                    SetKeyboardState(KeyboardState.Numeric);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.OnUpdateView");
            }
        }

        private void ApplyFormatForRegisterWaybill()
        {
            _multiButtonControl.SetButtonEnabledState(ButtonSkip, true);
            _multiButtonControl.SetButtonEnabledState(ButtonOk, _hasScannedEntities || txtScannedNumber.Text != string.Empty);
            SetDefaultMessageHolderPosition();
        }

        private void SetDefaultMessageHolderPosition()
        {
            _messageControl.Height = ScaleUtil.GetScaledPosition(110);
        }

        private void ApplyFormatForCorrectWVflow()
        {
            labelItemCounter.Visible = true;
            lblNoOfLoadCarriers.Visible = false;
            labelTask.Height = 30;
            txtScannedNumber.Top = 140 - 30;
            _messageControl.Top = 175 - 15;
            labelItemCounter.Text = GlobalTexts.NoOfItems + ":" + String.Format(" {0}", _formData.EntityMap.ScannedConsignmentAndItemsTotalCount);
        }
        private void ApplyFormatForControlScan()
        {
            labelItemCounter.Visible = false;
            lblNoOfLoadCarriers.Visible = false;
            var fromOperationList =
                _formData.HeaderText == GlobalTexts.OperationListControlScan;
            txtScannedNumber.Visible = !fromOperationList;
            labelTask.Visible = !fromOperationList;
        }


        void UpdateButtonsStatus()
        {
            Debug.Assert(_formData != null);

            // Now we scan consignment so disable every button except cancel
            if (_formData.IsScanningConsignment)
            {
                foreach (var tabButton in _multiButtonControl.ListButtons)
                {
                    if (tabButton.Name != ButtonOk)
                        _multiButtonControl.SetButtonEnabledState(tabButton.Name, false);
                }
                if (_multiButtonControl.Exists(ButtonBack))
                    _multiButtonControl.SetButtonEnabledState(ButtonBack, true);
                if(_multiButtonControl.Exists(ButtonOk))
                    _multiButtonControl.SetButtonEnabledState(ButtonOk, false);//Ok button should not be visible by default
                return;
            }


            // Can only do attempted pickup if no pickups has been scanned
            //_multiButtonControl.ShowButton(ButtonAttemptedPickUp, !_hasScannedEntities);

            // Can only finish operation if user has scanned something
            _multiButtonControl.SetButtonEnabledState(ButtonOk, _hasScannedEntities || txtScannedNumber.Text != string.Empty);

            // Cancel button can be disabled by supplying data in _formData
            _multiButtonControl.SetButtonEnabledState(ButtonBack, !_hasScannedEntities);

            if (_multiButtonControl.Exists(ButtonReconciliation))
                _multiButtonControl.SetButtonEnabledState(ButtonReconciliation, _hasScannedEntities);

            if (_multiButtonControl.Exists(ButtonDeviation))
                _multiButtonControl.SetButtonEnabledState(ButtonDeviation, _formData.ShowDeviationButton && _hasScannedEntities);

            // Set for delivery
            if (BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer)
                _multiButtonControl.SetButtonEnabledState(ButtonAttemptedDelivery, _hasScannedEntities);

        }

        private static bool IsValidLoadCarrier(string loadCarrier)
        {
            return Regex.IsMatch(loadCarrier, @"[A-Z]{3}[0-9]{3}");
        }


        private bool ValidateLoadCarrier()
        {
            var validationResult = IsValidLoadCarrier(txtNewCarrier.Text);
            if (!validationResult)
            {
                lblConfirmInfo.Text = GlobalTexts.WrongCarrierFormat;
            }
            return validationResult;
        }

        private void BarCodeEnteredManually(String enteredBarcode)
        {
            var scannerOutput = new FotScannerOutput { Code = enteredBarcode };
            ViewEvent.Invoke(ScanBarcodeViewEvents.BarcodeScanned, scannerOutput);
        }

        private void ShowPopupForNewCarrier(bool visible)
        {
            pnlConfirmMessage.Visible = visible;
            if (visible)
            {
                lblConfirmInfo.Text = GlobalTexts.AddCarrierAndSelectOperation;
                txtNewCarrier.Text = _formData.LoadCarrierId;
                pnlConfirmMessage.BringToFront();

            }
        }

        private void EnableAllButtons(bool status)
        {
            foreach (var button in touchPanel.Controls.OfType<ImageButton>())
            {
                button.Enabled = status;
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);

                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonBackClick");
            }
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);

                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.Cancel);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonCancelClick");
            }
        }

        private void ButtonSkipClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);
                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.Skip);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonSkipClick");
            }
        }

        private void ButtonChangeCarrierClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);
                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.ChangeCarrier);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonChangeCarrierClick");
            }
        }


        private void ButtonTaskDetailsClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);
                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.TaskDetails);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonTaskDetailsClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);

                if (ViewEvent != null)
                {
                    if (string.IsNullOrEmpty(txtScannedNumber.Text.Trim()))
                        ViewEvent.Invoke(ScanBarcodeViewEvents.Ok);
                    else
                        BarCodeEnteredManually(txtScannedNumber.Text.Trim());
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonOkClick");
            }
        }

        private void ButtonReconcilliationClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);
                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.Reconcilliation);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonReconcilliationClick");
            }
        }

        private void ButtonAttemptedPickupClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);
                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.AttemptedPickup);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonAttemptedPickupClick");
            }
        }

        private void ButtonAttemptedDeliveryClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogAssert(ViewEvent != null);
                if (ViewEvent != null)
                    ViewEvent.Invoke(ScanBarcodeViewEvents.AttemptedDelivery);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonAttemptedDeliveryClick");
            }
        }

        private void ButtonDeviationClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ScanBarcodeViewEvents.Deviation);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ButtonDeviationClick");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void PnlConfirmMessagePaint(object sender, PaintEventArgs e)
        {
            try
            {
                using (var pen = new Pen(Color.FromArgb(113, 112, 116), 2))
                {
                    var graphics = e.Graphics;
                    graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlConfirmMessage.Width - 3, pnlConfirmMessage.Height - 3));
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.PnlConfirmMessagePaint");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnLoadClick(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateLoadCarrier())
                {
                    return;
                }

                pnlConfirmMessage.Visible = false;
                ViewEvent.Invoke(SelectCarrierEvent.Load, txtNewCarrier.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.BtnLoadClick");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnUnloadClick(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateLoadCarrier())
                {
                    return;
                }

                pnlConfirmMessage.Visible = false;
                ViewEvent.Invoke(SelectCarrierEvent.Unload, txtNewCarrier.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.BtnUnloadClick");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickCancelChangeCarrier(object sender, EventArgs e)
        {
            try
            {
                ShowPopupForNewCarrier(false);
                EnableAllButtons(true);
                UpdateButtonsStatus();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanBarcode.ClickCancelChangeCarrier");
            }
        }
    }

    public class ScanBarcodeViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
        public const int ChangeCarrier = 3;
        public const int Load = 4;
        public const int Unload = 5;
        public const int AttemptedPickup = 6;
        public const int AttemptedDelivery = 7;
        public const int Reconcilliation = 8;
        public const int Skip = 9;
        public const int Deviation = 10;
        public const int TaskDetails = 11;
        public const int Cancel = 12;
    }

    #region View specific classes
    public class FormScanBarcodeData : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
        public bool CancelButtonDisabledWhenScanning { get; set; }
        public EntityMap EntityMap;
        public bool HasChangeCarrierButton { get; set; }
        public bool ShowNewCarrierPopUp { get; set; }
        public string LoadCarrierId { get; set; }
        public string ScanText { get; set; }
        public bool IsScanningConsignment { get; set; }
        public bool ScanPdf417Only { get; set; }
        public bool CorrectWVflow { get; set; }
        public bool ControlScanFlow { get; set; }
        public bool SkipReconcilliation { get; set; }
        public bool ShowNoOfItems { get; set; }
        public bool ShowShelf { get; set; }
        public bool ShowDeviationButton { get; set; }
        public bool ShowNoOfLoadCarriers { get; set; }
        public bool IsStartedFromOperationList { get; set; }
        public bool forceClearScanningText { get; set; }
    }
    #endregion

}
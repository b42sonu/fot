﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    partial class FormAttemptedDeliveryList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAttemptedDeliveryList));
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.listOperations = new Resco.Controls.AdvancedList.AdvancedList();
            this.textCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.StopTemplate = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.SelectedStopTemplate = new Resco.Controls.AdvancedList.RowTemplate();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.listOperations);
            this.touchPanel.Controls.Add(this.labelConsignment);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelConsignment
            // 
            this.labelConsignment.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelConsignment.Location = new System.Drawing.Point(22, 54);
            this.labelConsignment.Name = "labelConsignment";
            this.labelConsignment.Size = new System.Drawing.Size(0, 0);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(115, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // listOperations
            // 
            this.listOperations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listOperations.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listOperations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listOperations.DataRows.Clear();
            this.listOperations.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            resources.GetString("listOperations.HeaderRow")});
            this.listOperations.Location = new System.Drawing.Point(22, 85);
            this.listOperations.Name = "listOperations";
            this.listOperations.ScrollbarSmallChange = 32;
            this.listOperations.ScrollbarWidth = 26;
            this.listOperations.SelectedTemplateIndex = 2;
            this.listOperations.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectOnly;
            this.listOperations.ShowHeader = true;
            this.listOperations.Size = new System.Drawing.Size(439, 391);
            this.listOperations.TabIndex = 3;
            this.listOperations.TemplateIndex = 1;
            this.listOperations.Templates.Add(this.rowTemplate1);
            this.listOperations.Templates.Add(this.StopTemplate);
            this.listOperations.Templates.Add(this.SelectedStopTemplate);
            this.listOperations.TouchScrolling = true;
            // 
            // textCell1
            // 
            this.textCell1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCell1.CellSource.ConstantData = "Operation List";
            this.textCell1.DesignName = "textCell1";
            this.textCell1.Location = new System.Drawing.Point(0, 0);
            this.textCell1.Size = new System.Drawing.Size(-1, 32);
            this.textCell1.Visible = false;
            // 
            // rowTemplate1
            // 
            this.rowTemplate1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rowTemplate1.CellTemplates.Add(this.textCell1);
            this.rowTemplate1.Height = 0;
            this.rowTemplate1.Name = "rowTemplate1";
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnIndex = 0;
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(438, 64);
            // 
            // StopTemplate
            // 
            this.StopTemplate.CellTemplates.Add(this.textCellOperationDetail);
            this.StopTemplate.Height = 30;
            this.StopTemplate.Name = "StopTemplate";
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnIndex = 0;
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(438, 64);
            // 
            // SelectedStopTemplate
            // 
            this.SelectedStopTemplate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(190)))), ((int)(((byte)(74)))));
            this.SelectedStopTemplate.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.SelectedStopTemplate.Height = 30;
            this.SelectedStopTemplate.Name = "SelectedStopTemplate";
            // 
            // FormAttemptedDeliveryList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormAttemptedDeliveryList";
            this.Text = "FormAttemptedDeliveryList";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignment;
        private Resco.Controls.AdvancedList.AdvancedList listOperations;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell1;
        private Resco.Controls.AdvancedList.RowTemplate StopTemplate;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.RowTemplate SelectedStopTemplate;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
    }
}
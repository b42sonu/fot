﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
// US 23: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7932737
namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateAttemptedDelivery
    {
        ActionIsSingleEntity,
        ActionCallVasForSelectedEntity,
        ActionBackFromFlowVasMatrix,
        ActionBackFromFlowDepartureFromStop,
        ActionRemoveConsignmentFromCache,
        ActionGetConsignmentList,
        ActionGetScannedConsignments,
        ActionGetPlannedConsignments,
        ActionSendGoodsAttemptedDeliveryEvent,
        ActionCreateReturnedDeliveryOperation,
        ActionBackFromFlowCauseAndMeasure,
        ViewCommands,
        FlowDepartureFromStop,
        FlowCauseAndMeasure,
        FlowExecuteVasMatrix,
        ViewShowAttemptedDeliveryList,
        UpdateAttemptedDeliveryView,
        ThisFlowComplete
    }
    public class FlowResultAttemptedDelivery : BaseFlowResult
    {
        public MessageHolder MessageHolder { get; set; }
        public EntityMap EntityMap;

        public string CauseCode;
        public string MeasureCode;
        public string CommentDescription;
    }
    /// <summary>
    /// flow data that needed for this flow when any flow call this flow...
    /// </summary>
    public class FlowDataAttemptedDelivery : BaseFlowData
    {
        public EntityMap EntityMap { get; set; }
        public bool IsDirectFromOperationList;
        public List<DeliveryOperation> LoadList { get; set; }
        //public bool IsCallFromVasMatrix;
        //public string VasMatrixCauseCode;
    }
    public class FlowAttemptedDelivery : BaseFlow
    {
        private bool _isMultipleEntity;
        private string _comment;
        private CauseMeasure _selectedReasonAndActionCombination;
        private readonly ViewCommandsAttemptedDelivery _viewCommandsAttemptedDelivery;
        private FlowDataAttemptedDelivery _flowData;
        private readonly FormDataAttemptedDeliveryList _formDataAttemptedDeliveryList;
        private readonly FlowResultAttemptedDelivery _flowResult;
        private ConsignmentEntity _selectedConsignment;
        private readonly ActionCommandsAttemptedDelivery _actionCommandsAttemptedDelivery;
        //private ConsignmentEntity _consignmentEntity;
        public override WorkProcess WorkProcess
        {
            get
            {
                return WorkProcess.AttemptedDelivery;
            }
        }
        public FlowAttemptedDelivery()
        {
            _actionCommandsAttemptedDelivery = new ActionCommandsAttemptedDelivery(CommunicationClient.Instance);
            _viewCommandsAttemptedDelivery = new ViewCommandsAttemptedDelivery();
            _formDataAttemptedDeliveryList = new FormDataAttemptedDeliveryList();
            _flowResult = new FlowResultAttemptedDelivery();
            _selectedConsignment = null;

        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowAttemptedDelivery.BeginFlow(IFlowData flowData)");
            _flowData = (FlowDataAttemptedDelivery)flowData;
            ExecuteState(FlowStateAttemptedDelivery.ActionGetConsignmentList);//FlowCauseAndMeasure);
        }

        public void ExecuteState(FlowStateAttemptedDelivery state)
        {
            if (state > FlowStateAttemptedDelivery.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateAttemptedDelivery)state);
        }
        private void ExecuteViewState(FlowStateAttemptedDelivery stateAttemptedDelivery)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowDeliveryByCustomer;" + stateAttemptedDelivery);
                switch (stateAttemptedDelivery)
                {
                    case FlowStateAttemptedDelivery.ViewShowAttemptedDeliveryList:
                        _formDataAttemptedDeliveryList.IsDirectFromOperationList = _flowData.IsDirectFromOperationList;
                        _viewCommandsAttemptedDelivery.ShowAttemptedDeliveryListView(AttemptedDeliveryListViewEventHandler, _formDataAttemptedDeliveryList);
                        break;

                    case FlowStateAttemptedDelivery.UpdateAttemptedDeliveryView:
                        CurrentView.UpdateView(null);
                        break;

                    case FlowStateAttemptedDelivery.FlowDepartureFromStop:
                        var flowData = new FlowDataDepartureFromStop(_flowData.HeaderText, true, string.Empty, FlowDataDepartureFromStop.ActionTypes.LO, _flowData.EntityMap);
                        StartSubFlow<FlowDepartureFromStop>(flowData, (int)FlowStateAttemptedDelivery.ActionBackFromFlowDepartureFromStop, Process.DeliveryToCustomer);
                        break;

                    case FlowStateAttemptedDelivery.FlowCauseAndMeasure:
                        var flowDataCauseAndMeasure = new FlowDataCauseAndMeasure
                        {
                            Comment = string.Empty,
                            SelectedReasonAndActionCombination = _selectedReasonAndActionCombination,//new CauseMeasure(),
                            HeaderText = GlobalTexts.AttemptedDelivery
                        };
                        StartSubFlow<FlowCauseAndMeasure>(flowDataCauseAndMeasure, (int)FlowStateAttemptedDelivery.ActionBackFromFlowCauseAndMeasure, Process.Inherit);
                        break;

                    case FlowStateAttemptedDelivery.FlowExecuteVasMatrix:
                        var flowDataVasMatrix = new FlowDataVasMatrix(_selectedConsignment, this) { HeaderText = _flowData.HeaderText, PlaySuccessSound = false };
                        StartSubFlow<FlowVasMatrix>(flowDataVasMatrix, (int)FlowStateAttemptedDelivery.ActionBackFromFlowVasMatrix, Process.Inherit);
                        break;

                    case FlowStateAttemptedDelivery.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowAttemptedDelivery.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateAttemptedDelivery.ThisFlowComplete);
            }
        }
        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateAttemptedDelivery state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowDeliveryByCustomer;" + state);
                    bool isValid;
                    switch (state)
                    {
                        case FlowStateAttemptedDelivery.ActionGetConsignmentList:
                            state = _flowData.IsDirectFromOperationList
                                        ? FlowStateAttemptedDelivery.ActionGetPlannedConsignments
                                        : FlowStateAttemptedDelivery.ActionGetScannedConsignments;
                            break;

                        case FlowStateAttemptedDelivery.ActionGetPlannedConsignments:
                            _formDataAttemptedDeliveryList.ConsignmentList = _actionCommandsAttemptedDelivery.GetPlannedConsignments(ModelMain.SelectedOperationProcess);
                            state = FlowStateAttemptedDelivery.ViewShowAttemptedDeliveryList;
                            break;

                        case FlowStateAttemptedDelivery.ActionGetScannedConsignments:
                            _formDataAttemptedDeliveryList.ConsignmentList = _actionCommandsAttemptedDelivery.GetConsignmentEntities(_flowData.EntityMap, EntityStatus.Scanned, false);
                            state = _formDataAttemptedDeliveryList.ConsignmentList != null && _formDataAttemptedDeliveryList.ConsignmentList.Count == 0 ? FlowStateAttemptedDelivery.ThisFlowComplete : FlowStateAttemptedDelivery.ActionIsSingleEntity;// ViewShowAttemptedDeliveryList;
                            break;
                        case FlowStateAttemptedDelivery.ActionIsSingleEntity:
                            state = FlowStateAttemptedDelivery.ViewShowAttemptedDeliveryList;
                            if (_isMultipleEntity == false)
                            {
                                isValid = _actionCommandsAttemptedDelivery.IsSingleEntity(
                                        _formDataAttemptedDeliveryList.ConsignmentList);
                                _isMultipleEntity = !isValid;
                                if (isValid)
                                {
                                    state = FlowStateAttemptedDelivery.ActionCallVasForSelectedEntity;
                                    _selectedConsignment = _formDataAttemptedDeliveryList.ConsignmentList[0];
                                }
                            }
                            break;
                        case FlowStateAttemptedDelivery.ActionSendGoodsAttemptedDeliveryEvent:
                            _actionCommandsAttemptedDelivery.SendAttemptedDeliveryEvents(ModelMain.SelectedOperationProcess,
                                _selectedConsignment, _selectedReasonAndActionCombination, _comment);
                            state = _flowData.IsDirectFromOperationList
                                        ? FlowStateAttemptedDelivery.FlowDepartureFromStop
                                        : FlowStateAttemptedDelivery.ActionRemoveConsignmentFromCache;
                            break;

                        case FlowStateAttemptedDelivery.ActionRemoveConsignmentFromCache:
                            _actionCommandsAttemptedDelivery.RemoveConsignmentFromCache(_flowData.EntityMap, _selectedConsignment, _flowData.LoadList);
                            _flowResult.EntityMap = _flowData.EntityMap;
                            _flowResult.State = FlowResultState.Ok;
                            _flowResult.MessageHolder = new MessageHolder(GlobalTexts.ScanConsignmentDeleted, MessageState.Warning);
                            //SoundUtil.Instance.PlayWarningSound();
                            _selectedConsignment = null;
                            state = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ? FlowStateAttemptedDelivery.ActionCreateReturnedDeliveryOperation : FlowStateAttemptedDelivery.ActionCallVasForSelectedEntity;
                            break;

                        case FlowStateAttemptedDelivery.ActionBackFromFlowDepartureFromStop:
                            _selectedConsignment = null;
                            _actionCommandsAttemptedDelivery.VerifyFlowDepartureFromStopResult((FlowResultDepartureFromStop)SubflowResult);
                            _flowResult.State = FlowResultState.Ok;
                            state = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ? FlowStateAttemptedDelivery.ActionCreateReturnedDeliveryOperation : FlowStateAttemptedDelivery.ThisFlowComplete;
                            break;

                        case FlowStateAttemptedDelivery.ActionCreateReturnedDeliveryOperation:
                            _actionCommandsAttemptedDelivery.CreateReturnedDeliveryOperation(
                                ModelMain.SelectedOperationProcess, _flowData.LoadList);
                            state = _flowData.IsDirectFromOperationList ? FlowStateAttemptedDelivery.ThisFlowComplete : FlowStateAttemptedDelivery.ActionCallVasForSelectedEntity;
                            break;

                        case FlowStateAttemptedDelivery.ActionBackFromFlowCauseAndMeasure:
                            isValid = _actionCommandsAttemptedDelivery.VerifyFlowResultCauseAndMeasure((FlowResultCauseAndMeasure)SubflowResult, _flowResult, ref _selectedReasonAndActionCombination, out _comment);

                            if (isValid)
                                state = FlowStateAttemptedDelivery.ActionSendGoodsAttemptedDeliveryEvent;
                            else
                            {
                                state = _isMultipleEntity == false ? FlowStateAttemptedDelivery.ThisFlowComplete : FlowStateAttemptedDelivery.ActionGetConsignmentList;
                                _selectedConsignment = null;
                                if (state == FlowStateAttemptedDelivery.ThisFlowComplete)
                                {
                                    state = FlowStateAttemptedDelivery.ThisFlowComplete;
                                    if (_flowResult.MessageHolder == null)
                                    {
                                        _flowResult.State = FlowResultState.Cancel;
                                        _flowResult.MessageHolder = new MessageHolder(GlobalTexts.AttemptedDeliveryAborted, MessageState.Error);
                                    }
                                }
                            }
                            break;

                        case FlowStateAttemptedDelivery.ActionBackFromFlowVasMatrix:

                            state = _actionCommandsAttemptedDelivery.BackFromExecuteVas((FlowResultVasMatrix)SubflowResult, ref _selectedConsignment);
                            if (_selectedConsignment == null)
                            {
                                if (_isMultipleEntity == false)
                                {
                                    state = FlowStateAttemptedDelivery.ThisFlowComplete;
                                    if (_flowResult.MessageHolder == null)
                                    {
                                        _flowResult.State = FlowResultState.Cancel;
                                        _flowResult.MessageHolder = new MessageHolder(GlobalTexts.AttemptedDeliveryAborted, MessageState.Error);
                                    }
                                }
                            }
                            break;

                        case FlowStateAttemptedDelivery.ActionCallVasForSelectedEntity:
                            state = _selectedConsignment == null ? FlowStateAttemptedDelivery.ActionGetConsignmentList :
                                _actionCommandsAttemptedDelivery.CallVasForSelectedEntity(_selectedConsignment);

                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateAttemptedDelivery.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowAttemptedDelivery.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateAttemptedDelivery.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        /// <summary>
        /// Handler which will handle all type of events from Attempted Delivery list view..
        /// </summary>
        private void AttemptedDeliveryListViewEventHandler(int attemptedDeliveryListViewEvents, params object[] data)
        {
            switch (attemptedDeliveryListViewEvents)
            {
                case AttemptedDeliveryListViewEvent.Ok:
                    _selectedConsignment = (ConsignmentEntity)data[0];
                    ExecuteActionState(FlowStateAttemptedDelivery.ActionCallVasForSelectedEntity);//ActionSendGoodsAttemptedDeliveryEvent);
                    break;

                case AttemptedDeliveryListViewEvent.Back:
                    if (_flowResult.MessageHolder == null)
                    {
                        _flowResult.State = FlowResultState.Cancel;
                        _flowResult.MessageHolder = new MessageHolder(GlobalTexts.AttemptedDeliveryAborted, MessageState.Warning);
                    }
                    ExecuteViewState(FlowStateAttemptedDelivery.ThisFlowComplete);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in AttemptedDeliveryListViewEventHandler");
                    break;
            }
        }
        /// <summary>
        /// This methos ends the Attempted Delivery flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowAttemptedDelivery");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

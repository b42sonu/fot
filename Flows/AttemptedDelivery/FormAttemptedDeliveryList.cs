﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    public partial class FormAttemptedDeliveryList : BaseForm
    {
        #region "Variables and objects"
        private FormDataAttemptedDeliveryList _formData;
        private MultiButtonControl _tabButtons;
        private ConsignmentEntity _selectedConsignmentEntity;
        #endregion

        #region "Constructor and form load related method"
        public FormAttemptedDeliveryList()
        {
            InitializeComponent();
            SetTextToGui();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
        }

        private void SetTextToGui()
        {
            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            labelModuleName.Text = GlobalTexts.AttemptedDelivery;
            labelConsignment.Text = GlobalTexts.ConsignmentItemNumber;

        }
        public override void OnShow(IFormData formData)
        {
            _formData = (FormDataAttemptedDeliveryList)formData;

            listOperations.BeginUpdate();

            if (listOperations.DataRows != null)
                listOperations.DataRows.Clear();
            if (_formData != null)
            {
                BindList(_formData.ConsignmentList);
            }
            listOperations.EndUpdate();
            SetScrollPosition();
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataAttemptedDeliveryList)iformData;
        }

        private void BindList(IEnumerable<ConsignmentEntity> list)
        {
            var colTextValues = new string[1];
            if (list != null)
            {
                if (_formData.IsDirectFromOperationList)
                {
                    listOperations.Enabled = false;
                    listOperations.MultiSelect = true;

                }
                else
                {
                    listOperations.Enabled = true;
                    listOperations.MultiSelect = false;
                }

                foreach (ConsignmentEntity consignmentEntity in list)
                {
                    if (consignmentEntity != null)
                    {
                        colTextValues[0] = consignmentEntity.ToString();
                        var insertRow = new Row(1, 2, colTextValues);
                        listOperations.DataRows.Add(insertRow);

                        if (_formData.IsDirectFromOperationList)
                            insertRow.Selected = true;
                    }
                }

                if (_formData.IsDirectFromOperationList == false && listOperations.DataRows.Count > 0)
                    listOperations.DataRows[0].Selected = true;

                MaintainSelection();
            }
        }
        private void MaintainSelection()
        {
            if (_selectedConsignmentEntity != null)
            {
                foreach (Row row in listOperations.DataRows)
                {
                    if (_selectedConsignmentEntity != null && String.CompareOrdinal(Convert.ToString(row[0]), _selectedConsignmentEntity.ToString()) == 0)
                    {
                        row.Selected = true;
                        break;
                    }
                }
            }
        }
        private void SetScrollPosition()
        {
            if (listOperations.SelectedRows != null && listOperations.SelectedRows.Any())
            {
                var row = listOperations.SelectedRows.Count() == 2
                              ? listOperations.SelectedRows[1]
                              : listOperations.SelectedRows[0];

                //var row = advancedListOperationList.DataRows[currentRowIndex + 1];
                if (row.Index > 12)  // we are using 12 beacuse we are showing 12 stop visible in one show if user want to see 13 then user need to use scroll bar for this...
                    listOperations.ScrollPos = row.Index * row.Height;
            }
        }
        #endregion

        #region "Events"
        /// <summary>
        /// event for button back after click on this button flow goes to previous caling flow and end current flow...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormAttemptedDeliveryList method FormAttemptedDeliveryList.ButtonBackClick");
                _selectedConsignmentEntity = null;
                ViewEvent.Invoke(AttemptedDeliveryListViewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormAttemptedDeliveryList.ButtonBackClick");
            }
        }

        /// <summary>
        /// event for button confirm after click on this button flow goes to next screen...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                _selectedConsignmentEntity = null;
                Logger.LogEvent(Severity.Debug, "Executing FormAttemptedDeliveryList method FormAttemptedDeliveryList.ButtonOkClick");
                if (listOperations.DataRows.Count > 0)
                {
                    //var selectedEntities = new List<ConsignmentEntity>();
                    if (listOperations.SelectedRows != null && listOperations.SelectedRow != null && (_formData.ConsignmentList != null && listOperations.SelectedRow.Selected))
                    {
                        _selectedConsignmentEntity = _formData.ConsignmentList[listOperations.SelectedRow.Index];

                    }
                    if (_selectedConsignmentEntity == null)
                        GuiCommon.ShowModalDialog(GlobalTexts.AttemptedDelivery, GlobalTexts.SelectAtLeastOneItem, Severity.Warning, GlobalTexts.Ok);
                    else
                    {
                        ViewEvent.Invoke(AttemptedDeliveryListViewEvent.Ok, _selectedConsignmentEntity);
                    }

                }
                else
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.AttemptedDelivery, GlobalTexts.NoItemInList, Severity.Warning, GlobalTexts.Ok);
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormAttemptedDeliveryList.ButtonOkClick");
            }
        }


        #endregion
    }
    public class FormDataAttemptedDeliveryList : BaseFormData
    {
        /*public string Cause;
        public string Measure;
        public string CauseCode;
        public string MeasureCode;
        public string Comment;*/
        public List<ConsignmentEntity> ConsignmentList;
        public MessageHolder MessageHolder;
        public bool IsDirectFromOperationList;
    }
    public class AttemptedDeliveryListViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;
    }

}
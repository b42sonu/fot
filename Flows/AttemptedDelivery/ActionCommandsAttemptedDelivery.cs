﻿using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Entity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    internal class ActionCommandsAttemptedDelivery
    {
        private static ICommunicationClient _communicationClient;

        public ActionCommandsAttemptedDelivery(ICommunicationClient comClient)
        {
            _communicationClient = comClient;
        }

        /// <summary>
        /// Get all planned consignments from selected planned operation when attempted delivery flow start from operation list
        /// </summary>
        internal List<ConsignmentEntity> GetPlannedConsignments(OperationProcess operationProcess)
        {
            List<ConsignmentEntity> consignmentList = null;

            if (string.IsNullOrEmpty(operationProcess.StopId) == false &&
                string.IsNullOrEmpty(operationProcess.OperationId) == false && operationProcess.IsLoadListOperation == false)
            {
                PlannedConsignmentsType[] plannedConsignments =
                    CommandsOperationList.GetPlannedConsignmentsForOperation(operationProcess);
                if (plannedConsignments != null)
                {
                    var entityMap = new EntityMap();
                    entityMap.StorePlannedConsignments(plannedConsignments);
                    consignmentList = GetConsignmentEntities(entityMap, EntityStatus.Planned, true);
                }
            }
            else if (string.IsNullOrEmpty(operationProcess.StopId) == false &&
                string.IsNullOrEmpty(operationProcess.OperationId) == false && operationProcess.IsLoadListOperation)
            {
                List<DeliveryOperation> loadlist = CommandsOperations.GetLocallyStoredLoadList(string.Empty);
                foreach (var operation in loadlist)
                {
                    if (operation != null && (operation.Consignment != null && (operation.Consignment.ConsignmentNumber.Equals(operationProcess.OperationId))))
                    {
                        PlannedConsignmentsType plannedConsignment = operation.Consignment;
                        var plannedConsignments = new PlannedConsignmentsType[1];
                        plannedConsignments[0] = plannedConsignment;
                        if (plannedConsignments.Length > 0)
                        {
                            var entityMap = new EntityMap();
                            entityMap.StorePlannedConsignments(plannedConsignments);
                            consignmentList = GetConsignmentEntities(entityMap, EntityStatus.Planned, true);
                        }
                        break;
                    }
                }
            }
            return consignmentList;
        }
        /// <summary>
        /// get consignment from scanned consignment and scanned consignment item when attempted delivery flow start from delivery scan screen ...
        /// </summary>
        internal List<ConsignmentEntity> GetConsignmentEntities(EntityMap entityMap, EntityStatus status,
                                                                   bool checkedInitialState)
        {
            var consignmentList = new List<ConsignmentEntity>();
            if (entityMap != null)
                foreach (var consignment in entityMap.Values) //loop for geting scanned consignment
                {
                    // Add scanned consignments
                    if (consignment.HasStatus(status))
                    {
                        consignmentList.Add(consignment);
                    }

                    if ((consignment is LoadCarrier) == false)
                    {
                        var consignmentItems = entityMap.GetConsignmentItems(consignment.ConsignmentId);
                        // Add scanned consignment items
                        foreach (ConsignmentItem consignmentItem in consignmentItems.Values)
                        {
                            if (consignmentItem.HasStatus(status))
                            {
                                consignmentList.Add(consignmentItem);
                            }
                        }
                    }
                }
            return consignmentList;
        }

        #region "Goods event detail"

        /// send goods event for Attempted Delivery...
        internal bool SendAttemptedDeliveryEvents(OperationProcess operationProcess,
                                                      ConsignmentEntity consignmentEntity, CauseMeasure causeMeasure, string comment)
        {
            bool result = false;
            try
            {
                //List<ConsignmentEntityRow> listLoadCarrierEntity = GetLoadCarrierEntity(consignmentList);
                string causeCode = string.Empty;
                string measureCode = string.Empty;
                if (causeMeasure != null)
                {
                    causeCode = causeMeasure.CauseCode;
                    measureCode = causeMeasure.MeasureCode;
                }
                if (consignmentEntity != null)
                    if (consignmentEntity is LoadCarrier)
                        result = SendLoadCarrierEvent(consignmentEntity.EntityId, "LBF", causeCode,
                                                          measureCode, comment);
                    else
                        result = SendGoodsEvent(operationProcess, consignmentEntity, causeCode, measureCode, comment);
            }
            catch (PreCom.Core.NotLoggedInExeption ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsAttemptedDelivery.SendAttemptedDeliveryGoodsEvent");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsAttemptedDelivery.SendAttemptedDeliveryGoodsEvent");
            }

            return result;
        }

        private bool SendGoodsEvent(OperationProcess operationProcess, ConsignmentEntity consignmentEntity, string causeCode,
                                    string measureCode, string comment)
        {
            var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity);
            if (goodsEvent != null && goodsEvent.EventInformation != null)
            {
                goodsEvent.EventInformation.CauseCode = causeCode;
                goodsEvent.EventInformation.ActionCode = measureCode;
                goodsEvent.EventInformation.EventComment = comment;
            }

            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GoodsEventsEventInformation,
                                              consignmentEntity != null ? consignmentEntity.EntityId : null);
            if (_communicationClient != null)
                _communicationClient.SendGoodsEvent(goodsEvent, transaction);
            return true;
        }

        #endregion

        /// <summary>
        /// method for get load carrier entities from entity map..
        /// </summary>
        /// <param name="consignmentList"></param>
        /// <returns></returns>
        internal List<ConsignmentEntity> GetLoadCarrierEntity(List<ConsignmentEntity> consignmentList)
        {
            var list = new List<ConsignmentEntity>();

            if (consignmentList != null)
            {
                foreach (var consignmentEntity in consignmentList)
                {
                    if (consignmentEntity is LoadCarrier)
                        list.Add(consignmentEntity);
                }
            }
            return list;
        }


        #region "Load Carrier event"
        /// <summary>
        /// method for send load carrier event...
        /// </summary>
        public bool SendLoadCarrierEvent(string loadcarrier, string eventCode, string reasonCode, string actionCode, string comment)
        {
            if (string.IsNullOrEmpty(loadcarrier))
            {
                return false;
            }
            var eventInfo = LoadCarrierEventHelper.CreateLoadCarrierEvent(loadcarrier, null, null, null, null, eventCode, null);
            if (eventInfo != null)
            {
                eventInfo.EventInformation.ActionCode = actionCode;
                eventInfo.EventInformation.ReasonCode = reasonCode;
                eventInfo.EventInformation.Comment = comment;
                if (CommunicationClient.Instance != null)
                {
                    CommunicationClient.Instance.SendLoadCarrierEvent(eventInfo);
                }
            }
            return true;
        }
        #endregion




        /// <summary>
        /// method for remove selected consignemnt and consignment item from scanning list after sending good event...
        /// </summary>
        internal void RemoveConsignmentFromCache(EntityMap entityMap, ConsignmentEntity consignmentEntity, List<DeliveryOperation> loadList)
        {
            if (consignmentEntity != null)
            {
                if (loadList != null && loadList.Any())
                    AdditionalCommands.UpdateLoadListOperationStatus(loadList, consignmentEntity.ConsignmentId);
                if (entityMap != null && entityMap.Count > 0)
                    entityMap.RemoveEntity(consignmentEntity);
            }
        }

        /// <summary>
        /// method for verify flow result when back from departure from stop flow..
        /// </summary>
        /// <param name="flowResult"></param>
        public void VerifyFlowDepartureFromStopResult(FlowResultDepartureFromStop flowResult)
        {
            Logger.LogEvent(Severity.Debug,
                            "Executing command ActionCommandsAttemptedDelivery.VerifyFlowDepartureFromStopResult()");
        }

        //US1r  - 1r Returned delivery operation

        #region "method related to 1r user story(Returned delivery operation)"

        /// <summary>
        /// this method used for create the returned operation for delivery operation 
        /// </summary>
        internal bool CreateReturnedDeliveryOperation(OperationProcess operationProcess, List<DeliveryOperation> loadList)
        {
            try
            {
                if (operationProcess != null && operationProcess.OperationId != null)
                {
                    var stopId = string.Format("RD_{0}", operationProcess.OperationId);
                    var stop = CommandsOperationList.GetStop(stopId);
                    if (operationProcess.IsLoadListOperation && stop == null && loadList != null)
                    {
                        List<DeliveryOperation> operationsFromLoadlist = loadList;//CommandsOperations.GetLocallyStoredLoadList(string.Empty);
                        DeliveryOperation deliveryOperation = operationsFromLoadlist.FirstOrDefault(operation => operation.Consignment.ConsignmentNumber == operationProcess.OperationId);

                        if (deliveryOperation != null)
                        {
                            var operationListStop = new OperationListStop();
                            var stopType = new StopType { StopId = stopId };
                            operationListStop.Route = deliveryOperation.RouteId;
                            operationListStop.TripId = deliveryOperation.TripId;
                            operationListStop.ExternalTripId = deliveryOperation.ExternalTripId;

                            operationListStop.StopInformation = stopType;
                            var operationType = new PlannedOperationType
                                                    {
                                                        OperationId = stopId,
                                                        OperationType = PlannedOperationTypeOperationType.RETURNEDDELIVERY,
                                                        Status = StopOperationType.New,
                                                        PlannedConsignments = new PlannedConsignmentsType[1]
                                                    };
                            operationType.PlannedConsignments[0] = deliveryOperation.Consignment;
                            operationType.Route = deliveryOperation.RouteId;
                            operationType.ExternalTripId = deliveryOperation.ExternalTripId;
                            operationType.TripId = deliveryOperation.TripId;
                            operationType.LoadCarrierId = deliveryOperation.LogicalLoadCarrierId;



                            operationListStop.PlannedOperation = new PlannedOperationType[1];
                            operationListStop.PlannedOperation[0] = operationType;

                            if (ModelOperationList.OperationList != null)
                            {
                                if (ModelOperationList.OperationList.Stop != null)
                                {
                                    var list = ModelOperationList.OperationList.Stop.ToList();
                                    list.Add(operationListStop);
                                    ModelOperationList.OperationList.Stop = list.ToArray();
                                }
                            }
                            else
                            {
                                ModelOperationList.OperationList = new OperationListEx();
                                var operationListStops = new OperationListStop[1];
                                operationListStops[0] = operationListStop;
                                ModelOperationList.OperationList.Stop = operationListStops;
                            }

                        }

                    }
                    /*else
                    {

                        var stop = CommandsOperationList.GetStop(stopId);
                        if (stop == null)
                        {
                            var operationListStop = BaseActionCommands.GetCopyOfObject<OperationListStop>(
                                    CommandsOperationList.GetStop(operationProcess.StopId));
                            if (operationListStop != null)
                            {
                                operationListStop.StopInformation.StopId = stopId;
                                //PlannedOperationType plannedOperation = new PlannedOperationType();
                                var plannedOperation = BaseActionCommands.GetCopyOfObject<PlannedOperationType>(
                                        OperationCommands.GetPlannedOperation(operationListStop, operationProcess.OperationId));
                                if (plannedOperation != null)
                                {
                                    plannedOperation.OperationType = PlannedOperationTypeOperationType.RETURNEDDELIVERY;
                                    plannedOperation.Status = StopOperationType.New;
                                    var plannedOperationArray = new PlannedOperationType[1];
                                    plannedOperationArray[0] = plannedOperation;
                                    operationListStop.PlannedOperation = plannedOperationArray;
                                    if (ModelOperationList.OperationList != null)
                                    {
                                        var list = ModelOperationList.OperationList.Stop.ToList();
                                        list.Add(operationListStop);
                                        ModelOperationList.OperationList.Stop = list.ToArray();
                                    }
                                }
                            }
                        }
                    }*/
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsAttemptedDelivery.CreateReturnedDeliveryOperation");
                return false;
            }
            return true;
        }

        #endregion

        /// <summary>
        /// method for verify flow result when back from departure from stop flow..
        /// </summary>
        public bool VerifyFlowResultCauseAndMeasure(FlowResultCauseAndMeasure flowResultCauseAndMeasure,
                                                    FlowResultAttemptedDelivery flowResultAttemptedDelivery,
                                                   ref CauseMeasure causeMeasure, out string comment)
        {
            comment = null;
            var isValid = flowResultCauseAndMeasure != null && flowResultCauseAndMeasure.State == FlowResultState.Ok;

            if (isValid == false && flowResultAttemptedDelivery != null)
            {
                flowResultAttemptedDelivery.State = FlowResultState.Cancel;
                flowResultAttemptedDelivery.MessageHolder = new MessageHolder(GlobalTexts.AttemptedDeliveryAborted, MessageState.Warning);
            }
            else if (isValid && flowResultCauseAndMeasure.SelectedReasonAndActionCombination != null)
            {

                causeMeasure = flowResultCauseAndMeasure.SelectedReasonAndActionCombination;
                comment = flowResultCauseAndMeasure.Comment;
            }
            return isValid;
        }

        /// <summary>
        /// Back from vas flow...
        /// </summary>
        internal FlowStateAttemptedDelivery BackFromExecuteVas(FlowResultVasMatrix subflowResult, ref ConsignmentEntity consignmentEntity)
        {
            var state = FlowStateAttemptedDelivery.ActionGetConsignmentList;//ThisFlowComplete;
            if (subflowResult != null)
                if (subflowResult.State == FlowResultState.Ok)
                    state = FlowStateAttemptedDelivery.FlowCauseAndMeasure;
                else
                {
                    consignmentEntity = null;
                }
            return state;
        }

        /// <summary>
        /// call vas flow for selected entities one by one..
        /// </summary>
        internal FlowStateAttemptedDelivery CallVasForSelectedEntity(ConsignmentEntity consignmentEntity)
        {
            var state = FlowStateAttemptedDelivery.FlowExecuteVasMatrix;
            if (consignmentEntity is LoadCarrier)
                state = FlowStateAttemptedDelivery.FlowCauseAndMeasure;
            return state;
        }

        internal bool IsSingleEntity(List<ConsignmentEntity> consignmentList)
        {
            var isValid = consignmentList != null && consignmentList.Count == 1;
            return isValid;
        }
    }
}

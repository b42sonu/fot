﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery
{
    class ViewCommandsAttemptedDelivery
    {
        /// <summary>
        /// method for show FormAttemptedDeliveryList view...
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="formDataAttemptedDeliveryList"> </param>
        /// <returns></returns>
        internal FormAttemptedDeliveryList ShowAttemptedDeliveryListView(ViewEventDelegate viewEventHandler, FormDataAttemptedDeliveryList formDataAttemptedDeliveryList)
        {
             var view = ViewCommands.ShowView<FormAttemptedDeliveryList>(formDataAttemptedDeliveryList);
            view.SetEventHandler(viewEventHandler);
            return view;
        }
    }
}

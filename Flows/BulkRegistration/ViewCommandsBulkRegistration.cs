﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration
{
    class ViewCommandsBulkRegistration 
    {
        /// <summary>
        /// Open Bulk registration form
        /// </summary>
        internal FormBulkRegistration FormBulkRegistration(ViewEventDelegate viewEventHandler, Entity.EntityMap.ConsignmentEntity currentConsignmentEntity, FlowDataBulkRegistration flowData, MessageHolder messageHolder)
        {
            var formData = new FormDataBulkRegistration
                               {
                                   ConsignmentEntity = currentConsignmentEntity,
                                   HeaderText = flowData.HeaderText,
                                   MessageHolder = messageHolder
                               };

            var view = ViewCommands.ShowView<FormBulkRegistration>(formData);
            view.SetEventHandler(viewEventHandler);
            return view;

        }
    }
}

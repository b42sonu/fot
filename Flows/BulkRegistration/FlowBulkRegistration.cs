﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration
{

    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateBulkRegistration
    {
        ActionValidateInputs,
        ActionIsValidConsignment,
        ActionIsConsignmentMassRegistrationAllowed,
        ActionSendConsignmentEvent,
        ActionValidateGoods,
        ViewCommands,
        ViewRegisterConsignmentItems,
        FlowCanceled,
        FlowError,
        ThisFlowComplete
    }

    public class FlowDataBulkRegistration : BaseFlowData
    {
        public ConsignmentEntity ConsignmentEntity { get; set; }
        public bool CalledFromOperationList { get; set; }
        public EntityMap EntityMap { get; set; }
        //  TODO:Add mass registration check in this flow
    }

    public class FlowResultBulkRegistration : BaseFlowResult
    {
        public ConsignmentEntity ConsignmentEntity { get; set; }
    }

    /// <summary>
    /// US 38 http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554929
    /// </summary>
    public class FlowBulkRegistration : BaseFlow
    {
        private readonly FlowResultBulkRegistration _flowResult;

        private readonly ViewCommandsBulkRegistration _viewCommand;
        private ConsignmentEntity _currentConsignmentEntity;
        private FlowDataBulkRegistration _flowData;
        private readonly ActionCommandsBulkRegistration _actionCommandsBulkRegistration;
        private readonly ActionCommandsScanBarcode _actionCommandsScanBarcode;
        private string _consignmentItemScanned = string.Empty;
        private MessageHolder _messageHolder;

        public FlowBulkRegistration()
        {
            _flowResult = new FlowResultBulkRegistration();
            _viewCommand = new ViewCommandsBulkRegistration();
            _actionCommandsBulkRegistration = new ActionCommandsBulkRegistration(CommunicationClient.Instance);
            _actionCommandsScanBarcode = new ActionCommandsScanBarcode(_messageHolder);
            _messageHolder = new MessageHolder();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowBulkRegistration.BeginFlow(IFlowData flowData)");
            _flowData = (FlowDataBulkRegistration)flowData;
            _currentConsignmentEntity = _flowData.ConsignmentEntity;
            if (_flowData.CalledFromOperationList)
                ExecuteActionState(FlowStateBulkRegistration.ActionValidateGoods);
            else
            {
                ExecuteState(FlowStateBulkRegistration.ActionIsValidConsignment);
            }

        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateBulkRegistration)state);
        }

        public void ExecuteState(FlowStateBulkRegistration state)
        {

            if (state > FlowStateBulkRegistration.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);

        }

        private void ExecuteActionState(FlowStateBulkRegistration state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowStateBulkRegistration;" + state);
                    bool result;
                    switch (state)
                    {

                        case FlowStateBulkRegistration.ActionIsValidConsignment:
                            result = _actionCommandsBulkRegistration.ValidateConsignmentEntity(_currentConsignmentEntity, ref _messageHolder);
                            state = result
                                        ? FlowStateBulkRegistration.ActionIsConsignmentMassRegistrationAllowed
                                        : FlowStateBulkRegistration.FlowError;
                            break;

                        case FlowStateBulkRegistration.ActionIsConsignmentMassRegistrationAllowed:
                            result = _actionCommandsBulkRegistration.IsMassRegistrationAllowed(_currentConsignmentEntity);
                            if (result == false && _flowData.CalledFromOperationList)
                                GuiCommon.ShowModalDialog(GlobalTexts.BulkRegistration,
                                                          GlobalTexts.BulkRegistrationNotAllowed,Severity.Error, GlobalTexts.Ok);
                            state = result ? FlowStateBulkRegistration.ViewRegisterConsignmentItems : FlowStateBulkRegistration.FlowError;
                            break;

                        case FlowStateBulkRegistration.ActionValidateInputs:
                            var entityMap = _flowData.EntityMap;
                            state = _actionCommandsBulkRegistration.SetNextState(_consignmentItemScanned, _currentConsignmentEntity, ref entityMap);
                            break;

                        case FlowStateBulkRegistration.ActionSendConsignmentEvent:
                            _actionCommandsBulkRegistration.SendBulkRegistrationGoodsEvent(_currentConsignmentEntity, ModelMain.SelectedOperationProcess);
                            _flowResult.ConsignmentEntity = _currentConsignmentEntity;
                            state = FlowStateBulkRegistration.ThisFlowComplete;
                            break;

                        case FlowStateBulkRegistration.ActionValidateGoods:
                            _actionCommandsScanBarcode.ValidateGoods(new EntityMap(), _currentConsignmentEntity);
                            state = FlowStateBulkRegistration.ActionIsValidConsignment;
                            break;

                        default:
                            Logger.LogEvent(Severity.Error, "FlowStateBulkRegistration.ExecuteActionState: Encountered unhandled state");
                            break;
                    }
                } while (state < FlowStateBulkRegistration.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Failed to execute ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateBulkRegistration.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        private void ExecuteViewState(FlowStateBulkRegistration state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowStateBulkRegistration;" + state);
                switch (state)
                {
                    case FlowStateBulkRegistration.ViewRegisterConsignmentItems:
                        _viewCommand.FormBulkRegistration(BulkRegistrationEventHandler, _currentConsignmentEntity, _flowData, _messageHolder);
                        break;

                    case FlowStateBulkRegistration.FlowCanceled:
                        _flowResult.State = FlowResultState.Cancel;
                        _flowResult.Message = GlobalTexts.BulkRegistrationFlowAborted;
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    case FlowStateBulkRegistration.FlowError:
                        _flowResult.State = FlowResultState.Error;
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    case FlowStateBulkRegistration.ThisFlowComplete:
                        _flowResult.State = FlowResultState.Ok;
                        _flowResult.Message = GlobalTexts.BulkRegistrationCompleted;
                        _flowResult.ConsignmentEntity = _currentConsignmentEntity;
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "FlowStateBulkRegistration.ExecuteViewState: Encountered unhandled state");
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowStateBulkRegistration.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateBulkRegistration.ThisFlowComplete);
            }
        }

        private void BulkRegistrationEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case BulkRegistrationViewEvents.Ok:
                    if (_currentConsignmentEntity.IsScannedOnline)
                        _consignmentItemScanned = Convert.ToString(data[0]);
                    else
                    {
                        _consignmentItemScanned = _consignmentItemScanned = Convert.ToString(data[1]);
                        Convert.ToString(data[0]);
                    }

                    ExecuteState(FlowStateBulkRegistration.ActionValidateInputs);
                    break;

                case BulkRegistrationViewEvents.Back:
                    ExecuteState(FlowStateBulkRegistration.FlowCanceled);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "FlowBulkRegistration.EventVerificationEventHandler: Encountered unhandled state");
                    break;
            }
        }
    }
}

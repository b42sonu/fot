﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Utils;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration
{
    partial class FormBulkRegistration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.txtItemScannedCount = new PreCom.Controls.PreComInput2();
            this.txtItemCount = new PreCom.Controls.PreComInput2();
            this.lblScannedItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblItemCountFromLM = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblItemCount = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblItem = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblScannedItems)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCountFromLM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.txtItemScannedCount);
            this.touchPanel.Controls.Add(this.txtItemCount);
            this.touchPanel.Controls.Add(this.lblScannedItems);
            this.touchPanel.Controls.Add(this.lblItemCountFromLM);
            this.touchPanel.Controls.Add(this.lblConsignmentValue);
            this.touchPanel.Controls.Add(this.lblItemCount);
            this.touchPanel.Controls.Add(this.lblItem);
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.lblConsignment);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // txtItemScannedCount
            // 
            this.txtItemScannedCount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtItemScannedCount.Location = new System.Drawing.Point(372, 232);
            this.txtItemScannedCount.Name = "txtItemScannedCount";
            this.txtItemScannedCount.Size = new System.Drawing.Size(105, 48);
            this.txtItemScannedCount.TabIndex = 35;
            this.txtItemCount.MaxLength = 4;
            this.txtItemScannedCount.Text = "0";
            this.txtItemScannedCount.TextTranslation = false;
            // 
            // txtItemCount
            // 
            this.txtItemCount.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtItemCount.Location = new System.Drawing.Point(371, 177);
            this.txtItemCount.Name = "txtItemCount";
            this.txtItemCount.Size = new System.Drawing.Size(105, 48);
            this.txtItemCount.TabIndex = 34;
            this.txtItemCount.MaxLength = 4;
            this.txtItemCount.Text = "0";
            this.txtItemCount.TextTranslation = false;
            // 
            // lblScannedItems
            // 
            this.lblScannedItems.Location = new System.Drawing.Point(15, 240);
            this.lblScannedItems.Name = "lblScannedItems";
            this.lblScannedItems.Size = new System.Drawing.Size(281, 29);
            // 
            // lblItemCountFromLM
            // 
            this.lblItemCountFromLM.Location = new System.Drawing.Point(213, 125);
            this.lblItemCountFromLM.Name = "lblItemCountFromLM";
            this.lblItemCountFromLM.Size = new System.Drawing.Size(14, 29);
            this.lblItemCountFromLM.Text = "0";
            // 
            // lblConsignmentValue
            // 
            this.lblConsignmentValue.Location = new System.Drawing.Point(172, 90);
            this.lblConsignmentValue.Name = "lblConsignmentValue";
            this.lblConsignmentValue.Size = new System.Drawing.Size(132, 29);
            // 
            // lblItemCount
            // 
            this.lblItemCount.Location = new System.Drawing.Point(15, 189);
            this.lblItemCount.Name = "lblItemCount";
            this.lblItemCount.Size = new System.Drawing.Size(354, 29);
            // 
            // lblItem
            // 
            this.lblItem.Location = new System.Drawing.Point(15, 125);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(189, 29);
            // 
            // messageControlValidateScan
            // 
            this.messageControlValidateScan.Location = new System.Drawing.Point(15, 318);
            this.messageControlValidateScan.MessageText = string.Empty;
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(452, 97);
            this.messageControlValidateScan.TabIndex = 14;
           
            // 
            // lblConsignment
            // 
            this.lblConsignment.Location = new System.Drawing.Point(15, 90);
            this.lblConsignment.Name = "lblConsignment";
            this.lblConsignment.Size = new System.Drawing.Size(138, 29);
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 42);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;

            // 
            // FormRegisterTemperature
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormBulkRegistration";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblScannedItems)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCountFromLM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItemCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignment;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControlValidateScan;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private TransparentLabel lblItemCount;
        private TransparentLabel lblItem;
        private TransparentLabel lblConsignmentValue;
        private TransparentLabel lblItemCountFromLM;
        private TransparentLabel lblScannedItems;
        private PreCom.Controls.PreComInput2 txtItemScannedCount;
        private PreCom.Controls.PreComInput2 txtItemCount;
    }
}
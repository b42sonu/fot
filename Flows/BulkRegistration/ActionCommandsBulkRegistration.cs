﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration
{
    class ActionCommandsBulkRegistration
    {
        //TODO :: Write Unit Test
        private static ICommunicationClient _communicationClient;
        public ActionCommandsBulkRegistration(ICommunicationClient comClient)
        {
            _communicationClient = comClient;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Checks whether consignmentEntity is consignment and set offline message if scanned offline
        /// </summary>
        internal bool ValidateConsignmentEntity(ConsignmentEntity currentConsignmentEntity, ref MessageHolder messageHolder)
        {
            if (currentConsignmentEntity is Consignment)
            {
                if (currentConsignmentEntity.IsScannedOnline == false)
                {
                    messageHolder.Update(GlobalTexts.PdaOfflineDuringRequest, MessageState.Warning);
                }
                return true;
            }
            
            messageHolder.Update(GlobalTexts.InvalidInput, MessageState.Error);
            return false;
        }

        //TODO :: Write Unit Test
        internal bool ContainsConsignmentInformation(ConsignmentEntity currentConsignmentEntity)
        {
            if (currentConsignmentEntity.IsScannedOnline)
                return true;
            return false;
        }
        /// <summary>
        /// Should open bulk registration 
        /// </summary>
        internal bool IsMassRegistrationAllowed(ConsignmentEntity currentConsignmentEntity)
        {
            if (currentConsignmentEntity.IsScannedOnline)//If scanned online check property
                return currentConsignmentEntity.IsMassRegistrationAllowed;
            return true;//If offline he can perform 
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// If Scanned item count equals to 0 , no need to send consignment event
        /// </summary>
        internal FlowStateBulkRegistration SetNextState(string scannedItems, ConsignmentEntity consignmentEntity,ref EntityMap entityMap)
        {
            var consignment = consignmentEntity as Consignment;
            if (consignment != null)
            {
                consignment.ManuallyEnteredConsignmentItemCount = Convert.ToInt32(scannedItems);
                var consignmentFromEntityMap = entityMap.GetConsignment(consignment.ConsignmentId);
                if (consignmentFromEntityMap != null)
                consignmentFromEntityMap.ManuallyEnteredConsignmentItemCount = Convert.ToInt32(scannedItems);
            }
                
            //If 0 donot send Goods Event and complete flow 
            FlowStateBulkRegistration state = Convert.ToInt32(scannedItems) == 0 ? FlowStateBulkRegistration.ThisFlowComplete : FlowStateBulkRegistration.ActionSendConsignmentEvent;
            return state;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// send Consignment goods event for Bulk Registration
        /// </summary>
        public bool SendBulkRegistrationGoodsEvent(ConsignmentEntity consignmentEntity, OperationProcess operationProcess)
        {
            bool result = false;

            try
            {
                PreComFW.CommunicationEntity.GoodsEvents.GoodsEvents goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity);

                if (goodsEvent != null)
                {
                    if (goodsEvent.EventInformation.Consignment.Any())
                    {
                        goodsEvent.EventInformation.Consignment[0].MassRegistration = true;
                        goodsEvent.EventInformation.Consignment[0].ConsignmentItemsHandled = ((Consignment)consignmentEntity).ManuallyEnteredConsignmentItemCount;
                    }

                    var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GoodsEventsEventInformation, consignmentEntity.OrderNumber);

                    if (_communicationClient != null)//CommunicationClient.Instance != null)
                    {
                        _communicationClient.SendGoodsEvent(goodsEvent, transaction);
                        result = true;
                    }
                }
            }


            catch (PreCom.Core.NotLoggedInExeption ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsBulkRegistration.SendBulkRegistrationGoodsEvent");
            }
            catch (Exception ex)
            {
                //FlowResult.State = FlowResultState.Error;
                Logger.LogException(ex, "ActionCommandsBulkRegistration.SendBulkRegistrationGoodsEvent");
            }

            return result;
        }

    }
}

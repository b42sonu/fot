﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration
{
    /// <summary>
    /// US 38 http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554929
    /// </summary>
    public partial class FormBulkRegistration : BaseForm
    {
        #region local variables
        private FormDataBulkRegistration _formData;
        private readonly MultiButtonControl _tabButtons;
        private const string BackButton = "backButton";
        private const string OkButton = "okButton";

        #endregion

        #region constructor
        public FormBulkRegistration()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            InitOnScreenKeyBoardProperties();
            SetTextToGui();
        }
        #endregion

        #region Overriden Methods
        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            _formData = (FormDataBulkRegistration)formData;
            ClearStates();
            if (_formData != null)
            {
                lblConsignmentValue.Text = _formData.ConsignmentEntity.ConsignmentId;
                SetGuiAccordingToItem();
                lblModuleName.Text = _formData.HeaderText;
            }
        }

        public override void OnUpdateView(IFormData formData)
        {
            _formData = (FormDataBulkRegistration)formData;
            lblConsignmentValue.Text = _formData.ConsignmentEntity.ConsignmentId;
            lblModuleName.Text = _formData.HeaderText;
        }

        #endregion

        #region private methods
        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty);
            txtItemCount.Tag = keyBoardProperty;
            txtItemScannedCount.Tag = keyBoardProperty;

        }

        private void SetTextToGui()
        {
            lblScannedItems.Text = GlobalTexts.NoOfItemsScanned + " :";
            lblItemCount.Text = GlobalTexts.CorrectNumberOfItems + " :";
            lblConsignment.Text = GlobalTexts.Consignment + " :";
            lblItem.Text = GlobalTexts.NumberOfItems + " :";
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = OkButton });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
            lblHeading.Text = ModelUser.UserProfile.OrgUnitName;
        }
        /// <summary>
        /// Validate consignment count and scanned consignment count
        /// </summary>
        /// <returns></returns>
        private bool ValidateInputs()
        {
            if (!IsValidIntegers())
                return false;

            if ((!String.IsNullOrEmpty(txtItemCount.Text.Trim())))
            {
                if (_formData.ConsignmentEntity.IsKnownInLm)
                {
                    var messageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.BulkRegistration, String.Format(GlobalTexts.NewNumberOfItemsWillBeSet, txtItemCount.Text.Trim()),
                                        Severity.Info, GlobalTexts.Ok, GlobalTexts.Back);
                    if (messageBoxResult == GlobalTexts.Ok)
                    {
                        return true;

                    }
                }
                else
                {
                    var messageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.BulkRegistration, String.Format(GlobalTexts.ScannedAndTotalItem, txtItemCount.Text.Trim(), txtItemScannedCount.Text.Trim()),
                                        Severity.Info, GlobalTexts.Ok, GlobalTexts.Back);
                    if (messageBoxResult == GlobalTexts.Ok)
                    {
                        return true;
                    }
                }

                return false;
            }
            return false;
        }

        private bool IsValidIntegers()
        {
            bool result = true;
            var regex = new Regex(@"^[-+]?[0-9]+([\.][0-9]{1,})?$");
            if (!regex.IsMatch(txtItemCount.Text.Trim()))
            {
                GuiCommon.ShowModalDialog(GlobalTexts.BulkRegistration, GlobalTexts.PleaseEnterValidCount, Severity.Info, GlobalTexts.Ok)
                ;
                result = false;
            }
            if (!_formData.ConsignmentEntity.IsKnownInLm)
            {
                if (!regex.IsMatch(txtItemScannedCount.Text.Trim()))
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.BulkRegistration, GlobalTexts.PleaseEnterValidScannedCount, Severity.Info, GlobalTexts.Ok)
                        ;
                    result = false;
                }
            }
            if (txtItemCount.Text.Trim().Length > 5)//Text should not exceed overflow
            {
                GuiCommon.ShowModalDialog(GlobalTexts.BulkRegistration, GlobalTexts.PleaseEnterValidCount, Severity.Info, GlobalTexts.Ok)
                        ;
                result = false;
            }

            return result;
        }



        private void ClearStates()
        {
            txtItemCount.Text = String.Empty;
            txtItemScannedCount.Text = String.Empty;
        }

        private void SetGuiAccordingToItem()
        {
            if (_formData.ConsignmentEntity != null)
            {
                if (_formData.ConsignmentEntity.IsScannedOnline && _formData.ConsignmentEntity.IsKnownInLm)
                {
                    lblItemCount.Text = GlobalTexts.CorrectNumberOfItems;
                    lblScannedItems.Visible = false;
                    txtItemScannedCount.Visible = false;
                    lblItem.Visible = true;
                    messageControlValidateScan.ClearMessage();
                    lblItemCountFromLM.Visible = true;
                    lblItemCountFromLM.Text = _formData.ConsignmentEntity.ConsignmentItemCountFromLm.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    lblItemCount.Text = GlobalTexts.NumberOfItemsInConsignment;
                    txtItemScannedCount.Visible = true;
                    txtItemCount.Text = "0";
                    lblItem.Visible = false;
                    lblScannedItems.Visible = true;
                    messageControlValidateScan.ShowMessage(_formData.MessageHolder);
                    lblItemCountFromLM.Visible = false;
                }
            }
            else
            {
                lblItemCount.Text = GlobalTexts.NumberOfItemsInConsignment;
            }
        }
        #endregion

        #region Events
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(BulkRegistrationViewEvents.Back, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormBulkRegistration.ButtonBackClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if (ValidateInputs())
                    ViewEvent.Invoke(BulkRegistrationViewEvents.Ok, txtItemCount.Text, txtItemScannedCount.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormBulkRegistration.ButtonOkClick");
            }
        }
        #endregion

    }

    #region external classes
    public class BulkRegistrationViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;

    }

    public class FormDataBulkRegistration : BaseFormData
    {
        public ConsignmentEntity ConsignmentEntity { get; set; }
        public MessageHolder MessageHolder { get; set; }
    }
    #endregion
}
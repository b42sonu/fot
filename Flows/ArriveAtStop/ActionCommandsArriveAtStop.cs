﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationListEvent;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.ArriveAtStop
{
    class ActionCommandsArriveAtStop
    {
        internal bool IsValidateStopRequired()
        {
            return true; //TODO:currently we are not using this setting when we will use we need to send result parameter.
        }

        internal bool IsStopEventArrivedRequired()
        {
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsSendingStopEventArrivedRequired);
        }

        //Check TMI setting for Operation Started event
        internal bool IsOperationEventStartRequired()
        {
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsOperationEventStartRequired);
        }

        /// <summary>
        /// method for sedn async request for selected planned operation if all the operation are new...
        /// </summary>
        internal bool SendOperationListEventAsyncReq(OperationListStop operationListStop, PlannedOperationType plannedOperation, string stopId, string tripId, string loadCarrier, string operationStatus)
        {
            var operationListEvent = new OperationListEvent
            {
                CaptureEquipment = OperationListEventHelper.GetOperationListEventCaptureEquipment(),
                EventInformation = OperationListEventHelper.GetOperationListEventInformation(plannedOperation, operationStatus),
                LocationInformation = OperationListEventHelper.GetOperationListEventLocationInformation(operationListStop),
                OperationInformation = OperationListEventHelper.GetOperationListEventOperationInformation(plannedOperation, stopId, tripId, loadCarrier),
                UserInformation = OperationListEventHelper.GetOperationListEventUserInformation(),
                LoggingInformation = LoggingInformation.GetOperationListEventLoggingInformation(),
            };

#pragma warning disable 618
            if (PreCom.Application.Instance != null && (PreCom.Application.Instance.Communication != null && PreCom.Application.Instance.Communication.GetClient().CredentialId > 0))
#pragma warning restore 618
            {
                var objTran = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.OperationListEvent, "Stop Id:" + stopId);

                CommunicationClient.Instance.SendOperationListEvent(operationListEvent, objTran);
            }
            return true;
        }


        /// <summary>
        /// method for send arrive at stop retuest when all operation new in stop...
        /// </summary>
        /// <param name="operationProcess">Specifies object for operation process, having information regarding current operation.</param>
        /// <param name="arrivingAtOperationListStop"> </param>
        /// <param name="loadCarrier">Specifies the loadcarrier currently in use.</param>
        /// <returns></returns>
        internal bool SendArriveAtStopRequest(OperationProcess operationProcess, bool arrivingAtOperationListStop, string loadCarrier)
        {
            bool result = false;
            try
            {
                //If user started from operation list/ or operations from operation list
                if (arrivingAtOperationListStop)
                {
                    //Get current stop
                    var stop = CommandsOperationList.GetStop(operationProcess.StopId);
                    if (stop != null && stop.PlannedOperation != null)
                    {
                        //get current planned operation
                        var plannedOperation = OperationCommands.GetPlannedOperation(stop, operationProcess.OperationId);
                        //Get count of operations with status other than, new
                        var countOfStartedOrFinished = stop.PlannedOperation.Count(c => !c.Status.Equals(StopOperationType.New));
                        //If count of operations with status other than new is 0, it means user came
                        //at stop first time, so send Arrive at stop event.
                        if (countOfStartedOrFinished == 0)
                            result = SendOperationListEventAsyncReq(stop, plannedOperation, operationProcess.StopId, operationProcess.TripId, loadCarrier, StopOperationType.Arrived);
                    }
                }
                else
                {
                    result = SendOperationListEventAsyncReq(null, null, operationProcess.StopId, operationProcess.TripId, loadCarrier, StopOperationType.Arrived);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OperationListCommands.SendAyncStopStart()");
                throw;
            }
            return result;
        }

        //Sends the operation started event to server
        internal bool SendOperationStartedRequest(OperationProcess operationProcess, bool arrivingAtOperationListStop, string loadCarrier)
        {
            if (ModelUser.UserProfile.TmsAffiliation != TmsAffiliation.Amphora)//This event will be sent only for Amphora
                return false;
            bool result = false;

            try
            {
                Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsArriveAtStop.SendOperationStartedRequest()");
                //If user started from operation list/ or operations from operation list
                if (arrivingAtOperationListStop)
                {
                    //Get current stop
                    var stop = CommandsOperationList.GetStop(operationProcess.StopId);
                    if (stop != null && stop.PlannedOperation != null)
                    {
                        //get current planned operation
                        var plannedOperation = OperationCommands.GetPlannedOperation(stop, operationProcess.OperationId);
                        //Send Operation Started Event if operation status is new
                        if (plannedOperation.Status == StopOperationType.New)
                            result = SendOperationListEventAsyncReq(stop, plannedOperation, operationProcess.StopId, operationProcess.TripId, loadCarrier, StopOperationType.Started);

                    }
                }
                else
                {
                    result = SendOperationListEventAsyncReq(null, null, operationProcess.StopId, operationProcess.TripId, loadCarrier, StopOperationType.Started);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OperationListCommands.SendAyncStopStart()");
                throw;
            }
            return result;
        }

        /// <summary>
        /// method for updated the operation list status and marked status "started" for selected operation..
        /// </summary>
        /// <param name="operationProcess"></param>
        /// <returns></returns>
        internal bool UpdateSelectedOperationStatus(OperationProcess operationProcess)
        {
            if (ModelMain.SelectedOperationProcess == null)
                return false;

            bool result = false;
            var stop = CommandsOperationList.GetStop(operationProcess.StopId);
            if (stop != null)
            {
                var plannedOperation = OperationCommands.GetPlannedOperation(stop, operationProcess.OperationId);
                if (plannedOperation != null)
                {
                    if (String.CompareOrdinal(plannedOperation.Status, StopOperationType.New) == 0)
                        plannedOperation.Status = StopOperationType.Started;
                    result = true;
                }
            }
            return result;
        }

    }
}

﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using System;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
// US 1d: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7934332
namespace Com.Bring.PMP.PreComFW.Shared.Flows.ArriveAtStop
{
    /// <summary>
    /// Enum which specifies the collection of StateUnplannedPickUp for this flow.
    /// </summary>
    public enum FlowStateArriveAtStop
    {
        ActionValidateStopRequired,
        ActionStopEventRequired,
        ActionSendArriveAtStopEvent,
        ActionStartOperationEventRequired,
        ActionSendOperationStartedRequest,
        ActionUpdateOperationStatus,
        ActionOperationEventStartedRequired,
        ActionPersistOperationListChanges,
        ActionStartedFromOperationList,
        ViewCommands,

        ThisFlowComplete,
    }

    public class FlowDataArriveAtStop : BaseFlowData
    {
        public bool StartedFromOperationList { get; set; }
        public string LoadCarrierId { get; set; }
    }

    public class FlowResultArriveAtStop : BaseFlowResult
    {
    }

    public class FlowArriveAtStop : BaseFlow
    {
        private readonly ActionCommandsArriveAtStop _actionCommandArriveAtStop;
        private readonly FlowResultArriveAtStop _flowResult;
        private FlowDataArriveAtStop _flowData;

        public FlowArriveAtStop()
        {
            _actionCommandArriveAtStop = new ActionCommandsArriveAtStop();
            _flowResult = new FlowResultArriveAtStop();
        }


        public override void BeginFlow(IFlowData flowData)
        {

            _flowData =(FlowDataArriveAtStop) flowData;

            ExecuteState(FlowStateArriveAtStop.ActionValidateStopRequired);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateArriveAtStop)state);
        }

        public void ExecuteState(FlowStateArriveAtStop state)
        {
            if (state > FlowStateArriveAtStop.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        public void ExecuteViewState(FlowStateArriveAtStop state)
        {
            try
            {
                switch (state)
                {
                    case FlowStateArriveAtStop.ThisFlowComplete:
                        EndFlow();
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "Encountered unhandled view state in FlowArriveAtStop");
                        break;

                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowArriveAtStop.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
            }
        }


        public void ExecuteActionState(FlowStateArriveAtStop state)
        {
            try
            {
                do
                {
                    bool result;
                    switch (state)
                    {
                        case FlowStateArriveAtStop.ActionValidateStopRequired:
                            result = _actionCommandArriveAtStop.IsValidateStopRequired();
                            state = result ? FlowStateArriveAtStop.ActionStopEventRequired : FlowStateArriveAtStop.ActionUpdateOperationStatus;
                            break;

                        case FlowStateArriveAtStop.ActionStopEventRequired:
                            result = _actionCommandArriveAtStop.IsStopEventArrivedRequired();
                            state = result ? FlowStateArriveAtStop.ActionSendArriveAtStopEvent : FlowStateArriveAtStop.ActionStartOperationEventRequired;
                            break;

                        case FlowStateArriveAtStop.ActionSendArriveAtStopEvent:
                            _actionCommandArriveAtStop.SendArriveAtStopRequest(ModelMain.SelectedOperationProcess, _flowData.StartedFromOperationList, _flowData.LoadCarrierId);
                            state = FlowStateArriveAtStop.ActionStartOperationEventRequired;
                            break;

                        case FlowStateArriveAtStop.ActionStartOperationEventRequired://check operation start TMI setting
                            result = _actionCommandArriveAtStop.IsOperationEventStartRequired();
                            state = result
                                        ? FlowStateArriveAtStop.ActionSendOperationStartedRequest
                                        : FlowStateArriveAtStop.ActionStartedFromOperationList;
                            break;

                        case FlowStateArriveAtStop.ActionSendOperationStartedRequest:
                            _actionCommandArriveAtStop.SendOperationStartedRequest(ModelMain.SelectedOperationProcess, _flowData.StartedFromOperationList, _flowData.LoadCarrierId);//send operation started event
                            state = FlowStateArriveAtStop.ActionStartedFromOperationList;
                            break;

                        case FlowStateArriveAtStop.ActionStartedFromOperationList:
                            state = !_flowData.StartedFromOperationList
                                        ? FlowStateArriveAtStop.ThisFlowComplete
                                        : FlowStateArriveAtStop.ActionUpdateOperationStatus;
                            break;

                        case FlowStateArriveAtStop.ActionUpdateOperationStatus:
                            _actionCommandArriveAtStop.UpdateSelectedOperationStatus(ModelMain.SelectedOperationProcess);
                            state = FlowStateArriveAtStop.ActionPersistOperationListChanges;
                            break;

                        case FlowStateArriveAtStop.ActionPersistOperationListChanges:
                            CommandsOperationList.PersistOperationListChanges();
                            state = FlowStateArriveAtStop.ThisFlowComplete;
                            break;

                        default:
                            Logger.LogEvent(Severity.Error, "Unhandled action state encountered in FlowArriveAtStop");
                            break;

                    }
                } while (state < FlowStateArriveAtStop.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowArriveAtStop.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateArriveAtStop.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowArriveAtStop");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    public partial class FormReconcilliationDeliveryToAnotherLocation : BaseForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        private HtmlUtil _hmtlUtil;
        private ConsignmentEntity _currentConsignmentEntity;
        private FormDataReconcilliationDeliveryToAnotherLocation _formData;
        private const string BackButton = "backButton";
        private const string DetailsButton = "detailsButton";
        private const string OkButton = "okButton";
        private const string DeleteButton = "deleteButton";
        private const string DeviationButton = "deviationButton";
        private TransparentLabel _lblNumberOfCarriersValue;
        private TransparentLabel _lblNumberOfItemsValue;


        public FormReconcilliationDeliveryToAnotherLocation()
        {
            _multiButtonControl = new MultiButtonControl();
            InitializeComponent();
            AddNumberOfItemsAndCarrierControls();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToControls();
            SetStandardControlProperties(labelModuleName, lblOrgName, null, null, null, null, null);
        }

        private void AddNumberOfItemsAndCarrierControls()
        {
            _lblNumberOfCarriersValue =
                new TransparentLabel
                {
                    Size = new System.Drawing.Size(438, 30),
                    Location = new System.Drawing.Point(21, 415),
                    Text = "lblCarriersValue"
                };
            touchPanel.Controls.Add(_lblNumberOfCarriersValue);
            _lblNumberOfCarriersValue.BringToFront();

            _lblNumberOfItemsValue =
                    new TransparentLabel
                    {
                        Size = new System.Drawing.Size(438, 30),
                        Location = new System.Drawing.Point(21, 385),
                        Text = "lblItemsValue"
                    };
            touchPanel.Controls.Add(_lblNumberOfItemsValue);
            _lblNumberOfItemsValue.BringToFront();

        }


        public void ListBoxKeyDownEvent(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 40)
            {
                if ((ListConsignments.ActiveRowIndex <= 0) && (ListConsignments.DataRows.Count > ListConsignments.ActiveRowIndex + 1))
                {
                    ListConsignments.ActiveRowIndex = ListConsignments.ActiveRowIndex + 1;

                }
            }
            if (e.KeyValue == 38)
            {
                if ((ListConsignments.ActiveRowIndex > 0) && (ListConsignments.DataRows.Count > 1))
                {
                    ListConsignments.ActiveRowIndex = ListConsignments.ActiveRowIndex - 1;
                }
            }
        }

        private void SetTextToControls()
        {
            labelModuleName.Text = GlobalTexts.DeliveredToAnotherLocationHeader;
            cellHeaderCol1.CellSource.ConstantData = GlobalTexts.Item;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Details, ButtonDetailsClick, DetailsButton));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = OkButton });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Deviation, ButtonDeviationClick, TabButtonType.Normal) { Name = DeviationButton });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Delete, BtnDeleteClick, TabButtonType.Normal) { Name = DeleteButton });

            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
        }
        public override void OnShow(IFormData iformData)
        {
            Logger.LogEvent(Severity.Debug, "FormReconcilliationDeliveryToAnotherLocation;OnShow");
            _formData = (FormDataReconcilliationDeliveryToAnotherLocation)iformData;
            if (_formData != null)
            {
                if (ModelUser.UserProfile.OrgUnitName != null) lblOrgName.Text = ModelUser.UserProfile.OrgUnitName;
            }

            BindList();

            _multiButtonControl.SetButtonEnabledState(DetailsButton, ListConsignments.ActiveRowIndex != -1);
            _multiButtonControl.SetButtonEnabledState(DeleteButton, ListConsignments.ActiveRowIndex != -1);
            _multiButtonControl.SetButtonEnabledState(DeviationButton, ListConsignments.ActiveRowIndex != -1);
            OnUpdateView(iformData);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _lblNumberOfCarriersValue.Text = GlobalTexts.NoOfItems + ":";
            _lblNumberOfItemsValue.Text = GlobalTexts.NoOfCarriers + ":";
            if (_formData == null) return;

            _formData = (FormDataReconcilliationDeliveryToAnotherLocation)iformData;
            if (_formData.OriginalEntityMap == null) return;

            var items = 0;
            var carriers = 0;
            foreach (var consignmentEntity in _formData.OriginalEntityMap.GetConsignmentsWithoutEntities())
            {
                if (consignmentEntity is ConsignmentItem)
                    items += 1;
                else if (consignmentEntity is LoadCarrier)
                    carriers += 1;
            }
            _lblNumberOfCarriersValue.Text = GlobalTexts.NoOfItems + ": " + items;
            _lblNumberOfItemsValue.Text = GlobalTexts.NoOfCarriers + ": " + carriers;

            if (_currentConsignmentEntity != null && _currentConsignmentEntity is LoadCarrier)
                _multiButtonControl.SetButtonEnabledState(DeviationButton, false);
        }

        public override void ShowingView()
        {
            base.ShowingView();
            ListConsignments.Focus();
        }

        private void ButtonDetailsClick(object sender, EventArgs e)
        {
            try
            {
                if (_currentConsignmentEntity != null)
                    ViewEvent.Invoke(ReconcilliationDeliveryToAnotherLocationViewEvents.Details, _currentConsignmentEntity);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcilliationDeliveryToAnotherLocation.ButtonDetailsClick");
            }
        }

        public string CreateConsignmentItemDetailHtml(ConsignmentEntity consignmentEntity)
        {
            _hmtlUtil = new HtmlUtil();

            if (consignmentEntity != null)
            {

                _hmtlUtil.StartTable();

                String weight = String.Format("{0}", consignmentEntity.ConsignmentInstance.Measures.WeightKg);
                _hmtlUtil.AddToSimplTable(GlobalTexts.Weight, weight);


                {
                    String volum = String.Format("{0} ", consignmentEntity.ConsignmentInstance.Measures.VolumeDm3);
                    _hmtlUtil.AddToSimplTable(GlobalTexts.Volume, volum);
                }
                _hmtlUtil.EndTable();
            }
            return _hmtlUtil.GetResult();
        }


        private void OnActiveRowChanged(object sender, EventArgs e)
        {
            try
            {
                bool enable = ListConsignments.ActiveRowIndex != -1;
                if (enable)
                    SetCurrentConsignmentEntity(ListConsignments.ActiveRowIndex);
                var consignmentItemId = (string)ListConsignments.DataRows[ListConsignments.ActiveRowIndex][2];
                List<ConsignmentEntity> listConsignmentEntity =
                      _formData.OriginalEntityMap.GetConsignmentsWithoutEntities();
                foreach (ConsignmentEntity consignmentEntity in listConsignmentEntity)
                {
                    if (consignmentEntity.EntityId == consignmentItemId)
                    {
                        _currentConsignmentEntity = consignmentEntity;
                    }
                }

                _multiButtonControl.SetButtonEnabledState(DeleteButton, enable);
                _multiButtonControl.SetButtonEnabledState(DetailsButton, enable);
                if (_currentConsignmentEntity != null && _currentConsignmentEntity is LoadCarrier)
                    _multiButtonControl.SetButtonEnabledState(DeviationButton, false);
                else
                    _multiButtonControl.SetButtonEnabledState(DeviationButton, enable);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliationDeliveryToAnotherLocation.OnActiveRowChanged");
            }
        }

        private ConsignmentEntity GetCurrentConsignmentEntity()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconcilliationDeliveryToCustomer.GetCurrentConsignmentEntity()");
                return _currentConsignmentEntity;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcilliationDeliveryToCustomer.GetCurrentConsignmentEntity");
            }
            return null;
        }

        public void SetCurrentConsignmentEntity(int rowIndex)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconcilliationDeliveryToCustomer.SetCurrentConsignmentEntity()");

                if (rowIndex >= ListConsignments.DataRows.Count)
                    return;

                var consignmentId = (string)ListConsignments.DataRows[rowIndex][2];
                _currentConsignmentEntity = _formData.OriginalEntityMap.GetConsignment(consignmentId);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcilliationDeliveryToCustomer.SetCurrentConsignmentEntity");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ListConsignments.ActiveRowIndex = 0;//Clear selection when going back
                ViewEvent.Invoke(ReconcilliationDeliveryToAnotherLocationViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcilliationDeliveryToAnotherLocation.ButtonBackClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ReconcilliationDeliveryToAnotherLocationViewEvents.Ok);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcilliationDeliveryToAnotherLocation.ButtonOkClick");
            }
        }

        private void BtnDeleteClick(object sender, EventArgs e)
        {
            try
            {
                if (GuiCommon.ShowModalDialog(GlobalTexts.Delete, string.Format(GlobalTexts.DoYouWantToDeleteThisItem, _currentConsignmentEntity.EntityDisplayId), Severity.Warning, GlobalTexts.Yes, GlobalTexts.No) == GlobalTexts.Yes)
                    ViewEvent.Invoke(ReconcilliationDeliveryToAnotherLocationViewEvents.Delete, _currentConsignmentEntity);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormItemDetails.BtnDeleteClick");
            }
        }

        private void ButtonDeviationClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ReconcilliationDeliveryToAnotherLocationViewEvents.Deviation, GetCurrentConsignmentEntity());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconcilliationDeliveryToAnotherLocation.ButtonDeviationClick");
            }
        }

        private void BindList()
        {
            if (_formData != null && _formData.OriginalEntityMap != null)
            {
                List<ConsignmentEntity> listConsignmentEntity =
                    _formData.OriginalEntityMap.GetConsignmentsWithoutEntities();
                ListConsignments.BeginUpdate();
                ListConsignments.DataRows.Clear();
                foreach (ConsignmentEntity consignmentEntity in listConsignmentEntity)
                {
                    if (consignmentEntity is ConsignmentItem)
                    {
                        var colTextValues = new string[3];
                        colTextValues[0] = consignmentEntity.EntityId;//ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora ? consignmentEntity.EntityId : StringUtil.GetTextForConsignmentEntity(consignmentEntity.EntityDisplayId, false);//FOT 116

                        colTextValues[1] = GlobalTexts.Item;
                        colTextValues[2] = consignmentEntity.EntityId;
                        var insertRow = new Row(2, 1, colTextValues);
                        ListConsignments.DataRows.Add(insertRow);
                    }
                    else if (consignmentEntity is LoadCarrier)
                    {
                        var colTextValues = new string[3];
                        colTextValues[0] = consignmentEntity.EntityId;//StringUtil.GetTextForConsignmentEntity(consignmentEntity.EntityDisplayId, false);
                        colTextValues[1] = GlobalTexts.Carrier;
                        colTextValues[2] = consignmentEntity.EntityId;
                        var insertRow = new Row(2, 1, colTextValues);
                        ListConsignments.DataRows.Add(insertRow);
                    }
                }
                if (_currentConsignmentEntity != null)
                    SelectCurrentConsignmentEntity(_currentConsignmentEntity.EntityId);
                else
                    ListConsignments.ActiveRowIndex = 0;

                ListConsignments.EndUpdate();
            }
        }

        // After binding we try to activate the previous selected item
        private void SelectCurrentConsignmentEntity(string currentCosignmentItemId)
        {
            for (int i = 0; i < ListConsignments.DataRows.Count; i++)
            {
                var consignmentItemId = (string)ListConsignments.DataRows[i][2];
                if (currentCosignmentItemId == consignmentItemId)
                {
                    ListConsignments.ActiveRowIndex = i;
                    return;
                }
            }
            ListConsignments.ActiveRowIndex = 0;
        }

    }
    public class FormDataReconcilliationDeliveryToAnotherLocation : BaseFormData
    {
        public EntityMap OriginalEntityMap { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public string SelectedOrgName { get; set; }
    }
    public class ReconcilliationDeliveryToAnotherLocationViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int Details = 2;
        public const int Deviation = 3;
        public const int Delete = 4;
    }
}
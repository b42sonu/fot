﻿using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    partial class FormItemDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblReceipientValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPckValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPck = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAddress = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblReceipient = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblAddressValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblTown = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblTownValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipientValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPckValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTownValue)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblOrgName);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;

            // 
            // lblReceipientValue
            // 
            this.lblReceipientValue.AutoSize = false;
            this.lblReceipientValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblReceipientValue.Location = new System.Drawing.Point(152, 118);
            this.lblReceipientValue.Name = "lblReceipientValue";
            this.lblReceipientValue.Size = new System.Drawing.Size(306, 60);
            this.lblReceipientValue.AutoSize = false;
            // lblOrgName
            // 
            this.lblOrgName.AutoSize = false;
            this.lblOrgName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgName.Name = "lblOrgName";
            this.lblOrgName.Size = new System.Drawing.Size(477, 33);
            this.lblOrgName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblPckValue
            // 
            this.lblPckValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblPckValue.Location = new System.Drawing.Point(152, 81);
            this.lblPckValue.Name = "lblPckValue";
            this.lblPckValue.Size = new System.Drawing.Size(306, 30);
            this.lblPckValue.AutoSize = false;
            // lblPck
            // 
            this.lblPck.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblPck.Location = new System.Drawing.Point(21, 81);
            this.lblPck.Name = "lblPck";
            this.lblPck.Size = new System.Drawing.Size(100, 24);
            this.lblPck.Text = "Pck:";
            // 
            // lblAddress
            // 
            this.lblAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblAddress.Location = new System.Drawing.Point(21, 185);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(100, 24);
            this.lblAddress.Text = "Address:";
            // 
            // lblReceipient
            // 
            this.lblReceipient.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblReceipient.Location = new System.Drawing.Point(21, 118);
            this.lblReceipient.Name = "lblReceipient";
            this.lblReceipient.Size = new System.Drawing.Size(100, 24);
            this.lblReceipient.Text = "Receip:";
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblAddressValue
            // 
            this.lblAddressValue.AutoSize = false;
            this.lblAddressValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblAddressValue.Location = new System.Drawing.Point(152, 185);
            this.lblAddressValue.Name = "lblAddressValue";
            this.lblAddressValue.Size = new System.Drawing.Size(306, 60);
            this.lblAddressValue.AutoSize = false;
            // 
            // lblTown
            // 
            this.lblTown.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblTown.Location = new System.Drawing.Point(21, 252);
            this.lblTown.Name = "lblTown";
            this.lblTown.Size = new System.Drawing.Size(100, 24);
            this.lblTown.Text = "Town:";
            // 
            // lblTownValue
            // 
            this.lblTownValue.AutoSize = false;
            this.lblTownValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblTownValue.Location = new System.Drawing.Point(152, 252);
            this.lblTownValue.Name = "lblTownValue";
            this.lblTownValue.Size = new System.Drawing.Size(306, 60);
            this.lblTownValue.AutoSize = false;
            // 
            // FormItemDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FormItemDetails";
            this.Text = "FormItemDetails";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipientValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPckValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceipient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTownValue)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgName;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblReceipient;
        private Resco.Controls.CommonControls.TransparentLabel lblAddress;
        private Resco.Controls.CommonControls.TransparentLabel lblPck;
        private Resco.Controls.CommonControls.TransparentLabel lblReceipientValue;
        private Resco.Controls.CommonControls.TransparentLabel lblPckValue;
        private Resco.Controls.CommonControls.TransparentLabel lblAddressValue;
        private Resco.Controls.CommonControls.TransparentLabel lblTownValue;
        private Resco.Controls.CommonControls.TransparentLabel lblTown;
    }
}
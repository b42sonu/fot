﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu;
using Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem;
using Com.Bring.PMP.PreComFW.Shared.Flows.Reconciliation;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using LoadCarrier = Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap.LoadCarrier;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    #region External classes and Enums
    public enum FlowStateDeliveredToAnotherLocation
    {
        ActionStoreInLocalcache,
        ActionValidateOrgUnit,
        ActionDeleteItem,
        ActionStoreLoadCarrier,
        ActionIsLoadCarrierScannedBefore,
        ActionSendGoodsEvent,
        ActionSendReasonCodeEvent,
        ActionClearScannings,
        ActionValidateGoodsAndSetInfoToConsignmentEntity,
        ActionGetPlannedOperations,
        ActionIsItemScannedBefore,
        ActionProcessReturnedOperations,
        ActionCheckLastGoodsEvent,
        ActionIsMissingInformationAvailableForConsignment,
        ActionIsRbtInfoRequired,
        ActionPrepareWorkListItem,
        ActionGetRbtWorkListItem,
        ActionStoreSkippedConsignment,
        ActionUpdateLoadListOperations,

        ActionBackFromScanBarcode,
        ActionBackFromCorrectionMenu,
        ActionBackFromScanBarcodeForOrgUnit,
        ActionBackFromFlowVasMatrix,
        ActionBackFromFlowScanRbt,
        ActionBackFromFlowCauseAndMeasure,
        ActionBackFromFlowGetWorkListItem,
        ActionBackFromFlowScanBarcodeForRbtScan,
        ActionBackFromExecuteVasMatrixFinalProcess,

        ViewCommands,
        ViewScanOrgUnit,
        ViewScanShipment,
        ViewReconcilliation,
        ViewItemDetails,
        ViewScanRbt,
        ViewShowOperations,

        FlowReconcilliation,
        FlowExecuteVasMatrix,
        FlowExecuteVasMatrixFinalProcess,
        FlowGetWorkListItem,
        FlowScanBarcode,
        FlowCorrectionMenu,
        FlowCauseAndMeasure,
        ThisFlowComplete,

    }

    public class FlowDataDeliveredToAnotherLocation : BaseFlowData
    {
        public PlannedConsignmentsType[] PlannedConsignments { get; set; }
        public EntityMap EntityMap { get; set; }
    }

    public class FlowResultDeliveredToAnotherLocation : BaseFlowResult
    {
    }

    public class StoredCauseAndMeasure
    {
        public Consignment Consignment { get; set; }
        public CauseMeasure CauseMeasure { get; set; }
        public string FreeText { get; set; }
        public bool IsAttemptedDelivery { get; set; }

    }

    #endregion

    // US 25: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554917
    public class FlowDeliveredToAnotherLocation : BaseFlow
    {
        #region private vars
        private readonly ViewCommandsDeliveredToAnotherLocation _viewCommands;
        private FlowDataDeliveredToAnotherLocation _flowData;
        private EntityMap _entityMap;
        private MessageHolder _messageHolder;
        private FlowScanBarcode _flowScanBarcodeInstance;
        private readonly ActionCommandDeliveredToAnotherLocation _actionCommand;
        private PlannedConsignmentsType[] _plannedConsignments;
        private ConsignmentEntity _currentConsignmentEntity;
        private string _orgUnit = String.Empty;
        private string _loadCarrier;
        private string _orgName;
        private ConsignmentEntity _selectedConsignmentEntity;
        private readonly FlowResultDeliveredToAnotherLocation _flowResult;
        readonly CommandsHandleScannedBarcodes _scanCommands = new CommandsHandleScannedBarcodes();
        private readonly ActionCommandsReconciliation _actionCommandsReconciliation;
        private FotScannerOutput _scannerOutput;
        private CauseMeasure _selectedReasonAndAction;
        private string _freeText;
        private BarcodeRouteCarrierTrip _scannedRbt;
        private List<Stop> _listOfOperations;
        private readonly string _powerUnitId = string.Empty;
        private bool _isAttemptedDelivery;
        private string _scannedRouteId = string.Empty;
        private WorkListItem _workListItem;
        private List<StoredCauseAndMeasure> _listCauseAndMeasure;
        private BarcodeType _scannedBarcodeType;
        private bool _showNoAttemptedDeliveryMessage;
        private bool _isEnteredPowerUnit;
        private string _loadCarrierId = string.Empty;
        private ConsignmentEntity _currentScanning;
        private int _itemCountBeforeVas;//Check if VAS deleted some items
        private readonly VasSubFlowResultInfo _vasSubFlowResultInfo; //Stores info filled by VAS for this process
        private ConsignmentEntity _tempConsignmentEntity;
        private bool _isDeviationStartedFromScan;

        #endregion

        #region Constructor

        public FlowDeliveredToAnotherLocation()
        {
            _viewCommands = new ViewCommandsDeliveredToAnotherLocation();
            _actionCommandsReconciliation = new ActionCommandsReconciliation(CommunicationClient.Instance);
            _entityMap = new EntityMap();
            _selectedReasonAndAction = new CauseMeasure();
            _messageHolder = new MessageHolder();
            _actionCommand = new ActionCommandDeliveredToAnotherLocation(CommunicationClient.Instance);
            _flowResult = new FlowResultDeliveredToAnotherLocation();
            _listCauseAndMeasure = new List<StoredCauseAndMeasure>();
            _vasSubFlowResultInfo = new VasSubFlowResultInfo();
        }

        #endregion

        #region Overriden Methods

        public override WorkProcess WorkProcess
        {
            get
            {
                return WorkProcess.DeliverToAnotherLocation;
            }
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing flow FlowDeliveredToAnotherLocation");
            _flowData = (FlowDataDeliveredToAnotherLocation)flowData;
            _flowData.EntityMap = new EntityMap();
            if (_flowData.PlannedConsignments != null && _flowData.PlannedConsignments.Length > 0)
            {
                _plannedConsignments = _flowData.PlannedConsignments;
                _flowData.EntityMap.StorePlannedConsignments(_flowData.PlannedConsignments);
            }
            ExecuteState(FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateDeliveredToAnotherLocation)state);
        }

        public override bool IsStopCodeActiveForProcess()
        {
            return true;
        }


        #endregion

        public void ExecuteState(FlowStateDeliveredToAnotherLocation state)
        {
            if (state > FlowStateDeliveredToAnotherLocation.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        private void ExecuteViewState(FlowStateDeliveredToAnotherLocation state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowDeliveredToAnotherLocation;" + state);

            try
            {
                switch (state)
                {
                    case FlowStateDeliveredToAnotherLocation.FlowReconcilliation:
                        _isDeviationStartedFromScan = false;
                        _viewCommands.ViewSummaryView(SummaryViewEventHandler, _entityMap, _messageHolder, _orgName);
                        break;

                    case FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit:
                        _isDeviationStartedFromScan = true;
                        _viewCommands.ViewScanOrgUnit(ScanViewEventHandlerForOrgUnit, _messageHolder, _entityMap, true, _orgName, _currentConsignmentEntity, _orgUnit, _showNoAttemptedDeliveryMessage);

                        break;

                    case FlowStateDeliveredToAnotherLocation.FlowCauseAndMeasure:
                        var flowDataCauseAndMeasure = new FlowDataCauseAndMeasure
                        {
                            Comment = null,
                            SelectedReasonAndActionCombination = null,
                            HeaderText = _isAttemptedDelivery ? GlobalTexts.AttemptedDelivery : GlobalTexts.NotDelivered,
                        };
                        Process process = _isAttemptedDelivery ? Process.AttemptedDelivery : Process.NotDelivered;
                        StartSubFlow<FlowCauseAndMeasure>(flowDataCauseAndMeasure, (int)FlowStateDeliveredToAnotherLocation.ActionBackFromFlowCauseAndMeasure, process);
                        break;

                    case FlowStateDeliveredToAnotherLocation.ViewScanShipment:
                        _isDeviationStartedFromScan = true;
                        _viewCommands.ViewScanOrgUnit(ScanViewEventHandlerForShipment, _messageHolder, _entityMap, false, _orgName, _currentConsignmentEntity, _orgUnit, _showNoAttemptedDeliveryMessage);
                        break;

                    case FlowStateDeliveredToAnotherLocation.ViewItemDetails:
                        _viewCommands.ViewItemDetails(DetailsViewHandler, _selectedConsignmentEntity, _messageHolder, _orgName);
                        break;

                    case FlowStateDeliveredToAnotherLocation.ThisFlowComplete:
                        EndFlow();
                        break;

                    case FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix:
                        var flowDataVasMatrix = new FlowDataVasMatrix(_tempConsignmentEntity, this) { HeaderText = _flowData.HeaderText };
                        StartSubFlow<FlowVasMatrix>(flowDataVasMatrix, (int)FlowStateDeliveredToAnotherLocation.ActionBackFromFlowVasMatrix, Process.DeliveredOtherLoc);
                        break;

                    case FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrixFinalProcess:
                        _itemCountBeforeVas = _entityMap.ScannedConsignmentItemsTotalCount + _entityMap.ScannedLoadCarrierTotalCount;

                        var flowDataVasMatrixFinalProcess = new FlowDataVasMatrix(_entityMap, this) { VasSubFlowResultInfo = _vasSubFlowResultInfo, HeaderText = _flowData.HeaderText };
                        StartSubFlow<FlowVasMatrix>(flowDataVasMatrixFinalProcess, (int)FlowStateDeliveredToAnotherLocation.ActionBackFromExecuteVasMatrixFinalProcess, Process.DeliveredOtherLoc);
                        break;

                    case FlowStateDeliveredToAnotherLocation.FlowCorrectionMenu:
                        var flowData = new FlowDataCorrectionMenu(GlobalTexts.DeliveredToAnotherLocationHeader, _currentConsignmentEntity,
                                                                  _entityMap);
                        StartSubFlow<FlowCorrectionMenu>(flowData, (int)FlowStateDeliveredToAnotherLocation.ActionBackFromCorrectionMenu, Process.Inherit);
                        break;

                    case FlowStateDeliveredToAnotherLocation.FlowGetWorkListItem:
                        var flowDataGetWorkListItem = new FlowDataGetWorkListItem
                        {
                            IsPowerUnitEntered = _isEnteredPowerUnit,
                            Rbt = _scannedRbt,
                            //  RouteId = _scannedRouteId,
                            TripId = ModelMain.SelectedOperationProcess != null ? ModelMain.SelectedOperationProcess.TripId : string.Empty,
                            StopId = ModelMain.SelectedOperationProcess != null ? ModelMain.SelectedOperationProcess.StopId : string.Empty,
                            BarcodeType = _scannedBarcodeType,
                            LoadCarrierId = _loadCarrierId,
                            PowerUnitId = _isEnteredPowerUnit ? _scannerOutput.Code : ModelUser.UserProfile.PowerUnit,
                            //CurrentLocation = _location,
                            HeaderText = _flowData.HeaderText,
                            LoadCarrierEntity = _currentScanning != null ? (LoadCarrier)_currentScanning : null
                        };
                        StartSubFlow<FlowGetWorkListItemNotDelivered>(flowDataGetWorkListItem, (int)FlowStateDeliveredToAnotherLocation.ActionBackFromFlowGetWorkListItem, Process.Inherit);
                        break;



                    case FlowStateDeliveredToAnotherLocation.ViewScanRbt:
                        _viewCommands.ShowViewRbtScan(RbtScanEventHandler, _messageHolder, _flowData.HeaderText);
                        break;

                    case FlowStateDeliveredToAnotherLocation.ViewShowOperations:
                        _viewCommands.ShowOperations(FormOperationsHandler, _messageHolder);
                        break;

                    default:
                        Logger.LogEvent(Severity.Error, "Encountered unhandled view State");
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "state=" + state);
            }
        }

        public void ExecuteActionState(FlowStateDeliveredToAnotherLocation state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowDeliveredToAnotherLocation;" + state);
                    switch (state)
                    {

                        case FlowStateDeliveredToAnotherLocation.ActionStoreInLocalcache:
                            _scanCommands.StoreScannedConsignmentEntity(_entityMap, _currentConsignmentEntity);
                            state = FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromFlowVasMatrix:
                            state = _actionCommand.ValidateFlowVasMatrixResult((FlowResultVasMatrix)SubflowResult, _messageHolder, ref _tempConsignmentEntity, ref _currentConsignmentEntity);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromCorrectionMenu:
                            _actionCommand.ValidateSubflowResultWithChangedEntityMap(ref _entityMap, SubflowResult, ref _messageHolder, ref  _currentConsignmentEntity);
                            state = _isDeviationStartedFromScan ? FlowStateDeliveredToAnotherLocation.ViewScanShipment : FlowStateDeliveredToAnotherLocation.FlowReconcilliation;
                            break;


                        case FlowStateDeliveredToAnotherLocation.ActionBackFromScanBarcodeForOrgUnit:
                            state = _actionCommand.ValidateFlowScanBarcodeForOrgUnit((FlowResultScanBarcode)SubflowResult, BarcodeType.OrgUnitId, ref _currentConsignmentEntity, ref _messageHolder, out _loadCarrier, ref _orgUnit, ref _tempConsignmentEntity);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionValidateOrgUnit:
                            state = _actionCommand.ValidateOrgUnit(_orgUnit, _messageHolder, out _orgName);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionIsRbtInfoRequired:
                            state = _actionCommand.RbtInfoRequired();
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionPrepareWorkListItem:
                            if (_plannedConsignments != null && _plannedConsignments.Length > 0)
                            {
                                state = FlowStateDeliveredToAnotherLocation.FlowGetWorkListItem;
                            }
                            else
                            {
                                state = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                            }
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromScanBarcode:
                            state = _actionCommand.ValidateFlowScanBarcodeForOrgUnit((FlowResultScanBarcode)SubflowResult, BarcodeType.ConsignmentItemOrLoadCarrier, ref _currentConsignmentEntity, ref _messageHolder, out _loadCarrier, ref _orgUnit, ref _tempConsignmentEntity);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionStoreLoadCarrier:
                            _actionCommand.StoreLoadCarrier(_entityMap, _currentConsignmentEntity, ref _messageHolder);
                            state = FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionDeleteItem:
                            _actionCommand.DeleteItem(_selectedConsignmentEntity, _entityMap, out _messageHolder);
                            state = FlowStateDeliveredToAnotherLocation.FlowReconcilliation;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionSendGoodsEvent:
                            _actionCommand.SendGoodsEvent(_vasSubFlowResultInfo.Signature, _entityMap, _orgUnit, _workListItem);
                            _flowResult.State = FlowResultState.Ok;
                            state = FlowStateDeliveredToAnotherLocation.ActionUpdateLoadListOperations;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionSendReasonCodeEvent:
                            _actionCommand.CreateGoodsEvents(_tempConsignmentEntity, _isAttemptedDelivery, _selectedReasonAndAction, _freeText);
                            state = FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionClearScannings:
                            _actionCommand.ClearScannings(ref _entityMap, ref _orgUnit, ref _orgName);
                            state = FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit;
                            break;



                        case FlowStateDeliveredToAnotherLocation.ActionValidateGoodsAndSetInfoToConsignmentEntity:
                            // if (!(_selectedConsignmentEntity is LoadCarrier))
                            _actionCommandsReconciliation.ValidateGoodsAndSetInfoToConsignmentEntity(_selectedConsignmentEntity, true, ModelUser.UserProfile);
                            state = FlowStateDeliveredToAnotherLocation.ViewItemDetails;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromFlowCauseAndMeasure:
                            bool result = _actionCommand.VerifyFlowResultCauseAndMeasure((FlowResultCauseAndMeasure)SubflowResult, ref _selectedReasonAndAction, out _freeText, _listCauseAndMeasure, ref _tempConsignmentEntity, _messageHolder, _isAttemptedDelivery);
                            if (result)
                                state = (_currentConsignmentEntity is LoadCarrier) ? FlowStateDeliveredToAnotherLocation.ViewScanShipment : FlowStateDeliveredToAnotherLocation.ActionSendReasonCodeEvent;
                            else
                                state = FlowStateDeliveredToAnotherLocation.ViewScanShipment;

                            break;


                        case FlowStateDeliveredToAnotherLocation.ActionGetPlannedOperations:
                            _listOfOperations = _actionCommand.GetPlannedOperations(_scannedRbt, _powerUnitId, _messageHolder);
                            state = FlowStateDeliveredToAnotherLocation.ActionProcessReturnedOperations;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromFlowScanRbt:
                            state = _actionCommand.ValidateFlowScanRbt((FlowResultScanBarcode)SubflowResult, out _currentConsignmentEntity, ref _loadCarrierId, ref _scannedRbt);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionProcessReturnedOperations:
                            state = _actionCommand.SelectNextState(_listOfOperations);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionCheckLastGoodsEvent:
                            state = _actionCommand.SetNextState(_tempConsignmentEntity);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionIsMissingInformationAvailableForConsignment:
                            state = _actionCommand.IsCauseMeasureEnteredBefore(_listCauseAndMeasure,
                                                                                _tempConsignmentEntity, _entityMap, ref _selectedReasonAndAction, ref _freeText, ref _isAttemptedDelivery);
                            if (state == FlowStateDeliveredToAnotherLocation.ViewScanShipment)
                                _showNoAttemptedDeliveryMessage = true;

                            break;



                        case FlowStateDeliveredToAnotherLocation.ActionBackFromFlowScanBarcodeForRbtScan:
                            _scannedBarcodeType = ((FlowResultScanBarcode)SubflowResult).BarcodeType;
                            state = _actionCommand.ValidateFlowRbtScanResult((FlowResultScanBarcode)SubflowResult,
                                                                             out _currentScanning, ref _loadCarrierId, ref _scannedRbt
                                                                             );
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromFlowGetWorkListItem:

                            state = _actionCommand.ValidateFlowGetWorkListItemResult((FlowResultGetWorkListItem)SubflowResult, ref _workListItem, _messageHolder, ref _scannedRbt, ref _loadCarrierId, ref _scannedRouteId, _scannedBarcodeType);
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionStoreSkippedConsignment:
                            _actionCommand.StoreSkippedConsignment(ref _listCauseAndMeasure, _tempConsignmentEntity);
                            state = FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionBackFromExecuteVasMatrixFinalProcess:
                            state = _actionCommand.BackFromExecuteVasFinalProcess((FlowResultVasMatrix)SubflowResult, _entityMap, _itemCountBeforeVas);
                            if (state == FlowStateDeliveredToAnotherLocation.FlowReconcilliation)
                                CurrentView.AutoShow = true;
                            break;

                        case FlowStateDeliveredToAnotherLocation.ActionUpdateLoadListOperations:
                            AdditionalCommands.UpdateLoadListOperationStatus(null, _entityMap);
                            state = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                            break;


                        default:
                            state = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateDeliveredToAnotherLocation.ViewCommands);

            }
            catch (Exception e)
            {
                Logger.LogEvent(Severity.Error, "Unexpected error in ExecuteActionState(): " + e.Message);
                state = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }


        #region Event Handlers

        private void SummaryViewEventHandler(int summaryViewEvents, params object[] data)
        {
            switch (summaryViewEvents)
            {
                case ReconcilliationDeliveryToAnotherLocationViewEvents.Ok:
                    CurrentView.AutoShow = false;
                    _messageHolder.Clear();
                    ExecuteState(_entityMap.ScannedConsignmentItemsTotalCount + _entityMap.ScannedLoadCarrierTotalCount == 0
                                     ? FlowStateDeliveredToAnotherLocation.ThisFlowComplete
                                     : FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrixFinalProcess);
                    break;

                case ReconcilliationDeliveryToAnotherLocationViewEvents.Back:
                    _messageHolder.Clear();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ViewScanShipment);
                    break;
                case ReconcilliationDeliveryToAnotherLocationViewEvents.Details:
                    _selectedConsignmentEntity = (ConsignmentEntity)data[0];
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ActionValidateGoodsAndSetInfoToConsignmentEntity);
                    break;
                case ReconcilliationDeliveryToAnotherLocationViewEvents.Delete:
                    _selectedConsignmentEntity = (ConsignmentEntity)data[0];
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ActionDeleteItem);
                    break;
                case ReconcilliationDeliveryToAnotherLocationViewEvents.Deviation:
                    if (data != null && data.Length > 0)
                        _currentConsignmentEntity = (ConsignmentEntity)data[0];
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowCorrectionMenu);
                    break;

            }
        }

        private void FormOperationsHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {

                case FormOperations.FormOperationsViewEvents.Ok:
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ActionPrepareWorkListItem);
                    break;

                case FormOperations.FormOperationsViewEvents.Back:
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ViewScanRbt);
                    break;
            }
        }


        private void RbtScanEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case RbtScanViewEvents.Back:
                    _messageHolder.Clear();
                    _orgName = string.Empty;
                    _orgUnit = string.Empty;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit);
                    break;

                case RbtScanViewEvents.RbtScanned:
                case RbtScanViewEvents.Ok:
                    _scannerOutput = (FotScannerOutput)data[0];
                    var result = BaseActionCommands.ValidateBarcode(_scannerOutput,
                                                                           BarcodeType.Rbt,
                                                                            false);

                    if (result == false && viewEvent == RbtScanViewEvents.Ok)
                    {
                        SoundUtil.Instance.PlayScanSound();
                        _scannedBarcodeType = BarcodeType.Rbt;
                        _isEnteredPowerUnit = true;
                        ExecuteState(FlowStateDeliveredToAnotherLocation.FlowGetWorkListItem);
                    }
                    else
                    {
                        GoToFlowScanBarcodeForRbtScan();
                        _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                        _scannerOutput = (FotScannerOutput)data[0];
                        _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                        _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
                    }
                    break;

                case RbtScanViewEvents.Skip:
                    _messageHolder.Clear();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ViewScanShipment);
                    break;
            }
        }


        private void GoToFlowScanBarcodeForRbtScan()
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.LoadLineHaul, _messageHolder, BarcodeType.Rbt, _entityMap)
            {
                HaveCustomForm = true
            };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)FlowStateDeliveredToAnotherLocation.ActionBackFromFlowScanBarcodeForRbtScan, Process.Inherit);
        }

        private void ScanViewEventHandlerForOrgUnit(int scanBarcodeViewEvents, params object[] data)
        {
            switch (scanBarcodeViewEvents)
            {
                case ScanOrgUnitViewEvents.BarcodeScanned:
                case ScanOrgUnitViewEvents.Ok:
                    GoToFlowScanBarcodeForDeliveryToAnotherLocation(BarcodeType.OrgUnitId, FlowStateDeliveredToAnotherLocation.ActionBackFromScanBarcodeForOrgUnit);//
                    _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                    _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
                    break;

                case ScanOrgUnitViewEvents.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    _messageHolder = new MessageHolder();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ThisFlowComplete);
                    break;

                case ScanOrgUnitViewEvents.Skip:
                    _showNoAttemptedDeliveryMessage = false;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix);
                    break;
                case ScanOrgUnitViewEvents.AttemptedDel:
                    _showNoAttemptedDeliveryMessage = false;
                    _isAttemptedDelivery = true;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowCauseAndMeasure);
                    break;
                case ScanOrgUnitViewEvents.NotDelivered:
                    _showNoAttemptedDeliveryMessage = false;
                    _isAttemptedDelivery = false;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowCauseAndMeasure);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ScanViewEventHandler");
                    break;
            }
        }


        private void DetailsViewHandler(int itemDetailsViewEvents, params object[] data)
        {
            switch (itemDetailsViewEvents)
            {
                case ItemDetailsViewEvents.Back:
                    _messageHolder = new MessageHolder();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowReconcilliation);
                    break;
                case ItemDetailsViewEvents.Ok:
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowReconcilliation);
                    break;
            }
        }

        private void ScanViewEventHandlerForShipment(int scanBarcodeViewEvents, params object[] data)
        {
            switch (scanBarcodeViewEvents)
            {
                case ScanOrgUnitViewEvents.BarcodeScanned:
                    Logger.LogEvent(Severity.Debug, "#FlowDeliveredToAnotherLocation;ScanViewEventHandlerForShipment;Before FlowScanBarcode");//Added mottak 436 to be removed
                    GoToFlowScanBarcodeForDeliveryToAnotherLocation(BarcodeType.ConsignmentItemOrLoadCarrier, FlowStateDeliveredToAnotherLocation.ActionBackFromScanBarcode);
                    Logger.LogEvent(Severity.Debug, "#FlowDeliveredToAnotherLocation;ScanViewEventHandlerForShipment; After FlowScanBarcode");//Added mottak 436 to be removed
                    _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                    _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);

                    break;

                case ScanOrgUnitViewEvents.Reconcilliation:
                    _messageHolder = new MessageHolder();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowReconcilliation);
                    break;

                case ScanOrgUnitViewEvents.Ok:
                    _messageHolder.Clear();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowReconcilliation);
                    break;

                case ScanOrgUnitViewEvents.Deviation:
                    if (_entityMap != null && !_entityMap.IsScannedBefore(_currentConsignmentEntity))
                        _currentConsignmentEntity = null;
                    if (_currentConsignmentEntity != null && !(_currentConsignmentEntity is LoadCarrier))
                        ExecuteState(FlowStateDeliveredToAnotherLocation.FlowCorrectionMenu);
                    else
                    {
                        _messageHolder.Update(GlobalTexts.NoItemScanned, MessageState.Warning);
                        ExecuteState(FlowStateDeliveredToAnotherLocation.ViewScanShipment);
                    }
                    break;

                case ScanOrgUnitViewEvents.Back:
                    _messageHolder = new MessageHolder();
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ActionClearScannings);

                    break;

                case ScanOrgUnitViewEvents.Skip:
                    _showNoAttemptedDeliveryMessage = false;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.ActionStoreSkippedConsignment);
                    break;
                case ScanOrgUnitViewEvents.AttemptedDel:
                    _showNoAttemptedDeliveryMessage = false;
                    _isAttemptedDelivery = true;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowCauseAndMeasure);
                    break;
                case ScanOrgUnitViewEvents.NotDelivered:
                    _showNoAttemptedDeliveryMessage = false;
                    _isAttemptedDelivery = false;
                    ExecuteState(FlowStateDeliveredToAnotherLocation.FlowCauseAndMeasure);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in ScanViewEventHandler");
                    break;
            }
        }


        #endregion

        private void GoToFlowScanBarcodeForDeliveryToAnotherLocation(BarcodeType barcodeType, FlowStateDeliveredToAnotherLocation state)
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.DeliveredToAnotherLocationHeader, _messageHolder, barcodeType, _entityMap) { HaveCustomForm = true, WantLoadCarrierDetails = true };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)state, Process.Inherit);
        }

        private void EndFlow()
        {
            if (CurrentView != null)
                CurrentView.AutoShow = true;
            Logger.LogEvent(Severity.Debug, "Ending flow FlowDeliveredToAnotherLocation");
            BaseModule.EndSubFlow(_flowResult);
        }


    }
}
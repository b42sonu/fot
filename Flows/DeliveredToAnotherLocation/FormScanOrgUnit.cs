﻿using System;
using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Properties;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    public partial class FormScanOrgUnit : BaseBarcodeScanForm
    {
        #region class Variables

        private readonly MultiButtonControl _tabButtons;
        private const string BackButton = "backButton";
        private const string DeviationButton = "deviationButton";
        private const string OkButton = "okButton";
        //private const string ReconcilliationButton = "reconcilliationButton";
        private FormDataScanOrgUnit _formData;
        private int _itemCount;
        private readonly Resco.Controls.CommonControls.TransparentLabel _lblToOrgUnit;
        private bool showAttemptDel;

        #endregion

        #region Private Methods

        public FormScanOrgUnit()
        {
            InitializeComponent();
            SetStandardControlProperties(lblModuleName, lblHeading, lblScan, txtScannedNumber, MsgMessage, lblItem, lblNoOfLoadCarriers);
            _tabButtons = new MultiButtonControl();

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null) touchPanel.BackgroundImage = image;
            pictureBoxConsignment.Image = Resources.FOT_icon_attention;
            _lblToOrgUnit = CreateLbLOrgUnit();
            SetTextToGui();
        }
        private Resco.Controls.CommonControls.TransparentLabel CreateLbLOrgUnit()
        {
            var lbl = new Resco.Controls.CommonControls.TransparentLabel
                          {
                              AutoSize = false,
                              Location = ScaleUtil.GetScaledPoint(new Point(21, 385 - 29)),
                              Size = new Size(438, 60),
                              Name = "LbLOrgUnit",
                          };
            touchPanel.Controls.Add(lbl);
            return lbl;
        }

        /// <summary>
        /// Set Text and Buttons on View
        /// </summary>
        private void SetTextToGui()
        {
            lblHeading.Text = ModelUser.UserProfile.OrgUnitName;

            lblModuleName.Text = GlobalTexts.DeliveredToAnotherLocationHeader;
            lblChoose.Text = GlobalTexts.ChooseAction;
            lblNoAttemptedDel.Text = GlobalTexts.NoAttempetdDeliveryRegistered;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, TabButtonType.Cancel) { Name = BackButton, Activation = ActivateOn.IfNotScanned });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Deviation, BtnDeviationClick, DeviationButton) { Activation = ActivateOn.IfScanned });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, TabButtonType.Confirm) { Name = OkButton, Activation = ActivateOn.IfScanned });
            // _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Reconcillation, BtnReconcilliationClick, ReconcilliationButton) { Activation = ActivateOn.RemoveIfNotScanned });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        private void InitOnScreenKeyBoardProperties(string title)
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ConsignmentNumberTextChanged,
                Title = title
            };
            txtScannedNumber.Tag = keyBoardProperty;

        }

        private void ConsignmentNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _tabButtons.SetButtonEnabledState(OkButton, txtScannedNumber.Text != string.Empty || _itemCount > 0);
            if (MsgMessage != null)
                MsgMessage.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text != string.Empty)
            {
                BarcodeEnteredManually(textentered);
            }
        }

        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(ScanOrgUnitViewEvents.BarcodeScanned, scannerOutput);
        }


        private void ClearStates()
        {
            MsgMessage.ClearMessage();
        }

        #endregion

        #region overridenMethods
        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            OnUpdateView(formData);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormDataScanOrgUnit)iformData;
            if (_formData != null)
            {
                ClearStates();//Clear previous states
                _itemCount = _tabButtons.ScannedItemCount = _formData.ItemCount + _formData.LoadCarrierCount;

                _lblToOrgUnit.Text = GlobalTexts.NewLocation + ": " + _formData.OrgName;
                //No of items
                lblItem.Text = GlobalTexts.NoOfItems + ": " + _formData.ItemCount;
                //No of load carriers
                lblNoOfLoadCarriers.Text = GlobalTexts.NoOfCarriers + ": " + _formData.LoadCarrierCount;

                if (!_formData.ShouldScanOrgUnitOnly)
                {
                    SetViewForScanningConsignmentItem();
                }
                else
                {
                    SetViewForScanningOrgUnit();
                }

                InitOnScreenKeyBoardProperties(lblScan.Text);

                SetMessageToDisplay(lblScan.Text);

                _tabButtons.SetButtonEnabledState(OkButton, _formData.ItemCount + _formData.LoadCarrierCount > 0 || txtScannedNumber.Text != string.Empty);
            }

            if (_formData != null)
                showAttemptDel = _formData.ShowAttemptDel;
            else
                showAttemptDel = false;
            SetKeyboardState(KeyboardState.Numeric);
        }

        public override void ShowingView()
        {
            base.ShowingView();

            //Show attempted Delivery panel if required
            ShowAttemptedDeliveryPopup();

        }

        /// <summary>
        /// Sets the GUI for Attempted Delivery panel 
        /// </summary>
        private void ShowAttemptedDeliveryPopup()
        {
            if (showAttemptDel)
            {
                var modalResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning,
                                                           string.Format("{0}{1}{2}", GlobalTexts.NoAttempetdDeliveryRegistered, Environment.NewLine,
                                                                         GlobalTexts.ChooseAction),
                                                           Severity.Warning,
                                                           string.Format("{0}.{1}{2}", GlobalTexts.Attempt, Environment.NewLine, GlobalTexts.Delivery), GlobalTexts.Skip, string.Format("{0}{1}{2}", GlobalTexts.Not, Environment.NewLine, GlobalTexts.Delivered));
                StopScan();
                if (modalResult == GlobalTexts.Skip)
                {
                    try
                    {
                        ViewEvent.Invoke(ScanOrgUnitViewEvents.Skip);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogException(ex, "FormScanOrgUnit.BtnSkipClick");
                    }
                }
                else if (modalResult == string.Format("{0}{1}{2}", GlobalTexts.Not, Environment.NewLine, GlobalTexts.Delivered))
                {
                    try
                    {
                        ViewEvent.Invoke(ScanOrgUnitViewEvents.NotDelivered);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogException(ex, "FormScanOrgUnit.BtnNotDeliveredClick");
                    }
                }
                else
                {
                    try
                    {
                        ViewEvent.Invoke(ScanOrgUnitViewEvents.AttemptedDel);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogException(ex, "FormScanOrgUnit.BtnAttemptedDelClick");
                    }
                }
            }
            else
            {
                StartScan();
            }
        }


        /// <summary>
        /// Sets the correct message according to screen
        /// </summary>
        private void SetMessageToDisplay(string messagePart)
        {
            if (_formData.MessageHolder != null)
            {
                //_formData.messageHolder.Update(GlobalTexts.IllegalBarcode + Environment.NewLine + messagePart;
                //if (_formData.MessageHolder.Text.StartsWith(GlobalTexts.IllegalBarcode))
                //{
                //    _formData.messageHolder.Update(_formData.ShouldScanOrgUnitOnly
                //                                       ? GlobalTexts.InvalidNumberOrFormatScanOrEnterLocation
                //                                       : GlobalTexts.InvalidNumberOrFormatScanOrEnterItemOrcarrier;
                //}
                MsgMessage.ShowMessage(_formData.MessageHolder);
            }
        }

        /// <summary>
        /// Set View Elements while scanning consignment item 
        /// </summary>
        private void SetViewForScanningConsignmentItem()
        {
            lblScan.Text = GlobalTexts.ScanOrEnterItemOrCarrier + Colon;

            if (_formData.MessageHolder != null)
            {
                if (_formData.MessageHolder.State != MessageState.Error || (_formData.MessageHolder.State == MessageState.Error && _formData.MessageHolder.ErrorInfo == ErrorInfo.ShipmentStopped))//For first time only
                    txtScannedNumber.Text = string.Empty;
            }
            lblItem.Visible = true;
            lblNoOfLoadCarriers.Visible = true;
        }

        private void SetViewForScanningOrgUnit()
        {
            lblScan.Text = GlobalTexts.ScanEnterLocationId + Colon;

            if (_formData.MessageHolder != null)
            {
                if (_formData.MessageHolder.State == MessageState.Empty)//For first time only
                    txtScannedNumber.Text = _formData.OrgId;
            }
            _tabButtons.SetButtonEnabledState(OkButton, true);


        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            Logger.LogEvent(Severity.Debug, "FormScanOrgUnit; BarcodeScanned");//Added mottak 436 to be removed
            txtScannedNumber.Text = string.Empty;
            ViewEvent.Invoke(ScanOrgUnitViewEvents.BarcodeScanned, scannerOutput);
        }
        #endregion

        #region ButtonClickHandlers

        private void BtnDeviationClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ScanOrgUnitViewEvents.Deviation);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanOrgUnit.BtnDeviationClick");
            }
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ScanOrgUnitViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanOrgUnit.BtnBackClick");
            }
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            try
            {
                if (_formData.ShouldScanOrgUnitOnly)//Org Unit Screen
                {
                    if (!String.IsNullOrEmpty(txtScannedNumber.Text))
                    {
                        ViewEvent.Invoke(ScanOrgUnitViewEvents.BarcodeScanned, new FotScannerOutput { Code = txtScannedNumber.Text.Trim() });
                    }
                }
                else//Item Scanning screen
                {
                    if (!String.IsNullOrEmpty(txtScannedNumber.Text))
                    {
                        ViewEvent.Invoke(ScanOrgUnitViewEvents.BarcodeScanned, new FotScannerOutput { Code = txtScannedNumber.Text.Trim() });
                    }
                    else if (_formData.ItemCount + _formData.LoadCarrierCount > 0)
                        ViewEvent.Invoke(ScanOrgUnitViewEvents.Ok);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanOrgUnit.BtnOkClick");
            }
        }

        #endregion

    }

    #region internal classes

    public class FormDataScanOrgUnit : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
        public bool ShouldScanOrgUnitOnly { get; set; }
        public int ItemCount { get; set; }
        public int LoadCarrierCount { get; set; }
        public string OrgName { get; set; }
        public string OrgId { get; set; }
        public bool DisableDeviation { get; set; }
        public bool ShowAttemptDel { get; set; }

    }

    public class ScanOrgUnitViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
        public const int Reconcilliation = 3;
        public const int Deviation = 4;
        public const int Skip = 5;
        public const int AttemptedDel = 6;
        public const int NotDelivered = 7;
    }

    #endregion


}
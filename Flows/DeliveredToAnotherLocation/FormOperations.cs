﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    public partial class FormOperations : BaseForm
    {
        public FormOperations()
        {
            InitializeComponent();
            
            SetTextToUi();
            advancedListOperationList.BeginUpdate();
            advancedListOperationList.DataSource = _listClientOperationStops;
            advancedListOperationList.EndUpdate();
            advancedListOperationList.DataRows[0].Selected = true;
        }

        private void SetTextToUi()
        {
            btnOk.Text = GlobalTexts.Ok;
            btnBack.Text = GlobalTexts.Back;
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormOperationsViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Operations.BtnBackClick");
            }
        }

        public void PnlConfirmMessagePaint(object sender, PaintEventArgs e)
        {
            try
            {

                var pen = new Pen(Color.FromArgb(113, 112, 116), 2);
                var graphics = e.Graphics;
                graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlConfirmMessage.Width - 3, pnlConfirmMessage.Height - 3));
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Operations.PnlConfirmMessagePaint");
            }
        }

        public void PnlMessagePaint(object sender, PaintEventArgs e)
        {
            try
            {

                var pen = new Pen(Color.FromArgb(113, 112, 116), 2);
                var graphics = e.Graphics;
                graphics.DrawRectangle(pen, new Rectangle(1, 1, pnlMessage.Width - 3, pnlMessage.Height - 3));

                //// Create a red and black bitmap to demonstrate transparency.
                //var bmp = new Bitmap(75, 75);
                //var g = Graphics.FromImage(bmp);

                //g.FillEllipse(new SolidBrush(Color.Red), 0, 0, bmp.Width, bmp.Width);
                //g.DrawLine(new Pen(Color.Black), 0, 0, bmp.Width, bmp.Width);
                //g.DrawLine(new Pen(Color.Black), bmp.Width, 0, 0, bmp.Width);
                //g.Dispose();

                //var attr = new ImageAttributes();

                //// Set the transparency color key based on the upper-left pixel 
                //// of the image.
                //// Uncomment the following line to make all black pixels transparent:
                //// attr.SetColorKey(bmp.GetPixel(0, 0), bmp.GetPixel(0, 0));

                //// Set the transparency color key based on a specified value.
                //// Uncomment the following line to make all red pixels transparent:
                // attr.SetColorKey(Color.White, Color.White);

                //// Draw the image using the image attributes.
                //Rectangle dstRect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                //e.Graphics.DrawImage(bmp, dstRect, 0, 0, bmp.Width, bmp.Height,
                //    GraphicsUnit.Pixel, attr);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Operations.PnlMessagePaint");
            }
        }



        private void BtnScanClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormOperationsViewEvents.Ok);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Operations.BtnScanClick");
            }
        }

        private void AdvancedListOperationListRowSelect(object sender, Resco.Controls.AdvancedList.RowEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Operations.AdvancedListOperationListRowSelect");
            }
        }

        
        
        private readonly IList<BindingStopAmphora> _listClientOperationStops = new List<BindingStopAmphora>();



        public class FormOperationsViewEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
        }

    }
}
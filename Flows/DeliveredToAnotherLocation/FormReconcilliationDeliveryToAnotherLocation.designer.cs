﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    partial class FormReconcilliationDeliveryToAnotherLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
          
            this.ListConsignments = new Resco.Controls.AdvancedList.AdvancedList();
            this.templateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellBottomBorder = new Resco.Controls.AdvancedList.TextCell();
            this.lblOrgName = new Resco.Controls.CommonControls.TransparentLabel();
            this.cellHeaderCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowSelected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellSelectedTextCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedTextCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.cellSelectedTextCol3 = new Resco.Controls.AdvancedList.TextCell();
            this.cellHeaderCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.templateRowUnselected = new Resco.Controls.AdvancedList.RowTemplate();
            this.cellUnselectedTextCol1 = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedTextCol2 = new Resco.Controls.AdvancedList.TextCell();
            this.cellUnselectedTextCol3 = new Resco.Controls.AdvancedList.TextCell();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
         
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrgName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.lblOrgName);
            this.touchPanel.Controls.Add(this.ListConsignments);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLight;
            this.messageControl.Location = new System.Drawing.Point(21, 387);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438,110);
            this.messageControl.TabIndex = 33;
            // 
            // ListConsignments
            // 
            this.ListConsignments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.ListConsignments.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListConsignments.DataRows.Clear();
            this.ListConsignments.GridColor = System.Drawing.Color.Black;
            this.ListConsignments.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[0]);
            this.ListConsignments.Location = new System.Drawing.Point(21, 71);
            this.ListConsignments.Name = "ListConsignments";
            this.ListConsignments.ScrollbarSmallChange = 32;
            this.ListConsignments.ScrollbarWidth = 26;
            this.ListConsignments.ShowHeader = true;
            this.ListConsignments.Size = new System.Drawing.Size(438, 300);
            this.ListConsignments.TabIndex = 3;
            this.ListConsignments.Templates.Add(this.templateHeader);
            this.ListConsignments.Templates.Add(this.templateRowSelected);
            this.ListConsignments.Templates.Add(this.templateRowUnselected);
            this.ListConsignments.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            this.ListConsignments.MultiSelect = false;
            this.ListConsignments.KeyNavigation = true;
            // 
            // templateHeader
            // 
            this.templateHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol1);
            this.templateHeader.CellTemplates.Add(this.cellHeaderCol2);
            this.templateHeader.CellTemplates.Add(this.cellBottomBorder);
            this.templateHeader.Height = 33;
            this.templateHeader.Name = "templateHeader";
            // 
            // cellHeaderCol1
            // 
            this.cellHeaderCol1.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleLeft;
            this.cellHeaderCol1.CellSource.ConstantData = "Item";
            this.cellHeaderCol1.DesignName = "cellHeaderCol1";
            this.cellHeaderCol1.Location = new System.Drawing.Point(10, 0);
            this.cellHeaderCol1.Name = "cellHeaderCol1";
            this.cellHeaderCol1.Size = new System.Drawing.Size(280, 31);
            this.cellHeaderCol1.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);

            //
            // cellBottomBorder
            // 
            this.cellBottomBorder.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellBottomBorder.BackColor = System.Drawing.Color.Black;
            this.cellBottomBorder.DesignName = "cellBottomBorder";
            this.cellBottomBorder.Location = new System.Drawing.Point(0, 30);
            this.cellBottomBorder.Name = "cellBottomBorder";
            this.cellBottomBorder.Size = new System.Drawing.Size(438, 5);   
            // lblOrgName
            // 
            this.lblOrgName.AutoSize = false;
            this.lblOrgName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgName.Location = new System.Drawing.Point(3,32);
            this.lblOrgName.Name = "lblOrgName";
            this.lblOrgName.Size = new System.Drawing.Size(477, 33);
            this.lblOrgName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter; 

            // 
            // cellHeaderCol2
            // 
            this.cellHeaderCol2.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.cellHeaderCol2.CellSource.ConstantData = GlobalTexts.Type;
            this.cellHeaderCol2.DesignName = "cellHeaderCol2";
            this.cellHeaderCol2.Location = new System.Drawing.Point(280, 0);
            this.cellHeaderCol2.Name = "cellHeaderCol2";
            this.cellHeaderCol2.Size = new System.Drawing.Size(-1, 30);
            this.cellHeaderCol2.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
           
            // 
            // templateRowSelected
            // 
            this.templateRowSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(190)))), ((int)(((byte)(74))))); 
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol1);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol2);
            this.templateRowSelected.CellTemplates.Add(this.cellSelectedTextCol3);
            this.templateRowSelected.Height = 30;
            this.templateRowSelected.Name = "templateRowSelected";
            // 
            // cellSelectedTextCol1
            // 
            this.cellSelectedTextCol1.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellSelectedTextCol1.CellSource.ColumnIndex = 0;
            this.cellSelectedTextCol1.DesignName = "cellSelectedTextCol1";
            this.cellSelectedTextCol1.Location = new System.Drawing.Point(10, 0);
            this.cellSelectedTextCol1.Size = new System.Drawing.Size(280, 30);
            this.cellSelectedTextCol1.ForeColor = System.Drawing.Color.White;
            this.cellSelectedTextCol1.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellSelectedTextCol2
            // 
            this.cellSelectedTextCol2.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellSelectedTextCol2.CellSource.ColumnIndex = 1;
            this.cellSelectedTextCol2.DesignName = "cellSelectedTextCol2";
            this.cellSelectedTextCol2.Location = new System.Drawing.Point(280, 0);
            this.cellSelectedTextCol2.Size = new System.Drawing.Size(-1, 30);
            this.cellSelectedTextCol2.ForeColor = System.Drawing.Color.White;
            this.cellSelectedTextCol2.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);

            // 
            // cellSelectedTextCol3
            // 
            this.cellSelectedTextCol3.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellSelectedTextCol3.CellSource.ColumnIndex = 1;
            this.cellSelectedTextCol3.DesignName = "cellSelectedTextCol3";
            this.cellSelectedTextCol3.Location = new System.Drawing.Point(280, 0);
            this.cellSelectedTextCol3.Visible = false;
            this.cellSelectedTextCol3.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);

            // 
            // templateRowUnselected
            // 
            this.templateRowUnselected.BackColor = System.Drawing.Color.White; 
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol1);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol2);
            this.templateRowUnselected.CellTemplates.Add(this.cellUnselectedTextCol3);
            this.templateRowUnselected.Height = 30;
            this.templateRowUnselected.Name = "templateRowUnselected";
            // 
            // cellUnselectedTextCol1
            // 
            this.cellUnselectedTextCol1.Alignment = Resco.Controls.AdvancedList.Alignment.BottomLeft;
            this.cellUnselectedTextCol1.CellSource.ColumnIndex = 0;
            this.cellUnselectedTextCol1.DesignName = "cellUnselectedTextCol1";
            this.cellUnselectedTextCol1.Location = new System.Drawing.Point(10, 0);
            this.cellUnselectedTextCol1.Size = new System.Drawing.Size(280, 30);
            this.cellUnselectedTextCol1.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // cellUnselectedTextCol2
            // 
            this.cellUnselectedTextCol2.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellUnselectedTextCol2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cellUnselectedTextCol2.DesignName = "cellUnselectedTextCol2";
            this.cellUnselectedTextCol2.Location = new System.Drawing.Point(280, 0);
            this.cellUnselectedTextCol2.Size = new System.Drawing.Size(-1,30);
            this.cellUnselectedTextCol2.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);


            // 
            // cellUnselectedTextCol3
            // 
            this.cellUnselectedTextCol3.Alignment = Resco.Controls.AdvancedList.Alignment.BottomCenter;
            this.cellUnselectedTextCol3.DesignName = "cellUnselectedTextCol2";
            this.cellUnselectedTextCol3.Location = new System.Drawing.Point(280, 0);
            this.cellUnselectedTextCol3.Visible = false;
            this.cellUnselectedTextCol3.TextFont = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);


            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(3, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // FormReconcilliationDEliveryToAnotherLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormReconcilliationDeliveryToAnotherLocation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.AdvancedList.AdvancedList ListConsignments;
        private Resco.Controls.AdvancedList.RowTemplate templateHeader;
        private Resco.Controls.AdvancedList.TextCell cellBottomBorder;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol1;
        private Resco.Controls.AdvancedList.RowTemplate templateRowSelected;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol1;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol2;
        private Resco.Controls.AdvancedList.TextCell cellSelectedTextCol3;
        private Resco.Controls.AdvancedList.RowTemplate templateRowUnselected;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol1;
        private Resco.Controls.AdvancedList.TextCell cellHeaderCol2;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol2;
        private Resco.Controls.AdvancedList.TextCell cellUnselectedTextCol3;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgName;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
    }
}
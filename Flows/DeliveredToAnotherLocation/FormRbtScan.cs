﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{

    public partial class FormRbtScan : BaseBarcodeScanForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonSkip = "ButtonSkip";
        private const string ButtonOk = "ButtonOk";

        public FormRbtScan()
        {
            InitializeComponent();
            SetStandardControlProperties(labelModuleName, lblOrgUnitName, lblScanHeading, txtScannedNumber, messageControlValidateScan, null, null);
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToControls()
        {

            lblScanHeading.Text = GlobalTexts.ScanRbtOrEnterPowerUnit;
            lblOrgUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Skip, ButtonSkipClick, ButtonSkip));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
            _multiButtonControl.GenerateButtons();
        }

        

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };

            txtScannedNumber.Tag = keyBoardProperty;
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOk, txtScannedNumber.Text != string.Empty );
            if (messageControlValidateScan != null)
                messageControlValidateScan.ClearMessage();
            if (userFinishedTyping && txtScannedNumber.Text != string.Empty)
            {
                BarcodeEnteredManually(textentered);
            }
        }

        public override void OnShow(IFormData formData)
        {
            txtScannedNumber.Text = string.Empty;
            base.OnShow(formData);
            messageControlValidateScan.ClearMessage();
            SetKeyboardState(KeyboardState.Numeric);
            OnUpdateView(formData);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            var formData = (FormDataRbtScan)iformData;
            if (formData != null)
            {
                PopulateMessage(formData.MessageHolder);
                labelModuleName.Text = formData.HeaderText;
                _multiButtonControl.SetButtonEnabledState(ButtonOk, txtScannedNumber.Text != string.Empty);
            }
        }
        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
                messageControlValidateScan.ShowMessage(messageHolder);
            else
                messageControlValidateScan.ClearMessage();
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtScannedNumber.Text = string.Empty;
            ViewEvent.Invoke(RbtScanViewEvents.RbtScanned, scannerOutput);
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(RbtScanViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRbtScan.ButtonBackClick");
            }
        }

        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(RbtScanViewEvents.Ok, scannerOutput);
        }

        private void ButtonSkipClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(RbtScanViewEvents.Skip);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRbtScan.ButtonSkipClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                BarcodeEnteredManually(txtScannedNumber.Text.Trim());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRbtScan.ButtonBackClick");
            }
        }
        
    }
    #region View specific classes

    public class FormDataRbtScan : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
    }
    #endregion

    public class RbtScanViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int RbtScanned = 2;
        public const int Skip = 3;

    }
}
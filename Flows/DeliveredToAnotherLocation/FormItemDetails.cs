﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    public partial class FormItemDetails : BaseForm
    {
        private readonly MultiButtonControl _tabButtons;
        private const string BackButton = "backButton";
        private FormDataItemDetails _formData;
        private readonly DetailsPanel _detailsPanel;
        private List<DetailsPanel.Info> _infoList ;


        public FormItemDetails()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
            SetStandardControlProperties(labelModuleName, lblOrgName, null, null, null, null, null);
            _detailsPanel = new DetailsPanel();
            touchPanel.Controls.Add(_detailsPanel);
        }

        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.DeliveredToAnotherLocationHeader;
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

        }

        public override void OnShow(IFormData iformData)
        {
            _formData = (FormDataItemDetails)iformData;
            ClearFields();
            if (_formData != null)
            {
                FillConsignmentItemDetails(_formData.ConsignmentEntity);
                if (ModelUser.UserProfile.OrgUnitName != null) lblOrgName.Text = ModelUser.UserProfile.OrgUnitName;
            }
        }

        private void ClearFields()
        {
            _detailsPanel.Controls.Clear();
        }

        /// <summary>
        /// Fills the Item Details
        /// </summary>
        private void FillConsignmentItemDetails(ConsignmentEntity consignmentEntity)
        {
            _infoList = new List<DetailsPanel.Info>();

            if (consignmentEntity is ConsignmentItem)
            {
                _infoList = ItemDetailsUtil.PrepareInfoList(consignmentEntity as ConsignmentItem);
                _detailsPanel.AddSections(_infoList);
            }
            else if (consignmentEntity is LoadCarrier)
            {
                CreateLoadCarrierDetails(consignmentEntity.CastToLoadCarrier());
                _detailsPanel.AddFormattedSections(_infoList);
            }
           // _detailsPanel.AddSections(_infoList);
        }

        public void CreateLoadCarrierDetails(LoadCarrier loadCarrier)
        {
            if (loadCarrier == null) return;
            _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.Carrier, Section = loadCarrier.LoadCarrierId });

            if (loadCarrier.LogicalLoadCarrierDetails != null && loadCarrier.LogicalLoadCarrierDetails != null)
            {
                var loadcarrierDetails = loadCarrier.LogicalLoadCarrierDetails[0];

                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.LoadCarrierType, Section = loadcarrierDetails.LoadCarrierType });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.DestinationName, Section = loadcarrierDetails.DestinationName1 + ' ' + loadcarrierDetails.DestinationName2 });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.PostgangCode, Section = loadcarrierDetails.PostgangCode });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ProductionFlowCode, Section = loadcarrierDetails.ProductionFlowCode });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.TimeCreated, Section = loadcarrierDetails.TimeCreated.ToShortTimeString() });
            }
            else
            {
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.LoadCarrierType, Section = string.Empty });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.DestinationName, Section = string.Empty });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.PostgangCode, Section = string.Empty });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.ProductionFlowCode, Section = string.Empty });
                _infoList.Add(new DetailsPanel.Info { Heading = GlobalTexts.TimeCreated, Section = string.Empty });
            }
        }
    

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(ItemDetailsViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormItemDetails.ButtonBackClick");
            }
        }
    }

    public class FormDataItemDetails : BaseFormData
    {
        public ConsignmentEntity ConsignmentEntity { get; set; }
        public string OrgName { get; set; }

    }

    public class ItemDetailsViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }
}
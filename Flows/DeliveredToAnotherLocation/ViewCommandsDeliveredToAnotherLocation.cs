﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    public class ViewCommandsDeliveredToAnotherLocation
    {
        /// <summary>
        /// Opens up scanning page
        /// </summary>
        /// <param name="scanViewEventHandler">Handler for Scanning view</param>
        /// <param name="messageHolder">Message container</param>
        /// <param name="entityMap">entity map</param>
        /// <param name="shouldScanOrgUnitOnly">Should scan Org Unit only</param>
        /// <param name="orgName"></param>
        /// <param name="consignmentEntity"></param>
        /// <param name="orgUnitId"></param>
        /// <param name="showAttemptDel"></param>
        public void ViewScanOrgUnit(ViewEventDelegate scanViewEventHandler, MessageHolder messageHolder, EntityMap entityMap, bool shouldScanOrgUnitOnly, string orgName, ConsignmentEntity consignmentEntity, String orgUnitId, bool showAttemptDel)
        {

            var formData = new FormDataScanOrgUnit
                               {
                                   ShouldScanOrgUnitOnly = shouldScanOrgUnitOnly,
                                   MessageHolder = messageHolder,
                                   ItemCount = entityMap.ScannedConsignmentItemsTotalCount,
                                   LoadCarrierCount = entityMap.ScannedLoadCarrierTotalCount,
                                   OrgName = orgName,
                                   OrgId = orgUnitId,
                                   DisableDeviation = consignmentEntity == null,
                                   ShowAttemptDel = showAttemptDel
                               };
            var view = ViewCommands.ShowView<FormScanOrgUnit>(formData);
            view.SetEventHandler(scanViewEventHandler);
        }


       




        public void ViewSummaryView(ViewEventDelegate summaryViewHandler, EntityMap entityMap, MessageHolder messageHolder, string orgName)
        {
          var formData = new FormDataReconcilliationDeliveryToAnotherLocation
                               {
                                   OriginalEntityMap = entityMap,
                                   HeaderText = GlobalTexts.DeliveredToAnotherLocationHeader,
                                   MessageHolder = messageHolder,
                                   SelectedOrgName = orgName
                               };
            var view = ViewCommands.ShowView<FormReconcilliationDeliveryToAnotherLocation>(formData);
            view.SetEventHandler(summaryViewHandler);
        }

        public void ViewItemDetails(ViewEventDelegate detailsViewHandler, ConsignmentEntity currentConsignmentEntity, MessageHolder messageHolder, string orgName)
        {
              var formData = new FormDataItemDetails
                               {
                                   ConsignmentEntity = currentConsignmentEntity,
                                   HeaderText = GlobalTexts.DeliveryToTheCustomer,
                                   OrgName = orgName

                               };
            var view = ViewCommands.ShowView<FormItemDetails>(formData);
            view.SetEventHandler(detailsViewHandler);
        }

        public void ShowViewRbtScan(ViewEventDelegate viewEventHandler, MessageHolder messageHolder, string header)
        {
             var formData = new FormDataRbtScan { MessageHolder = messageHolder, HeaderText = header };
            var view = ViewCommands.ShowView<FormRbtScan>(formData);
            view.SetEventHandler(viewEventHandler);
        }

        public void ShowOperations(ViewEventDelegate viewEventHandler, MessageHolder messageHolder)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ViewCommandsDeliveredToAnotherLocation.ShowOperations()");
            var formData = new FormDataRbtScan { MessageHolder = messageHolder };
            var view = ViewCommands.ShowView<FormOperations>(formData);
            view.SetEventHandler(viewEventHandler);
        }

       
    }
}

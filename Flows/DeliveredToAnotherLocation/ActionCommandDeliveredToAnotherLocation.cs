﻿using System;
using System.Linq;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.CauseAndMeasure;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu;
using Com.Bring.PMP.PreComFW.Shared.Flows.GetWorkListItem;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using Consignment = Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap.Consignment;
using LoadCarrier = Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap.LoadCarrier;
using GoodsEvents = Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents.GoodsEvents;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    class ActionCommandDeliveredToAnotherLocation
    {
        private readonly ICommunicationClient _communicationClient;

        public ActionCommandDeliveredToAnotherLocation(ICommunicationClient communicationClient)
        {
            _communicationClient = communicationClient;
        }

        internal FlowStateDeliveredToAnotherLocation ValidateFlowScanBarcodeForOrgUnit(FlowResultScanBarcode flowResultScanBarcode, BarcodeType validTypes, ref ConsignmentEntity consignmentEntity, ref MessageHolder messageHolder, out string loadCarrier, ref string orgUnit, ref ConsignmentEntity tempConsignmentEntity)
        {
            FlowStateDeliveredToAnotherLocation resultState;
            loadCarrier = string.Empty;

            switch (flowResultScanBarcode.State)
            {
                case FlowResultState.Ok:

                    if ((flowResultScanBarcode.BarcodeType & validTypes) == BarcodeType.Unknown)
                    {
                        messageHolder.Update(GlobalTexts.InvalidNumber + ".\r\n" + BaseActionCommands.ScanTextFromValidBarcodeTypes(validTypes, ""), MessageState.Error);
                        resultState = (validTypes & flowResultScanBarcode.BarcodeType) == BarcodeType.OrgUnitId ? FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit : FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                    }
                    else
                    {
                        tempConsignmentEntity = flowResultScanBarcode.CurrentConsignmentEntity;
                        switch (flowResultScanBarcode.BarcodeType)
                        {
                            case BarcodeType.ConsignmentItemNumber:
                                if (tempConsignmentEntity != null)
                                    SetMessageHolderScanned(tempConsignmentEntity, ref messageHolder);
                                //For HubWorker1, PK/PIB, Driver 2 and Landpostbud (FOT-062 and its extension), User not need to check attempted delivery to be last event
                                resultState = (ModelUser.UserProfile.Role != Role.PostOfficeAndStore) && (ModelUser.UserProfile.Role != Role.HubWorker1) ?
                                    FlowStateDeliveredToAnotherLocation.ActionCheckLastGoodsEvent :
                                    FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix;
                                break;

                            case BarcodeType.OrgUnitId:
                                if (flowResultScanBarcode.MessageHolder != null)
                                    messageHolder = new MessageHolder();
                                orgUnit = flowResultScanBarcode.OrgUnitId;
                                resultState = FlowStateDeliveredToAnotherLocation.ActionValidateOrgUnit;
                                break;

                            case BarcodeType.LoadCarrier:
                                loadCarrier = flowResultScanBarcode.CurrentConsignmentEntity.LoadCarrierId;
                                consignmentEntity = flowResultScanBarcode.CurrentConsignmentEntity;

                                if (consignmentEntity != null)
                                    consignmentEntity.CastToLoadCarrier().GetMessageForRegistrationOfCarrier(messageHolder, null);
                                resultState = FlowStateDeliveredToAnotherLocation.ActionStoreLoadCarrier;
                                //TODO: remove commented code after testing
                                //if (consignmentEntity.IsScannedOnline == false)//offline
                                //{
                                //    messageHolder.Update(String.Format(GlobalTexts.OfflineLoadCarrierUnloaded, consignmentEntity.EntityDisplayId.Trim());
                                //    messageHolder.State = MessageState.Warning;
                                //}
                                //else//Online
                                //{
                                //    if (consignmentEntity.IsKnownInLm)//Did validate Ok
                                //    {
                                //        messageHolder.Update(String.Format(GlobalTexts.LoadCarrierUnloaded, consignmentEntity.EntityDisplayId.Trim());
                                //        messageHolder.State = MessageState.Information;
                                //    }
                                //    else//ValidateOK == false
                                //    {
                                //        messageHolder.Update(String.Format(GlobalTexts.LoadCarrierUnknownInLm, consignmentEntity.EntityDisplayId);
                                //        messageHolder.State = MessageState.Error;
                                //        resultState = FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                                //    }

                                //}

                                break;

                            default:
                                resultState = FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit;
                                break;
                        }
                    }

                    break;

                case FlowResultState.Error:
                    //messageHolder.State = MessageState.Error;
                    //messageHolder.Update(GlobalTexts.IllegalBarcode;
                    resultState = validTypes == BarcodeType.OrgUnitId ? FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit : FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                    break;
                case FlowResultState.Cancel:
                    resultState = FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit;
                    break;

                case FlowResultState.Finished:
                    resultState = FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit;
                    break;

                case FlowResultState.Warning:
                    resultState = FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                    break;

                case FlowResultState.Duplicate:

                    switch (flowResultScanBarcode.BarcodeType)
                    {
                        case BarcodeType.LoadCarrier:
                            messageHolder.Update(GlobalTexts.LoadCarrierAlreadyScanned, MessageState.Warning);
                            resultState = FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                            break;

                        case BarcodeType.ConsignmentItemNumber:
                            consignmentEntity = flowResultScanBarcode.CurrentConsignmentEntity;
                            resultState = FlowStateDeliveredToAnotherLocation.FlowCorrectionMenu;
                            break;
                        default:
                            resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                            break;
                    }


                    break;

                default:
                    resultState = FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }

            return resultState;
        }

        /// <summary>
        /// 
        /// </summary>
        public void SetMessageHolderScanned(ConsignmentEntity consignmentEntity, ref MessageHolder messageHolder)
        {
            var messageHolderText = string.Empty;
            var messageHolderState = MessageState.Empty;

            if (consignmentEntity != null)
            {
                if (consignmentEntity is ConsignmentItem)
                {
                    messageHolderText = String.Format(GlobalTexts.ItemUnloaded, consignmentEntity.EntityDisplayId);
                }

                if (consignmentEntity.IsScannedOnline == false)
                {
                    messageHolderText = GlobalTexts.Offline + ": " + messageHolderText;
                    messageHolderState = MessageState.Warning;
                }
                else
                {
                    messageHolderState = MessageState.Information;
                }
            }

            messageHolder.Update(messageHolderText, messageHolderState);
        }

        /// <summary>
        /// Validate Organisation Unit if its valid from Database
        /// </summary>
        internal FlowStateDeliveredToAnotherLocation ValidateOrgUnit(string orgUnit, MessageHolder messageHolder, out string orgName)
        {
            orgName = SettingDataProvider.Instance.GetLocationName(orgUnit);
            if (string.IsNullOrEmpty(orgName) == false)//Not valid Org Unit
            {
                return FlowStateDeliveredToAnotherLocation.ActionIsRbtInfoRequired;
            }
            //If illegal Organisation Unit show error message
            SoundUtil.Instance.PlayScanErrorSound();
            if (messageHolder != null)
            {
                messageHolder.Update(GlobalTexts.PleaseScanValidOrgUnit, MessageState.Error);
            }
            return FlowStateDeliveredToAnotherLocation.ViewScanOrgUnit;
        }

        //TODO :: Write Unit Test
        internal bool StoreLoadCarrier(EntityMap entityMap, ConsignmentEntity consignmentEntity, ref MessageHolder messageHolder)
        {
            try
            {
                consignmentEntity.EntityStatus |= EntityStatus.Scanned;
                entityMap.Store(consignmentEntity);
                return true;
            }
            catch (Exception e)
            {
                Logger.LogException(e, " ActionCommandDeliveredToAnotherLocation StoreLoadCarrier");
            }

            return true;
        }

        public FlowStateDeliveredToAnotherLocation ValidateFlowScanRbt(FlowResultScanBarcode subflowResult, out ConsignmentEntity consignmentEntity, ref string loadCarrierId, ref BarcodeRouteCarrierTrip rbt)
        {
            var resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;

            consignmentEntity = subflowResult.CurrentConsignmentEntity;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:

                    if (subflowResult.BarcodeType == BarcodeType.Unknown)
                        resultState = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                    else
                    {
                        loadCarrierId = subflowResult.BarcodeRouteCarrierTrip.LoadCarrierId;
                        rbt = subflowResult.BarcodeRouteCarrierTrip;
                        return FlowStateDeliveredToAnotherLocation.ActionGetRbtWorkListItem;
                    }
                    break;

                case FlowResultState.Error:
                    resultState = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                    break;

                case FlowResultState.Cancel:
                    resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                    break;

                case FlowResultState.Finished:
                    break;

                default:
                    resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            return resultState;
        }

        public bool IsScannedBefore(EntityMap entityMap, ref ConsignmentEntity damageEntity, MessageHolder messageHolder, ref int numberOfItems)
        {
            if (entityMap.IsScannedBefore(damageEntity))
            {
                messageHolder.Update(string.Format(GlobalTexts.LoadCarrierAlreadyRegistered, damageEntity.ConsignmentId), MessageState.Error);
                SoundUtil.Instance.PlayWarningSound();
                return true;
            }
            numberOfItems++;
            return false;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// Deletes Consignment 
        /// </summary>
        internal void DeleteItem(ConsignmentEntity selectedConsignmentEntity, EntityMap entityMap, out MessageHolder messageHolder)
        {
            messageHolder = new MessageHolder();
            if (entityMap.GetScannedBefore(ref selectedConsignmentEntity))
            {
                entityMap.RemoveEntity(selectedConsignmentEntity);
            }
        }


        internal FlowStateDeliveredToAnotherLocation BackFromFlowSignature(FlowResultSignature subflowResult, out Signature signature)
        {
            FlowStateDeliveredToAnotherLocation returnState;
            signature = null;

            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    {
                        signature = subflowResult.Signature;
                        return FlowStateDeliveredToAnotherLocation.ActionSendGoodsEvent;
                    }
                case FlowResultState.Cancel:
                    {
                        returnState = FlowStateDeliveredToAnotherLocation.FlowReconcilliation;
                        break;
                    }
                default:
                    {
                        returnState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                        Logger.LogEvent(Severity.Error, "Unhandled subflowresult from signatureflow");
                        break;
                    }
            }
            return returnState;
        }

        /// <summary>
        /// Send Goods Event for scanned Goods
        /// </summary>
        internal FlowStateDeliveredToAnotherLocation SendGoodsEvent(Signature signature, EntityMap entityMap, string orgUnit, WorkListItem workListItem)
        {

            if (entityMap.ScannedLoadCarrierTotalCount > 0)
            {
                var lstLoadCarriers = entityMap.Where(p => p.Value is LoadCarrier).Select(p => p.Value);
                //Add Goods Event For Load Carrier
                var loadCarriers = lstLoadCarriers as List<Consignment> ?? lstLoadCarriers.ToList();

                //Remove Load Carrier from Entity Map
                foreach (var consignment in loadCarriers)
                {
                    SendLoadCarrierEvent(consignment.LoadCarrierId, EventCodeUtil.GetEventCodeFromProcessForLoadCarrier(), orgUnit, signature, workListItem);
                    entityMap.RemoveEntity(consignment);
                }
            }

            if (entityMap.HasScannedEntities)
            {
                var goodsEvent = GoodsEventHelper.CreateGoodsEventMessage(entityMap, ModelMain.SelectedOperationProcess, signature);
                if (goodsEvent != null)
                {
                    goodsEvent.EventInformation.OrgUnitIdDeliveredTo = orgUnit;
                    if (workListItem != null)
                    {
                        if (goodsEvent.OperationInformation == null)
                            goodsEvent.OperationInformation = new GoodsEventsOperationInformation();
                        goodsEvent.OperationInformation.TripId = workListItem.TripId;
                        goodsEvent.OperationInformation.RouteId = workListItem.RouteId;
                        goodsEvent.OperationInformation.ExternalTripId = workListItem.ExternalTripId;
                    }
                    var selectedOperationId = ModelMain.SelectedOperationProcess.OperationId;

                    //Store eventinformation in goods event history
                    var transaction = new Transaction(Guid.NewGuid(),
                                                      (byte)TransactionCategory.GoodsEventsEventInformation,
                                                      selectedOperationId);
                    CommunicationClient.Instance.SendGoodsEvent(goodsEvent, transaction);
                }
            }

            return FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
        }

        /// <summary>
        /// Send Load Carrier Event for a load carrier
        /// </summary>
        public void SendLoadCarrierEvent(string loadcarrier, string eventCode, string orgUnit, Signature signature, WorkListItem workListItem)
        {
            if (string.IsNullOrEmpty(loadcarrier))
            {
                return;
            }
            var eventInfo = LoadCarrierEventHelper.CreateLoadCarrierEvent(loadcarrier, null, null, null, null, eventCode, signature);
            if (eventInfo != null)
            {
                eventInfo.EventInformation.OrgUnitIdTransferredTo = orgUnit;
                if (workListItem != null)
                {
                    eventInfo.EventInformation.StopId = workListItem.StopId;
                    eventInfo.EventInformation.TripId = workListItem.TripId;
                }
                if (CommunicationClient.Instance != null)
                {
                    CommunicationClient.Instance.SendLoadCarrierEvent(eventInfo);
                }
            }
        }

        public void SetDuplicateLoadCarrierMessage(ref MessageHolder messageHolder)
        {
            messageHolder.Update(GlobalTexts.LoadCarrierAlreadyScanned, MessageState.Information);
        }

        internal void ValidateSubflowResultWithChangedEntityMap(ref EntityMap entityMap, IFlowResult subflowResult, ref MessageHolder messageHolder, ref ConsignmentEntity consignmentEntity)
        {
            var flowResultScanCorrectionMenu = (FlowResultCorrectionMenu)subflowResult;

            if (flowResultScanCorrectionMenu.EntityMap != null)
            {
                entityMap = flowResultScanCorrectionMenu.EntityMap;
            }
            if (!entityMap.IsScannedBefore(consignmentEntity))
                consignmentEntity = null;

            if (flowResultScanCorrectionMenu.MessageHolder != null)
            {
                if (flowResultScanCorrectionMenu.MessageHolder.State != MessageState.Empty)
                {
                    messageHolder = flowResultScanCorrectionMenu.MessageHolder;
                }
            }
        }

        /// <summary>
        /// Get Current Org Unit Name to be prefilled for specific roles 
        /// </summary>
        internal string GetOrgUnitForRoles(ref string orgName)
        {
            switch (ModelUser.UserProfile.Role)
            {
                case Role.Driver2:
                    if (ModelUser.UserProfile.OrgUnitId != null)
                        orgName = SettingDataProvider.Instance.GetLocationName(ModelUser.UserProfile.OrgUnitId);
                    return ModelUser.UserProfile.OrgUnitId;
                case Role.Driver1:
                    return string.Empty; //TODO: fetch from operation list
                default:
                    return string.Empty;

            }
        }


        internal FlowStateDeliveredToAnotherLocation ValidateFlowVasMatrixResult(FlowResultVasMatrix subflowResult, MessageHolder messageHolder, ref ConsignmentEntity tempConsignmentEntity, ref ConsignmentEntity currentConsignmentEntity)
        {
            if(subflowResult != null)
            {
                switch (subflowResult.State)
                {
                    case FlowResultState.Ok:
                        {
                            if (subflowResult.MessageIsFromVasMatrix)
                            {
                                messageHolder.Update(subflowResult.Message, MessageState.Warning);
                            }
                            currentConsignmentEntity = tempConsignmentEntity;
                            return FlowStateDeliveredToAnotherLocation.ActionStoreInLocalcache;
                        }

                    case FlowResultState.Cancel:
                        messageHolder.Update(GlobalTexts.ScanningAborted, MessageState.Warning);
                        return FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                }
            }
            return FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
        }



        internal void ClearScannings(ref EntityMap entityMap, ref string orgUnit, ref string orgName)
        {
            if (entityMap != null)
            {
                entityMap = new EntityMap();
                orgUnit = String.Empty;
                orgName = string.Empty;
            }

        }

        public bool VerifyFlowResultCauseAndMeasure(FlowResultCauseAndMeasure flowResultCauseAndMeasure, ref CauseMeasure causeMeasure, out string freeText, List<StoredCauseAndMeasure> listCauseAndMeasure, ref ConsignmentEntity currentConsignmentEntity, MessageHolder messageHolder, bool isAttemptedDelivery)
        {
            bool isValid = flowResultCauseAndMeasure != null && flowResultCauseAndMeasure.State == FlowResultState.Ok;
            freeText = string.Empty;
            if (isValid)
            {
                causeMeasure = flowResultCauseAndMeasure.SelectedReasonAndActionCombination;//Store 
                freeText = flowResultCauseAndMeasure.Comment;
                if ((currentConsignmentEntity.ConsignmentInstance.HasUnknownConsignment == false))
                {
                    var measure = new StoredCauseAndMeasure
                    {
                        CauseMeasure = causeMeasure,
                        Consignment = currentConsignmentEntity.ConsignmentInstance,
                        FreeText = freeText,
                        IsAttemptedDelivery = isAttemptedDelivery
                    };
                    listCauseAndMeasure.Add(measure);
                }
            }
            else
            {
                currentConsignmentEntity = null;//Clear current entity
                messageHolder.Update(isAttemptedDelivery ? GlobalTexts.AttemptDeliveryAborted : GlobalTexts.NotDeliveredAborted, MessageState.Warning);
            }
            return isValid;
        }

        public void CreateGoodsEvents(ConsignmentEntity consignmentEntity, bool isAttemptedDelivery, CauseMeasure selectedCauseMeasure, string freetext)
        {
            string eventCode = isAttemptedDelivery ? "H" : "VI";
            var goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity, eventCode);
            goodsEvent.EventInformation.CauseCode = selectedCauseMeasure.CauseCode;
            goodsEvent.EventInformation.ActionCode = selectedCauseMeasure.MeasureCode;
            goodsEvent.EventInformation.EventComment = freetext;
            SendGoodsEvent(goodsEvent);
        }

        public void SendGoodsEvent(GoodsEvents goodsEvent)
        {
            var transaction = new Transaction(Guid.NewGuid(),
                                              (byte)TransactionCategory.GoodsEventsEventInformation,
                                              "Not Delivered");
            _communicationClient.SendGoodsEvent(goodsEvent, transaction);
        }

        public FlowStateDeliveredToAnotherLocation SelectNextState(List<Stop> listStops)
        {
            var state = FlowStateDeliveredToAnotherLocation.ViewShowOperations;
            if (listStops != null)
            {
                state = listStops.Count > 1 ? FlowStateDeliveredToAnotherLocation.ViewShowOperations : FlowStateDeliveredToAnotherLocation.ActionPrepareWorkListItem;
            }
            return state;
        }

        /// <summary>
        /// Get Goods List Interface to be called 
        /// </summary>
        public List<Stop> GetPlannedOperations(BarcodeRouteCarrierTrip rbt, string powerUnitId, MessageHolder messageHolder)
        {

            // T20240_GetGoodsListFromFOTReply reply = RequestForOperations(rbt, powerUnitId);
            //  if (reply == null)
            //  {
            //      messageHolder.Update("No operations received ";
            //      messageHolder.State = MessageState.Error;
            //  }
            return null;
        }

        /// <summary>
        /// Set Next state after Checking goods event 
        /// </summary>
        /// <param name="currentConsignmentEntity"></param>
        /// <returns></returns>
        internal FlowStateDeliveredToAnotherLocation SetNextState(ConsignmentEntity currentConsignmentEntity)
        {
            if (currentConsignmentEntity.Event != null)
            {
                if (currentConsignmentEntity.Event.First().EventCode ==
                    EventCodeUtil.GetEventCodeFromProcess(Process.AttemptedDelivery))//Check if last event is Attempted Delivery
                {
                    return FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix;
                }
            }
            return FlowStateDeliveredToAnotherLocation.ActionIsMissingInformationAvailableForConsignment;
        }

        /// <summary>
        /// Check if user has already entered information of cause and measure for the consignment
        /// </summary>
        internal FlowStateDeliveredToAnotherLocation IsCauseMeasureEnteredBefore(List<StoredCauseAndMeasure> listCauseAndMeasure, ConsignmentEntity currentConsignmentEntity, EntityMap entityMap, ref CauseMeasure causeAndMeasure, ref string freeText, ref bool isAttemptedDelivery)
        {
            if (currentConsignmentEntity != null)
            {
                //If Consignment does not exist
                foreach (var storedCauseAndMeasure in listCauseAndMeasure)
                {

                    if (storedCauseAndMeasure.Consignment != null && storedCauseAndMeasure.Consignment.ConsignmentId == currentConsignmentEntity.ConsignmentId)
                    {
                        Consignment consignment = entityMap.GetConsignment(storedCauseAndMeasure.Consignment.ConsignmentId);
                        if (consignment != null)
                        {
                            causeAndMeasure = storedCauseAndMeasure.CauseMeasure;
                            freeText = storedCauseAndMeasure.FreeText;
                            isAttemptedDelivery = storedCauseAndMeasure.IsAttemptedDelivery;
                            if (storedCauseAndMeasure.CauseMeasure == null)//consignment is Skipped earlier
                                return FlowStateDeliveredToAnotherLocation.FlowExecuteVasMatrix;
                            return FlowStateDeliveredToAnotherLocation.ActionSendReasonCodeEvent;
                        }
                        listCauseAndMeasure.Remove(storedCauseAndMeasure);//If does not exist in entity map remove it from cause and measure collection
                        SoundUtil.Instance.PlayWarningSound();
                        return FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                    }
                }
                SoundUtil.Instance.PlayWarningSound();
                return FlowStateDeliveredToAnotherLocation.ViewScanShipment;
            }
            return FlowStateDeliveredToAnotherLocation.ThisFlowComplete;

        }

        //TODO:: Write Test Case
        /// <summary>
        /// This method, validates the result from flow FlowGetWorkListItem and decides which will be the next stateId.
        /// </summary>
        /// <returns>Returns next stateId to execute.</returns>
        public FlowStateDeliveredToAnotherLocation ValidateFlowGetWorkListItemResult(FlowResultGetWorkListItem subflowResult, ref WorkListItem newWorkListItem, MessageHolder messageHolder, ref BarcodeRouteCarrierTrip rbt, ref string loadCarrier, ref string routeId, BarcodeType scannedBarcodeType)
        {
            var resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
            if (subflowResult != null)
            {
                switch (subflowResult.State)
                {
                    case FlowResultState.Ok:
                        newWorkListItem = subflowResult.WorkListItem;
                        switch (scannedBarcodeType)
                        {
                            case BarcodeType.Rbt:
                                rbt = newWorkListItem.Rbt;
                                loadCarrier = newWorkListItem.Rbt != null ? newWorkListItem.Rbt.LoadCarrierId : "";
                                routeId = newWorkListItem.Rbt != null ? newWorkListItem.Rbt.RouteId : "";
                                ModelMain.SelectedOperationProcess.UpdateFromWorkListItem(newWorkListItem);
                                break;
                        }
                        resultState = FlowStateDeliveredToAnotherLocation.ViewScanShipment;
                        break;

                    case FlowResultState.Cancel:
                        switch (scannedBarcodeType)
                        {
                            case BarcodeType.Rbt:
                                messageHolder.Update(GlobalTexts.ScanningRbtAborted, MessageState.Warning);
                                break;

                            case BarcodeType.LoadCarrier:
                                messageHolder.Update(GlobalTexts.ScanningLoadCarrierAborted, MessageState.Warning);
                                break;

                        }
                        resultState = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                        break;

                    case FlowResultState.Error:
                        messageHolder.Update(subflowResult.Message, MessageState.Error);
                        resultState = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                        break;

                    default:
                        resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                        Logger.LogEvent(Severity.Error, "Encountered unknown State");
                        break;
                }
            }
            return resultState;
        }


        //TODO:: Write Test Case
        /// <summary>
        /// This method, validates the result from flow FlowShowOperations and decides which will be the next stateId.
        /// </summary>
        /// <returns>Returns next stateId to execute.</returns>
        public FlowStateDeliveredToAnotherLocation ValidateFlowRbtScanResult(FlowResultScanBarcode subflowResult, out ConsignmentEntity consignmentEntity, ref string loadCarrierId, ref BarcodeRouteCarrierTrip rbt)
        {
            var resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;

            consignmentEntity = subflowResult.CurrentConsignmentEntity;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:

                    if (subflowResult.BarcodeType == BarcodeType.Unknown)
                        resultState = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                    else
                    {
                        loadCarrierId = subflowResult.BarcodeRouteCarrierTrip.LoadCarrierId;
                        rbt = subflowResult.BarcodeRouteCarrierTrip;
                        return FlowStateDeliveredToAnotherLocation.FlowGetWorkListItem;
                    }
                    break;

                case FlowResultState.Error:
                    resultState = FlowStateDeliveredToAnotherLocation.ViewScanRbt;
                    break;

                case FlowResultState.Cancel:
                    resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                    break;

                case FlowResultState.Finished:
                    break;

                default:
                    resultState = FlowStateDeliveredToAnotherLocation.ThisFlowComplete;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            return resultState;
        }

        /// <summary>
        /// Store skipped consignment
        /// </summary>
        internal void StoreSkippedConsignment(ref List<StoredCauseAndMeasure> listCauseAndMeasure, ConsignmentEntity currentConsignmentEntity)
        {
            if (currentConsignmentEntity != null && currentConsignmentEntity.ConsignmentInstance != null)
            {
                if ((currentConsignmentEntity.ConsignmentInstance.ConsignmentId != ConsignmentEntity.UndefinedConsignmentId))
                {
                    var measure = new StoredCauseAndMeasure
                    {
                        CauseMeasure = null,
                        Consignment = currentConsignmentEntity.ConsignmentInstance,
                        FreeText = string.Empty
                    };
                    listCauseAndMeasure.Add(measure);
                }
            }

        }

        internal FlowStateDeliveredToAnotherLocation BackFromExecuteVasFinalProcess(FlowResultVasMatrix subflowResult, EntityMap entityMap, int itemCountBeforeVas)
        {
            if (subflowResult == null)//
            {
                Logger.LogEvent(Severity.Debug, "FlowResultVasMatrix Cannot be null");
                return FlowStateDeliveredToAnotherLocation.FlowReconcilliation;//TODO
            }
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    {
                        if (entityMap.ScannedConsignmentItemsTotalCount + entityMap.ScannedLoadCarrierTotalCount != itemCountBeforeVas)//If items have been removed by VAS 
                        {
                            return FlowStateDeliveredToAnotherLocation.FlowReconcilliation;
                        }
                        return entityMap.ScannedConsignmentItemsTotalCount + entityMap.ScannedLoadCarrierTotalCount > 0 ?
                            FlowStateDeliveredToAnotherLocation.ActionSendGoodsEvent :
                            FlowStateDeliveredToAnotherLocation.FlowReconcilliation;

                    }

                case FlowResultState.Cancel:
                    return FlowStateDeliveredToAnotherLocation.FlowReconcilliation;

                default:
                    return FlowStateDeliveredToAnotherLocation.FlowReconcilliation;
            }


        }

        /// <summary>
        /// Is RBT information required according to role, only AMPHORA Driver1 requires RBT information
        /// </summary>
        internal FlowStateDeliveredToAnotherLocation RbtInfoRequired()
        {
            bool rbtInfoRequired = (ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Amphora) &&
                                   (ModelUser.UserProfile.Role == Role.Driver1);
            if (rbtInfoRequired)
            {
                return FlowStateDeliveredToAnotherLocation.ActionPrepareWorkListItem;
            }
            return FlowStateDeliveredToAnotherLocation.ViewScanShipment;
        }
    }
}

﻿
using System;
using Resco.Controls;
using Resco.Controls.CommonControls;
using Alignment = Resco.Controls.OutlookControls.Alignment;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.DeliveredToAnotherLocation
{
    partial class FormScanOrgUnit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblItem = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtScannedNumber = new PreCom.Controls.PreComInput2();
            this.lblScan = new Resco.Controls.CommonControls.TransparentLabel();
            this.MsgMessage = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.pictureBoxConsignment = new System.Windows.Forms.PictureBox();
            this.lblNoAttemptedDel = new System.Windows.Forms.Label();
               this.lblNoOfLoadCarriers = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblChoose = new System.Windows.Forms.Label();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            //this.touchPanel.Controls.Add(this.pnlAttemptedDelivery);
            this.touchPanel.Controls.Add(this.lblItem);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.MsgMessage);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblNoOfLoadCarriers);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.lblScan);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
          
            // 
            // lblItem
            // 
            //this.lblItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblItem.Location = new System.Drawing.Point(21, 388);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(133, 29);
            this.lblItem.Text = "No. of Items";
            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(21, 142);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(438, 48);
            this.txtScannedNumber.TabIndex = 24;
            this.txtScannedNumber.TextTranslation = false;
            // 
            // lblScan
            // 
            this.lblScan.AutoSize = false;
            this.lblScan.Location = new System.Drawing.Point(21, 90-8);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(438, 60);
            this.lblScan.Text = "Scan/Enter Location Id";
            // 
            // lblNoOfLoadCarriers
            // 
            this.lblNoOfLoadCarriers.Location = new System.Drawing.Point(161 + 19, 348);
            this.lblNoOfLoadCarriers.Name = "lblNoOfLoadCarriers";
            this.lblNoOfLoadCarriers.Size = new System.Drawing.Size(14, 29);
            // 
            // MsgMessage
            // 
            this.MsgMessage.Location = new System.Drawing.Point(21, 199);
            this.MsgMessage.MessageText = string.Empty;
            this.MsgMessage.Name = "MsgMessage";
            this.MsgMessage.Size = new System.Drawing.Size(438, 141);
            this.MsgMessage.TabIndex = 14;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 32);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(477, 33);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.TopCenter;
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(3, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(474, 27);
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // pictureBoxConsignment
            // 
            this.pictureBoxConsignment.Location = new System.Drawing.Point(185, 8);
            this.pictureBoxConsignment.Name = "pictureBoxConsignment";
            this.pictureBoxConsignment.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxConsignment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // lblNoAttemptedDel
            // 
            this.lblNoAttemptedDel.Location = new System.Drawing.Point(55, 71);
            this.lblNoAttemptedDel.Name = "lblNoAttemptedDel";
            this.lblNoAttemptedDel.Size = new System.Drawing.Size(305, 98);
            this.lblNoAttemptedDel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblChoose
            // 
            this.lblChoose.Location = new System.Drawing.Point(28, 177);
            this.lblChoose.Name = "lblChoose";
            this.lblChoose.Size = new System.Drawing.Size(408, 40);
            this.lblChoose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FormAttemptedDelivery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormScanOrgUnit";
            this.Text = "FormScanOrgUnit";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNoOfLoadCarriers)).EndInit();
            this.ResumeLayout(false);

        }
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblScan;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl MsgMessage;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        //private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private TransparentLabel lblItem;
        private System.Windows.Forms.PictureBox pictureBoxConsignment;
        private System.Windows.Forms.Label lblNoAttemptedDel;
        private System.Windows.Forms.Label lblChoose;
        private TransparentLabel lblNoOfLoadCarriers;
    }
}
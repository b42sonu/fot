﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;


//US 27 : http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554919
namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    public enum FlowStatesPrinter
    {
        ActionGetStateBasedonProcess,

        //Into Holding out at hub
        ActionIntoHoldingAtHubSendRequesPrintBcReport,

        //Remainin goods at hub
        ActionRemainingGoodsAtHubIsPrinterKnown,

        //Unloadlist States
        ActionUnloadlistIsWorkListItemLoadCarrier,
        ActionUnloadListAmphoraPrintRequest,

        //LoadList states
        ActionloadlistIsPrinterKnown,
        ActionLoadIsWorkListIemLoadCarrier,
        ActionSetErrorMessageForLoadLineHaul,
        ActionLoadListIsCarrierLineHaul,
        ActionLoadListSendPrintCd,
        ActioLoadListSendPrintBcReport,

        //Building of load carrier
        ActionBuildingLoadCarrierIsCarrierForLineHaul,
        ActionBuildingOfLoadCarrierIsEmailSet,
        ActionBuildingOfLoadCarrierIsPrinterKnown,
        ActionBuildingLoadCarrierSetErrorMessageLoadLineHaul,
        ActionBuildingLoadCarrierPrintCdRequest,
        ActionBuildingLoadCarrierIsEmailAddressSet,

        //Common States
        ActionIsStartedFromGoods,
        ActionGetPrinters,
        ActionSetErrorMessageForNoPrinters,
        ActionSetPrinter,
        ActionHasLoadCarrierEmailSet,
        ActionGetNextStateAfterDefaultPrinter,
        ActionGetTheLastScreenAfterCancelFromChangePrinter,
        ActionIsPrinterSet,
        ActionBcReport,
        ActionSelectPrinter,
        ActionValidateLoadCarrier,
        ActionCreateNewWorkListItem,
        ActionCreateNewWorkListItemFromRbt,
        ActionSendBcReportForRbt,

        ActionBackFromFlowScanBarcode,
        ActionGetWorkListItem,

        //View states
        ViewCommands,
        ThisFlowComplete,
        ViewShowPrinter,
        ViewShowPrintInformation,
        ViewScanLogicalLoadCarrier

    }

    public class FlowResultPrintLoadList : BaseFlowResult
    {
    }

    public class FlowDataPrintLoadList : BaseFlowData
    {
        public WorkListItem WorkListItem;
        public bool IsProcessStartedFromGoods;
        public bool IsProcessStartedFromReconcilition;
        public string PlacementId { get; set; }
    }

    public class FlowPrintLoadList : BaseFlow
    {
        private readonly Process _runningProcess;
        private bool _isMailAddressSet;

        private string _selectedPrinter;
        private readonly FlowResultPrintLoadList _flowResult;
        private FlowDataPrintLoadList _flowData;
        private readonly ActionCommandsPrinter _actionCommandsPrinter;
        private readonly ViewCommandsPrinter _viewCommandsPrinter;
        private List<EntityPrinter> _printers;
        private MessageHolder _messageHolder;
        private WorkListItem _activeWorkListItem;
        private FlowScanBarcode _flowScanBarcodeInstance;
        private ConsignmentEntity _currentScanning;
        private readonly EntityMap _entityMap;
        private string _loadCarrier;
        private bool _isSecondTime;

        public FlowPrintLoadList()
        {
            _entityMap = new EntityMap();
            _runningProcess = BaseModule.CurrentFlow.CurrentProcess;
            _messageHolder = new MessageHolder();
            _viewCommandsPrinter = new ViewCommandsPrinter();
            _flowResult = new FlowResultPrintLoadList();
            _flowData = new FlowDataPrintLoadList();
            _actionCommandsPrinter = new ActionCommandsPrinter(CommunicationClient.Instance);
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowPrint.BeginFlow");
            _flowData = (FlowDataPrintLoadList)flowData;
            _activeWorkListItem = _flowData.WorkListItem;
            if (ModelUser.UserProfile.DefaultPrinter != null) _selectedPrinter = ModelUser.UserProfile.DefaultPrinter;
            ExecuteState(FlowStatesPrinter.ActionIsStartedFromGoods);
        }


        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesPrinter)state);
        }

        public void ExecuteState(FlowStatesPrinter state)
        {
            if (state > FlowStatesPrinter.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesPrinter state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowPrintLoadList;" + state);  
            try
            {
                switch (state)
                {
                    case FlowStatesPrinter.ViewShowPrinter:
                        _viewCommandsPrinter.ShowPrinterListForm(_printers, _selectedPrinter);
                        ViewCommands.CurrentView.SetEventHandler(FormDisplayPrintersEventHandler);
                        break;

                    case FlowStatesPrinter.ViewShowPrintInformation:
                        _isSecondTime = true;
                        _viewCommandsPrinter.ShowFormPrintInformation(_messageHolder, _selectedPrinter, _flowData.WorkListItem, _isMailAddressSet);
                        ViewCommands.CurrentView.SetEventHandler(FormPrintInformationEventHandler);
                        break;

                    case FlowStatesPrinter.ViewScanLogicalLoadCarrier:
                        _isSecondTime = true;
                        _viewCommandsPrinter.ShowFormScanLoadCarrier(_messageHolder, _selectedPrinter, _activeWorkListItem, _flowData.IsProcessStartedFromGoods);
                        ViewCommands.CurrentView.SetEventHandler(FormScanLoadCarrierEventHandler);
                        break;

                    case FlowStatesPrinter.ThisFlowComplete:
                        EndFlow();
                        break;

                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowPrintLoadList.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesPrinter.ThisFlowComplete);
            }
        }

        private void ExecuteActionState(FlowStatesPrinter state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowPrintLoadList;" + state);  
                    bool result;
                    switch (state)
                    {
                        //Action States for Unload list
                        case FlowStatesPrinter.ActionUnloadlistIsWorkListItemLoadCarrier:
                            result = _actionCommandsPrinter.IsWorklistItemLoadCarrier(_flowData.WorkListItem);
                            state = result
                                         ? FlowStatesPrinter.ThisFlowComplete
                                         : FlowStatesPrinter.ActionGetPrinters;
                            break;

                        case FlowStatesPrinter.ActionUnloadListAmphoraPrintRequest:
                             result=_actionCommandsPrinter.SendBcReport(_selectedPrinter, _flowData, _messageHolder);
                             state = result ? FlowStatesPrinter.ActionHasLoadCarrierEmailSet : FlowStatesPrinter.ViewShowPrintInformation;
                            break;

                        //Action States for IntoHoldingAtHub
                        case FlowStatesPrinter.ActionIntoHoldingAtHubSendRequesPrintBcReport:
                             result=_actionCommandsPrinter.SendBcReport(_selectedPrinter, _flowData, _messageHolder);
                             state = result ? FlowStatesPrinter.ActionHasLoadCarrierEmailSet : FlowStatesPrinter.ViewShowPrintInformation;
                            break;
                            

                        //Action Remaining Goods At hub 
                        case FlowStatesPrinter.ActionRemainingGoodsAtHubIsPrinterKnown:
                            result = _actionCommandsPrinter.IsPrinterSet(out _selectedPrinter);
                            state = result
                                         ? FlowStatesPrinter.ActionBcReport
                                         : FlowStatesPrinter.ActionGetPrinters;
                            break;

                        //Action States for Load line haul 

                        case FlowStatesPrinter.ActionloadlistIsPrinterKnown:
                            result = _actionCommandsPrinter.IsPrinterSet(out _selectedPrinter);
                            state = result
                                         ? FlowStatesPrinter.ActionLoadListIsCarrierLineHaul
                                         : FlowStatesPrinter.ActionGetPrinters;
                            break;

                        case FlowStatesPrinter.ActionLoadListIsCarrierLineHaul:
                            result = _actionCommandsPrinter.IsCarrierForLoadLineHaul(_flowData.WorkListItem);
                            state = result
                                         ? FlowStatesPrinter.ActionSetErrorMessageForLoadLineHaul
                                         : FlowStatesPrinter.ActionLoadIsWorkListIemLoadCarrier;
                            break;

                        case FlowStatesPrinter.ActionSetErrorMessageForLoadLineHaul:
                            _actionCommandsPrinter.ActionSetErrorMessageForLoadLineHaul(_messageHolder);
                            state = FlowStatesPrinter.ViewShowPrintInformation;
                            break;

                        case FlowStatesPrinter.ActionLoadIsWorkListIemLoadCarrier:
                            result = _actionCommandsPrinter.IsWorklistItemLoadCarrier(_flowData.WorkListItem);
                            state = result
                                        ? FlowStatesPrinter.ActionLoadListSendPrintCd
                                        : FlowStatesPrinter.ActioLoadListSendPrintBcReport;
                            break;

                        case FlowStatesPrinter.ActionLoadListSendPrintCd:
                            _actionCommandsPrinter.SendRequestForPrintCd(_messageHolder, _selectedPrinter, _activeWorkListItem);
                            state = FlowStatesPrinter.ActionHasLoadCarrierEmailSet;
                            break;

                        case FlowStatesPrinter.ActioLoadListSendPrintBcReport:
                            _actionCommandsPrinter.SendBcReport(_selectedPrinter, _flowData, _messageHolder);
                            state = FlowStatesPrinter.ActionHasLoadCarrierEmailSet;
                            break;

                        //Action states for building of load carrier

                        case FlowStatesPrinter.ActionBuildingLoadCarrierIsCarrierForLineHaul:
                            result = _actionCommandsPrinter.IsCarrierForLoadLineHaul(_flowData.WorkListItem);
                            state = result
                                         ? FlowStatesPrinter.ActionBuildingLoadCarrierSetErrorMessageLoadLineHaul
                                         : FlowStatesPrinter.ActionBuildingOfLoadCarrierIsPrinterKnown;
                            break;


                        case FlowStatesPrinter.ActionBuildingOfLoadCarrierIsEmailSet:
                            result = _actionCommandsPrinter.IsEmailSetForLoadCarrier(_flowData.WorkListItem);
                            state = result
                                         ? FlowStatesPrinter.ActionBuildingLoadCarrierPrintCdRequest
                                         : FlowStatesPrinter.ActionBuildingOfLoadCarrierIsPrinterKnown;
                            break;



                        case FlowStatesPrinter.ActionBuildingOfLoadCarrierIsPrinterKnown:
                            result = _actionCommandsPrinter.IsPrinterSet(out _selectedPrinter);
                            state = result
                                         ? FlowStatesPrinter.ActionBuildingLoadCarrierPrintCdRequest
                                         : FlowStatesPrinter.ActionGetPrinters;
                            break;


                        case FlowStatesPrinter.ActionBuildingLoadCarrierSetErrorMessageLoadLineHaul:
                            _actionCommandsPrinter.ActionSetErrorMessageForLoadLineHaul(_messageHolder);
                            state = FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                            break;

                        case FlowStatesPrinter.ActionBuildingLoadCarrierPrintCdRequest:
                            result = _actionCommandsPrinter.SendRequestForPrintCd(_messageHolder, _selectedPrinter, _activeWorkListItem);
                            state = result
                                        ? FlowStatesPrinter.ActionBuildingLoadCarrierIsEmailAddressSet
                                        : FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                            break;

                        case FlowStatesPrinter.ActionBuildingLoadCarrierIsEmailAddressSet:
                            _isMailAddressSet = _actionCommandsPrinter.HasLoadCarrierEmailSet(_activeWorkListItem, _messageHolder, _selectedPrinter);
                            state = FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                            break;

                        //Common States for printer
                        case FlowStatesPrinter.ActionIsStartedFromGoods:
                            result = _actionCommandsPrinter.ActionIsStartedFromGoods(_flowData);
                            state = result
                                        ? FlowStatesPrinter.ViewScanLogicalLoadCarrier
                                        : FlowStatesPrinter.ActionGetStateBasedonProcess;
                            break;

                        case FlowStatesPrinter.ActionGetStateBasedonProcess:
                            state = _actionCommandsPrinter.ActionGetStateBasedonProcess(_runningProcess);
                            break;

                        case FlowStatesPrinter.ActionGetPrinters:
                            _printers = _actionCommandsPrinter.GetPrintersList();
                            state = FlowStatesPrinter.ActionSetErrorMessageForNoPrinters;
                            break;

                        case FlowStatesPrinter.ActionSetErrorMessageForNoPrinters:
                            state = _actionCommandsPrinter.SetErrorMessageForNoPrinters(_flowData.IsProcessStartedFromReconcilition, _messageHolder, _printers);
                            break;

                        case FlowStatesPrinter.ActionSetPrinter:
                            _actionCommandsPrinter.SetDefaultPrinter(_selectedPrinter);
                            state = FlowStatesPrinter.ActionGetNextStateAfterDefaultPrinter;
                            break;

                        case FlowStatesPrinter.ActionHasLoadCarrierEmailSet:
                            _isMailAddressSet = _actionCommandsPrinter.HasLoadCarrierEmailSet(_flowData.WorkListItem, _messageHolder, _selectedPrinter);
                            state = FlowStatesPrinter.ViewShowPrintInformation;
                            break;


                        case FlowStatesPrinter.ActionGetNextStateAfterDefaultPrinter:
                            state = _actionCommandsPrinter.GetNextStateAfterDefaultPrinter(_runningProcess, _flowData,_activeWorkListItem);
                            break;

                        case FlowStatesPrinter.ActionGetTheLastScreenAfterCancelFromChangePrinter:
                            state = _actionCommandsPrinter.ActionGetTheLastScreenAfterCancelFromChangePrinter(_runningProcess, _flowData, _isSecondTime, _messageHolder);
                            break;

                        case FlowStatesPrinter.ActionBcReport:
                            result = _actionCommandsPrinter.SendBcReport(_selectedPrinter, _flowData, _messageHolder);
                            state = result ? FlowStatesPrinter.ActionHasLoadCarrierEmailSet : state = FlowStatesPrinter.ViewShowPrintInformation;
                            break;

                        case FlowStatesPrinter.ActionIsPrinterSet:
                            _actionCommandsPrinter.IsPrinterSet(out _selectedPrinter);
                            break;

                        case FlowStatesPrinter.ActionGetWorkListItem:
                            _actionCommandsPrinter.GetExistingWorkListItem(_loadCarrier, _messageHolder, ref _activeWorkListItem);
                            state = FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                            break;

                        case FlowStatesPrinter.ActionBackFromFlowScanBarcode:
                            state = _actionCommandsPrinter.ValidateFlowLoadCarrierScanResult((FlowResultScanBarcode)SubflowResult, out _currentScanning, ref _loadCarrier, _messageHolder, _flowData.IsProcessStartedFromGoods);
                            break;


                        case FlowStatesPrinter.ActionValidateLoadCarrier:
                            result = _actionCommandsPrinter.ValidateLoadCarrierId((LoadCarrier)_currentScanning, ref _messageHolder);
                            state = result ? FlowStatesPrinter.ActionCreateNewWorkListItem :
                                               FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                            break;

                        case FlowStatesPrinter.ActionCreateNewWorkListItem:
                            _activeWorkListItem = _actionCommandsPrinter.CreateWorkListItem(_currentScanning);
                            result = _actionCommandsPrinter.IsPrinterSet(out _selectedPrinter);
                            state = result
                                        ? FlowStatesPrinter.ActionBuildingLoadCarrierPrintCdRequest
                                        : FlowStatesPrinter.ActionGetPrinters;
                            break;

                        case FlowStatesPrinter.ActionCreateNewWorkListItemFromRbt:
                            _activeWorkListItem = _actionCommandsPrinter.CreateWorkListItemFromLoadCarrier(_loadCarrier);
                            state = FlowStatesPrinter.ActionSendBcReportForRbt;
                            break;

                        case FlowStatesPrinter.ActionSendBcReportForRbt:
                            _actionCommandsPrinter.SendBcReport(_selectedPrinter, _flowData, _messageHolder);
                            state = FlowStatesPrinter.ActionBuildingLoadCarrierIsEmailAddressSet;
                            break;


                    }
                } while (state < FlowStatesPrinter.ViewCommands);
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowPrintLoadList.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesPrinter.ThisFlowComplete);
            }
            ExecuteViewState(state);
        }

        public void FormDisplayPrintersEventHandler(int viewEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormDisplayPrintersEventHandler()");
            switch (viewEvent)
            {

                case FormEventDisplayPrinters.Ok:
                    _selectedPrinter = Convert.ToString(data[0]);
                    ExecuteState(FlowStatesPrinter.ActionSetPrinter);
                    break;

                case FormEventDisplayPrinters.Back:
                    ExecuteState(FlowStatesPrinter.ActionGetTheLastScreenAfterCancelFromChangePrinter);
                    break;
            }
        }

        public void FormPrintInformationEventHandler(int viewEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormPrintInformationEventHandler()");
            switch (viewEvent)
            {
                case FormPrintInformationEvent.Ok:
                    ExecuteState(FlowStatesPrinter.ThisFlowComplete);
                    break;

                case FormPrintInformationEvent.Back:
                    ExecuteState(FlowStatesPrinter.ThisFlowComplete);
                    break;

                case FormPrintInformationEvent.ChangePrinter:
                    ExecuteState(FlowStatesPrinter.ActionGetPrinters);
                    break;
            }
        }

        public void FormScanLoadCarrierEventHandler(int viewEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FormScanLoadCarrierEventHandler()");
            switch (viewEvent)
            {
                case FormScanLoadCarrierEvents.Finish:
                    ExecuteState(FlowStatesPrinter.ThisFlowComplete);
                    break;

                case FormScanLoadCarrierEvents.Ok:
                case FormScanLoadCarrierEvents.BarcodeScanned:
                    if (!_flowData.IsProcessStartedFromGoods)
                    {
                        GoToFlowScanBarcodeForLoadCarrier();
                    }
                    else
                    {
                        GoToFlowScanBarcodeForLoadCarrierOrRbt();
                    }
                    _flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
                    _flowScanBarcodeInstance.FotScannerOutput = (FotScannerOutput)data[0];
                    _flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
                    break;

                case FormScanLoadCarrierEvents.ChangeCarrier:
                    ExecuteState(FlowStatesPrinter.ActionGetPrinters);
                    break;

                case FormScanLoadCarrierEvents.Back:
                    ExecuteState(FlowStatesPrinter.ThisFlowComplete);
                    break;
            }
        }

        private void GoToFlowScanBarcodeForLoadCarrier()
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.LoadLineHaul, _messageHolder, BarcodeType.LoadCarrier, _entityMap)
            {
                HaveCustomForm = true,
                NoValidationOfLoadCarrierFromServer = false,
                PlaySuccesSound = false,
                WantLoadCarrierDetails=true
            };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)FlowStatesPrinter.ActionBackFromFlowScanBarcode, Process.Inherit);
        }

        private void GoToFlowScanBarcodeForLoadCarrierOrRbt()
        {
            var flowData = new FlowDataScanBarcode(GlobalTexts.LoadLineHaul, _messageHolder, BarcodeType.LoadCarrier | BarcodeType.Rbt, _entityMap)
            {
                HaveCustomForm = true,
                NoValidationOfLoadCarrierFromServer = true,
                PlaySuccesSound = false,
            };
            _flowScanBarcodeInstance = (FlowScanBarcode)StartSubFlow<FlowScanBarcode>(flowData, (int)FlowStatesPrinter.ActionBackFromFlowScanBarcode, Process.Inherit);
        }

        /// <summary>
        /// This method ends the Flow Print
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowPrintLoadList.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.VHCargoSign;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    class ActionCommandsPrinter
    {
        private readonly ICommunicationClient _communicationClient;
        public ActionCommandsPrinter(ICommunicationClient communicationClient)
        {
            _communicationClient = communicationClient;
        }

        public bool ActionIsStartedFromGoods(FlowDataPrintLoadList flowdata)
        {
            return flowdata.IsProcessStartedFromGoods;
        }

        /// <summary>
        /// This method will return the state based on from which process the flow is started
        /// </summary>
        /// <param name="curentProcess">Hold the process from which this flow starts  </param>
        /// <returns>Any state that would be execute next</returns>
        public FlowStatesPrinter ActionGetStateBasedonProcess(Process curentProcess)
        {
            var state = FlowStatesPrinter.ThisFlowComplete;

            switch (curentProcess)
            {
                case Process.LoadLineHaul:
                case Process.LoadDistribTruck:
                    state = FlowStatesPrinter.ActionloadlistIsPrinterKnown;
                    break;
                case Process.RemainingGoodsAtHub:
                    state = FlowStatesPrinter.ActionRemainingGoodsAtHubIsPrinterKnown;
                    break;
                case Process.BuildLoadCarrier:
                    state = FlowStatesPrinter.ActionBuildingOfLoadCarrierIsEmailSet;
                    break;

                case Process.IntoHoldingAtHub:
                    state = FlowStatesPrinter.ActionGetPrinters;
                    break;

                case Process.UnloadLineHaul:
                case Process.UnloadPickUpTruck:
                    state = FlowStatesPrinter.ActionUnloadlistIsWorkListItemLoadCarrier;
                    break;

            }
            return state;
        }


        /// <summary>
        /// Method will check is the work list item is of load carrier type
        /// </summary>
        /// <param name="workListItem"></param>
        /// <returns>True if work list item is of load carrier type </returns>
        public bool IsWorklistItemLoadCarrier(WorkListItem workListItem)
        {
            try
            {
                return workListItem != null && workListItem.Type == WorkListItemType.LoadCarrier;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.IsWorklistItemLoadCarrier");
                return false;
            }

        }


        /// <summary>
        /// Returns list of available printers
        /// </summary>
        /// <returns></returns>
        public List<EntityPrinter> GetPrintersList()
        {
            var printers = new List<EntityPrinter>();
            try
            {
                var printersList = GetPrinters(ModelUser.UserProfile.OrgUnitId);
                if (printersList != null)
                    printers.AddRange(from printer in printersList where printer != string.Empty select new EntityPrinter(printer));
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.IsPrinterKnown");
                return null;
            }
            return printers;
        }


        /// <summary>
        /// Method will Set the error message in case of no printers  
        /// </summary>

        public FlowStatesPrinter SetErrorMessageForNoPrinters(bool isProcessStartedFromReconcilition, MessageHolder messageHolder, List<EntityPrinter> printers)
        {
            var state = FlowStatesPrinter.ThisFlowComplete;
            try
            {
                if (printers == null || printers.Count < 1)
                {

                    messageHolder.Update(String.Format(GlobalTexts.NoPrinterFor, ModelUser.UserProfile.OrgUnitId), MessageState.Error);
                    state = isProcessStartedFromReconcilition ? FlowStatesPrinter.ViewShowPrintInformation : FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                }
                else
                {
                    state = FlowStatesPrinter.ViewShowPrinter;
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.ActionUpdateScreen");
                return state;
            }
            return state;
        }


        /// <summary>
        /// Method will Set Default Printer
        /// </summary>
        /// <param name="selectedPrinter">This string will have the name printer selected by driver or hubworker</param>
        /// <returns></returns>
        public void SetDefaultPrinter(String selectedPrinter)
        {
            try
            {
                if (ModelUser.UserProfile.RoleName != null)
                    switch (ModelUser.UserProfile.Role)
                    {
                        case Role.HubWorker1:
                            ModelUser.UserProfile.DefaultPrinter = selectedPrinter;
                            SettingsCommands.Instance.SaveUserProfile(ModelUser.UserProfile);
                            break;

                    }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.SetDefaultPrinter");
            }

        }


        //TODO :: Write Unit Test
        /// <summary>
        /// Method will VHWebServiceMethod method  printCd for print or mail  
        /// </summary>
        /// <returns></returns>
        public bool ActionAmphoraPrintRequest()
        {
            const bool isRequestSucessful = false;
            try
            {
                //TODO: SendRequest for Amphora Print
                return isRequestSucessful;

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.ActionAmphoraPrintRequest");
                return isRequestSucessful;
            }

        }


        //TODO :: Write Unit Test
        /// <summary>
        /// Method will check weather load carrier has mail set or not and return bool value accordingly
        /// </summary>
        /// <param name="workListItem"></param>
        /// <param name="messageHolder"></param>
        /// <param name="selectedPrinter"></param>
        /// <returns></returns>
        public bool HasLoadCarrierEmailSet(WorkListItem workListItem, MessageHolder messageHolder, string selectedPrinter)
        {
            bool isMailSet;
            try
            {
                isMailSet = workListItem != null && workListItem.LoadCarrierDetails != null && workListItem.LoadCarrierDetails.RecipientEmailAddress != null && !string.IsNullOrEmpty(workListItem.LoadCarrierDetails.RecipientEmailAddress);
                messageHolder.Update(isMailSet ? GlobalTexts.MailSucessFul : string.Format(GlobalTexts.PrintSucessful, selectedPrinter), MessageState.Information);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.HasLoadCarrierEmailSet");
                isMailSet = false;
            }
            return isMailSet;
        }



        public FlowStatesPrinter GetNextStateAfterDefaultPrinter(Process curentProcess, FlowDataPrintLoadList flowdata,
                                                                 WorkListItem currentWorkListItem)
        {
            var state = FlowStatesPrinter.ThisFlowComplete;

            if (flowdata != null && flowdata.IsProcessStartedFromGoods)
            {
                return currentWorkListItem != null ? FlowStatesPrinter.ActionBuildingLoadCarrierPrintCdRequest : FlowStatesPrinter.ViewScanLogicalLoadCarrier;
            }

            switch (curentProcess)
            {
                case Process.LoadLineHaul:
                case Process.LoadDistribTruck:
                    state = FlowStatesPrinter.ActionLoadListIsCarrierLineHaul;
                    break;

                case Process.BuildLoadCarrier:
                    state = FlowStatesPrinter.ActionBuildingLoadCarrierPrintCdRequest;
                    break;

                case Process.RemainingGoodsAtHub:
                    state = FlowStatesPrinter.ActionBcReport;
                    break;

                case Process.IntoHoldingAtHub:
                    state = FlowStatesPrinter.ActionIntoHoldingAtHubSendRequesPrintBcReport;
                    break;
                case Process.UnloadLineHaul:
                case Process.UnloadPickUpTruck:
                    state = FlowStatesPrinter.ActionUnloadListAmphoraPrintRequest;
                    break;

            }
            return state;
        }

        public FlowStatesPrinter ActionGetTheLastScreenAfterCancelFromChangePrinter(Process curentProcess, FlowDataPrintLoadList flowdata, bool isSecondTime, MessageHolder messageHolder)
        {
            var state = FlowStatesPrinter.ThisFlowComplete;
            if (isSecondTime)
            {

                state = flowdata != null && flowdata.IsProcessStartedFromGoods ||
                        curentProcess == Process.BuildLoadCarrier
                            ? FlowStatesPrinter.ViewScanLogicalLoadCarrier
                            : FlowStatesPrinter.ViewShowPrintInformation;
                messageHolder.Clear();
            }
            return state;
        }



        public string SetExtraParametersAccordingToProcess(Process curentProcess, FlowDataPrintLoadList flowData, ref FlowStatesPrinter state)
        {

            var extraParameters = string.Empty;
            if (flowData != null)
            {
                switch (curentProcess)
                {
                    case Process.LoadDistribTruck:
                        extraParameters = GetExtraParameterForLoadingDistributionTruck(flowData.WorkListItem);
                        break;

                    case Process.LoadLineHaul:
                        extraParameters = GetExtraParameterForOtherProcesses(flowData.WorkListItem);
                        break;

                    case Process.RemainingGoodsAtHub:
                        extraParameters = DateTime.Now.ToShortDateString() + "#" +
                                          DateTime.Now.AddHours(12).ToShortDateString() + "#" + flowData.PlacementId +
                                          "#";
                        state = FlowStatesPrinter.ActionIntoHoldingAtHubSendRequesPrintBcReport;
                        break;

                }
            }
            return extraParameters;
        }

        /// <summary>
        /// Method will VHWebServiceMethod method  printAmphora for print or mail  
        /// </summary>
        /// <returns></returns>
        public bool SendBcReport(string selectedPrinter, FlowDataPrintLoadList flowData, MessageHolder messageHolder)
        {
            try
            {
                var currenProcess = BaseModule.CurrentFlow.CurrentProcess;
                var reportNumber = GetReportNumber(currenProcess);
                var extraParames = GetExtraParameters(currenProcess, flowData);

                //Call to FOT server
                if (_communicationClient.IsConnected())
                {

                    var request = new VH_PrintBCReportRequest
                        {
                            locationNumber = ModelUser.UserProfile.OrgUnitId,
                            reportNumber = reportNumber,
                            reportOutputFormat = selectedPrinter,
                            extraParameters = extraParames
                        };

                    var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GetPrinterNames,
                                                      "PrintBcReport");
                    var reply = _communicationClient.Query<VH_PrintBCReportReply>(request, transaction);

                    if (reply == null || reply.PrintBCReportResult != "OK")
                    {
                        messageHolder.Update(GlobalTexts.ErrorConnectingPrinter, MessageState.Warning);
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    messageHolder.Update(GlobalTexts.OfflineOperationCanNotPerform, MessageState.Warning);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.SendRequestForPrintOrMailToAmphora");
                messageHolder.Update(ex.Message, MessageState.Warning);
                return false;
            }
            return false;
        }


        /// <summary>
        /// This method will check weather user has default printer or not
        /// </summary>
        /// <returns></returns>
        public bool IsPrinterSet(out string selectedPrinter)
        {
            selectedPrinter = string.Empty;
            bool isPrinterSet;
            try
            {
                isPrinterSet = !string.IsNullOrEmpty(ModelUser.UserProfile.DefaultPrinter);
                if (isPrinterSet) selectedPrinter = ModelUser.UserProfile.DefaultPrinter;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.IsPrinterSet");
                return false;
            }
            return isPrinterSet;
        }

        /// <summary>
        /// This method will check weather user has default printer or not
        /// </summary>
        /// <returns></returns>
        public bool IsEmailSetForLoadCarrier(WorkListItem workListItem)
        {
            var isEmailSet = false;
            try
            {
                if (workListItem != null && (workListItem.LoadCarrierDetails != null && workListItem.LoadCarrierDetails.RecipientEmailAddress != null))
                    isEmailSet = !string.IsNullOrEmpty(workListItem.LoadCarrierDetails.RecipientEmailAddress);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.IsEmailSetForLoadCarrier");
                return false;
            }
            return isEmailSet;
        }


        /// <summary>
        /// Method will Check weather Cariier Is for Load line haul
        /// </summary>
        /// <param name="worklistItem"></param>
        /// <returns></returns>
        public bool IsCarrierForLoadLineHaul(WorkListItem worklistItem)
        {
            var isLoadLineHaul = false;
            try
            {
                if (worklistItem.LoadCarrierDetails != null &&
                    worklistItem.LoadCarrierDetails.PostgangCode != null)
                    isLoadLineHaul = worklistItem.LoadCarrierDetails.PostgangCode ==
                                     LoadCarrierTypes.LineHaul;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.IsCarrierForLoadLineHaul");
                return false;
            }
            return isLoadLineHaul;
        }


        /// <summary>
        /// This method will return true if process has been started form Reconciltion otherwise false
        /// </summary>
        /// <param name="messageHolder"></param>
        /// <returns></returns>
        public void ActionSetErrorMessageForLoadLineHaul(MessageHolder messageHolder)
        {

            try
            {
                messageHolder.Update(GlobalTexts.PrintErrorLoadLineHaul, MessageState.Error);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.ActionShowErrorMessage");
            }
        }


        /// <summary>
        /// Method will VHWebServiceMethod method  printCd for print or mail  
        /// </summary>
        /// <returns></returns>
        public bool SendRequestForPrintCd(MessageHolder messageHolder, string selectedPrinter, WorkListItem worklistItem)
        {
            try
            {

                var printCdRequest = new VH_PrintCDRequest
                    {
                        locationNumber = ModelUser.UserProfile.OrgUnitId,
                        LBid = worklistItem.LoadCarrierId,
                        printerName = selectedPrinter,
                    };

                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GetPrinterNames, "PrintCDRequest");
                if (CommunicationClient.Instance.IsConnected())
                {
                    var reply = _communicationClient.Query<VH_PrintCDReply>(printCdRequest, transaction);
                    if (reply != null && reply.PrintCDResult == "OK")
                    {
                        return true;
                    }
                }
                else
                {
                    messageHolder.Update(GlobalTexts.OfflineCanNotSendRequest, MessageState.Warning);
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsPrinter.SendRequestForPrintCd");
                messageHolder.Update(GlobalTexts.PrintError, MessageState.Error);

            }
            return false;
        }




        //TODO :: Write Unit Test
        private string GetReportNumber(Process process)
        {
            string reportNumber = string.Empty;
            switch (process)
            {

                case Process.UnloadPickUpTruck:
                    reportNumber = "10";
                    break;


                case Process.LoadLineHaul:
                    reportNumber = "11";
                    break;

                case Process.UnloadLineHaul:
                    reportNumber = "12";
                    break;

                case Process.LoadDistribTruck:
                    reportNumber = "13";
                    break;


                case Process.IntoHoldingAtHub:
                    reportNumber = "15";
                    break;

                case Process.RemainingGoodsAtHub:
                    reportNumber = "16";
                    break;

            }
            return reportNumber;
        }
        //TODO :: Write Unit Test
        private string GetExtraParameters(Process process, FlowDataPrintLoadList flowData)
        {
            string extraParameters = string.Empty;
            if (flowData != null)
            {
                switch (process)
                {
                    case Process.LoadDistribTruck:
                        extraParameters = GetExtraParameterForLoadingDistributionTruck(flowData.WorkListItem);
                        break;

                    case Process.UnloadPickUpTruck:
                    case Process.UnloadLineHaul:
                    case Process.LoadLineHaul:
                        extraParameters = GetExtraParameterForOtherProcesses(flowData.WorkListItem);
                        break;

                    case Process.IntoHoldingAtHub:
                        extraParameters = ModelUser.UserProfile.OrgUnitId+"##########";
                        break;

                    case Process.RemainingGoodsAtHub:
                        var timeForFirstScan = ModelMain.SelectedOperationProcess.TimeForFirstScan;
                        extraParameters = ModelUser.UserProfile.OrgUnitId + "####" + timeForFirstScan.ToString("dd.MM.yyyy") + "#" +
                                          DateTime.Now.AddHours(12).ToString("dd.MM.yyyy") + "#" + timeForFirstScan.ToString("HH:mm:ss") + "#" + DateTime.Now.ToString("HH:mm:ss") + "#" + flowData.PlacementId +
                                          "#";
                        break;

                }
            }
            return extraParameters;
        }
        //TODO :: Write Unit Test
        private string GetExtraParameterForLoadingDistributionTruck(WorkListItem workListItem)
        {
            var extraParameterForLoadDistribution = ModelUser.UserProfile.OrgUnitId + "#" + workListItem.LoadCarrierId + "#"
                + workListItem.RouteId + "#" + workListItem.TripId + "#" + DateTime.Now.ToString("yyyy-MM-dd") + "#"
                                                       + DateTime.Now.AddHours(12).ToString("yyyy-MM-dd") + "#"
                                                       + ModelMain.SelectedOperationProcess.TimeForFirstScan.ToShortTimeString() + "#"
                                                       + DateTime.Now.AddHours(12).ToShortTimeString() + "#";
            return extraParameterForLoadDistribution;
        }

        //TODO :: Write Unit Test
        private string GetExtraParameterForOtherProcesses(WorkListItem workListItem)
        {
            var extraParameterForLoadLineHaul = string.Empty;
            if (workListItem != null)
            {
                extraParameterForLoadLineHaul = ModelUser.UserProfile.OrgUnitId + "#" + workListItem.LoadCarrierId + "#" +workListItem.RouteId + "#" + workListItem.TripId + "######";
            }
            return extraParameterForLoadLineHaul;
        }


        //TODO :: Write Unit Test

        /// <summary>
        /// method for get printers name...
        /// </summary>
        /// <param name="locationNumber"></param>
        /// <returns></returns>
        public string[] GetPrinters(string locationNumber)
        {
            string[] printers = null;
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsPrinter.GetPrinters()");

            if (_communicationClient.IsConnected())
            {

                var request = new GetPrintersRequest
                    {
                        locationNumber = locationNumber
                    };

                var reply = GetPrintersRequestResponse(request);
                if (reply != null && reply.PrintersForLocation != null)
                {
                    printers = reply.PrintersForLocation;
                }
            }
            return printers;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// methor for get printer request response from server....
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetPrintersReply GetPrintersRequestResponse(GetPrintersRequest request)
        {
            GetPrintersReply reply = null;
            try
            {
                var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GetPrinterNames, "PrintersName");
                reply = _communicationClient.Query<GetPrintersReply>(request, transaction);
            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsPrinter.GetPrintersRequestResponse");
            }
            return reply;
        }

        public void GetExistingWorkListItem(string loadCarrierId, MessageHolder messageHolder, ref WorkListItem activeWorkListItem)
        {
            bool result = false;

            if (!(loadCarrierId == null || ModelMain.WorkListItems == null))
            {
                foreach (var worklistitem in ModelMain.WorkListItems)
                {
                    if (worklistitem.LoadCarrierId == loadCarrierId)
                    {
                        if (activeWorkListItem.LoadCarrierId == loadCarrierId)
                        {
                            messageHolder.Update(string.Format(GlobalTexts.LoadCarrierAlreadyActive, loadCarrierId), MessageState.Warning);
                            SoundUtil.Instance.PlayWarningSound();
                        }
                        else
                        {
                            messageHolder.Update(string.Format(GlobalTexts.LoadCarrierActivated, loadCarrierId), MessageState.Information);
                            SoundUtil.Instance.PlaySuccessSound();
                            activeWorkListItem = worklistitem;
                        }
                        result = true;
                    }
                }
            }
            if (!result)
            {
                messageHolder.Update(string.Format(GlobalTexts.LoadCarrierNotActivated, loadCarrierId), MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
            }

        }

        internal FlowStatesPrinter ValidateFlowLoadCarrierScanResult(FlowResultScanBarcode subflowResult, out ConsignmentEntity consignmentEntity, ref string loadCarrierId, MessageHolder messageHolder, bool isStartedFromGoods)
        {
            var resultState = FlowStatesPrinter.ThisFlowComplete;
            consignmentEntity = subflowResult.CurrentConsignmentEntity;
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    if (subflowResult.BarcodeType == BarcodeType.Unknown)
                        resultState = FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                    else
                    {
                        switch (subflowResult.BarcodeType)
                        {
                            case BarcodeType.LoadCarrier:
                                if (isStartedFromGoods)
                                {
                                    resultState = FlowStatesPrinter.ActionValidateLoadCarrier;
                                }
                                else
                                {
                                    loadCarrierId = subflowResult.CurrentConsignmentEntity.LoadCarrierId;
                                    resultState = FlowStatesPrinter.ActionCreateNewWorkListItem;
                                }
                                break;

                            case BarcodeType.Rbt:
                                loadCarrierId = subflowResult.BarcodeRouteCarrierTrip.LoadCarrierId;
                                return FlowStatesPrinter.ActionCreateNewWorkListItemFromRbt;
                        }
                    }
                    break;

                case FlowResultState.Error:
                    resultState = FlowStatesPrinter.ViewScanLogicalLoadCarrier;
                    break;

                case FlowResultState.Cancel:
                    resultState = FlowStatesPrinter.ThisFlowComplete;
                    break;

                case FlowResultState.Finished:
                    break;

                default:
                    resultState = FlowStatesPrinter.ThisFlowComplete;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            return resultState;
        }


        internal bool ValidateLoadCarrierId(LoadCarrier loadCarrierEntity, ref MessageHolder messageHolder)
        {
            var result = ActionCommandsScanBarcode.ValidateLoadCarrier(loadCarrierEntity);

            if (result == false && loadCarrierEntity.IsScannedOnline)
            {
                messageHolder.Update(GlobalTexts.LoadCarrierNotValid, MessageState.Warning);
                SoundUtil.Instance.PlayWarningSound();
                return false;
            }

            messageHolder.Update(string.Format(GlobalTexts.LoadCarrierActivated, loadCarrierEntity.EntityDisplayId), MessageState.Information);
            SoundUtil.Instance.PlaySuccessSound();
            return true;
        }

        internal WorkListItem CreateWorkListItemFromLoadCarrier(string loadCarrierId)
        {

            var workListItem = new WorkListItem
            {
                LoadCarrierId = loadCarrierId,
                Type = WorkListItemType.LoadCarrier
            };

            return workListItem;
        }
        internal WorkListItem CreateWorkListItem(ConsignmentEntity consignmentEntity)
        {
            if (consignmentEntity != null)
            {
                var loadCarrierEntity =
                    (LoadCarrier)consignmentEntity;

                var workListItem = new WorkListItem
                {
                    LoadCarrierId = loadCarrierEntity.LoadCarrierId,
                    Type = WorkListItemType.LoadCarrier,
                    StopType = OperationType.BuildingOfLoadCarrier,
                    StopId = string.Empty,
                    TripId = string.Empty
                };

                if (loadCarrierEntity.LogicalLoadCarrierDetails != null)
                {
                    var loadCarrierDetails = loadCarrierEntity.LogicalLoadCarrierDetails;
                    if (loadCarrierDetails != null)
                        workListItem.LoadCarrierDetails = loadCarrierDetails[0];
                }
                return workListItem;
            }
            return null;
        }

    }

}

﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    partial class FormPrinterInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblPrinterNameValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDestValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblLoadCarierValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPrinterNameText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDestText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblLoadCarierText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblPrinterNameValue);
            this.touchPanel.Controls.Add(this.lblDestValue);
            this.touchPanel.Controls.Add(this.lblLoadCarierValue);
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.lblPrinterNameText);
            this.touchPanel.Controls.Add(this.lblDestText);
            this.touchPanel.Controls.Add(this.lblLoadCarierText);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 519, 480, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 0, 16, 16);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // lblPrinterNameValue
            // 
            this.lblPrinterNameValue.Location = new System.Drawing.Point(111, 260);
            this.lblPrinterNameValue.Name = "lblPrinterNameValue";
            this.lblPrinterNameValue.Size = new System.Drawing.Size(356, 40);
            // 
            // lblDestValue
            // 
            this.lblDestValue.Location = new System.Drawing.Point(111, 230);
            this.lblDestValue.Name = "lblDestValue";
            this.lblDestValue.Size = new System.Drawing.Size(366, 40);
            // 
            // lblLoadCarierValue
            // 
            this.lblLoadCarierValue.Location = new System.Drawing.Point(111,76);
            this.lblLoadCarierValue.Name = "lblLoadCarierValue";
            this.lblLoadCarierValue.Size = new System.Drawing.Size(366, 40);
            // 
            // lblPrinterNameText
            // 
            this.lblPrinterNameText.Location = new System.Drawing.Point(21, 260);
            this.lblPrinterNameText.Name = "lblPrinterNameText";
            this.lblPrinterNameText.Size = new System.Drawing.Size(90, 42);
            // 
            // lblDestText
            // 
            this.lblDestText.Location = new System.Drawing.Point(21, 230);
            this.lblDestText.Name = "lblDestText";
            this.lblDestText.Size = new System.Drawing.Size(90, 40);
            // 
            // lblLoadCarierText
            // 
            this.lblLoadCarierText.Location = new System.Drawing.Point(21, 76);
            this.lblLoadCarierText.Name = "lblLoadCarierText";
            this.lblLoadCarierText.Size = new System.Drawing.Size(90, 40);
            
            // 
            // lblHeading
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 42);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // 
            // MsgMessage
            // 
            this.messageControl.Location = new System.Drawing.Point(21, 110);
            this.messageControl.MessageText = "";
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438, 110);
            this.messageControl.TabIndex = 14;
            // 
            // FormPrinterInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormPrinterInformation";
            this.Text = "FormPrinterInformation";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);
        }

        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblDestText;
        private Resco.Controls.CommonControls.TransparentLabel lblLoadCarierText;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TransparentLabel lblPrinterNameText;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private Resco.Controls.CommonControls.TransparentLabel lblPrinterNameValue;
        private Resco.Controls.CommonControls.TransparentLabel lblDestValue;
        private Resco.Controls.CommonControls.TransparentLabel lblLoadCarierValue;
    }
}
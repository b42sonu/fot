﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    internal class ViewCommandsPrinter
    {
        /// <summary>
        /// Open view to select the printers available for terminal
        /// </summary>
        internal void ShowPrinterListForm(List<EntityPrinter> listPrinters, string defaultPrinter)
        {
            try
            {
                ViewCommands.ShowView<FormDisplayPrinters>(new FormDataDisplayPrinters
                    {
                        PrinterList = listPrinters,
                        DefaultPrinter = defaultPrinter
                    });

                Logger.LogEvent(Severity.Debug, "Command ViewCommandsPrint.ShowPrinterListForm() finished");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsPrint.ShowPrinterListForm()");
            }
        }

        /// <summary>
        /// Open view to see the print information
        /// </summary>
        internal void ShowFormPrintInformation(MessageHolder messageHolder, string selectedPrinter, WorkListItem workListItem, bool isEmailAddressSet)
        {
            try
            {
                var formDataPrinterInformation = new FormDataPrinterInfromation
                  {
                      MessageHolder = messageHolder,
                      WorkListItem = workListItem,
                      SelectedPrinter = selectedPrinter,
                      IsEmailAddressSet = isEmailAddressSet
                  };
                ViewCommands.ShowView<FormPrinterInformation>(formDataPrinterInformation);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsPrint.ShowFormPrintInformation()");
            }
        }

        /// <summary>
        /// Open view to scan load carrier 
        /// </summary>
        internal void ShowFormScanLoadCarrier(MessageHolder messageHolder, string selectedPrinter, WorkListItem workListItem, bool isStartedFromGoods)
        {
            try
            {
                var formDataScanLogicaLoadCarrier = new FormDataScanLogicaLoadCarrier
                  {
                      MessageHolder = messageHolder,
                      WorkListItem = workListItem,
                      SelectedPrinter = selectedPrinter,
                      StartedFromGoods = isStartedFromGoods
                  };
                ViewCommands.ShowView<FormScanLoadCarrier>(formDataScanLogicaLoadCarrier);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ViewCommandsPrint.ShowFormPrintInformation()");
            }
        }
    }
}
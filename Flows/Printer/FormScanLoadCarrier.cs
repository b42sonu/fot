﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

public class FormScanLoadCarrierEvents : ViewEvent
{
    public const int Ok = 0;
    public const int BarcodeScanned = 1;
    public const int Back = 2;
    public const int ChangeCarrier = 3;
    public const int Finish = 4;

}

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    public partial class FormScanLoadCarrier : BaseBarcodeScanForm
    {
        private FormDataScanLogicaLoadCarrier _formData;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();
        private const string ButtonOk = "buttonOk";
        private const string ButtonChangePrinter = "ChangePrinter";
        private const string ButtonBack = "buttonBack";
        private string _scantext;


        public FormScanLoadCarrier()
        {
            InitializeComponent();
            SetStandardControlProperties(lblModuleName, lblOrgnaisationUnitName, null, null, null, null, null);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            InitOnScreenKeyBoardProperties();
            SetTextToControls();
        }

        private void SetTextToControls()
        {
            lblActivateCarier.Text = GlobalTexts.ActivateCarierForPrinting;

            lblPrinterNameText.Text = GlobalTexts.Printer + Colon;
            lblDestText.Text = GlobalTexts.Dest + Colon;
            lblLoadCarierText.Text = GlobalTexts.LoadCarrierAbbrevation + Colon;
            lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblModuleName.Text = GlobalTexts.PrintVoucher;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick) { Name = ButtonBack });
            _multiButtonControl.ListButtons.Add(new TabButton(String.Format(GlobalTexts.ChangePrinter, Environment.NewLine), ButtonChangePrinterClick, ButtonChangePrinter));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
        }

        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
                {
                    TextChanged = ScannedNumberTextChanged,
                    Title = _scantext
                };
            preComInputScanInput.Tag = keyBoardProperty;
        }


        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            _multiButtonControl.SetButtonEnabledState(ButtonOk, preComInputScanInput.Text != string.Empty || _formData.WorkListItem != null);

            if (messageBox != null)
                messageBox.ClearMessage();
            if (userFinishedTyping && preComInputScanInput.Text != string.Empty)
                BarcodeEnteredManually(textentered);
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            preComInputScanInput.Text = "";
            ViewEvent.Invoke(FormScanLoadCarrierEvents.BarcodeScanned, scannerOutput);
        }

        private void BarcodeEnteredManually(string textentered)
        {
            var scannerOutput = new FotScannerOutput { Code = textentered };
            ViewEvent.Invoke(FormScanLoadCarrierEvents.BarcodeScanned, scannerOutput);
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            ClaerLabels();

            if (formData != null)
            {
                _formData = (FormDataScanLogicaLoadCarrier)formData;
                var validTypes = _formData.StartedFromGoods ? Barcode.BarcodeType.Rbt | Barcode.BarcodeType.LoadCarrier : Barcode.BarcodeType.LoadCarrier;
                _scantext = BaseActionCommands.ScanTextFromValidBarcodeTypes(validTypes, GlobalTexts.ScanOrEnterBarcode);
                lblScan.Text = _scantext + Colon;
                InitOnScreenKeyBoardProperties();
                if (_formData.WorkListItem != null)
                {
                    labelEmailStatus.Text = _formData.WorkListItem.LoadCarrierDetails != null && (_formData.WorkListItem.LoadCarrierDetails.RecipientEmailAddress != null && !string.IsNullOrEmpty(
                                                                                                                                                                                  _formData.WorkListItem.LoadCarrierDetails.RecipientEmailAddress))
                                                ? GlobalTexts.EmailIsKnown
                                                : GlobalTexts.EmailIsNotKnown;
                    //labelEmailStatus.Text = _formData.WorkListItem.LoadCarrierDetails != null && (_formData.WorkListItem.LoadCarrierDetails.RecipientEmailAddress != null && string.IsNullOrEmpty(
                    //   _formData.WorkListItem.LoadCarrierDetails.RecipientEmailAddress)) ? "e-mail is not known" : "e-mail is known";

                    if (_formData.WorkListItem.LoadCarrierId != null)
                        lblLoadCarierText.Text = GlobalTexts.LoadCarrierAbbrevation + Colon + _formData.WorkListItem.LoadCarrierId;

                    if (_formData.WorkListItem.LoadCarrierDetails != null)
                           lblDestText.Text = GlobalTexts.Dest + Colon + _formData.WorkListItem.LoadCarrierDetails.DestinationName1;
                    _multiButtonControl.SetButtonEnabledState(ButtonBack, true);
                }
                _multiButtonControl.SetButtonEnabledState(ButtonOk, preComInputScanInput.Text != string.Empty || _formData.WorkListItem != null);
            }

            lblPrinterNameValue.Text = _formData.SelectedPrinter ?? ModelUser.UserProfile.DefaultPrinter;


            if (_formData.MessageHolder != null)
            {
                if (_formData.MessageHolder != null && _formData.MessageHolder.State != MessageState.Error)
                    preComInputScanInput.Text = string.Empty;
                messageBox.ShowMessage(_formData.MessageHolder);
            }
            SetKeyboardState(KeyboardState.Numeric);
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(preComInputScanInput.Text))
                    ViewEvent.Invoke(FormScanLoadCarrierEvents.Finish);
                else
                    BarcodeEnteredManually(preComInputScanInput.Text.Trim());
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanLaodCarrier.ButtonOkClick");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormScanLoadCarrierEvents.Finish);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanLaodCarrier.ButtonOkClick");
            }
        }

        private void ButtonChangePrinterClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormScanLoadCarrierEvents.ChangeCarrier);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormScanLaodCarrier.ButtonChangeCarrierClick");
            }
        }

        private void ClaerLabels()
        {

            lblLoadCarierValue.Text = string.Empty;
            lblDestValue.Text = string.Empty;
            labelEmailStatus.Text = string.Empty;
        }

    }
}
public class FormDataScanLogicaLoadCarrier : BaseFormData
{
    public WorkListItem WorkListItem { get; set; }
    public MessageHolder MessageHolder { get; set; }
    public string SelectedPrinter { get; set; }
    public bool StartedFromGoods { get; set; }
}
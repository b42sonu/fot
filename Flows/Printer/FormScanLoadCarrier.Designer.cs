﻿namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    partial class FormScanLoadCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.lblActivateCarier = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPrinterNameValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDestValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.preComInputScanInput = new PreCom.Controls.PreComInput2();
            this.lblScan = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblLoadCarierValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageBox = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblPrinterNameText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblDestText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblLoadCarierText = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelEmailStatus = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelEmailStatus)).BeginInit();
        
            this.SuspendLayout();
            // 
            // lblActivateCarier
            // 
            this.lblActivateCarier.Location = new System.Drawing.Point(21, 107);
            this.lblActivateCarier.Name = "lblActivateCarier";
            this.lblActivateCarier.Size = new System.Drawing.Size(452, 40);
            // 
            // lblPrinterNameValue
            // 
            this.lblPrinterNameValue.Location = new System.Drawing.Point(111, 414);
            this.lblPrinterNameValue.Name = "lblPrinterNameValue";
            this.lblPrinterNameValue.Size = new System.Drawing.Size(356, 40);
            // 
            // lblDestValue
            // 
            this.lblDestValue.Location = new System.Drawing.Point(111, 379);
            this.lblDestValue.Name = "lblDestValue";
            this.lblDestValue.Size = new System.Drawing.Size(366, 40);
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.preComInputScanInput);
            this.touchPanel.Controls.Add(this.lblScan);
            this.touchPanel.Controls.Add(this.lblActivateCarier);
            this.touchPanel.Controls.Add(this.lblPrinterNameValue);
            this.touchPanel.Controls.Add(this.lblDestValue);
            this.touchPanel.Controls.Add(this.lblLoadCarierValue);
            this.touchPanel.Controls.Add(this.messageBox);
            this.touchPanel.Controls.Add(this.lblPrinterNameText);
            this.touchPanel.Controls.Add(this.lblDestText);
            this.touchPanel.Controls.Add(this.lblLoadCarierText);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.labelEmailStatus);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // preComInputScanInput
            // 
            this.preComInputScanInput.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.preComInputScanInput.Location = new System.Drawing.Point(21, 193);
            this.preComInputScanInput.Name = "preComInputScanInput";
            this.preComInputScanInput.Size = new System.Drawing.Size(436, 48);
            this.preComInputScanInput.TabIndex = 24;
            this.preComInputScanInput.TextTranslation = false;
            // 
            // lblScan
            // 
            this.lblScan.Location = new System.Drawing.Point(21, 150);
            this.lblScan.Name = "lblScan";
            this.lblScan.Size = new System.Drawing.Size(452, 40);
            // 
            // lblLoadCarierValue
            // 
            this.lblLoadCarierValue.Location = new System.Drawing.Point(111, 344);
            this.lblLoadCarierValue.Name = "lblLoadCarierValue";
            this.lblLoadCarierValue.Size = new System.Drawing.Size(366, 40);
            // 
            // MsgMessage
            // 
            this.messageBox.Location = new System.Drawing.Point(21, 244);
            this.messageBox.MessageText = "";
            this.messageBox.Name = "messageBox";
            this.messageBox.Size = new System.Drawing.Size(438, 97);
            this.messageBox.TabIndex = 14;
            // 
            // lblPrinterNameText
            // 
            this.lblPrinterNameText.Location = new System.Drawing.Point(21, 414);
            this.lblPrinterNameText.Name = "lblPrinterNameText";
            this.lblPrinterNameText.Size = new System.Drawing.Size(90, 42);
            // 
            // lblDestText
            // 
            this.lblDestText.Location = new System.Drawing.Point(21, 379);
            this.lblDestText.Name = "lblDestText";
            this.lblDestText.Size = new System.Drawing.Size(90, 40);
            // 
            // lblLoadCarierText
            // 
            this.lblLoadCarierText.Location = new System.Drawing.Point(21, 344);
            this.lblLoadCarierText.Name = "lblLoadCarierText";
            this.lblLoadCarierText.Size = new System.Drawing.Size(90, 40);
         
            // 
            // lblHeading
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 42);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblModuleName
            // 
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(142, 3);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(217, 50);
            // 
            // labelModuleName
            // 
            this.labelEmailStatus.AutoSize = false;
            this.labelEmailStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.labelEmailStatus.Location = new System.Drawing.Point(21, 449);
            this.labelEmailStatus.Name = "labelModuleName";
            this.labelEmailStatus.Size = new System.Drawing.Size(200, 35);
            this.labelEmailStatus.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleLeft;
            // 
            // FormScanLoadCarier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormScanLoadCarrier";
            this.Text = "FormScanLoadCarrier";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelEmailStatus)).EndInit();
            this.ResumeLayout(false);

        }

        private Resco.Controls.CommonControls.TransparentLabel lblActivateCarier;
        private Resco.Controls.CommonControls.TransparentLabel lblPrinterNameValue;
        private Resco.Controls.CommonControls.TransparentLabel lblDestValue;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private PreCom.Controls.PreComInput2 preComInputScanInput;
        private Resco.Controls.CommonControls.TransparentLabel lblScan;
        private Resco.Controls.CommonControls.TransparentLabel lblLoadCarierValue;
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageBox;
        private Resco.Controls.CommonControls.TransparentLabel lblPrinterNameText;
        private Resco.Controls.CommonControls.TransparentLabel lblDestText;
        private Resco.Controls.CommonControls.TransparentLabel lblLoadCarierText;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelEmailStatus;
    }
}
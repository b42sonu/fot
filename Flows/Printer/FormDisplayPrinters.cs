﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    public class FormEventDisplayPrinters : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }
    public partial class FormDisplayPrinters : BaseForm
    {
        private FormDataDisplayPrinters _formDataPrinterList;
        private const string ButtonOk = "buttonOk";
        private const string ButtonBack = "ButtonBack";
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();

        public FormDisplayPrinters()
        {
            InitializeComponent();
            SetStandardControlProperties(labelModuleName,lblHeading,null,null,null,null,null);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToGui();
        }

        //Set texts 
        private void SetTextToGui()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormEventDisplayPrinters.SetTextToGui()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                touchPanel.Controls.Add(_multiButtonControl);
                _multiButtonControl.GenerateButtons();
                labelModuleName.Text = GlobalTexts.PrintVoucher;
                lblSelectPrinter.Text = GlobalTexts.SelectPrinterFromlist;
                lblHeading.Text = ModelUser.UserProfile.OrgUnitName;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayPrinters.SetTextToGui");
            }
        }

        public override void OnShow(IFormData formData)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormEventDisplayPrinters.OnShow()");
                messageControl.ClearMessage();
                listBoxPrinterList.DataSource = null;
                listBoxPrinterList.DisplayMember = "Name";
                if (formData != null)
                {
                    _formDataPrinterList = (FormDataDisplayPrinters)formData;
                    if (_formDataPrinterList.PrinterList != null)
                    {
                        listBoxPrinterList.DataSource = _formDataPrinterList.PrinterList;
                        foreach (var selectedPrinter in listBoxPrinterList.Items)
                        {
                            if (_formDataPrinterList.DefaultPrinter != null && _formDataPrinterList.DefaultPrinter.ToLower() == ((EntityPrinter)(selectedPrinter)).Name.ToLower())
                            {
                                listBoxPrinterList.SelectedItem = selectedPrinter;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayPrinters.OnShow");
            }
        }


        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormEventDisplayPrinters.ButtonOkClick()");
                ViewEvent.Invoke(FormEventDisplayPrinters.Ok, ((EntityPrinter)listBoxPrinterList.SelectedItem).Name);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayPrinters.ButtonOkClick()");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormDisplayPrinters.ButtonBackClick()");
                ViewEvent.Invoke(FormEventDisplayPrinters.Back, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayPrinters.ButtonBackClick");
            }
        }
    }
}
public class FormDataDisplayPrinters : BaseFormData
{
    public List<EntityPrinter> PrinterList { get; set; }
    public string DefaultPrinter { get; set; }
}
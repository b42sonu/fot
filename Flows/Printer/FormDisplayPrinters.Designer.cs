﻿using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    partial class FormDisplayPrinters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblConsignmentHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.listBoxPrinterList = new System.Windows.Forms.ListBox();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblSelectPrinter = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSelectPrinter)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblSelectPrinter);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.messageControl);
            this.touchPanel.Controls.Add(this.listBoxPrinterList);
            this.touchPanel.Controls.Add(this.lblConsignmentHeading);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.HScrollBarProperties.Bounds = new System.Drawing.Rectangle(0, 184, 184, 16);
            this.touchPanel.HScrollBarProperties.LargeChange = 1;
            this.touchPanel.HScrollBarProperties.Maximum = 0;
            this.touchPanel.HScrollBarProperties.Minimum = 0;
            this.touchPanel.HScrollBarProperties.Value = 0;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            this.touchPanel.VScrollBarProperties.Bounds = new System.Drawing.Rectangle(184, 0, 16, 184);
            this.touchPanel.VScrollBarProperties.LargeChange = 1;
            this.touchPanel.VScrollBarProperties.Maximum = 0;
            this.touchPanel.VScrollBarProperties.Minimum = 0;
            this.touchPanel.VScrollBarProperties.Value = 0;
            // 
            // lblConsignmentHeading
            // 
            this.lblConsignmentHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.lblConsignmentHeading.Location = new System.Drawing.Point(2, 5);
            this.lblConsignmentHeading.Name = "lblConsignmentHeading";
            this.lblConsignmentHeading.Size = new System.Drawing.Size(0, 0);
            // 
            // listBoxPrinterList
            // 
            this.listBoxPrinterList.Location = new System.Drawing.Point(21, 130);
            this.listBoxPrinterList.Name = "listBoxPrinterList";
            this.listBoxPrinterList.Size = new System.Drawing.Size(438, 250);
            this.listBoxPrinterList.TabIndex = 3;
            this.listBoxPrinterList.DisplayMember = "Name";
            //
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(21, 316);
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(438, 110);
            this.messageControl.TabIndex = 56;
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(135, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // lblSelectPrinter
            // 
            this.lblSelectPrinter.Location = new System.Drawing.Point(21, 90);
            this.lblSelectPrinter.Name = "lblSelectPrinter";
            this.lblSelectPrinter.Size = new System.Drawing.Size(24, 15);
            this.lblSelectPrinter.Text = "Text";
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = false;
            this.lblHeading.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblHeading.Location = new System.Drawing.Point(3, 42);
            this.lblHeading.Name = "lblOrgnaisationUnitName";
            this.lblHeading.Size = new System.Drawing.Size(477, 35);
            this.lblHeading.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            
            // 
            // FormDisplayPrinterList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Name = "FormDisplayPrinters";
            this.Text = "FormDisplayPrinters";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSelectPrinter)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentHeading;
        private System.Windows.Forms.ListBox listBoxPrinterList;
        private MessageControl messageControl;
        private Resco.Controls.CommonControls.TransparentLabel lblSelectPrinter;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
    }
}
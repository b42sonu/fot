﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Printer
{
    public class FormPrintInformationEvent : ViewEvent
    {
        public const int Ok = 0;
        public const int ChangePrinter = 1;
        public const int Back = 2;

    }

    public partial class FormPrinterInformation : BaseForm
    {
        private FormDataPrinterInfromation _formData;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();
        private const string BouutonOk = "buttonOk";
        private const string ButtonChangePrinter = "ChangePrinter";
        private const string ButtonBack = "ButtonBack";
        public FormPrinterInformation()
        {
            InitializeComponent();
            SetStandardControlProperties(labelModuleName, lblOrgnaisationUnitName, null, null, null, null, null);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
        }

        private void SetTextToControls()
        {
            labelModuleName.Text = GlobalTexts.PrintVoucher;
            lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            lblLoadCarierText.Text = GlobalTexts.LoadCarrierAbbrevation + ":";
            lblDestText.Text = GlobalTexts.Dest + ":";
            lblPrinterNameText.Text = GlobalTexts.Printer + ":";
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, ButtonBack));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.ChangePrinter, ButtonChangePrinterClick, ButtonChangePrinter));
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = BouutonOk });
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();

        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            if (formData != null)
            {
                _formData = (FormDataPrinterInfromation)formData;
                if (_formData.WorkListItem != null && _formData.WorkListItem.LoadCarrierId != null)
                    lblLoadCarierText.Text = GlobalTexts.LoadCarrierAbbrevation + ":" + _formData.WorkListItem.LoadCarrierId;
                // lblLoadCarierValue.Text = _formData.WorkListItem.LoadCarrierId;
                if (_formData.SelectedPrinter != null)
                    lblPrinterNameText.Text = GlobalTexts.Printer + ":" + _formData.SelectedPrinter;
                if (_formData.WorkListItem != null && (_formData.WorkListItem.LoadCarrierDetails != null && _formData.WorkListItem.LoadCarrierDetails.DestinationName1 != null))
                    lblDestText.Text = GlobalTexts.Dest + ":" + _formData.WorkListItem.LoadCarrierDetails.DestinationName1;
                // lblDestValue.Text = _formData.WorkListItem.LoadCarrierDetails.DestinationName1;
            }
            if (_formData.MessageHolder == null) return;
            messageControl.ShowMessage(_formData.MessageHolder);
            EnableDiableButtons(_formData.MessageHolder.State != MessageState.Error);
        }

        private void EnableDiableButtons(bool value)
        {
            if (value)
            {
                _multiButtonControl.SetButtonEnabledState(ButtonBack, false);
                _multiButtonControl.SetButtonEnabledState(BouutonOk, true);
                _multiButtonControl.SetButtonEnabledState(ButtonChangePrinter, true);
            }
            else
            {
                _multiButtonControl.SetButtonEnabledState(BouutonOk, false);
                _multiButtonControl.SetButtonEnabledState(ButtonChangePrinter, false);
                _multiButtonControl.SetButtonEnabledState(ButtonBack, true);
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormPrintInformationEvent.Ok);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPrintInformationEvent.ButtonOkClick");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormPrintInformationEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPrintInformationEvent.ButtonOkClick");
            }
        }



        private void ButtonChangePrinterClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormPrintInformationEvent.ChangePrinter);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormPrintInformationEvent.ButtonOkClick");
            }
        }
    }
}

public class FormDataPrinterInfromation : BaseFormData
{
    public WorkListItem WorkListItem { get; set; }
    public MessageHolder MessageHolder { get; set; }
    public string SelectedPrinter { get; set; }
    public bool IsEmailAddressSet { get; set; }
}
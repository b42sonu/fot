﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.MessageHandling
{
    partial class FormNewMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.pnlNewMessage = new System.Windows.Forms.Panel();
            this.txtNewMessage = new PreCom.Controls.PreComInput2();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).BeginInit();
            this.pnlNewMessage.SuspendLayout();
            this.SuspendLayout();
            
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblModuleName);
            this.touchPanel.Controls.Add(this.pnlNewMessage);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            // 
            // lblModuleName
            // 
            this.lblModuleName.AutoSize = false;
            this.lblModuleName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblModuleName.Location = new System.Drawing.Point(2, 2);
            this.lblModuleName.Name = "lblModuleName";
            this.lblModuleName.Size = new System.Drawing.Size(477, 33);
            this.lblModuleName.Text = "Messages";
            this.lblModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            
            // 
            // pnlNewMessage
            // 
            this.pnlNewMessage.Controls.Add(this.txtNewMessage);
            this.pnlNewMessage.Location = new System.Drawing.Point(0, 47);
            this.pnlNewMessage.Name = "pnlNewMessage";
            this.pnlNewMessage.Size = new System.Drawing.Size(480, 307);
            // 
            // txtNewMessage
            // 
            this.txtNewMessage.Location = new System.Drawing.Point(0, 0);
            this.txtNewMessage.MaxLength = 100;
            this.txtNewMessage.Multiline = true;
            this.txtNewMessage.Name = "txtNewMessage";
            this.txtNewMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNewMessage.Size = new System.Drawing.Size(480, 307);
            this.txtNewMessage.TabIndex = 28;
            this.txtNewMessage.TextTranslation = false;

          
            // 
            // FormMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormMessages";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblModuleName)).EndInit();
            this.pnlNewMessage.ResumeLayout(false);
            this.ResumeLayout(false);

        }



        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel lblModuleName;
        private Panel pnlNewMessage;
        private PreCom.Controls.PreComInput2 txtNewMessage;



    }
}
﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.MessageHandling
{
    class ViewCommandsMessageHandling
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageViewEventHandler">Handler for message view </param>
        /// <param name="messagesToDriver"> List of all the messages</param>
        /// <param name="currentView"> Current selected tab and view</param>
        /// <param name="unreadMessageCount"> Number of unread message</param>
        /// <param name="selectedMessage"> currently selected message</param>
        /// <param name="calledFromDelivery">Called from </param>
        /// <param name="orderNumber"></param>
        /// <param name="inboxView"></param>
        public void ViewMessages(ViewEventDelegate messageViewEventHandler, List<MessageToDriver> messagesToDriver, MessageView currentView, int unreadMessageCount, 
            MessageToDriver selectedMessage, bool calledFromDelivery, string orderNumber, bool inboxView)
        {
            var formData = new FormDataMessages
            {
                ListMessages = messagesToDriver,
                CurrentView = currentView,
                UnreadMessageCount = unreadMessageCount,
                MessageToDriver = selectedMessage,
                CalledFromStopDetails = calledFromDelivery,
                OrderNumber = orderNumber,
                InboxView = inboxView
            };
            var view = ViewCommands.ShowView<FormMessages>(formData);
            view.SetEventHandler(messageViewEventHandler);
        }

        
        public void ViewNewMessages(ViewEventDelegate newMessageViewHandler, MessageView currentView, MessageToDriver selectedMessageToDriver)
        {
            var formData = new FormDataNewMessage()
                               {
                                   CurrentView = currentView,
                                   MessageToDriver = selectedMessageToDriver,
                               };
            var view = ViewCommands.ShowView<FormNewMessage>(formData);
            view.SetEventHandler(newMessageViewHandler);
        }
    }
}

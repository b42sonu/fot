﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using CommunicationEntity.Messaging;
using PreCom;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.MessageHandling
{
    class ActionCommandsMessageHandling
    {
        /// <summary>
        /// Get all messages from local database
        /// </summary>
        internal List<MessageToDriver> GetAllMessages(string userId)
        {
            return SettingDataProvider.Instance.GetMessages(userId);
        }

        /// <summary>
        /// Send Message from Driver to Server and also save in local database
        /// </summary>
        internal void SendAndSaveMessageFromDriver(string messageText, string correlationId)
        {
            var message = new MessageFromDriver
            {
                MessageTime = DateTime.Now,
                MessageId = Guid.NewGuid().ToString(),
                SessionId = ModelUser.SessionId,
                UnitId = ModelUser.UserProfile.TmsAffiliation == TmsAffiliation.Alystra
                        ? ModelUser.UserProfile.UserId : ModelUser.UserProfile.PowerUnit,
                CaptureEquipmentId = Application.Instance.Platform.Hardware.Tag,
                UserLogonId = ModelUser.UserProfile.UserId,
                CorrelationId = correlationId,
                MessageText = messageText
            };
            
            CommunicationClient.Instance.SendMessageFromDriver(message);

            SettingDataProvider.Instance.SaveSentMessage(message, MessageStatusCode.Sent);
        }

        /// <summary>
        /// Updates the Status of Message
        /// </summary>
        internal bool UpdateMessageStatusIfNew(MessageToDriver currentMessageToDriver, string messageStatus)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsMessageHandling.UpdateMessageStatusIfNew()");
            try
            {
                if ((currentMessageToDriver == null) || (String.IsNullOrEmpty(messageStatus)))
                {
                    Logger.LogEvent(Severity.Info, "Invalid inputs to update messageStatus =" + messageStatus);
                    return false;
                }
                Logger.LogEvent(Severity.Info, "CurrentMessageId =" + currentMessageToDriver.MessageId + ",MessageStatus " + messageStatus);
                if (currentMessageToDriver.Status == MessageStatusCode.Unread)
                {
                    currentMessageToDriver.Status = MessageStatusCode.Read;
                    return SettingDataProvider.Instance.UpdateMessage(currentMessageToDriver.MessageId, messageStatus);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "Exception in ActionCommandsMessageHandling.UpdateMessageStatusIfNew() " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Permanently remove messages marked as deleted from PDA
        /// </summary>
        internal void DeleteAllPermanently()
        {
            SettingDataProvider.Instance.DeleteAllMessages();
        }

        /// <summary>
        /// Set the messageTxt status to deleted 
        /// </summary>
        internal void DeleteMessage(MessageToDriver currentMessageToDriver)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsMessageHandling.DeleteMessage()");
            try
            {
                if (currentMessageToDriver != null)
                {
                    string status = (currentMessageToDriver.Status == MessageStatusCode.Sent) ? MessageStatusCode.SentDeleted :  currentMessageToDriver.Status == MessageStatusCode.Read ? MessageStatusCode.ReadDeleted : currentMessageToDriver.Status;
                    
                    SettingDataProvider.Instance.UpdateMessage(currentMessageToDriver.MessageId,
                                                              status);
                }
            }
            catch(Exception ex)
            {
                Logger.LogException(ex, "Exception in ActionCommandsMessageHandling.DeleteMessage() " + ex.Message); 
            }
           

        }

        /// <summary>
        /// Set status of messageTxt to what it was before deletion of messageTxt
        /// </summary>
        /// <param name="currentMessageToDriver">Current Message</param>
        internal void RestoreMessage(MessageToDriver currentMessageToDriver)
        {
            if (currentMessageToDriver != null)
            {
                SettingDataProvider.Instance.UpdateMessage(currentMessageToDriver.MessageId,
                                                           currentMessageToDriver.Status ==
                                                           MessageStatusCode.SentDeleted
                                                               ? MessageStatusCode.Sent
                                                               : MessageStatusCode.Read);
            }

        }
    }
}

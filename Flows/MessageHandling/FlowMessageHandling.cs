﻿using System;
using System.Linq;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.MessageHandling
{
    public enum FlowStateMessageHandling
    {
        ActionGetMessages,
        ActionStopMessageAlertTimer,
        ActionDeleteMessage,
        ActionReadMessage,
        ActionSendMessage,
        ActionRestoreMessage,
        ActionDeleteAll,
        ViewCommands,
        ViewMessages,
        ViewNewMessages,
        ThisFlowComplete,
    }

    public class FlowDataMessageHandling : BaseFlowData
    {
        public bool IsNewMessage { get; set; }
        public string OrderNumber { get; set; }
    }

    public class FlowResultMessageHandling : BaseFlowResult
    {
    }

    public class FlowMessageHandling : BaseFlow
    {
        private readonly FlowResultMessageHandling _flowResult;
        private FlowDataMessageHandling _flowData;
        private readonly ViewCommandsMessageHandling _viewCommands;
        private readonly ActionCommandsMessageHandling _actionCommands;
        private List<MessageToDriver> _listMessages;
        private MessageToDriver _selectedMessageToDriver;
        private string _currentMessageText;
        private int _unreadMessagesCount;
        private MessageView _currentView;
        private bool _calledFromDelivery;
        private bool _inboxViewTab;
        private string _correlationId;
        public FlowMessageHandling()
        {
            _flowResult = new FlowResultMessageHandling();
            _viewCommands = new ViewCommandsMessageHandling();
            _actionCommands = new ActionCommandsMessageHandling();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Intializing FlowMessageHandling");
            _flowData = (FlowDataMessageHandling)flowData;
            _currentView = _flowData.IsNewMessage == false ? MessageView.MessageView : MessageView.NewMessage;
            _calledFromDelivery = _flowData.IsNewMessage;
            _inboxViewTab = true;
            ExecuteState(FlowStateMessageHandling.ActionStopMessageAlertTimer);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateMessageHandling)state);
        }

        public void ExecuteState(FlowStateMessageHandling state)
        {
            if (state > FlowStateMessageHandling.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        private void ExecuteActionState(FlowStateMessageHandling state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowMessageHandling;" + state);
                    switch (state)
                    {
                        case FlowStateMessageHandling.ActionStopMessageAlertTimer:
                            SignalTimer.End(SignalType.MessageToDriver);
                            state = FlowStateMessageHandling.ActionGetMessages;
                            break;

                        case FlowStateMessageHandling.ActionGetMessages:
                            _listMessages = _actionCommands.GetAllMessages(ModelUser.UserProfile.UserId);
                            _unreadMessagesCount = _listMessages.Count(p => p.Status == "U");
                            state = FlowStateMessageHandling.ViewMessages;
                            break;

                        case FlowStateMessageHandling.ActionDeleteMessage:
                            _actionCommands.DeleteMessage(_selectedMessageToDriver);
                            _currentView = MessageView.MessageView;
                            state = FlowStateMessageHandling.ActionGetMessages;
                            break;

                        case FlowStateMessageHandling.ActionSendMessage:
                            _actionCommands.SendAndSaveMessageFromDriver(_currentMessageText, _correlationId);
                            state = _flowData.IsNewMessage ? FlowStateMessageHandling.ThisFlowComplete :
                            FlowStateMessageHandling.ActionGetMessages;
                            break;

                        case FlowStateMessageHandling.ActionRestoreMessage:
                            _actionCommands.RestoreMessage(_selectedMessageToDriver);
                            state = FlowStateMessageHandling.ActionGetMessages;
                            break;

                        case FlowStateMessageHandling.ActionReadMessage:
                            bool result = _actionCommands.UpdateMessageStatusIfNew(_selectedMessageToDriver, MessageStatusCode.Read);
                            if (result == false)
                            {
                                GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.ErrorUpdatingMessageStatus, Severity.Error,
                                                          GlobalTexts.Ok);
                                state = FlowStateMessageHandling.ViewMessages;
                                break;
                            }
                            //_currentView = MessageView.ReadMessage;
                            
                            state = FlowStateMessageHandling.ViewNewMessages;
                            break;



                        case FlowStateMessageHandling.ActionDeleteAll:
                            _actionCommands.DeleteAllPermanently();
                            state = FlowStateMessageHandling.ActionGetMessages;
                            break;

                    }
                }
                while (state < FlowStateMessageHandling.ViewCommands);
                ExecuteViewState(state);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowMessageHandling.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateMessageHandling.ThisFlowComplete);
            }
        }

        private void ExecuteViewState(FlowStateMessageHandling state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowMessageHandling;" + state);

            try
            {
                switch (state)
                {
                    case FlowStateMessageHandling.ViewMessages:
                        _viewCommands.ViewMessages(MessagesViewHandler, _listMessages, _currentView, _unreadMessagesCount, _selectedMessageToDriver, _calledFromDelivery, _flowData.OrderNumber, _inboxViewTab);
                        _inboxViewTab = false;
                        break;

                    case FlowStateMessageHandling.ViewNewMessages:
                        _correlationId = _currentView == MessageView.ReplyMessage
                                             ? _selectedMessageToDriver.CorrelationId
                                             : string.Empty;
                        _viewCommands.ViewNewMessages(NewMessageViewHandler, _currentView, _selectedMessageToDriver);
                        break;

                    case FlowStateMessageHandling.ThisFlowComplete:
                        EndFlow();
                        break;

                }

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowMessageHandling.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
            }
        }

        private void MessagesViewHandler(int itemDetailsViewEvents, params object[] data)
        {
            switch (itemDetailsViewEvents)
            {
                case MessagesViewEvents.Back:
                    ExecuteViewState(FlowStateMessageHandling.ThisFlowComplete);
                    break;

                case MessagesViewEvents.Delete:
                    _selectedMessageToDriver = (MessageToDriver)data[0];
                    ExecuteActionState(FlowStateMessageHandling.ActionDeleteMessage);
                    break;

                case MessagesViewEvents.NewMessage:
                    _currentView = MessageView.NewMessage;
                    _selectedMessageToDriver = null;
                    _inboxViewTab = false;
                    ExecuteViewState(FlowStateMessageHandling.ViewNewMessages);
                    break;

                case MessagesViewEvents.Read:
                    _currentView = MessageView.ReadMessage;
                    _selectedMessageToDriver = (MessageToDriver)(data[0]);
                    _inboxViewTab = false;
                    ExecuteActionState(FlowStateMessageHandling.ActionReadMessage);
                    break;

                case MessagesViewEvents.Send:
                    _currentView = MessageView.MessageView;
                    _currentMessageText = (string)data[0];
                    ExecuteActionState(FlowStateMessageHandling.ActionSendMessage);
                    break;

                case MessagesViewEvents.Restore:
                    _currentView = MessageView.MessageView;
                    _selectedMessageToDriver = (MessageToDriver)(data[0]);
                    ExecuteActionState(FlowStateMessageHandling.ActionRestoreMessage);
                    break;

                case MessagesViewEvents.Reply:
                    _selectedMessageToDriver = (MessageToDriver)(data[0]);
                    _currentView = MessageView.ReplyMessage;
                    _inboxViewTab = false;
                    ExecuteViewState(FlowStateMessageHandling.ViewNewMessages);
                    break;

                case MessagesViewEvents.Messages:
                    _currentView = MessageView.MessageView;
                    ExecuteViewState(FlowStateMessageHandling.ViewMessages);
                    break;

                case MessagesViewEvents.DeleteAll:
                    ExecuteActionState(FlowStateMessageHandling.ActionDeleteAll);
                    break;

                case MessagesViewEvents.Refresh:
                    ExecuteActionState(FlowStateMessageHandling.ActionGetMessages);
                    break;


            }
        }

        private void NewMessageViewHandler(int newMessageViewEvents, params object[] data)
        {
            switch (newMessageViewEvents)
            {
                case NewMessageViewEvents.Back:
                    ExecuteViewState(_calledFromDelivery
                                         ? FlowStateMessageHandling.ThisFlowComplete
                                         : FlowStateMessageHandling.ViewMessages);

                    break;
                case NewMessageViewEvents.Delete:
                    _selectedMessageToDriver = (MessageToDriver)data[0];
                    ExecuteActionState(FlowStateMessageHandling.ActionDeleteMessage);
                    break;
                case NewMessageViewEvents.Reply:
                    _selectedMessageToDriver = (MessageToDriver)(data[0]);
                    _currentView = MessageView.ReplyMessage;
                    _inboxViewTab = false;
                    ExecuteViewState(FlowStateMessageHandling.ViewNewMessages);
                    break;
                case NewMessageViewEvents.Messages:
                    _currentView = MessageView.MessageView;
                    ExecuteActionState(FlowStateMessageHandling.ActionGetMessages);
                    break;
                case NewMessageViewEvents.Send:
                    _currentView = MessageView.MessageView;
                    _currentMessageText = (string)data[0];
                    ExecuteActionState(FlowStateMessageHandling.ActionSendMessage);
                    break;
            }
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowMessageHandling");
            BaseModule.EndSubFlow(_flowResult);
        }
    }

    public sealed class MessageStatusCode
    {
        public static string Unread = "U";
        public static string Read = "R";
        public static string Sent = "S";
        public static string ReadDeleted = "RD";
        public static string SentDeleted = "SD";
    }

    public enum MessageView
    {
        NewMessage,
        ReadMessage,
        ReplyMessage,
        MessageView
    }
}

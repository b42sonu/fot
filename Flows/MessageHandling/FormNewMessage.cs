﻿using System;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.MessageHandling
{

    public class FormDataNewMessage : BaseFormData
    {
        public MessageView CurrentView { get; set; }
        public MessageToDriver MessageToDriver { get; set; }
        public bool CalledFromStopDetails { get; set; }
        public string OrderNumber { get; set; }
        public bool InboxView { get; set; }
    }

    public class NewMessageViewEvents : ViewEvent
    {
        public const int Back = 0;
        public const int Delete = 1;
        public const int Reply = 2;
        public const int Messages = 3;
        public const int Send = 4;
    }

    public partial class FormNewMessage : BaseForm
    {
        #region private vars
        private FormDataNewMessage _formData;
        private readonly MultiButtonControl _tabButtons;
        private const string BackButton = "backButton";
        private const string DeleteButton = "deleteButton";
        private const string ReplyButton = "replyButton";
        private const string SendButton = "sendButton";
        #endregion

        public FormNewMessage()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            CreateButtons();
        }

        private void CreateButtons()
        {
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Reply, ButtonReplyClick, ReplyButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Send, ButtonSendClick, SendButton));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }

        private void TranslateTexts()
        {
            SetButtonTextIfExists(BackButton, GlobalTexts.Back);
            SetButtonTextIfExists(DeleteButton, GlobalTexts.Delete);
            SetButtonTextIfExists(ReplyButton, GlobalTexts.Reply);
            SetButtonTextIfExists(SendButton, GlobalTexts.Send);
        }

        private void SetButtonTextIfExists(string buttonName, string caption)
        {
            var btn = _tabButtons.GetButton(buttonName);
            if (btn != null) btn.Text = caption;
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            TranslateTexts();
            _formData = (FormDataNewMessage)formData;
            if (_formData != null)
            {
                SetButtonVisibility(true);
                switch (_formData.CurrentView)
                {
                    case MessageView.ReadMessage:
                        txtNewMessage.ReadOnly = true;
                        lblModuleName.Text = GlobalTexts.ReadMessage;

                        if (_formData.MessageToDriver != null)
                        {
                            txtNewMessage.Text = HandleNewLine(_formData.MessageToDriver.MessageText);
                            _tabButtons.SetButtonEnabledState(DeleteButton, !((_formData.MessageToDriver.Status == MessageStatusCode.ReadDeleted) || (_formData.MessageToDriver.Status == MessageStatusCode.ReadDeleted)));

                        }

                        break;

                    case MessageView.NewMessage:
                        txtNewMessage.ReadOnly = false;
                        ShiftFocus();
                        txtNewMessage.Text = string.Empty;
                        if (_formData.CalledFromStopDetails)
                            txtNewMessage.Text = _formData.OrderNumber;
                        lblModuleName.Text = GlobalTexts.NewMessage;
                        break;

                    case MessageView.ReplyMessage:
                        SetViewForReply();
                        break;
                }
               

            }
        }

        private string HandleNewLine(string message)
        {
            if(String.IsNullOrEmpty(message) == false)
            {
               message = message.Replace("\n", "\r\n");
            }
            return message;
        }

        private void SetViewForReply()
        {
            ShiftFocus();
            txtNewMessage.ReadOnly = false;
            txtNewMessage.Text = string.Empty;
            lblModuleName.Text = GlobalTexts.NewMessage;
        }

        private void ShiftFocus()
        {
            lblModuleName.Focus();
        }

        private void SetButtonVisibility(bool showDeleteButton)
        {
            _tabButtons.Clear();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));//Back button visible always
            if (_formData.CurrentView == MessageView.ReadMessage)
            {
                if (_formData.MessageToDriver != null)
                {
                    if (_formData.MessageToDriver.Status != MessageStatusCode.Sent && _formData.MessageToDriver.Status != MessageStatusCode.SentDeleted)
                    {
                        _tabButtons.AddEmptyButton();
                        _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Reply, ButtonReplyClick, ReplyButton));
                        if (showDeleteButton)
                            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));//Delete button added like this for position 
                        _tabButtons.AddEmptyButton();
                    }
                    else
                    {
                        _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
                    }
                }
            }
            else
            {
                _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Send, ButtonSendClick, SendButton));
            }
            _tabButtons.GenerateButtons();

        }

        #region ButtonClickHandlers

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(_formData.CalledFromStopDetails
                                     ? NewMessageViewEvents.Back
                                     : NewMessageViewEvents.Messages);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonBackClick");
            }
        }

        private void ButtonSendClick(object sender, EventArgs e)
        {
            try
            {

                if (String.IsNullOrEmpty(txtNewMessage.Text) == false)
                {
                    ViewEvent.Invoke(NewMessageViewEvents.Send, txtNewMessage.Text);
                }
                else
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Message, GlobalTexts.PleaseEnterMessageToSend, Severity.Info, GlobalTexts.Ok);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.NewMessageClick");
            }
        }

        private void ButtonDeleteClick(object sender, EventArgs e)
        {
            try
            {
                string result = GuiCommon.ShowModalDialog(GlobalTexts.Delete,
                                                          GlobalTexts.ConfirmDeleteMessage, Severity.Warning, GlobalTexts.Yes,
                                                          GlobalTexts.No);
                if (result == GlobalTexts.Yes)
                    ViewEvent.Invoke(NewMessageViewEvents.Delete, _formData.MessageToDriver);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonDeleteClick");
            }
        }

        private void ButtonReplyClick(object sender, EventArgs e)
        {
            try
            {
                SetViewForReply();
                EnableOnScreenKeyboard();
                OnResume();
                _tabButtons.Clear();
                _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
                _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Send, ButtonSendClick, SendButton));
                _tabButtons.GenerateButtons();

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonReplyClick");
            }
        }

        #endregion

    }
}
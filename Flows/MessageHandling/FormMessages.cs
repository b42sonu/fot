﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.MessageHandling
{

    public class FormDataMessages : BaseFormData
    {
        public List<MessageToDriver> ListMessages { get; set; }
        public MessageView CurrentView { get; set; }
        public int UnreadMessageCount { get; set; }
        public MessageToDriver MessageToDriver { get; set; }
        public bool CalledFromStopDetails { get; set; }
        public string OrderNumber { get; set; }
        public bool InboxView { get; set; }
    }

    public class MessagesViewEvents : ViewEvent
    {
        public const int Read = 0;
        public const int Back = 1;
        public const int NewMessage = 2;
        public const int Delete = 3;
        public const int DeleteAll = 4;
        public const int Reply = 5;
        public const int Restore = 6;
        public const int Messages = 7;
        public const int Send = 8;
        public const int Refresh = 9;
    }

    public partial class FormMessages : BaseForm
    {
        private FormDataMessages _formData;
        private readonly MultiButtonControl _tabButtons;
        private const string BackButton = "backButton";
        private const string NewMessageButton = "newMessageButton";
        private const string ReadButton = "readButton";
        private const string DeleteButton = "deleteButton";
        private const string ReplyButton = "replyButton";
        private const string DeleteAllButton = "deleteAllButton";
        private const string RestoreButton = "restoreButton";
        private const string SendButton = "sendButton";
        private MessageToDriver _currentMessage;
        bool _isFirstTime = true;
        public FormMessages()
        {
            _tabButtons = new MultiButtonControl();
            InitializeComponent();
            CreateButtons();
        }

        private void CreateButtons()
        {
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.NewMessage, NewMessageClick, NewMessageButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Read, ButtonReadClick, ReadButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Reply, ButtonReplyClick, ReplyButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Send, ButtonSendClick, SendButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.DeleteAll, ButtonDeleteAllClick, DeleteAllButton));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Restore, ButtonRestoreClick, RestoreButton));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }
        private void TranslateTexts()
        {
            SetButtonTextIfExists(BackButton, GlobalTexts.Back);
            SetButtonTextIfExists(DeleteButton, GlobalTexts.Delete);
            SetButtonTextIfExists(NewMessageButton, GlobalTexts.NewMessage);
            SetButtonTextIfExists(ReadButton, GlobalTexts.Read);
            SetButtonTextIfExists(ReplyButton, GlobalTexts.Reply);
            SetButtonTextIfExists(SendButton, GlobalTexts.Reply);
            SetButtonTextIfExists(DeleteAllButton, GlobalTexts.DeleteAll);
            SetButtonTextIfExists(RestoreButton, GlobalTexts.Restore);

            if (tabDeleted != null) tabDeleted.TabItem.Text = GlobalTexts.Deleted;
            if (tabRead != null) tabRead.TabItem.Text = GlobalTexts.Sent;
        }
        private void SetButtonTextIfExists(string buttonName, string caption)
        {
            var btn = _tabButtons.GetButton(buttonName);
            if (btn != null) btn.Text = caption;
        }

        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            TranslateTexts();
            _formData = (FormDataMessages)formData;
            if (_formData != null)
            {
                tabInbox.TabItem.Text = GlobalTexts.Inbox + "(" + _formData.UnreadMessageCount + ")";
                txtNewMessage.ReadOnly = false;
                switch (_formData.CurrentView)
                {
                    case MessageView.ReadMessage:
                        pnlNewMessage.Visible = true;
                        tabControlOperationList.Visible = false;
                        txtNewMessage.ReadOnly = true;
                        HideOnScreenKeyboard();
                        lblModuleName.Text = GlobalTexts.ReadMessage;
                        if (_formData.MessageToDriver != null)
                        {
                            txtNewMessage.Text = _formData.MessageToDriver.MessageText;
                        }
                        EnableButtonsForNewReadMessage();
                        break;

                    case MessageView.NewMessage:
                        pnlNewMessage.Visible = true;
                        tabControlOperationList.Visible = false;
                        txtNewMessage.Text = string.Empty;
                        if (_formData.CalledFromStopDetails == false)
                            txtNewMessage.Focus();
                        if (_formData.MessageToDriver != null)
                        {
                            txtNewMessage.Text = _formData.MessageToDriver.MessageText;
                            txtNewMessage.ReadOnly = true;
                        }
                        if (_formData.CalledFromStopDetails)
                        {
                            txtNewMessage.Text = _formData.OrderNumber + " ";//Add extra space Mottak 837
                            txtNewMessage.Select(txtNewMessage.Text.Length + 1, 0);
                        }
                        lblModuleName.Text = GlobalTexts.NewMessage;

                        EnableButtonsForNewReadMessage();
                        break;

                    case MessageView.MessageView:
                        if (_isFirstTime || _formData.InboxView)
                        {
                            _isFirstTime = false;
                            tabControlOperationList.SelectedTab = tabInbox;
                        }
                        HideOnScreenKeyboard();
                        TabControlOperationSelectedIndexChanged(tabControlOperationList, null);
                        tabControlOperationList.Visible = true;
                        pnlNewMessage.Visible = false;
                        lblModuleName.Text = GlobalTexts.Message;
                        break;

                    case MessageView.ReplyMessage:
                        pnlNewMessage.Visible = true;
                        txtNewMessage.Focus();
                        tabControlOperationList.Visible = false;
                        txtNewMessage.Text = string.Empty;
                        lblModuleName.Text = GlobalTexts.NewMessage;
                        EnableButtonsForNewReadMessage();

                        break;
                }

            }
        }



        private void EnableButtonsForNewReadMessage()
        {
            _tabButtons.Clear();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton));
            if (_formData.CurrentView == MessageView.ReadMessage)
            {
                if (_formData.MessageToDriver != null)
                {
                    if (_formData.MessageToDriver.Status != MessageStatusCode.Sent && _formData.MessageToDriver.Status != MessageStatusCode.SentDeleted)
                    {
                        _tabButtons.AddEmptyButton();
                        _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Reply, ButtonReplyClick, ReplyButton));
                        _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
                        _tabButtons.AddEmptyButton();
                    }
                    else
                    {
                        _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
                    }
                }

            }
            else
            {
                _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Send, ButtonSendClick, SendButton));
            }

            _tabButtons.GenerateButtons();

        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(MessagesViewEvents.Back);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonBackClick");
            }
        }

        private void ButtonReadClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(MessagesViewEvents.Read, _currentMessage);// advancedListOperationList.DataRows[advancedListOperationList.SelectedRow.Index + 1]["MessageId"]
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonReadClick");
            }
        }

        private void NewMessageClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(MessagesViewEvents.NewMessage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.NewMessageClick");
            }
        }

        private void ButtonSendClick(object sender, EventArgs e)
        {
            try
            {

                if (String.IsNullOrEmpty(txtNewMessage.Text) == false)
                {
                    ViewEvent.Invoke(MessagesViewEvents.Send, txtNewMessage.Text);
                }
                else
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Message, GlobalTexts.PleaseEnterMessageToSend, Severity.Info, GlobalTexts.Ok);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.NewMessageClick");
            }
        }



        private void ButtonDeleteClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Info, "Current message Delete=" + _currentMessage);
                string result;
                if (tabControlOperationList.SelectedTab == tabInbox)
                {
                    result = GuiCommon.ShowModalDialog(GlobalTexts.Delete,
                                        GlobalTexts.ConfirmDeleteMessage,
                                         Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
                    if (result == GlobalTexts.Yes)
                        ViewEvent.Invoke(MessagesViewEvents.Delete, _currentMessage);
                }
                else
                {
                    result = GuiCommon.ShowModalDialog(GlobalTexts.Delete,
                                                           GlobalTexts.ConfirmDeleteMessage, Severity.Warning, GlobalTexts.Yes,
                                                           GlobalTexts.No);
                    if (result == GlobalTexts.Yes)
                        ViewEvent.Invoke(MessagesViewEvents.Delete, _currentMessage);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonDeleteClick");
            }
        }

        private void ButtonDeleteAllClick(object sender, EventArgs e)
        {
            try
            {
                var result = GuiCommon.ShowModalDialog(GlobalTexts.Delete,
                                          GlobalTexts.ConfirmDeleteAllMessage,
                                          Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
                if (result == GlobalTexts.Yes)
                    ViewEvent.Invoke(MessagesViewEvents.DeleteAll);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonDeleteAllClick");
            }
        }

        public void RefreshPage()
        {
            ViewEvent.Invoke(MessagesViewEvents.Refresh);
        }

        private void ButtonReplyClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(MessagesViewEvents.Reply, _currentMessage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonReplyClick");
            }
        }

        private void ButtonRestoreClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(MessagesViewEvents.Restore, _currentMessage);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.ButtonRestoreClick");
            }
        }


        private void TabControlOperationSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                var tabControl = (Resco.Controls.CommonControls.TabControl)sender;
                _tabButtons.BeginUpdate();
                SetButtons(tabControl.SelectedIndex);

                switch (tabControl.SelectedIndex)
                {
                    case 0:
                        tabInbox.Controls.Add(advancedListMessages);
                        advancedListMessages.BeginUpdate();
                        advancedListMessages.DataSource = _formData.ListMessages.Where(p => (p.Status == MessageStatusCode.Unread) || (p.Status == MessageStatusCode.Read)).OrderByDescending(p => p.Status).ThenByDescending(p=>p.MessageTime);
                        advancedListMessages.EndUpdate();
                        advancedListMessages.ActiveRowIndex = 0;

                        break;
                    case 1:
                        tabRead.Controls.Add(advancedListMessages);
                        advancedListMessages.BeginUpdate();
                        advancedListMessages.DataSource = _formData.ListMessages.Where(p => p.Status == MessageStatusCode.Sent).OrderByDescending(p=>p.MessageTime);
                        advancedListMessages.EndUpdate();
                        advancedListMessages.ActiveRowIndex = 0;
                        break;
                    case 2:
                        tabDeleted.Controls.Add(advancedListMessages);
                        advancedListMessages.BeginUpdate();
                        advancedListMessages.DataSource = _formData.ListMessages.Where(p => (p.Status == MessageStatusCode.ReadDeleted) || (p.Status == MessageStatusCode.SentDeleted)).OrderByDescending(p=>p.MessageTime);
                        advancedListMessages.EndUpdate();
                        advancedListMessages.ActiveRowIndex = 0;
                        break;
                }
                EnableDisableButtonsAccordingToSelection();
                _tabButtons.EndUpdate();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.TabControlOperationSelectedIndexChanged");
            }
        }

        private void SetButtons(int p)
        {
            var tab = (TabMessages)p;
            _tabButtons.Clear();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, BackButton) { ButtonType = TabButtonType.Cancel });
            switch (tab)
            {
                case TabMessages.Inbox:
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.NewMessage, NewMessageClick, NewMessageButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Read, ButtonReadClick, ReadButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Reply, ButtonReplyClick, ReplyButton));

                    break;

                case TabMessages.Sent:
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.NewMessage, NewMessageClick, NewMessageButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Read, ButtonReadClick, ReadButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDeleteClick, DeleteButton));
                    _tabButtons.AddEmptyButton();
                    break;
                case TabMessages.Deleted:
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.NewMessage, NewMessageClick, NewMessageButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Read, ButtonReadClick, ReadButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.DeleteAll, ButtonDeleteAllClick, DeleteAllButton));
                    _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Restore, ButtonRestoreClick, RestoreButton));
                    break;
            }
            _tabButtons.GenerateButtons();
        }

        private void EnableDisableButtonsAccordingToSelection()
        {
            if(_tabButtons.Exists(ReadButton))
                _tabButtons.SetButtonEnabledState(ReadButton, advancedListMessages.ActiveRowIndex != -1);
            if(_tabButtons.Exists(ReplyButton))
                _tabButtons.SetButtonEnabledState(ReplyButton, advancedListMessages.ActiveRowIndex != -1);
            if(_tabButtons.Exists(DeleteButton))
                _tabButtons.SetButtonEnabledState(DeleteButton, advancedListMessages.ActiveRowIndex != -1);
            if (_tabButtons.Exists(DeleteAllButton))
                _tabButtons.SetButtonEnabledState(DeleteAllButton, advancedListMessages.DataRows.Count > 0);
            if (_tabButtons.Exists(RestoreButton))
                _tabButtons.SetButtonEnabledState(RestoreButton, advancedListMessages.ActiveRowIndex != -1);
            if (_currentMessage != null)
            {
                if (advancedListMessages.DataRows != null && advancedListMessages.DataRows.Count > 0)
                {
                    if (_tabButtons.Exists(DeleteButton))
                        _tabButtons.SetButtonEnabledState(DeleteButton, _currentMessage.Status != MessageStatusCode.Unread);
                    if(_tabButtons.Exists(ReplyButton))
                        _tabButtons.SetButtonEnabledState(ReplyButton, (_currentMessage.Status != MessageStatusCode.Sent) && (_currentMessage.Status != MessageStatusCode.SentDeleted));

                }
            }


        }

        private void AdvancedListOperationListRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                EnableDisableButtonsAccordingToSelection();
                if (advancedListMessages.ActiveRowIndex >= 0)
                {
                    var consignmentItemId = (string)e.DataRow["MessageId"];
                    _currentMessage = _formData.ListMessages.FirstOrDefault(p => p.MessageId == consignmentItemId);
                    if (_currentMessage != null)
                    {
                        if(_tabButtons.Exists(DeleteButton))
                             _tabButtons.SetButtonEnabledState(DeleteButton, _currentMessage.Status != MessageStatusCode.Unread);//Disable delete button if message no read  
                    }
                }
                else
                {
                    _currentMessage = null;
                }

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormMessages.OnActiveRowChanged");
            }
        }
    }

    public enum TabMessages
    {
        Inbox,
        Sent,
        Deleted,
    }
}
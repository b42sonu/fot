﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RbtScan
{
    class ViewRbtScan
    {
        public static void FormRbtScan(ViewEventDelegate viewEventHandler)
        {
            var view = ViewCommands.ShowView<FormRbtScan>();
            view.AutoShow = true;
            view.SetEventHandler(viewEventHandler);

        }
    }
}

﻿using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RbtScan
{
    partial class FormRbtScan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.messageControlValidateScan = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.lblScanHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtScannedNumber = new System.Windows.Forms.TextBox();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.buttonCancel = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOk = new Resco.Controls.OutlookControls.ImageButton();
            this.buttonOrgUnit = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOrgUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.messageControlValidateScan);
            this.touchPanel.Controls.Add(this.lblScanHeading);
            this.touchPanel.Controls.Add(this.txtScannedNumber);
            this.touchPanel.Controls.Add(this.buttonOrgUnit);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.buttonCancel);
            this.touchPanel.Controls.Add(this.buttonOk);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            //
            // messageControlValidateScan
            // 
            this.messageControlValidateScan.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControlValidateScan.Location = new System.Drawing.Point(24, 316);
            this.messageControlValidateScan.Name = "messageControlValidateScan";
            this.messageControlValidateScan.Size = new System.Drawing.Size(424, 110);
            this.messageControlValidateScan.TabIndex = 56;

            // lblScanHeading
            // 
            //this.lblScanHeading.AutoSize = false;
            this.lblScanHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblScanHeading.Location = new System.Drawing.Point(29, 60);
            this.lblScanHeading.Name = "lblScanHeading";
            //this.lblScanHeading.Size = new System.Drawing.Size(450, 25);

            // 
            // txtScannedNumber
            // 
            this.txtScannedNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.txtScannedNumber.Location = new System.Drawing.Point(24, 100);
            this.txtScannedNumber.Name = "txtScannedNumber";
            this.txtScannedNumber.Size = new System.Drawing.Size(424, 40);
            this.txtScannedNumber.TabIndex = 37;

            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(135, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(0, 0);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonCancel.ForeColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(0, 501);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(158, 50);
            this.buttonCancel.TabIndex = 29;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // buttonOk
            // 
            this.buttonOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOk.ForeColor = System.Drawing.Color.White;
            this.buttonOk.Location = new System.Drawing.Point(322, 501);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(158, 50);
            this.buttonOk.TabIndex = 28;
            //
            //buttonOrgUnit
            // 
            this.buttonOrgUnit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonOrgUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.buttonOrgUnit.ForeColor = System.Drawing.Color.White;
            this.buttonOrgUnit.Location = new System.Drawing.Point(161, 501);
            this.buttonOrgUnit.Name = "buttonOrgUnit";
            this.buttonOrgUnit.Size = new System.Drawing.Size(158, 50);
            this.buttonCancel.TabIndex = 29;
            this.buttonOrgUnit.Click += new System.EventHandler(ButtonOrgUnitClick);
            // 
            // FormRbtScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormRbtScan";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblScanHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            this.ResumeLayout(false);

        }

       
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton buttonCancel;
        private Resco.Controls.OutlookControls.ImageButton buttonOrgUnit;
        private Resco.Controls.OutlookControls.ImageButton buttonOk;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private TextBox txtScannedNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblScanHeading;
        private MessageControl messageControlValidateScan;


        #endregion
    }
}
﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage.View;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RbtScan
{

    public partial class FormRbtScan : BaseBarcodeScanForm
    {
        public FormRbtScan()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            SetTextToControls();
            InitOnScreenKeyBoardProperties();
        }

        private void SetTextToControls()
        {
            labelModuleName.Text = GlobalTexts.LoadLineHaul;
            lblScanHeading.Text = GlobalTexts.ScanLoadCarrierAndTripId;
            buttonCancel.Text = GlobalTexts.Back;
            buttonOrgUnit.Text = GlobalTexts.OrgUnit;
            buttonOk.Text = GlobalTexts.Ok;
        }
        private void InitOnScreenKeyBoardProperties()
        {
            var keyBoardProperty = new OnScreenKeyboardProperties(GlobalTexts.EnterBarcode, KeyPadTypes.Numeric, KeyPadTypes.Qwerty)
            {
                TextChanged = ScannedNumberTextChanged
            };

            txtScannedNumber.Tag = keyBoardProperty;
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            if (messageControlValidateScan != null)
                messageControlValidateScan.ClearMessage();
            if (userFinishedTyping)
                ValidateScanBarCode(textentered);
        }

        private void ValidateScanBarCode(string textentered)
        {

            String enteredBarcode = textentered;
            var scannerOutput = new FotScannerOutput { Code = enteredBarcode };
            BarcodeScanned(scannerOutput);
        }
        public override void OnShow(IFormData formData)
        {
            base.OnShow(formData);
            messageControlValidateScan.ClearMessage();
            SetKeyboardState(KeyboardState.Numeric);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            FormDataRbtScan formData = (FormDataRbtScan)iformData;
            PopulateMessage(formData.MessageHolder);

        }
        private void PopulateMessage(MessageHolder messageHolder)
        {
            if (messageHolder != null)
                messageControlValidateScan.ShowMessage(messageHolder);
            else
                messageControlValidateScan.ClearMessage();
        }

        public override void BarcodeScanned(FotScannerOutput scannerOutput)
        {
            txtScannedNumber.Text = "";
            ViewEvent.Invoke(FormDamageScan.DamageScanViewEvents.BarcodeScanned, scannerOutput);
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(RbtScanViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadingScan.ButtonBackClick");
            }
        }

        private void ButtonOrgUnitClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(RbtScanViewEvents.OrgUnit);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormLoadingScan.ButtonOrgUnitClick");
            }
        }
    }
    #region View specific classes

    public class FormDataRbtScan : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
    }
    #endregion

    public class RbtScanViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
        public const int BarcodeScanned = 2;
        public const int OrgUnit = 3;

    }
}
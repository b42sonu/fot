﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RbtScan
{
    public enum FlowStatesRbtScan
    {
        ActionValidateScan,
        ViewCommands,
        ShowFormRbtScan,
        ViewUpdateFormRbtScan,
        ThisFlowComplete
    }

    public class FlowResultRbtScan : BaseFlowResult
    {
        public bool IsToSelectOrgUnit { get; set; }
    }

    public class FlowRbtScan : BaseFlow
    {
        private readonly FlowResultRbtScan _flowResult;
        private FotScannerOutput _scannerOutput;
        private readonly ActionCommandsScanBarcode _actionCommandsScanBarcode;
        private readonly MessageHolder _messageHolder;
        private BarcodeData _currentScanningBarcode;
        public FlowRbtScan()
        {

            _messageHolder = new MessageHolder();
            _flowResult = new FlowResultRbtScan();
            _actionCommandsScanBarcode = _actionCommandsScanBarcode = new ActionCommandsScanBarcode(_messageHolder);
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowRbtScan.BeginFlow");
            ExecuteState(FlowStatesRbtScan.ShowFormRbtScan);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesRbtScan)state);
        }

        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowStateLoading, which acts as name for next command.</param>
        public void ExecuteState(FlowStatesRbtScan state)
        {
            if (state > FlowStatesRbtScan.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesRbtScan state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowRbtScan;" + state);
                switch (state)
                {
                    case FlowStatesRbtScan.ShowFormRbtScan:
                        ViewRbtScan.FormRbtScan(LoadingScanEventHandler);
                        break;

                    case FlowStatesRbtScan.ViewUpdateFormRbtScan:
                        IFormData formData = new FormDataRbtScan { MessageHolder = _messageHolder };
                        CurrentView.UpdateView(formData);
                        break;

                    case FlowStatesRbtScan.ThisFlowComplete:
                        EndFlow();
                        break;
                }

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowRbtScan.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesRbtScan.ThisFlowComplete);
            }
        }


        private void ExecuteActionState(FlowStatesRbtScan state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowRbtScan;" + state);
                do
                {
                    switch (state)
                    {
                        case FlowStatesRbtScan.ActionValidateScan:
                            bool result = _actionCommandsScanBarcode.ValidateBarcode(_scannerOutput, BarcodeType.Rbt, out _currentScanningBarcode, true);
                            state = result ? 
                                FlowStatesRbtScan.ThisFlowComplete : FlowStatesRbtScan.ViewUpdateFormRbtScan;
                            break;
                    }
                } while (state < FlowStatesRbtScan.ViewCommands);

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowRbtScan.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesRbtScan.ThisFlowComplete);
            }
            ExecuteViewState(state);
        }

        private void LoadingScanEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case RbtScanViewEvents.Back:
                    ExecuteState(FlowStatesRbtScan.ThisFlowComplete);
                    break;

                case RbtScanViewEvents.BarcodeScanned:
                    _scannerOutput = (FotScannerOutput)data[0];
                    ExecuteState(FlowStatesRbtScan.ActionValidateScan);
                    break;

                case RbtScanViewEvents.OrgUnit:
                    _flowResult.IsToSelectOrgUnit = true;
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStatesRbtScan.ThisFlowComplete);
                    break;
            }
        }

        /// <summary>
        /// This method ends the loading flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowLoading.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}
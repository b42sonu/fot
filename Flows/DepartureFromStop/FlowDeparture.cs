﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.AttemptedDelivery;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Commands;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop
{
    // US 1e: http://kp-confluence.postennorge.no/display/FOT/1e+-+Finishing+Operation+from+Operation+List+%28Departure+from+Stop%29
    /// <summary>
    /// Enum which specifies the collection of State for this flow.
    /// </summary>


    /// <summary>
    /// This class acts as initial data for flow, FlowDepartureFromStop.
    /// </summary>
    public class FlowDataDeparture : BaseFlowData
    {

    }


    /// <summary>
    /// This class acts as , result from flow FlowDepartureFromStop when it will come back to parent flow which
    /// initialized this flow.
    /// </summary>
    public class FlowResultDeparture : BaseFlowResult
    {
    }

    /// <summary>
    /// This class acts as a flow for departing from stop.
    /// </summary>
    public class FlowDeparture : BaseFlow
    {
        #region Private, readonly and constant variables

        readonly ActionCommandsDepartureFromStop _actionCommands = new ActionCommandsDepartureFromStop();
        private PlannedOperationTypeOperationType _operationType;
        private bool _result;

        #endregion

        /// <summary>
        /// This method initializes the flow with required initial data.
        /// </summary>
        /// <param name="flowData">Specifies the flow data supplied by parent flow.</param>
        public override void BeginFlow(IFlowData flowData)
        {

            if(ModelMain.SelectedOperationProcess.IsLoadListOperation == false)
            {
               var plannedOperation =
               ModelOperationList.OperationList.GetPlannedOperation(ModelMain.SelectedOperationProcess.StopId,
                                                                    ModelMain.SelectedOperationProcess.OperationId);
                _operationType = plannedOperation.OperationType;     
            }
            else
                _operationType = PlannedOperationTypeOperationType.DELIVERY; 
           

            var initialState = ModelMain.SelectedOperationProcess.IsLoadListOperation
                                   ? FlowStateDepartureFromStop.ActionDepartureConfirmation
                                   : FlowStateDepartureFromStop.ActionIsPickUpOperationsPendingOnStop;

            ExecuteState(initialState);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateDepartureFromStop)state);
        }

        public void ExecuteState(FlowStateDepartureFromStop state)
        {
            if (state > FlowStateDepartureFromStop.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        private void ExecuteViewState(FlowStateDepartureFromStop state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowDepartureFromStop;" + state);

            try
            {
                switch (state)
                {
                    case FlowStateDepartureFromStop.ThisFlowComplete:
                        EndFlow();
                        break;

                    case FlowStateDepartureFromStop.FlowAttemptedDelivery:
                        var flowDataAttemptedDelivery = new FlowDataAttemptedDelivery
                        {
                            EntityMap = null,
                            IsDirectFromOperationList = true
                        };
                        StartSubFlow<FlowAttemptedDelivery>(flowDataAttemptedDelivery, (int)FlowStateDepartureFromStop.ActionBackFromFlowAttemptedDelivery, Process.AttemptedDelivery);
                        break;

                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }
            catch (Exception e)
            {
                string errorMessage = "Unexpected error in FlowDepartureFromStop.ExecuteState(): " + e.Message;
                Logger.LogException(e, "FlowDepartureFromStop.ExecuteState");
                _actionCommands.FlowResult = new FlowResultDepartureFromStop { State = FlowResultState.Exception, Message = errorMessage };
                ExecuteState(FlowStateDepartureFromStop.ThisFlowComplete);
            }
        }

        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateDepartureFromStop state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowDepartureFromStop;" + state);
                    switch (state)
                    {

                        case FlowStateDepartureFromStop.ActionIsPickUpOperationsPendingOnStop:
                            _result = CommandsOperationList.IsOperationsOfTypePendingOnStop(ModelMain.SelectedOperationProcess.StopId, PlannedOperationTypeOperationType.PICKUP, StopOperationType.Started);
                            state = _result == false ?
                                FlowStateDepartureFromStop.ActionDepartureConfirmation :
                                FlowStateDepartureFromStop.ActionIsCompletingOperationRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionDepartureConfirmation:
                            state = _actionCommands.ShowDepartureConfirmation(_operationType);
                            break;

                        case FlowStateDepartureFromStop.ActionIsCompletingOperationRequired:
                            _result = _actionCommands.IsCompletingOperationRequired();
                            state = _result ? FlowStateDepartureFromStop.ActionDepartureNotAllowed : FlowStateDepartureFromStop.ActionDepartureConfirmation;
                            break;

                        case FlowStateDepartureFromStop.ActionDepartureNotAllowed:
                            _actionCommands.ShowDepartureNotAllowed();
                            state = FlowStateDepartureFromStop.ThisFlowComplete;
                            break;

                        case FlowStateDepartureFromStop.ActionMarkLoadListOperationStatusToFinished:
                            //We are passing here stop id, because we placed consignment number in stopid
                            AdditionalCommands.UpdateLoadListOperationStatus(ModelMain.SelectedOperationProcess.StopId);
                            state = FlowStateDepartureFromStop.ThisFlowComplete;
                            break;

                        case FlowStateDepartureFromStop.ActionIsOperationFinishedEventRequired:
                            _result = _actionCommands.IsOperationFinishEventRequired();
                            state = _result ? 
                                FlowStateDepartureFromStop.ActionSendOperationFinishedEvent: 
                                FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionSendOperationFinishedEvent:
                            EventReportHelper.SendOperationFinishedEvent(ModelMain.SelectedOperationProcess, string.Empty, true, null, null);
                            state = FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired:
                            _result = _actionCommands.IsSummaryMessageRequired();
                            state = _result
                                        ? FlowStateDepartureFromStop.ActionTriggerSummaryMessage
                                        : FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionTriggerSummaryMessage:
                            var actionType = _actionCommands.ActionTypeForSelectedOperation(_operationType);
                            EventReportHelper.SendActualsSummaryMessage(ModelMain.SelectedOperationProcess.StopId, ModelMain.SelectedOperationProcess.TripId, true, null, actionType);
                            state = FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired:
                            _result = _actionCommands.IsDepartureFromStopEventRequired();
                            state = _result ? 
                                FlowStateDepartureFromStop.ActionSendDepartureFromStopEvent : 
                                FlowStateDepartureFromStop.ActionMarkOperationStatusToFinished;
                            break;

                        case FlowStateDepartureFromStop.ActionSendDepartureFromStopEvent:
                            var eventCode = EventCodeUtil.GetEventCodeFromProcess(Process.Departure);
                            EventReportHelper.SendDepartureFromStopEvent(ModelMain.SelectedOperationProcess, string.Empty, true, eventCode, null);
                            state = FlowStateDepartureFromStop.ActionMarkOperationStatusToFinished;
                            break;

                        case FlowStateDepartureFromStop.ActionMarkOperationStatusToFinished:
                            CommandsOperationList.MarkSelectedOperationStatus(ModelMain.SelectedOperationProcess, StopOperationType.Finished);
                            state = FlowStateDepartureFromStop.ActionPersistOperationListChanges;
                            break;

                        case FlowStateDepartureFromStop.ActionPersistOperationListChanges:
                            CommandsOperationList.PersistOperationListChanges();
                            state = FlowStateDepartureFromStop.ThisFlowComplete;
                            break;

                        case FlowStateDepartureFromStop.ActionBackFromFlowAttemptedDelivery:
                            state = FlowStateDepartureFromStop.ThisFlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateDepartureFromStop.ViewCommands);
            }

            catch (Exception e)
            {
                string errorMessage = "Unexpected error in FlowDepartureFromStop.ExecuteState(): " + e.Message;
                Logger.LogException(e, "FlowDepartureFromStop.ExecuteState");
                _actionCommands.FlowResult = new FlowResultDepartureFromStop { State = FlowResultState.Exception, Message = errorMessage };
                state = FlowStateDepartureFromStop.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        /// <summary>
        /// This method, does cleanup action for the flow.
        /// </summary>
        public override void CleanupFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDepartureFromStop.CleanupFlow");
        }

        /// <summary>
        /// This method ends the FlowDepartureFromStop.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDepartureFromStop.EndFlow");
            var flowResult = new FlowResultDepartureFromStop
                {
                    State = _actionCommands.FlowResult.State
                };
            BaseModule.EndSubFlow(flowResult);
        }
    }
}

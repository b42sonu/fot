﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Commands;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop
{
    // US 1e: http://kp-confluence.postennorge.no/display/FOT/1e+-+Finishing+Operation+from+Operation+List+%28Departure+from+Stop%29
    /// <summary>
    /// Enum which specifies the collection of State for this flow.
    /// </summary>
    public enum FlowStateDepartureFromStop
    {
        ActionIsProcessWasRelatedToOperation,
        ActionIsOperationFinishedEventRequired,
        ActionIsAllOperationsOnStopFinished,
        ActionIsStopSummaryMessageRequired,
        ActionIsDepartureFromStopEventRequired,
        ActionSendOperationFinishedEvent,
        ActionIsPromptForSummaryRequired,
        ActionTriggerSummaryMessage,
        ActionIsProcessUnloading,
        ActionShowUnloadingVerificationPopUp,
        ActionSendDepartureFromStopEvent,
        ActionMarkOperationStatusToFinished,
        ActionPersistOperationListChanges,
        ActionIsCompletingOperationRequired,
        ActionIsPickUpOperationsPendingOnStop,
        ActionDepartureConfirmation,
        ActionDepartureNotAllowed,
        ActionMarkLoadListOperationStatusToFinished,
        ActionBackFromFlowAttemptedDelivery,

        ViewCommands,
        FlowAttemptedDelivery,
        ThisFlowComplete
    }

    /// <summary>
    /// This class acts as initial data for flow, FlowDepartureFromStop.
    /// </summary>
    public class FlowDataDepartureFromStop : BaseFlowData
    {
        public enum ActionTypes
        {
            // ReSharper disable InconsistentNaming
            LO,
            LA
            // ReSharper restore InconsistentNaming
        }

        public FlowDataDepartureFromStop(string flowName, bool isStartedFromOperationList, string loadCarrier, ActionTypes actionType, EntityMap entityMap)
        {
            IsStartedFromOperationList = isStartedFromOperationList;
            LoadCarrier = loadCarrier;
            HeaderText = flowName;
            ActionType = actionType;
            EntityMap = entityMap;
        }

        public bool IsStartedFromOperationList { get; set; }
        public string LoadCarrier { get; set; }
        public ActionTypes ActionType { get; set; }
        public EntityMap EntityMap { get; set; }
        public Signature Signature { get; set; }

    }


    /// <summary>
    /// This class acts as , result from flow FlowDepartureFromStop when it will come back to parent flow which
    /// initialized this flow.
    /// </summary>
    public class FlowResultDepartureFromStop : BaseFlowResult
    {
    }

    /// <summary>
    /// This class acts as a flow for departing from stop.
    /// </summary>
    public class FlowDepartureFromStop : BaseFlow
    {
        #region Private, readonly and constant variables

        private FlowDataDepartureFromStop _flowData;
        readonly ActionCommandsDepartureFromStop _actionCommands = new ActionCommandsDepartureFromStop();

        #endregion

        /// <summary>
        /// This method initializes the flow with required initial data.
        /// </summary>
        /// <param name="flowData">Specifies the flow data supplied by parent flow.</param>
        public override void BeginFlow(IFlowData flowData)
        {
            _flowData = (FlowDataDepartureFromStop)flowData;
#if DEBUG
            if (_flowData.IsStartedFromOperationList)
            {
                //Logger.LogAssert(string.IsNullOrEmpty(ModelMain.SelectedOperationProcess.StopId) == false &&
                //    string.IsNullOrEmpty(ModelMain.SelectedOperationProcess.TripId) == false);
            }
#endif

            var intialState = ModelMain.SelectedOperationProcess.IsLoadListOperation
                                  ? FlowStateDepartureFromStop.ActionMarkLoadListOperationStatusToFinished
                                  : FlowStateDepartureFromStop.ActionIsProcessWasRelatedToOperation;
            ExecuteState(intialState);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateDepartureFromStop)state);

        }

        public void ExecuteState(FlowStateDepartureFromStop state)
        {
            if (state > FlowStateDepartureFromStop.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        private void ExecuteViewState(FlowStateDepartureFromStop state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowDepartureFromStop;" + state);

            try
            {
                switch (state)
                {
                    case FlowStateDepartureFromStop.ThisFlowComplete:
                        EndFlow();
                        break;
                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }
            catch (Exception e)
            {
                string errorMessage = "Unexpected error in FlowDepartureFromStop.ExecuteState(): " + e.Message;
                Logger.LogException(e, "FlowDepartureFromStop.ExecuteState");
                _actionCommands.FlowResult = new FlowResultDepartureFromStop { State = FlowResultState.Exception, Message = errorMessage };
                ExecuteState(FlowStateDepartureFromStop.ThisFlowComplete);
            }
        }

        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateDepartureFromStop state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowDepartureFromStop;" + state);
                    switch (state)
                    {
                        case FlowStateDepartureFromStop.ActionIsProcessWasRelatedToOperation:
                            bool result = _flowData.IsStartedFromOperationList;
                            state = result ? FlowStateDepartureFromStop.ActionIsOperationFinishedEventRequired
                                                  : FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionIsOperationFinishedEventRequired:
                            result = _actionCommands.IsOperationFinishEventRequired();
                            state = result ? FlowStateDepartureFromStop.ActionSendOperationFinishedEvent
                                                  : FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionMarkOperationStatusToFinished:
                            CommandsOperationList.MarkSelectedOperationStatus(ModelMain.SelectedOperationProcess, StopOperationType.Finished);
                            state = FlowStateDepartureFromStop.ActionPersistOperationListChanges;
                            break;

                        case FlowStateDepartureFromStop.ActionPersistOperationListChanges:
                            CommandsOperationList.PersistOperationListChanges();
                            state = FlowStateDepartureFromStop.ActionIsAllOperationsOnStopFinished;
                            break;

                        case FlowStateDepartureFromStop.ActionSendOperationFinishedEvent:
                            EventReportHelper.SendOperationFinishedEvent(ModelMain.SelectedOperationProcess, _flowData.LoadCarrier, _flowData.IsStartedFromOperationList, _flowData.Signature, _flowData.EntityMap);
                            state = FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionIsAllOperationsOnStopFinished:
                            //result = CommandsOperationList.IsAllOperationsOnStopFinished(ModelMain.SelectedOperationProcess.StopId);
                            state = FlowStateDepartureFromStop.ThisFlowComplete;
                            break;

                        case FlowStateDepartureFromStop.ActionIsStopSummaryMessageRequired:
                            result = _actionCommands.IsSummaryMessageRequired();
                            state = result ? FlowStateDepartureFromStop.ActionIsPromptForSummaryRequired
                                                  : FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionIsPromptForSummaryRequired:
                            result = _actionCommands.IsPromptForSummaryRequired();
                            state = result ? FlowStateDepartureFromStop.ActionShowUnloadingVerificationPopUp
                                                  : FlowStateDepartureFromStop.ActionTriggerSummaryMessage;
                            break;

                        case FlowStateDepartureFromStop.ActionShowUnloadingVerificationPopUp:
                            result = _actionCommands.ShowPopUpForUnloadingVerification();
                            state = result ? FlowStateDepartureFromStop.ActionTriggerSummaryMessage
                                                  : FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionTriggerSummaryMessage:
                            EventReportHelper.SendActualsSummaryMessage(ModelMain.SelectedOperationProcess.StopId, ModelMain.SelectedOperationProcess.TripId, _flowData.IsStartedFromOperationList, _flowData.EntityMap, _flowData.ActionType);
                            state = FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired;
                            break;

                        case FlowStateDepartureFromStop.ActionIsDepartureFromStopEventRequired:
                            result = _actionCommands.IsDepartureFromStopEventRequired();
                            state = result ? FlowStateDepartureFromStop.ActionSendDepartureFromStopEvent
                                                  : FlowStateDepartureFromStop.ActionMarkOperationStatusToFinished;
                            break;

                        case FlowStateDepartureFromStop.ActionSendDepartureFromStopEvent:
                            EventReportHelper.SendDepartureFromStopEvent(ModelMain.SelectedOperationProcess, _flowData.LoadCarrier, _flowData.IsStartedFromOperationList, _flowData.Signature);
                            state = FlowStateDepartureFromStop.ActionMarkOperationStatusToFinished;
                            break;
                        case FlowStateDepartureFromStop.ActionMarkLoadListOperationStatusToFinished:
                            //We are passing here stop id, because we placed consignment number in stopid
                            AdditionalCommands.UpdateLoadListOperationStatus(_flowData.EntityMap);
                            state = FlowStateDepartureFromStop.ThisFlowComplete;
                            break;
                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateDepartureFromStop.ViewCommands);
            }

            catch (Exception e)
            {
                string errorMessage = "Unexpected error in FlowDepartureFromStop.ExecuteState(): " + e.Message;
                Logger.LogException(e, "FlowDepartureFromStop.ExecuteState");
                _actionCommands.FlowResult = new FlowResultDepartureFromStop { State = FlowResultState.Exception, Message = errorMessage };
                state = FlowStateDepartureFromStop.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        /// <summary>
        /// This method, does cleanup action for the flow.
        /// </summary>
        public override void CleanupFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDepartureFromStop.CleanupFlow");
        }

        /// <summary>
        /// This method ends the FlowDepartureFromStop.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowDepartureFromStop.EndFlow");
            var flowResult = new FlowResultDepartureFromStop
                {
                    State = _actionCommands.FlowResult.State
                };
            BaseModule.EndSubFlow(flowResult);
        }
    }
}

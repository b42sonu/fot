﻿using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Storage;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop
{
    /// <summary>
    /// This class is all action commands that willbe used by, flowDepartureFromStop.
    /// </summary>
    public class ActionCommandsDepartureFromStop
    {
        public FlowResultDepartureFromStop FlowResult;

        #region Action Commands

        /// <summary>
        /// Default constructor for class.
        /// </summary>
        public ActionCommandsDepartureFromStop()
        {
            FlowResult = new FlowResultDepartureFromStop();
        }

        /// <summary>
        /// This function checks and returns true/false on the basis of that, according to TMI setting
        /// if OperationFinishedEvent required or not.
        /// </summary>
        public bool IsOperationFinishEventRequired()
        {
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsOperationEventFinishedRequired);
        }

        /// <summary>
        /// This function checks and returns true/false on the basis of that, according to TMI setting
        /// if SummaryMessage required or not.
        /// </summary>
        public bool IsSummaryMessageRequired()
        {
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsStopSummaryRequired);
        }

        
        /// <summary>   
        /// This function checks and returns true/false on the basis of that, according to TMI setting
        /// if DepartureFromStopEvent required or not.
        /// </summary>
        public bool IsDepartureFromStopEventRequired()
        {
            var tmiValue = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsSendingDepartureEventRequired);
            if (!tmiValue)
            {
                FlowResult.State = FlowResultState.Finished;
                FlowResult.Message = string.Empty;
            }
            return tmiValue;
        }
        //TODO:: Write Unit Test
        /// <summary>
        /// This function checks that current user is Driver or Hubworker. If driver then returns true else returns false.
        /// </summary>
        public bool IsPromptForSummaryRequired()
        {
           var tmiValue = SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsPromptForSummaryRequired);
            return tmiValue;
        }


        //TODO:: Write Unit Test
        /// <summary>
        /// This function shows a model up to user, for verifying that unloading is completed or not.
        /// </summary>
        public bool ShowPopUpForUnloadingVerification()
        {
            var boxResult = GuiCommon.ShowModalDialog(GlobalTexts.UnloadingCompleted, GlobalTexts.UnloadingVerification, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
            return boxResult == GlobalTexts.Yes;
        }

        //TODO :: Write Unit Test
        internal FlowDataDepartureFromStop.ActionTypes ActionTypeForSelectedOperation(PlannedOperationTypeOperationType operationType)
        {
            
            switch (operationType)
            {
                case PlannedOperationTypeOperationType.LOAD:
                case PlannedOperationTypeOperationType.LOAD_DISTRIBUTION:
                case PlannedOperationTypeOperationType.LOAD_LINE_HAUL:
                case PlannedOperationTypeOperationType.PICKUP:
                    return FlowDataDepartureFromStop.ActionTypes.LA;

                case PlannedOperationTypeOperationType.UNLOAD:
                case PlannedOperationTypeOperationType.UNLOAD_LINE_HAUL:
                case PlannedOperationTypeOperationType.UNLOAD_PICKUP:
                case PlannedOperationTypeOperationType.DELIVERY:
                    return FlowDataDepartureFromStop.ActionTypes.LO;

                default:
                    Logger.LogEvent(Severity.Error, "Unknown operation state encountered");
                    return FlowDataDepartureFromStop.ActionTypes.LO;
            }

        }

        /// <summary>
        /// This method checks that, if TMI settings says that started pick up should complete before, departing from
        /// stop or not.
        /// </summary>
        /// <returns>Specifies boolean value that specifies, that user needs to complete started pick up or not.</returns>
        public  bool IsCompletingOperationRequired()
        {
            Logger.LogEvent(Severity.Debug,
                            "Executing command ActionCommandsOperationListAmphora.IsCompletingOperationRequired()");
            return SettingDataProvider.Instance.GetTmiValue<bool>(TmiSettings.IsCompletingOperationRequired);
        }


        /// <summary>
        /// This method calls OnUpdateView method, on FormOperationListAmphora by passing form data, to show
        /// pop up for Departure Not Allowed, because pick up operations are pending.
        /// </summary>
        public void ShowDepartureNotAllowed()
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsOperationListAmphora.ShowDepartureNotAllowed()");
            GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.DepartureNotAllowed, Severity.Error, GlobalTexts.Ok);
        }

        /// <summary>
        /// This method calls OnUpdateView method, on FormOperationListAmphora by passing form data, to show
        /// pop up for the confirmation of departure.
        /// </summary>
        public FlowStateDepartureFromStop ShowDepartureConfirmation(PlannedOperationTypeOperationType operationType)
        {
            Logger.LogEvent(Severity.Debug, "Executing command ActionCommandsOperationListAmphora.ShowDepartureConfirmation()");
            var result = string.Empty;

            result = operationType == PlannedOperationTypeOperationType.DELIVERY
                         ? GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.DepartFromStop, Severity.Warning,
                                                     GlobalTexts.Yes, GlobalTexts.No, GlobalTexts.AttemptedDelivery)
                         : GuiCommon.ShowModalDialog(GlobalTexts.Warning, GlobalTexts.DepartFromStop, Severity.Warning,
                                                     GlobalTexts.Yes, GlobalTexts.No);
            if (result == GlobalTexts.Yes)
            {
                return ModelMain.SelectedOperationProcess.IsLoadListOperation
                           ? FlowStateDepartureFromStop.ActionMarkLoadListOperationStatusToFinished
                           : FlowStateDepartureFromStop.ActionIsOperationFinishedEventRequired;
            }
                
            return result == GlobalTexts.No
                       ? FlowStateDepartureFromStop.ThisFlowComplete
                       : FlowStateDepartureFromStop.FlowAttemptedDelivery;
        }

        #endregion




    }
}

﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTimeConsumption
{
    public enum FlowStateRegisterTimeConsumptionOnDelivery
    {
        ActionGetReasonActionCombinations,
        ActionSendEvent,
        ViewCommands,
        ViewRegisterConsignmentTime,
        ThisFlowCancelled,
        ThisFlowComplete,
    }

    public class FlowDataRegisterTimeConsumptionOnDelivery : BaseFlowData
    {
        public ConsignmentEntity ConsignmentEntity { get; set; }
    }

    public class FlowResultRegisterTimeConsumptionOnDelivery : BaseFlowResult
    {
        public MessageHolder MessageHolder;
    }

    // US 79: http://kp-confluence.postennorge.no/display/FOT/79+-+Register+time+consumption+on+delivery
    class FlowRegisterTimeConsumptionOnDelivery : BaseFlow
    {
        #region Private, Readonly and constants variables

        private readonly string _reasonCode = string.Empty;
        private string _actionCode = string.Empty;
        private readonly FlowResultRegisterTimeConsumptionOnDelivery _flowResult;
        private readonly ActionCommandsRegisterTimeConsumption _actionCommand;
        private readonly ViewCommandsRegisterTimeConsumption _viewCommands;
        private List<CauseMeasure> _reasonActionCombinations;
        public FlowDataRegisterTimeConsumptionOnDelivery FlowData { get; set; }
        private string _measureDesc = string.Empty;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public FlowRegisterTimeConsumptionOnDelivery()
        {
            _flowResult = new FlowResultRegisterTimeConsumptionOnDelivery();
            _actionCommand = new ActionCommandsRegisterTimeConsumption(CommunicationClient.Instance);
            _viewCommands = new ViewCommandsRegisterTimeConsumption();
            _reasonCode = "AJ";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="flowData"></param>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowRegisterTimeConsumptionVasSupport.BeginFlow");
            if (flowData != null)
            {
                FlowData = (FlowDataRegisterTimeConsumptionOnDelivery)flowData;
            }
            //  _orgUnitName = ModelUser.UserProfile.OrgUnitName;
            ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery.ActionGetReasonActionCombinations);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateRegisterTimeConsumptionOnDelivery)state);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        public void ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery state)
        {
            if (state > FlowStateRegisterTimeConsumptionOnDelivery.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        public void ExecuteViewState(FlowStateRegisterTimeConsumptionOnDelivery state)
        {
            Logger.LogEvent(Severity.Debug, "#FlowRegisterTimeConsumptionOnDelivery;" + state);
            switch (state)
            {
                case FlowStateRegisterTimeConsumptionOnDelivery.ViewRegisterConsignmentTime:
                    _viewCommands.ShowViewRegisterTimeConsumption(FormRegisterTimeConsumptionEventHandler, _reasonActionCombinations);
                    break;
                case FlowStateRegisterTimeConsumptionOnDelivery.ThisFlowCancelled:
                    _flowResult.State = FlowResultState.Cancel;
                    EndFlow();
                    break;
                case FlowStateRegisterTimeConsumptionOnDelivery.ThisFlowComplete:
                    _flowResult.State = FlowResultState.Ok;
                    EndFlow();
                    break;
            }


        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        private void ExecuteActionState(FlowStateRegisterTimeConsumptionOnDelivery state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowRegisterTimeConsumptionOnDelivery;" + state);
                switch (state)
                {
                    case FlowStateRegisterTimeConsumptionOnDelivery.ActionSendEvent:
                        _actionCommand.SendGoodsEvents(FlowData.ConsignmentEntity, ModelMain.SelectedOperationProcess, _reasonCode, _actionCode);
                        ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery.ThisFlowComplete);
                        break;
                    case FlowStateRegisterTimeConsumptionOnDelivery.ActionGetReasonActionCombinations:
                        _reasonActionCombinations = _actionCommand.GetReasonActionCombinations(_reasonCode);
                        ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery.ViewRegisterConsignmentTime);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowFlowStateRegisterTimeConsumption");
                ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery.ThisFlowComplete);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="registerTimeConsumptionViewEvents"></param>
        /// <param name="data"></param>
        private void FormRegisterTimeConsumptionEventHandler(int registerTimeConsumptionViewEvents, params object[] data)
        {
            switch (registerTimeConsumptionViewEvents)
            {
                case RegisterTimeConsumptionViewEvents.Back:
                    ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery.ThisFlowCancelled);
                    break;

                case RegisterTimeConsumptionViewEvents.Ok:
                    _actionCode = data[0].ToString();
                    _measureDesc = data[1].ToString();
                    ExecuteState(FlowStateRegisterTimeConsumptionOnDelivery.ActionSendEvent);
                    break;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowStateRegisterTimeConsumptionVasSupport.EndFlow");
            _flowResult.MessageHolder = new MessageHolder(_measureDesc + " " + GlobalTexts.IsRegistered, MessageState.Information);
            BaseModule.EndSubFlow(_flowResult);
        }

    }
}

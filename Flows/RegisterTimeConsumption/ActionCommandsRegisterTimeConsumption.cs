﻿using System;
using System.Linq;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using PreCom.Core.Communication;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTimeConsumption
{
    class ActionCommandsRegisterTimeConsumption
    {
        #region Private Variables
        private static ICommunicationClient _communicationClient;
        //private string goodsEventMessage;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comClient"></param>
        public ActionCommandsRegisterTimeConsumption(ICommunicationClient comClient)
        {
            _communicationClient = comClient;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationProcess"> </param>
        /// <param name="reasonCode"></param>
        /// <param name="actioCode"></param>
        /// <param name="consignmentEntity"> </param>
        public bool SendGoodsEvents(ConsignmentEntity consignmentEntity, OperationProcess operationProcess, string reasonCode, string actioCode)
        {
            bool isValid = false;
            PreComFW.CommunicationEntity.GoodsEvents.GoodsEvents goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity, null);

            if (goodsEvent != null)
            {
                if (goodsEvent.EventInformation != null)
                {
                    goodsEvent.EventInformation.ActionCode = actioCode;
                    goodsEvent.EventInformation.CauseCode = reasonCode;
                }
                var transaction = new Transaction(Guid.NewGuid(),
                                                  (byte)TransactionCategory.GoodsEventsEventInformation,
                                                  "FlowRegisterConsumptionTime");
                if (_communicationClient != null)
                {
                    _communicationClient.SendGoodsEvent(goodsEvent, transaction);
                    isValid = true;
                }
            }

            return isValid;
            //return goodsEventMessage;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CauseMeasure> GetReasonActionCombinations(string reasonCode)
        {
            var causeMeasureCombinations =
                SettingDataProvider.Instance.GetDistinctCombinationForReasonActionTypes(
                    ModelUser.UserProfile.RoleName, ModelUser.UserProfile.LanguageCode);
            if (causeMeasureCombinations != null)
                causeMeasureCombinations = (from n in causeMeasureCombinations where n.CauseCode == reasonCode select n).ToList<CauseMeasure>();

            return causeMeasureCombinations;
        }
    }
}

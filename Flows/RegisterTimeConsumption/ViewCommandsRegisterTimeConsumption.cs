﻿using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTimeConsumption
{
    class ViewCommandsRegisterTimeConsumption
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewEventHandler"></param>
        /// <param name="timeConsumptions"></param>
        public void ShowViewRegisterTimeConsumption(ViewEventDelegate viewEventHandler, List<CauseMeasure> timeConsumptions)
        {
             var formData = new FormDataRegisterTimeConsumption { ListTimeConsumptions = timeConsumptions};
            var view = ViewCommands.ShowView<FormRegisterTimeConsumption>(formData);
            view.SetEventHandler(viewEventHandler);
        }
    }
}

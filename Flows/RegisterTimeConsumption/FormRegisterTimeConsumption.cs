﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTimeConsumption
{
    public partial class FormRegisterTimeConsumption : BaseForm
    {
        private FormDataRegisterTimeConsumption _formData;
        /// <summary>
        /// 
        /// </summary>
        public FormRegisterTimeConsumption()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
        }
        /// <summary>
        /// 
        /// </summary>
        private void SetTextToGui()
        {
            labelModuleName.Text = GlobalTexts.RegisterTimeConsumption;
            labelInstructions.Text = GlobalTexts.SelectRegisterTimeInstruction;
            MultiButtonControl _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back,ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonType.Confirm));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();

            //labelModuleName.Text = GlobalTexts.RegisterTimeConsumption;
            //labelInstructions.Text = GlobalTexts.SelectRegisterTimeInstruction;
            //var tabButtons = new MultiButtonControl();
            //tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            //tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick));
            //touchPanel.Controls.Add(tabButtons);
            //tabButtons.GenerateButtons();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iformData"></param>
        public override void OnShow(IFormData iformData)
        {
            _formData = (FormDataRegisterTimeConsumption)iformData;
            if (_formData != null)
            {
                advancedListTime.DataSource = _formData.ListTimeConsumptions;

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(RegisterTimeConsumptionViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterTimeConsumption.ButtonBack");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                if (advancedListTime.SelectedRow == null)
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Attention, GlobalTexts.NoTimeConsumptionSelected, Severity.Warning, GlobalTexts.Ok);
                }
                else
                {
                    string actionCode = advancedListTime.SelectedRow["MeasureCode"].ToString();
                    string measureDesc = advancedListTime.SelectedRow["MeasureDescription"].ToString();
                    ViewEvent.Invoke(RegisterTimeConsumptionViewEvents.Ok, actionCode, measureDesc);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterTimeConsumption.ButtonConfirmClick");
            }

        }
    }

    public class FormDataRegisterTimeConsumption : BaseFormData
    {
        public List<CauseMeasure> ListTimeConsumptions { get; set; }

    }
    public class RegisterTimeConsumptionViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }
}
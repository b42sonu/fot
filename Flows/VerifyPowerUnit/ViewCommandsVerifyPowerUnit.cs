﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Model;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VerifyPowerUnit
{
    class ViewCommandsVerifyPowerUnit
    {

        internal static FormVerifyPowerUnitId ShowVerifyPowerUnitId(ViewEventDelegate viewEventHandler, string message, string headerText, VerificationTypes type)
        {
            var formData = new FormDataVerifyPowerUnitId
            {
                HeaderText = headerText,
                PowerUnitId = type == VerificationTypes.PowerUnitId ? ModelUser.UserProfile.PowerUnit : ModelUser.UserProfile.RouteId,
                Message = message,
                Type = type
            };

            var view = ViewCommands.ShowView<FormVerifyPowerUnitId>(formData);
            view.SetEventHandler(viewEventHandler);
            return view;
        }

    }
}

﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VerifyPowerUnit
{
    partial class FormVerifyPowerUnitId
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.messageControlBox = new MessageControl();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.lblMessage = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblPowerUnit = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtPowerUnitID=new PreComInput2();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).BeginInit();
           
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPowerUnit)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.touchPanel.Controls.Add(this.lblPowerUnit);
            this.touchPanel.Controls.Add(this.lblMessage);
            this.touchPanel.Controls.Add(this.lblHeading);
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.txtPowerUnitID);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.Controls.Add(this.messageControlBox);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }

            // 
            // _messageControlBox
            // 
            this.messageControlBox.Location = new System.Drawing.Point(6, 350);
            this.messageControlBox.Name = "messageControlBox";
            this.messageControlBox.Size = new System.Drawing.Size(468, 123);
            this.messageControlBox.TabIndex = 1;
            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = false;
            this.lblMessage.Location = new System.Drawing.Point(11, 105);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(446, 125);
            this.lblMessage.Text = "";
            this.lblMessage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // lblHeading
            // 
            this.lblHeading.Location = new System.Drawing.Point(11, 70);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(353, 29);
            this.lblHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            
            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial",9F,System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11,8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;
            // 
            // lblPowerUnit
            // 
            this.lblPowerUnit.Location = new System.Drawing.Point(11, 241);
            this.lblPowerUnit.Name = "lblPowerUnit";
            this.lblPowerUnit.Size = new System.Drawing.Size(157, 29);
            this.lblPowerUnit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            

            this.txtPowerUnitID.Location = new System.Drawing.Point(190, 235);
            this.txtPowerUnitID.Name = "txtPowerUnitId";
            this.txtPowerUnitID.Size = new System.Drawing.Size(200, 70);
            this.txtPowerUnitID.TabIndex = 0;
            this.txtPowerUnitID.TextTranslation = false;
            this.txtPowerUnitID.GotFocus += new System.EventHandler(TxtFieldGotFocus);
            this.txtPowerUnitID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            // 
            // FormVerifyPowerUnitId
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormVerifyPowerUnitId";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeading)).EndInit();
           
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPowerUnit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TransparentLabel lblHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblMessage;
        private Resco.Controls.CommonControls.TransparentLabel lblPowerUnit;
        private PreCom.Controls.PreComInput2 txtPowerUnitID;
        private MessageControl messageControlBox;
    }
}
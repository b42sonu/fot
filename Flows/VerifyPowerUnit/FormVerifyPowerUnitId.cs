﻿using System;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using System.Text.RegularExpressions;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.VerifyPowerUnit
{
    public enum VerificationTypes
    {
        PowerUnitId,
        RouteId
    }

    public partial class FormVerifyPowerUnitId : BaseForm
    {
        private const string TabButtonBackName = "Back";
        private const string TabButtonOkName = "Ok";
        private MultiButtonControl _tabButtons;
        private VerificationTypes _verificationType;
        public FormVerifyPowerUnitId()
        {
            InitializeComponent();
            SetTextToGui();
            
        }

        private void SetTextToGui()
        {
            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonBackName) { ButtonType = TabButtonType.Cancel });
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonOkName) { ButtonType = TabButtonType.Confirm });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
            if (ModelUser.UserProfile.OrgUnitName != null)
                lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            labelModuleName.Text = GlobalTexts.Loading;
        }


        private void InitOnScreenKeyBoardProperties(FormDataVerifyPowerUnitId data)
        {

            var virtualKepPadText = data.Type == VerificationTypes.PowerUnitId
                                           ? GlobalTexts.PowerUnit
                                           : GlobalTexts.RouteId;
            txtPowerUnitID.Tag = new OnScreenKeyboardProperties(virtualKepPadText, KeyPadTypes.QwertyUpperCase, KeyPadTypes.Numeric)
                                     {
                                         TextChanged = ScannedNumberTextChanged
                                     };
        }

        private void ScannedNumberTextChanged(bool userFinishedTyping, string textentered)
        {
            if (ModelUser.UserProfile.Role != Role.Driver2 && ModelUser.UserProfile.Role != Role.Landpostbud)
                _tabButtons.SetButtonEnabledState(TabButtonOkName, txtPowerUnitID.Text.Trim() != string.Empty);
        }

        

        /// <summary>
        /// This method will show the power unit Id in text field,that will come as input.
        /// </summary>
        /// <param name="data"></param>
        public override void OnShow(IFormData data)
        {
            var formData = (FormDataVerifyPowerUnitId)data;
            if (formData != null)
            {

                InitOnScreenKeyBoardProperties(formData);

                _verificationType = formData.Type;
                switch (formData.Type)
                {
                    case VerificationTypes.PowerUnitId:
                        lblPowerUnit.Text = GlobalTexts.PowerUnitId + ":";
                        lblHeading.Text = GlobalTexts.VerifyChangePowerUnit;
                        break;
                    case VerificationTypes.RouteId:
                        lblPowerUnit.Text = GlobalTexts.RouteId + ":";
                        lblHeading.Text = GlobalTexts.VerifyChangeRouteId;
                        break;
                }
                txtPowerUnitID.Text = formData.PowerUnitId;
                labelModuleName.Text = formData.HeaderText;
                if (formData.Message != null)
                    lblMessage.Text = formData.Message;
            }
            SetKeyboardState(KeyboardState.AlphaUpperCase);
        }

        /// <summary>
        /// This event will run when user will click on back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(VerifyPowerUnitIdEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormVerifyPowerUnitId.ButtonBack");
            }
        }

        /// <summary>
        /// This event will run when user will click on Ok button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {

                switch(_verificationType)
                {
                
                    case VerificationTypes.PowerUnitId:
                        if (string.IsNullOrEmpty(txtPowerUnitID.Text))
                        {
                            messageControlBox.Visible = true;
                            messageControlBox.ShowMessage(GlobalTexts.InvalidPowerUnitId, MessageState.Error);
                            return;
                        }
                        break;
                    case VerificationTypes.RouteId:

                        if (!string.IsNullOrEmpty(txtPowerUnitID.Text) && (!Regex.IsMatch(txtPowerUnitID.Text, @"\d{4}") || txtPowerUnitID.Text.Length != 4))
                        {
                            messageControlBox.Visible = true;
                            messageControlBox.ShowMessage(GlobalTexts.InvalidRouteId, MessageState.Error);
                            return;
                        }
                        break;
                }
                ViewEvent.Invoke(VerifyPowerUnitIdEvent.Ok, txtPowerUnitID.Text);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormVerifyPowerUnitId.ButtonOkClick");
            }
        }

        /// <summary>
        /// Every time when user will focus into test field for Power Unit Id, this
        /// event will run and shows the kep pad for entering power unit id.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtFieldGotFocus(object sender, EventArgs e)
        {
            try
            {
                messageControlBox.Visible = false;
                messageControlBox.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormVerifyPowerUnitId.TxtFieldGotFocus");
            }
        }
    }

    public class VerifyPowerUnitIdEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;

        public static implicit operator VerifyPowerUnitIdEvent(int viewEvent)
        {
            return new VerifyPowerUnitIdEvent { Value = viewEvent };
        }
    }

    public class FormDataVerifyPowerUnitId : BaseFormData
    {
        public string PowerUnitId { get; set; }
        public string Message { get; set; }
        public VerificationTypes Type { get; set; }
    }
}
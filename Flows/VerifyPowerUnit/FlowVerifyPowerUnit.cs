﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VerifyPowerUnit
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateVerifyPowerUnit
    {
        ViewCommands,
        ViewShowVerifyPowerUnit,
        ThisFlowComplete
    }

    public class FlowDataVerifyPowerUnit : BaseFlowData
    {
        public string Message { get; set; }
        public VerificationTypes Type { get; set; }
    }

    public class FlowResultVerifyPowerUnit : BaseFlowResult
    {
        public String PowerUnitId;
    }

    /// <summary>
    /// This class acts as a flow for scanning of barcode.
    /// </summary>
    public class FlowVerifyPowerUnit : BaseFlow
    {

        private string _powerUnitId = string.Empty;
        private FlowDataVerifyPowerUnit _flowData;
        private readonly FlowResultVerifyPowerUnit _flowResult;
        
        public FlowVerifyPowerUnit()
        {
            _flowResult = new FlowResultVerifyPowerUnit();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            _flowData = (FlowDataVerifyPowerUnit)flowData;

            Logger.LogEvent(Severity.Debug, "initialized flow FlowVerifyPowerUnit");
            ExecuteState(FlowStateVerifyPowerUnit.ViewShowVerifyPowerUnit);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateVerifyPowerUnit)state);
        }

        public void ExecuteState(FlowStateVerifyPowerUnit state)
        {
            if (state > FlowStateVerifyPowerUnit.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStateVerifyPowerUnit state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowResultVerifyPowerUnit;" + state);
                switch (state)
                {
                    case FlowStateVerifyPowerUnit.ViewShowVerifyPowerUnit:
                        ViewCommandsVerifyPowerUnit.ShowVerifyPowerUnitId(VerifyPowerUnitIdViewEventHandler, _flowData.Message, _flowData.HeaderText, _flowData.Type);
                        break;

                    case FlowStateVerifyPowerUnit.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowVerifyPowerUnit.ExecuteViewState");
                _flowResult.State=FlowResultState.Exception;
                EndFlow();
            }
        }



        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateVerifyPowerUnit state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowResultVerifyPowerUnit;" + state);
                    //check the stateReconcilliation and execute the same 
                    switch (state)
                    {
                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateVerifyPowerUnit.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowVerifyPowerUnit.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state= FlowStateVerifyPowerUnit.ThisFlowComplete;
            }

            ExecuteViewState(state);
        }


        public void VerifyPowerUnitIdViewEventHandler(int verifyPowerUnitIdEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowLoading.VerifyPowerUnitIdViewEventHandler");
            switch ((VerifyPowerUnitIdEvent)verifyPowerUnitIdEvent)
            {
                case VerifyPowerUnitIdEvent.Ok:
                    _powerUnitId = Convert.ToString(data[0]);
                    _flowResult.PowerUnitId = _powerUnitId;
                    _flowResult.State = FlowResultState.Ok;

                    ExecuteState(FlowStateVerifyPowerUnit.ThisFlowComplete);
                    break;

                case VerifyPowerUnitIdEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;

                    ExecuteState(FlowStateVerifyPowerUnit.ThisFlowComplete);
                    break;
            }
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowVerifyPowerUnit");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

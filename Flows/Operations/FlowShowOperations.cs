﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Operations
{
    /// <summary>
    /// Enum which specifies the collection of state for flow FlowShowOperations.
    /// </summary>
    public enum FlowStates
    {
        ViewCommands,
        ViewShowLoadingOperations,

        ThisFlowComplete
    }

    /// <summary>
    /// This class acts as initial data for flow, FlowShowOperations.
    /// </summary>
    public class FlowDataShowOperations : BaseFlowData
    {
        public List<Operation> Operations;
    }


    /// <summary>
    /// This class acts as , result from flow FlowShowOperations when it will come back to parent flow which
    /// initialized this flow.
    /// </summary>
    public class FlowResultShowOperations : BaseFlowResult
    {
        public Operation SelectedOperation { get; set; }
    }


    /// <summary>
    /// This class acts as a flow for displaying loading/unloading operations
    /// </summary>
    public class FlowShowOperations : BaseFlow
    {

        private FlowDataShowOperations _flowData;
        private readonly FlowResultShowOperations _flowResult;

        /// <summary>
        /// This is the default constructor for flow FlowShowOperations.
        /// </summary>
        public FlowShowOperations()
        {
            _flowResult = new FlowResultShowOperations();
        }


        /// <summary>
        /// This method initializes the flow with required initial data.
        /// </summary>
        /// <param name="flowData">Specifies the flow data supplied by parent flow.</param>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing FlowShowOperations");

            _flowData = (FlowDataShowOperations)flowData;

            ExecuteViewState(FlowStates.ViewShowLoadingOperations);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStates)state);
        }

        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowStates, which acts as name for next command.</param>
        public void ExecuteState(FlowStates state)
        {
            if (state > FlowStates.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        /// <summary>
        /// This method have responsibility to execute, view commands, flow commands.
        /// </summary>
        /// <param name="state">Specifies the state/command that need to execute.</param>
        private void ExecuteViewState(FlowStates state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowShowOperations;" + state);
                switch (state)
                {
                    case FlowStates.ViewShowLoadingOperations:
                        ViewCommandsShowOperations.ShowOperations(ShowOperationsViewEventHandler, _flowData.Operations);
                        break;

                    case FlowStates.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowShowLOperations.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStates.ThisFlowComplete);
            }
        }



        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStates state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowShowOperations;" + state);
                    switch (state)
                    {
                        case FlowStates.ViewShowLoadingOperations:
                            state = FlowStates.ViewShowLoadingOperations;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;

                    }
                } while (state < FlowStates.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowDataShowOperations.ExecuteActionState");
                _flowResult.State=FlowResultState.Exception;
                state = FlowStates.ThisFlowComplete;
             }

            ExecuteViewState(state);
        }

        /// <summary>
        /// This method acts as common event handler for, all evets raised from FormOperations. 
        /// screen.
        /// </summary>
        /// <param name="loadingOperationsEvent">Specifies the value from enum LoadingOperationsEvent which corresponds
        /// to event raised on screen.</param>
        /// <param name="data">Specifies list of parameters as supplied from, form.</param>
        public void ShowOperationsViewEventHandler(int loadingOperationsEvent, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowShowOperations.ShowOperationsViewEventHandler");
            switch ((LoadingOperationsEvent)loadingOperationsEvent)
            {
                case LoadingOperationsEvent.Ok:
                    _flowResult.State = FlowResultState.Ok;
                    var selectedOperationIndex = (int) data[0];

                    if(selectedOperationIndex > -1)
                    {
                        var selectedOperation = _flowData.Operations[selectedOperationIndex];
                        _flowResult.SelectedOperation = selectedOperation;
                        CommandsOperationList.UpdateOperationProcessFromOperation(selectedOperation);
                    }
                    ExecuteState(FlowStates.ThisFlowComplete);
                    break;

                case LoadingOperationsEvent.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStates.ThisFlowComplete);
                    break;

                case LoadingOperationsEvent.DifferentStop:
                    _flowResult.State = FlowResultState.RequestMoreOptions;
                    ExecuteState(FlowStates.ThisFlowComplete);
                    break;

                    
            }
        }

        


        
        /// <summary>
        /// This method, does cleanup action for the flow.
        /// </summary>
        public override void CleanupFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowShowOperations.CleanupFlow");
        }

        /// <summary>
        /// This method ends the FlowLoadList.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowShowOperations.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }

}

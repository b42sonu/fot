﻿using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Entity;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.Operations
{
    class ViewCommandsShowOperations
    {
        /// <summary>
        /// This method shows the screen FormDataEventVerification.
        /// </summary>
        /// <param name="viewEventHandler">Specifies the handler for different from screen FormDataEventVerification.</param>
        /// <param name="loadingOperations">Specifies the list of objects of type loadingOperations, which needs to show on this screen.</param>
        /// <returns>Returns object of type FormOperations.</returns>
        internal static FormOperations ShowOperations(ViewEventDelegate viewEventHandler, List<Operation> loadingOperations)
        {
            List<Operation> activeLoadingOperations = null;
            if (loadingOperations != null)
            {
                activeLoadingOperations = (from o in loadingOperations where o.Status != LoadingOperationStatus.Finished select o).ToList();
            }

            var formData = new FormDataOperations { LoadOperations = activeLoadingOperations };

            var view = ViewCommands.ShowView<FormOperations>();
            view.SetEventHandler(viewEventHandler);
            view.UpdateView(formData);
            return view;
        }
    }
}

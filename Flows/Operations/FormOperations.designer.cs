﻿using System.Drawing;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Controls;
using Com.Bring.PMP.PreComFW.Shared.Constants;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Operations
{
    partial class FormOperations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            new System.ComponentModel.ComponentResourceManager(typeof(FormOperations));
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.imageListOperationStatus = new System.Windows.Forms.ImageList() { ImageSize = new Size(20, 20) };//
            this.imageListOperationType = new System.Windows.Forms.ImageList() { ImageSize = new Size(40, 40) };//
            //this.btnDifferentStop = new Resco.Controls.OutlookControls.ImageButton();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.listOperations = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplateHeader = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCellHeaderTemplate = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplatePlannedOp = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCellOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.imageCellOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellOperationTripId = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplatePlannedOpAlternateTemp = new Resco.Controls.AdvancedList.RowTemplate();
            this.imageCellAlternateOperationType = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationDetail = new Resco.Controls.AdvancedList.TextCell();
            this.imageCellAlternameOperationStatus = new Resco.Controls.AdvancedList.ImageCell();
            this.textCellAlternateOperationOperationId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateOperationStopId = new Resco.Controls.AdvancedList.TextCell();
            this.textCellAlternateOperationTripId = new Resco.Controls.AdvancedList.TextCell();
            this.lblTripId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblStopId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblTripIdHead = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblStopIdHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOperationId = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOperationIdHead = new Resco.Controls.CommonControls.TransparentLabel();
            this.testBindingSource = new System.Windows.Forms.BindingSource(this.components);
            //((System.ComponentModel.ISupportInitialize)(this.btnDifferentStop)).BeginInit();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).BeginInit();
            this.SuspendLayout();

            this.imageListOperationType.Images.Clear();
            this.imageListOperationType.Images.Add(GuiCommon.Instance.Loading());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.Unloading());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.Delivery());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.LoadingLineHaulTruck());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.LoadDistributionTruck());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.UnloadLineHaul());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.UnloadPickUpTruck());
            this.imageListOperationType.Images.Add(GuiCommon.Instance.Pickup());
            this.imageListOperationStatus.Images.Add(GuiCommon.Instance.New());
            this.imageListOperationStatus.Images.Add(GuiCommon.Instance.Started());
            this.imageListOperationStatus.Images.Add(GuiCommon.Instance.Finished());


            // 
            // labelModuleName
            // 
            this.labelModuleName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(11, 8);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(459, 27);
            
            this.labelModuleName.TextAlignment = Resco.Controls.CommonControls.Alignment.BottomCenter;

            // 
            // btnDifferentStop
            // 
            //this.btnDifferentStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            //this.btnDifferentStop.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            //this.btnDifferentStop.ForeColor = System.Drawing.Color.White;
            //this.btnDifferentStop.Location = new System.Drawing.Point(161, 501);
            //this.btnDifferentStop.Name = "btnDifferentStop";
            //this.btnDifferentStop.Size = new System.Drawing.Size(158, 50);
            //this.btnDifferentStop.TabIndex = 2;
            //this.btnDifferentStop.Click += new System.EventHandler(OnClickDifferentStop);
            // 
            // touchPanel
            // 
            //this.touchPanel.Controls.Add(this.btnDifferentStop);
            this.touchPanel.Controls.Add(this.listOperations);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.lblTripId);
            this.touchPanel.Controls.Add(this.lblOperationId);
            this.touchPanel.Controls.Add(this.lblOperationIdHead);
            this.touchPanel.Controls.Add(this.lblStopId);
            this.touchPanel.Controls.Add(this.lblTripIdHead);
            this.touchPanel.Controls.Add(this.lblStopIdHeading);
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            // 
            // advancedListOperationList
            // 
            this.listOperations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listOperations.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listOperations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listOperations.DataRows.Clear();
            //this.listOperations.HeaderRow = new Resco.Controls.AdvancedList.HeaderRow(0, new string[] {
            //resources.GetString("advancedListOperationList.HeaderRow")});
            this.listOperations.Location = new System.Drawing.Point(0, 40);
            this.listOperations.MultiSelect = false;
            this.listOperations.Name = "advancedListOperationList";
            this.listOperations.ScrollbarSmallChange = 32;
            this.listOperations.ScrollbarWidth = 26;
            this.listOperations.SelectedTemplateIndex = 2;
            this.listOperations.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectOnly;
            this.listOperations.ShowHeader = true;
            this.listOperations.Size = new System.Drawing.Size(480, 300);
            this.listOperations.TabIndex = 0;
            this.listOperations.TemplateIndex = 1;
            this.listOperations.Templates.Add(this.rowTemplateHeader);
            this.listOperations.Templates.Add(this.rowTemplatePlannedOp);
            this.listOperations.Templates.Add(this.rowTemplatePlannedOpAlternateTemp);
            this.listOperations.TouchScrolling = true;
            this.listOperations.RowSelect += new Resco.Controls.AdvancedList.RowEventHandler(this.ListOperationRowSelect);
            // 
            // RowTemplateHeader
            // 
            this.rowTemplateHeader.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.rowTemplateHeader.CellTemplates.Add(this.textCellHeaderTemplate);
            this.rowTemplateHeader.Height = 0;
            this.rowTemplateHeader.Name = "RowTemplateHeader";
            // 
            // TextCellHeaderTemplate
            // 
            this.textCellHeaderTemplate.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellHeaderTemplate.CellSource.ConstantData = "Operation List";
            this.textCellHeaderTemplate.DesignName = "TextCellHeaderTemplate";
            this.textCellHeaderTemplate.Location = new System.Drawing.Point(0, 0);
            this.textCellHeaderTemplate.Size = new System.Drawing.Size(-1, 32);
            this.textCellHeaderTemplate.Visible = false;

            // 
            // RowTemplatePlannedOp
            // 
            this.rowTemplatePlannedOp.CellTemplates.Add(this.imageCellOperationType);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationDetail);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.imageCellOperationStatus);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationOperationId);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationStopId);
            this.rowTemplatePlannedOp.CellTemplates.Add(this.textCellOperationTripId);
            this.rowTemplatePlannedOp.Height = 64;
            this.rowTemplatePlannedOp.Name = "RowTemplatePlannedOp";
            // 
            // ImageCellOperationType
            // 
            this.imageCellOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellOperationType.AutoResize = true;
            this.imageCellOperationType.CellSource.ColumnName = "OperationType";
            this.imageCellOperationType.DesignName = "ImageCellOperationType";
            this.imageCellOperationType.ImageList = this.imageListOperationType;
            this.imageCellOperationType.Location = new System.Drawing.Point(15, 10);
            this.imageCellOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellOperationDetail
            // 
            this.textCellOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellOperationDetail.DesignName = "textCellOperationDetail";
            this.textCellOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // ImageCellOperationStatus
            // 
            this.imageCellOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellOperationStatus.CellSource.ColumnName = "Status";
            this.imageCellOperationStatus.DesignName = "ImageCellOperationStatus";
            this.imageCellOperationStatus.ImageList = this.imageListOperationStatus;
            this.imageCellOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.imageCellOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellOperationOperationId
            // 
            this.textCellOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellOperationOperationId.DesignName = "textCellOperationOperationId";
            this.textCellOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationOperationId.Visible = false;
            // 
            // textCellOperationStopId
            // 
            this.textCellOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellOperationStopId.DesignName = "textCellOperationStopId";
            this.textCellOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationStopId.Visible = false;
            // 
            // TextCellOperationOrderNumber
            // 
            this.textCellOperationTripId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellOperationTripId.CellSource.ColumnName = "TripId";
            this.textCellOperationTripId.DesignName = "TextCellOperationOrderNumber";
            this.textCellOperationTripId.Location = new System.Drawing.Point(0, 0);
            this.textCellOperationTripId.Size = new System.Drawing.Size(0, 64);
            this.textCellOperationTripId.Visible = false;
            // 
            // RowTemplatePlannedOpAlternateTemp
            // 
            this.rowTemplatePlannedOpAlternateTemp.BackColor = System.Drawing.Color.LightGray;
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.imageCellAlternateOperationType);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationDetail);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.imageCellAlternameOperationStatus);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationOperationId);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationStopId);
            this.rowTemplatePlannedOpAlternateTemp.CellTemplates.Add(this.textCellAlternateOperationTripId);
            this.rowTemplatePlannedOpAlternateTemp.Height = 64;
            this.rowTemplatePlannedOpAlternateTemp.Name = "RowTemplatePlannedOpAlternateTemp";
            // 
            // ImageCellAlternateOperationType
            // 
            this.imageCellAlternateOperationType.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellAlternateOperationType.AutoResize = true;
            this.imageCellAlternateOperationType.CellSource.ColumnName = "OperationType";
            this.imageCellAlternateOperationType.DesignName = "ImageCellAlternateOperationType";
            this.imageCellAlternateOperationType.ImageList = this.imageListOperationType;
            this.imageCellAlternateOperationType.Location = new System.Drawing.Point(15, 10);
            this.imageCellAlternateOperationType.Size = new System.Drawing.Size(40, 40);
            // 
            // textCellAlternateOperationDetail
            // 
            this.textCellAlternateOperationDetail.CellSource.ColumnName = "OperationDetail";
            this.textCellAlternateOperationDetail.DesignName = "textCellAlternateOperationDetail";
            this.textCellAlternateOperationDetail.Location = new System.Drawing.Point(57, 0);
            this.textCellAlternateOperationDetail.SelectedBackColor = System.Drawing.SystemColors.HighlightText;
            this.textCellAlternateOperationDetail.SelectedForeColor = System.Drawing.SystemColors.Highlight;
            this.textCellAlternateOperationDetail.Size = new System.Drawing.Size(370, 64);
            // 
            // ImageCellAlternameOperationStatus
            // 
            this.imageCellAlternameOperationStatus.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.imageCellAlternameOperationStatus.CellSource.ColumnName = "Status";
            this.imageCellAlternameOperationStatus.DesignName = "ImageCellAlternameOperationStatus";
            this.imageCellAlternameOperationStatus.ImageList = this.imageListOperationStatus;
            this.imageCellAlternameOperationStatus.Location = new System.Drawing.Point(430, 0);
            this.imageCellAlternameOperationStatus.Size = new System.Drawing.Size(-1, 64);
            // 
            // textCellAlternateOperationOperationId
            // 
            this.textCellAlternateOperationOperationId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationOperationId.CellSource.ColumnName = "OperationId";
            this.textCellAlternateOperationOperationId.DesignName = "textCellAlternateOperationOperationId";
            this.textCellAlternateOperationOperationId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationOperationId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationOperationId.Visible = false;
            // 
            // TextCellAlternateOperationStopId
            // 
            this.textCellAlternateOperationStopId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationStopId.CellSource.ColumnName = "StopId";
            this.textCellAlternateOperationStopId.DesignName = "TextCellAlternateOperationStopId";
            this.textCellAlternateOperationStopId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationStopId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationStopId.Visible = false;
            // 
            // TextCellAlternateOperationOrderNumber
            // 
            this.textCellAlternateOperationTripId.Alignment = Resco.Controls.AdvancedList.Alignment.MiddleCenter;
            this.textCellAlternateOperationTripId.CellSource.ColumnName = "OrderNumber";
            this.textCellAlternateOperationTripId.DesignName = "TextCellAlternateOperationOrderNumber";
            this.textCellAlternateOperationTripId.Location = new System.Drawing.Point(0, 0);
            this.textCellAlternateOperationTripId.Size = new System.Drawing.Size(0, 64);
            this.textCellAlternateOperationTripId.Visible = false;



            // 
            // lblTripIdHead
            // 
            //this.lblTripIdHead.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTripIdHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblTripIdHead.Location = new System.Drawing.Point(3, 340);
            this.lblTripIdHead.Name = "lblTripIdHead";
            this.lblTripIdHead.Size = new System.Drawing.Size(220, 40);
            
            // 
            // lblTripId
            // 
            //this.lblTripId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTripId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblTripId.Location = new System.Drawing.Point(150, 340);
            this.lblTripId.Name = "lblTripId";
            this.lblTripId.Size = new System.Drawing.Size(245, 40);
            this.lblTripId.Text = string.Empty;
            // 
            // lblStopIdHeading
            // 
            //this.lblStopIdHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStopIdHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblStopIdHeading.Location = new System.Drawing.Point(3, 380);
            this.lblStopIdHeading.Name = "lblStopIdHeading";
            this.lblStopIdHeading.Size = new System.Drawing.Size(148, 40);
            
            // 
            // lblStopId
            // 
            //this.lblStopId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStopId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblStopId.AutoSize = false;
            this.lblStopId.Location = new System.Drawing.Point(150, 380);
            this.lblStopId.Name = "lblStopId";
            this.lblStopId.Size = new System.Drawing.Size(322, 30);
           
            // 
            // lblOperationIdHead
            // 
            //this.lblStopIdHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblOperationIdHead.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.lblOperationIdHead.Location = new System.Drawing.Point(3, 420);
            this.lblOperationIdHead.Name = "lblOperationIdHead";
            this.lblOperationIdHead.Size = new System.Drawing.Size(172, 40);

            // 
            // lblOperationId
            // 
            //this.lblStopId.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblOperationId.Location = new System.Drawing.Point(150, 420);
            this.lblOperationId.Name = "lblOperationId";
            this.lblOperationId.AutoSize = false;
            this.lblOperationId.Size = new System.Drawing.Size(305, 70);
            this.lblOperationId.Text = string.Empty;
            //lblOperationId.AutoSize = false;


            // 
            // FormOperationList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormOperations";
            this.Size = new System.Drawing.Size(480, 552);
            //((System.ComponentModel.ISupportInitialize)(this.btnDifferentStop)).EndInit();
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.testBindingSource)).EndInit();
            this.ResumeLayout(false);

        }


        #endregion
        private System.Windows.Forms.BindingSource testBindingSource;

        private System.Windows.Forms.ImageList imageListOperationStatus;
        private System.Windows.Forms.ImageList imageListOperationType;

        //private Resco.Controls.OutlookControls.ImageButton btnDifferentStop;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.AdvancedList.AdvancedList listOperations;
        private Resco.Controls.CommonControls.TransparentLabel lblTripId;
        private Resco.Controls.CommonControls.TransparentLabel lblStopId;
        private Resco.Controls.CommonControls.TransparentLabel lblOperationId;
        private Resco.Controls.CommonControls.TransparentLabel lblTripIdHead;
        private Resco.Controls.CommonControls.TransparentLabel lblStopIdHeading;
        private Resco.Controls.CommonControls.TransparentLabel lblOperationIdHead;

        private Resco.Controls.AdvancedList.RowTemplate rowTemplateHeader;
        private Resco.Controls.AdvancedList.TextCell textCellHeaderTemplate;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOp;
        private Resco.Controls.AdvancedList.ImageCell imageCellOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellOperationDetail;
        private Resco.Controls.AdvancedList.ImageCell imageCellOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellOperationTripId;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplatePlannedOpAlternateTemp;
        private Resco.Controls.AdvancedList.ImageCell imageCellAlternateOperationType;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationDetail;
        private Resco.Controls.AdvancedList.ImageCell imageCellAlternameOperationStatus;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationOperationId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationStopId;
        private Resco.Controls.AdvancedList.TextCell textCellAlternateOperationTripId;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;





    }
}
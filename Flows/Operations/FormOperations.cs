﻿using System;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.AdvancedList;
using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.Operations
{
    /// <summary>
    /// This class acts as initial data for screen FormOperations.
    /// </summary>
    public class FormDataOperations : BaseFormData
    {
        public List<Operation> LoadOperations { get; set; }
    }


    /// <summary>
    /// This class, shows the list of Loading/Unloading operations, depending on for
    /// which operation type screen is opened.
    /// </summary>
    public partial class FormOperations : BaseForm
    {

        #region Private, readonly and constant variables
        private int _selectedOperationIndex = -1;
        private FormDataOperations _formDataEventVerification;
        readonly MultiButtonControl _multiButtonControl = new MultiButtonControl();
        private const string ButtonBack = "ButtonBack";
        private const string ButtonScan = "ButtonScan";
        private const string ButtonDiffStop = "DifferentStop";

        #endregion

        #region Methods and Functions

        /// <summary>
        /// Default constructor for class.
        /// </summary>
        public FormOperations()
        {
            InitializeComponent();
            SetTextToGui();
        }

        private void SetTextToGui()
        {
            //btnBack.Text = GlobalTexts.Back;
            //btnScan.Text = GlobalTexts.Scan;
            labelModuleName.Text = GlobalTexts.Operations;
            //btnDifferentStop.Text = GlobalTexts.DifferentStop;
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.DifferentStop, OnClickDifferentStop) { Name = ButtonDiffStop });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Scan, OnClickScan, TabButtonType.Confirm) { Name = ButtonScan });
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();

            if (BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer)
            {
                lblTripIdHead.Text = GlobalTexts.BookingRef + ":";
                lblStopIdHeading.Text = GlobalTexts.Consignor + ":";
                lblOperationIdHead.Text = GlobalTexts.Address + ":";
            }
            else
            {
                lblTripIdHead.Text = GlobalTexts.TripId + ":";
                lblStopIdHeading.Text = GlobalTexts.StopId + ":";
                lblOperationIdHead.Text = GlobalTexts.OperationId + ":";
            }
        }

        /// <summary>
        /// This method refreshes the existing list of operation, with specified list of operations
        /// </summary>
        /// <param name="formData">Specifies object for FormDataOperations class.</param>
        public override void OnUpdateView(IFormData formData)
        {
            if (formData != null)
            {
                _formDataEventVerification = (FormDataOperations)formData;
                var loadingOperations = _formDataEventVerification.LoadOperations;
                if (loadingOperations == null || loadingOperations.Count == 0)
                {
                    ClearOperationDetail();
                }
                BindOperations(loadingOperations);

                //listOperations.SelectionMode = SelectionMode.SelectOnly;
                listOperations.MultiSelect = false;

                if (listOperations.DataRows.Count == 0)
                    _multiButtonControl.SetButtonEnabledState(ButtonScan, false);

                _multiButtonControl.BeginUpdate();
                //btnDifferentStop.Visible = BaseModule.CurrentFlow.CurrentProcess != Process.DeliveryToCustomer;
                if (BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer)
                    _multiButtonControl.SetButtonEnabledState(ButtonDiffStop, false);
                //btnDifferentStop.Visible = false;
                else
                    _multiButtonControl.SetButtonEnabledState(ButtonDiffStop, true);
                //btnDifferentStop.Visible = true;

                _multiButtonControl.EndUpdate();
            }
        }

        /// <summary>
        /// This method shows the TripId and StopId for selected operation.
        /// </summary>
        /// <param name="e"></param>
        private void ShowOperationDetail(RowEventArgs e)
        {
            if (BaseModule.CurrentFlow.CurrentProcess == Process.DeliveryToCustomer)
            {
                string operationId = Convert.ToString(e.DataRow["OperationId"]);
                if (_formDataEventVerification != null && _formDataEventVerification.LoadOperations != null && string.IsNullOrEmpty(operationId) == false)
                {
                    var operationDetail = (from n in _formDataEventVerification.LoadOperations
                                           where n.OperationId == operationId
                                           select n).FirstOrDefault<Operation>();

                    if (operationDetail != null)
                    {
                        lblTripId.Text = operationDetail.OrderNumber;
                        lblStopId.Text = operationDetail.ConsignorName;
                        lblOperationId.Text = operationDetail.ConsignorFullAddress;
                    }
                }
            }
            else
            {
                string stopId = Convert.ToString(e.DataRow["StopId"]);
                lblTripId.Text = Convert.ToString(e.DataRow["TripId"]);
                lblStopId.Text = stopId.Length > 10 ? stopId.Substring(10) : stopId;
                lblOperationId.Text = Convert.ToString(e.DataRow["OperationId"]);
            }
        }

        /// <summary>
        /// This method clears the TripId and StopId details at the bottom.
        /// </summary>
        private void ClearOperationDetail()
        {
            lblTripId.Text = string.Empty;
            lblStopId.Text = string.Empty;
            lblOperationId.Text = string.Empty;
        }

        /// <summary>
        /// This method binds the specified list of operations to advance list
        /// </summary>
        private void BindOperations(List<Operation> loadingOperations)
        {
            listOperations.BeginUpdate();
            listOperations.DataSource = loadingOperations;
            listOperations.EndUpdate();
        }

        /// <summary>
        /// This method runs when user clicks any operation in list of operations. it deselects
        /// all rows in list of operations.
        /// </summary>
        private void DeSelectOtherRows()
        {
            for (var i = 0; i <= listOperations.DataRows.Count - 1; i++)
            {
                var row = listOperations.DataRows[i];
                row.Selected = false;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// This event runs every time user selects any row in list of operations and processes the
        /// selected row.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListOperationRowSelect(object sender, RowEventArgs e)
        {
            try
            {
                switch (e.DataRow.CurrentTemplateIndex)
                {
                    case 1: // The master row has to be collapsed
                        _multiButtonControl.SetButtonEnabledState(ButtonScan, false);
                        break;
                    case 2: // The master row has to be expanded  
                        ProcessSelectedRow(e);
                        _multiButtonControl.SetButtonEnabledState(ButtonScan, true);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormOperations.ListOperationRowSelect");
            }
        }

        /// <summary>
        /// This method does the following:
        /// 1) Deselect all other rows.
        /// 2) Selects the clicked row.
        /// 3) Get the stopId, TripId and OperationId from selected row.
        /// 4) Shows the operation detail at the bottom.
        /// </summary>
        /// <param name="e"></param>
        private void ProcessSelectedRow(RowEventArgs e)
        {
            DeSelectOtherRows();
            e.DataRow.Selected = true;
            _selectedOperationIndex = e.RowIndex;
            ShowOperationDetail(e);
        }


        /// <summary>
        /// This event runs when user clicks on back button and send message to flow that
        /// user clicked back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadingOperationsEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormOperations.ButtonBackClick");
            }
        }

        /// <summary>
        /// This event runs when user clicks on scan button and send message to flow that user clicked on
        /// scan button and passes parameters that are required by flow to handle the click of scan button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickScan(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadingOperationsEvent.Ok, _selectedOperationIndex);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormOperation.OnClickScan");
            }
        }

        /// <summary>
        /// This event runs when user clicks on different stop button and passes message to flow that user clicked 
        /// on Defferent stop button.
        /// </summary> 
        /// /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClickDifferentStop(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(LoadingOperationsEvent.DifferentStop);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormOperations.OnClickDifferentStop");
            }
        }
        #endregion

    }

    /// <summary>
    /// This class is having list of constants, corresponding to each event that can be raised on this
    /// screen, these constants will be used by flow to check which type of event occured on screen.
    /// </summary>
    public class LoadingOperationsEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;
        public const int DifferentStop = 2;


        public static implicit operator LoadingOperationsEvent(int viewEvent)
        {
            return new LoadingOperationsEvent { Value = viewEvent };
        }
    }


}
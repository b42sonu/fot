﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Views
{
    partial class FormDisplayCorrectWeightAndVolume
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
       
            this.columnConsignmentBool = new Resco.Controls.SmartGrid.Column();
            this.columnConsignmentValueString = new Resco.Controls.SmartGrid.Column();
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.btnDelete = new Resco.Controls.OutlookControls.ImageButton();
            this.btnCorrectItem = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCorrectItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.btnDelete);
            this.touchPanel.Controls.Add(this.btnCorrectItem);
            this.touchPanel.Controls.Add(this.labelModuleName);
            
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480,552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;

            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(322,501);
            this.btnOk.Text = GlobalTexts.Ok;
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(158, 50);
            this.btnOk.TabIndex = 5;
            this.btnOk.Click += new EventHandler(this.ButtonOk); 

          
            // 
            // Correct item
            // 
            this.btnCorrectItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCorrectItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnCorrectItem.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnCorrectItem.ForeColor = System.Drawing.Color.White;
            this.btnCorrectItem.Location = new System.Drawing.Point(161, 501);
            this.btnCorrectItem.Name = "btnCorrectItem";
            this.btnCorrectItem.Size = new System.Drawing.Size(158, 50);
            this.btnCorrectItem.TabIndex = 3;
            this.btnCorrectItem.Text = GlobalTexts.CorrectItem;
            this.btnCorrectItem.Click += new EventHandler(this.ButtonCorectItem);

            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(0, 501);
            this.btnDelete.Text = GlobalTexts.Delete;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(158, 50);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Click += new EventHandler(this.ButtonDelete); 
            // 
            // labelModuleName
            // 
            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(480, 30);
            this.labelModuleName.TextAlignment=Alignment.MiddleCenter;
            this.labelModuleName.Text = GlobalTexts.CorrectWeightVolume;

            // lblConsignmentNumber
            // 
            this.lblConsignmentNumber.Location = new System.Drawing.Point(12, 70);
            this.lblConsignmentNumber.Name = "lblConsignmentNumber";
            this.lblConsignmentNumber.Size = new System.Drawing.Size(100, 96);
            this.lblConsignmentNumber.Text = "Correct Weight for";
            this.lblConsignmentNumber.Visible = false;
            //  
            // columnConsignmentBool
            // 
            this.columnConsignmentBool.CellEdit = Resco.Controls.SmartGrid.CellEditType.CheckBox;
            this.columnConsignmentBool.DataMember = "IsChecked";
            this.columnConsignmentBool.GridLine = false;
            this.columnConsignmentBool.MinimumWidth = 10;
            this.columnConsignmentBool.Name = "IsChecked";
            this.columnConsignmentBool.Width = 30;
            // 
            // columnConsignmentValueString
            // 
            this.columnConsignmentValueString.DataMember = "ConsignmentEntity";
            this.columnConsignmentValueString.MinimumWidth = 10;
            this.columnConsignmentValueString.Name = "ConsignmentEntityId";
            this.columnConsignmentValueString.Width = 405;
            // 
            // FormCorrectWeightAndVolume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormDisplayCorrectWeightAndVolume";
            this.Text = "FormDisplayCorrectWeightAndVolume";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCorrectItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnCorrectItem;
        private Resco.Controls.OutlookControls.ImageButton btnDelete;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentNumber;
        private Resco.Controls.SmartGrid.Column columnConsignmentBool;
        private Resco.Controls.SmartGrid.Column columnConsignmentValueString;
    }
}
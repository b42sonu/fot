﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using PreCom.Utils;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Views
{
    partial class FormRegisterWeightAndVolume
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.btnNewRegistration = new Resco.Controls.OutlookControls.ImageButton();
            this.btnOk = new Resco.Controls.OutlookControls.ImageButton();
            this.btnBack = new Resco.Controls.OutlookControls.ImageButton();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblConsignmentNumber = new Resco.Controls.CommonControls.TransparentLabel();
            this.radioBtnConsignmentItem = new System.Windows.Forms.RadioButton();
            this.radioBtnConsignment = new System.Windows.Forms.RadioButton();
            this.lblNumberOfItems = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnNewRegistration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblNumberOfItems);
            this.touchPanel.Controls.Add(this.radioBtnConsignment);
            this.touchPanel.Controls.Add(this.radioBtnConsignmentItem);
            this.touchPanel.Controls.Add(this.lblConsignmentNumber);
            this.touchPanel.Controls.Add(this.btnNewRegistration);
            this.touchPanel.Controls.Add(this.btnOk);
            this.touchPanel.Controls.Add(this.btnBack);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480,552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            //this.touchPanel.GotFocus += new System.EventHandler(this.touchPanel_GotFocus);
            // 
            // btnNewRegistration
            // 
            this.btnNewRegistration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNewRegistration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnNewRegistration.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.btnNewRegistration.ForeColor = System.Drawing.Color.White;
            this.btnNewRegistration.Location = new System.Drawing.Point(161,501);
            this.btnNewRegistration.Name = "btnNewRegistration";
            this.btnNewRegistration.Size = new System.Drawing.Size(158, 50);
            this.btnNewRegistration.TabIndex = 6;
            this.btnNewRegistration.Text = "New registration";
            this.btnNewRegistration.Click += new System.EventHandler(this.ButtonNewRegistrationClick);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnOk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnOk.ForeColor = System.Drawing.Color.White;
            this.btnOk.Location = new System.Drawing.Point(322,501);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(158, 50);
            this.btnOk.TabIndex = 5;
            this.btnOk.Click += new System.EventHandler(this.ButtonOkClick);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.btnBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.btnBack.ForeColor = System.Drawing.Color.White;
            this.btnBack.Location = new System.Drawing.Point(0, 501);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(158, 50);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // labelModuleName
            // 

            this.labelModuleName.AutoSize = false;
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(0, 2);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(480, 30);
            this.labelModuleName.TextAlignment = Alignment.MiddleCenter;
            this.labelModuleName.Text = GlobalTexts.CorrectWeightVolume;

            
            // lblConsignmentNumber
            // 
            this.lblConsignmentNumber.Location = new System.Drawing.Point(12, 70+25);
            this.lblConsignmentNumber.Name = "lblConsignmentNumber";
            this.lblConsignmentNumber.Size = new System.Drawing.Size(100, 96);
            this.lblConsignmentNumber.Text = "Correct Weight for";
            // radioBtnConsignmentItem
            // 
            this.radioBtnConsignmentItem.Location = new System.Drawing.Point(270, 55);
            this.radioBtnConsignmentItem.Name = "radioBtnConsignmentItem";
            this.radioBtnConsignmentItem.Size = new System.Drawing.Size(180, 30);
            this.radioBtnConsignmentItem.TabIndex = 29;
            this.radioBtnConsignmentItem.Text = GlobalTexts.Item;
            this.radioBtnConsignmentItem.BackColor = System.Drawing.SystemColors.InactiveCaption;
            //this.radioBtnConsignmentItem.CheckedChanged += new System.EventHandler(this.RadioBtnsEntititypeCheckChanged);

            // 
            // radioBtnConsignment
            // 
            this.radioBtnConsignment.Location = new System.Drawing.Point(270, 99);
            this.radioBtnConsignment.Name = "radioBtnConsignment";
            this.radioBtnConsignment.Size = new System.Drawing.Size(180, 30);
            this.radioBtnConsignment.TabIndex = 30;
            this.radioBtnConsignment.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.radioBtnConsignment.Text = GlobalTexts.Consignment;
            this.radioBtnConsignment.CheckedChanged += new System.EventHandler(this.RadioBtnsEntititypeCheckChanged);
            // 
            // lblNumberOfItems
            // 
            this.lblNumberOfItems.AutoSize = false;
            this.lblNumberOfItems.Location = new System.Drawing.Point(21, 415);
            this.lblNumberOfItems.Name = "lblNumberOfItems";
            this.lblNumberOfItems.Size = new System.Drawing.Size(438,30);
            this.lblNumberOfItems.Text = "Number of items in shipment";
            // 
            // FormCorrectWeightAndVolume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormRegisterWeightAndVolume";
            this.Text = "FormRegisterWeightAndVolume";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnNewRegistration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.OutlookControls.ImageButton btnBack;
        private Resco.Controls.OutlookControls.ImageButton btnNewRegistration;
        private Resco.Controls.OutlookControls.ImageButton btnOk;
        private Resco.Controls.CommonControls.TransparentLabel lblConsignmentNumber;
        private Resco.Controls.CommonControls.TransparentLabel lblNumberOfItems;
        private System.Windows.Forms.RadioButton radioBtnConsignment;
        private System.Windows.Forms.RadioButton radioBtnConsignmentItem;
    }
}
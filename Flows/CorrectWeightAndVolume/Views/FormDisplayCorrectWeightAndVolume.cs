﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Model;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Views
{
    public partial class FormDisplayCorrectWeightAndVolume : BaseForm
    {
        private FormDataCorrectWeightAndVolume _formDataCorrectWeightAndVolume;
        private readonly ListBox _listBoxForEntityId;
        private readonly TransparentLabel _labelWeight;
        private readonly TransparentLabel _labelLength;
        private readonly TransparentLabel _labelWidth;
        private readonly TransparentLabel _labelHeight;
        private readonly TransparentLabel _labelVolume;
        private readonly TouchPanel _volumePanel;
        private readonly TouchPanel _sizePanel;
        private readonly TouchPanel _weightPanel;
        List<String> _listOfChangedEntities;

        private const string correctItemButton = "correctItem";
        private const string DeleteButton = "deleteButton";
        private const string OkButton = "buttonOk";
        private const string newRegistrationButton = "buttonNewRegistration";
        private readonly MultiButtonControl _multiButtonControl;

        public FormDisplayCorrectWeightAndVolume()
        {
            InitializeComponent();

            AddOrgUnitControl();
            btnOk.Visible = false;
            btnDelete.Visible = false;
            btnCorrectItem.Visible = false;
            _multiButtonControl = new MultiButtonControl();
            SetTextToControls();

            _listBoxForEntityId = new ListBox
                                     {
                                         Location = new Point(20, 50 + 50),
                                         Size = new Size(439, 200),
                                     };
            _listBoxForEntityId.SelectedIndexChanged += ListBoxForEntityIdSelectedIndexChanged;
            touchPanel.Controls.Add(_listBoxForEntityId);

            _volumePanel = new TouchPanel
            {
                Size = new Size(439, 90),
                Location = new Point(20, 290 + 50),
            };

            _sizePanel = new TouchPanel
            {
                Size = _volumePanel.Size,
                Location = _volumePanel.Location,
            };
            _weightPanel = new TouchPanel
            {
                Size = new Size(439, 30),
                Location = new Point(20, 260 + 50),
            };

            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
                _volumePanel.BackgroundImage = image;
                _sizePanel.BackgroundImage = image;
                _weightPanel.BackgroundImage = image;
            }

            _labelWeight = CreateMeasureLabels(GlobalTexts.Weight+Colon, 0, WeightUnitType.KG.ToString(), _weightPanel);
            _labelVolume = CreateMeasureLabels(GlobalTexts.Volume + Colon, 0, VolumeUnitType.DM3.ToString(), _volumePanel);
            _labelLength = CreateMeasureLabels(GlobalTexts.Length + Colon, 0, HLWUnitType.CM.ToString(), _sizePanel);
            _labelWidth = CreateMeasureLabels(GlobalTexts.Width + Colon, 30, HLWUnitType.CM.ToString(), _sizePanel);
            _labelHeight = CreateMeasureLabels(GlobalTexts.Height + Colon, 60, HLWUnitType.CM.ToString(), _sizePanel);
            touchPanel.Controls.Add(_sizePanel);
            touchPanel.Controls.Add(_volumePanel);
            touchPanel.Controls.Add(_weightPanel);
        }



        void ListBoxForEntityIdSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (_listBoxForEntityId.SelectedIndex < 0) return;
                if (_listOfChangedEntities == null)return;
                foreach (var weightAndVolumeData in _formDataCorrectWeightAndVolume.ConsignmentDataList)
                {
                    if (weightAndVolumeData.ConsignmentEntity.EntityId == _listOfChangedEntities[_listBoxForEntityId.SelectedIndex])
                        _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData = weightAndVolumeData;

                }
                if (_formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData != null &&
                    _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.Measures != null)
                {
                    var measures = _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.Measures;
                    _labelVolume.Text = measures.VolumeDm3.ToString(CultureInfo.InvariantCulture);
                    _labelHeight.Text = measures.HeightCm.ToString(CultureInfo.InvariantCulture);
                    _labelLength.Text = measures.LengthCm.ToString(CultureInfo.InvariantCulture);
                    _labelWidth.Text = measures.WidthCm.ToString(CultureInfo.InvariantCulture);
                    _labelWeight.Text = measures.WeightKg.ToString(CultureInfo.InvariantCulture);
                }
                if (_formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData != null && _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity is Consignment)
                {
                    _volumePanel.BringToFront();
                }
                else
                {
                    _sizePanel.BringToFront();
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormDisplayCorrectWieghtAndVolume.ListBoxForEntityIdSelectedIndexChanged");
            }
        }

         private void SetTextToControls()
         {
             _multiButtonControl.Clear();
             _multiButtonControl.ListButtons.Clear();
             _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Delete, ButtonDelete, TabButtonType.Cancel) { Name = DeleteButton });
             _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.NewRegistration, ButtonNewRegistrationClick) { Name = newRegistrationButton });
             _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOk, TabButtonType.Confirm) { Name = OkButton });
             _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.CorrectItem, ButtonCorectItem) { Name = correctItemButton });
             touchPanel.Controls.Add(_multiButtonControl);
             _multiButtonControl.GenerateButtons();
         }

        public override void OnShow(IFormData formData)
        {
            try
            {
            _formDataCorrectWeightAndVolume = (FormDataCorrectWeightAndVolume)formData;
            //var correctItemBtnLocation = new Point(161, 501);
            //var deleteBtnLocation = new Point(0, 501);
           
            if (_formDataCorrectWeightAndVolume.IsStartedByDoublescan)
            {
                //btnDelete.Location = correctItemBtnLocation;
                //btnCorrectItem.Location = deleteBtnLocation;
                //btnDelete.Text = string.Empty;
                //btnDelete.Enabled = false;
                _multiButtonControl.SetButtonEnabledState(DeleteButton, false);
                _multiButtonControl.SetButtonEnabledState(newRegistrationButton, false);
            }
            else
            {
                //btnDelete.Location = deleteBtnLocation;
                //btnCorrectItem.Location = correctItemBtnLocation;
                //btnDelete.Text = GlobalTexts.Delete;
                //btnDelete.Enabled = true;
                _multiButtonControl.SetButtonEnabledState(DeleteButton, true);
                _multiButtonControl.SetButtonEnabledState(newRegistrationButton, true);
            }
            if (_formDataCorrectWeightAndVolume == null) return;
            _listBoxForEntityId.DataSource = null;
                _listOfChangedEntities = new List<String>();
            foreach (var correctWeightAndVolumeData in _formDataCorrectWeightAndVolume.ConsignmentDataList)
            {
                if (correctWeightAndVolumeData.Measures.Changed) _listOfChangedEntities.Add(correctWeightAndVolumeData.ConsignmentEntity.EntityId);
            }
            _listBoxForEntityId.BeginUpdate();
            _listBoxForEntityId.Items.Clear();
            if (_listOfChangedEntities.Count > 0)
            {
                _listBoxForEntityId.DataSource = _listOfChangedEntities;
                _listBoxForEntityId.EndUpdate();
                _listBoxForEntityId.SelectedIndex = 0;
            }
            else
            {
                _labelWeight.Text = "0";
                _labelVolume.Text = "0";
                _labelLength.Text = "0";
                _labelWidth.Text = "0";
                _labelHeight.Text = "0";
            }
            _listBoxForEntityId.Hide();
            _listBoxForEntityId.Show();
            //btnCorrectItem.Enabled = true;
            _multiButtonControl.SetButtonEnabledState(correctItemButton, true);
           // btnCorrectItem.Text = GlobalTexts.CorrectItem;                    
            Refresh();
            _listBoxForEntityId.Focus();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "OnShow.FormDisplayCorrectWeightAndVolume");
            }
        }


        private TransparentLabel CreateMeasureLabels(string legend, int y, string unit, TouchPanel pnl)
        {
            if (pnl == null) pnl = touchPanel;

            var lblLegend = new TransparentLabel
            {
                Location = new Point(15, y),
                Text = legend
            };
            pnl.Controls.Add(lblLegend);

            var label = new TransparentLabel
            {
                Location = new Point(176, y),
                Size = new Size(117, 21)
            };


            pnl.Controls.Add(label);


            var lblUnit = new TransparentLabel
            {
                Location =
                    new Point(299, y),
                Text = unit
            };
            pnl.Controls.Add(lblUnit);
            return label;
        }

        private void ButtonOk(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormDisplayWeightAndVolumeEvents.Ok);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEventVerification.ButtonBackClick");
            }
        }

        private void ButtonCorectItem(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormDisplayWeightAndVolumeEvents.CorrectItem);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEventVerification.ButtonBackClick");
            }
        }

        private void ButtonNewRegistrationClick(object sender, EventArgs e)
        {
            try
            {

                ViewEvent.Invoke(FormDisplayWeightAndVolumeEvents.NewRegistration, _formDataCorrectWeightAndVolume);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterWeightVolume.ButtonNewRegistrationClick");
            }
        }

        private void ButtonDelete(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormDisplayWeightAndVolumeEvents.Delete);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormEventVerification.ButtonDelete");
            }
        }
        private void AddOrgUnitControl()
        {
            var lblOrgUnit = new TransparentLabel
            {
                AutoSize = false,
                Size = new Size(480, 33),
                Location = new Point(0, 35),
                Text = ModelUser.UserProfile.OrgUnitName,
                TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter,
                Font = new Font("Arial", 9F, FontStyle.Bold),
            };
            touchPanel.Controls.Add(lblOrgUnit);
        }
    }

    public class FormDisplayWeightAndVolumeEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int CorrectItem = 1;
        public const int NewRegistration = 2;
        public const int Delete = 3;
    }


}
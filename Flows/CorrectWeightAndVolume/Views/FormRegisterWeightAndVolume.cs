﻿using System;
using System.Drawing;
using System.Globalization;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Model;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Views
{
    public partial class FormRegisterWeightAndVolume : BaseForm
    {
        private readonly PositiveIntTextBox _txtBoxWeight;
        private readonly PositiveIntTextBox _txtBoxLength;
        private readonly PositiveIntTextBox _txtBoxWidth;
        private readonly PositiveIntTextBox _txtBoxHeight;
        private readonly PositiveIntTextBox _txtBoxVolume;

        private readonly ControlGroup _weightGroup;
        private readonly ControlGroup _lengthGroup;
        private readonly ControlGroup _widthGroup;
        private readonly ControlGroup _volumeGroup;
        private readonly ControlGroup _heightGroup;

        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string OkButton = "okButton";
       // private const string NewRegistrationButton = "newRegistrationButton";

        private readonly TouchPanel _radioGroupBox;
        private FormDataCorrectWeightAndVolume _formDataCorrectWeightAndVolume;
        public FormRegisterWeightAndVolume()
        {
            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            SetTextToControls();

            var image = GuiCommon.Instance.BackgroundImage;

            _radioGroupBox = new TouchPanel
            {
                Location = new Point(270, 60 + 25),
                Size = new Size(184, 60),
            };
            var lblOrgUnit = new TransparentLabel
            {
                AutoSize = false,
                Size = new Size(480, 33),
                Location = new Point(0, 35),
                Text = ModelUser.UserProfile.OrgUnitName,
                TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter,
                Font = new Font("Arial", 9F, FontStyle.Bold),
            };
            touchPanel.Controls.Add(lblOrgUnit);
            _weightGroup = CreateNumericControl(GlobalTexts.Weight + Colon, 175, WeightUnitType.KG.ToString());

            _volumeGroup = CreateNumericControl(GlobalTexts.Volume + Colon, 175 + 50, VolumeUnitType.DM3.ToString());
            _lengthGroup = CreateNumericControl(GlobalTexts.Length + Colon, 175 + 50, HLWUnitType.CM.ToString());
            _widthGroup = CreateNumericControl(GlobalTexts.Width + Colon, 225 + 50, HLWUnitType.CM.ToString());
            _heightGroup = CreateNumericControl(GlobalTexts.Height + Colon, 275 + 50, HLWUnitType.CM.ToString());

            _txtBoxWeight = _weightGroup.Numfield;
            _txtBoxLength = _lengthGroup.Numfield;
            _txtBoxWidth = _widthGroup.Numfield;
            _txtBoxHeight = _heightGroup.Numfield;
            _txtBoxVolume = _volumeGroup.Numfield;

            radioBtnConsignmentItem.Location = new Point(0, 0);
            radioBtnConsignment.Location = new Point(0, 30);
            _radioGroupBox.Controls.Add(radioBtnConsignment);
            _radioGroupBox.Controls.Add(radioBtnConsignmentItem);
            touchPanel.Controls.Add(_radioGroupBox);

            if (image == null) return;
            touchPanel.BackgroundImage = image;
            _radioGroupBox.BackgroundImage = image;
        }

        private void SetTextToControls()
        {
            //if (_formDataCorrectWeightAndVolume != null && _formDataCorrectWeightAndVolume.IsStartedByDoublescan)
            //{
            //    btnBack.Text = GlobalTexts.Back;
            //    btnBack.Enabled = true;
            //    btnNewRegistration.Text = String.Empty;
            //}
            //else
            //{
            //    btnBack.Text = string.Empty;
            //    btnBack.Enabled = false;
            //    btnNewRegistration.Text = GlobalTexts.NewRegistration;
            //    btnNewRegistration.WordWrap = true;
            //}
            //btnOk.Text = GlobalTexts.Ok;
            btnOk.Visible = false;
            btnNewRegistration.Visible = false;
            btnBack.Visible = false;
            _multiButtonControl.Clear();
            _multiButtonControl.ListButtons.Clear();
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick, TabButtonType.Cancel) { Name = ButtonBack });
            //_multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.NewRegistration, ButtonNewRegistrationClick) { Name = NewRegistrationButton });
            _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = OkButton });
            touchPanel.Controls.Add(_multiButtonControl);
            _multiButtonControl.GenerateButtons();
        }

        private void CollectWeightAndVolumeValues()
        {
            var measures = _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.Measures;

            if (_txtBoxWeight.IntValue != measures.WeightKg)
                measures.WeightKg = _txtBoxWeight.IntValue;

            if (_txtBoxLength.IntValue != measures.LengthCm)
                measures.LengthCm = _txtBoxLength.IntValue;

            if (_txtBoxWidth.IntValue != measures.WidthCm)
                measures.WidthCm = _txtBoxWidth.IntValue;

            if (_txtBoxHeight.IntValue != measures.HeightCm)
                measures.HeightCm = _txtBoxHeight.IntValue;

            if (_txtBoxVolume.IntValue != measures.VolumeDm3)
                measures.VolumeDm3 = _txtBoxVolume.IntValue;
        }

        private bool RegisterChanges()
        {
            if (_isUpdatingScreen)
                return false;

            var measures = _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.Measures;
            if (measures.Changed)
                return true;

            if (_txtBoxWeight.IntValue != measures.WeightKg)
                measures.Changed = true;

            if (_txtBoxLength.IntValue != measures.LengthCm)
                measures.Changed = true;

            if (_txtBoxWidth.IntValue != measures.WidthCm)
                measures.Changed = true;

            if (_txtBoxHeight.IntValue != measures.HeightCm)
                measures.Changed = true;

            if (_txtBoxVolume.IntValue != measures.VolumeDm3)
                measures.Changed = true;

            return measures.Changed;
        }

        private bool _isUpdatingScreen;
        private void ShowWeightAndVolume()
        {
            _isUpdatingScreen = true;
            var measures = _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.Measures;
            _txtBoxWeight.Text = ShowZeroAsEmpty(measures.WeightKg);
            _txtBoxLength.Text = ShowZeroAsEmpty(measures.LengthCm);
            _txtBoxWidth.Text = ShowZeroAsEmpty(measures.WidthCm);
            _txtBoxHeight.Text = ShowZeroAsEmpty(measures.HeightCm);
            _txtBoxVolume.Text = ShowZeroAsEmpty(measures.VolumeDm3);
            _isUpdatingScreen = false;
        }
        private static string ShowZeroAsEmpty(int value)
        {
            return value == 0 ? string.Empty : value.ToString(CultureInfo.InvariantCulture);
        }

        #region "numericinputfields"

        private ControlGroup CreateNumericControl(string legend, int y, string unit)
        {
            var onScreenKeyBoardProperties = new OnScreenKeyboardProperties(legend, KeyPadTypes.Numeric);

            var controlGroup = new ControlGroup
                   {
                       Legend = new TransparentLabel
                        {
                            Location = new Point(15, y),
                            Text = legend
                        },
                       Numfield = new PositiveIntTextBox
                       {
                           Tag = onScreenKeyBoardProperties,
                           Location = new Point(176, y),
                           MaxLength = 7,
                           Size = new Size(117, 21),
                       },
                       Unit = new TransparentLabel
                    {
                        Location = new Point(299, y),
                        Text = unit
                    }
                   };
            controlGroup.Numfield.TextChanged += NumfieldTextChanged;
            touchPanel.Controls.Add(controlGroup.Numfield);
            touchPanel.Controls.Add(controlGroup.Legend);
            touchPanel.Controls.Add(controlGroup.Unit);
            return controlGroup;
        }

        void NumfieldTextChanged(object sender, EventArgs e)
        {
            RegisterChanges();
            //btnOk.Enabled = _formDataCorrectWeightAndVolume.AnyChanges;
            _multiButtonControl.SetButtonEnabledState(OkButton, _formDataCorrectWeightAndVolume.AnyChanges);
        }


        #endregion

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {
                if (RegisterChanges())
                    CollectWeightAndVolumeValues();
                ViewEvent.Invoke(FormRegisterWeightAndVolumeEvents.Ok, _formDataCorrectWeightAndVolume);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterWeightVolume.ButtonOkClick");
            }
        }

        private void ButtonNewRegistrationClick(object sender, EventArgs e)
        {
            try
            {
                if (_formDataCorrectWeightAndVolume.IsStartedByDoublescan)
                {
                    return;
                }
                if (RegisterChanges())
                    CollectWeightAndVolumeValues();

                ViewEvent.Invoke(FormRegisterWeightAndVolumeEvents.NewRegistration, _formDataCorrectWeightAndVolume);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterWeightVolume.ButtonNewRegistrationClick");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(FormRegisterWeightAndVolumeEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterWeightVolume.ButtonBackClick");
            }
        }

        private void RadioBtnsEntititypeCheckChanged(object sender, EventArgs e)
        {
            try
            {
                if (_updatingRadioBtns) return;
                if (ViewEvent == null) return;
                if (!_formDataCorrectWeightAndVolume.FirstShowAfterScan)
                {
                    if (RegisterChanges())
                    {
                        //Mottak 737 suggestion by Thomas to add popup to confirm
                        var result = GuiCommon.ShowModalDialog(GlobalTexts.Confirm, GlobalTexts.ConfirmChanges, Severity.Warning,
                                                  GlobalTexts.Ok, GlobalTexts.Cancel);
                        if(result == GlobalTexts.Cancel)
                        {
                            _updatingRadioBtns = true;
                            if (radioBtnConsignment.Checked)
                                radioBtnConsignmentItem.Checked = true;
                            else
                            {
                                radioBtnConsignment.Checked = true;
                            }
                            _updatingRadioBtns = false;
                        }
                        CollectWeightAndVolumeValues();
                    }
                        
                }
                if (radioBtnConsignmentItem.Checked)
                {
                    NumericFieldsVisibility(false);
                    _formDataCorrectWeightAndVolume.ConsignmentWasSelected = false;
                    ViewEvent.Invoke(FormRegisterWeightAndVolumeEvents.ShowConsignmentItem);
                }
                else
                {
                    NumericFieldsVisibility(true);
                    _formDataCorrectWeightAndVolume.ConsignmentWasSelected = true;
                    ViewEvent.Invoke(FormRegisterWeightAndVolumeEvents.ShowConsignment);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterWeightAndVolume.RadioBtnsEntititypeCheckChanged");
            }
        }

        private void NumericFieldsVisibility(bool isConsignment)
        {
            _txtBoxVolume.Visible = isConsignment; _volumeGroup.Legend.Visible = isConsignment; _volumeGroup.Unit.Visible = isConsignment;
            _txtBoxLength.Visible = _lengthGroup.Legend.Visible = _lengthGroup.Unit.Visible = !isConsignment;
            _txtBoxWidth.Visible = !isConsignment; _widthGroup.Legend.Visible = !isConsignment; _widthGroup.Unit.Visible = !isConsignment;
            _txtBoxHeight.Visible = !isConsignment; _heightGroup.Legend.Visible = !isConsignment; _heightGroup.Unit.Visible = !isConsignment;
        }

        private bool _updatingRadioBtns;
        public override void OnShow(IFormData formData)
        {
            _formDataCorrectWeightAndVolume = (FormDataCorrectWeightAndVolume)formData;
            if (_formDataCorrectWeightAndVolume == null) return;
            if (_formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData == null) return;

            lblConsignmentNumber.Text = GlobalTexts.Corectwvfor + " " + Environment.NewLine + _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity.EntityId;

            if (_formDataCorrectWeightAndVolume.FirstShowAfterScan)
            {
                _updatingRadioBtns = true;
                var consignmentItemIsScanned = (_formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity is ConsignmentItem);
                if (consignmentItemIsScanned)
                {
                    radioBtnConsignmentItem.Checked = true;
                    NumericFieldsVisibility(false);
                }
                else
                {
                    radioBtnConsignment.Checked = true;
                    NumericFieldsVisibility(true);
                }

                _formDataCorrectWeightAndVolume.FirstShowAfterScan = false;
                btnNewRegistration.Text = _formDataCorrectWeightAndVolume.IsStartedByDoublescan ? string.Empty : GlobalTexts.NewRegistration;
                _updatingRadioBtns = false;
                SetTextToControls();
                SetKeyboardState(KeyboardState.Numeric);
            }
            _radioGroupBox.Visible = _formDataCorrectWeightAndVolume.RadioButtonsVisible;

            lblNumberOfItems.Text = GlobalTexts.NumberOfItemsInShipment + " " +
                                    _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity.ConsignmentItemCountFromLm;
            ShowWeightAndVolume();
            EnableOnScreenKeyboard();
            _txtBoxWeight.BringToFront();
            _txtBoxWeight.Focus();
            btnOk.Enabled = _formDataCorrectWeightAndVolume.AnyChanges;
            _multiButtonControl.SetButtonEnabledState(OkButton, _formDataCorrectWeightAndVolume.AnyChanges);
        }

        private class ControlGroup
        {
            public TransparentLabel Legend;
            public PositiveIntTextBox Numfield;
            public TransparentLabel Unit;
        }
        public class FormRegisterWeightAndVolumeEvents : ViewEvent
        {
            public const int Ok = 0;
            public const int Back = 1;
            public const int NewRegistration = 2;
            public const int ShowConsignment = 3;
            public const int ShowConsignmentItem = 4;
        }
    }
}
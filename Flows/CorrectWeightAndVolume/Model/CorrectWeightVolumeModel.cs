﻿using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Model
{
    /// <summary>
    /// used to pass data into the flow from when started by doublescan
    /// </summary>
    public class FlowDataCorrectWeightAndVolume : BaseFlowData
    {
        public ConsignmentEntity ConsignmentEntity;
        public MessageHolder MessageHolder;
    }

    /// <summary>
    /// used by FormDisplay... and FormRegister... to exchange information with the flow
    /// </summary>
    internal class FormDataCorrectWeightAndVolume : BaseFormData
    {
        public List<CorrectWeightAndVolumeData> ConsignmentDataList;
        public bool IsStartedByDoublescan;// used to control buttonvisibility
        public string ScannedItemId;
        public bool FirstShowAfterScan;// how to populate the text boxes
        public bool ConsignmentWasSelected;// state of radiobuttons
        public bool ConsignmentByRadioButton;// the consignment was requested after radiobutton click
        public bool RadioButtonsVisible;

        public bool ListContainsRelatedEntity()
        {
            if (CurrentCorrectWeightAndVolumeData.ConsignmentEntity is Consignment)
            {
                var currentConsignmentId = CurrentCorrectWeightAndVolumeData.ConsignmentEntity.EntityId;
                if (currentConsignmentId == ConsignmentEntity.UndefinedConsignmentId)
                    return false;

                return ConsignmentDataList
                    .Where(correctWeightAndVolumeData => correctWeightAndVolumeData.ConsignmentEntity is ConsignmentItem)
                    .Any(correctWeightAndVolumeData => correctWeightAndVolumeData.ConsignmentEntity.ConsignmentInstance.EntityId == currentConsignmentId);
            }
            // currententity is consignmentitem
            {
                var currentConsignmentItemId = CurrentCorrectWeightAndVolumeData.ConsignmentEntity.EntityId;
                if (ConsignmentDataList
                    .Where(correctWeightAndVolumeData => correctWeightAndVolumeData.ConsignmentEntity is Consignment)
                    .Any(correctWeightAndVolumeData => correctWeightAndVolumeData.ConsignmentEntity.ConsignmentInstance.EntityId == currentConsignmentItemId))
                {
                    ScannedItemId = currentConsignmentItemId;
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// gets or sets the current item in ConsignmentDataList
        /// </summary>
        public CorrectWeightAndVolumeData CurrentCorrectWeightAndVolumeData
        {
            get
            {
                return ConsignmentDataList
                  .FirstOrDefault(consignmentData => consignmentData.Current);
            }
            set
            {
                foreach (var consignmentData in ConsignmentDataList)
                {
                    consignmentData.Current =
                        consignmentData.ConsignmentEntity == value.ConsignmentEntity;
                }
            }
        }

        /// <summary>
        /// returns list item with given id, id can be consignmentId or consignmentItemId
        /// </summary>
        /// <param name="id">list item or null</param>
        /// <returns></returns>
        internal CorrectWeightAndVolumeData FindById(string id)
        {
            {
                return ConsignmentDataList
                    .FirstOrDefault(consignmentData => consignmentData.ConsignmentEntity.EntityId == id);
            }
        }
        /// <summary>
        /// returns true if any of the entities have been changed
        /// </summary>
        internal bool AnyChanges
        {
            get
            {
                //return ConsignmentDataList.Any(correctWeightAndVolumeData => correctWeightAndVolumeData.Measures.Changed);
                return CurrentCorrectWeightAndVolumeData.Measures.Changed;
            }
        }
    };

    /// <summary>
    /// format of items in FormDataCorrectWeightAndVolume.ConsignmentDataList
    /// </summary>
    internal class CorrectWeightAndVolumeData
    {
        public bool Current { get; set; }
        public ConsignmentEntity ConsignmentEntity { get; set; }
        public Measures Measures { get; set; }
    }
}


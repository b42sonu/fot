﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.CommunicationEntity.GoodsEvents;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Model;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Views;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using Symbol.Barcode;
using System.Linq;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume
{
    internal class ActionCommandsCorrectWeightAndVolume
    {

        /// <summary>
        /// present a screen to change values for weight and volume
        /// </summary>
        private readonly FlowDataCorrectWeightAndVolume _flowdata;
        private readonly FlowCorrectWeightAndVolume _flow;
        private readonly FlowResultCorrectWeightAndVolume _flowResult;
        private FormDataCorrectWeightAndVolume _formDataCorrectWeightAndVolume;
        private FotScannerOutput _scannerOutput;
        private readonly bool _isStartedByDoublescan;

        //TODO :: Write Unit Test
        public ActionCommandsCorrectWeightAndVolume(FlowCorrectWeightAndVolume flow, FlowDataCorrectWeightAndVolume flowdata, FlowResultCorrectWeightAndVolume flowResult)
        {
            _flow = flow;
            _flowdata = flowdata;
            _flowResult = flowResult;
            if (_flowdata != null) _isStartedByDoublescan = _flowdata.ConsignmentEntity != null;
        }

        /// <summary>
        /// first action in this flow, flow is entered from correct w/v in goods menu
        /// or from correction menu as a result of doublescan
        /// </summary>
        /// <returns></returns>



        //TODO :: Write Unit Test
        // SendGoodsEvent
        // ConFirmDelete
        // ViewScanbarcode
        // CorrectRegistration
        internal void ActionViewWeightVolumeSummary()
        {
            var view = ViewCommands.ShowView<FormDisplayCorrectWeightAndVolume>(_formDataCorrectWeightAndVolume);
            view.SetEventHandler(EventHandlerForViewWeightVolumeSummary);
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// bring up view for scanning entityNumber, this view is used in several flows
        /// </summary>
        internal void ActionViewScanbarcode()
        {
            switch (_flowdata.MessageHolder.State)
            {
                case MessageState.Error:
                    SoundUtil.Instance.PlayScanErrorSound();
                    break;
                case MessageState.Warning:
                    SoundUtil.Instance.PlayWarningSound();
                    break;
            }
            var entitymap = new EntityMap();

            if (_formDataCorrectWeightAndVolume.ConsignmentDataList.Count > 0)
            {
                //var firstEntity = _formDataCorrectWeightAndVolume.ConsignmentDataList[0].ConsignmentEntity;
                //firstEntity.EntityStatus = EntityStatus.Scanned;
                //entitymap.Store(firstEntity);

                //Store all scanned items in entity map too
                foreach (var consignmentData in _formDataCorrectWeightAndVolume.ConsignmentDataList)
                {
                    var entity = consignmentData.ConsignmentEntity;
                    entity.EntityStatus = EntityStatus.Scanned;
                    entitymap.Store(entity);
                }

            }

            var formData = new FormScanBarcodeData
            {
                MessageHolder = _flowdata.MessageHolder,
                HeaderText = _flowdata.HeaderText,
                ShowNoOfItems = true,
                CorrectWVflow = true,
                EntityMap = entitymap,// control scanbarcode back button visibility
                CancelButtonDisabledWhenScanning = _formDataCorrectWeightAndVolume.ConsignmentDataList.Count > 0,
                ScanText = BaseActionCommands.ScanTextFromValidBarcodeTypes(BarcodeType.Shipment, "")

            };

            var view = ViewCommands.ShowView<FormScanBarcode>(formData);
            view.SetEventHandler(EventHandlerForScanBarcodeView);
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// adds new item in ConsignmentDataList if not already present
        /// </summary>
        /// <param name="consignmentEntity">value from scanbarcode</param>
        private void AddConsignmentDataFromEntity(ConsignmentEntity consignmentEntity)
        {
            var foundConsignmentData = _formDataCorrectWeightAndVolume.FindById(consignmentEntity.EntityId);
            if (foundConsignmentData == null)
            {
                var consignmentData = new CorrectWeightAndVolumeData
                    {
                        ConsignmentEntity = consignmentEntity,
                        Current = true,
                        Measures = (consignmentEntity is ConsignmentItem)
                                    ? consignmentEntity.CastToConsignmentItem().Measures
                                    : consignmentEntity.CastToConsignment().Measures
                    };

                _formDataCorrectWeightAndVolume.ConsignmentDataList.Add(consignmentData);
                _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData = consignmentData;
            }
            else
            {
                _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData = foundConsignmentData;
            }
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// collects data for all scanned items and builds the goodsevent message.
        /// if started from operation list additional trip and stop parameters are equipped
        /// </summary>
        /// <returns>this is the final action og the flow
        ///and it will return to the originating flow</returns>
        internal void ActionSendGoodsEvent()
        {
            var consignments = new List<GoodsEventsEventInformationConsignment>();
            foreach (var consignmentData in _formDataCorrectWeightAndVolume.ConsignmentDataList)
            {
                if (consignmentData.ConsignmentEntity is ConsignmentItem)
                {
                    var consignmentItems = new List<GoodsEventsEventInformationConsignmentConsignmentItem>();

                    var consignmentItem = consignmentData.ConsignmentEntity.CastToConsignmentItem();

                    if (consignmentItem.Measures.Changed)
                    {
                        consignmentItems.Add(new GoodsEventsEventInformationConsignmentConsignmentItem
                        {
                            ConsignmentItemNumber = consignmentItem.ItemId,
                            Weight = consignmentData.Measures.WeightKg,
                            Length = consignmentData.Measures.LengthCm,
                            Height = consignmentData.Measures.HeightCm,
                            Width = consignmentData.Measures.WidthCm,
                            WeightSpecified = consignmentData.Measures.WeightKg > 0,
                            LengthSpecified = consignmentData.Measures.LengthCm > 0,
                            HeightSpecified = consignmentData.Measures.HeightCm > 0,
                            WidthSpecified = consignmentData.Measures.WidthCm > 0,
                            WeightUnit = WeightUnitType.KG,
                            WeightUnitSpecified = consignmentData.Measures.WeightKg > 0,
                            HLWUnit = HLWUnitType.CM,
                            HLWUnitSpecified = true,
                        });


                        consignments.Add(new GoodsEventsEventInformationConsignment
                        {
                            EventType = GoodsEventHelper.GetEventType(consignmentItem),
                            ConsignmentNumber = consignmentItem.ConsignmentIdForGoodsEvent,
                            ConsignmentItem = consignmentItems.ToArray(),
                        });
                    }
                }
                else
                {
                    var consignment = consignmentData.ConsignmentEntity.CastToConsignment();
                    if (consignment.Measures.Changed)
                    {
                        consignments.Add(new GoodsEventsEventInformationConsignment
                        {
                            EventType = GoodsEventHelper.GetEventType(consignment),
                            ConsignmentNumber = consignment.ConsignmentIdForGoodsEvent,
                            Weight = consignmentData.Measures.WeightKg,
                            WeightSpecified = consignmentData.Measures.WeightKg > 0,
                            WeightUnit = WeightUnitType.KG,
                            WeightUnitSpecified = true,
                            Volume = consignmentData.Measures.VolumeDm3,
                            VolumeSpecified = consignmentData.Measures.VolumeDm3 > 0,
                            VolumeUnit = VolumeUnitType.DM3,
                            VolumeUnitSpecified = true
                        });
                    }
                }
            }
            if (consignments.Count <= 0)
            {
                Logger.LogEvent(Severity.Debug, "no changes - no goodsevent");
                return;
            }

            var eventInformation = new GoodsEventsEventInformation
                                       {
                                           EventCode = EventCodeUtil.GetEventCodeFromProcess(),
                                           EventTime = DateTime.Now,
                                           Consignment = consignments.ToArray(),
                                       };

            var goodsEventMessage = GoodsEventHelper.CreateGoodsEvent();
            goodsEventMessage.EventInformation = eventInformation;
            goodsEventMessage.LoggingInformation.ProcessName = "Goods";
            goodsEventMessage.LoggingInformation.SubProcessName = "CorrectWeightVolume";

            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GoodsEventsEventInformation, null);
            CommunicationClient.Instance.SendGoodsEvent(goodsEventMessage, transaction);
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// bring up view for changing WidthVolume
        /// </summary>
        /// <returns>ends executeactionstate</returns>
        internal void ActionViewWeightAndVolume()
        {
            var view = ViewCommands.ShowView<FormRegisterWeightAndVolume>(_formDataCorrectWeightAndVolume);
            view.SetEventHandler(EventHandlerForRegisterWeightAndVolume);
        }
        //TODO :: Write Unit Test
        // ViewWeightAndVolume
        // ViewScanbarcode
        internal Boolean ActionInitialAction()
        {

            //_actionCommandsScanBarcode = new ActionCommandsScanBarcode(_flowdata.MessageHolder);

            _formDataCorrectWeightAndVolume = new FormDataCorrectWeightAndVolume
            {
                ConsignmentDataList = new List<CorrectWeightAndVolumeData>(),
                IsStartedByDoublescan = _flowdata.ConsignmentEntity != null,
            };
            if (_formDataCorrectWeightAndVolume.IsStartedByDoublescan)
            {
                AddConsignmentDataFromEntity(_flowdata.ConsignmentEntity);
                _formDataCorrectWeightAndVolume.FirstShowAfterScan = true;
                if (_flowdata.ConsignmentEntity is ConsignmentItem)
                {
                    _formDataCorrectWeightAndVolume.ScannedItemId = _flowdata.ConsignmentEntity.EntityId;
                    _formDataCorrectWeightAndVolume.RadioButtonsVisible = !_flowdata.ConsignmentEntity.CastToConsignmentItem().HasUnknownConsignment && !_flowdata.ConsignmentEntity.CastToConsignmentItem().IsConsignmentSynthetic;
                }
                else
                    _formDataCorrectWeightAndVolume.ScannedItemId = null;
            }


            return _formDataCorrectWeightAndVolume.IsStartedByDoublescan;
        }

        //TODO :: Write Unit Test
        // comments
        internal FlowStateCorrectWeightAndVolume ActionDeleteNrItem()
        {
            FlowStateCorrectWeightAndVolume state;
            _formDataCorrectWeightAndVolume.ConsignmentDataList.Remove(_formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData);
            if (_formDataCorrectWeightAndVolume.ConsignmentDataList.Any(p => p.Measures.Changed))
            {
                _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData = _formDataCorrectWeightAndVolume.ConsignmentDataList[0];
                state = FlowStateCorrectWeightAndVolume.ViewWeightVolumeSummary;
            }
            else
            {
                RemoveUnchangedConsignmentData();
                if (_isStartedByDoublescan)
                {
                    _flowResult.State = FlowResultState.Cancel;
                    state = FlowStateCorrectWeightAndVolume.ThisFlowComplete;
                }
                else
                {
                    state = FlowStateCorrectWeightAndVolume.ViewScanbarcode;
                }
            }


            return state;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// the state of the radiobuttons have been changed, if consignment has been 
        /// seleted a "scan" is initiated
        /// </summary>
        /// <returns>
        /// true: the consignment has to be "scanned"
        /// false: the consignment or item is available for diaplay
        /// </returns>
        internal Boolean ActionEntityTypehasChanged()
        {
            if (_formDataCorrectWeightAndVolume.ConsignmentWasSelected)
            {
                var consignment = _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity.ConsignmentInstance;
                var consignmentEntityFound = _formDataCorrectWeightAndVolume.FindById(consignment.EntityId);
                if (consignmentEntityFound != null)
                {
                    _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData = consignmentEntityFound;
                    return false;
                }
                _scannerOutput = new FotScannerOutput { Code = consignment.EntityId, Type = DecoderTypes.UNKNOWN };
                _formDataCorrectWeightAndVolume.ConsignmentByRadioButton = true;
                return true;
            }
            // item was selected - is always found in cache
            foreach (var dataCvw in _formDataCorrectWeightAndVolume.ConsignmentDataList)
            {
                if (dataCvw.ConsignmentEntity.ConsignmentInstance.EntityId !=
                    _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity.ConsignmentId)
                    continue;
                if (dataCvw.ConsignmentEntity is Consignment)
                    continue;
                _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData = dataCvw;
                return false;
            }
            Logger.LogEvent(Severity.Debug, "expected but not found;" + _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity.ConsignmentId);
            return false;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// the user must conform the delete action to perform the actual
        /// cancel of the WidthVolume corrections
        /// </summary>
        /// <returns>action for delete if confirmed, else back to WVsummary</returns>
        internal Boolean ActionConFirmDelete()
        {
            var number = _formDataCorrectWeightAndVolume.CurrentCorrectWeightAndVolumeData.ConsignmentEntity.EntityDisplayId;
            var msgBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Attention, GlobalTexts.Delete + "?\n\r" + number, Severity.Warning,
                                                         GlobalTexts.Yes, GlobalTexts.No);
            return (msgBoxResult == GlobalTexts.Yes);
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// the user has pushed button for correction in the summary view and 
        /// is moved to the correction view with data for the entity selected in the list
        /// </summary>
        /// <returns></returns>
        internal void ActionCorrectRegistration()
        {
            _formDataCorrectWeightAndVolume.FirstShowAfterScan = true;
        }


        //TODO :: Write Unit Test
        /// <summary>
        /// the user has scanned an item and will now send a validate goods request
        /// the response is used to set initial values in the correction screen
        /// </summary>
        /// <returns>if scanned item cannot be validated user is returned to she scanning view</returns>
        internal void ActionExecuteFlowScanBarcode()
        {
            SoundUtil.Instance.TurnSoundOff();
            var flowScanBarcodeInstance = _flow.GoToFlowScanBarcodeForMainScan();
            flowScanBarcodeInstance.FlowResult.State = FlowResultState.Ok;
            flowScanBarcodeInstance.FotScannerOutput = _scannerOutput;
            flowScanBarcodeInstance.ExecuteActionState(FlowStateScanBarcode.ActionValidateBarcode);
        }

        internal bool ActionBackFromFlowScanBarcode()
        {
            SoundUtil.Instance.TurnSoundOn();
            var subFlowResult = (FlowResultScanBarcode)_flow.SubflowResult;
            if (subFlowResult.CurrentConsignmentEntity != null)
            {
                if (subFlowResult.CurrentConsignmentEntity is Consignment && subFlowResult.CurrentConsignmentEntity.IsConsignmentSynthetic)
                {
                    _flowdata.MessageHolder.Update(GlobalTexts.SyntheticConsignment, MessageState.Error);
                    return false;
                }
            }

            if (subFlowResult.State != FlowResultState.Ok)
            {
                if (_flowdata.MessageHolder.Text == GlobalTexts.ScanningConsignmentNotAuthorized)
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Attention, _flowdata.MessageHolder.Text, Severity.Warning, GlobalTexts.Ok);
                    _formDataCorrectWeightAndVolume.RadioButtonsVisible = false;
                    _formDataCorrectWeightAndVolume.FirstShowAfterScan = true;
                    _flowdata.MessageHolder.Clear();
                }

                return false;
            }


            if (_flowdata.MessageHolder.State == MessageState.Information)
            {
                GuiCommon.ShowModalDialog(GlobalTexts.Attention, _flowdata.MessageHolder.Text, Severity.Info, GlobalTexts.Ok);
            }
            else
            {
                SoundUtil.Instance.PlaySuccessSound();
            }
            _flowdata.MessageHolder = new MessageHolder();

            _flowdata.ConsignmentEntity = subFlowResult.CurrentConsignmentEntity;
            AddConsignmentDataFromEntity(_flowdata.ConsignmentEntity);
            if (_formDataCorrectWeightAndVolume.ConsignmentByRadioButton) // triggered by radiobutton
            {
                _formDataCorrectWeightAndVolume.FirstShowAfterScan = false;
                _formDataCorrectWeightAndVolume.ConsignmentByRadioButton = false;
            }
            else
            {
                _formDataCorrectWeightAndVolume.FirstShowAfterScan = true;
            }

            if (_flowdata.ConsignmentEntity is ConsignmentItem)
            {
                _formDataCorrectWeightAndVolume.ScannedItemId = _flowdata.ConsignmentEntity.EntityId;
                _formDataCorrectWeightAndVolume.RadioButtonsVisible = !_flowdata.ConsignmentEntity.CastToConsignmentItem().HasUnknownConsignment && !_flowdata.ConsignmentEntity.CastToConsignmentItem().IsConsignmentSynthetic;
            }
            else
            {
                _formDataCorrectWeightAndVolume.ScannedItemId = null;
                _formDataCorrectWeightAndVolume.RadioButtonsVisible = _formDataCorrectWeightAndVolume.ListContainsRelatedEntity();
            }

            //if (_isStartedByDoublescan)
            //{
            //    _formDataCorrectWeightAndVolume.RadioButtonsVisible = false;
            //}

            return true;
        }



        //TODO :: Write Unit Test
        // ThisFlowComplete
        internal void ActionEndAction()
        {
            SoundUtil.Instance.TurnSoundOn();
        }


        #region  code made by FF



        #endregion



        #region eventHandlers
        //TODO :: Write Unit Test
        private void EventHandlerForScanBarcodeView(int flowEvents, object[] data)
        {
            switch (flowEvents)
            {
                case ScanBarcodeViewEvents.Ok:
                    _flow.ExecuteState((int)FlowStateCorrectWeightAndVolume.ViewWeightVolumeSummary);
                    break;
                case ScanBarcodeViewEvents.BarcodeScanned:
                    SoundUtil.Instance.PlayScanSound();
                    _scannerOutput = (FotScannerOutput)data[0];
                    _formDataCorrectWeightAndVolume.FirstShowAfterScan = true;
                    _flow.ExecuteState((int)FlowStateCorrectWeightAndVolume.ExecuteFlowScanBarcode);
                    break;

                default:
                    _flow.ExecuteState((int)FlowStateCorrectWeightAndVolume.ThisFlowComplete);
                    break;

            }
        }
        //TODO :: Write Unit Test
        private void EventHandlerForRegisterWeightAndVolume(int correctWeightAndVolumeViewEvents, params object[] data)
        {
            switch (correctWeightAndVolumeViewEvents)
            {
                case FormRegisterWeightAndVolume.FormRegisterWeightAndVolumeEvents.Back:
                    _flow.ExecuteActionState(_formDataCorrectWeightAndVolume.IsStartedByDoublescan
                                                 ? FlowStateCorrectWeightAndVolume.ThisFlowCancelled
                                                 : FlowStateCorrectWeightAndVolume.ActionRemoveUnchangedConsignmentData);

                    break;

                case FormRegisterWeightAndVolume.FormRegisterWeightAndVolumeEvents.NewRegistration:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.ActionNewRegistration);
                    break;

                case FormRegisterWeightAndVolume.FormRegisterWeightAndVolumeEvents.Ok:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.ViewWeightVolumeSummary);
                    break;

                case FormRegisterWeightAndVolume.FormRegisterWeightAndVolumeEvents.ShowConsignment:
                case FormRegisterWeightAndVolume.FormRegisterWeightAndVolumeEvents.ShowConsignmentItem:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.EntityTypehasChanged);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in RegisterTemperatureViewEvent");
                    break;
            }
        }
        //TODO :: Write Unit Test
        private void EventHandlerForViewWeightVolumeSummary(int formDisplayWeightAndVolumeEvents,
                                                                   params object[] data)
        {
            switch (formDisplayWeightAndVolumeEvents)
            {
                case FormDisplayWeightAndVolumeEvents.NewRegistration:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.ActionNewRegistration);
                    break;

                case FormDisplayWeightAndVolumeEvents.CorrectItem:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.CorrectRegistration);
                    break;

                case FormDisplayWeightAndVolumeEvents.Ok:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.SendGoodsEvent);
                    break;

                case FormDisplayWeightAndVolumeEvents.Delete:
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.ConFirmDelete);
                    break;

                default:
                    Logger.LogEvent(Severity.Error,
                                    "Encountered unhandled event in DisplayCorrectWeightVolumeViewEventHandler");
                    _flow.ExecuteActionState(FlowStateCorrectWeightAndVolume.ThisFlowComplete);
                    break;
            }
        }

        #endregion



        internal void RemoveUnchangedConsignmentData()
        {
            var listUnchangedConsignmentDataList =
               _formDataCorrectWeightAndVolume.ConsignmentDataList.Where(p => p.Measures.Changed == false).ToList();
            foreach (var correctWeightAndVolumeData in listUnchangedConsignmentDataList)
            {
                _formDataCorrectWeightAndVolume.ConsignmentDataList.Remove(correctWeightAndVolumeData);
            }

        }
    }
}

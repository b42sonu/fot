﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Model;
using Com.Bring.PMP.PreComFW.Shared.Flows.ScanBarcode;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume
{
    public enum FlowStateCorrectWeightAndVolume
    {

        InitialAction,
        ViewScanbarcode,
        ExecuteFlowScanBarcode,
        ViewWeightAndVolume,
        EntityTypehasChanged,
        BackFromFlowScanBarcode,
        ViewWeightVolumeSummary,
        CorrectRegistration,
        SendGoodsEvent,
        ConFirmDelete,
        DeleteNrItem,
        ActionNewRegistration,
        ActionRemoveUnchangedConsignmentData,
        EndAction,
        ThisFlowCancelled,
        ThisFlowComplete,
        BreakLoop
    }

    public class FlowResultCorrectWeightAndVolume : BaseFlowResult { }

    // US 34 - http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=8554925
    public class FlowCorrectWeightAndVolume : BaseFlow
    {
        private ActionCommandsCorrectWeightAndVolume _actionCommandsCorrectWeightAndVolume;
        private FlowResultCorrectWeightAndVolume _flowResult;
        private FlowDataCorrectWeightAndVolume _flowData;

        public override void BeginFlow(IFlowData flowData)
        {
            _flowResult = new FlowResultCorrectWeightAndVolume();
            _flowData = (FlowDataCorrectWeightAndVolume)flowData;
            _flowData.MessageHolder = new MessageHolder();
            _actionCommandsCorrectWeightAndVolume = new ActionCommandsCorrectWeightAndVolume(this, _flowData,
                                                                                             _flowResult);

            ExecuteActionState(FlowStateCorrectWeightAndVolume.InitialAction);
        }

        public override void ExecuteState(int state)
        {
            ExecuteActionState((FlowStateCorrectWeightAndVolume)state);
        }


        internal void ExecuteActionState(FlowStateCorrectWeightAndVolume state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowCorrectWeightAndVolume;" + state);

                    switch (state)
                    {

                        case FlowStateCorrectWeightAndVolume.InitialAction:
                            state = _actionCommandsCorrectWeightAndVolume.ActionInitialAction()
                                        ? FlowStateCorrectWeightAndVolume.ViewWeightAndVolume
                                        : FlowStateCorrectWeightAndVolume.ViewScanbarcode;
                            break;

                        case FlowStateCorrectWeightAndVolume.ViewScanbarcode:
                            _actionCommandsCorrectWeightAndVolume.ActionViewScanbarcode();
                            state = FlowStateCorrectWeightAndVolume.BreakLoop;
                            break;

                        case FlowStateCorrectWeightAndVolume.ActionNewRegistration:
                            _actionCommandsCorrectWeightAndVolume.RemoveUnchangedConsignmentData();
                            state = FlowStateCorrectWeightAndVolume.ViewScanbarcode;
                            break;

                        case FlowStateCorrectWeightAndVolume.ActionRemoveUnchangedConsignmentData:
                            _actionCommandsCorrectWeightAndVolume.RemoveUnchangedConsignmentData();
                            state = FlowStateCorrectWeightAndVolume.ViewScanbarcode;
                            break;

                        case FlowStateCorrectWeightAndVolume.ExecuteFlowScanBarcode:
                            _actionCommandsCorrectWeightAndVolume.ActionExecuteFlowScanBarcode();
                            state = FlowStateCorrectWeightAndVolume.BreakLoop;
                            break;

                        case FlowStateCorrectWeightAndVolume.ViewWeightAndVolume:
                            _actionCommandsCorrectWeightAndVolume.ActionViewWeightAndVolume();
                            state = FlowStateCorrectWeightAndVolume.BreakLoop;
                            break;

                        case FlowStateCorrectWeightAndVolume.EntityTypehasChanged:
                            state = _actionCommandsCorrectWeightAndVolume.ActionEntityTypehasChanged()
                                        ? FlowStateCorrectWeightAndVolume.ExecuteFlowScanBarcode
                                        : FlowStateCorrectWeightAndVolume.ViewWeightAndVolume;
                            break;

                        case FlowStateCorrectWeightAndVolume.BackFromFlowScanBarcode:
                            var result = _actionCommandsCorrectWeightAndVolume.ActionBackFromFlowScanBarcode();
                            state = result
                                        ? FlowStateCorrectWeightAndVolume.ViewWeightAndVolume
                                        : FlowStateCorrectWeightAndVolume.ViewScanbarcode;
                            break;

                        case FlowStateCorrectWeightAndVolume.ViewWeightVolumeSummary:
                            _actionCommandsCorrectWeightAndVolume.ActionViewWeightVolumeSummary();
                            state = FlowStateCorrectWeightAndVolume.BreakLoop;
                            break;

                        case FlowStateCorrectWeightAndVolume.CorrectRegistration:
                            _actionCommandsCorrectWeightAndVolume.ActionCorrectRegistration();
                            state = FlowStateCorrectWeightAndVolume.ViewWeightAndVolume;
                            break;

                        case FlowStateCorrectWeightAndVolume.SendGoodsEvent:
                            _actionCommandsCorrectWeightAndVolume.ActionSendGoodsEvent();
                            state = FlowStateCorrectWeightAndVolume.EndAction;
                            break;

                        case FlowStateCorrectWeightAndVolume.ConFirmDelete:
                            state = _actionCommandsCorrectWeightAndVolume.ActionConFirmDelete()
                                        ? FlowStateCorrectWeightAndVolume.DeleteNrItem
                                        : FlowStateCorrectWeightAndVolume.ViewWeightVolumeSummary;
                            break;

                        case FlowStateCorrectWeightAndVolume.DeleteNrItem:
                            state = _actionCommandsCorrectWeightAndVolume.ActionDeleteNrItem();
                            break;

                        case FlowStateCorrectWeightAndVolume.EndAction:
                            _flowResult.State = FlowResultState.Ok;
                            _flowResult.Message = GlobalTexts.Correctwv;
                            _actionCommandsCorrectWeightAndVolume.ActionEndAction();
                            state = FlowStateCorrectWeightAndVolume.ThisFlowComplete;
                            break;

                        case FlowStateCorrectWeightAndVolume.ThisFlowComplete:
                            EndFlow();
                            state = FlowStateCorrectWeightAndVolume.BreakLoop;
                            break;
                        case FlowStateCorrectWeightAndVolume.ThisFlowCancelled:
                            _flowResult.State = FlowResultState.Cancel;
                            EndFlow();
                            state = FlowStateCorrectWeightAndVolume.BreakLoop;
                            break;
                    }
                } while (state != FlowStateCorrectWeightAndVolume.BreakLoop);
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowCorrectWeightAndVolume.ExecuteActionState");
                _flowResult.State = FlowResultState.Error;
                EndFlow();
            }
        }

        internal FlowScanBarcode GoToFlowScanBarcodeForMainScan()
        {
            var entityMap = new EntityMap();
            var flowData = new FlowDataScanBarcode(GlobalTexts.CorrectWeightVolume, _flowData.MessageHolder,
                                                   BarcodeType.Shipment, entityMap) { HaveCustomForm = true };

            var flowScanBarcodeInstance = (FlowScanBarcode) StartSubFlow<FlowScanBarcode>(flowData,
                (int) FlowStateCorrectWeightAndVolume.BackFromFlowScanBarcode, Process.Inherit);
            return flowScanBarcodeInstance;
        }

        private void EndFlow()
        {
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}
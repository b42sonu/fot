﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.Camera;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using System.Globalization;

// US 54: http://kp-confluence.postennorge.no/pages/viewpage.action?pageId=7932745
namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTemperature
{
    /// <summary>
    /// Enum which specifies the collection of state for this flow.
    /// </summary>
    public enum FlowStateRegisterTemperature
    {
        ActionValidateTemperature,
        ActionBackFromFlowCamera,
        ActionSendGoodsTemperatureEvent,
        ActionDeletePictureFolder,
        ViewCommands,
        ViewShowRegisterTemperature,
        UpdateView,
        FlowCamera,
        ThisFlowComplete
    }
    public class FlowResultRegisterTemperature : BaseFlowResult
    {
    }

    /// <summary>
    /// flow data that needed for this flow when any flow call this flow...
    /// </summary>
    public class FlowRegisterTemperatureData : BaseFlowData
    {
        public string TemperatureFunctionalityFor;
        public EntityMap EntityMap { get; set; }
        public ConsignmentEntity ConsignmentEntity;
    }
    public class FlowRegisterTemperature : BaseFlow
    {
        private readonly ViewCommandsRegisterTemperature _viewCommandsRegisterTemperature;
        private FlowRegisterTemperatureData _flowData;
        private readonly FormRegisterTemperatureData _formDataTemperature;
        private readonly FlowResultRegisterTemperature _flowResult;
        private readonly MessageHolder _messageHolder;
        private string _photoProcessGuid = string.Empty;
        private readonly ActionCommandsRegisterTemperature _actionCommandsRegisterTemperature;
        private bool _validateOnly;
        public FlowRegisterTemperature()
        {
            _messageHolder = new MessageHolder();
            _actionCommandsRegisterTemperature = new ActionCommandsRegisterTemperature(CommunicationClient.Instance, _messageHolder);
            _viewCommandsRegisterTemperature = new ViewCommandsRegisterTemperature();
            _formDataTemperature = new FormRegisterTemperatureData();
            _flowResult = new FlowResultRegisterTemperature();
        }
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing command FlowRegisterTemperature.BeginFlow(IFlowData flowData)");
            _flowData = (FlowRegisterTemperatureData)flowData;
            if (_flowData != null && _flowData.ConsignmentEntity != null)
            {
                _formDataTemperature.ConsignmentItemNumber = _flowData.ConsignmentEntity.EntityId;
                _formDataTemperature.ConsignmentItemType = 
                    _flowData.ConsignmentEntity is ConsignmentItem ? GlobalTexts.ConsignmentItem : GlobalTexts.Consignment;

                //_formDataTemperature.ProductCategory = _flowData.Damage.ProductCategory;
            }
            else
            {
                Logger.LogAssert(false, "FlowRegisterTemerature started without neccesary arguments");    
            }

            ExecuteState(FlowStateRegisterTemperature.ViewShowRegisterTemperature);
        }

        public void ExecuteState(FlowStateRegisterTemperature state)
        {
            if (state > FlowStateRegisterTemperature.ViewCommands)
                ExecuteViewState(state);
            else
                ExecuteActionState(state);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateRegisterTemperature)state);
        }
        private void ExecuteViewState(FlowStateRegisterTemperature stateRegisterTemperature)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowRegisterTemperature;" + stateRegisterTemperature);
                switch (stateRegisterTemperature)
                {
                    case FlowStateRegisterTemperature.ViewShowRegisterTemperature:
                        _viewCommandsRegisterTemperature.ShowRegisterTemperatureView(RegisterTemperatureViewEventHandler, _formDataTemperature, _flowData);
                        break;

                    case FlowStateRegisterTemperature.FlowCamera:
                        var flowDataCamera = new FlowDataCamera
                        {
                            HeaderText = GlobalTexts.RegisterTemperature,
                            CameraFunctionalityFor = GlobalTexts.Temperature,
                            ConsignmentItemType = _flowData.ConsignmentEntity is ConsignmentItem ? GlobalTexts.ConsignmentItem : GlobalTexts.Consignment,
                            ConsignmentItemNumber = _flowData.ConsignmentEntity.EntityId,
                            FolderName = _photoProcessGuid,
                        };
                        StartSubFlow<FlowCamera>(flowDataCamera, (int)FlowStateRegisterTemperature.ActionBackFromFlowCamera, Process.Inherit);
                        break;

                    case FlowStateRegisterTemperature.UpdateView:
                        CurrentView.UpdateView(_formDataTemperature);
                        break;

                    case FlowStateRegisterTemperature.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }

            catch (Exception e)
            {
                Logger.LogException(e, "FlowRegisterTemperature.ExecuteViewState");
                _flowResult.State=FlowResultState.Exception;
                ExecuteState(FlowStateRegisterTemperature.ThisFlowComplete);
            }
        }

        /// <summary>
        /// This method executes the action states for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateRegisterTemperature state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowRegisterTemperature;" + state);
                    switch (state)
                    {
                        case FlowStateRegisterTemperature.ActionValidateTemperature:
                            bool result = _actionCommandsRegisterTemperature.IsValidTemperature(_formDataTemperature);
                            state = result ? (_validateOnly ? FlowStateRegisterTemperature.UpdateView : FlowStateRegisterTemperature.ActionSendGoodsTemperatureEvent) : FlowStateRegisterTemperature.UpdateView;
                            break;

                        case FlowStateRegisterTemperature.ActionSendGoodsTemperatureEvent:
                            _actionCommandsRegisterTemperature.SendRegisterTemperatureGoodsEvent(_flowData.ConsignmentEntity, ModelMain.SelectedOperationProcess, Convert.ToDecimal(_formDataTemperature.Temperature,CultureInfo.InvariantCulture),
                                _formDataTemperature.EventComment, _photoProcessGuid);
                            state = string.IsNullOrEmpty(_photoProcessGuid) ? FlowStateRegisterTemperature.ThisFlowComplete : FlowStateRegisterTemperature.ActionDeletePictureFolder;
                            break;

                        case FlowStateRegisterTemperature.ActionDeletePictureFolder:
                            _actionCommandsRegisterTemperature.DeletePicturesFolder(_photoProcessGuid);
                            state = FlowStateRegisterTemperature.ThisFlowComplete;
                            break;

                        case FlowStateRegisterTemperature.ActionBackFromFlowCamera:
                            state = _actionCommandsRegisterTemperature.ValidateFlowCameraResult((FlowResultCamera)SubflowResult, out _photoProcessGuid, _formDataTemperature);
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;

                    }
                } while (state < FlowStateRegisterTemperature.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowRegisterTemperature.ExecuteActionState");
                _flowResult.State=FlowResultState.Exception;
                state=FlowStateRegisterTemperature.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }

        /// <summary>
        /// Handler which will handle all type of events from register temperature view..
        /// </summary>
        private void RegisterTemperatureViewEventHandler(int registerTemperatureViewEvents, params object[] data)
        {
            switch (registerTemperatureViewEvents)
            {
                case RegisterTemperatureViewEvent.Ok:
                    _flowResult.Message = GlobalTexts.RegisterTemperature;
                    _flowResult.State = FlowResultState.Ok;
                    _validateOnly = false; 
                    _formDataTemperature.Temperature = Convert.ToString(data[0]);
                    _formDataTemperature.EventComment = Convert.ToString(data[1]);

                    ExecuteActionState(FlowStateRegisterTemperature.ActionValidateTemperature);
                    break;
                case RegisterTemperatureViewEvent.Back:
                    _flowResult.Message=GlobalTexts.RegisterTempratureCancelled;
                    _flowResult.State = FlowResultState.Cancel;
                    if (string.IsNullOrEmpty(_photoProcessGuid))
                        ExecuteViewState(FlowStateRegisterTemperature.ThisFlowComplete);
                    else
                        ExecuteActionState(FlowStateRegisterTemperature.ActionDeletePictureFolder);
                    break;
                case RegisterTemperatureViewEvent.Camera:
                    ExecuteViewState(FlowStateRegisterTemperature.FlowCamera);
                    break;
                case RegisterTemperatureViewEvent.Validate://Added validation of temperature
                    _formDataTemperature.Temperature = Convert.ToString(data[0]);
                    _validateOnly = true;//Added for validation of temperature only
                    ExecuteActionState(FlowStateRegisterTemperature.ActionValidateTemperature);
                    break;
                default:
                    Logger.LogEvent(Severity.Error, "Encountered unhandled event in RegisterTemperatureViewEvent");
                    break;
            }
        }

        /// <summary>
        /// This methos ends the register temperature flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowRegisterTemperature");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

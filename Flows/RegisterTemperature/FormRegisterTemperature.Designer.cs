﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
using Resco.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTemperature
{
    partial class FormRegisterTemperature
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.labelConsignmentValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelConsignment = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelProductCategoryValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelProductCategory = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCelcius = new Resco.Controls.CommonControls.TransparentLabel();
            this.messageControl = new Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl();
            this.labelPicture = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtComment = new PreCom.Controls.PreComInput2();
            this.labelComment = new Resco.Controls.CommonControls.TransparentLabel();
            this.txtTemperature = new PreCom.Controls.PreComInput2();
            this.labelTemperature = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelProductCategoryValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelProductCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCelcius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTemperature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.labelConsignmentValue);
            this.touchPanel.Controls.Add(this.labelConsignment);
            this.touchPanel.Controls.Add(this.labelProductCategoryValue);
            this.touchPanel.Controls.Add(this.labelProductCategory);
            this.touchPanel.Controls.Add(this.labelCelcius);
            this.touchPanel.Controls.Add(this.messageControl);
          
            this.touchPanel.Controls.Add(this.labelPicture);
            this.touchPanel.Controls.Add(this.txtTemperature);
            this.touchPanel.Controls.Add(this.txtComment);
            this.touchPanel.Controls.Add(this.labelComment);
            this.touchPanel.Controls.Add(this.labelTemperature);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 552);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // labelConsignmentValue
            // 
            this.labelConsignmentValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelConsignmentValue.Location = new System.Drawing.Point(22, 77);
            this.labelConsignmentValue.Name = "labelConsignmentValue";
            this.labelConsignmentValue.Size = new System.Drawing.Size(0, 0);
            // 
            // labelConsignment
            // 
            this.labelConsignment.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelConsignment.Location = new System.Drawing.Point(22, 47);
            this.labelConsignment.Name = "labelConsignment";
            this.labelConsignment.Size = new System.Drawing.Size(187, 24);
            // 
            // labelProductCategoryValue
            // 
            this.labelProductCategoryValue.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelProductCategoryValue.Location = new System.Drawing.Point(213, 107);
            this.labelProductCategoryValue.Name = "labelProductCategoryValue";
            this.labelProductCategoryValue.Size = new System.Drawing.Size(0, 0);
            // 
            // labelProductCategory
            // 
            this.labelProductCategory.Font = new System.Drawing.Font("Arial",9F, System.Drawing.FontStyle.Regular);
            this.labelProductCategory.Location = new System.Drawing.Point(22, 107);
            this.labelProductCategory.Name = "labelProductCategory";
            this.labelProductCategory.Size = new System.Drawing.Size(172, 24);
          
            // 
            // labelCelcius
            // 
            this.labelCelcius.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCelcius.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelCelcius.Location = new System.Drawing.Point(441, 160);
            this.labelCelcius.Name = "labelCelcius";
            this.labelCelcius.Size = new System.Drawing.Size(24, 24);
            this.labelCelcius.Text = "ºC";
         
            // 
            // messageControl
            // 
            this.messageControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.messageControl.Location = new System.Drawing.Point(22, 359);
            this.messageControl.MessageText = "";
            this.messageControl.Name = "messageControl";
            this.messageControl.Size = new System.Drawing.Size(436, 120);
            this.messageControl.TabIndex = 32;
            
            // 
            // labelPicture
            // 
            this.labelPicture.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelPicture.Location = new System.Drawing.Point(22, 300);
            this.labelPicture.Name = "labelPicture";
            this.labelPicture.Size = new System.Drawing.Size(295, 24);
            
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(22, 224);
            this.txtComment.MaxLength = 200;
            this.txtComment.Multiline = true;
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(436, 67);
            this.txtComment.TabIndex = 2;
            this.txtComment.TextTranslation = false;
            // 
            // labelComment
            // 
            this.labelComment.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelComment.Location = new System.Drawing.Point(22, 194);
            this.labelComment.Name = "labelComment";
            this.labelComment.Size = new System.Drawing.Size(188, 24);
            
            // 
            // txtTemperature
            // 
            this.txtTemperature.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTemperature.Location = new System.Drawing.Point(294, 150);
            this.txtTemperature.MaxLength = 4;
            this.txtTemperature.Name = "txtTemperature";
            this.txtTemperature.Size = new System.Drawing.Size(142, 41);
            this.txtTemperature.TabIndex = 1;
            this.txtTemperature.TextTranslation = false;
            this.txtTemperature.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TxtTemperatureKeyUp);
            // 
            // labelTemperature
            // 
            this.labelTemperature.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelTemperature.Location = new System.Drawing.Point(22, 154);
            this.labelTemperature.Name = "labelTemperature";
            this.labelTemperature.Size = new System.Drawing.Size(162, 24);
            
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(105, 3);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(249, 27);
            
            // 
            // FormRegisterTemperature
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 552);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormRegisterTemperature";
            this.Text = "FormRegisterTemperature";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignmentValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelConsignment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelProductCategoryValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelProductCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCelcius)).EndInit();
            
            ((System.ComponentModel.ISupportInitialize)(this.labelPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelTemperature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private PreCom.Controls.PreComInput2 txtTemperature;
        private Resco.Controls.CommonControls.TransparentLabel labelTemperature;
        private PreCom.Controls.PreComInput2 txtComment;
        private Resco.Controls.CommonControls.TransparentLabel labelComment;
        private Resco.Controls.CommonControls.TransparentLabel labelPicture;
        
        private Com.Bring.PMP.PreComFW.Shared.Controls.MessageControl messageControl;
        private Resco.Controls.CommonControls.TransparentLabel labelCelcius;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignmentValue;
        private Resco.Controls.CommonControls.TransparentLabel labelConsignment;
        private Resco.Controls.CommonControls.TransparentLabel labelProductCategoryValue;
        private Resco.Controls.CommonControls.TransparentLabel labelProductCategory;
    }
}
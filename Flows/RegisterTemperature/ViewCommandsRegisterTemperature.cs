﻿using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTemperature
{
    class ViewCommandsRegisterTemperature
    {
        /// <summary>
        /// method for show Register temperature view..
        /// </summary>
        internal FormRegisterTemperature ShowRegisterTemperatureView(ViewEventDelegate viewEventHandler, FormRegisterTemperatureData formDataTemperature, FlowRegisterTemperatureData flowData)
        {
             formDataTemperature.ProductCategory = GetProductCategory(flowData);
            var view = ViewCommands.ShowView<FormRegisterTemperature>(formDataTemperature);
            view.SetEventHandler(viewEventHandler);
            return view;
        }
        /// <summary>
        /// method for get product category for consignment...
        /// </summary>
        internal string GetProductCategory(FlowRegisterTemperatureData flowData)
        {
            string productCategory = string.Empty;
            if (flowData != null && flowData.EntityMap != null && flowData.EntityMap.Values != null && flowData.EntityMap.Values.Any())
            {
                string consignmentId = flowData.ConsignmentEntity.ConsignmentId;
                Consignment consignment = flowData.EntityMap.GetConsignment(consignmentId);
                if (consignment != null)
                    productCategory = consignment.ProductCategory;
            }
            return productCategory;
        }

    }
}

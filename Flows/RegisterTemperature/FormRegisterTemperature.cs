﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System.Windows.Forms;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTemperature
{
    public partial class FormRegisterTemperature : BaseForm
    {
        #region "Variables and constructor"
        private FormRegisterTemperatureData _formData;
        private MultiButtonControl _tabButtons;
        public FormRegisterTemperature()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            SetTextToGui();
            InitOnScreenKeyBoardProperties();
        }
        private void SetTextToGui()
        {

            _tabButtons = new MultiButtonControl();
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Camera, ButtonPhotoClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonConfirmClick, TabButtonType.Confirm));
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
            
            labelPicture.Text = GlobalTexts.TakePictureConfirmation;
            labelComment.Text = GlobalTexts.OptionalComment;
            labelTemperature.Text = GlobalTexts.Temperature + "(+/-)";
            labelModuleName.Text = GlobalTexts.RegisterTemperature;

            labelConsignment.Text = GlobalTexts.ConsignmentOrItem + ":";
            labelProductCategory.Text = GlobalTexts.ProductCategory + ":";
            labelPicture.Text = GlobalTexts.TakePictureConfirmation;
            labelComment.Text = GlobalTexts.OptionalComment;
            labelTemperature.Text = GlobalTexts.Temperature + "(+/-)";
            labelModuleName.Text = GlobalTexts.RegisterTemperature;
        }
        private void InitOnScreenKeyBoardProperties()
        {
            //var keyBoardProperty = new OnScreenKeyboardProperties("Enter Temperature", KeyPadTypes.Numeric, KeyPadTypes.Qwerty);
            //keyBoardProperty.TextChanged = ConsignmentNumberTextChanged;
            txtTemperature.Tag = new OnScreenKeyboardProperties(GlobalTexts.Temperature, KeyPadTypes.Numeric, KeyPadTypes.Symbol)
                                     {
                                         TextChanged = TemperatureTextChanged
                                     };
            txtComment.Tag = new OnScreenKeyboardProperties(GlobalTexts.Comment);//, KeyPadTypes.Qwerty);
        }

        private void TemperatureTextChanged(bool userFinishedTyping, string textentered)
        {
            if (userFinishedTyping)
            {
                ValidateTemperature(textentered);
            }
        }

        private void ValidateTemperature(String temperature)
        {
            if (temperature != string.Empty)
            {
                TemperatureEntered(temperature);
            }
        }

        public void TemperatureEntered(string temperature)
        {
            ViewEvent.Invoke(RegisterTemperatureViewEvent.Validate, temperature);
        }

        public override void OnShow(IFormData formData)
        {
            _formData = (FormRegisterTemperatureData)formData;
            if(_formData!=null)
            {
                ClearFields();
                labelConsignmentValue.Text = _formData.ConsignmentItemNumber;
                labelProductCategoryValue.Text = _formData.ProductCategory;
            }
        }

      
        /// <summary>
        /// clear the fields..
        /// </summary>
        public void ClearFields()
        {
            txtComment.Text = string.Empty;
            txtTemperature.Text = string.Empty;
            labelConsignmentValue.Text = string.Empty;
            labelProductCategoryValue.Text = string.Empty;
            messageControl.ShowMessage("", MessageState.Empty);
        }

        public override void OnUpdateView(IFormData iformData)
        {
            _formData = (FormRegisterTemperatureData)iformData;
            if (_formData != null && _formData.MessageHolder != null)
            {
                if(_formData.MessageHolder.State != MessageState.Empty)
                {
                    var messageHolder = _formData.MessageHolder;
                    messageControl.ShowMessage(messageHolder.Text, messageHolder.State);    
                }
                else
                {
                    messageControl.ClearMessage();
                }
            }
        }
        #endregion

        #region "Events"

        /// <summary>
        /// button for back from this screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormRegisterTemperature method FormRegisterTemperature.ButtonBackClick");
                ViewEvent.Invoke(RegisterTemperatureViewEvent.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterTemperature.ButtonBackClick");
            }
        }

        /// <summary>
        /// ok/ enter button for send consignment & consignment item event to tm..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonConfirmClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormRegisterTemperature method FormRegisterTemperature.ButtonConfirmClick");

                if (_formData != null)
                {
                    _formData.Temperature = txtTemperature.Text.Trim();
                    _formData.EventComment = txtComment.Text.Trim();
                    _formData.MessageHolder = null;
                    ViewEvent.Invoke(RegisterTemperatureViewEvent.Ok, _formData.Temperature, _formData.EventComment);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterTemperature.ButtonConfirmClick");
            }
        }
        /// <summary>
        /// button click for call  camera subflow...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonPhotoClick(object sender, EventArgs e)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormRegisterTemperature method FormRegisterTemperature.ButtonPhotoClick");
                ViewEvent.Invoke(RegisterTemperatureViewEvent.Camera);

            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterTemperature.ButtonPhotoClick");
            }
        }

        private void TxtTemperatureKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                    txtComment.Focus();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterTemperature.TxtTemperatureKeyUp");
            }
        }


        #endregion

    }
    public class FormRegisterTemperatureData : BaseFormData
    {
        public string ConsignmentItemType;
        public string ConsignmentItemNumber;
        public string Temperature;
        public string EventComment;
        public MessageHolder MessageHolder;
        public string ProductCategory;
    }
    public class RegisterTemperatureViewEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Ok = 1;
        public const int Camera = 2;
        public const int Validate = 3;
    }
}
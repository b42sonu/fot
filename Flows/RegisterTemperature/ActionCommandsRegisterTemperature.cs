﻿using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PreCom.Core.Communication;
using System;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Communication;
using Com.Bring.PMP.PreComFW.Shared.Flows.Camera;
using System.Text.RegularExpressions;
using Com.Bring.PMP.PreComFW.Shared.Constants;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTemperature
{
    class ActionCommandsRegisterTemperature
    {
        private readonly MessageHolder _messageHolder;
        private static ICommunicationClient _communicationClient;
        public ActionCommandsRegisterTemperature(ICommunicationClient comClient, MessageHolder messageHolder)
        {
            _messageHolder = messageHolder;
            _communicationClient = comClient;
        }
        /// <summary>
        /// check is temperature is valid or not...
        /// </summary>
        /// <param name="formRegisterTemperatureData"></param>
        /// <returns></returns>
        internal bool IsValidTemperature(FormRegisterTemperatureData formRegisterTemperatureData)
        {
             bool result = formRegisterTemperatureData != null && !string.IsNullOrEmpty(formRegisterTemperatureData.Temperature);

            if (formRegisterTemperatureData != null)
            {
                if (!result)
                {
                    _messageHolder.Update(GlobalTexts.EnterTemprature, MessageState.Error);
                    formRegisterTemperatureData.MessageHolder = _messageHolder;
                }
                else
                {
                    var regex = new Regex(@"^[-+]?[0-9]+([\.][0-9]{1,})?$");//Numerical plus single 
                    if (regex.IsMatch(formRegisterTemperatureData.Temperature))
                    {
                        _messageHolder.Clear();
                    }
                    else
                    {
                        result = false;
                        _messageHolder.Update(GlobalTexts.ValidateTemprature, MessageState.Error);
                    }

                    formRegisterTemperatureData.MessageHolder = _messageHolder;
                }
            }

            return result;
        }
        //TODO :: Write Unit Test
        /// <summary>
        /// This method, validates the result from flow FlowCamera and decides which will be the next state.
        /// </summary>
        /// <returns>Returns next state to execute.</returns>
        internal FlowStateRegisterTemperature ValidateFlowCameraResult(FlowResultCamera subflowResult, out string photoProcessGuid, FormRegisterTemperatureData formRegisterTemperatureData)
        {
             photoProcessGuid = null;
            FlowStateRegisterTemperature resultState;
            _messageHolder.Clear();
            switch (subflowResult.State)
            {
                case FlowResultState.Ok:
                    photoProcessGuid = subflowResult.FolderName;
                    resultState = FlowStateRegisterTemperature.UpdateView;

                    _messageHolder.Update(subflowResult.Message, MessageState.Information);
                    break;

                case FlowResultState.Cancel:
                    resultState = FlowStateRegisterTemperature.UpdateView;

                    _messageHolder.Update(subflowResult.Message, MessageState.Warning);
                    break;

                default:
                    resultState = FlowStateRegisterTemperature.UpdateView;
                    Logger.LogEvent(Severity.Error, "Encountered unknown State");
                    break;
            }
            formRegisterTemperatureData.MessageHolder = _messageHolder;
            return resultState;
        }

        #region "Goods event detail"

        /// <summary>
        /// send goods event for regsiter temperature...
        /// </summary>
        public bool SendRegisterTemperatureGoodsEvent(ConsignmentEntity consignmentEntity, OperationProcess operationProcess, decimal registerTemperature, string eventComment, string photoProcessGuid)
        {
            bool result = false;

            try
            {
                PreComFW.CommunicationEntity.GoodsEvents.GoodsEvents goodsEvent = GoodsEventHelper.CreateGoodsEvent(consignmentEntity);

                if (goodsEvent != null)
                {
                    //set temerature data and comment in event information...
                    if (goodsEvent.EventInformation != null)
                    {
                        goodsEvent.EventInformation.EventTemperature = registerTemperature;
                        goodsEvent.EventInformation.EventTemperatureSpecified = true;
                        goodsEvent.EventInformation.EventComment = eventComment + " " + registerTemperature;
                    }

                    //attach photo ...
                    if (string.IsNullOrEmpty(photoProcessGuid) == false)
                        goodsEvent = GoodsEventHelper.AddPhotoInGoodsEvent(goodsEvent, consignmentEntity, photoProcessGuid);

                    var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.GoodsEventsEventInformation, consignmentEntity.OrderNumber);

                    if (_communicationClient != null)//CommunicationClient.Instance != null)
                    {
                        _communicationClient.SendGoodsEvent(goodsEvent, transaction);
                        result = true;
                    }
                }
            }


            catch (PreCom.Core.NotLoggedInExeption ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsRegisterTemperature.SendRegisterTemperatureGoodsEvent");
            }
            catch (Exception ex)
            {
                //FlowResult.State = FlowResultState.Error;
                Logger.LogException(ex, "ActionCommandsRegisterTemperature.SendRegisterTemperatureGoodsEvent");
            }

            return result;
        }

        #endregion
        //TODO :: Write Unit Test
        /// <summary>
        /// delete the folder of captured picture after adding images in goods event and send to server....
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        internal bool DeletePicturesFolder(string folderName)
        {
            try
            {
                return GoodsEventHelper.DeletePicturesFolder(folderName);

            }
            catch (Exception ex)
            {
                Logger.LogExpectedException(ex, "ActionCommandsRegisterTemperature.DeletePicturesFolder");
            }
            return false;
        }
    }
}

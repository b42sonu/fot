﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SetSignPriority;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow
{
    class ActionCommandsSignature
    {
        private readonly MessageHolder _messageHolder;
        int _messageIndex;

        //TODO :: Write Unit Test
        public ActionCommandsSignature(MessageHolder messageHolder)
        {
            _messageHolder = messageHolder;
        }

        enum EnumSignatureMessages
        {
            NameNotFilled = 1,
            SignatureIsNotFilled = 2,
            NameIsInvalid = 4,
            SignatureIsInvalid = 8
        }

        //TODO :: Write Unit Test
        internal bool ValidateSignature(Signature signature, SignaturePriority signaturePriority, out string messageTitle)
        {
            bool valid = true;
            _messageIndex = 0;
            
            if (signaturePriority == SignaturePriority.SignatureAndName)
            {
                if (string.IsNullOrEmpty(signature.PrintedName.Trim()))
                {
                    _messageIndex += (int)EnumSignatureMessages.NameNotFilled;
                    valid = false;
                }
                else
                {
                    valid = IsFullnameLegal(signature.PrintedName);
                    if (!valid)
                        _messageIndex += (int)EnumSignatureMessages.NameIsInvalid;
                }
            }


            int minimalBlackPixels = SettingDataProvider.Instance.GetDeviceSetting(DeviceSettingNames.SignatureMinimalBlackPixels, 1000);
            bool signatureValid = signature.Sketch != null && IsSignatureValid(GetBitmapFromByteArray(signature.Sketch), minimalBlackPixels);
#pragma warning disable 618
            bool isAutoTesting = PreCom.Application.Instance.Settings.Read(PreCom.Application.Instance.Platform, "AutoTesting", false, false);//added as per gaurav's request for automation testing
#pragma warning restore 618
            if (!signatureValid && !isAutoTesting)
            {
                valid = false;
            }

            _messageHolder.Clear();
            
            messageTitle = GlobalTexts.IncorrectSignature;
            if (valid == false)
            {
                Logger.LogEvent(Severity.Debug, "ValidateSignature;_messageIndex=" + _messageIndex);
                switch (_messageIndex)
                {
                    case 1:
                    case 4:
                        _messageHolder.Update(GlobalTexts.NameMissing, MessageState.Error);
                        break;

                    case 2:
                        _messageHolder.Update(GlobalTexts.SignatureMissing,MessageState.Error);
                        break;

                    case 3:
                    case 6:
                    case 9:
                        _messageHolder.Update(GlobalTexts.NameAndSignatureMissing, MessageState.Error);
                        break;

                    case 8:// ? never used?
                        _messageHolder.Update(GlobalTexts.SignatureMissing, MessageState.Error);
                        break;

                    case 12:// ? never used?
                        _messageHolder.Update(GlobalTexts.NameAndSignatureMissing, MessageState.Error);
                        break;
                }
            }
            return valid;
        }

        //TODO :: Write Unit Test
        /// <summary>
        /// full name length check (between 2 and 28)
        /// </summary>
        /// <param name="fullname"></param>
        /// <returns></returns>
        private Boolean IsFullnameLegal(string fullname)
        {
            int nameMinimalChar = SettingDataProvider.Instance.GetDeviceSetting(DeviceSettingNames.SignatureNameMinimalChar, 2);
            int nameMaximumChar = SettingDataProvider.Instance.GetDeviceSetting(DeviceSettingNames.SignatureNameMaximumChar, 28);

            return fullname.Length >= nameMinimalChar && fullname.Length <= nameMaximumChar;
        }

        //TODO :: Write Unit Test
        private Bitmap GetBitmapFromByteArray(byte[] bmpArray)
        {
            // First, create a memory a stream where the
            // passed byte array will be stored
            using (var bitmapMs = new MemoryStream())
            {
                // Write the byte array to the memory stream
                bitmapMs.Write(bmpArray, 0, bmpArray.Length);

                // Create a new bitmap object from the memory stream
                var theBitmap = new Bitmap(bitmapMs);

                // Finally, return the bitmap
                return theBitmap;
            }
        }

        //TODO :: Write Unit Test
        private bool IsSignatureValid(Bitmap bitmap, int minimumBlackPixels)
        {
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format16bppRgb555);
            try
            {
                int bytes = data.Width * data.Height * 2; // 16 bits per pixel
                var rgbValues = new byte[bytes];
                System.Runtime.InteropServices.Marshal.Copy(data.Scan0, rgbValues, 0, bytes);
                bool empty = true;
                int blackPixelrx2 = 0; // This figure is twice as many actually responded because we have 16 bit color
                foreach (byte t in rgbValues)
                {
                    // Sjekk om det er noe svart i bildet
                    if (t == 0x00)
                    {
                        blackPixelrx2++;
                        if (blackPixelrx2 > minimumBlackPixels)
                        {
                            empty = false;
                            break;
                        }
                    }
                }

                if (blackPixelrx2 < minimumBlackPixels / 3)
                {
                    _messageIndex += (int)EnumSignatureMessages.SignatureIsNotFilled;
                }
                else if (blackPixelrx2 < minimumBlackPixels)
                {
                    _messageIndex += (int)EnumSignatureMessages.SignatureIsInvalid;
                }
                return !empty;
            }
            finally
            {
                bitmap.UnlockBits(data);
            }
        }


    }
}

﻿using System;
using System.Drawing;
using System.IO;
using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SetSignPriority;
using Resco.Controls.OutlookControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow
{
    public partial class FormSignatureView : BaseForm
    {
        private readonly MultiButtonControl _multiButtonControl;
        private const string ButtonBack = "ButtonBack";
        private const string ButtonOk = "ButtonOk";
        private const string ButtonErase = "ButtonErase";
        //FormSignatureView _formMain;
        public FormSignatureView()
        {

            InitializeComponent();
            _multiButtonControl = new MultiButtonControl();
            touchPanel.Controls.Add(_multiButtonControl);
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
            {
                touchPanel.BackgroundImage = image;
            }
            InitOnScreenKeyBoardProperties();
            SetTextToGui();
            SetStandardControlProperties(lblProcessHeader, null, null, null, null, null, null);
            AddButtons();
            _multiButtonControl.GetButton(ButtonErase).BringToFront();

        }


        private void AddButtons()
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "Executing FormReconciliation.AddButtonsAccordintToProcess()");

                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Back, BtnBackClick, TabButtonType.Cancel) { Name = ButtonBack });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Erase, BtnEraseClick, ButtonErase) { Enabled = false });
                _multiButtonControl.ListButtons.Add(new TabButton(GlobalTexts.Ok, BtnOkClick, TabButtonType.Confirm) { Name = ButtonOk });
                _multiButtonControl.GenerateButtons();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormReconciliation.SetTextToGui");
            }
        }

        private void SignatureHandler(object sender, ContentsChangedEventArgs args)
        {
            if (inkbSignature.Image != null)
            {
                _multiButtonControl.SetButtonEnabledState(ButtonErase, true);
            }
        }

        private void SetTextToGui()
        {
            lblInfo.Text = GlobalTexts.SignatureInfo;
            lblNoOfConsigItm.Text = GlobalTexts.NrOfConsignments;
            lblFullName.Text = GlobalTexts.CustomerName + ":";
            lblSignature.Text = GlobalTexts.Signature + ":";
        }

        private void InitOnScreenKeyBoardProperties()
        {
            txtFullName.Tag = new OnScreenKeyboardProperties(GlobalTexts.FullName, KeyPadTypes.Qwerty);
        }

        /// <summary>
        /// Gets the signature.
        /// </summary>
        /// <returns></returns>
        private Signature ReadSignature()
        {
            var signature = new Signature { PrintedName = txtFullName.Text.Trim() };

            try
            {
                using (var signatureStream = new MemoryStream())
                {
                    using (Image image = inkbSignature.Image)
                    {
                        image.Save(signatureStream, System.Drawing.Imaging.ImageFormat.Png);
                    }

                    signature.Sketch = signatureStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Severity.Error, "Method : FormSignatureView.ReadSignature " + ex.Message);
            }
            return signature;
        }

        private void BtnOkClick(object sender, EventArgs e)
        {
            try
            {
                var signature = ReadSignature();

                ViewEvent.Invoke((int)ViewStateSign.Ok, signature);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSignatureView.BtnOkClick");
            }
        }

        /// <summary>
        /// Handles the Click event of the btnErase control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event iformData.</param>
        private void BtnEraseClick(object sender, EventArgs e)
        {
            try
            {
                var modalMessageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Erase, GlobalTexts.EraseSignature, Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);
                if (modalMessageBoxResult == GlobalTexts.Yes)
                {
                    ClearSignatureArea();
                    _multiButtonControl.SetButtonEnabledState(ButtonErase, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSignatureView.BtnEraseClick");
            }
        }

        private void ClearSignatureArea()
        {
            inkbSignature.Clear();
            inkbSignature.Refresh();
        }

        private void BtnBackClick(object sender, EventArgs e)
        {
            try
            {
                if (ViewEvent == null)
                    return;

                ClearSignatureArea();
                ViewEvent.Invoke((int)ViewStateSign.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormSignatureView.BtnBackClick");
            }
        }
        public override void OnShow(IFormData formData)
        {
            ClearField();
            inkbSignature.ContentsChanged += SignatureHandler;
            OnUpdateView(formData);

        }

        public override void OnUpdateView(IFormData iformData)
        {
            if (iformData != null)
            {
                var formDataSignatureView = (FormDataSignatureView)iformData;
                lblNoOfConsigItm.Text = GlobalTexts.ConsignmentItemCount + ": " +
                                        formDataSignatureView.ConsignmentItemCount;


                if (formDataSignatureView.ShowServiceCount)
                {
                    lblNoOfServices.Visible = true;
                    lblNoOfServices.Text = GlobalTexts.ServicesAdded + ": " +
                                           formDataSignatureView.ServiceCount;
                }
                else
                    lblNoOfServices.Visible = false;

                //Show Load Carrier count
                if (formDataSignatureView.ShowLoadCarrierCount)
                {
                    lblNoOfLoadCarriers.Visible = true;
                    lblNoOfLoadCarriers.Text = GlobalTexts.NoOfCarriers + ": " + formDataSignatureView.LoadCarrierCount;
                }
                else
                {
                    lblNoOfLoadCarriers.Visible = false;
                }

                if (formDataSignatureView.HeaderText != null)
                    lblProcessHeader.Text = formDataSignatureView.HeaderText;

                if (formDataSignatureView.MessageHolder.State == MessageState.Error)
                {
                    GuiCommon.ShowModalDialog(formDataSignatureView.ValidationMessageTitle, formDataSignatureView.MessageHolder.Text, Severity.Error, GlobalTexts.Ok);
                    if (txtFullName.Visible)
                        txtFullName.Focus();
                    BusyUtil.Reset();
                }
                else
                {
                    ClearSignatureArea();
                    txtFullName.Text = formDataSignatureView.RecipientName;
                }

                //below if statement used for set text box and label other visible true other wise if remove this then textbox and lable remain false and 
                //also it is not handle by our architecture ..
                Visible = true;

                if (formDataSignatureView.SignaturePriority == SignaturePriority.WithoutSignatureAndName)
                    SetNameAndSignatureControlVisibility(false);
                else
                {
                    SetNameAndSignatureControlVisibility(true);
                    lblFullName.Visible = txtFullName.Visible = formDataSignatureView.SignaturePriority == SignaturePriority.SignatureAndName;

                    if (formDataSignatureView.SignatureOwnerTitle != null &&
                        formDataSignatureView.SignaturePriority == SignaturePriority.SignatureAndName)
                        lblFullName.Text = formDataSignatureView.SignatureOwnerTitle + ":";


                    SetKeyboardState(KeyboardState.Alpha);
                }
            }

        }
        private void SetNameAndSignatureControlVisibility(bool visible)
        {
            lblFullName.Visible = visible;
            txtFullName.Visible = visible;
            lblSignature.Visible = visible;
            inkbSignature.Visible = visible;
        }

        private void ClearField()
        {

            ClearSignatureArea();
            txtFullName.Text = string.Empty;
            lblNoOfServices.Visible = false;

        }
    }
    public enum ViewStateSign
    {
        Ok,
        Erase,
        Back
    }

    public class FormDataSignatureView : BaseFormData
    {
        public int ConsignmentItemCount { get; set; }
        public string SignatureOwnerTitle { get; set; }
        public MessageHolder MessageHolder { get; set; }
        public SignaturePriority SignaturePriority { get; set; }
        public string ValidationMessageTitle { get; set; }
        public string RecipientName { get; set; }
        public bool ShowLoadCarrierCount { get; set; }
        public int LoadCarrierCount { get; set; }

        //added two more property related to alystra operation list..
        public int ServiceCount { get; set; }
        public bool ShowServiceCount { get; set; }
    }
}
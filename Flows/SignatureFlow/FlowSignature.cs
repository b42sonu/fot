﻿using Com.Bring.PMP.PreComFW.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using System;
using Com.Bring.PMP.PreComFW.Shared.VasSubFlows.SetSignPriority;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow
{
    /// <summary>
    /// Enum which specifies the collection of stateSignature for this flow.
    /// </summary>
    public enum FlowStateSignature
    {
        ActionValidateSignature,

        ViewCommands,
        ViewSignature,
        ViewUpdateSignature,
        ThisFlowComplete,
        ThisFlowCancel
    }

    public class FlowDataSignature : BaseFlowData
    {
        // For flow with signature only
        public FlowDataSignature(string headerText, EntityMap entityMap, SignaturePriority signaturePriority)
        {
            HeaderText = headerText;
            NumberOfConsignmentItems = entityMap.ScannedConsignmentItemsTotalCount;
            NumberOfLoadCarriers = entityMap.ScannedLoadCarrierTotalCount;
            SignaturePriority = signaturePriority;
        }

        public FlowDataSignature(string headerText, int consignmentItemCount)
        {
            HeaderText = headerText;
            NumberOfConsignmentItems = consignmentItemCount;
            SignaturePriority = SignaturePriority.SignatureAndName;
        }

        public int NumberOfConsignmentItems { get; set; }
        public string SignatureOwnerTitle { get; set; } //"Customer name" is default
        public SignaturePriority SignaturePriority { get; set; }
        public string RecipientName { get; set; }
        public bool ShowLoadCarrierCount { get; set; }
        public int NumberOfLoadCarriers { get; set; }

        //added two more property related to alystra operation list..
        public int ServiceCount { get; set; }
        public bool ShowServiceCount { get; set; }
    }

    public class FlowResultSignature : BaseFlowResult
    {
        public Signature Signature;
    }

    /// <summary>
    /// This class acts as a flow for Signature.
    /// </summary>
    public class FlowSignature : BaseFlow
    {
        private readonly ActionCommandsSignature _actionCommands;
        private readonly ViewCommandsSignature _viewCommands;

        private FlowDataSignature _flowData;
        private readonly FlowResultSignature _flowResult;
        private readonly MessageHolder _messageHolder;
        private Signature _signature;
        
        private string _invalidMessageTitle = GlobalTexts.Error;
        public FlowSignature()
        {
            _messageHolder = new MessageHolder();
            _viewCommands = new ViewCommandsSignature(_messageHolder);
            _actionCommands = new ActionCommandsSignature(_messageHolder);
            _flowResult = new FlowResultSignature();
        }


        public FlowResultSignature GetFlowResult()
        {
            return _flowResult;
        }

        /// <summary>
        /// Activate flow
        /// </summary>
        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Initializing flow FlowSignature");
            ModelMain.IsOnSensitiveScreenOrModule = true;
            _flowData = (FlowDataSignature)flowData;
            ExecuteState(FlowStateSignature.ViewSignature);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateSignature)state);
        }

        public void ExecuteState(FlowStateSignature stateSignature)
        {
            if (stateSignature > FlowStateSignature.ViewCommands)
            {
                ExecuteViewState(stateSignature);
            }
            else
            {
                ExecuteActionState(stateSignature);
            }
        }

        private void ExecuteViewState(FlowStateSignature state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowSignature;" + state);
                switch (state)
                {
                    case FlowStateSignature.ViewSignature:
                        _viewCommands.ShowFormSignatureView(SignatureViewEventHandler, _flowData, _invalidMessageTitle);
                        break;

                    case FlowStateSignature.ViewUpdateSignature:
                        var formData = new FormDataSignatureView
                        {
                            HeaderText = _flowData.HeaderText,
                            ConsignmentItemCount = _flowData.NumberOfConsignmentItems,
                            SignatureOwnerTitle = _flowData.SignatureOwnerTitle,
                            MessageHolder = _messageHolder,
                            SignaturePriority = _flowData.SignaturePriority,
                            ValidationMessageTitle = _invalidMessageTitle,
                            ServiceCount = _flowData.ServiceCount,
                            ShowServiceCount = _flowData.ShowServiceCount,
                            RecipientName = _flowData.RecipientName,
                            LoadCarrierCount = _flowData.NumberOfLoadCarriers,
                            ShowLoadCarrierCount = _flowData.ShowLoadCarrierCount
                        };
                        CurrentView.OnUpdateView(formData);
                        break;

                    case FlowStateSignature.ThisFlowComplete:
                        _flowResult.Signature = _signature;
                        _flowResult.State = FlowResultState.Ok;
                        EndFlow();
                        break;

                    case FlowStateSignature.ThisFlowCancel:
                        _flowResult.Message = GlobalTexts.SignatureCancelled;
                        _flowResult.State = FlowResultState.Cancel;
                        EndFlow();
                        break;

                    default:
                        throw new Exception("Encountered unhandled FlowStateSignature;" + state);
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowSignature.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                EndFlow();
            }
        }



        /// <summary>
        /// This method executes the state for the flow
        /// </summary>
        public void ExecuteActionState(FlowStateSignature state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowSignature;" + state);
                    //check the state and execute the same
                    //bool result;
                    switch (state)
                    {
                        case FlowStateSignature.ActionValidateSignature:
                            bool valid = _actionCommands.ValidateSignature(_signature, _flowData.SignaturePriority, out _invalidMessageTitle);
                            state = valid
                                                 ? FlowStateSignature.ThisFlowComplete
                                                 : FlowStateSignature.ViewUpdateSignature;//ViewSignature;
                            break;


                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateSignature.ViewCommands);
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowSignature.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateSignature.ThisFlowComplete;
            }
            ExecuteViewState(state);
        }


        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Ending flow FlowSignature");
            BaseModule.EndSubFlow(_flowResult);
            ModelMain.IsOnSensitiveScreenOrModule = false;
        }

        /// <summary>
        /// handles viewevents from signatureflow
        /// </summary>
        /// <param name="action"></param>
        /// <param name="data"></param>
        public void SignatureViewEventHandler(int action, object[] data)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowSignature.SignatureViewEventHandler");
            switch ((ViewStateSign)action)
            {
                case ViewStateSign.Ok:
                    _signature = (Signature)data[0];
                    ExecuteState(FlowStateSignature.ActionValidateSignature);
                    break;

                case ViewStateSign.Back:
                    ExecuteState(FlowStateSignature.ThisFlowCancel);
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Unhandled event viewState encountered");
                    break;

            }
        }



    }
}

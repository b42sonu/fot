﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.SignatureFlow
{
    class ViewCommandsSignature
    {
        private readonly MessageHolder _messageHolder;

        public ViewCommandsSignature(MessageHolder messageHolder)
        {
            _messageHolder = messageHolder;
        }

        /// <summary>
        /// Gets the view without showing. Enables clearing of fields without flickering
        /// </summary>
        /// <returns></returns>
        internal FormSignatureView GetFormSignatureView()
        {
            var view = ViewCommands.GetOrCreateView<FormSignatureView>();
            return view;
        }

        internal void ShowFormSignatureView(ViewEventDelegate viewEventHandler, FlowDataSignature flowData, string invalidMessageTitle)
        {
            var formData = new FormDataSignatureView
            {
                HeaderText = flowData.HeaderText,
                ConsignmentItemCount = flowData.NumberOfConsignmentItems,
                SignatureOwnerTitle = flowData.SignatureOwnerTitle,
                MessageHolder = _messageHolder,
                SignaturePriority = flowData.SignaturePriority,
                ValidationMessageTitle = invalidMessageTitle,
                ServiceCount = flowData.ServiceCount,
                ShowServiceCount = flowData.ShowServiceCount,
                RecipientName = flowData.RecipientName,
                LoadCarrierCount = flowData.NumberOfLoadCarriers,
                ShowLoadCarrierCount = flowData.ShowLoadCarrierCount
            };

            var view = ViewCommands.ShowView<FormSignatureView>(formData);
            view.SetEventHandler(viewEventHandler);
        }
    }
}

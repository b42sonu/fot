﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Views;
using System.Threading;
using Com.Bring.PMP.PreComFW.Shared.Utility;
namespace Com.Bring.PMP.PreComFW.Shared.Flows
{

    public enum PushMessageFlowStates
    {
        Accept,
        Reject,
        AcceptAll,
        StartFlow,
        EndFlow,
        OperationListReceived,
        ShowUpdatedOperationList,
        DriverAvailabilityResponseReceived,
        ShowConsignmentDetail,
        ProcessNextMessage,
        RefreshOperationList,
        ShowOperationListUpdateSummary,
        UpdatedOprListStopSelected
    }

    public enum PushMessageViews
    {
        FormOperation,
        FormConsignmentDetail
    }

    public class FlowPushMessage : BaseFlow
    {
        AutoResetEvent _commandFinishedEvent;
        /// <summary>
        /// Specifes the module that is taking the responsibility to hanle the push message currently.
        /// </summary>
        public BaseModule FlowExecutor
        {
            get { return CommunicationModel.Instance.CurrentPushMessageHandler; }
        }
        /// <summary>
        /// 
        /// </summary>
        public override void InitFlowobject[] startupData()
        {
            Logger.LogEvent(Severity.Debug, "FlowPushMessage initialized.");
            GetNextMessage();
        }

        /// <summary>
        /// This method takes no of state as parameter and executes the related state. 
        /// </summary>
        /// <param name="state">Specifies the state that need to execute.</param>
        public override void ExecuteState(int state)
        {
            ExecuteState((PushMessageFlowStates)state);
        }

        /// <summary>
        /// This method is responsible for execute the next pushmessageflow state.
        /// </summary>
        /// <param name="state">Specifies the state that need to execute.</param>
        public void ExecuteState(PushMessageFlowStates state)
        {
            try
            {
                bool result;
                int flowState;
                switch (state)
                {
                    case PushMessageFlowStates.OperationListReceived:
                        Logger.LogEvent(Severity.Debug, "OperationListReceived state executed.");
                        result = PushMessageCommands.OperationListReceived();
                        
                        flowState = result
                                            ? (int)PushMessageFlowStates.ShowUpdatedOperationList
                                            : (int)PushMessageFlowStates.ProcessNextMessage;
                        ExecuteState(flowState);
                        break;
                    case PushMessageFlowStates.ShowUpdatedOperationList:
                        Logger.LogEvent(Severity.Debug, "ShowUpdatedOperationList state executed.");
                        _commandFinishedEvent = new AutoResetEvent(false);
                        PushMessageCommands.ShowUpdatedOperationList(_commandFinishedEvent);
                        _commandFinishedEvent.WaitOne();
                        var form = (FormReceiveOperationList)CommunicationModel.Instance.CurrentPushMessageHandler.CurrentView;
                        form.SetEventHandler(UpdatedOperationListEventsHandler);
                        
                        break;
                    case PushMessageFlowStates.ProcessNextMessage:
                        Logger.LogEvent(Severity.Debug, "ProcessNextMessage state executed.");
                        GetNextMessage();
                        break;
                    case PushMessageFlowStates.DriverAvailabilityResponseReceived:
                        Logger.LogEvent(Severity.Debug, "DriverAvailabilityResponseReceived state executed.");
                        PushMessageCommands.DriverAvailabilityResponseReceived();
                        ExecuteState(PushMessageFlowStates.ProcessNextMessage);
                        
                        break;
                    case PushMessageFlowStates.ShowConsignmentDetail:
                        Logger.LogEvent(Severity.Debug, "ShowConsignmentDetail state executed.");
                        _commandFinishedEvent = new AutoResetEvent(false);
                        PushMessageCommands.ShowConsignmentDetail(_commandFinishedEvent);
                        _commandFinishedEvent.WaitOne();
                        var formConsignDetail = (FormConsignmentDetail)CommunicationModel.Instance.CurrentPushMessageHandler.CurrentView;
                        formConsignDetail.SetEventHandler(UpdatedOperationListEventsHandler);
                        
                        
                        break;
                    case PushMessageFlowStates.RefreshOperationList:
                        Logger.LogEvent(Severity.Debug, "RefreshOperationList state executed.");
                        PushMessageCommands.RefreshOperationList();
                        ExecuteState(PushMessageFlowStates.ProcessNextMessage);
                        
                        break;
                    case PushMessageFlowStates.ShowOperationListUpdateSummary:
                        Logger.LogEvent(Severity.Debug, "ShowOperationListUpdateSummary state executed.");
                        result = PushMessageCommands.ShowOperationListUpdateSummary();
                        flowState = result
                                            ? (int)PushMessageFlowStates.RefreshOperationList
                                            : (int)PushMessageFlowStates.ProcessNextMessage;
                        ExecuteState(flowState);
                        
                        break;
                    case PushMessageFlowStates.UpdatedOprListStopSelected:
                        Logger.LogEvent(Severity.Debug, "UpdatedOprListStopSelected state executed.");
                        PushMessageCommands.GetFilteredOperationsForStop();
                        
                        break;
                    case PushMessageFlowStates.Accept:
                        Logger.LogEvent(Severity.Debug, "Accept state executed.");
                        PushMessageCommands.AcceptRejectOperations("Accept");
                        
                        break;
                    case PushMessageFlowStates.Reject:
                        Logger.LogEvent(Severity.Debug, "Reject state executed.");
                        PushMessageCommands.AcceptRejectOperations("Reject");
                        
                        break;
                    case PushMessageFlowStates.AcceptAll:
                        Logger.LogEvent(Severity.Debug, "Accept All state executed.");
                        PushMessageCommands.AcceptRejectOperations("AcceptAll");

                        break;
                    case PushMessageFlowStates.EndFlow:
                        Logger.LogEvent(Severity.Debug, "EndFlow state executed.");
                        PushMessageCommands.EndFlow();
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowPushMessage.ExecuteState()");
                
            }
        }

        /// <summary>
        /// This method handles all events relate to updated operation list and executes the state 
        /// checking its type.
        /// </summary>
        /// <param name="eventName">Specifies the name of the event that occured.</param>
        /// <param name="data">specifies data sent by function, who raised the event.</param>
        public void UpdatedOperationListEventsHandler(int eventName, params object[] data)
        {
            try
            {
                switch (eventName)
                {
                    case UpdatedOperationListEvent.Accept:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, Accept event received.");
                        ExecuteState(PushMessageFlowStates.Accept);
                        break;
                    case UpdatedOperationListEvent.AcceptAll:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, AccceptAll event received.");
                        ExecuteState(PushMessageFlowStates.AcceptAll);
                        break;
                    case UpdatedOperationListEvent.Reject:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, Reject event received.");
                        ExecuteState(PushMessageFlowStates.Reject);
                        break;
                    case UpdatedOperationListEvent.Info:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, Info event received.");
                        ExecuteState(PushMessageFlowStates.ShowConsignmentDetail);
                        break;
                    case UpdatedOperationListEvent.BackFromInfo:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, BackFromInfo event received .");
                        ExecuteState(PushMessageFlowStates.ShowUpdatedOperationList);
                        break;
                    case UpdatedOperationListEvent.StopSelected:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, Stop Selected Event received.");
                        ExecuteState(PushMessageFlowStates.UpdatedOprListStopSelected);
                        break;
                    case UpdatedOperationListEvent.OperationListUpdateCompleted:
                        Logger.LogEvent(Severity.Debug, "In UpdatedOperationListEventsHandler of FlowPushMessage, OperationListUpdate event received.");
                        ExecuteState(PushMessageFlowStates.ShowOperationListUpdateSummary);
                        break;

                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowPushMessage.UpdatedOperationListEventsHandler()");
            }
           
        }

        public override void CleanupFlow()
        {
            ExecuteState((int)PushMessageFlowStates.EndFlow);
        }

        #region "Public Methods"

        /// <summary>
        /// This method will check the type of the push message and then will operate accordingly to get the exact type from the
        /// message(that is in xml)
        /// </summary>
        public void GetNextMessage()
        {
            try
            {
                
                if (Communication.CommunicationClient.Instance.MsgQueue!= null && Communication.CommunicationClient.Instance.MsgQueue.Count > 0)
                {
                    var newMessage = Communication.CommunicationClient.Instance.MsgQueue.Dequeue();
                    Logger.LogEvent(Severity.Debug, "Retrived next message from queue.");
                    CommunicationModel.Instance.CurrentPushMessage = newMessage.Message;
                    switch (newMessage.Type)
                    {
                        case "G30202_OperationList":
                        case "OperationList":
                            Logger.LogEvent(Severity.Debug, "Message of type operation list received.");
                            ExecuteState(PushMessageFlowStates.OperationListReceived);
                            break;
                        case "G30201_DriverAvailability":
                            Logger.LogEvent(Severity.Debug, "Message of type driver availability response received.");
                            ExecuteState(PushMessageFlowStates.DriverAvailabilityResponseReceived);
                            break;
                        default:
                            ExecuteState(PushMessageFlowStates.EndFlow);
                            break;
                    }
                }
                else
                {
                    Logger.LogEvent(Severity.Debug, "No message available in queue, so executing the end flow.");
                    ExecuteState(PushMessageFlowStates.EndFlow);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowPushMessage.GetNextMessage()");
            }
        }
        #endregion
    }
}

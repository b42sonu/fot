﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterVas
{
    public partial class FormRegisterVas : BaseForm
    {
        readonly MultiButtonControl _tabButtons = new MultiButtonControl();
        private List<VasDetail> _vasDetail;
        private FormDataRegisterVas _formData;
        readonly PdaVas _vasItem;
        public FormRegisterVas()
        {
            InitializeComponent();
            var image = GuiCommon.Instance.BackgroundImage;
            if (image != null)
                touchPanel.BackgroundImage = image;
            _vasItem = new PdaVas();
            AddButtons();
        }

        private void AddButtons()
        {
            labelModuleName.Text = GlobalTexts.SelectedVas;
            labelCustomer.Text = GlobalTexts.Customer;
            if (ModelUser.UserProfile.OrgUnitName != null)
                lblOrgnaisationUnitName.Text = ModelUser.UserProfile.OrgUnitName;
            labelConfirmedBy.Text = GlobalTexts.ConfirmedBy + " :";
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Back, ButtonBackClick));
            _tabButtons.ListButtons.Add(new TabButton(GlobalTexts.Ok, ButtonOkClick, TabButtonType.Confirm) { Name = "Ok" });
            touchPanel.Controls.Add(_tabButtons);
            _tabButtons.GenerateButtons();
        }
        public override void OnShow(IFormData formData)
        {
            if (formData != null)
            {
                textBoxAmount.Text = string.Empty;
                textBoxConfirmBy.Text = string.Empty;
                _formData = (FormDataRegisterVas)formData;
                _vasDetail = _formData.VasDetail;
                advancedListTime.DataSource = _vasDetail;
                labelModuleName.Text = _formData.HeaderText;
                if (_formData.Customer != null) labelCustomerValue.Text = _formData.Customer;
                if (_formData.Stop != null) labelStopValue.Text = _formData.Stop;
            }
        }

        private void TextBoxConfirmByTextChanged(object sender, EventArgs e)
        {
            EnableDisableOkButton();
        }

        private void TextBoxAmountTextChanged(object sender, EventArgs e)
        {
            EnableDisableOkButton();
        }

        private void EnableDisableOkButton()
        {
            var condition = advancedListTime.SelectedRows.Any() && !string.IsNullOrEmpty(textBoxAmount.Text) &&
                            !string.IsNullOrEmpty(textBoxConfirmBy.Text);
            _tabButtons.SetButtonEnabledState("Ok", condition);

        }



        private void OnActiveRowChanged(object sender, EventArgs e)
        {
            try
            {

                if (advancedListTime.SelectedRows.Any())
                {
                    labelAmount.Text = GlobalTexts.Amount + "(" + advancedListTime.SelectedRow["VasMeasure"] + "):";
                    _vasItem.VasType = advancedListTime.SelectedRow["VasType"].ToString();
                    _vasItem.VasTypeId = advancedListTime.SelectedRow["VasId"].ToString();
                    _vasItem.Amount = advancedListTime.SelectedRow["VasMeasure"].ToString();
                }
                else
                {
                    labelAmount.Text = GlobalTexts.Amount;

                }
                EnableDisableOkButton();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterVas.OnActiveRowChanged");
            }
        }

        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                ViewEvent.Invoke(RegisterVasViewEvents.Back);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterVas.ButtonBackClick");
            }
        }

        private void ButtonOkClick(object sender, EventArgs e)
        {
            try
            {

                var regex = new Regex(@"^^[0-9]\d{0,9}(\.\d{1,3})?%?$");
                if (!regex.IsMatch(textBoxAmount.Text.Trim()))
                {
                    GuiCommon.ShowModalDialog(GlobalTexts.Error, GlobalTexts.EnterValidAmount, Severity.Error, GlobalTexts.Ok);
                }
                else
                {
                    _vasItem.Amount = textBoxAmount.Text.Trim() + " " + _vasItem.Amount;
                    ViewEvent.Invoke(RegisterVasViewEvents.Ok, textBoxConfirmBy.Text.Trim(), textBoxAmount.Text.Trim(), _vasItem);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormRegisterVas.ButtonBackClick");
            }
        }
    }
    #region View specific classes

    public class FormDataRegisterVas : BaseFormData
    {
        public MessageHolder MessageHolder { get; set; }
        public List<VasDetail> VasDetail { get; set; }
        public string Customer { get; set; }
        public string Stop { get; set; }
    }

    public class RegisterVasViewEvents : ViewEvent
    {
        public const int Ok = 0;
        public const int Back = 1;
    }

    #endregion

}

﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterVas
{
    class ViewCommandsRegisterVas
    {
        public static void ShowViewSelectVas(ViewEventDelegate viewEventHandler, MessageHolder messageHolder, FlowDataRegisterVas flowdata,List<VasDetail> vasDetail)
        {
           var formData = new FormDataRegisterVas
                {
                    MessageHolder = messageHolder, 
                    HeaderText = flowdata.HeaderText,
                    VasDetail = vasDetail,
                    Customer = flowdata.Customer,
                    Stop = flowdata.Stop
                };
            var view = ViewCommands.ShowView<FormRegisterVas>(formData);
            view.SetEventHandler(viewEventHandler);
        }
    }
}


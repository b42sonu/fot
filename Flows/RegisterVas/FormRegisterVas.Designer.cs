﻿

using Resco.Controls.CommonControls;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterVas
{
    partial class FormRegisterVas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.touchPanel = new Resco.Controls.CommonControls.TouchPanel();
            this.textBoxAmount = new System.Windows.Forms.TextBox();
            this.textBoxConfirmBy = new System.Windows.Forms.TextBox();
            this.labelConfirmedBy = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelAmount = new Resco.Controls.CommonControls.TransparentLabel();
            this.advancedListTime = new Resco.Controls.AdvancedList.AdvancedList();
            this.rowTemplate1 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell5 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell6 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell1 = new Resco.Controls.AdvancedList.TextCell();
            this.rowTemplate2 = new Resco.Controls.AdvancedList.RowTemplate();
            this.textCell7 = new Resco.Controls.AdvancedList.TextCell();
            this.textCell8 = new Resco.Controls.AdvancedList.TextCell();
            this.TextCell2 = new Resco.Controls.AdvancedList.TextCell();
            this.labelCustomer = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelModuleName = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelCustomerValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.labelStopValue = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblColonCustomer = new Resco.Controls.CommonControls.TransparentLabel();
            this.lblOrgnaisationUnitName = new Resco.Controls.CommonControls.TransparentLabel();
            this.touchPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelConfirmedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomerValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStopValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColonCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // touchPanel
            // 
            this.touchPanel.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.touchPanel.Controls.Add(this.lblOrgnaisationUnitName);
            this.touchPanel.Controls.Add(this.lblColonCustomer);
            this.touchPanel.Controls.Add(this.textBoxAmount);
            this.touchPanel.Controls.Add(this.textBoxConfirmBy);
            this.touchPanel.Controls.Add(this.labelConfirmedBy);
            this.touchPanel.Controls.Add(this.labelAmount);
            this.touchPanel.Controls.Add(this.advancedListTime);
            this.touchPanel.Controls.Add(this.labelCustomer);
            this.touchPanel.Controls.Add(this.labelModuleName);
            this.touchPanel.Controls.Add(this.labelCustomerValue);
            this.touchPanel.Controls.Add(this.labelStopValue);
            this.touchPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.touchPanel.Location = new System.Drawing.Point(0, 0);
            this.touchPanel.Name = "touchPanel";
            this.touchPanel.Size = new System.Drawing.Size(480, 535);
            this.touchPanel.TouchScrollBounceMode = Resco.Controls.CommonControls.TouchScrollBounceMode.None;
            this.touchPanel.TouchScrollMode = Resco.Controls.CommonControls.TouchScrollMode.None;
            // 
            // lblOrgnaisationUnitName
            // 
            this.lblOrgnaisationUnitName.AutoSize = false;
            this.lblOrgnaisationUnitName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblOrgnaisationUnitName.Location = new System.Drawing.Point(3, 32);
            this.lblOrgnaisationUnitName.Name = "lblOrgnaisationUnitName";
            this.lblOrgnaisationUnitName.Size = new System.Drawing.Size(477, 35);
            this.lblOrgnaisationUnitName.TextAlignment = Resco.Controls.CommonControls.Alignment.MiddleCenter;
            // 
            // textBoxConfirmedBy
            // 
            this.textBoxAmount.Location = new System.Drawing.Point(203, 390);
            this.textBoxAmount.Name = "textBoxConfirmedBy";
            this.textBoxAmount.Size = new System.Drawing.Size(255, 41);
            this.textBoxAmount.TabIndex = 0;
            this.textBoxAmount.TextChanged += new System.EventHandler(TextBoxAmountTextChanged);
            
            // 
            // textBoxAmount
            // 
            this.textBoxConfirmBy.Location = new System.Drawing.Point(203, 437);
            this.textBoxConfirmBy.Name = "textBoxAmount";
            this.textBoxConfirmBy.Size = new System.Drawing.Size(255, 41);
            this.textBoxConfirmBy.TabIndex = 0;
            this.textBoxConfirmBy.TextChanged += new System.EventHandler(TextBoxConfirmByTextChanged);
            // 
            // labelConfirmedBy
            // 
            this.labelConfirmedBy.AutoSize = false;
            this.labelConfirmedBy.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelConfirmedBy.Location = new System.Drawing.Point(21, 437);
            this.labelConfirmedBy.Name = "labelConfirmedBy";
            this.labelConfirmedBy.Size = new System.Drawing.Size(170, 28);
            // 
            // labelAmount
            // 
            this.labelAmount.AutoSize = false;
            this.labelAmount.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelAmount.Location = new System.Drawing.Point(21, 397);
            this.labelAmount.Name = "labelAmount";
            this.labelAmount.Size = new System.Drawing.Size(170, 28);
            // 
            // advancedListTime
            // 
            this.advancedListTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.advancedListTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(192)))), ((int)(((byte)(196)))));
            this.advancedListTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.advancedListTime.DataRows.Clear();
            this.advancedListTime.Location = new System.Drawing.Point(21, 100);
            this.advancedListTime.Name = "advancedListTime";
            this.advancedListTime.ScrollbarSmallChange = 32;
            this.advancedListTime.ScrollbarWidth = 26;
            this.advancedListTime.SelectedTemplateIndex = 1;
            this.advancedListTime.SelectionMode = Resco.Controls.AdvancedList.SelectionMode.SelectDeselect;
            this.advancedListTime.Size = new System.Drawing.Size(438, 270);
            this.advancedListTime.TabIndex = 38;
            this.advancedListTime.Templates.Add(this.rowTemplate1);
            this.advancedListTime.Templates.Add(this.rowTemplate2);
            this.advancedListTime.TouchScrolling = true;
            this.advancedListTime.ActiveRowChanged += new System.EventHandler(this.OnActiveRowChanged);
            // 
            // rowTemplate1
            // 
            this.rowTemplate1.BackColor = BackColor = System.Drawing.Color.White;
           // this.rowTemplate1.CellTemplates.Add(this.textCell5);
            this.rowTemplate1.CellTemplates.Add(this.textCell6);
          //  this.rowTemplate1.CellTemplates.Add(this.TextCell1);
            this.rowTemplate1.Height = 30;
            this.rowTemplate1.Name = "rowTemplate1";
            // 
            // textCell5
            // 
            this.textCell5.CellSource.ColumnName = "VasId";
            this.textCell5.DesignName = "textCell5";
            this.textCell5.Location = new System.Drawing.Point(0, 0);
            this.textCell5.Size = new System.Drawing.Size(80, 64);
            this.textCell5.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // textCell6
            // 
            this.textCell6.CellSource.ColumnName = "VasType";
            this.textCell6.DesignName = "textCell6";
            this.textCell6.Location = new System.Drawing.Point(0, 0);
            this.textCell6.Size = new System.Drawing.Size(435, 64);
            this.textCell6.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // TextCell1
            // 
            this.TextCell1.CellSource.ColumnName = "VasMeasure";
            this.TextCell1.DesignName = "TextCell1";
            this.TextCell1.Location = new System.Drawing.Point(345, 0);
            this.TextCell1.Size = new System.Drawing.Size(-1, 64);
            this.TextCell1.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // rowTemplate2
            // 
            this.rowTemplate2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(190)))), ((int)(((byte)(74))))); 
         //   this.rowTemplate2.CellTemplates.Add(this.textCell7);
            this.rowTemplate2.CellTemplates.Add(this.textCell8);
           // this.rowTemplate2.CellTemplates.Add(this.TextCell2);
            this.rowTemplate2.Height = 30;
            this.rowTemplate2.Name = "rowTemplate2";
            // 
            // textCell7
            // 
            this.textCell7.CellSource.ColumnName = "VasId";
            this.textCell7.DesignName = "textCell7";
            this.textCell7.Location = new System.Drawing.Point(0, 0);
            this.textCell7.Size = new System.Drawing.Size(80, 64);
            this.textCell7.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.textCell7.ForeColor = System.Drawing.Color.White;
            // 
            // textCell8
            // 
            this.textCell8.CellSource.ColumnName = "VasType";
            this.textCell8.DesignName = "textCell8";
            this.textCell8.Location = new System.Drawing.Point(0, 0);
            this.textCell8.Size = new System.Drawing.Size(435, 64);
            this.textCell8.ForeColor = System.Drawing.Color.White;
            this.textCell8.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // TextCell2
            // 
            this.TextCell2.CellSource.ColumnName = "VasMeasure";
            this.TextCell2.DesignName = "TextCell2";
            this.TextCell2.Location = new System.Drawing.Point(345, 0);
            this.TextCell2.Size = new System.Drawing.Size(-1, 64);
            this.TextCell2.ForeColor = System.Drawing.Color.White;
            this.TextCell2.TextFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            // 
            // labelCustomer
            // 
            this.labelCustomer.AutoSize = false;
            this.labelCustomer.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.labelCustomer.Location = new System.Drawing.Point(21, 66);
            this.labelCustomer.Name = "labelCustomer";
            this.labelCustomer.Size = new System.Drawing.Size(112, 28);
            // 
            // labelModuleName
            // 
            this.labelModuleName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.labelModuleName.Location = new System.Drawing.Point(162, 0);
            this.labelModuleName.Name = "labelModuleName";
            this.labelModuleName.Size = new System.Drawing.Size(127, 27);
            // 
            // labelCustomerValue
            // 
            this.labelCustomerValue.AutoSize = false;
            this.labelCustomerValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelCustomerValue.Location = new System.Drawing.Point(153, 66);
            this.labelCustomerValue.Name = "labelCustomerValue";
            this.labelCustomerValue.Size = new System.Drawing.Size(320, 28);
            // 
            // labelStopValue
            // 
            this.labelStopValue.AutoSize = false;
            this.labelStopValue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.labelStopValue.Location = new System.Drawing.Point(153, 106);
            this.labelStopValue.Name = "labelStopValue";
            this.labelStopValue.Size = new System.Drawing.Size(320, 28);
            // 
            // lblColonCustomer
            // 
            this.lblColonCustomer.AutoSize = false;
            this.lblColonCustomer.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.lblColonCustomer.Location = new System.Drawing.Point(141, 66);
            this.lblColonCustomer.Name = "lblColonCustomer";
            this.lblColonCustomer.Size = new System.Drawing.Size(10, 28);
            this.lblColonCustomer.Text = ":";
            // 
            // FormDamageScan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(480, 535);
            this.Controls.Add(this.touchPanel);
            this.Location = new System.Drawing.Point(0, 52);
            this.Name = "FormDamageScan";
            this.Text = "FormRegisterVas";
            this.touchPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.labelConfirmedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelModuleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelCustomerValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelStopValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblColonCustomer)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private Resco.Controls.CommonControls.TransparentLabel lblOrgnaisationUnitName;
        private Resco.Controls.CommonControls.TouchPanel touchPanel;
        private Resco.Controls.CommonControls.TransparentLabel labelModuleName;
        private Resco.Controls.CommonControls.TransparentLabel labelCustomer;
        private Resco.Controls.CommonControls.TransparentLabel labelCustomerValue;
        private Resco.Controls.AdvancedList.AdvancedList advancedListTime;
        private Resco.Controls.CommonControls.TransparentLabel labelStopValue;
        private System.Windows.Forms.TextBox textBoxAmount;
        private System.Windows.Forms.TextBox textBoxConfirmBy;
        private Resco.Controls.CommonControls.TransparentLabel labelConfirmedBy;
        private Resco.Controls.CommonControls.TransparentLabel labelAmount;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate1;
        private Resco.Controls.AdvancedList.TextCell textCell5;
        private Resco.Controls.AdvancedList.TextCell textCell6;
        private Resco.Controls.AdvancedList.TextCell TextCell1;
        private Resco.Controls.AdvancedList.RowTemplate rowTemplate2;
        private Resco.Controls.AdvancedList.TextCell textCell7;
        private Resco.Controls.AdvancedList.TextCell textCell8;
        private Resco.Controls.AdvancedList.TextCell TextCell2;
        private Resco.Controls.CommonControls.TransparentLabel lblColonCustomer;
    }
}

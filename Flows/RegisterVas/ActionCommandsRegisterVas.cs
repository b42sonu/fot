﻿
using System.Collections.Generic;

using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Storage;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterVas
{
    public class ActionCommandsRegisterVas
    {
        public List<VasDetail> GetVasDetail()
        {
            return SettingDataProvider.GetVasTypes();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.RegisterVas
{
    public enum FlowStateRegisterVas
    {
        ActionGetVas,
        ActionRegisterVas,
        ViewCommands,

        ViewSelectVas,
        ThisFlowComplete,
    }

    public class FlowDataRegisterVas : BaseFlowData
    {
        public string Customer { get; set; }
        public string Stop { get; set; }
    }

    public class FlowResultRegisterVas : BaseFlowResult
    {
        public PdaVas VasItem { get; set; }
    }

    public class FlowRegisterVas : BaseFlow
    {
        private readonly FlowResultRegisterVas _flowResult;
        private readonly MessageHolder _messageHolder;
        private readonly ActionCommandsRegisterVas _actionCommandsRegisterVas;
        private FlowDataRegisterVas _flowData;
        private List<VasDetail> _vasDetail;
        private string _customerName;
        private string _amount;
        public FlowRegisterVas()
        {
            _flowResult = new FlowResultRegisterVas();
            _messageHolder = new MessageHolder();
            _actionCommandsRegisterVas = new ActionCommandsRegisterVas();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowRegisterVas.BeginFlow");
            if (flowData != null)
            {
                _flowData = (FlowDataRegisterVas)flowData;
            }
            ExecuteState(FlowStateRegisterVas.ActionGetVas);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStateRegisterVas)state);
        }

        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowRegisterVas, which acts as name for next command.</param>
        public void ExecuteState(FlowStateRegisterVas state)
        {
            if (state > FlowStateRegisterVas.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStateRegisterVas state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowRegisterVas;" + state);

                switch (state)
                {
                    case FlowStateRegisterVas.ViewSelectVas:
                        ViewCommandsRegisterVas.ShowViewSelectVas(SelectScannerEventHandler, _messageHolder, _flowData, _vasDetail);
                        break;

                    case FlowStateRegisterVas.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowStateRegisterVas.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStateRegisterVas.ThisFlowComplete);
            }
        }
        public void ExecuteActionState(FlowStateRegisterVas state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowRegisterVas;" + state);
                    switch (state)
                    {
                        case FlowStateRegisterVas.ActionGetVas:
                            _vasDetail = _actionCommandsRegisterVas.GetVasDetail();
                            state = FlowStateRegisterVas.ViewSelectVas;
                            break;

                        case FlowStateRegisterVas.ActionRegisterVas:
                            EventReportHelper.SendAlystraRegisterVasOperationEvent(ModelMain.SelectedOperationProcess, _customerName, _flowResult.VasItem);
                            state = FlowStateRegisterVas.ThisFlowComplete;
                            break;
                    }
                } while (state < FlowStateRegisterVas.ViewCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowStateRegisterVas.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                state = FlowStateRegisterVas.ThisFlowComplete;
            }
            ExecuteViewState(state);

        }

        private void SelectScannerEventHandler(int viewEvent, params object[] data)
        {
            switch (viewEvent)
            {
                case RegisterVasViewEvents.Back:
                    _flowResult.State = FlowResultState.Cancel;
                    ExecuteState(FlowStateRegisterVas.ThisFlowComplete);
                    break;

                case RegisterVasViewEvents.Ok:
                    _amount = Convert.ToString(data[0]);
                    _customerName = Convert.ToString(data[1]);
                    _flowResult.VasItem =(PdaVas) data[2];
                    _flowResult.State = FlowResultState.Ok;
                    ExecuteState(FlowStateRegisterVas.ActionRegisterVas);
                    break;
            }
        }

        /// <summary>
        /// This method ends the flow FlowStateRegisterVas.
        /// </summary>
        private
        void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowRegisterVas.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}

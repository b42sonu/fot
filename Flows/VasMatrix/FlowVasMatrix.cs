﻿using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Devices;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix
{
    public enum FlowStatesVasMatrix
    {
        DecideValidationProcess,
        IsProcessSupportNeededForSingleScanning,
        IsProcessSupportNeededForScanningCollection,
        GetNextProcessSupport,
        CheckTriggerOnlyOncePerProcess,
        IsProcessSupportShownBefore,
        CheckTriggerOnlyOnceForConsignment,
        IsProcessSupportForConsignmentTriggeredBefore,
        StoreUsedProcessSupportId,
        DeleteUsedProcessSupportId,
        CheckTypeOfProcessSupport,
        DisplayTextInline,
        DisplayInfoDialog,
        DisplayWarningDialog,
        DisplayExclusiveInfoDialog,
        DisplayExclusiveWarningDialog,
        DisplayErrorDialog,
        GetProcessSupportSubProcess,
        BackFromProcessSupportFlow,
        StoreUsedProcessSupportForExclusiveAction,

        ViewCommands,
        TriggerProcessSupportSubProcess,
        ThisFlowComplete,
    }

    // State codea are as follows:
    // Error: Something went wrong
    // Ok: Process can continue
    // Cancel: Process was aborted (by user or logic)
    public class FlowResultVasMatrix : BaseFlowResult
    {
        public BaseFlowResult SubFlowResult; // FlowResult from supprt sub-flow
    }

    public class FlowDataVasMatrix : BaseFlowData
    {
        public BaseFlow CallingFlow;
        public string AttributeEqualTo;
        public string TmsName;
        public string RoleName;
        public ConsignmentEntity ConsignmentEntity;
        public bool PlaySuccessSound;
        public VasSubFlowResultInfo VasSubFlowResultInfo;

        internal VasHistory VasHistory { get; private set; }

        // Setting the entitymap is only done for final processing of all scanned consignment entities
        // This will trigger VasMatrix entries with AttributeEqualTo set to "ProcessCollection"
        // which again will trigger a subprocess (only once per Vascode)
        public EntityMap EntityMap { get; set; }

        public FlowDataVasMatrix(ConsignmentEntity consignmentEntity, BaseFlow callingFlow)
        {
            InitDefaultValues();
            ConsignmentEntity = consignmentEntity;
            CallingFlow = callingFlow;

            VasHistory = CallingFlow.VasHistory;
        }

        public FlowDataVasMatrix(EntityMap entityMap, BaseFlow callingFlow)
        {
            InitDefaultValues();
            Logger.LogAssert(entityMap != null, "Can not start FlowVasMatrix as collection process with EntityMap == null");
            EntityMap = entityMap;
            CallingFlow = callingFlow;

            // If user do several collection processes inside same work process, we should show process support 
            // even if it has been shown before
            VasHistory = new VasHistory();
        }

        private void InitDefaultValues()
        {
            RoleName = ModelUser.UserProfile.RoleName;
            TmsName = ModelUser.UserProfile.TmsAffiliation;
            PlaySuccessSound = true;
        }

        public bool IsCollectionProcess { get { return EntityMap != null; } }
    }

    public class MockedConsignmentDetail
    {
        public VASType[] VasType { get; set; }
        public string ProductCode { get; set; }
    }

    public class FlowVasMatrix : BaseFlow
    {
        private FlowDataVasMatrix _flowData;
        private readonly FlowResultVasMatrix _flowResult;
        private List<ProcessSupportData> _processSupportDataList; // List of all process suport entries returned from query
        private ProcessSupportData _processSupportData;
        private BaseFlow _processSupportSubFlow;
        
        public FlowVasMatrix()
        {
            _flowResult = new FlowResultVasMatrix { State = FlowResultState.Error };
        }

        public override void BeginFlow(IFlowData flowData)
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowVasMatrix.BeginFlow");
            _flowData = (FlowDataVasMatrix)flowData;

            ExecuteState(FlowStatesVasMatrix.DecideValidationProcess);
        }


        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesVasMatrix)state);
        }

        public void ExecuteState(FlowStatesVasMatrix state)
        {
            if (state > FlowStatesVasMatrix.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteActionState(FlowStatesVasMatrix state)
        {
            try
            {
                do
                {
                    bool result;
                    Logger.LogEvent(Severity.Debug, "#FlowVasMatrix;" + state);
                    switch (state)
                    {
                        case FlowStatesVasMatrix.DecideValidationProcess:
                            result = ActionCommandsVasMatrix.DecideValidationProcess(_flowData);
                            state = result ? FlowStatesVasMatrix.IsProcessSupportNeededForSingleScanning : FlowStatesVasMatrix.IsProcessSupportNeededForScanningCollection;
                            break;

                        case FlowStatesVasMatrix.IsProcessSupportNeededForSingleScanning:
                            result = ActionCommandsVasMatrix.IsProcessSupportNeededForSingleScanning(_flowData, _flowResult, out _processSupportDataList);
                            state = result ? FlowStatesVasMatrix.GetNextProcessSupport : FlowStatesVasMatrix.ThisFlowComplete;
                            break;

                        case FlowStatesVasMatrix.IsProcessSupportNeededForScanningCollection:
                            result = ActionCommandsVasMatrix.IsProcessSupportNeededForScanningCollection(_flowData, _flowResult, out _processSupportDataList);
                            state = result ? FlowStatesVasMatrix.GetNextProcessSupport : FlowStatesVasMatrix.ThisFlowComplete;
                            break;

                        case FlowStatesVasMatrix.GetNextProcessSupport:
                            result = ActionCommandsVasMatrix.GetProcessSupport(_processSupportDataList, out _processSupportData);
                            state = result ? FlowStatesVasMatrix.CheckTriggerOnlyOncePerProcess : FlowStatesVasMatrix.ThisFlowComplete;
                            break;

                        case FlowStatesVasMatrix.CheckTriggerOnlyOncePerProcess:
                            result = ActionCommandsVasMatrix.CheckTriggerOnlyOncePerProcess(_processSupportData);
                            state = result ? FlowStatesVasMatrix.IsProcessSupportShownBefore : FlowStatesVasMatrix.CheckTriggerOnlyOnceForConsignment;
                            break;

                        case FlowStatesVasMatrix.IsProcessSupportShownBefore:
                            result = ActionCommandsVasMatrix.IsProcessSupportShownBefore(_flowData.VasHistory, _processSupportData.Id, _flowResult);
                            state = result ? FlowStatesVasMatrix.GetNextProcessSupport : FlowStatesVasMatrix.CheckTriggerOnlyOnceForConsignment;
                            break;

                        case FlowStatesVasMatrix.CheckTriggerOnlyOnceForConsignment:
                            result = ActionCommandsVasMatrix.CheckTriggerOnlyOnceForConsignment(_processSupportData, _flowData.IsCollectionProcess);
                            state = result ? FlowStatesVasMatrix.IsProcessSupportForConsignmentTriggeredBefore : FlowStatesVasMatrix.CheckTypeOfProcessSupport;
                            break;

                        case FlowStatesVasMatrix.IsProcessSupportForConsignmentTriggeredBefore:
                            result = ActionCommandsVasMatrix.IsProcessSupportForConsignmentShownBefore(_flowData.VasHistory, _processSupportData, _flowData, _flowResult);
                            state = result ? FlowStatesVasMatrix.GetNextProcessSupport : FlowStatesVasMatrix.CheckTypeOfProcessSupport;
                            break;

                        case FlowStatesVasMatrix.CheckTypeOfProcessSupport:
                            state = ActionCommandsVasMatrix.CheckTypeOfProcessSupport(_processSupportData, _flowData, _flowResult);
                            break;

                        case FlowStatesVasMatrix.DisplayTextInline:
                            ActionCommandsVasMatrix.DisplayWarningInline(_processSupportData.Message, _flowData.ConsignmentEntity, _flowResult);
                            state = FlowStatesVasMatrix.StoreUsedProcessSupportId;
                            break;

                        case FlowStatesVasMatrix.DisplayInfoDialog:
                            ActionCommandsVasMatrix.DisplayInfoDialog(_processSupportData.Message, _flowData.ConsignmentEntity, _flowResult);
                            state = FlowStatesVasMatrix.StoreUsedProcessSupportId;
                            break;

                        case FlowStatesVasMatrix.DisplayWarningDialog:
                            result = ActionCommandsVasMatrix.DisplayWarningDialog(_processSupportData.Message, _flowData.ConsignmentEntity, _flowResult);
                            state = result ? FlowStatesVasMatrix.StoreUsedProcessSupportId : FlowStatesVasMatrix.DeleteUsedProcessSupportId;
                            break;

                        case FlowStatesVasMatrix.DisplayExclusiveInfoDialog:
                            ActionCommandsVasMatrix.DisplayInfoDialog(_processSupportData.Message, _flowData.ConsignmentEntity, _flowResult);
                            state = FlowStatesVasMatrix.StoreUsedProcessSupportForExclusiveAction;
                            break;

                        case FlowStatesVasMatrix.DisplayExclusiveWarningDialog:
                            ActionCommandsVasMatrix.DisplayWarningDialog(_processSupportData.Message, _flowData.ConsignmentEntity, _flowResult);
                            state = FlowStatesVasMatrix.StoreUsedProcessSupportForExclusiveAction;
                            break;

                        case FlowStatesVasMatrix.DisplayErrorDialog:
                            ActionCommandsVasMatrix.DisplayErrorDialog(_processSupportData.Message, _flowData.ConsignmentEntity, _flowResult);
                            state = FlowStatesVasMatrix.DeleteUsedProcessSupportId;
                            break;

                        case FlowStatesVasMatrix.GetProcessSupportSubProcess:
                            result = ActionCommandsVasMatrix.GetProcessSupportSubFlow(_processSupportData, _flowResult, out _processSupportSubFlow);
                            state = result ? FlowStatesVasMatrix.TriggerProcessSupportSubProcess : FlowStatesVasMatrix.StoreUsedProcessSupportId;
                            break;

                        case FlowStatesVasMatrix.BackFromProcessSupportFlow:
                            result = ActionCommandsVasMatrix.EvaluateProcessSupportFlowResult(SubflowResult, _flowResult);
                            state = result ? FlowStatesVasMatrix.StoreUsedProcessSupportId : FlowStatesVasMatrix.DeleteUsedProcessSupportId;
                            break;

                        case FlowStatesVasMatrix.StoreUsedProcessSupportId:
                            ActionCommandsVasMatrix.StoreUsedProcessSupportId(_flowData, _processSupportData);
                            state = FlowStatesVasMatrix.GetNextProcessSupport;
                            break;

                        case FlowStatesVasMatrix.StoreUsedProcessSupportForExclusiveAction:
                            ActionCommandsVasMatrix.StoreUsedProcessSupportId(_flowData, _processSupportData);
                            state = FlowStatesVasMatrix.ThisFlowComplete;
                            break;

                        case FlowStatesVasMatrix.DeleteUsedProcessSupportId:
                            ActionCommandsVasMatrix.DeleteUsedProcessSupportId(_flowData);
                            state = FlowStatesVasMatrix.ThisFlowComplete;
                            break;
                    }
                } while (state < FlowStatesVasMatrix.ViewCommands);
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowVasMatrix.ExecuteActionState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesVasMatrix.ThisFlowComplete);
            }
            ExecuteViewState(state);
        }


        private void ExecuteViewState(FlowStatesVasMatrix state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowVasMatrix;" + state);
                switch (state)
                {
                    case FlowStatesVasMatrix.TriggerProcessSupportSubProcess:
                        FotScanner.Instance.Deactivate();

                        var flowData = new FlowDataVasProcessSupport
                        {
                            Param = _processSupportData.Param,
                            EntityMap = _flowData.EntityMap,
                            CurrentConsignmentEntity = _flowData.ConsignmentEntity,
                            HeaderText = _flowData.HeaderText,
                            VasSubFlowResultInfo = _flowData.VasSubFlowResultInfo
                        };
                        BaseModule.StartSubFlow(_processSupportSubFlow, flowData, (int)FlowStatesVasMatrix.BackFromProcessSupportFlow, Process.Inherit);
                        break;

                    case FlowStatesVasMatrix.ThisFlowComplete:
                        EndFlow();
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowVasMatrix.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesVasMatrix.ThisFlowComplete);
            }
        }

        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowVasMatrix.EndFlow");

            //If running for single scanning then only sound should play
            if (_flowData.ConsignmentEntity != null)
            {
                switch (_flowResult.State)
                {
                    case FlowResultState.Ok:
                        if(!_flowResult.MessageIsFromVasMatrix)
                        PlaySuccessSound();
                        break;

                    case FlowResultState.Cancel:
                    case FlowResultState.Warning:
                        SoundUtil.Instance.PlayWarningSound();
                        break;

                    case FlowResultState.Error:
                        SoundUtil.Instance.PlayScanErrorSound();
                        break;
                }
            }
            BaseModule.EndSubFlow(_flowResult);
        }

        private void PlaySuccessSound()
        {
            if (_flowData.PlaySuccessSound)
            {
                if (_flowData.ConsignmentEntity.IsScannedOnline && _flowResult.MessageIsFromVasMatrix == false)
                    SoundUtil.Instance.PlaySuccessSound();
                else // case of offline with ok or have uscces, but showing inline message
                    SoundUtil.Instance.PlayWarningSound();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix
{
    public class VasCodesQueryParser
    {
        private string _query;
        private List<OperationData> _queryOperations;

        
        public bool ExecuteQuery(string vasCodes, string query)
        {
            _query = query;
            PrepareQuery();
            DecodeQueryOperations();

            return IsQueryValid(vasCodes.Split(new[] {','}));
        }

        private bool IsQueryValid(string[] vasCodes)
        {
            foreach (var queryOperation in _queryOperations)
            {
                switch (queryOperation.Operation)
                {
                    case Operation.Mandatory:
                        if (vasCodes.Contains(queryOperation.VasCodes) == false)
                            return false;
                        break;

                    case Operation.Excluded:
                        if (vasCodes.Contains(queryOperation.VasCodes))
                            return false;
                        break;

                    case Operation.Any:
                        string[] requestedVas = queryOperation.VasCodes.Split(new [] {','});
                        if (vasCodes.Intersect(requestedVas).Any() == false)
                            return false;
                        break;

                    case Operation.None:
                        requestedVas = queryOperation.VasCodes.Split(new[] { ',' });
                        if (vasCodes.Intersect(requestedVas).Any())
                            return false;
                        break;
                }
            }
            return true;
        }



        private void DecodeQueryOperations()
        {
            _queryOperations = new List<OperationData>();
            while (_query != string.Empty)
            {
                var operationData = ExtractQueryOperation();
                _queryOperations.Add(operationData);
            }
        }


        // Extract an atomic operation
        private OperationData ExtractQueryOperation()
        {
            var result = new OperationData(); 

            int endIndex;

            // Extract operation inside paranteses
            if (_query.StartsWith("+(") || _query.StartsWith("-("))
            {
                result.Operation = _query[0] == '+' ? Operation.Any : Operation.None;
                var index = _query.IndexOf(')');
                result.VasCodes = _query.Substring(2, index - 2);
                endIndex = index + 1;
            }
            // Extract standalone operations
            else if (_query.StartsWith("+") || _query.StartsWith("-"))
            {
                result.Operation = _query[0] == '+' ? Operation.Mandatory : Operation.Excluded;

                int endIndexPlus  = _query.IndexOf('+', 1);
                int endIndexMinus = _query.IndexOf('-', 1);
                if (endIndexPlus == -1 && endIndexMinus == -1)
                {
                    // This is last statement
                    endIndex = _query.Length;
                    result.VasCodes = _query.Substring(1);
                }
                else
                {
                    if (endIndexPlus == -1)
                        endIndexPlus = int.MaxValue;
                    if (endIndexMinus == -1)
                        endIndexMinus = int.MaxValue;

                    endIndex = Math.Min(endIndexPlus, endIndexMinus);
                    result.VasCodes = _query.Substring(1, endIndex - 1);
                }
            }
            else
                throw new Exception("Failed to extract query operation");

            _query = _query.Substring(endIndex);
            return result;
        }


        // Prepend first statement with operator if missing
        // Add brackets around stand-alone list-statement if missing
        private void PrepareQuery()
        {
            if (_query.IndexOf('+') == -1 && _query.IndexOf('-') == -1 && _query.IndexOf('(') == -1)
            {
                _query = "(" + _query + ")";
            }

            if(_query[0] != '+' && _query[0] != '-')
                _query = "+" + _query;

            

        }

        internal enum Operation
        {
            Mandatory,  // Must have this vas code
            Excluded,   // Must NOT have this vas code
            None,       // None of the codes in list
            Any,        // At least one of the codes in list
        }

        internal class OperationData
        {
            internal Operation Operation;
            
            internal string VasCodes;
        }
    }


    public class VasCodeList : List<string>
    {
    }
}
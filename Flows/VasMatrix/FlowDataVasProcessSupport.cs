﻿using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix
{
    public class FlowDataVasProcessSupport : BaseFlowData
    {
        public string Param;
        public EntityMap EntityMap;
        public ConsignmentEntity CurrentConsignmentEntity;
        public VasSubFlowResultInfo VasSubFlowResultInfo;
    }
}
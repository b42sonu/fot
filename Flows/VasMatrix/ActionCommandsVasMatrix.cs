﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Validation;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix
{
    public static class ActionCommandsVasMatrix
    {
        internal static string GetVasCodesFromConsignmentEntity(ConsignmentEntity consignmentEntity)
        {
            string result = string.Empty;

            VASType[] vasObjects = consignmentEntity.Vas;
            // If this is a consignment item, we add the vas codes from the consignment too
            if (consignmentEntity is ConsignmentItem)
            {
                if (consignmentEntity.ConsignmentInstance.Vas != null)
                {
                    vasObjects = vasObjects != null
                                     ? vasObjects.Concat(consignmentEntity.ConsignmentInstance.Vas).ToArray()
                                     : consignmentEntity.ConsignmentInstance.Vas;
                }
            }

            if (vasObjects != null)
            {
                // Remove duplicates
                var vasCodes = vasObjects.Select(vasType => vasType.VASCode).Distinct().ToArray();

                // Generate comma separated string
                result = string.Join(",", vasCodes);
            }
            return result;
        }


        internal static bool IsProcessSupportNeededForSingleScanning(FlowDataVasMatrix flowData, FlowResultVasMatrix flowResult, out List<ProcessSupportData> processSupportList)
        {
            bool result = false;
            processSupportList = null;

            if (ValidateMandatoryArguments(flowData))
            {
                string vasCodes = GetVasCodesFromConsignmentEntity(flowData.ConsignmentEntity);

                Logger.LogEvent(Severity.Debug, "Executing VAS for process {0}: VasCodes: '{1}', ProductCode: {2}, AttributeEqualTo: '{3}', Role: {0}",
                    flowData.CallingFlow.WorkProcess, vasCodes, flowData.ConsignmentEntity.ProductCode, flowData.AttributeEqualTo,
                    flowData.RoleName);

                var vasQueryParams = new VasQueryParameters(vasCodes, flowData);

                processSupportList = SettingDataProvider.Instance.GetProcessSupportData(vasQueryParams);
                if (processSupportList != null)
                {
                    if (processSupportList.Any(processSupport => processSupport.BehaviourCode != BehaviourCode.Ignore))
                    {
                        processSupportList = RemoveDuplicates(processSupportList);
                        processSupportList = EnforceItemNumberRestrictions(processSupportList, flowData.ConsignmentEntity.ConsignmentItemCountFromLm);
                        processSupportList = EnforceExtendedVasQuery(processSupportList, vasCodes);
                        processSupportList = RemoveSurpressedProcessSupport(processSupportList);

                        if (processSupportList.Any(processSupport => processSupport.BehaviourCode != BehaviourCode.Ignore))
                        {
                            ModifyMultipleInlinesToPopups(processSupportList);
                            result = true;
                        }
                    }
                }
            }

            if (result == false)
            {
                // Finished with flow, no VAS Matrix support required
                flowResult.State = FlowResultState.Ok;
            }

            return result;
        }

        private static List<ProcessSupportData> EnforceExtendedVasQuery(IEnumerable<ProcessSupportData> processSupportList, string vasCodes)
        {
            return processSupportList.Where(processSupportData => ExecuteExtendedVasQuery(processSupportData.Query, vasCodes)).ToList();
        }

        private static bool ExecuteExtendedVasQuery(string vasCodesQuery, string vasCodes)
        {
            // No query, everything fine
            if (vasCodesQuery == string.Empty)
                return true;

            var vasCodesQueryParser = new VasCodesQueryParser();
            return vasCodesQueryParser.ExecuteQuery(vasCodes, vasCodesQuery);
        }

        private static List<ProcessSupportData> RemoveSurpressedProcessSupport(
            List<ProcessSupportData> processSupportList)
        {
            var result = new List<ProcessSupportData>();
            try
            {
                var allIdExclusions = new List<int>();

                // Retrieve all id's that should be excluded
                foreach (var processSupport in processSupportList)
                {
                    if (processSupport.IdExclusions != null)
                    {
                        string idExclusions = processSupport.IdExclusions.Replace(" ", string.Empty);
                        if (idExclusions != string.Empty)
                        {
                            var ids = idExclusions.Split(new[] {','});
                            foreach (var idString in ids)
                            {
                                int id = Convert.ToInt32(idString);
                                if (allIdExclusions.Contains(id) == false)
                                    allIdExclusions.Add(id);
                            }
                        }
                    }
                }

                result.AddRange(processSupportList.Where(processSupport => allIdExclusions.Contains(processSupport.Id) == false));
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsVasMatrix.RemoveSurpressedProcessSupport");
            }


            return result;
        }

        private static List<ProcessSupportData> EnforceItemNumberRestrictions(IEnumerable<ProcessSupportData> processSupportList, int consignmentItemCountFromLm)
        {
            var result =
                processSupportList.Where(
                    processSupport => IsNumberRestrictionFulfilled(processSupport.ItemCount, consignmentItemCountFromLm)).ToList();
            return result;
        }

        private static bool IsNumberRestrictionFulfilled(string itemCountExpression, int consignmentItemCountFromLm)
        {
            bool result = true;
            try
            {
                var expression = itemCountExpression.Replace(" ", string.Empty);

                if (expression != string.Empty)
                {
                    switch (expression[0])
                    {
                        case '<':
                            int itemCount = Convert.ToInt32(expression.Substring(1));
                            result = consignmentItemCountFromLm < itemCount;
                            break;

                        case '>':
                            itemCount = Convert.ToInt32(expression.Substring(1));
                            result = consignmentItemCountFromLm > itemCount;
                            break;

                        default:
                            itemCount = Convert.ToInt32(expression);
                            result = consignmentItemCountFromLm == itemCount;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "ActionCommandsVasMatrix.IsNumberRestrictionFulfilled");
            }
            return result;
        }

        // If we have more than one inline message, we make it into an info message except the last one
        private static void ModifyMultipleInlinesToPopups(List<ProcessSupportData> processSupportList)
        {
            if (processSupportList.Count(processSupport => processSupport.BehaviourCode == BehaviourCode.DisplayTextInline) > 1)
            {
                for (int i = 0; i < processSupportList.Count - 1; i++)
                {
                    var processSupportData = processSupportList[i];
                    if (processSupportData.BehaviourCode == BehaviourCode.DisplayTextInline)
                    {
                        processSupportData.BehaviourCode = BehaviourCode.DisplayTextDialog;
                    }
                }
            }
        }


        // Due to joining and multiple queries we can get duplicated entries, so we remove them
        // Also remove multiples of same subprocess. Can happen if same subprcess is invoked
        // on several vascodes with different input data. If that is the case, we only keep
        // the instance with highest priority (from 0 to 9) 
        internal static List<ProcessSupportData> RemoveDuplicates(List<ProcessSupportData> processSupportList)
        {
            if (processSupportList.Count > 1)
            {
                // Sort after priority, behaviorcode
                processSupportList.Sort(Compare);

                // Remove multiples of same instance
                processSupportList = processSupportList.GroupBy(x => x.Id).Select(y => y.First()).ToList();
            }
            return processSupportList;
        }

        private static int Compare(ProcessSupportData a, ProcessSupportData b)
        {
            if (a.BehaviourCode != b.BehaviourCode)
            {
                // To keep backwards compatibility, we treat Abort with highest pri, even though it's value is low
                if (a.BehaviourCode == BehaviourCode.Abort)
                    return -1;
                if (b.BehaviourCode == BehaviourCode.Abort)
                    return 1;

                return a.BehaviourCode - b.BehaviourCode;
            }

            if (a.Priority != b.Priority)
                return a.Priority - b.Priority;

            return a.Id - b.Id;
        }

        /// <summary>
        /// Collect process support data for all scanned entrities 
        ///  </summary>
        internal static bool IsProcessSupportNeededForScanningCollection(FlowDataVasMatrix flowData, FlowResultVasMatrix flowResult,
            out List<ProcessSupportData> allProcessSupportList)
        {
            bool result = false;
            allProcessSupportList = new List<ProcessSupportData>();

            if (ValidateMandatoryArguments(flowData))
            {
                var vasQueryParams = new VasQueryParameters(flowData);

                foreach (Consignment consignment in flowData.EntityMap.Values)
                {
                    //Check if consignment is scanned
                    if (consignment.HasStatus(EntityStatus.Scanned))
                    {
                        var vasCodes = GetVasCodesFromConsignmentEntity(consignment);
                        vasQueryParams.UpdateEntityInfo(vasCodes, consignment.ProductCode);
                        allProcessSupportList = AddProcessSupportData(vasQueryParams, allProcessSupportList);
                    }
                    //Check if contained consignment items are scanned
                    if (consignment.ConsignmentItemsMap != null)
                    {
                        foreach (ConsignmentItem consignmentItem in consignment.ConsignmentItemsMap.Values)
                        {
                            if (consignmentItem.HasStatus(EntityStatus.Scanned))
                            {
                                var vasCodes = GetVasCodesFromConsignmentEntity(consignmentItem);
                                vasQueryParams.UpdateEntityInfo(vasCodes, consignmentItem.ProductCode);

                                var processSupportList = GetProcessSupportData(vasQueryParams);
                                processSupportList = RemoveDuplicates(processSupportList);
                                processSupportList = EnforceExtendedVasQuery(processSupportList, vasCodes);
                                allProcessSupportList = allProcessSupportList.Union(processSupportList).ToList();  
                            }
                        }
                    }
                }
            }

            if (allProcessSupportList.Any(processSupport => processSupport.BehaviourCode != BehaviourCode.Ignore))
            {
                allProcessSupportList = RemoveDuplicates(allProcessSupportList);
                allProcessSupportList = RemoveSurpressedProcessSupport(allProcessSupportList);
                result = true;
            }
            else
            {
                // Finished with flow, no VAS Matrix support required
                flowResult.State = FlowResultState.Ok;
            }

            return result;
        }

        private static List<ProcessSupportData> GetProcessSupportData(VasQueryParameters vasQueryParameters)
        {
            return SettingDataProvider.Instance.GetProcessSupportData(vasQueryParameters);
        }

        private static List<ProcessSupportData> AddProcessSupportData(VasQueryParameters vasQueryParameters, IEnumerable<ProcessSupportData> processSupportList)
        {
            var processSupportListSingleEntity = SettingDataProvider.Instance.GetProcessSupportData(vasQueryParameters);

            return processSupportList.Union(processSupportListSingleEntity).ToList();
        }

        //TODO :: Write Unit Test
        private static bool ValidateMandatoryArguments(FlowDataVasMatrix flowData)
        {
            if (flowData.CallingFlow.WorkProcess == WorkProcess.None)
                return false;
            if (string.IsNullOrEmpty(flowData.TmsName))
                return false;
            if (string.IsNullOrEmpty(flowData.RoleName))
                return false;
            return true;
        }

        internal static bool GetProcessSupport(IList<ProcessSupportData> processSupportDataList,
                                               out ProcessSupportData processSupportData)
        {
            processSupportData = null;
            if (processSupportDataList.Any())
            {
                processSupportData = processSupportDataList[0];
                processSupportDataList.RemoveAt(0);
            }

            return processSupportData != null;
        }

        internal static bool CheckTriggerOnlyOncePerProcess(ProcessSupportData processSupportData)
        {
            // Collection processes should by definition only be shown once
            return processSupportData.ShowOncePerProcess;
        }

        internal static bool CheckTriggerOnlyOnceForConsignment(ProcessSupportData processSupportData,
                                                                bool isCollectionProcess)
        {
            // This does not apply for collection processes
            if (isCollectionProcess)
                return false;
            return processSupportData.ShowOncePerConsignment;
        }


        internal static bool IsProcessSupportShownBefore(VasHistory vasHistory, int processSupportId, FlowResultVasMatrix flowResult)
        {
            // If we have shown this process support before, we finsh here...
            if (vasHistory.IsProcessSupportShownBefore(processSupportId))
            {
                flowResult.State = FlowResultState.Ok;
                return true;
            }
            return false;
        }

        //TODO :: Write Unit Test
        internal static bool IsProcessSupportForConsignmentShownBefore(VasHistory vasHistory,
            ProcessSupportData processSupportData, FlowDataVasMatrix flowData, FlowResultVasMatrix flowResult)
        {
            // If we have shown this process support before, we finsh here...
            if (vasHistory.IsProcessSupportForConsignmentShownBefore(processSupportData.Id, flowData.ConsignmentEntity.ConsignmentId))
            {
                flowResult.State = FlowResultState.Ok;
                return true;
            }
            return false;
        }


        //TODO :: Write Unit Test
        internal static void StoreUsedProcessSupportId(FlowDataVasMatrix flowData, ProcessSupportData processSupportData)
        {
            if (CheckTriggerOnlyOncePerProcess(processSupportData) ||
                CheckTriggerOnlyOnceForConsignment(processSupportData, flowData.IsCollectionProcess))
            {
                flowData.VasHistory.AddUsedProcessSupportId(processSupportData.Id, flowData.ConsignmentEntity);
            }
        }

        internal static void DeleteUsedProcessSupportId(FlowDataVasMatrix flowData)
        {
            flowData.VasHistory.DeleteProcessHistoryForConsignmentEntity(flowData.ConsignmentEntity);
        }

        //TODO :: Write Unit Test
        internal static FlowStatesVasMatrix CheckTypeOfProcessSupport(ProcessSupportData processSupportData,
                                                                      FlowDataVasMatrix flowData,
                                                                      FlowResultVasMatrix flowResult)
        {
            switch (processSupportData.BehaviourCode)
            {
                case BehaviourCode.DisplayTextInline:
                    return FlowStatesVasMatrix.DisplayTextInline;

                case BehaviourCode.DisplayTextDialog:
                    return FlowStatesVasMatrix.DisplayInfoDialog;

                case BehaviourCode.DisplayWarningDialog:
                    return FlowStatesVasMatrix.DisplayWarningDialog;

                case BehaviourCode.DisplayExclusiveInfoDialog:
                    return FlowStatesVasMatrix.DisplayExclusiveInfoDialog;

                case BehaviourCode.DisplayExclusiveWarningDialog:
                    return FlowStatesVasMatrix.DisplayExclusiveWarningDialog;

                case BehaviourCode.DisplayErrorDialog:
                    return FlowStatesVasMatrix.DisplayErrorDialog;

                case BehaviourCode.TriggerSubProcess:
                    return FlowStatesVasMatrix.GetProcessSupportSubProcess;

                case BehaviourCode.NoAction:
                    flowResult.State = FlowResultState.Ok;
                    return FlowStatesVasMatrix.StoreUsedProcessSupportId;

                case BehaviourCode.Abort:
                    flowResult.State = FlowResultState.Ok;
                    return FlowStatesVasMatrix.StoreUsedProcessSupportForExclusiveAction;

                default:
                    Logger.LogEvent(Severity.ExpectedError, "Unsupported BehaviourCode {0} detected in ActionCommandsVasMatrix.CheckTypeOfProcessSupport()",
                                    processSupportData.BehaviourCode);

                    flowResult.State = FlowResultState.Ok;
                    return FlowStatesVasMatrix.StoreUsedProcessSupportId;
            }
        }

        internal static void DisplayWarningInline(string message, ConsignmentEntity scannedEntity, FlowResultVasMatrix flowResult)
        {
            flowResult.Message = VasStringFormatter.Format(message, scannedEntity);
            flowResult.MessageIsFromVasMatrix = true;
            flowResult.State = FlowResultState.Ok;
        }

        internal static void DisplayInfoDialog(string message, ConsignmentEntity scannedEntity,
                                               FlowResultVasMatrix flowResult)
        {
            GuiCommon.ShowModalDialog(GlobalTexts.Info, VasStringFormatter.Format(message, scannedEntity), Severity.Info,
                                      GlobalTexts.Ok);

            flowResult.State = FlowResultState.Ok;
        }

        internal static bool DisplayWarningDialog(string message, ConsignmentEntity scannedEntity,
                                                  FlowResultVasMatrix flowResult)
        {
            var dialogResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning,
                VasStringFormatter.Format(message, scannedEntity), Severity.Warning, GlobalTexts.Ok, GlobalTexts.Cancel);

            flowResult.State = dialogResult == GlobalTexts.Ok ? FlowResultState.Ok : FlowResultState.Cancel;

            return flowResult.State == FlowResultState.Ok;
        }

        internal static void DisplayErrorDialog(string message, ConsignmentEntity scannedEntity, FlowResultVasMatrix flowResult)
        {
            GuiCommon.ShowModalDialog(GlobalTexts.Error, VasStringFormatter.Format(message, scannedEntity),
                                      Severity.Error, GlobalTexts.Ok);
            flowResult.State = FlowResultState.Cancel;
        }


        internal static bool GetProcessSupportSubFlow(ProcessSupportData processSupportData, FlowResultVasMatrix flowResult, out BaseFlow subFlow)
        {
            const string sharedFlowsNameSpace = "Com.Bring.PMP.PreComFW.Shared.VasSubFlows";

            bool result = false;
            subFlow = null;
            try
            {
                if (string.IsNullOrEmpty(processSupportData.SubProcessName))
                {
                    Logger.LogEvent(Severity.Error, "Expected sub-process not specified");
                }
                else
                {
                    string subFlowTypeString = string.Format("{0}.{1}", sharedFlowsNameSpace,
                                                             processSupportData.SubProcessName);
                    var flowTypeDescription = Type.GetType(subFlowTypeString);
                    if (flowTypeDescription != null)
                    {
                        subFlow = (BaseFlow) Activator.CreateInstance(flowTypeDescription);
                        result = true;
                    }
                    else
                    {
                        Logger.LogEvent(Severity.Error, "Sub-process type {0} is not defined. Please check server side configuration",
                                        processSupportData.SubProcessName);
                    }
                }
            }
            catch (Exception ex)
            {

                Logger.LogException(ex, "ActionCommandsVasMatrix.GetProcessSupportSubProcess()");
            }

            return result;
        }


        internal static bool EvaluateProcessSupportFlowResult(IFlowResult processSupportFlowResult, FlowResultVasMatrix flowResult)
        {
            flowResult.State = processSupportFlowResult.State;
            flowResult.InnerFlowResult = processSupportFlowResult;

            return processSupportFlowResult.State == FlowResultState.Ok;
        }

        // Returns true if we are validating for a single scanning, false if we are doing mass validation for a collection
        internal static bool DecideValidationProcess(FlowDataVasMatrix flowData)
        {
            if (flowData.ConsignmentEntity != null)
            {
                Logger.LogAssert(flowData.EntityMap == null, "Both consignment-entity and entitymap can not be set");
                return true;
            }

            Logger.LogAssert(flowData.EntityMap != null, "Both consignment-entity and entitymap can not be null");
            return false;
        }
    }
}
                            

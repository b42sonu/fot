﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Barcode;
using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Views;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.HandleLoadUnloadOptions
{
    public enum FlowStatesHandleLoadUnloadOptions
    {
        ActionStartRequiredFlow,
        ActionBackFromFlow,
        ViewCommands,
        FlowLoadLineHaul,
        FlowLoadDistributionTruck,
        FlowUnloadLineHaul,
        FlowUnloadPickupTruck,
        FlowLoadCombinationTrip,
        ThisFlowComplete
    }

    public class FlowResultHandleLoadUnloadOptions : BaseFlowResult
    {

    }

    public class FlowDataHandleLoadUnloadOptions : BaseFlowData
    {
        public LoadUnloadOptions FlowName { get; set; }
        public PlannedConsignmentsType[] SelectedConsignments;
        public bool IsStartedFromOperationList { get; set; }
    }

    public class FlowHandleLoadUnloadOptions : BaseFlow
    {
        private IFlowFactory _factory;
        private BaseFlow _flow;
        private FlowDataHandleLoadUnloadOptions _flowData;
        private WorkListItem _selectedWorkListItem;
        private readonly FlowResultHandleLoadUnloadOptions _flowResult;
        private bool _isToActivateWorkListItem;
        private BarcodeRouteCarrierTrip _rbt;
        private string _loadCarrier;
        private BarcodeType _scannedBarcodeType;
        private ScanningProcessStartedForm _scanningStartedFrom;

        public FlowHandleLoadUnloadOptions()
        {
            _flowResult = new FlowResultHandleLoadUnloadOptions();
        }

        public override void BeginFlow(IFlowData flowData)
        {
            _flowData = (FlowDataHandleLoadUnloadOptions)flowData;
            Logger.LogEvent(Severity.Debug, "Executing FlowHandleLoadUnloadOptions.BeginFlow");

            _scanningStartedFrom = _flowData.IsStartedFromOperationList
                                       ? ScanningProcessStartedForm.FromOperationListOperation
                                       : ScanningProcessStartedForm.FromLoadUnloadMenu;


            //If user have selected operation of type Loading/Unloading from Operation List
            //Then set value of physical carrier to current load carrier
            if (_flowData.IsStartedFromOperationList)
            {
                _loadCarrier = ModelMain.SelectedOperationProcess.PhysicalLoadCarrierId;
            }

            ExecuteState(FlowStatesHandleLoadUnloadOptions.ActionStartRequiredFlow);
        }

        public override void ExecuteState(int state)
        {
            ExecuteState((FlowStatesHandleLoadUnloadOptions)state);
        }

        /// <summary>
        /// This method calls ExecuteViewState or ExecuteActionState on the basis that current command is View Command or Action command.
        /// </summary>
        /// <param name="state">Specifies value from enum FlowStateLoading, which acts as name for next command.</param>
        public void ExecuteState(FlowStatesHandleLoadUnloadOptions state)
        {
            if (state > FlowStatesHandleLoadUnloadOptions.ViewCommands)
            {
                ExecuteViewState(state);
            }
            else
            {
                ExecuteActionState(state);
            }
        }

        private void ExecuteViewState(FlowStatesHandleLoadUnloadOptions state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowHandleLoadUnloadOptions;" + state);

                if (state == FlowStatesHandleLoadUnloadOptions.FlowLoadLineHaul || state == FlowStatesHandleLoadUnloadOptions.FlowLoadDistributionTruck || state == FlowStatesHandleLoadUnloadOptions.FlowLoadCombinationTrip)
                    _factory = ModelMain.FlowFactories[FlowType.Loading];
                else
                    _factory = ModelMain.FlowFactories[FlowType.Unloading];


                switch (state)
                {
                    case FlowStatesHandleLoadUnloadOptions.FlowLoadLineHaul:

                        _flow = _factory.CreateFlow(FlowType.LoadLineHaulTruck);
                        var flowDataLoadLineHaul = new FlowDataLoadUnload
                            {
                                HeaderText = GlobalTexts.LoadLineHaul,
                                SelectedConsignments = _flowData.SelectedConsignments,
                                IsToActivateWorkListItem = _isToActivateWorkListItem,
                                ScannedBarcodeType = _scannedBarcodeType,
                                LoadCarrierId = _loadCarrier,
                                Rbt = _rbt,
                                SelectedWorkListItem = _selectedWorkListItem,
                                ScanningProcessStartedForm = _scanningStartedFrom
                            };
                        BaseModule.StartSubFlow(_flow, flowDataLoadLineHaul, (int)FlowStatesHandleLoadUnloadOptions.ActionBackFromFlow, Process.LoadLineHaul);
                        break;

                    case FlowStatesHandleLoadUnloadOptions.FlowLoadDistributionTruck:

                        _flow = _factory.CreateFlow(FlowType.LoadDistributionTruck);
                        var flowDataLoadDistributionTruck = new FlowDataLoadUnload
                            {
                                HeaderText = GlobalTexts.LoadDistributionTruck,
                                SelectedConsignments = _flowData.SelectedConsignments,
                                IsToActivateWorkListItem = _isToActivateWorkListItem,
                                ScannedBarcodeType = _scannedBarcodeType,
                                LoadCarrierId = _loadCarrier,
                                Rbt = _rbt,
                                SelectedWorkListItem = _selectedWorkListItem,
                                ScanningProcessStartedForm = _scanningStartedFrom

                            };
                        BaseModule.StartSubFlow(_flow, flowDataLoadDistributionTruck, (int)FlowStatesHandleLoadUnloadOptions.ActionBackFromFlow, Process.LoadDistribTruck);
                        break;

                    case FlowStatesHandleLoadUnloadOptions.FlowLoadCombinationTrip:

                        _flow = _factory.CreateFlow(FlowType.LoadCombinationTrip);
                        var flowDataLoadUnload = new FlowDataLoadUnload
                        {
                            HeaderText = GlobalTexts.LoadCombTrip,
                            SelectedConsignments = _flowData.SelectedConsignments,
                            IsToActivateWorkListItem = _isToActivateWorkListItem,
                            ScannedBarcodeType = _scannedBarcodeType,
                            LoadCarrierId = _loadCarrier,
                            Rbt = _rbt,
                            SelectedWorkListItem = _selectedWorkListItem,
                            ScanningProcessStartedForm = _scanningStartedFrom

                        };
                        BaseModule.StartSubFlow(_flow, flowDataLoadUnload, (int)FlowStatesHandleLoadUnloadOptions.ActionBackFromFlow, Process.LoadCombinationTrip);
                        break;

                    case FlowStatesHandleLoadUnloadOptions.FlowUnloadLineHaul:
                        _flow = _factory.CreateFlow(FlowType.UnloadLineHaulTruck);
                        var flowDataUnloadLineHaul = new FlowDataLoadUnload
                        {
                            HeaderText = GlobalTexts.UnloadLineHaul,
                            SelectedConsignments = _flowData.SelectedConsignments,
                            IsToActivateWorkListItem = _isToActivateWorkListItem,
                            ScannedBarcodeType = _scannedBarcodeType,
                            LoadCarrierId = _loadCarrier,
                            Rbt = _rbt,
                            SelectedWorkListItem = _selectedWorkListItem,
                            ScanningProcessStartedForm = _scanningStartedFrom
                        };
                        BaseModule.StartSubFlow(_flow, flowDataUnloadLineHaul, (int)FlowStatesHandleLoadUnloadOptions.ActionBackFromFlow, Process.UnloadLineHaul);
                        break;


                    case FlowStatesHandleLoadUnloadOptions.FlowUnloadPickupTruck:
                        _flow = _factory.CreateFlow(FlowType.UnloadPickUpTruck);
                        var flowDataUnloadPickUpTruck = new FlowDataLoadUnload
                        {
                            HeaderText = GlobalTexts.UnloadPickUpTruck,
                            SelectedConsignments = _flowData.SelectedConsignments,
                            IsToActivateWorkListItem = _isToActivateWorkListItem,
                            ScannedBarcodeType = _scannedBarcodeType,
                            LoadCarrierId = _loadCarrier,
                            Rbt = _rbt,
                            SelectedWorkListItem = _selectedWorkListItem,
                            ScanningProcessStartedForm = _scanningStartedFrom
                        };
                        BaseModule.StartSubFlow(_flow, flowDataUnloadPickUpTruck, (int)FlowStatesHandleLoadUnloadOptions.ActionBackFromFlow, Process.UnloadPickUpTruck);
                        break;

                    case FlowStatesHandleLoadUnloadOptions.ThisFlowComplete:
                        EndFlow();
                        break;
                }

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowHandleLoadUnloadOptions.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesHandleLoadUnloadOptions.ThisFlowComplete);
            }
        }


        private void ExecuteActionState(FlowStatesHandleLoadUnloadOptions state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowHandleLoadUnloadOptions;" + state);

                do
                {
                    switch (state)
                    {
                        case FlowStatesHandleLoadUnloadOptions.ActionStartRequiredFlow:
                            state = GoToRequiredFlow(_flowData.FlowName);
                            break;
                        case FlowStatesHandleLoadUnloadOptions.ActionBackFromFlow:
                            Logger.LogAssert(SubflowResult as FlowResultLoadUnload != null, "Invalid FlowResultLoadUnload flow result");
                            state = ValidateFlowResult((FlowResultLoadUnload)SubflowResult);
                            break;
                    }
                } while (state < FlowStatesHandleLoadUnloadOptions.ViewCommands);

            }
            catch (Exception e)
            {
                Logger.LogException(e, "FlowHandleLoadUnloadOptions.ExecuteViewState");
                _flowResult.State = FlowResultState.Exception;
                ExecuteState(FlowStatesHandleLoadUnloadOptions.ThisFlowComplete);
            }
            ExecuteViewState(state);
        }


        private FlowStatesHandleLoadUnloadOptions GoToRequiredFlow(LoadUnloadOptions flowName)
        {
            switch (flowName)
            {
                case LoadUnloadOptions.LoadLineHaul:
                    return FlowStatesHandleLoadUnloadOptions.FlowLoadLineHaul;
                case LoadUnloadOptions.LoadDistributionTruck:
                    return FlowStatesHandleLoadUnloadOptions.FlowLoadDistributionTruck;
                case LoadUnloadOptions.UnloadLineHaul:
                    return FlowStatesHandleLoadUnloadOptions.FlowUnloadLineHaul;
                case LoadUnloadOptions.UnloadPickUpTruck:
                    return FlowStatesHandleLoadUnloadOptions.FlowUnloadPickupTruck;
                case LoadUnloadOptions.LoadCombinationTrip:
                    return FlowStatesHandleLoadUnloadOptions.FlowLoadCombinationTrip;

            }
            return FlowStatesHandleLoadUnloadOptions.ThisFlowComplete;
        }

        private FlowStatesHandleLoadUnloadOptions ValidateFlowResult(FlowResultLoadUnload subFlowResult)
        {

            switch (subFlowResult.State)
            {
                case FlowResultState.Ok:
                    {
                        if (subFlowResult.IsToStartAnotherProcess)
                        {
                            _scanningStartedFrom = ScanningProcessStartedForm.FromOtherProcess;
                            _rbt = subFlowResult.Rbt;
                            _loadCarrier = subFlowResult.LoadCarrier;
                            _selectedWorkListItem = subFlowResult.SelectedWorkListItem;
                            _scannedBarcodeType = subFlowResult.ScannedbarCodeType;
                            var processToStart = _selectedWorkListItem != null
                                                     ? _selectedWorkListItem.StopType
                                                     : subFlowResult.SelectedProcessToStart;

                            _isToActivateWorkListItem = subFlowResult.IsToActivateNewWorkListItem;
                            switch (processToStart)
                            {
                                case OperationType.LoadLineHaulTruck:
                                    return FlowStatesHandleLoadUnloadOptions.FlowLoadLineHaul;
                                case OperationType.LoadingDistributionTruck:
                                    return FlowStatesHandleLoadUnloadOptions.FlowLoadDistributionTruck;
                                case OperationType.UnloadLineHaulTruck:
                                    return FlowStatesHandleLoadUnloadOptions.FlowUnloadLineHaul;
                                case OperationType.UnloadPickUpTruck:
                                    return FlowStatesHandleLoadUnloadOptions.FlowUnloadPickupTruck;
                                default:
                                    return FlowStatesHandleLoadUnloadOptions.FlowLoadCombinationTrip;

                            }
                        }
                        return FlowStatesHandleLoadUnloadOptions.ThisFlowComplete;
                    }
                case FlowResultState.Cancel:
                    return FlowStatesHandleLoadUnloadOptions.ThisFlowComplete;
            }
            return FlowStatesHandleLoadUnloadOptions.ThisFlowComplete;
        }


        /// <summary>
        /// This method ends the loading flow.
        /// </summary>
        private void EndFlow()
        {
            Logger.LogEvent(Severity.Debug, "Executing FlowHandleLoadUnloadOptions.EndFlow");
            BaseModule.EndSubFlow(_flowResult);
        }
    }
}
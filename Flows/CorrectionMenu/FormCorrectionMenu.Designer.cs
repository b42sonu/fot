﻿using Com.Bring.PMP.PreComFW.Shared.Constants;
namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu
{
    partial class FormCorrectionMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows MainForm Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            this.labelHeading = new Resco.Controls.CommonControls.TransparentLabel();
            this.SuspendLayout();
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            this.buttonBack.Font = new System.Drawing.Font("Tahoma",9F, System.Drawing.FontStyle.Regular);
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(242, 501);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(240, 60);
            this.buttonBack.TabIndex = 25;
            this.buttonBack.Click += new System.EventHandler(this.ButtonBackClick);
            // 
            // labelHeading
            // 
            this.labelHeading.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            this.labelHeading.AutoSize = false;
            this.labelHeading.ForeColor = System.Drawing.Color.Black;
            this.labelHeading.Location = new System.Drawing.Point(3, 10);
            this.labelHeading.Name = "labelHeading";
            this.labelHeading.Size = new System.Drawing.Size(474, 98);
            // 
            // FormCorrectionMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(192F, 192F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelHeading);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Location = new System.Drawing.Point(0,0);
            this.Name = "FormCorrectionMenu";
            this.Size = new System.Drawing.Size(480, 552);
            this.ResumeLayout(false);

        }

        #endregion
        private Resco.Controls.CommonControls.TransparentLabel labelHeading;
        private Resco.Controls.OutlookControls.ImageButton buttonBack;
    }
}
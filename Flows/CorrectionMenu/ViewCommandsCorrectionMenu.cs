﻿using Com.Bring.PMP.PreComFW.Shared.Commands;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Forms;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;


namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu
{
    internal class ViewCommandsCorrectionMenu
    {
        public ViewCommandsCorrectionMenu(FlowDataCorrectionMenu flowDataCorrectionMenu, bool registerTimeConsumptionVisible)
        {
            ModuleMenu = CreateCorrectionMenu(flowDataCorrectionMenu, registerTimeConsumptionVisible);
        }

        private FormModuleMenu _formModuleMenu;

        public readonly ModuleMenu ModuleMenu;

        internal void InitCorrectionMenu(ViewEventDelegate viewEventHandler, string headerText, ConsignmentEntity consignmentEntity)
        {
            _formModuleMenu = ViewCommands.GetOrCreateView<FormModuleMenu>();

            _formModuleMenu.SetEventHandler(viewEventHandler);

            string info = null;
            if (consignmentEntity != null)
            {
                if (consignmentEntity is ConsignmentItem)
                    info = string.Format("{0}: {1}", GlobalTexts.Item, consignmentEntity.CastToConsignmentItem().ItemId);
                else if (consignmentEntity is LoadCarrier)
                    info = string.Format("{0}: {1}", GlobalTexts.Carrier, consignmentEntity.CastToLoadCarrier().LoadCarrierId);
                else
                    info = string.Format("{0}: {1}", GlobalTexts.Consignment, consignmentEntity.CastToConsignment().ConsignmentId);

            }

            var formData = new FormDataModuleMenu
            {
                HeaderText = headerText,
                ModuleMenu = ModuleMenu,
                CancelEvent = (int)FlowStateCorrectionMenu.ActionMenuCanceled,
                Info = info
            };
            ViewCommands.ShowView(_formModuleMenu, formData);
        }


        private ModuleMenu CreateCorrectionMenu(FlowDataCorrectionMenu flowDataCorrectionMenu, bool registerTimeConsumptionVisible)
        {
            var moduleMenu = new ModuleMenu();

            moduleMenu.MenuItems = new[]
            {
                new ModuleMenuItem
                {
                    Text = GlobalTexts.RegisterDamage,
                    Roles = new[]
                    {
                        new RoleData(Role.Driver1, 1), new RoleData(Role.Driver2, 1), new RoleData(Role.HubWorker1, 1),
                        new RoleData(Role.Landpostbud, 1), new RoleData(Role.PostOfficeAndStore, 1), 
                        new RoleData(Role.HomeDeliveryHub, 1)
                    },
                    ActionIndex = (int) FlowStateCorrectionMenu.FlowRegisterDamage,
                    Hidden = BaseModule.CurrentFlow.CurrentProcess == Process.RegisterDamage || flowDataCorrectionMenu.ConsignmentEntity is LoadCarrier
                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.CorrectWeightAndVolume,
                    Roles = new[]{new RoleData(Role.Driver1, 2), new RoleData(Role.Driver2, 2), new RoleData(Role.HubWorker1, 2)},
                    ActionIndex = (int) FlowStateCorrectionMenu.FlowCorrectWeightAndVolume,
                    Hidden = flowDataCorrectionMenu.ConsignmentEntity is LoadCarrier

                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.ChangeProductGroup,
                    Roles = new[]{new RoleData(Role.Driver1, 3), new RoleData(Role.Driver2, 5), new RoleData(Role.HubWorker1, 3) },
                    ActionIndex = (int) FlowStateCorrectionMenu.FlowChangeProductGroup,
                    Hidden = ActionCommandsCorrectionMenu.HasLegalProductCategoryChanges(flowDataCorrectionMenu.ConsignmentEntity) == false
                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.Delete,
                    Roles = new[]{new RoleData(Role.Driver1, 4), new RoleData(Role.Driver2, 3), new RoleData(Role.HubWorker1, 4), 
                        new RoleData(Role.Customer, 1), new RoleData(Role.Landpostbud, 2), new RoleData(Role.PostOfficeAndStore, 2), new RoleData(Role.HomeDeliveryHub, 2)},
                    ActionIndex = (int) FlowStateCorrectionMenu.ActionDeleteEntity,
                    Disabled = flowDataCorrectionMenu.DisableDeleteButton
                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.RegisterTemperature,
                    Roles = new[]{new RoleData(Role.Driver1, 5), new RoleData(Role.HubWorker1, 5)},
                    ActionIndex = (int) FlowStateCorrectionMenu.FlowRegisterTemperature,
                    Disabled = flowDataCorrectionMenu.ConsignmentEntity == null
                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.BulkRegistration,
                    Roles = new[]{new RoleData(Role.Driver1, 6), new RoleData(Role.HubWorker1, 6)},
                    ActionIndex = (int) FlowStateCorrectionMenu.FlowBulkRegistration,
                    Hidden = ShouldHideBulkRegistration(flowDataCorrectionMenu)
                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.GeneralDeviations,
                    Roles = new[] {new RoleData(Role.Driver2, 4), new RoleData(Role.HubWorker1, 7), new RoleData(Role.Landpostbud, 3), new RoleData(Role.PostOfficeAndStore, 3), new RoleData(Role.HomeDeliveryHub, 3)},
                    ActionIndex = (int)FlowStateCorrectionMenu.FlowGeneralDeviations,
                    Hidden = (flowDataCorrectionMenu.ConsignmentEntity is Consignment || BaseModule.CurrentFlow.CurrentProcess == Process.GeneralDeviations)
                },
                new ModuleMenuItem
                {
                    Text = GlobalTexts.RegisterTimeConsumption,
                    Roles = new[] {new RoleData(Role.Driver1, 7), new RoleData(Role.Driver2, 6)},
                    ActionIndex = (int) FlowStateCorrectionMenu.FlowRegisterTimeConsumptionOnDelivery,
                    Hidden = registerTimeConsumptionVisible == false
                }
            };

            moduleMenu.MenuItems = moduleMenu.FilterItems(ModelUser.UserProfile.Role, ModelUser.UserProfile.TmsAffiliation);
            return moduleMenu;
        }

        internal bool ShouldHideBulkRegistration(FlowDataCorrectionMenu flowDataCorrectionMenu)
        {
            var consignment = flowDataCorrectionMenu.ConsignmentEntity as Consignment;
            if (consignment == null)//case consignment is not scanned
            {
                return true;
            }
            if (consignment.IsScannedOnline)//if scanned online should be visible if mass registration is set
            {
                return consignment.IsMassRegistrationAllowed == false;
            }
            return false;//if scanned offline should be visible

        }

        internal void ShowCorrectionMenu()
        {
            _formModuleMenu.OnUpdateView(null);
        }

    }
}

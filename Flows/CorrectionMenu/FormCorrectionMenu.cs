﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using System.Collections.Generic;
using System.Windows.Forms;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Storage;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu
{
    public partial class FormCorrectionMenu : BaseForm
    {
        private FormDataCorrectionMenu _formDataCorrectionMenu;
        private Button _btnMenuDelete;
        private Button _btnMenuGeneralDeviation;
        private Button _btnAlterProductCategory;
        private int _currentButtonIndex = 0;
        private const int buttonSize = 6;
        public bool Result { get; set; }

        public FormCorrectionMenu()
        {
            InitializeComponent();
            SetTextToGui();
            ShowCorrectionMenu();
        }

        private void SetTextToGui()
        {
            buttonBack.Text = GlobalTexts.Back;
        }


        #region Private Methods and Functions

        public override void OnShow(IFormData formData)
        {
            if (formData == null) return;
            _formDataCorrectionMenu = (FormDataCorrectionMenu)formData;
            labelHeading.Text = _formDataCorrectionMenu.HeaderText;
            if (_btnMenuDelete != null) _btnMenuDelete.Enabled = !_formDataCorrectionMenu.DisableDeleteButton;
            if (!(_formDataCorrectionMenu.ConsignmentEntity is ConsignmentItem))
                _btnMenuGeneralDeviation.Enabled = false;
            if (_btnAlterProductCategory != null)
                _btnAlterProductCategory.Enabled = _formDataCorrectionMenu.HasLegalProductCategoryChanges;//Enable only if product group can be changed
        }

        /// <summary>
        /// This method dynamically creates buttons, for all correction menu items.
        /// </summary>
        private void ShowCorrectionMenu()
        {
            int controlHeight = ScaleUtil.GetScaledPosition(40); //Specifies the height for each control.
            int spaceInControls = ScaleUtil.GetScaledPosition(14); // Specifies the vertical space between rows of controls.
            int yCoordinate = ScaleUtil.GetScaledPosition(105); // Specifies the initial vertical point from where drawing of controls will start
            SuspendLayout();

            var controlSize = new System.Drawing.Size(ScaleUtil.GetScaledPosition(400), ScaleUtil.GetScaledPosition(50));
            Boolean isPagesizeAdded = false;
            this.Controls.Clear();
            int count = 0;
            //Set instruction




            //Get tmi settings from database on the basis of UserRole, TMS and current process

            List<TmiSetting> correctionMenuItems = GetTMISettings();

            foreach (var menuItem in correctionMenuItems)
            {

                if (count < _currentButtonIndex)
                {
                    count++;
                    continue;

                }
                if (count >= (_currentButtonIndex + buttonSize))
                {
                    isPagesizeAdded = true;
                    _currentButtonIndex += buttonSize;
                    break;

                }

                var btnMenuItem = new Button
                                      {
                                          Location = new System.Drawing.Point(ScaleUtil.GetScaledPosition(45), yCoordinate),
                                          Size = controlSize,
                                          Text = menuItem.SettingValue,
                                          ForeColor = System.Drawing.Color.White,
                                          BackColor = System.Drawing.Color.FromArgb(46, 48, 50),
                                          Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular)
                                      };

                switch (menuItem.SettingName)
                {
                    case "Delete":
                        _btnMenuDelete = btnMenuItem;
                        break;
                    case "Package type / Transport equipment":
                        if (!SettingDataProvider.Instance.GetTmiValue<bool>(
                            TmiSettingType.IsTransportEquipmentVerificationRequired) &&
                            !SettingDataProvider.Instance.GetTmiValue<bool>(
                                TmiSettingType.IsPackageTypeVerificationRequired))
                            btnMenuItem.Enabled = false;
                        break;
                    case "General Deviation":
                        _btnMenuGeneralDeviation = btnMenuItem;
                        break;
                    case "Alter Product Group":
                        _btnAlterProductCategory = btnMenuItem;
                        break;

                }
                btnMenuItem.Click += OnMenuItemClicked;
                btnMenuItem.Tag = menuItem.SettingName;
                Controls.Add(btnMenuItem);
                //Update vertical point by adding control space and control height.
                yCoordinate = yCoordinate + controlHeight + spaceInControls;
                count++;
            }

            Resco.Controls.OutlookControls.ImageButton buttonBack = new Resco.Controls.OutlookControls.ImageButton();
            buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
            buttonBack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
            buttonBack.ForeColor = System.Drawing.Color.White;
            buttonBack.Location = new System.Drawing.Point(242, 480);
            buttonBack.Name = "buttonCancel";
            buttonBack.Size = new System.Drawing.Size(230, 50);
            buttonBack.TabIndex = 29;
            buttonBack.Text = GlobalTexts.Back;
            buttonBack.Click += new System.EventHandler(ButtonBackClick);
            this.Controls.Add(buttonBack);

            if (isPagesizeAdded)
            {
                Resco.Controls.OutlookControls.ImageButton buttonMore = new Resco.Controls.OutlookControls.ImageButton();
                buttonMore.BackColor = System.Drawing.Color.FromArgb((int)(((byte)(46))), ((int)(((byte)(48)))), ((int)(((byte)(50)))));
                buttonMore.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular);
                buttonMore.ForeColor = System.Drawing.Color.White;
                buttonMore.Location = new System.Drawing.Point(10, 480);
                buttonMore.Name = "buttonCancel";
                buttonMore.Size = new System.Drawing.Size(230, 50);
                buttonMore.TabIndex = 29;
                buttonMore.Text = "More";
                buttonMore.Click += new System.EventHandler(ButtonMoreClick);
                this.Controls.Add(buttonMore);
            }

            if (!isPagesizeAdded)
            {
                if (correctionMenuItems.Count % buttonSize > 0)
                {
                    _currentButtonIndex = ((correctionMenuItems.Count / buttonSize) * buttonSize) + buttonSize;
                }
            }
            if (_btnAlterProductCategory != null)
                _btnAlterProductCategory.Enabled = _formDataCorrectionMenu.HasLegalProductCategoryChanges;//Enable only if product group can be changed
            ResumeLayout(false);
        }

        private void ButtonMoreClick(object sender, EventArgs e)
        {
            try
            {
                ShowCorrectionMenu();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormGoodsMenu.ButtonMoreClick");
                throw;
            }
        }


        /// <summary>
        /// This function returns list of TmiSetting objects, that we need to display on this screen.
        /// </summary>
        /// <returns>Specifies list of TmiSetting objects.</returns>
        private List<TmiSetting> GetTMISettings()
        {
            var tmiSettings = new List<TmiSetting>
              {
                  new TmiSetting {SettingName = "Register damage", SettingValue = GlobalTexts.Registerdamage},
                  new TmiSetting {SettingName = "Correct w/v", SettingValue = GlobalTexts.CorrectWeightVolume},
                  new TmiSetting {SettingName = "Delete", SettingValue = GlobalTexts.Delete},
                  new TmiSetting {SettingName = "Register temperature", SettingValue = GlobalTexts.Registertemprature},
                  new TmiSetting {SettingName = "Package type / Transport equipment", SettingValue = GlobalTexts.PackagetypeSlashTransportEquipment},
                  new TmiSetting {SettingName = "General Deviation", SettingValue = GlobalTexts.GeneralDeviations},
                  new TmiSetting { SettingName = "Alter Product Group", SettingValue = GlobalTexts.AlterProductGroup },
                  new TmiSetting { SettingName = "Bulk Registration", SettingValue = GlobalTexts.BulkRegistration }
              };
            return tmiSettings;
        }

        #endregion

        #region Events
        /// <summary>
        /// This method acts as handler for back button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonBackClick(object sender, EventArgs e)
        {
            try
            {
                if (_currentButtonIndex > buttonSize)
                {
                    _currentButtonIndex -= buttonSize * 2;
                    ShowCorrectionMenu();
                }
                else
                {
                    ViewEvent.Invoke(CorrectionMenuEvent.Back, null);
                    SetEventHandler(null);
                }




            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormCorrectionMenu.ButtonBackClick");
                throw;
            }
        }

        /// <summary>
        /// This method acts as handler for any button from correction menu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMenuItemClicked(object sender, EventArgs e)
        {
            try
            {
                var menuItem = Convert.ToString(((Button)sender).Tag);

                switch (menuItem)
                {
                    case "Delete":
                        ViewEvent.Invoke(CorrectionMenuEvent.Delete, null);
                        break;
                    case "Register damage":
                        ViewEvent.Invoke(CorrectionMenuEvent.RegisterDamage, null);
                        break;
                    case "Correct w/v":
                        ViewEvent.Invoke(CorrectionMenuEvent.Correct, null);
                        break;
                    case "General Deviation":
                        ViewEvent.Invoke(CorrectionMenuEvent.GeneralDeviation, null);
                        break;
                    case "Register temperature":
                        ViewEvent.Invoke(CorrectionMenuEvent.RegisterTemperature, null);
                        break;
                    case "Package type / Transport equipment":
                        ViewEvent.Invoke(CorrectionMenuEvent.PackageTypeOrTransportEquipment, null);
                        break;
                    case "Alter Product Group":
                        ViewEvent.Invoke(CorrectionMenuEvent.AlterProductGroup, null);//
                        break;
                    case "Bulk Registration":
                        ViewEvent.Invoke(CorrectionMenuEvent.BulkRegistration, null);
                        break;
                }

                Result = true;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FormCorrectionMenu.OnMenuItemClicked");
                Result = false;
            }
        }
        #endregion

    }
    public class FormDataCorrectionMenu : BaseFormData
    {
        public bool DisableDeleteButton { get; set; }
        public ConsignmentEntity ConsignmentEntity;
        public bool HasLegalProductCategoryChanges { get; set; }
    }

    public class CorrectionMenuEvent : ViewEvent
    {
        public const int Back = 0;
        public const int Delete = 1;
        public const int RegisterDamage = 2;
        public const int Correct = 3;
        public const int RegisterTemperature = 4;
        public const int PackageTypeOrTransportEquipment = 5;
        public const int GeneralDeviation = 6;
        public const int GetMessage = 7;
        public const int MessageBack = 8;
        public const int AlterProductGroup = 9;
        public const int BulkRegistration = 10;

        public static implicit operator CorrectionMenuEvent(int viewEvent)
        {

            return new CorrectionMenuEvent { Value = viewEvent };
        }
    }
}
﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.EventHelpers;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu
{
    public class ActionCommandsCorrectionMenu
    {
        private readonly MessageHolder _messageHolder;
        //TODO :: Write Unit Test
        public ActionCommandsCorrectionMenu()
        {
            _messageHolder = new MessageHolder();
        }

        //TODO :: Write Unit Test
        public bool ValidateFlowResult(BaseFlowResult subflowResult, FlowResultCorrectionMenu flowResult)
        {
            if (subflowResult != null && string.IsNullOrEmpty(subflowResult.Message) == false)
            {
                _messageHolder.Update(subflowResult.Message, MessageState.Information);
            }
            else
            {
                _messageHolder.Clear();
            }
            if (subflowResult != null) 
                flowResult.State = subflowResult.State;

            flowResult.MessageHolder = _messageHolder;
            if (flowResult.State != FlowResultState.Ok)
            {
                _messageHolder.Update(_messageHolder.Text, MessageState.Error); 
                return false;
            }
            return true;
        }

        //TODO :: Write Unit Test
        public static void ValidateSubflowResultWithChangedEntityMap(ref EntityMap entityMap, IFlowResult flowResult, ref MessageHolder messageHolder, ref ConsignmentEntity currentScanning)
        {
            var flowResultScanCorrectionMenu = (FlowResultCorrectionMenu)flowResult;

            if (flowResultScanCorrectionMenu.EntityMap != null)
            {
                entityMap = flowResultScanCorrectionMenu.EntityMap;
            }

            if (entityMap.IsScannedBefore(currentScanning) == false)
                currentScanning = null;

            if (flowResultScanCorrectionMenu.MessageHolder != null)
            {
                if (flowResultScanCorrectionMenu.MessageHolder.State != MessageState.Empty)
                {
                    messageHolder = flowResultScanCorrectionMenu.MessageHolder;
                }
            }

        }
        //TODO :: Write Unit Test
        /// <summary>
        /// Returns if this product category can be changed into another alternative
        /// </summary>
        public static bool HasLegalProductCategoryChanges(ConsignmentEntity consignmentEntity)
        {
            bool result = false;
            if (consignmentEntity is ConsignmentItem) //If item is scanned no product group information is available
                result = true;
            else
            {
                if (consignmentEntity != null && string.IsNullOrEmpty(consignmentEntity.ProductCategory) == false)
                {
                    var possibleGroupTransitions =
                        SettingDataProvider.Instance.GetProductGroupTransitions(consignmentEntity.ProductCategory,
                        ModelUser.UserProfile.LanguageCode);

                    result = possibleGroupTransitions.Count > 0;
                }
            }
            return result;
        }

        //TODO :: Write Unit Test
        internal bool DeleteEntity(FlowDataCorrectionMenu flowData, FlowResultCorrectionMenu flowResult)
        {
            bool result = false;
            try
            {
                // FOT 86 extension
                if (flowData != null)
                {
                    if(flowData.ConsignmentEntity is ConsignmentItem)
                    {
                        var modalResult = GuiCommon.ShowModalDialog(GlobalTexts.Delete,
                                                               string.Format(GlobalTexts.AreYouSureYouWantToDelete,
                                                                             flowData.ConsignmentEntity.EntityId),
                                                               Severity.Warning,
                                                               GlobalTexts.Yes, GlobalTexts.No);
                        if (modalResult == GlobalTexts.No)
                        {
                            _messageHolder.Update(GlobalTexts.DeviationProcessAborted, MessageState.Warning);
                            flowResult.MessageHolder = _messageHolder;
                            flowResult.EntityMap = flowData.EntityMap;
                            return true;
                        }
                    }

                    EventReportHelper.SendItemDeleteEvent(flowData.ConsignmentEntity);
                    flowData.EntityMap.RemoveEntity(flowData.ConsignmentEntity);
                    // Remove consignment from VAS history, so ShowOnlyOnce settings will show again
                    if (flowData.WorkProcess != WorkProcess.None)
                    {
                        flowData.VasHistory.DeleteProcessHistoryForConsignmentEntity(flowData.ConsignmentEntity);
                    }

                    string text = flowData.ConsignmentEntity is Consignment ? 
                        (flowData.ConsignmentEntity is LoadCarrier ? String.Format(GlobalTexts.LoadCarrierDeleted, flowData.ConsignmentEntity.EntityDisplayId)
                                                     : String.Format(GlobalTexts.ItemDeleted, flowData.ConsignmentEntity.EntityDisplayId))
                                              : String.Format(GlobalTexts.ConsignmentItemDeleted, flowData.ConsignmentEntity.EntityDisplayId);

                    _messageHolder.Update(text, MessageState.Information);
                    flowResult.EntityMap = flowData.EntityMap;

                }

                flowResult.MessageHolder = _messageHolder;
               // flowResult.IsToPreventUpdate = true; //SYED ADDED THIS TO FIX DEFECT 1116
                result = true;
            }

            catch (Exception e)
            {
                _messageHolder.Update(String.Format(GlobalTexts.ProcessForDeleteItemFailed, flowData.ConsignmentEntity.EntityDisplayId), MessageState.Error);
                Logger.LogException(e, "ActionCommandsCorrectionMenu.DeleteEntity");
            }

            return result;
        }

        internal ViewCommandsCorrectionMenu ValidateVasMatrixResult(FlowDataCorrectionMenu flowDataCorrectionMenu, FlowResultVasMatrix flowResultVasMatrix)
        {
            // VasMatrix can only trigger on subflow CanRegisterTimeConsumptions, so we validate very easily
            // Only if vasmatrix triggered we have an InnerFlowResult
            bool registerTimeConsumptionVisible = flowResultVasMatrix.InnerFlowResult != null &&
                                              flowResultVasMatrix.InnerFlowResult.State == FlowResultState.Ok;
            return new ViewCommandsCorrectionMenu(flowDataCorrectionMenu, registerTimeConsumptionVisible);
        }
    }
}


﻿using System;
using Com.Bring.PMP.PreComFW.CommunicationEntity.OperationList;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Controls;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Flows.BulkRegistration;
using Com.Bring.PMP.PreComFW.Shared.Flows.ChangeProductGroup;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume;
using Com.Bring.PMP.PreComFW.Shared.Flows.CorrectWeightAndVolume.Model;
using Com.Bring.PMP.PreComFW.Shared.Flows.GeneralDeviation;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterDamage;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTemperature;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using Com.Bring.PMP.PreComFW.Shared.Storage;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using Com.Bring.PMP.PreComFW.Shared.Flows.RegisterTimeConsumption;

namespace Com.Bring.PMP.PreComFW.Shared.Flows.CorrectionMenu
{
    public enum FlowStateCorrectionMenu
    {
        ActionDeleteEntity,
        ActionValidateFlowResult,
        ActionValidateFlowResultWithUpdatedEntityMap,
        ActionMenuCanceled,
        ActionBackFromFlowVasMatrix,
        FlowCommands,

        ViewInitCorrectionMenu,
        ViewShowCorrectionMenu,

        FlowCheckCanRegisterTimeConsumption,
        FlowRegisterDamage,
        FlowCorrectWeightAndVolume,
        FlowChangeProductGroup,
        FlowRegisterTemperature,
        FlowBulkRegistration,
        FlowRegisterTimeConsumptionOnDelivery,
        FlowGeneralDeviations,

        ThisFlowComplete,
    }

    public class FlowDataCorrectionMenu : FlowDataModuleMenu
    {
        public FlowDataCorrectionMenu(string headerText, ConsignmentEntity consignmentEntity, EntityMap entityMap)
        {
            HeaderText = headerText;
            ConsignmentEntity = consignmentEntity;
            EntityMap = entityMap;

            ParentWorkProcessFlow = BaseModule.ParentWorkProcessFlow ?? BaseModule.CurrentFlow;

            VasHistory = ParentWorkProcessFlow.VasHistory;
            WorkProcess = ParentWorkProcessFlow.WorkProcess;
        }

        public WorkProcess WorkProcess
        {
            get;
            private set;
        }

        public ConsignmentEntity ConsignmentEntity { get; private set; }
        public EntityMap EntityMap { get; private set; }
        public PlannedConsignmentsType[] PlannedConsignments;
        public bool DisableDeleteButton { get; set; }
        public BaseFlow ParentWorkProcessFlow { get; private set; }
        public VasHistory VasHistory { get; private set; }
    }

    public class FlowResultCorrectionMenu : BaseFlowResult
    {
        public MessageHolder MessageHolder { get; set; }
        public EntityMap EntityMap { get; set; }
       // public bool IsToPreventUpdate { get; set; }//SYED ADDED THIS TO FIX DEFECT 1116
    }

    public class FlowCorrectionMenu : BaseFlow
    {
        private ViewCommandsCorrectionMenu _viewCommandsCorrectionMenu;
        private readonly ActionCommandsCorrectionMenu _actionCommandsCorrectionMenu;
        private readonly FlowResultCorrectionMenu _flowResult = new FlowResultCorrectionMenu();
        private FlowDataCorrectionMenu _flowData;
        private FlowStateCorrectionMenu _returnState;
        private readonly MessageHolder _messageHolder = new MessageHolder();


        public FlowCorrectionMenu()
        {
            _actionCommandsCorrectionMenu = new ActionCommandsCorrectionMenu();
        }

        public override void BeginFlow(IFlowData flowData)
        {

            Logger.LogEvent(Severity.Debug, "Initializing flow FlowCorrectionMenu");
            _flowData = (FlowDataCorrectionMenu)flowData;

            _returnState = FlowStateCorrectionMenu.ViewShowCorrectionMenu;

            ExecuteState((int)FlowStateCorrectionMenu.FlowCheckCanRegisterTimeConsumption);

        }

        public override void ExecuteState(int state)
        {
            var flowState = (FlowStateCorrectionMenu)state;
            if (flowState > FlowStateCorrectionMenu.FlowCommands)
            {
                ExecuteFlowState(flowState);
            }
            else
            {
                ExecuteActionState(flowState);
            }
        }

        public override WorkProcess WorkProcess
        {
            get
            {
                if (_flowData != null)
                    return _flowData.WorkProcess;
                return WorkProcess.Correction;
            }
        }


        private void ExecuteActionState(FlowStateCorrectionMenu state)
        {
            try
            {
                do
                {
                    Logger.LogEvent(Severity.Debug, "#FlowCorrectionMenu;" + state);

                    bool result;
                    switch (state)
                    {
                        case FlowStateCorrectionMenu.ActionBackFromFlowVasMatrix:
                            var flowResultVasMatrix = (FlowResultVasMatrix)SubflowResult;
                            _viewCommandsCorrectionMenu = _actionCommandsCorrectionMenu.ValidateVasMatrixResult(_flowData, flowResultVasMatrix);
                            state = FlowStateCorrectionMenu.ViewInitCorrectionMenu;
                            break;

                        case FlowStateCorrectionMenu.ActionDeleteEntity:
                            result = _actionCommandsCorrectionMenu.DeleteEntity(_flowData, _flowResult);
                            state = result ? FlowStateCorrectionMenu.ThisFlowComplete : _returnState;
                            break;

                        case FlowStateCorrectionMenu.ActionValidateFlowResult:
                            result = _actionCommandsCorrectionMenu.ValidateFlowResult((BaseFlowResult)SubflowResult, _flowResult);
                            state = result ? FlowStateCorrectionMenu.ThisFlowComplete : _returnState;
                            break;

                        case FlowStateCorrectionMenu.ActionMenuCanceled:
                            _messageHolder.Update(GlobalTexts.CorrectionMenuAborted, MessageState.Warning);
                            _flowResult.State = FlowResultState.Cancel;
                            _flowResult.MessageHolder = _messageHolder;
                            state = FlowStateCorrectionMenu.ThisFlowComplete;
                            break;

                        default:
                            LogUnhandledTransitionState(state.ToString());
                            break;
                    }
                } while (state < FlowStateCorrectionMenu.FlowCommands);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "FlowPickUp.ExecuteActionState");

                _flowResult.State = FlowResultState.Exception;
                state = FlowStateCorrectionMenu.ThisFlowComplete;
            }


            ExecuteFlowState(state);
        }

        private void ExecuteFlowState(FlowStateCorrectionMenu state)
        {
            try
            {
                Logger.LogEvent(Severity.Debug, "#FlowCorrectionMenu;" + state);
                switch (state)
                {
                    case FlowStateCorrectionMenu.FlowCheckCanRegisterTimeConsumption:
                        var flowDataVasMatrix = new FlowDataVasMatrix(_flowData.ConsignmentEntity, _flowData.ParentWorkProcessFlow)
                        {
                            AttributeEqualTo = "CanRegisterTimeConsumption",
                            PlaySuccessSound = false,
                            HeaderText = _flowData.HeaderText
                        };
                        StartSubFlow<FlowVasMatrix>(flowDataVasMatrix, (int)FlowStateCorrectionMenu.ActionBackFromFlowVasMatrix, Process.Inherit);
                        break;

                    case FlowStateCorrectionMenu.ViewInitCorrectionMenu:
                        _viewCommandsCorrectionMenu.InitCorrectionMenu(CorrectionMenuViewEventHandler, _flowData.HeaderText, _flowData.ConsignmentEntity);
                        break;

                    case FlowStateCorrectionMenu.ViewShowCorrectionMenu:
                        _viewCommandsCorrectionMenu.ShowCorrectionMenu();
                        break;

                    case FlowStateCorrectionMenu.FlowRegisterDamage:
                        var flowDataRegisterDamage = new FlowDataRegisterDamage
                        {
                            DamageEntity = _flowData.ConsignmentEntity,
                            EntityMap = _flowData.EntityMap
                        };
                        StartSubFlow<FlowRegisterDamage>(flowDataRegisterDamage, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.RegisterDamage);
                        break;

                    case FlowStateCorrectionMenu.FlowCorrectWeightAndVolume:
                        var flowCorrectWeightAndVolumeData = new FlowDataCorrectWeightAndVolume
                        {
                            HeaderText = GlobalTexts.CorrectWeightVolume,
                            ConsignmentEntity = _flowData.ConsignmentEntity,
                        };
                        StartSubFlow<FlowCorrectWeightAndVolume>(flowCorrectWeightAndVolumeData, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.CorrectWeightVolume);
                        break;

                    case FlowStateCorrectionMenu.FlowChangeProductGroup:
                        var flowDataChangeProductGroup = new FlowDataProductGroup
                        {
                            HeaderText = GlobalTexts.AlterProductGroup,
                            ConsignmentEntity = _flowData.ConsignmentEntity,
                        };
                        StartSubFlow<FlowChangeProductGroup>(flowDataChangeProductGroup, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.ChangeProductGroup);
                        break;

                    case FlowStateCorrectionMenu.FlowRegisterTemperature:
                        var flowRegisterTemperatureData = new FlowRegisterTemperatureData
                        {
                            ConsignmentEntity = _flowData.ConsignmentEntity,
                            EntityMap = _flowData.EntityMap
                        };
                        StartSubFlow<FlowRegisterTemperature>(flowRegisterTemperatureData, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.RegisterTemperature);
                        break;


                    case FlowStateCorrectionMenu.FlowBulkRegistration:
                        var flowDataBulkRegistration = new FlowDataBulkRegistration
                        {
                            ConsignmentEntity = _flowData.ConsignmentEntity,
                            HeaderText = CurrentProcess.ToString(),
                            EntityMap = _flowData.EntityMap
                        };
                        StartSubFlow<FlowBulkRegistration>(flowDataBulkRegistration, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.Inherit);
                        break;

                    case FlowStateCorrectionMenu.FlowGeneralDeviations:
                        var flowDataGeneralDeviation = new FlowDataGeneralDeviations
                        {
                            HeaderText = GlobalTexts.GeneralDeviations,
                            IsStartedFromDoubleScan = true,
                            CurrentConsignmentEntity = _flowData.ConsignmentEntity
                        };
                        StartSubFlow<FlowGeneralDeviations>(flowDataGeneralDeviation, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.GeneralDeviations);
                        break;

                    case FlowStateCorrectionMenu.FlowRegisterTimeConsumptionOnDelivery:
                        var flowDataRegisterTimeConsumption = new FlowDataRegisterTimeConsumptionOnDelivery
                        {
                            ConsignmentEntity = _flowData.ConsignmentEntity
                        };
                        StartSubFlow<FlowRegisterTimeConsumptionOnDelivery>(flowDataRegisterTimeConsumption, (int)FlowStateCorrectionMenu.ActionValidateFlowResult, Process.RegisterTimeConsumption);
                        break;

                    case FlowStateCorrectionMenu.ThisFlowComplete:
                        BaseModule.EndSubFlow(_flowResult);
                        break;

                    default:
                        LogUnhandledTransitionState(state.ToString());
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e, "state: " + state);
                _flowResult.State = FlowResultState.Exception;
                BaseModule.EndSubFlow(_flowResult);
            }
        }

        private void CorrectionMenuViewEventHandler(int correctionMenuViewEvent, params object[] data)
        {
            ExecuteState(correctionMenuViewEvent);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    public enum BehaviourCode
    {
        Ignore = 0,
        DisplayErrorDialog = 1,
        DisplayExclusiveWarningDialog = 2,
        DisplayExclusiveInfoDialog = 3,
        DisplayWarningDialog = 4,
        TriggerSubProcess = 5,
        DisplayTextDialog = 6,
        DisplayTextInline = 7,
        NoAction = 8,
        Abort = 9
    }


    public static class DataReaderExtensions
    {
        public static string GetStringValue(this IDataReader dataReader, string column)
        {
            var result = dataReader.GetValue(dataReader.GetOrdinal(column)) as string;
            return result;
        }

        public static int GetIntegerValue(this IDataReader dataReader, string column)
        {
            var result = Convert.ToInt32(dataReader.GetValue(dataReader.GetOrdinal(column)));
            return result;
        }

        public static bool GetBooleanValue(this IDataReader dataReader, string column)
        {
            var result = Convert.ToBoolean(dataReader.GetValue(dataReader.GetOrdinal(column)));
            return result;
        }

        public static string GetOptionalStringValue(this IDataReader dataReader, string column)
        {
            string result = string.Empty;
            int ordinal = dataReader.GetOrdinal(column);
            if (dataReader.IsDBNull(ordinal) == false)
                result = dataReader.GetValue(dataReader.GetOrdinal(column)).ToString();
            return result;
        }

        public static BehaviourCode GetBehaviourCodeValue(this IDataReader dataReader, string column)
        {
            var result = (BehaviourCode)GetIntegerValue(dataReader, column);
            return result;
        }
    }

    partial class SettingDataProvider
    {
        //private readonly SqlCeProcessSupportDataProvider _sqlCeProcessSupportDataProvider;
        private readonly FotProcessSupportDataProvider _fotProcessSupportDataProvider;


        public List<ProcessSupportData> GetProcessSupportData(VasQueryParameters vasQueryParameters)
        {

            var processSupportDataFromTable = _fotProcessSupportDataProvider.RetrieveProcessSupportData(vasQueryParameters);
            //var processSupportDataFromSqlCe = _sqlCeProcessSupportDataProvider.RetrieveProcessSupportData(vasQueryParameters);

            //if (Compare(processSupportDataFromTable, processSupportDataFromSqlCe) == false)
            //{
            //    Logger.LogEvent(Severity.Error, "Differences detected between result set of new and old VasMatrix functionality");
            //}

            return processSupportDataFromTable;
        }

        //private bool Compare(List<ProcessSupportData> processSupportDataFromTable, List<ProcessSupportData> processSupportDataFromSqlCe)
        //{
        //    processSupportDataFromTable = ActionCommandsVasMatrix.RemoveDuplicates(processSupportDataFromTable);
        //    processSupportDataFromSqlCe = ActionCommandsVasMatrix.RemoveDuplicates(processSupportDataFromSqlCe);

        //    if (processSupportDataFromSqlCe.Count != processSupportDataFromTable.Count)
        //        return false;

        //    for (int i = 0; i < processSupportDataFromSqlCe.Count; i++)
        //    {
        //        if (processSupportDataFromTable[i].Equals(processSupportDataFromSqlCe[i]) == false)
        //            return false;
        //    }

        //    return true;
        //}

        void CreateVasMatrixTables()
        {
            if (HaveOldVasTableDefinitions)
            {
                DeleteAllVasTables();
            }

            CreateVasMatrixTable();
            CreateVasMatrixVasCodesTable();
            CreateVasMatrixProductCodesTable();
            CreateVasMatrixRolesTable();
            CreateVasMatrixMessagesTable();
            CreateVasMatrixTmsNamesTable();
            CreateVasMatrixWorkProcessesTable();
        }

        /// <summary>
        /// Delete all vas tabeles so they can be re-created with new definition
        /// </summary>
        private void DeleteAllVasTables()
        {
            try
            {
                // Need to delete all linked tables, before deletet VasMatrixSettings
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixTmsNames, null);
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixVasCodes, null);
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixWorkProcesses, null);
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixMessages, null);
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixProductCodes, null);
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixRoles, null);
                _sqlCeHelper.ExecuteNonQuery("DROP TABLE " + TableName.PB_VasMatrixSettings, null);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataProvider.DeleteAllVasTables");
            }
        }

        private bool HaveOldVasTableDefinitions
        {
            get
            {
                if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixSettings))
                {
                    var dataReader = _sqlCeHelper.ExecuteDataReader("select top (1) * from  " + TableName.PB_VasMatrixSettings, null, true);
                    if (dataReader != null)
                    {
                        try
                        {
                            //exception is thrown if column is missing in table...
                            dataReader.GetOrdinal("Query");
                        }
                        catch
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        private void CreateVasMatrixTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixSettings) == false)
            {
                var queryString = string.Format("CREATE TABLE [{0}] (" +
                                                    "[Id] [int] NOT NULL, " +
                                                    "[BehaviourCode] [int] NOT NULL, " +
                                                    "[SubProcessName] [nvarchar](150) NULL, " +
                                                    "[ShowOncePerProcess] [bit] NOT NULL, " +
                                                    "[ShowOncePerConsignment] [bit] NOT NULL, " +
                                                    "[IsCollectionProcess] [bit] NOT NULL, " +
                                                    "[Parameter] [nvarchar](2000) NOT NULL, " +
                                                    "[Priority] [int] NOT NULL, " +
                                                    "[AttributeEqualTo] [nvarchar](50) NULL, " +
                                                    "[Query] [nvarchar](2000) NULL, " +
                                                    "[ItemCount] [nvarchar](10) NULL, " +
                                                    "[IdExclusions] [nvarchar](1000) NULL, " +
                                                    "[UpdatedServerUTC] [datetime] NULL, " +
                                                    "[PMC_Row_Status] [int] NULL," +
                                                    "CONSTRAINT [PK_PB_VasMatrixSettings] PRIMARY KEY ([Id])) ", TableName.PB_VasMatrixSettings);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixSettings);
            }
        }

        void CreateVasMatrixVasCodesTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixVasCodes) == false)
            {
                var queryString = string.Format(
                    "CREATE TABLE [{0}] (" +
                    "[Id] [int] NOT NULL, " +
                    "[VasCode] [nvarchar](20) NOT NULL, " +
                    "[UpdatedServerUTC] [datetime] NULL, " +
                    "[PMC_Row_Status] [int] NULL, " +
                    "CONSTRAINT [PK_PB_VasMatrixVasCodes] PRIMARY KEY (Id, VasCode))", TableName.PB_VasMatrixVasCodes);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixVasCodes);
            }
        }

        void CreateVasMatrixProductCodesTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixProductCodes) == false)
            {
                var queryString = string.Format(
                    "CREATE TABLE [{0}] (" +
                    "[Id] [int] NOT NULL, " +
                    "[ProductCode] [nvarchar](20) NOT NULL, " +
                    "[UpdatedServerUTC] [datetime] NULL, " +
                    "[PMC_Row_Status] [int] NULL, " +
                    "CONSTRAINT [PK_PB_VasMatrixProductCodes] PRIMARY KEY (Id, ProductCode))", TableName.PB_VasMatrixProductCodes);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixProductCodes);
            }
        }


        void CreateVasMatrixRolesTable()
        {
            //_sqlCeHelper.ExecuteNonQuery("DROP TABLE [PB_VasMatrixRoles]", null);

            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixRoles) == false)
            {
                var queryString = string.Format(
                    "CREATE TABLE [{0}] (" +
                    "[Id] [int] NOT NULL, " +
                    "[RoleName] [nvarchar](20) NOT NULL, " +
                    "[UpdatedServerUTC] [datetime] NULL, " +
                    "[PMC_Row_Status] [int] NULL, " +
                    "CONSTRAINT [PK_PB_VasMatrixRoles] PRIMARY KEY (Id, RoleName))", TableName.PB_VasMatrixRoles);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixRoles);
            }
        }

        void CreateVasMatrixMessagesTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixMessages) == false)
            {
                var queryString = string.Format("CREATE TABLE [{0}] (" +
                                            "[Id] [int] NOT NULL, " +
                                            "[LanguageCode] [nchar](10) NOT NULL," +
                                            "[Message] [nvarchar](400) NOT NULL, " +
                                            "[UpdatedServerUTC] [datetime] NULL, " +
                                            "[PMC_Row_Status] [int] NULL, " +
                                            "CONSTRAINT [PK_PB_VasMatrixMessages] PRIMARY KEY (Id, LanguageCode))", TableName.PB_VasMatrixMessages);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixMessages);
            }
        }


        private void CreateVasMatrixTmsNamesTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixTmsNames) == false)
            {
                var queryString = string.Format(
                    "CREATE TABLE [{0}] (" +
                    "[Id] [int] NOT NULL, " +
                    "[TmsName] [nvarchar](20) NOT NULL, " +
                    "[UpdatedServerUTC] [datetime] NULL, " +
                    "[PMC_Row_Status] [int] NULL, " +
                    "CONSTRAINT [PK_PB_VasMatrixTmsNames] PRIMARY KEY (Id, TmsName))", TableName.PB_VasMatrixTmsNames);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixTmsNames);

            }
        }


        void CreateVasMatrixWorkProcessesTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_VasMatrixWorkProcesses) == false)
            {
                var queryString = string.Format("CREATE TABLE [{0}] (" +
                    "[Id] [int] NOT NULL, " +
                    "[WorkProcess] [nvarchar](50) NOT NULL, " +
                    "[UpdatedServerUTC] [datetime] NULL, " +
                    "[PMC_Row_Status] [int] NULL, " +
                    "CONSTRAINT [PK_PB_VasMatrixWorkProcesses] PRIMARY KEY (Id, WorkProcess))", TableName.PB_VasMatrixWorkProcesses);

                _sqlCeHelper.CreateTable(queryString, TableName.PB_VasMatrixWorkProcesses);
            }

        }

    }
}
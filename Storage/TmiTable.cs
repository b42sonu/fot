﻿using System;
using System.Collections.Generic;
using System.Data;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    internal class TmiTable : List<TmiTableEntry>
    {
        internal void MapTableData(IDataReader dataReader)
        {
            try
            {
                Clear();
                while (dataReader != null && dataReader.Read())
                {

                    var processSupportData = new TmiTableEntry(dataReader);
                    Add(processSupportData);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "VasTable.MapTableData");
            }
        }
    }

    internal class TmiTableEntry
    {
        internal TmiTableEntry(IDataReader dataReader)
        {
            try
            {
                TmsAffiliation = dataReader.GetStringValue("TmsId");
                SettingCode = dataReader.GetStringValue("SettingCode");
                UserRole = dataReader.GetStringValue("UserRole");
                ProcessCode = dataReader.GetStringValue("ProcessCode");
                Value = dataReader.GetStringValue("Value");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "TmiTableEntry");
            }
        }

        internal string TmsAffiliation { get; private set; }
        internal string SettingCode { get; private set; }
        internal string ProcessCode { get; private set; }
        internal string UserRole { get; private set; }
        internal string Value { get; private set; }
    }
}

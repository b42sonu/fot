﻿namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    public class ProcessSupportData
    {
        public ProcessSupportData(int id, BehaviourCode behaviourCode, bool showOncePerProcess, bool showOncePerConsignment, string subProcessName,
                                  string message, string param, int priority, string query, string idExclusions, string itemCount)
        {
            Id = id;
            BehaviourCode = behaviourCode;
            ShowOncePerProcess = showOncePerProcess;
            ShowOncePerConsignment = showOncePerConsignment;
            SubProcessName = subProcessName;
            Message = message;
            Param = param;
            Priority = priority;
            Query = query;
            IdExclusions = idExclusions;
            ItemCount = itemCount;
        }

        public int Id { get; private set; }
        public BehaviourCode BehaviourCode { get; set; }
        public bool ShowOncePerProcess { get; private set; }
        public bool ShowOncePerConsignment { get; private set; }
        public string SubProcessName { get; private set; }
        public string Message { get; private set; }
        public string Param { get; private set; }
        public int Priority { get; private set; }
        public string Query { get; private set; }
        public string IdExclusions { get; private set; }
        public string ItemCount { get; private set; }


        protected bool Equals(ProcessSupportData other)
        {
            if (Id != other.Id)
                return false;

            if (BehaviourCode != other.BehaviourCode)
                return false;

            if (ShowOncePerProcess.Equals(other.ShowOncePerProcess) == false)
                return false;

            if (ShowOncePerConsignment.Equals(other.ShowOncePerConsignment) == false)
                return false;

            if (string.Equals(SubProcessName, other.SubProcessName) == false && (!string.IsNullOrEmpty(SubProcessName) || !string.IsNullOrEmpty(other.SubProcessName)))
                return false;

            if (string.Equals(Message, other.Message) == false && (!string.IsNullOrEmpty(Message) || !string.IsNullOrEmpty(other.Message)))
                return false;

            if (string.Equals(Param, other.Param) == false)
                return false;

            if (Priority != other.Priority)
                return false;

            if (string.Equals(Query, other.Query) == false && (!string.IsNullOrEmpty(Query) || !string.IsNullOrEmpty(other.Query)))
                return false;

            if (string.Equals(IdExclusions, other.IdExclusions) == false && (!string.IsNullOrEmpty(IdExclusions) || !string.IsNullOrEmpty(other.IdExclusions)))
                return false;

            if (string.Equals(ItemCount, other.ItemCount) == false && (!string.IsNullOrEmpty(ItemCount) || !string.IsNullOrEmpty(other.ItemCount)))
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode * 397) ^ (int)BehaviourCode;
                hashCode = (hashCode * 397) ^ ShowOncePerProcess.GetHashCode();
                hashCode = (hashCode * 397) ^ ShowOncePerConsignment.GetHashCode();
                hashCode = (hashCode * 397) ^ (SubProcessName != null ? SubProcessName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Message != null ? Message.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Param != null ? Param.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Priority;
                hashCode = (hashCode * 397) ^ (Query != null ? Query.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IdExclusions != null ? IdExclusions.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ItemCount != null ? ItemCount.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object input)
        {
            if (ReferenceEquals(null, input)) return false;
            if (ReferenceEquals(this, input)) return true;
            if (input.GetType() != GetType()) return false;
            return Equals((ProcessSupportData)input);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PMC.PreComX.Mock;
using PreCom.Utils;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    public class FotProcessSupportDataProvider : IProcessSupportDataProvider
    {
        private readonly IStorageBase _storageBase;
        private readonly SqlCeHelper _sqlCeHelper;
        private readonly VasTable _vasTable;

        private string _currentRoleName, _currentTmsName;

        private const string RetrieveQuery =
            "SELECT DISTINCT V.Id, V.IsCollectionProcess, C.VasCode, P.ProductCode, W.WorkProcess, V.AttributeEqualTo, V.BehaviourCode, " +
            "V.ShowOncePerProcess, V.ShowOncePerConsignment, V.SubProcessName, M.Message, V.Parameter, V.Priority, " +
            "V.Query, V.IdExclusions, V.ItemCount FROM PB_VasMatrixSettings AS V INNER JOIN " +
            "PB_VasMatrixVasCodes AS C ON V.Id = C.Id INNER JOIN " +
            "PB_VasMatrixProductCodes AS P ON V.Id = P.Id INNER JOIN " +
            "PB_VasMatrixRoles AS R ON V.Id = R.Id INNER JOIN " +
            "PB_VasMatrixTmsNames AS T ON V.Id = T.Id INNER JOIN " +
            "PB_VasMatrixWorkProcesses AS W ON V.Id = W.Id LEFT OUTER JOIN " +
            "PB_VasMatrixMessages AS M ON V.Id = M.Id AND LanguageCode = @LanguageCode " +
            "WHERE (R.RoleName = @RoleName OR R.RoleName = 'Any') AND (T.TmsName = @TmsName OR T.TmsName = 'Any')";

        public FotProcessSupportDataProvider(IStorageBase storageBase, SqlCeHelper sqlCeHelper)
        {
            _storageBase = storageBase;
            _sqlCeHelper = sqlCeHelper;
            _vasTable = new VasTable();

            Language.LanguageChanged += LanguageChanged;
            ModelUser.SettingsChanged += OnSettingsChanged;
        }

        public void OnSettingsChanged(SettingChangeType settingChangeType)
        {
            switch (settingChangeType)
            {
                case SettingChangeType.SettingsSynced:
                    if (ModelUser.UserProfile != null)
                        ReflectUpdatedSettings();
                    break;

                case SettingChangeType.ProfileUpdated:
                    if (_currentRoleName != ModelUser.UserProfile.RoleName || _currentTmsName != ModelUser.UserProfile.TmsAffiliation)
                        ReflectUpdatedSettings();
                    break;
            }
        }


        void LanguageChanged(LanguageArgs languageArgs)
        {
            ReflectUpdatedSettings();
        }

        public void ReflectUpdatedSettings()
        {
            try
            {
                var paramArray = new[]
                {
                    _storageBase.CreateParameter("@LanguageCode", ModelUser.UserProfile.LanguageCode),
                    _storageBase.CreateParameter("@TmsName", ModelUser.UserProfile.TmsAffiliation),
                    _storageBase.CreateParameter("@RoleName", ModelUser.UserProfile.RoleName)
                };

                using (var dataReader = _sqlCeHelper.ExecuteDataReader(RetrieveQuery, paramArray))
                {
                    if (dataReader != null)
                    {
                        _vasTable.MapTableData(dataReader);
                    }
                }
                _currentRoleName = ModelUser.UserProfile.RoleName;
                _currentTmsName = ModelUser.UserProfile.TmsAffiliation;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FOTProcessSupportDataProvider.ReflectUpdatedSettings");
            }
        }

        public List<ProcessSupportData> RetrieveProcessSupportData(VasQueryParameters vasQueryParameters)
        {
            using (ProfilingMeasurement.MeasureFunction("FOTProcessSupportDataProvider.RetrieveProcessSupportData"))
            {
                var result = new List<ProcessSupportData>();

                foreach (VasTableEntry tableEntry in _vasTable)
                {
                    // Ignore collection process entries
                    if (tableEntry.IsCollectionProcess != vasQueryParameters.IsCollectionProcess)
                        continue;

                    // If we alreadys have added this entry we skip it
                    if (result.Exists(entry => entry.Id == tableEntry.Id))
                        continue;

                    // Check if entry contains required vascode
                    if (tableEntry.VasCode != "Any" && vasQueryParameters.VasCodes.Any(vasCode => vasCode == tableEntry.VasCode) == false)
                        continue;

                    // Check if entry contains required productcode
                    if (tableEntry.ProductCode != "Any" && tableEntry.ProductCode != vasQueryParameters.ProductCode)
                        continue;

                    // Check if entry contains required work-process
                    if (tableEntry.WorkProcess != "Any" && tableEntry.WorkProcess != vasQueryParameters.WorkProcess)
                        continue;

                    // Check if entry contains required attribute
                    if (tableEntry.AttributeEqualTo != "Any" && tableEntry.AttributeEqualTo != vasQueryParameters.AttributeEqualTo)
                        continue;

                    result.Add(tableEntry);
                }
                return result;
            }
        }
    }
}

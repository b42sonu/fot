﻿using System;
using System.Data;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PMC.PreComX.Mock;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    class SqlCeTmiDataProvider : ITmiSettingsDataProvider
    {
        private readonly IStorageBase _storageBase;
        private readonly SqlCeHelper _sqlCeHelper;

        public SqlCeTmiDataProvider()
        {
            var precom = ApplicationWrapper.Wrap(ModelModules.Application);

            _storageBase = precom.Storage;
            _sqlCeHelper = new SqlCeHelper();
        }

        public SqlCeTmiDataProvider(IStorageBase storageBase, SqlCeHelper sqlCeHelper)
        {
            _storageBase = storageBase;
            _sqlCeHelper = sqlCeHelper;
        }

        public T GetTmiValue<T>(TmiSettings settingCode)
        {
            return ConvertTmiResult<T>(GetTmiValueString(settingCode, ModelUser.UserProfile.RoleName, ModelUser.UserProfile.TmsAffiliation));
        }

        public T GetTmiValue<T>(TmiSettings settingCode, string roleName, string tmsAffiliation)
        {
            return ConvertTmiResult<T>(GetTmiValueString(settingCode, roleName, tmsAffiliation));
        }

        public T ConvertTmiResult<T>(string tmiValue)
        {
            if (string.IsNullOrEmpty(tmiValue))
                return default(T);

            switch (typeof(T).Name.ToLower())
            {
                case "int32":
                    return (T)(object)int.Parse(tmiValue);

                case "string":
                    return (T)(object)tmiValue;

                case "boolean":
                    switch (tmiValue.ToUpper())
                    {
                        case "TRUE":
                        case "YES":
                            return (T)(object)true;

                        case "NO":
                        case "FALSE":
                            return (T)(object)false;

                        default:
                            throw new ArgumentException(
                                string.Format("Boolean value {0} is not supported in GetTmiValue",
                                              tmiValue));
                    }

                default:
                    throw new ArgumentException(string.Format("Datatype {0} is not supported in GetTmiValue", typeof(T).Name));
            }
        }

        private string GetTmiValueString(TmiSettings settingName, string roleName, string tmsAffilliation)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Tmi))
            {
                Process process = BaseModule.CurrentFlow != null
                                      ? BaseModule.CurrentFlow.CurrentProcess
                                      : Process.Default;

                // All Fields In Key Is Set
                var value = GetTmiValue(settingName, process, roleName, tmsAffilliation);
                if (string.IsNullOrEmpty(value) == false)
                    return value;

                // Default UserRole
                value = GetTmiValue(settingName, process, "Default", tmsAffilliation);
                if (string.IsNullOrEmpty(value) == false)
                    return value;

                // Default ProcessCode
                value = GetTmiValue(settingName, Process.Default, roleName, tmsAffilliation);
                if (string.IsNullOrEmpty(value) == false)
                    return value;

                // Default UserRole and ProcessCode
                value = GetTmiValue(settingName, Process.Default, "Default", tmsAffilliation);
                if (string.IsNullOrEmpty(value) == false)
                    return value;


                Logger.LogEvent(Severity.Error, "Failed to retrieve TMI-setting '{0}'", settingName);

                return null;
            }
        }

        private const string TmiSettingsQuery =
            "SELECT Value FROM PB_TMISettings WHERE UserRole = @UserRole AND SettingCode = @SettingCode " +
            "AND ProcessCode = @ProcessCode AND TMSId = @TMSId";


        private string GetTmiValue(TmiSettings settingCode, Process process, string userRole,
                                          string tmsAffiliation)
        {
            if (ModelUser.UserProfile != null)
            {
                var parameters = new IDataParameter[4];
                parameters[0] = _storageBase.CreateParameter("@UserRole", userRole);
                parameters[1] = _storageBase.CreateParameter("@SettingCode", settingCode);
                parameters[2] = _storageBase.CreateParameter("@ProcessCode", process);
                parameters[3] = _storageBase.CreateParameter("@TMSId", tmsAffiliation);

                return _sqlCeHelper.ExecuteScalar(TmiSettingsQuery, parameters);
            }
            return null;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Data;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PMC.PreComX.Mock;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    public class SqlCeProcessSupportDataProvider : IProcessSupportDataProvider
    {
        private readonly IStorageBase _storageBase;
        private readonly SqlCeHelper _sqlCeHelper;

        public SqlCeProcessSupportDataProvider(IStorageBase storageBase, SqlCeHelper sqlCeHelper)
        {
            _storageBase = storageBase;
            _sqlCeHelper = sqlCeHelper;
        }

        private const string SelectQuery =
            "SELECT V.Id, V.BehaviourCode, V.ShowOncePerProcess, V.ShowOncePerConsignment, V.SubProcessName, M.Message, " +
            "V.Parameter, V.Priority, V.Query, V.IdExclusions, V.ItemCount " +
            "FROM PB_VasMatrixSettings AS V INNER JOIN " +
            "PB_VasMatrixVasCodes AS C ON V.Id = C.Id INNER JOIN " +
            "PB_VasMatrixProductCodes AS P ON V.Id = P.Id INNER JOIN " +
            "PB_VasMatrixRoles AS R ON V.Id = R.Id INNER JOIN " +
            "PB_VasMatrixTmsNames AS T ON V.Id = T.Id INNER JOIN " +
            "PB_VasMatrixWorkProcesses AS W ON V.Id = W.Id LEFT OUTER JOIN " +
            "PB_VasMatrixMessages AS M ON V.Id = M.Id ";


        public List<ProcessSupportData> RetrieveProcessSupportData(VasQueryParameters vasQueryParameters)
        {
            using (ProfilingMeasurement.MeasureFunction("FOTProcessSupportDataProvider.RetrieveProcessSupportData"))
            {
                List<ProcessSupportData> processSupportData = null;

                try
                {
                    var query = BuildVasQuery(vasQueryParameters);

                    var paramArray = new IDataParameter[query.Parameters.Count];
                    query.Parameters.CopyTo(paramArray, 0);

                    using (var dataReader = _sqlCeHelper.ExecuteDataReader(query.Text, paramArray))
                    {
                        if (dataReader != null)
                        {
                            processSupportData = MapProcessSupportData(dataReader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "ProcessSupportData.GetProcessSupportData");
                }

                return processSupportData;
            }
        }

        private Query BuildVasQuery(VasQueryParameters queryParams)
        {
            Logger.LogEvent(Severity.Debug, "BuildVasQuery;" + queryParams.WorkProcess + "," + queryParams.TmsName + " " + queryParams.RoleName);

            var query = new Query(_storageBase) { Text = SelectQuery };

            AddMandatoryParams(query, queryParams.WorkProcess, queryParams.TmsName, queryParams.RoleName, queryParams.IsCollectionProcess);
            AddOptionalParams(query, queryParams.ProductCode, queryParams.VasCodesString, queryParams.AttributeEqualTo);
            AddSortInstructions(query);

            return query;
        }

        private static List<ProcessSupportData> MapProcessSupportData(IDataReader dataReader)
        {
            var result = new List<ProcessSupportData>();

            while (dataReader != null && dataReader.Read())
            {
                var behaviourCode = dataReader.GetBehaviourCodeValue("BehaviourCode");

                if (behaviourCode != BehaviourCode.Ignore)
                {
                    var processSupportData = new ProcessSupportData(
                        dataReader.GetIntegerValue("Id"), 
                        behaviourCode,
                        dataReader.GetBooleanValue("ShowOncePerProcess"),
                        dataReader.GetBooleanValue("ShowOncePerConsignment"),
                        dataReader.GetOptionalStringValue("SubProcessName"),
                        dataReader.GetOptionalStringValue("Message"), 
                        dataReader.GetStringValue("Parameter"),
                        dataReader.GetIntegerValue("Priority"),
                        dataReader.GetOptionalStringValue("Query"),
                        dataReader.GetOptionalStringValue("IdExclusions"),
                        dataReader.GetOptionalStringValue("ItemCount"));
                    
                    result.Add(processSupportData);
                }
            }

            return result;
        }

        

        private void AddMandatoryParams(Query query, string workProcess, string tmsName, string roleName, bool isCollectionProcess)
        {
            if (workProcess == WorkProcess.None.ToString())
                throw new Exception("Missing WorkProcess name");
            if (string.IsNullOrEmpty(tmsName))
                throw new Exception("Missing tms name");
            if (string.IsNullOrEmpty(roleName))
                throw new Exception("Missing role name");
            
            const string selectString = "WHERE (WorkProcess = @WorkProcess OR WorkProcess = 'Any') " +
                                        "AND (TmsName = @TmsName OR TmsName = 'Any') " +
                                        "AND (RoleName = @RoleName OR RoleName = 'Any')" +
                                        "AND (M.LanguageCode IS NULL OR M.LanguageCode = @LanguageCode) " +
                                        "AND (IsCollectionProcess = @IsCollectionProcess) ";

            query.AddParameter("@WorkProcess", workProcess);
            query.AddParameter("@TmsName", tmsName);
            query.AddParameter("@RoleName", roleName);
            query.AddParameter("@LanguageCode", ModelUser.UserProfile.LanguageCode);
            query.AddParameter("@IsCollectionProcess", isCollectionProcess ? "TRUE" : "FALSE");

            query.Text += selectString;
        }

        private void AddOptionalParams(Query query, string productCode, string vasCode, string attributeEqualTo)
        {
            if (string.IsNullOrEmpty(productCode) == false)
            {

                query.Text += "AND (ProductCode = @ProductCode OR ProductCode = 'Any') ";
                query.AddParameter("@ProductCode", productCode);
            }
            else
            {
                query.Text += "AND (ProductCode = 'Any') ";
            }

            if (string.IsNullOrEmpty(vasCode) == false)
            {
                // Can not use parameter for comma-separated values, so use string.Format()
                // We can get comma separated values here, as there can be many vas-codes on one consignment-entity (scanning)
                query.Text += string.Format("AND (VasCode in ({0}) OR VasCode = 'Any') ", vasCode);
            }
            else
            {
                query.Text += "AND (VasCode = 'Any') ";
            }

            if (string.IsNullOrEmpty(attributeEqualTo) == false)
            {
                query.Text += "AND AttributeEqualTo = @EqualTo ";
                query.AddParameter("@EqualTo", attributeEqualTo);
            }
            else
            {
                query.Text += "AND AttributeEqualTo = '' ";
            }
        }

        private void AddSortInstructions(Query query)
        {
            query.Text += "ORDER BY V.Priority, V.BehaviourCode";
        }
    }

    internal class Query
    {
        private readonly IStorageBase _storageBase;

        internal Query(IStorageBase storageBase)
        {
            _storageBase = storageBase;
            Parameters = new List<IDataParameter>();
        }

        internal string Text;
        internal readonly IList<IDataParameter> Parameters;

        internal void AddParameter(string name, object value)
        {
            var param = _storageBase.CreateParameter(name, value);
            param.ParameterName = name;
            param.Value = value;
            Parameters.Add(param);
        }
    }
}
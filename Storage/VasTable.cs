﻿using System;
using System.Collections.Generic;
using System.Data;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    internal class VasTable : List<VasTableEntry>
    {
        internal void MapTableData(IDataReader dataReader)
        {
            try
            {
                Clear();
                while (dataReader != null && dataReader.Read())
                {

                    var processSupportData = new VasTableEntry(dataReader);
                    Add(processSupportData);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "VasTable.MapTableData");
            }
        }
    }


    internal class VasTableEntry
    {
        internal VasTableEntry(IDataReader dataReader)
        {
            try
            {
                Id = dataReader.GetIntegerValue("Id");
                IsCollectionProcess = dataReader.GetBooleanValue("IsCollectionProcess");
                VasCode = dataReader.GetStringValue("VasCode");
                ProductCode = dataReader.GetStringValue("ProductCode");
                WorkProcess = dataReader.GetStringValue("WorkProcess");
                AttributeEqualTo = dataReader.GetStringValue("AttributeEqualTo");
                BehaviourCode = dataReader.GetBehaviourCodeValue("BehaviourCode");
                ShowOncePerProcess = dataReader.GetBooleanValue("ShowOncePerProcess");
                ShowOncePerConsignment = dataReader.GetBooleanValue("ShowOncePerConsignment");
                SubProcessName = dataReader.GetStringValue("SubProcessName");
                Message = dataReader.GetStringValue("Message");
                Parameter = dataReader.GetStringValue("Parameter");
                Priority = dataReader.GetIntegerValue("Priority");
                Query = dataReader.GetStringValue("Query");
                IdExclusions = dataReader.GetStringValue("IdExclusions");
                ItemCount = dataReader.GetStringValue("ItemCount");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "VasTableEntry");
            }
        }

        public static implicit operator ProcessSupportData(VasTableEntry vte)
        {
            var result = new ProcessSupportData(vte.Id, vte.BehaviourCode, vte.ShowOncePerProcess, vte.ShowOncePerConsignment, vte.SubProcessName,
                vte.Message, vte.Parameter, vte.Priority, vte.Query, vte.IdExclusions, vte.ItemCount);

            return result;
        }


        internal int Id { get; private set; }
        internal bool IsCollectionProcess { get; private set; }
        internal string VasCode { get; private set; }
        internal string ProductCode { get; private set; }
        internal string WorkProcess { get; private set; }
        internal string AttributeEqualTo { get; private set; }
        internal BehaviourCode BehaviourCode { get; private set; }
        internal bool ShowOncePerProcess { get; private set; }
        internal bool ShowOncePerConsignment { get; private set; }
        internal string SubProcessName { get; private set; }
        internal string Message { get; private set; }
        internal string Parameter { get; private set; }
        internal int Priority { get; private set; }
        internal string Query { get; private set; }
        internal string IdExclusions { get; private set; }
        internal string ItemCount { get; private set; }
    }
}
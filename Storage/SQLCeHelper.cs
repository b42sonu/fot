﻿using System;
using System.Data;
using System.Data.SqlServerCe;
using System.Globalization;
using System.Linq;
using Com.Bring.PMP.PreComFW.Shared.Flows.ArriveAtStop;
using Com.Bring.PMP.PreComFW.Shared.Flows.DepartureFromStop;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PMC.PreComX.Mock;
using System.Data.SqlTypes;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    public class SqlCeHelper
    {
        public IApplication PrecomApplicationInstance = null;

        public SqlCeHelper()
#pragma warning disable 618
            : this(ApplicationWrapper.Wrap(PreCom.Application.Instance))
#pragma warning restore 618
        { }

        public SqlCeHelper(IApplication precom)
        {
            PrecomApplicationInstance = precom;
        }

        /// <summary>
        /// This fuction new table in database and returns bool value, spcifying that query executed successfully or not.
        /// </summary>
        /// <param name="query">Specifies query to execute.</param>
        /// <param name="tableName">Specifies the name of table, on which to excute query. This parameter is
        /// only for use while making entry in log.</param>
        /// <returns>True if trable was created</returns>
        public bool CreateTable(string query, TableName tableName)
        {
            var returnValue = false;

            try
            {
                // Check for parcel table
                if (TableExists(tableName) == false)
                {
                    using (var command = PrecomApplicationInstance.Storage.CreateCommand())
                    {
                        command.CommandText = query;

                        PrecomApplicationInstance.Storage.ExecuteNonCommand(command);

                        // Table creation check
                        if (PrecomApplicationInstance.Storage.ObjectExists(tableName.ToString(), PreCom.Core.ObjectType.Table))
                        {
                            returnValue = true;
                        }

                        Logger.LogEvent(Severity.Info, tableName + " table created successfully");
                    }
                }
            }

            catch (Exception ex)
            {
                returnValue = false;
                Logger.LogException(ex, "SqlCeHelper.CreateTable " + tableName);
            }

            return returnValue;
        }

        public bool TableExists(string tableName)
        {
            return PrecomApplicationInstance.Storage.ObjectExists(tableName, PreCom.Core.ObjectType.Table);
        }

        public bool TableExists(TableName tableName)
        {
            return TableExists(tableName.ToString());
        }

        /// <summary>
        /// This function executes a query which returns one or more records and returns object of IDataReader.
        /// </summary>
        /// <param name="query">Specifies query to execute.</param>
        /// <param name="queryParams">Specifies the array of parameters as required by query.</param>
        /// <returns></returns>
        public IDataReader ExecuteDataReader(string query, IDataParameter[] queryParams)
        {
            return ExecuteDataReader(query, queryParams, false);
        }

        public IDataReader ExecuteDataReader(string query, IDataParameter[] queryParams, bool forceRead)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Db))
            {
                IDataReader dataReader = null;

                try
                {
                    if (PrecomApplicationInstance.IsInitialized || forceRead)
                    {
                        using (var command = PrecomApplicationInstance.Storage.CreateCommand(query))
                        {
                            if (queryParams != null)
                            {
                                foreach (var param in queryParams)
                                {
                                    command.Parameters.Add(param);
                                }
                            }

                            dataReader = command.ExecuteReader();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "ExecuteDataReader " + query);
                }

                return dataReader;
            }
        }

        /// <summary>
        /// This methos executes the query which inserts or updates record table, and retrns bool value specifying status.
        /// </summary>
        /// <param name="query">Specifies query to execute.</param>
        /// <param name="queryParams">Specifies the array of parameters as required by query.</param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string query, IDataParameter[] queryParams)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Db))
            {
                var returnValue = false;

                try
                {
                    if (PrecomApplicationInstance.Storage.IsInitialized)
                    {
                        using (var command = PrecomApplicationInstance.Storage.CreateCommand(query))
                        {
                            if (queryParams != null)
                            {
                                foreach (var param in queryParams)
                                {
                                    command.Parameters.Add(param);
                                }
                            }

                            if (command.ExecuteNonQuery() != 0)
                            {
                                returnValue = true;
                            }
                            else
                            {
                                Logger.LogEvent(Severity.Error, "Failed to execute query: " + query);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "SqlCeHelper.ExecuteNonQuery");
                }

                return returnValue;
            }
        }

        /// <summary>
        /// This method executes a query, which returns a single value.
        /// </summary>
        /// <param name="query">Specifies query to execute.</param>
        /// <param name="queryParams">Specifies the array of parameters as required by query.</param>
        /// <returns></returns>
        public string ExecuteScalar(string query, IDataParameter[] queryParams)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Db))
            {
                var value = string.Empty;

                try
                {
                    if (PrecomApplicationInstance.Storage.IsInitialized)
                    {
                        using (var command = PrecomApplicationInstance.Storage.CreateCommand(query))
                        {
                            if (queryParams != null)
                            {
                                foreach (var param in queryParams)
                                {
                                    command.Parameters.Add(param);
                                }
                            }

                            // For some reasons native ExecuteScalar fails in certain conditions...
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.Read())
                                {
                                    value = reader[0].ToString();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "SqlCeHelper.ExecuteScalar " + query);
                }

                return value;
            }
        }

        /// <summary>
        /// This method deletes the records from table in local database
        /// </summary>
        /// <param name="query">Specifies query to execute.</param>
        /// <param name="queryParams">Specifies the array of parameters as required by query.</param>
        /// <param name="tableNameName">Specifies the name of table, on which to excute query.</param>
        /// <returns></returns>
        public bool ExecuteDelete(string query, IDataParameter[] queryParams, TableName tableNameName)
        {
            using (ProfilingMeasurement.Measure(TuningMeasurementType.Db))
            {
                var isDeleted = false;

                try
                {
                    if (PrecomApplicationInstance.Storage.IsInitialized)
                    {
                        using (var command = PrecomApplicationInstance.Storage.CreateCommand(query))
                        {
                            if (queryParams != null)
                            {
                                foreach (var param in queryParams)
                                {
                                    command.Parameters.Add(param);
                                }
                            }

                            command.ExecuteNonQuery();
                            isDeleted = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogException(ex, "SqlCeHelper.ExecuteDelete " + tableNameName);
                    isDeleted = false;
                }

                return isDeleted;
            }
        }


        public bool CheckConstraintExists(string constraintName)
        {
            const string query = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = @ConstraintName";
            var parameters = new[] { PrecomApplicationInstance.Storage.CreateParameter("@ConstraintName", constraintName) };

            return ExecuteScalar(query, parameters) == "1";
        }


        internal bool AddConstraint(string constraintName, TableName tableName, TableName foreignTableName)
        {
            // Can not use tablename as a param for some reason
            var queryString = string.Format(
                "ALTER TABLE [{0}] ADD CONSTRAINT [{1}] FOREIGN KEY([Id]) REFERENCES [{2}] ([Id])",
                tableName, constraintName, foreignTableName);


            return ExecuteNonQuery(queryString, null);
        }

        public int GetColumnCapacity(TableName tableName, string columnName)
        {
            var query = string.Format("SELECT character_maximum_length FROM information_schema.columns " +
                                         "WHERE (table_name = '{0}') AND (column_name = '{1}')", tableName, columnName);

            return Convert.ToInt32(ExecuteScalar(query, null));
        }


        internal void SetColumnCapacity(TableName tableName, string columnName, int capacity)
        {
            var query = string.Format("ALTER TABLE {0} ALTER COLUMN {1} NVARCHAR({2}) NULL", tableName, columnName,
                                         capacity);
            ExecuteNonQuery(query, null);
        }


        public void InsertMasterData(TableName tableName, string incomingData)
        {
            if (string.IsNullOrEmpty(incomingData))
            {
                Logger.LogEvent(Severity.ExpectedError, tableName + " is empty");
                return;
            }
            var lines = incomingData.Split(Convert.ToChar((byte)24)); // FOT row separator

            using (var cmd1 = PrecomApplicationInstance.Storage.CreateCommand("SELECT TOP (1) * FROM " + tableName))
            {
                using (var schemaInfo = PrecomApplicationInstance.Storage.ExecuteQueryCommand(cmd1))
                {
                    using (var cmd = new SqlCeCommand(tableName.ToString()))
                    {
                        using (var conn = new SqlCeConnection(PrecomApplicationInstance.Storage.CreateCommand().Connection.ConnectionString + "Encrypt Database=True;Password=PocketMobileCommunicationsAB;"))
                        {
                            cmd.Connection = conn;
                            cmd.CommandType = CommandType.TableDirect;
                            cmd.Connection.Open();

                            using (var res = cmd.ExecuteResultSet(ResultSetOptions.Updatable))
                            {
                                foreach (var data in lines.Select(row => row.Split(Convert.ToChar((byte)25)))) // FOT field separator
                                {
                                    InsertMasterDataRow(data, res, schemaInfo);
                                }
                            }
                        }
                    }
                }
            }

            InsertSyncStatus(tableName, lines.Count());
        }

        private void InsertSyncStatus(TableName tableName, int numberOfRows)
        {
            var found = false;
            var alias = tableName.ToString();
            DateTime? updatedSince = null;

            try
            {
                // Get current maximum value
#pragma warning disable 618
                using (var cmd = PreCom.Application.Instance.Storage.CreateCommand("SELECT MAX(UpdatedServerUTC) FROM " + tableName))
#pragma warning restore 618
                {
                    using (var r = cmd.ExecuteReader())
                    {
                        if (r.Read())
                        {
                            updatedSince = r.GetDateTime(0);
                        }
                    }
                }

                // Check if the row exists
#pragma warning disable 618
                using (var cmd = PreCom.Application.Instance.Storage.CreateCommand("SELECT NumberOfRows FROM SyncNM_Status WHERE Object = @TableName AND Alias = @Alias"))
                {
                    cmd.Parameters.Add(PreCom.Application.Instance.Storage.CreateParameter("@TableName", tableName));
                    cmd.Parameters.Add(PreCom.Application.Instance.Storage.CreateParameter("@Alias", alias));

                    using (var r = PreCom.Application.Instance.Storage.ExecuteQueryCommand(cmd))
                    {
                        if (r.Read())
                        {
                            found = true;
                        }
                    }
#pragma warning restore 618
                }

#pragma warning disable 618
                using (var cmd = PreCom.Application.Instance.Storage.CreateCommand())
#pragma warning restore 618
                {
                    if (found)
                    {
                        // Update
                        cmd.CommandText = "UPDATE SyncNM_Status SET NumberOfRows = @NumberOfRows, LastUpdatedServerUTC=@LastUpdatedServerUTC WHERE Object = @TableName AND Alias = @Alias";
                    }
                    else
                    {
                        // Insert
                        cmd.CommandText = "INSERT INTO SyncNM_Status (Object, Alias, NumberOfRows, LastUpdatedServerUTC) VALUES (@TableName, @Alias, @NumberOfRows, @LastUpdatedServerUTC)";
                    }

#pragma warning disable 618
                    cmd.Parameters.Add(PreCom.Application.Instance.Storage.CreateParameter("@NumberOfRows", numberOfRows));
                    cmd.Parameters.Add(PreCom.Application.Instance.Storage.CreateParameter("@LastUpdatedServerUTC", updatedSince));
                    cmd.Parameters.Add(PreCom.Application.Instance.Storage.CreateParameter("@TableName", tableName.ToString()));
                    cmd.Parameters.Add(PreCom.Application.Instance.Storage.CreateParameter("@Alias", alias));

                    PreCom.Application.Instance.Storage.ExecuteNonCommand(cmd);
#pragma warning restore 618
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SQLCeHelper.InsertSyncStatus");
            }
        }

        private void InsertMasterDataRow(string[] data, SqlCeResultSet res, IDataReader schemaInfo)
        {
            var rec = res.CreateRecord();
            var sqlCeDataReader = schemaInfo as SqlCeDataReader;
            if (sqlCeDataReader != null)
            {
                // In case of differences in the DB-schema use the shortest list.
                for (var i = 0; i < Math.Min(sqlCeDataReader.FieldCount,data.Length); i++)
                {
                    var fieldType = sqlCeDataReader.GetFieldType(i);

                    if (fieldType != null)
                    {
                        if (data[i].Length == 1 && Convert.ToChar(data[i]) == Convert.ToChar((byte)26)) // FOT representation of NULL
                        {
                            continue;
                        }

                        var t = fieldType.Name;

                        switch (t)
                        {
                            case "SqlString":
                            case "String":
                                rec.SetString(i, data[i]);
                                break;
                            case "SqlInt64":
                            case "Int64":
                                rec.SetInt64(i, long.Parse(data[i]));
                                break;
                            case "SqlInt32":
                            case "Int32":
                                rec.SetInt32(i, int.Parse(data[i]));
                                break;
                            case "SqlInt16":
                            case "Int16":
                                rec.SetInt16(i, short.Parse(data[i]));
                                break;
                            case "SqlDateTime":
                            case "DateTime":
                                rec.SetDateTime(i, new DateTime(long.Parse(data[i])));
                                break;
                            case "SqlGuid":
                            case "Guid":
                                rec.SetGuid(i, new Guid(data[i]));
                                break;
                            case "SqlDouble":
                            case "Double":
                                rec.SetDouble(i, double.Parse(data[i].Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator).Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)));
                                break;
                            case "SqlBoolean":
                            case "Boolean":
                                rec.SetBoolean(i, bool.Parse(data[i]));
                                break;
                            //case "SqlBinary":
                            //    return PreCom.Application.Instance.Storage.CreateParameter("@" + DataField.name, System.Text.Encoding.Default.GetBytes(Data));
                            case "SqlDecimal":
                            case "Decimal":
                                rec.SetDecimal(i, decimal.Parse(data[i].Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator).Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)));
                                break;
                            case "SqlMoney":
                            case "Money":
                                rec.SetSqlMoney(i, new SqlMoney(decimal.Parse(data[i].Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator).Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))));
                                break;
                            case "SqlSingle":
                            case "Single":
                                rec.SetSqlSingle(i, new SqlSingle(double.Parse(data[i].Replace(",", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator).Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))));
                                break;
                            //case "SqlByte":
                            //    return PreCom.Application.Instance.Storage.CreateParameter("@" + DataField.name, System.Text.Encoding.Default.GetBytes(Data));
                            default:
                                rec.SetString(i, data[i]);
                                break;
                        }
                    }
                    else
                    {
                        rec.SetString(i, data[i]);
                    }
                }
            }

            res.Insert(rec);
        }

        internal int NumberOfRowsInTable(TableName tableName)
        {
            var query = string.Format("SELECT COUNT(1) FROM {0}", tableName);

            return Convert.ToInt32(ExecuteScalar(query, null));
        }
    }
}

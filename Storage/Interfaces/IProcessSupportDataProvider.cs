﻿using System.Collections.Generic;

namespace Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces
{
    public interface IProcessSupportDataProvider
    {
        List<ProcessSupportData> RetrieveProcessSupportData(VasQueryParameters vasQueryParameters);
    }
}
﻿namespace Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces
{
    interface ITmiSettingsDataProvider
    {
        T GetTmiValue<T>(TmiSettings settingCode);
    }
}

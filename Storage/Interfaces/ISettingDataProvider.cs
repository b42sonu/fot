using System;
using System.Collections.Generic;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Flows.VasMatrix;
using CommunicationEntity.Messaging;

namespace Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces
{
    public class ProductGroupTransition
    {
        public ProductGroupTransition(string toCode, string description)
        {
            ToCode = toCode;
            Description = description;
        }

        public string DisplayValue
        {
            get { return string.Format("{0}: {1}", ToCode, Description); }
        }

        public string ToCode { get; private set; }
        public string Description { get; private set; }
    }

    public interface ISettingDataProvider
    {
        /// <summary>
        /// Retrieves dummy TMI settingElement
        /// </summary>
        /// <param name="settingName"></param>
        T GetTmiValue<T>(TmiSettings settingName);
        T GetTmiValue<T>(TmiSettings settingName, string roleName, string tmsAffiliation);
        
        List<ActionType> GetActionTypes(string langCode);
        List<EventType> GetEventTypes(string langCode);
        List<ReasonType> GetReasonTypes(string langCode);


        /// <summary>
        /// Creates different table in local storage.
        /// </summary>
        void CreateTables();

        /// <summary>
        /// This method creates TMI settings table in local database.
        /// </summary>
        void CreateTmiSettingsTable();

        /// <summary>
        /// This method creates settings table in local database.
        /// </summary>
        void CreateSettingsTable();

        /// <summary>
        /// This method creates Location table in local database.
        /// </summary>
        void CreateLocationTable();

        /// <summary>
        /// This method creates Profile Table in local database.
        /// </summary>
        void CreateUserProfileTable();

        /// <summary>
        /// This method creates PB_EventTypes table in local database.
        /// </summary>
        void CreateEventTable();

        /// <summary>
        /// This method creates PB_ReasonTypes table in local database.
        /// </summary>
        void CreateReasonTable();

        /// <summary>
        /// This method creates PB_ReasonTypes table in local database.
        /// </summary>
        void CreateActionTable();

        /// <summary>
        /// This method creates PB_ReasonTypes table in local database.
        /// </summary>
        void CreateEventReasonActionTable();

        /// <summary>
        /// Retrieves local user profile for logged in user
        /// </summary>
        /// <returns>Specifies object for UserProfile entity.</returns>
        UserProfile GetUserProfile();

        /// <summary>
        /// Save userprofile locally
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        bool SaveUserProfile(UserProfile userProfile);

        /// <summary>
        /// Get number of days to chage password.
        /// </summary>
        /// <returns>Number of days to chage password.</returns>
        int GetPasswordExpiryInterval();

        /// <summary>
        /// Get number of days to display password change warning.
        /// </summary>
        /// <returns>number of days to display warning.</returns>
        int GetPasswordChangeWarningDateCount();

        /// <summary>
        /// Check whether password is changed
        /// </summary>
        /// <param name="settings">List of <see cref="Entity.SettingElement"/>.</param>
        /// <returns>True, if password is changed, otherwise false.</returns>
        bool IsPasswordChanged(List<SettingElement> settings);

        /// <summary>
        /// Get location name.
        /// </summary>
        /// <param name="locationId">The location id.</param>
        /// <returns>Location name.</returns>
        string GetLocationName(string locationId);

        /// <summary>
        /// Get post code for the location.
        /// </summary>
        /// <param name="locationId">The location id.</param>
        /// <returns>Post code.</returns>
        string GetPostalCode(string locationId);

        /// <summary>
        /// Get country code for the location.
        /// </summary>
        /// <param name="locationId">The location id.</param>
        /// <returns>Post code.</returns>
        string GetCountryCode(string locationId);

        /// <summary>
        /// Check the possibility to change password.
        /// </summary>
        /// <returns>True, if possible to change password. otherwise false.</returns>
        bool IsPasswordChangeable();

        /// <summary>
        /// Check whether password is expired.
        /// </summary>
        /// <param name="userSettings">List of user settings.</param>
        /// <returns>True, if password is expired. otherwise false.</returns>
        bool IsPasswordExpired(List<SettingElement> userSettings);

        /// <summary>
        /// Get default location id.
        /// </summary>
        /// <returns>Location id.</returns>
        string GetDefaultLocationId();

        /// <summary>
        /// Get regular expression of the password policy.
        /// </summary>
        /// <returns>The regular expression.</returns>
        string GetPasswordPolicyExpression();

        string GetSetting(string settingName);
        string GetCommonSetting(string settingName);
        string GetCommonSetting(string settingName, string defaultValue);

        void SaveSettings(SettingElements setting);
        bool SaveSetting(string name, string value);

        /// <summary>
        /// Save password changed date.
        /// </summary>
        /// <param name="date">Password changed date</param>
        void SavePasswordChangedDate(DateTime date);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetUserId();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="credentialKey"></param>
        /// <returns></returns>
        string GetCredentialsValue(string credentialKey);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetCredentialsBase64();

        /// <summary>
        /// This function returns all locations from local database.
        /// </summary>
        /// <returns>List of locations to return.</returns>
        List<LocationElement> GetLocations();

        List<DamageCode> GetDamageCodes(string langcode);
        List<EventReasonActionType> GetEventReasonActionTypes();

        List<CauseMeasure> GetDistinctCombinationForReasonActionTypes(string role, string langCode);
        int GetDeviceSetting(DeviceSettingNames name, int defaultValue);
        string GetDeviceSetting(DeviceSettingNames name, string defaultValue);

        List<ProductGroupTransition> GetProductGroupTransitions(string productGroupCode, string languageCode);

        string IsValidOrgUnit(string orgUnit);

        List<ProcessSupportData> GetProcessSupportData(VasQueryParameters vasQueryParameters);

        /// <summary>
        /// Gets the help text to be shown 
        /// </summary>
        string GetHelpText(string moduleName, string processName, string viewName, string language);


        bool SaveReceivedMessage(MessageToDriver message);
        void InsertMasterData(TableName tableName, string p);
        int NumberOfRowsInTable(TableName tableName);

        DateTime? GetLatestRowForTable(TableName table);

        List<MessageToDriver> GetMessages(string userId);

        bool UpdateMessage(string messageId, string status);

        bool SaveSentMessage(MessageFromDriver message, string status);

        int GetUnreadMessageCount();

        bool DeleteAllMessages();

        bool MessageExists(string messageId);
    }

    public class VasQueryParameters
    {
        // For single scanning
        public VasQueryParameters(string vasCodes, FlowDataVasMatrix flowDataVasMatrix)
        {
            IsCollectionProcess = false;

            VasCodesString = vasCodes;
            VasCodes = vasCodes.Split(new[] { ',' });
            ProductCode = flowDataVasMatrix.ConsignmentEntity.ProductCode;
            WorkProcess = flowDataVasMatrix.CallingFlow.WorkProcess.ToString();
            RoleName = flowDataVasMatrix.RoleName;
            TmsName = flowDataVasMatrix.TmsName;
            AttributeEqualTo = flowDataVasMatrix.AttributeEqualTo ?? string.Empty;

        }

        // For scanning collection
        public VasQueryParameters(FlowDataVasMatrix flowDataVasMatrix)
        {
            IsCollectionProcess = true;
            WorkProcess = flowDataVasMatrix.CallingFlow.WorkProcess.ToString();
            RoleName = flowDataVasMatrix.RoleName;
            TmsName = flowDataVasMatrix.TmsName;
            AttributeEqualTo = flowDataVasMatrix.AttributeEqualTo ?? string.Empty;
        }

        public void UpdateEntityInfo(string vasCodes, string productCode)
        {
            VasCodesString = vasCodes;
            VasCodes = vasCodes.Split(new[] { ',' });
            ProductCode = productCode;
        }

        public string VasCodesString { get; private set; }
        public string[] VasCodes { get; private set; }
        public string ProductCode { get; private set; }
        public string WorkProcess { get; private set; }
        public string RoleName { get; private set; }
        public string TmsName { get; private set; }
        public string AttributeEqualTo { get; private set; }
        public bool IsCollectionProcess { get; private set; }

    }
}
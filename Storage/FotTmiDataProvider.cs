﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using PMC.PreComX.Mock;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    internal class FotTmiDataProvider : ITmiSettingsDataProvider
    {
        private readonly IStorageBase _storageBase;
        private readonly SqlCeHelper _sqlCeHelper;
        private readonly TmiTable _tmiTable;
        private string _currentRoleName;
        private string _currentTmsName;

        private const string RetrieveQuery =
            "SELECT UserRole, SettingCode, ProcessCode, TMSId, Value FROM PB_TMISettings " +
            " WHERE (UserRole = @RoleName OR UserRole = 'Default') AND (TMSId = @TmsName OR TMSId = 'Default')";


        // For Unit testing
        public FotTmiDataProvider()
        {
            _sqlCeHelper = new SqlCeHelper();
            _tmiTable = new TmiTable();
            
            ReflectSettings();
        }

        public FotTmiDataProvider(IStorageBase storageBase, SqlCeHelper sqlCeHelper)
        {
            _storageBase = storageBase;
            _sqlCeHelper = sqlCeHelper;
            _tmiTable = new TmiTable();
            
            ModelUser.SettingsChanged += OnSettingsChanged;
        }

        public void OnSettingsChanged(SettingChangeType settingChangeType)
        {
            switch (settingChangeType)
            {
                case SettingChangeType.SettingsSynced:
                    if (ModelUser.UserProfile != null)
                        ReflectSettings();
                    break;

                case SettingChangeType.ProfileUpdated:
                    if (_currentRoleName != ModelUser.UserProfile.RoleName || _currentTmsName != ModelUser.UserProfile.TmsAffiliation)
                        ReflectSettings();
                    break;
            }
        }

        public void ReflectSettings()
        {
            try
            {
                var paramArray = new[]
                        {
                            _storageBase.CreateParameter("@TmsName", ModelUser.UserProfile.TmsAffiliation),
                            _storageBase.CreateParameter("@RoleName", ModelUser.UserProfile.RoleName)
                        };

                using (var dataReader = _sqlCeHelper.ExecuteDataReader(RetrieveQuery, paramArray))
                {
                    if (dataReader != null)
                    {
                        _tmiTable.MapTableData(dataReader);
                    }
                }
                _currentRoleName = ModelUser.UserProfile.RoleName;
                _currentTmsName = ModelUser.UserProfile.TmsAffiliation;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "FotTmiDataProvider.ReflectUpdatedSettings");
            }
        }

        public T GetTmiValue<T>(TmiSettings settingCode)
        {
            if (ModelUser.UserProfile == null)
            {
                Logger.LogEvent(Severity.Error, "TMI-value read before user profile is set up");
                return default(T);
            }
            if (ModelMain.IsUnitTesting)
                return default(T);

            var tmiValueString = GetTmiValueString(settingCode);
            return ConvertTmiResult<T>(tmiValueString);
        }

        public T ConvertTmiResult<T>(string tmiValue)
        {
            if (string.IsNullOrEmpty(tmiValue))
                return default(T);

            switch (typeof(T).Name)
            {
                case "Int32":
                    return (T)(object)int.Parse(tmiValue);

                case "String":
                    return (T)(object)tmiValue;

                case "Boolean":
                    switch (tmiValue.ToUpper())
                    {
                        case "TRUE":
                        case "YES":
                            return (T)(object)true;

                        case "NO":
                        case "FALSE":
                            return (T)(object)false;

                        default:
                            throw new ArgumentException(string.Format("Boolean value {0} is not supported in GetTmiValue", tmiValue));
                    }

                default:
                    throw new ArgumentException(string.Format("Datatype {0} is not supported in GetTmiValue",
                                                              typeof(T).Name));
            }
        }

        private string GetTmiValueString(TmiSettings settingType)
        {
            const string Default = "Default";
            string settingName = settingType.ToString();

            string process = BaseModule.CurrentFlow != null
                  ? BaseModule.CurrentFlow.CurrentProcess.ToString() : Default;

            string tmsAffiliation = ModelUser.UserProfile.TmsAffiliation;

            // All Fields In Key Is Set
            string value = GetTmiValue(settingName, process, ModelUser.UserProfile.RoleName, tmsAffiliation);
            if (string.IsNullOrEmpty(value) == false)
            {
                return value;
            }

            // Default UserRole
            value = GetTmiValue(settingName, process, Default, tmsAffiliation);
            if (string.IsNullOrEmpty(value) == false)
            {
                return value;
            }

            // Default ProcessCode
            value = GetTmiValue(settingName, Default, ModelUser.UserProfile.RoleName, tmsAffiliation);
            if (string.IsNullOrEmpty(value) == false)
            {
                return value;
            }

            // Default UserRole and ProcessCode
            value = GetTmiValue(settingName, Default, Default, tmsAffiliation);
            if (string.IsNullOrEmpty(value) == false)
            {
                return value;
            }

            // DefaultUserRole And default ProcessCode And Default Tms
            value = GetTmiValue(settingName, Default, Default, Default);
            if (string.IsNullOrEmpty(value) == false)
            {
                return value;
            }

            Logger.LogEvent(Severity.Error, "Failed to retrieve TMI-setting '{0}'", settingName);

            return null;

        }

        private string GetTmiValue(string settingCode, string process, string userRole,
                                   string tmsAffiliation)
        {
            if (_tmiTable.Count == 0)
                ReflectSettings();

            foreach (TmiTableEntry tableEntry in _tmiTable)
            {
                if (tableEntry.SettingCode != settingCode)
                    continue;

                if (tableEntry.ProcessCode != process)
                    continue;

                if (tableEntry.UserRole != userRole)
                    continue;

                if (tableEntry.TmsAffiliation != tmsAffiliation)
                    continue;

                return tableEntry.Value;
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity;
using Com.Bring.PMP.PreComFW.Shared.CommunicationEntity.Login;
using System.Data;
using System.Text;
using Com.Bring.PMP.PreComFW.Shared.Constants;
using Com.Bring.PMP.PreComFW.Shared.Entity.Com.Bring.PMP.PreComFW.Shared.Entity;
using Com.Bring.PMP.PreComFW.Shared.Entity.EntityMap;
using Com.Bring.PMP.PreComFW.Shared.Measurement;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Storage.Interfaces;
using Com.Bring.PMP.PreComFW.Shared.Utility;
using CommunicationEntity.Messaging;
using PMC.PreComX.Mock;
using Com.Bring.PMP.PreComFW.Shared.Entity;
using PreCom.Core.Communication;
using SettingElement = Com.Bring.PMP.PreComFW.Shared.Entity.SettingElement;

namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    public enum Process //OBSERVE: MAX length of process name is 20 characters
    {
        Unknown,
        None,
        Default,
        DeliveredToHubByCust,       // 1
        Departure,                  // TRD
        Arrival,                    // TRA
        AttemptedPickup,            // 7
        RegisterDamage,             // 5
        AttemptedDelivery,          // H
        Unloading,                  // 6A
        UnplannedPickup,            // A
        DeliveryToCustomer,         // I
        Loading,                    // 3A
        BuildLoadCarrier,           // ???
        DeliveryToSubContrct,       // 2A
        RemainingGoodsAtHub,        // VG
        Pickup,                     // A
        RegisterTemperature,        // A1
        CorrectWeightVolume,                // ???
        ControlScan,
        LoadLineHaul,
        LoadDistribTruck,
        LoadCombinationTrip,
        LoadByCustomer,
        GeneralDeviations,
        UnloadLineHaul,
        PrintOutMailLoadList,

        UnloadPickUpTruck,
        ChangeProductGroup,
        DeliveredOtherLoc,
        DelToCustomerAtPo,
        ArrivalRegistration,
        DeliveryToPostOffice,
        DeliveryByCustomer,
        ExecuteLinking,
        IntoHoldingAtHub,
        AgeValidation,
        PickingFromWarehouse,
        OutOfHoldingAtHub,
        ArrAtCustWareHouse,
        InTerminal,
        RegisterTimeConsumption,
        NotDelivered,
        UnloadEntireLineHaul,
        MessageToFromDriver,
        Inherit

    }

    public enum WorkProcess
    {
        None, // Not a work-process
        DeliveryToCustomer,
        DeliverToAnotherLocation,
        Unloading,
        ArrivalRegistration,
        DeliveryToAnotherDriverSubContractor,
        Deviation,
        HandedIn,
        LoadingOfDistributionTruck,
        LoadingOfLineHaulTruck,
        LoadingOfCombinationTruck,
        UnloadingOfPickupTruck,
        UnloadingOfLinehaulTruck,
        BuildingOfLoadCarriers,
        AttemptedDelivery,
        Correction,
        UnplannedPickup,
        InTerminal
    }

    //If client side TMI is changed, the server sides TMI must change
    public enum TmiSettings
    {
        IsConsignmentRegistrationAllowed,

        IsVerificationOfUnplannedConsignmentsRequired,
        IsMultipleConsignmentsAllowed,
        IsConsignmentItemCountVerificationRequired,

        IsSendingConsignmentEventRequired,

        IsUnplannedConsignmentsAllowed,//IsUnplannedConsignmentAllowed, // Previously named IsAllowedToEnterUnplannedConsignment

        IsOnlineConsignmentNumberRequired,
        IsOfflineConsignmentNumberRequired,
        IsValidateStopRequired,
        IsSendingStopEventArrivedRequired,
        IsOperationEventStartRequired,
        IsCustomerInfoRequired,
        IsDamageSignatureRequired,
        IsBookingNumberRequired,
        IsSingleEventVerificationRequired,
        IsOperationEventFinishedRequired,
        IsSendingDepartureEventRequired,
        IsStopSummaryRequired,
        IsGpsPositioningAllowed,
        IsPromptForSummaryRequired,
        IsCompletingOperationRequired,
        HeartbeatInterval,

        // TODO: Usage of this property is not finalized and are not in local table
        NumberOfPhotos,
        CameraResolutionSetting,

        //added for alystra operation list
        DisplayStopInfoPopup,
    }

    public enum DeviceSettingNames
    {
        NumberOfPhotos,
        CameraResolution,
        SignatureMinimalBlackPixels,
        SignatureNameMinimalChar,
        SignatureNameMaximumChar
    }


    /// <summary>
    /// Data provider class of the settingElement module.
    /// </summary>
    ///
    public partial class SettingDataProvider : ISettingDataProvider
    {
        private static ISettingDataProvider _instance;
        private static IStorageBase _storageBase;
        private static SqlCeHelper _sqlCeHelper;
        private static IApplication _precom;
        private readonly SqlCeTmiDataProvider _sqlCeTmiSettingsProvider;
        private readonly FotTmiDataProvider _fotTmiSettingsProvider;

        private ITmiSettingsDataProvider TmiSettingsProvider
        {
            get { return _fotTmiSettingsProvider; }
        }

        public SettingDataProvider()
            : this(ApplicationWrapper.Wrap(ModelModules.Application))
        {
        }

        public SettingDataProvider(IApplication precom)
        {
            _precom = precom;
            _storageBase = _precom.Storage;

            //Initialize sqlcehelper object
            _sqlCeHelper = new SqlCeHelper();

            _fotTmiSettingsProvider = new FotTmiDataProvider(_storageBase, _sqlCeHelper);
            _sqlCeTmiSettingsProvider = new SqlCeTmiDataProvider(_storageBase, _sqlCeHelper);

            _fotProcessSupportDataProvider = new FotProcessSupportDataProvider(_storageBase, _sqlCeHelper);
        }

        public void SetApplication(IApplication precom)
        {
            _precom = precom;
        }

        public static ISettingDataProvider Instance
        {
            get
            {
                //Initialize singleton class instance
                if (_precom == null)
                {
                    _instance = _instance ?? (_instance = new SettingDataProvider());
                }
                else
                {
                    _instance = _instance ?? (_instance = new SettingDataProvider(_precom));

                }

                return _instance;
            }
        }

        /// <summary>
        /// Method shall only be used for unittesting
        /// </summary>
        /// <param name="settingDataProvider">A mocked object implementing</param>
        public static void SetSettingDataProvider(ISettingDataProvider settingDataProvider)
        {
            _instance = settingDataProvider;
        }


        public T GetTmiValue<T>(TmiSettings settingName)
        {
            var fotResult = TmiSettingsProvider.GetTmiValue<T>(settingName);
            //var sqlCeResult = _sqlCeTmiSettingsProvider.GetTmiValue<T>(settingName);

            //if (fotResult.Equals(sqlCeResult) == false)
            //    Logger.LogEvent(Severity.Error, "Optimized tmiquery result for setting '{0}' is '{1}' and not equal to standard '{2}'. Please report this defect",
            //        settingName, fotResult, sqlCeResult);

            return fotResult;
        }

        public T GetTmiValue<T>(TmiSettings settingName, string roleName, string tmsAffilliation)
        {
            return _sqlCeTmiSettingsProvider.GetTmiValue<T>(settingName, roleName, tmsAffilliation);
        }


        /// <summary>
        /// convert OrderInfoValidationType setting value string value into enum
        /// </summary>
        public static OrderInfoValidationType OrderInfoValidationTypeFromString(string tmiValue)
        {
            var infoMandatoryType = OrderInfoValidationType.NoValidation;
            switch (tmiValue)
            {
                case "CustomerNo": //only customer
                    infoMandatoryType = OrderInfoValidationType.CustomerId;
                    break;

                case "BookingNo": //only booking
                    infoMandatoryType = OrderInfoValidationType.OrderNumber;
                    break;

                case "CustomerNoAndBookingNo": //both (customer and booking)
                    infoMandatoryType = OrderInfoValidationType.CustomerIdAndBookingNumber;
                    break;

                case "No":
                    infoMandatoryType = OrderInfoValidationType.NoValidation;
                    break;

                default:
                    Logger.LogEvent(Severity.Error, "Unhandled OrderInfoValidationType state encountered in TmiSettings.OrderInfoValidationTypeFromString");
                    break;

            }
            return infoMandatoryType;
        }

        /// <summary>
        /// Creates different table in local storage.
        /// </summary>
        public void CreateTables()
        {
            CreateSettingsTable();
            CreateLocationTable();
            CreateUserProfileTable();
            CreateTmiSettingsTable();
            CreateEventTable();
            CreateReasonTable();
            CreateActionTable();
            CreateEventReasonActionTable();
            CreateDamageCodeTable();
            CreateDeviceSettingsTable();
            CreateProductGroupTable();
            CreateProductGroupTransitionTable();
            CreateVasMatrixTables();
            CreateLimitedOpeningHoursTable();
            CreateOpeningHoursTable();
            CreateFixedClosingDatesTable();
            CreateRolesTable();
            CreateVasTypeTable();
            //Help Data
            CreateHelpTable();
            CreateMessageTables();//Messages Data
            //MockHelpData();
        }


        private void CreateMessageTables()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_Messages) == false)
            {
                var querystring = string.Format("CREATE TABLE [{0}] (" +
                                                "[UnitId] [nvarchar](50) NULL, " +
                                                "[CaptureEquipmentId] [nvarchar](50) NULL, " +
                                                "[UserLogonId] [nvarchar](50) NULL, " +
                                                "[MessageTime] [nvarchar](50) NULL, " +
                                                "[UserSessionId] [nvarchar](100) NULL, " +
                                                "[MessageId] [nvarchar](100) NOT NULL, " +
                                                "[CorrelationId] [nvarchar](100) NULL, " +
                                                "[MessageText] [nvarchar](4000) NULL, " +
                                                "[UserId] [nvarchar](50) NULL, " +
                                                "[Status] [nvarchar](2) NULL) "
                                                , TableName.PB_Messages);

                _sqlCeHelper.CreateTable(querystring, TableName.PB_Messages);
            }
            //    const string query = "Delete from PB_Messages";
            //  _sqlCeHelper.ExecuteDelete(query, null, TableName.PB_Messages);


        }


        #region HelpData



        //private void InsertHelpRow(string moduleName, string processName, string viewName, string language,
        //                           string description)
        //{
        //    const string query =
        //        "INSERT [PB_Help] ([ModuleName], [ProcessName], [ViewName], [LangCode], [Text])" +
        //        " VALUES (@ModuleName, @ProcessName, @ViewName, @LangCode, @Text)";

        //    var parameters = new IDataParameter[]
        //        {
        //            PreCom.Application.Instance.Storage.CreateParameter("@ModuleName", moduleName),
        //            PreCom.Application.Instance.Storage.CreateParameter("@ProcessName", processName),
        //            PreCom.Application.Instance.Storage.CreateParameter("@ViewName", viewName),
        //            PreCom.Application.Instance.Storage.CreateParameter("@LangCode", language),
        //            PreCom.Application.Instance.Storage.CreateParameter("@Text", description)
        //        };
        //    _sqlCeHelper.ExecuteNonQuery(query, parameters);
        //}


        private void CreateHelpTable()
        {
            if (_sqlCeHelper.TableExists(TableName.PB_Help) == false)
            {
                const string querystring = "CREATE TABLE [PB_Help] (" +
                                           "[ModuleName] [nvarchar](50) NOT NULL, " +
                                           "[ProcessName] [nvarchar](50) NOT NULL, " +
                                           "[ViewName] [nvarchar](50) NOT NULL, " +
                                           "[LangCode] [nvarchar](50) NOT NULL, " +
                                           "[Text] [nvarchar](4000) NULL, " +
                                           "[UpdatedServerUTC] [datetime] NULL," +
                                           "[PMC_Row_Status] [int] NULL, " +
                                           "CONSTRAINT [PK_PB_Help] PRIMARY KEY ([ModuleName], [ProcessName], [ViewName], [LangCode]))";
                _sqlCeHelper.CreateTable(querystring, TableName.PB_Help);
            }
        }

        #endregion

        /// <summary>
        /// This method creates PB_PackageTypes table in local database.
        /// </summary>
        public void CreateTmiSettingsTable()
        {
            const string querystring = "CREATE TABLE [PB_TMISettings] (" +
                                       "[UserRole] [nvarchar] (50) NOT NULL, " +
                                       "[SettingCode] [nvarchar] (250) NOT NULL, " +
                                       "[ProcessCode] [nvarchar] (250) NOT NULL, " +
                                       "[TMSId] [nvarchar] (50) NOT NULL, " +
                                       "[Value] [nvarchar] (250), " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_TMISettings] PRIMARY KEY ([UserRole], [SettingCode], [ProcessCode], [TMSId]))";

            _sqlCeHelper.CreateTable(querystring, TableName.PB_TMISettings);
        }

        /// <summary>
        /// This method creates settings table in local database.
        /// </summary>
        public void CreateSettingsTable()
        {
            const string querystring = "CREATE TABLE [PB_Settings] ( " +
                                       "CredentialId int NOT NULL, " +
                                       "Name nvarchar(50) NOT NULL, " +
                                       "Value nvarchar(50) NOT NULL ) ";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_Settings);
        }

        /// <summary>
        /// This method creates Location table in local database.
        /// </summary>
        public void CreateLocationTable()
        {
            const string querystring = "CREATE TABLE [PB_OrgUnits] (" +
                                       "[Number] [nvarchar](50) NOT NULL," +
                                       "[Name] [nvarchar](50) NULL," +
                                       "[Type] [nvarchar](50) NULL," +
                                       "[PostalNumber] [nvarchar](50) NULL," +
                                       "[CountryCode] [nvarchar](50) NULL," +
                                       "[LocationIdent] [nvarchar](50) NULL," +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_OrgUnits] PRIMARY KEY ([Number]))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_OrgUnits);
        }

        public void InsertMasterData(TableName tableName, string p)
        {
            _sqlCeHelper.InsertMasterData(tableName, p);
        }

        public int NumberOfRowsInTable(TableName tableName)
        {
            return _sqlCeHelper.NumberOfRowsInTable(tableName);
        }

        /// <summary>
        /// This method creates Profile Table in local database.
        /// </summary>
        public void CreateUserProfileTable()
        {
            const string querystring = "CREATE TABLE [PB_Profile] (" +
                                       "[UserId] [nvarchar] (50) NOT NULL, " +
                                       "[UserRole] [nvarchar] (50) NOT NULL, " +
                                       "[OrgUnitId] [nvarchar] (50) NULL, " +
                                       "[PowerUnit] [nvarchar] (50) NULL, " +
                                       "[LoadCarrier] [nvarchar] (50) NULL, " +
                                       "[UnitId] [nvarchar] (50) NULL, " +
                                       "[TMSAffiliation] [nvarchar] (50) NULL, " +
                                       "[RouteId] [nvarchar] (50) NULL, " +
                                       "[CompanyCode] [nvarchar] (50) NULL, " +
                                       "[TelephoneNumber] [nvarchar] (50) NULL, " +
                                       "[ChangedDate] [DateTime] NULL, " +
                                       "[FirstName] [nvarchar] (50) NULL, " +
                                       "[LastName] [nvarchar] (50) NULL, " +
                                       "[GPSEnabled] [BIT] NOT NULL DEFAULT(0), " +
                                       "CONSTRAINT [PK_PB_Profile] PRIMARY KEY ([UserId]))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_Profile);
        }

        /// <summary>
        /// This method creates PB_EventTypes table in local database.
        /// </summary>
        public void CreateEventTable()
        {
            const string querystring = "CREATE TABLE [PB_EventTypes] (" +
                                       "[LangCode] [nvarchar] (50) NOT NULL, " +
                                       "[Code] [nvarchar](50) NOT NULL, " +
                                       "[Type] [nvarchar](50) NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_EventTypes] PRIMARY KEY (Code, LangCode))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_EventTypes);
        }

        /// <summary>
        /// This method creates PB_Reasons table in local database.
        /// </summary>
        public void CreateReasonTable()
        {
            const string querystring = "CREATE TABLE [PB_Reasons] (" +
                                       "[Code] [nvarchar](50) NOT NULL, " +
                                       "[Description] [nvarchar] (2000) NULL, " +
                                       "[LangCode] [nvarchar] (50) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_ReasonTypes] PRIMARY KEY (Code, LangCode))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_Reasons);
        }

        /// <summary>
        /// This method creates PB_Reasons table in local database.
        /// </summary>
        public void CreateActionTable()
        {
            const string querystring = "CREATE TABLE [PB_Actions] (" +
                                       "[Code] [nvarchar](50) NOT NULL, " +
                                       "[Description] [nvarchar] (2000) NULL, " +
                                       "[LangCode] [nvarchar] (50) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_ActionTypes] PRIMARY KEY (Code, LangCode))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_Actions);
        }

        /// <summary>
        /// This method creates PB_Reasons table in local database.
        /// </summary>
        public void CreateEventReasonActionTable()
        {
            const string querystring = "CREATE TABLE [PB_EventReasonAction] (" +
                                       "[EventCode] [nvarchar](50) NOT NULL, " +
                                       "[ReasonCode] [nvarchar](50) NOT NULL, " +
                                       "[ActionCode] [nvarchar](50) NOT NULL, " +
                                       "[Role] [nvarchar] (50) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_EventReasonAction] PRIMARY KEY (EventCode, ReasonCode, ActionCode, Role))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_EventReasonAction);
        }

        public void CreateDamageCodeTable()
        {
            const string querystring = "CREATE TABLE [PB_DamageCodes] ( " +
                                       "[Code] [nvarchar] (50) NOT NULL, " +
                                       "[Description] [nvarchar] (200) NULL,  " +
                                       "[LangCode] [nvarchar] (50) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_DamageCodes] PRIMARY KEY (Code, LangCode ))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_DamageCodes);
        }

        public void CreateDeviceSettingsTable()
        {
            const string querystring = "CREATE TABLE [PB_DeviceSettings] (" +
                                       "[DeviceType] [nvarchar] (50) NOT NULL, " +
                                       "[Name] [nvarchar] (200) NOT NULL, " +
                                       "[Value] [nvarchar] (200) NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL, " +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_DeviceSettings] PRIMARY KEY (DeviceType, Name))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_DeviceSettings);
        }

        public void CreateProductGroupTable()
        {
            const string querystring = "CREATE TABLE [PB_ProductGroups] (" +
                                       "[Code] [nvarchar](50) NOT NULL, " +
                                       "[Description] [nvarchar] (2000) NULL, " +
                                       "[LangCode] [nvarchar] (50) NOT NULL, " +
                                       "[TMSId] [nvarchar] (50) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_ProductGroup] PRIMARY KEY (Code, LangCode, TMSId))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_ProductGroups);
        }

        private void CreateProductGroupTransitionTable()
        {
            const string querystring = "CREATE TABLE [PB_ProductGroupTransitions] (" +
                                       "[FromCode] [nvarchar](50) NOT NULL, " +
                                       "[ToCode] [nvarchar] (50) NOT NULL, " +
                                       "[TMSId] [nvarchar] (50) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                       "CONSTRAINT [PK_PB_ProductGroupTransition] PRIMARY KEY (FromCode, ToCode, TMSId))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_ProductGroupTransitions);
        }

        private void CreateOpeningHoursTable()
        {
            const string querystring = "CREATE TABLE [PB_OpeningHours] (" +
                                      "[DayIndex] [int] NOT NULL," +
                                      "[StartTime] [nchar](6) NOT NULL," +
                                      "[EndTime] [nchar](6) NOT NULL, " +
                                      "[CountryCode] [nvarchar] (10) NOT NULL, " +
                                       "[UpdatedServerUTC] [datetime] NULL," +
                                       "[PMC_Row_Status] [int] NULL, " +
                                      "CONSTRAINT [PK_PB_OpeningHours] PRIMARY KEY (DayIndex, StartTime, EndTime, CountryCode))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_OpeningHours);
        }

        private void CreateLimitedOpeningHoursTable()
        {
            const string querystring = "CREATE TABLE [PB_LimitedOpeningHours] (" +
                          "[Date] [datetime] NOT NULL," +
                          "[StartTime] [nchar](6) NOT NULL, " +
                          "[EndTime] [nchar](6) NOT NULL, " +
                          "[CountryCode] [nvarchar] (10) NOT NULL, " +
                          "[Comment] [nvarchar] (100), " +
                           "[UpdatedServerUTC] [datetime] NULL," +
                           "[PMC_Row_Status] [int] NULL, " +
                          "CONSTRAINT [PK_PB_LimitedOpeningHours] PRIMARY KEY (Date, StartTime, EndTime, CountryCode))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_LimitedOpeningHours);
        }

        private void CreateFixedClosingDatesTable()
        {
            const string querystring = "CREATE TABLE [PB_FixedClosingDates] (" +
                           "[Date] [datetime] NOT NULL," +
                           "[CountryCode] [nvarchar] (10) NOT NULL, " +
                           "[UpdatedServerUTC] [datetime] NULL," +
                            "[PMC_Row_Status] [int] NULL, " +
                           "CONSTRAINT [PK_PB_FixedClosingDates] PRIMARY KEY (Date, CountryCode))";

            _sqlCeHelper.CreateTable(querystring, TableName.PB_FixedClosingDates);
        }

        private void CreateVasTypeTable()
        {
            const string querystring = "CREATE TABLE [PB_VasTypes] (" +
                           "[Code] [nvarchar] (10)NOT NULL," +
                           "[Text] [nvarchar] (200) NOT NULL, " +
                           "[Measure] [nvarchar] (50) NOT NULL, " +
                           "[SignatureRequired] [nvarchar] (10) NOT NULL, " +
                           "[UpdatedServerUTC] [datetime] NULL," +
                           "[PMC_Row_Status] [int] NULL, " +
                          "CONSTRAINT [PK_PB_VasTypes] PRIMARY KEY (Code))";

            _sqlCeHelper.CreateTable(querystring, TableName.PB_VasTypes);
        }

        private void CreateRolesTable()
        {
            const string querystring = "CREATE TABLE [PB_Roles] ( " +
                                       "[UserId] [nvarchar] (50) NOT NULL, " +
                                       "[RoleName] [nvarchar] (50) NOT NULL) ";
            //TODO Add constriant
            //"CONSTRAINT [PK_PB_PB_Roles] UNIQUE(UserId, RoleName))";
            _sqlCeHelper.CreateTable(querystring, TableName.PB_Roles);
        }

        /// <summary>
        /// This function gets the userprofile for specified userid.
        /// </summary>
        /// <returns>Specifies object for UserProfile entity.</returns>
        public UserProfile GetUserProfile()
        {
            string userId = GetUserId();
            var objParams = new IDataParameter[1];
            objParams[0] = _storageBase.CreateParameter("@UserId", userId);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader("SELECT * FROM PB_Profile WHERE UserId = @UserId", objParams))
            {
                var userProfile = GetUserProfileFromDataReader(dataReader);
                if (userProfile != null)
                    userProfile.Roles = GetUserRoles(userId);
                return userProfile;
            }
        }

        /// <summary>
        /// Gets the parcel from data reader.
        /// </summary>
        /// <param name="dataReader">The data reader</param>
        /// <returns></returns>
        private UserProfile GetUserProfileFromDataReader(IDataReader dataReader)
        {
            UserProfile userProfile = null;
            try
            {
                if (dataReader != null && dataReader.Read())
                {
                    userProfile = new UserProfile
                    {
                        UserId = GetStringFromDataReader(dataReader, "UserId"),
                        RoleName = GetStringFromDataReader(dataReader, "UserRole"),
                        OrgUnitId = GetStringFromDataReader(dataReader, "OrgUnitId"),
                        PowerUnit = GetStringFromDataReader(dataReader, "PowerUnit"),
                        LoadCarrier = GetStringFromDataReader(dataReader, "LoadCarrier"),
                        TmsAffiliation = GetStringFromDataReader(dataReader, "TmsAffiliation"),
                        RouteId = GetStringFromDataReader(dataReader, "RouteId"),
                        CompanyCode = GetStringFromDataReader(dataReader, "CompanyCode"),
                        TelephoneNumber = GetStringFromDataReader(dataReader, "TelephoneNumber"),
                        FirstName = GetStringFromDataReader(dataReader, "FirstName"),
                        LastName = GetStringFromDataReader(dataReader, "LastName"),
                        GPSEnabled = GetBoolFromDataReader(dataReader, "GPSEnabled"),
                    };
                }
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "GetUserProfileFromDataReader");
            }

            return userProfile;
        }

        private List<string> GetUserRoles(string userId)
        {
            var roles = new List<string>();
            try
            {

                var objParams = new IDataParameter[1];
                objParams[0] = _storageBase.CreateParameter("@UserId", userId);
                using (var dataReader = _sqlCeHelper.ExecuteDataReader("SELECT * FROM PB_Roles WHERE UserId = @UserId", objParams))
                {

                    while (dataReader.Read())
                    {
                        string roleName = GetStringFromDataReader(dataReader, "RoleName");
                        roles.Add(roleName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataProvider.GetUserRoles");
            }

            return roles;
        }

        /// <summary>
        /// Checks if user has a local profile
        /// </summary>
        private bool IsKnownLocalUser(string userId)
        {
            var objParams = new IDataParameter[1];
            objParams[0] = _storageBase.CreateParameter("@UserId", userId);
            string count = _sqlCeHelper.ExecuteScalar("SELECT count(1) FROM PB_Profile WHERE UserId = @UserId", objParams);
            return count == "1";
        }

        public DateTime? GetLatestRowForTable(TableName table)
        {
            DateTime? result = null;
            string tableName = table.ToString();
            if (_sqlCeHelper.TableExists(tableName))
            {
                const string query = "SELECT LastUpdatedServerUTC FROM SyncNM_Status WHERE Object = @TableName OR Alias = @TableName";
                var objParams = new IDataParameter[1];
                objParams[0] = _storageBase.CreateParameter("@TableName", table);

                using (var dataReader = _sqlCeHelper.ExecuteDataReader(query, objParams, true))
                {
                    if (dataReader.Read())
                    {
                        if (dataReader.IsDBNull(0) == false)
                            result = dataReader.GetDateTime(0);
                    }
                }
            }

            return result;
        }



        private string GetStringFromDataReader(IDataReader dataReader, string columnName)
        {
            string result = null;
            int ordinal = dataReader.GetOrdinal(columnName);
            if (dataReader.IsDBNull(ordinal) == false)
            {
                result = dataReader.GetString(ordinal);
            }
            return result;
        }

        private bool GetBoolFromDataReader(IDataReader dataReader, string columnName)
        {
            bool result = false;
            int ordinal = dataReader.GetOrdinal(columnName);
            if (dataReader.IsDBNull(ordinal) == false)
            {
                result = dataReader.GetBoolean(ordinal);
            }
            return result;
        }


        /// <summary>
        /// Get number of days to display password change warning.
        /// </summary>
        /// <returns>number of days to display warning.</returns>
        public int GetPasswordChangeWarningDateCount()
        {
            string warningDatestring = GetCommonSetting(GlobalConstants.PASSWORD_WARNING_DATE_COUNT);

            int dateCount = string.IsNullOrEmpty(warningDatestring) == false
                                ? Convert.ToInt32(warningDatestring)
                                : GlobalConstants.PASSWORD_WARNING_DAY_COUNT_VALUE;
            return dateCount;
        }

        /// <summary>
        /// Get number of days between change of password is requierd
        /// </summary>
        /// <returns>Number of days to change password.</returns>
        public int GetPasswordExpiryInterval()
        {
            int dateCount;
            string changedDatestring = GetCommonSetting(GlobalConstants.PASSWORD_CHANGED_DATE);
            if (string.IsNullOrEmpty(changedDatestring) == false)
            {
                var changedDate = new DateTime(Convert.ToInt64(changedDatestring));
                dateCount = DateTime.Now.Subtract(changedDate).Days;
            }
            else
            {
                dateCount = GlobalConstants.PASSWORD_EXPIRE_DAY_COUNT_VALUE;
            }
            return dateCount;
        }

        /// <summary>
        /// Check whether password is changed
        /// </summary>
        /// <param name="settings">List of <see cref="Entity.SettingElement"/>.</param>
        /// <returns>True, if password is changed, otherwise false.</returns>
        public bool IsPasswordChanged(List<SettingElement> settings)
        {
            bool passwordChanged = false;
            SettingElement settingElement = settings.Find(o => o.Name == GlobalConstants.PASSWORD_CHANGED_DATE);

            if (settingElement != null)
            {
                passwordChanged = true;
            }

            return passwordChanged;
        }

        /// <summary>
        /// Get location name.
        /// </summary>
        /// <param name="locationId">The location id.</param>
        /// <returns>Location name.</returns>
        public string GetLocationName(string locationId)
        {
            var objParams = new IDataParameter[1];
            objParams[0] = _storageBase.CreateParameter("@id", locationId);
            return _sqlCeHelper.ExecuteScalar("SELECT Name FROM PB_OrgUnits WHERE Number = @id", objParams);
        }

        public string GetHelpText(string moduleName, string processName, string viewName, string language)
        {
            Logger.LogEvent(Severity.Debug, "gethelp;" + moduleName + "," + processName + "," + viewName);
            //var localHelp = GetFileBasedHelp(moduleName, processName, viewName, language);
            var localHelp = GetXmlBasedHelp(moduleName, processName, viewName, language);
            if (localHelp.Length > 0)
            {
                return localHelp;
            }
            var objParams = new IDataParameter[4];
            objParams[0] = _storageBase.CreateParameter("@moduleName", moduleName);
            objParams[1] = _storageBase.CreateParameter("@processName", processName);
            objParams[2] = _storageBase.CreateParameter("@viewName", viewName);
            objParams[3] = _storageBase.CreateParameter("@language", language);
            const string sqlCmd = "SELECT Text FROM PB_Help WHERE ModuleName = @moduleName AND ProcessName = @processName AND ViewName = @viewName AND LangCode = @language ";

            var result = _sqlCeHelper.ExecuteScalar(sqlCmd, objParams);
            if (result.Length == 0)
            {
                var objParamsNob = new IDataParameter[4];
                objParamsNob[0] = _storageBase.CreateParameter("@moduleName", moduleName);
                objParamsNob[1] = _storageBase.CreateParameter("@processName", processName);
                objParamsNob[2] = _storageBase.CreateParameter("@viewName", viewName);
                objParamsNob[3] = _storageBase.CreateParameter("@language", "nob");
                result = _sqlCeHelper.ExecuteScalar(sqlCmd, objParamsNob);
            }


            return result;
        }
        #region "local help"

        private List<HelpTexts> _foundHelp;
        /// <summary>
        /// reads help text from file in PreCom folder as an alternative to tha table based help system
        /// is to be used for fast testing of changes in the help texts
        /// </summary>
        /// <param name="moduleName"></param>
        /// <param name="processName"></param>
        /// <param name="viewName"></param>
        /// <param name="language"></param>
        /// <returns>the help text from help.csv or an empty string if the file does not exist or if the 
        /// keywords do not match</returns>
        internal string GetFileBasedHelp(string moduleName, string processName, string viewName, string language)
        {
            try
            {
                if (_foundHelp == null)
                {
                    var helpFilePath = FileUtil.GetApplicationPath() + "\\help.csv";
                    if (!new FileInfo(helpFilePath).Exists) return "";

                    using (var fileStream = new StreamReader(helpFilePath))
                    {

                        var helpTexts = new List<HelpTexts>();

                        while (!fileStream.EndOfStream)
                        {
                            var line = fileStream.ReadLine();
                            var lineparts = line.Split(Convert.ToChar(";"));
                            if (lineparts.Length == 5)
                            {
                                helpTexts.Add(new HelpTexts
                                               {
                                                   ModuleName = lineparts[0],
                                                   ProcessName = lineparts[1],
                                                   ViewName = lineparts[2],
                                                   Text = lineparts[3],
                                                   Language = lineparts[4]
                                               });
                            }
                            else
                            {
                                Logger.LogEvent(Severity.Debug, "GetLocallyStoredHelp rejected line;" + line);
                            }
                        }
                        _foundHelp = helpTexts;
                        fileStream.Close();
                    }
                }

                foreach (var helpline in _foundHelp.Where(helpline => helpline.ModuleName.ToLower() == moduleName.ToLower() && helpline.ProcessName.ToLower() == processName.ToLower() && helpline.ViewName.ToLower() == viewName.ToLower() && helpline.Language.ToLower() == language.ToLower()))
                {
                    return helpline.Text;
                }
                return "";
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Severity.Debug, "GetLocallyStoredHelp Error;" + ex.Message);
            }

            return null;
        }

        internal string GetXmlBasedHelp(string actualModuleName, string actualProcessName, string actualViewName, string language)
        {
            string helpText = string.Empty;
            string xmlLanguage = string.Empty;
            if (Directory.Exists(FileUtil.GetApplicationPath() + "\\Help"))
            {
                var xmlFiles = Directory.GetFiles(FileUtil.GetApplicationPath() + "\\Help", "*.xml");
                if (xmlFiles.Length > 0)
                {
                    foreach (var xml in xmlFiles)
                    {
                        var fileName = Path.GetFileName(xml);
                        if (fileName != null)
                        {
                            var fileInfo = fileName.Split('_');
                            if (fileInfo.Length > 3)
                            {
                                var moduleName = fileInfo[0];
                                var processName = fileInfo[1];
                                var viewName = fileInfo[2];
                                if ((moduleName == actualModuleName) && (processName == actualProcessName) && (viewName == actualViewName))
                                {
                                    var xDoc = new XmlDocument();
                                    //load up the xml from the location 
                                    xDoc.Load(FileUtil.GetApplicationPath() + "\\Help\\" + fileName);

                                    // cycle through each child noed 
                                    if (xDoc.DocumentElement != null)
                                        foreach (XmlNode node in xDoc.DocumentElement.ChildNodes)
                                        {
                                            if (node.Name == "Language")
                                            {
                                                xmlLanguage = node.InnerText;
                                            }
                                            if ((node.Name == "HelpText") && (xmlLanguage == language))
                                            {
                                                helpText = node.InnerText;
                                                return helpText;
                                            }
                                        }
                                }
                            }


                        }
                    }
                }
            }
            return helpText;

        }
        internal class HelpTexts
        {
            internal string ModuleName;
            internal string ProcessName;
            internal string ViewName;
            internal string Language;
            internal string Text;
        }
        # endregion
        /// <summary>
        /// Get all locations.
        /// </summary>
        /// <returns>Location name.</returns>
        public List<LocationElement> GetLocations()
        {
            var locationElements = new List<LocationElement>();
            using (var dataReader = _sqlCeHelper.ExecuteDataReader("SELECT * FROM PB_OrgUnits", null))
            {
                while (dataReader.Read())
                {
                    var locationElement = new LocationElement
                        {
                            Number = dataReader["Number"].ToString(),
                            Name = dataReader["Name"].ToString(),
                            PostalNumber = dataReader["PostalNumber"].ToString(),
                            CountryCode = dataReader["CountryCode"].ToString()
                        };

                    locationElements.Add(locationElement);
                }
                return locationElements;
            }
        }

        public bool SaveUserProfile(UserProfile userProfile)
        {
            try
            {
                string userId = GetUserId();
                //get profile from database
                bool existingUser = IsKnownLocalUser(userId);
                if (existingUser == false)
                {
                    Logger.LogEvent(Severity.Debug, "Adding new local profile");
                    //Insert user profile in to local database
                    InsertUserProfile(userProfile);
                    InsertRoles(userId, userProfile.Roles);
                }
                else //UpdateStatus user profile in local database
                {
                    Logger.LogEvent(Severity.Debug, "Updating local user profile");
                    UpdateUserProfile(userProfile);
                    UpdateRoles(userId, userProfile.Roles);
                }
                return true;
            }

            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataProvider.SaveUserProfile");
                return false;
            }
        }

        /// <summary>
        /// Get post code for the location.
        /// </summary>
        /// <param name="locationId">The location id.</param>
        /// <returns>Post code.</returns>
        public string GetPostalCode(string locationId)
        {
            try
            {
                var objParams = new IDataParameter[1];
                objParams[0] = _storageBase.CreateParameter("@id", locationId);

                return _sqlCeHelper.ExecuteScalar("SELECT PostalNumber FROM PB_OrgUnits WHERE Number = @id", objParams);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataProvider.GetPostalCode");
                return string.Empty;
            }
        }

        /// <summary>
        /// Get post code for the location.
        /// </summary>
        /// <param name="locationId">The location id.</param>
        /// <returns>Country code</returns>
        public string GetCountryCode(string locationId)
        {
            try
            {
                var objParams = new[] { _storageBase.CreateParameter("@id", locationId) };
                return _sqlCeHelper.ExecuteScalar("SELECT CountryCode FROM PB_OrgUnits WHERE Number = @id", objParams);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataProvider.GetCountryCode");
                return string.Empty;
            }


        }

        /// <summary>
        /// Check the possibility to change password.
        /// </summary>
        /// <returns>True, if possible to change password. otherwise false.</returns>
        public bool IsPasswordChangeable()
        {
            bool isChangeable = false;

            string passwordChanged = GetSetting(GlobalConstants.PASSWORD_CHANGED_DATE);

            if (!string.IsNullOrEmpty(passwordChanged))
            {
                var changeDate = new DateTime(long.Parse(passwordChanged));

                if (DateTime.UtcNow.Day != changeDate.Day || DateTime.UtcNow.Month != changeDate.Month
                    || DateTime.UtcNow.Year != changeDate.Year)
                {
                    isChangeable = true;
                }
            }
            else
            {
                isChangeable = true;
            }

            return isChangeable;
        }

        /// <summary>
        /// Check whether password is expired.
        /// </summary>
        /// <param name="userSettings">List of user settings.</param>
        /// <returns>True, if password is expired. otherwise false.</returns>
        public bool IsPasswordExpired(List<SettingElement> userSettings)
        {
            bool isExpired = false;

            try
            {
                // Get date of lastpassword change
                SettingElement settingElementPasswordChangedDate =
                    userSettings.Find(o => o.Name == GlobalConstants.PASSWORD_CHANGED_DATE);
                if (settingElementPasswordChangedDate != null)
                {
                    // Check if password for user never expires
                    if (settingElementPasswordChangedDate.Value == GlobalConstants.PASSWORD_NEVER_EXPIRED_VALUE)
                    {
                        return false;
                    }

                    int passwordExpiryInterval = GetPasswordExpiryInterval();
                    var lastChangedDate = new DateTime(long.Parse(settingElementPasswordChangedDate.Value));

                    double daysSincePwChange = Convert.ToDouble(DateTime.UtcNow.Subtract(lastChangedDate).TotalDays);
                    if (daysSincePwChange >= passwordExpiryInterval)
                    {
                        isExpired = true;
                    }
                    else
                    {
                        var daysToExpiration = (int)(passwordExpiryInterval - daysSincePwChange + 0.5);
                        isExpired = CheckPasswordAlmostExpired(daysToExpiration);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataProvider.IsPasswordExpired ");
                isExpired = false;
            }

            return isExpired;
        }

        /// <summary>
        /// Issue a warning if it is less than n days to password expires
        /// </summary>
        /// <returns></returns>
        private bool CheckPasswordAlmostExpired(int daysToExpiration)
        {
            bool result = false;
            try
            {
                if (daysToExpiration <= GetPasswordChangeWarningDateCount())
                {
                    // Ask user if want's to change password
                    string messageText = string.Format(GlobalTexts.PasswordSoonExpired, daysToExpiration);

                    var messageBoxResult = GuiCommon.ShowModalDialog(GlobalTexts.Warning, messageText,
                        Severity.Warning, GlobalTexts.Yes, GlobalTexts.No);

                    result = messageBoxResult == GlobalTexts.Yes;
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "CheckPasswordAlmostExpired");
            }

            return result;
        }

        /// <summary>
        /// Get default location id.
        /// </summary>
        /// <returns>Location id.</returns>
        public string GetDefaultLocationId()
        {
            string locationId = GetSetting(GlobalConstants.USER_LOCATION_ID);
            return locationId;
        }

        /// <summary>
        /// Get regular expression of the password policy.
        /// </summary>
        /// <returns>The regular expression.</returns>
        public string GetPasswordPolicyExpression()
        {
            string pwdPolicy = GetCommonSetting(GlobalConstants.PASSWORD_POLICY);
            return pwdPolicy;
        }


        public string GetCommonSetting(string settingName)
        {
            return GetSetting(settingName, 0);
        }

        public string GetCommonSetting(string settingName, string defaultValue)
        {
            string result = GetSetting(settingName, 0);
            if (result == string.Empty)
                return defaultValue;
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="settingName"></param>
        /// <returns></returns>
        public string GetSetting(string settingName)
        {
            int credentialId = _precom.Communication.GetClient().CredentialId;
            return GetSetting(settingName, credentialId);
        }

        private string GetSetting(string settingName, int credentialId)
        {
            var objParams = new IDataParameter[2];
            objParams[0] = _storageBase.CreateParameter("@Name", settingName);
            objParams[1] = _storageBase.CreateParameter("@CredentialId", credentialId);
            string settingValue = _sqlCeHelper.ExecuteScalar(
                "SELECT Value FROM PB_Settings WHERE CredentialId = @CredentialId AND Name = @Name", objParams);
            return settingValue;
        }

        public void SaveSettings(SettingElements settings)
        {
            int credentialId = _precom.Communication.GetClient().CredentialId;

            if (DeleteSettings(credentialId))
            {
                foreach (SettingElement setting in settings.UserSettings)
                {
                    InsertSetting(setting.Name, setting.Value, credentialId);
                }

                if (DeleteSettings(0))
                {
                    foreach (SettingElement setting in settings.CommonSettings)
                    {
                        InsertSetting(setting.Name, setting.Value, 0);
                    }
                }
            }
            else
            {
                Logger.LogEvent(Severity.Error, "Failed to save settings from server");
            }
        }

        private bool DeleteSettings(int credentialId)
        {
            var objParams = new IDataParameter[1];
            objParams[0] = _storageBase.CreateParameter("@CredentialId", credentialId);

            var status = _sqlCeHelper.ExecuteDelete(
                "DELETE FROM PB_Settings WHERE CredentialId = @CredentialId OR CredentialId = 0",
                objParams, TableName.PB_Settings);
            return status;
        }

        public bool SaveSetting(string name, string value)
        {
            int credentialId = _precom.Communication.GetClient().CredentialId;
            // Save setting in local DB
            bool result = SaveSetting(name, value, credentialId);
            if (result)
            {
                UpdateServerSetting(name, value);
            }
            return result;
        }

        /// <summary>
        /// Generic method  for saving any kind of CustomSetting
        /// NOTE: This is not to be called directly, onloy through Setting
        /// </summary>
        private static void UpdateServerSetting(string settingName, string settingValue)
        {
            var customSetting = new CustomSetting
            {
                Name = settingName,
                Value = settingValue
            };

            var transaction = new Transaction(Guid.NewGuid(), (byte)TransactionCategory.UserDetail, settingName);
            var sendArgs = new RequestArgs(QueuePriority.High, Persistence.UntilDelivered, transaction);

#pragma warning disable 618
            PreCom.Application.Instance.Communication.Send(customSetting, sendArgs);
#pragma warning restore 618
        }

        private bool SaveSetting(string name, string value, int credentialId)
        {
            bool result;
            // Insert new setting if not exists
            var currentValue = GetSetting(name);
            if (string.IsNullOrEmpty(currentValue))
            {
                result = InsertSetting(name, value, credentialId);
            }
            else
            {
                result = currentValue == value || UpdateSetting(name, value, credentialId);
            }
            return result;
        }

        private bool InsertSetting(string name, string value, int credentialId)
        {
            var objParams = new IDataParameter[3];
            objParams[0] = _storageBase.CreateParameter("@Name", name);
            objParams[1] = _storageBase.CreateParameter("@Value", value);
            objParams[2] = _storageBase.CreateParameter("@CredentialId", credentialId);
            var status =
                _sqlCeHelper.ExecuteNonQuery(
                    "INSERT INTO PB_Settings (CredentialId, Name, Value) VALUES (@CredentialId, @Name, @Value)",
                    objParams);
            return status;
        }

        private bool UpdateSetting(string name, string value, int credentialId)
        {
            var objParams = new IDataParameter[3];
            objParams[0] = _storageBase.CreateParameter("@Name", name);
            objParams[1] = _storageBase.CreateParameter("@Value", value);
            objParams[2] = _storageBase.CreateParameter("@CredentialId", credentialId);

            bool result = _sqlCeHelper.ExecuteNonQuery(
                "UPDATE PB_Settings SET Value = @Value WHERE CredentialId = @CredentialId AND Name = @Name", objParams);
            return result;
        }

        /// <summary>
        /// Save password changed date.
        /// </summary>
        /// <param name="date">Password changed date</param>
        public void SavePasswordChangedDate(DateTime date)
        {
            int credentialId = _precom.Communication.GetClient().CredentialId;
            SaveSetting(GlobalConstants.PASSWORD_CHANGED_DATE, date.Ticks.ToString(CultureInfo.InvariantCulture), credentialId);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string GetUserId()
        {
            return GetCredentialsValue("username");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="credentialKey"></param>
        /// <returns></returns>
        public string GetCredentialsValue(string credentialKey)
        {
            var objParams = new IDataParameter[2];
            objParams[0] = _storageBase.CreateParameter("@CredentialKey", credentialKey);
            objParams[1] = _storageBase.CreateParameter("@CredentialId", _precom.Communication.GetClient().CredentialId);
            var value = _sqlCeHelper.ExecuteScalar(
                "SELECT [credentialValue] FROM [PMC_Communication_LoginCredentials] WHERE CredentialKey = @CredentialKey and CredentialID = @CredentialId",
                objParams);
            return value;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string GetCredentialsBase64()
        {
            string username = GetCredentialsValue("username");
            string password = GetCredentialsValue("password");
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(username + ":" + password));
        }

        public bool SaveReceivedMessage(MessageToDriver message)
        {
            const string querystring = "INSERT INTO PB_Messages (UnitId, CaptureEquipmentId, UserLogonId, MessageTime, UserSessionId, MessageId, CorrelationId, MessageText, UserId, Status) " +
                                       " VALUES (@UnitId,@CaptureEquipmentId, @UserLogonId, @MessageTime,@UserSessionId,@MessageId,@CorrelationId,@MessageText, @UserId, @Status)";

            var queryParams = new[]
                {
                    _storageBase.CreateParameter("@UnitId", message.UnitId),
                    _storageBase.CreateParameter("@CaptureEquipmentId", message.CaptureEquipmentId),
                    _storageBase.CreateParameter("@UserLogonId", message.UserLogonId),
                    _storageBase.CreateParameter("@MessageTime", message.MessageTime),
                    _storageBase.CreateParameter("@UserSessionId", message.UserSessionId),
                    _storageBase.CreateParameter("@MessageId", message.MessageId),
                    _storageBase.CreateParameter("@CorrelationId", message.CorrelationId),
                    _storageBase.CreateParameter("@MessageText", message.MessageText),
                    _storageBase.CreateParameter("@UserId", message.UserId),
                    _storageBase.CreateParameter("@Status", message.Status)
                };

            var status = _sqlCeHelper.ExecuteNonQuery(querystring, queryParams);
            return status;
        }

        public bool SaveSentMessage(MessageFromDriver message, string status)
        {
            const string querystring = "INSERT INTO PB_Messages (UnitId, CaptureEquipmentId, UserLogonId, MessageTime, UserSessionId, MessageId, CorrelationId, MessageText, UserId, Status) " +
                                       " VALUES (@UnitId,@CaptureEquipmentId, @UserLogonId, @MessageTime,@UserSessionId,@MessageId,@CorrelationId,@MessageText, @UserId, @Status)";

            var queryParams = new[]
                {
                    _storageBase.CreateParameter("@UnitId", message.UnitId),
                    _storageBase.CreateParameter("@CaptureEquipmentId", message.CaptureEquipmentId),
                    _storageBase.CreateParameter("@UserLogonId", message.UserLogonId),
                    _storageBase.CreateParameter("@MessageTime", message.MessageTime),
                    _storageBase.CreateParameter("@UserSessionId", message.SessionId),
                    _storageBase.CreateParameter("@MessageId", message.MessageId),
                    _storageBase.CreateParameter("@CorrelationId", message.CorrelationId),
                    _storageBase.CreateParameter("@MessageText", message.MessageText),
                    _storageBase.CreateParameter("@UserId", message.UserLogonId),
                    _storageBase.CreateParameter("@Status",status)
                };

            var result = _sqlCeHelper.ExecuteNonQuery(querystring, queryParams);
            return result;
        }

        public bool MessageExists(string messageId)
        {
            var query = new StringBuilder();
            var objParams = new IDataParameter[1];
            objParams[0] = _storageBase.CreateParameter("@MessageId", messageId);
            query.Append(string.Format("SELECT COUNT(1) FROM {0} where MessageId = @MessageId", TableName.PB_Messages));
            int count = Convert.ToInt32(_sqlCeHelper.ExecuteScalar(query.ToString(), objParams));
            if (count > 0)
                return true;
            return false;
        }
        public int GetUnreadMessageCount()
        {
            var query = new StringBuilder();
            query.Append(string.Format("SELECT COUNT(1) FROM {0}", TableName.PB_Messages));
            query.Append(" where Status = 'U'");
            return Convert.ToInt32(_sqlCeHelper.ExecuteScalar(query.ToString(), null));
        }

        public List<MessageToDriver> GetMessages(string userId)
        {
            var result = new List<MessageToDriver>();

            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_Messages);
            // queryString.Append(" WHERE UserId = @UserId ");

            var parameters = new IDataParameter[1];
            parameters[0] = _storageBase.CreateParameter("@UserId", userId);

            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var messageToDriver = new MessageToDriver
                            {
                                CaptureEquipmentId = dataReader.IsDBNull(dataReader.GetOrdinal("CaptureEquipmentId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("CaptureEquipmentId")),
                                CorrelationId = dataReader.IsDBNull(dataReader.GetOrdinal("CorrelationId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("CorrelationId")),
                                MessageId = dataReader.IsDBNull(dataReader.GetOrdinal("MessageId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("MessageId")),
                                MessageText = dataReader.IsDBNull(dataReader.GetOrdinal("MessageText")) ? "" : dataReader.GetString(dataReader.GetOrdinal("MessageText")),
                                MessageTime = dataReader.IsDBNull(dataReader.GetOrdinal("MessageTime")) ? DateTime.MaxValue : DateTime.Parse(dataReader.GetString(dataReader.GetOrdinal("MessageTime"))),
                                Status = dataReader.IsDBNull(dataReader.GetOrdinal("Status")) ? "" : dataReader.GetString(dataReader.GetOrdinal("Status")),
                                UnitId = dataReader.IsDBNull(dataReader.GetOrdinal("UnitId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("UnitId")),
                                UserId = dataReader.IsDBNull(dataReader.GetOrdinal("UserId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("UserId")),
                                UserLogonId = dataReader.IsDBNull(dataReader.GetOrdinal("UserLogonId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("UserLogonId")),
                                UserSessionId = dataReader.IsDBNull(dataReader.GetOrdinal("UserSessionId")) ? "" : dataReader.GetString(dataReader.GetOrdinal("UserSessionId")),


                            };
                        result.Add(messageToDriver);
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// Delete message with 
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="status"> </param>
        /// <returns></returns>
        public bool UpdateMessage(string messageId, string status)
        {
            Logger.LogEvent(Severity.Info, "MessageId =" + messageId + " Status =" + status);
            var objParams = new IDataParameter[2];
            objParams[0] = _storageBase.CreateParameter("@MessageId", messageId);
            objParams[1] = _storageBase.CreateParameter("@Status", status);

            bool result = _sqlCeHelper.ExecuteNonQuery(
                "UPDATE PB_Messages SET Status = @Status WHERE MessageId = @MessageId",
                objParams);

            return result;
        }

        /// <summary>
        /// Delete all messages marked deleted
        /// </summary>
        /// <returns></returns>
        public bool DeleteAllMessages()
        {
            var objParams = new IDataParameter[0];
            bool result = _sqlCeHelper.ExecuteNonQuery(" Delete from PB_Messages WHERE Status = 'RD' OR Status = 'UD' OR Status = 'SD' ", objParams);
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public bool InsertUserProfile(UserProfile userProfile)
        {
            const string queryString =
                "INSERT INTO PB_Profile (UserId, UserRole, OrgUnitId, PowerUnit, LoadCarrier, TMSAffiliation," +
                " RouteId, CompanyCode, TelephoneNumber, FirstName, LastName) " +
                " VALUES (@UserId,@UserRole, @OrgUnitId, @PowerUnit,@LoadCarrier, " +
                " @TMSAffiliation, @RouteId, @CompanyCode, @TelephoneNumber, @FirstName, @LastName)";

            return UpdateUserProfileTable(userProfile, queryString);
        }

        public bool UpdateUserProfile(UserProfile userProfile)
        {
            const string queryString =
                "Update PB_Profile Set OrgUnitId = @OrgUnitId, UserRole = @UserRole, PowerUnit = @PowerUnit, LoadCarrier = @LoadCarrier," +
                " TMSAffiliation = @TMSAffiliation, RouteId = @RouteId, CompanyCode = @CompanyCode," +
                " TelephoneNumber = @TelephoneNumber, FirstName = @FirstName, LastName =@LastName" +
                " where UserId=@UserId";

            return UpdateUserProfileTable(userProfile, queryString);
        }

        private static bool UpdateUserProfileTable(UserProfile userProfile, string queryString)
        {
            var queryParams = new[]
                {
                    _storageBase.CreateParameter("@UserId", userProfile.UserId),
                    _storageBase.CreateParameter("@UserRole", userProfile.RoleName),
                    _storageBase.CreateParameter("@OrgUnitId", userProfile.OrgUnitId),
                    _storageBase.CreateParameter("@PowerUnit", userProfile.PowerUnit),
                    _storageBase.CreateParameter("@LoadCarrier", userProfile.LoadCarrier),
                    _storageBase.CreateParameter("@TMSAffiliation", userProfile.TmsAffiliation),
                    _storageBase.CreateParameter("@RouteId", userProfile.RouteId),
                    _storageBase.CreateParameter("@CompanyCode", userProfile.CompanyCode),
                    _storageBase.CreateParameter("@TelephoneNumber", userProfile.TelephoneNumber),
                    _storageBase.CreateParameter("@FirstName", userProfile.FirstName),
                    _storageBase.CreateParameter("@LastName", userProfile.LastName)
                };

            var status = _sqlCeHelper.ExecuteNonQuery(queryString, queryParams);
            return status;
        }


        private void InsertRoles(string credentialId, IEnumerable<string> roleList)
        {
            const string queryString = "INSERT INTO PB_Roles (UserId, RoleName) VALUES (@UserId, @RoleName)";


            foreach (string roleName in roleList)
            {
                var queryParams = new[]
                {
                    _storageBase.CreateParameter("@UserId", credentialId),
                    _storageBase.CreateParameter("@RoleName", roleName)
                };

                _sqlCeHelper.ExecuteNonQuery(queryString, queryParams);
            }
        }

        private void UpdateRoles(string credentialId, IEnumerable<string> roleList)
        {
            // Delete existing roles first
            const string queryString = "DELETE FROM PB_Roles WHERE UserId = @UserId";

            var queryParams = new[] { _storageBase.CreateParameter("@UserId", credentialId) };
            _sqlCeHelper.ExecuteDelete(queryString, queryParams, TableName.PB_Roles);

            InsertRoles(credentialId, roleList);
        }

        /// <summary>
        /// method for adding columns in sql table
        /// </summary>
        /// 
        /// DO NOT REMOVE
        /// 
        //private void AddColumnInTable(TableName tableName, string[] columnNames, string[] columnTypes)
        //{
        //    var dataReader = _sqlCeHelper.ExecuteDataReader("select top (1) * from  " + tableName, null, true);
        //    if (dataReader != null)
        //    {
        //        for (int i = 0; i < columnNames.Length; i++)
        //        {
        //            try
        //            {
        //                //exception is thrown if column is missing in table...
        //                dataReader.GetOrdinal(columnNames[i]);
        //            }
        //            catch
        //            {
        //                AlterTable(TableName.PB_Profile, columnNames[i], columnTypes[i]);
        //            }
        //        }
        //    }
        //}
        //
        //private void AlterTable(TableName tableName, string columnName, string datatType)
        //{
        //    string query = string.Format("ALTER TABLE {0} ADD {1} {2}", tableName, columnName, datatType);
        //    _sqlCeHelper.ExecuteNonQuery(query, null);
        //}


        public int GetRowCount(string tableName)
        {
            try
            {
                var resultObject = _sqlCeHelper.ExecuteScalar("SELECT COUNT(1) FROM " + tableName, null);
                int result = Convert.ToInt32(resultObject);
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "SettingDataprovider.GetRowCount");
                return 0;
            }
        }


        public List<PackageType> GetPackageTypes()
        {
            var result = new List<PackageType>();

            using (var dataReader = _sqlCeHelper.ExecuteDataReader("SELECT * FROM PB_PackageTypes", null))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var packageType = new PackageType
                            {
                                LanguageCode = dataReader.GetString(dataReader.GetOrdinal("LangCode")),
                                Code = dataReader.GetString(dataReader.GetOrdinal("Code")),
                                Text = dataReader.GetString(dataReader.GetOrdinal("Description"))
                            };
                        result.Add(packageType);
                    }
                }
                return result;
            }
        }

        public List<ReasonType> GetReasonTypes(string langCode)
        {
            var result = new List<ReasonType>();

            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_Reasons);
            querystring.Append(" WHERE LangCode = @LangCode");

            var parameters = new IDataParameter[1];
            parameters[0] = _storageBase.CreateParameter("@LangCode", langCode);

            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var reasonType = new ReasonType
                            {
                                LangCode = dataReader.GetString(dataReader.GetOrdinal("LangCode")),
                                Code = dataReader.GetString(dataReader.GetOrdinal("Code")),
                                Description = dataReader.GetString(dataReader.GetOrdinal("Description"))
                            };
                        result.Add(reasonType);
                    }
                }
            }
            return result;
        }

        public List<ActionType> GetActionTypes(string langCode)
        {
            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_Actions);
            querystring.Append(" WHERE  LangCode = @LangCode ");

            var parameters = new IDataParameter[1];
            parameters[0] = _storageBase.CreateParameter("@LangCode", langCode);


            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            var result = new List<ActionType>();
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var actionType = new ActionType
                            {
                                Code = dataReader.GetString(dataReader.GetOrdinal("Code")),
                                LangCode = dataReader.GetString(dataReader.GetOrdinal("LangCode")),
                                Description = dataReader.GetString(dataReader.GetOrdinal("Description"))
                            };
                        result.Add(actionType);
                    }
                }
                return result;
            }
        }

        public List<EventType> GetEventTypes(string langCode)
        {
            var result = new List<EventType>();

            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_EventTypes);
            querystring.Append(" WHERE LangCode = @LangCode ");

            var parameters = new IDataParameter[1];
            parameters[0] = _storageBase.CreateParameter("@LangCode", langCode);

            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var eventType = new EventType
                            {
                                LangCode = dataReader.GetString(dataReader.GetOrdinal("LangCode")),
                                Code = dataReader.GetString(dataReader.GetOrdinal("Code")),
                                Type = dataReader.GetString(dataReader.GetOrdinal("Type"))
                            };
                        result.Add(eventType);
                    }
                }
                return result;
            }
        }

        public List<EventReasonActionType> GetEventReasonActionTypes()
        {
            var result = new List<EventReasonActionType>();

            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_EventReasonAction);


            var parameters = new IDataParameter[0];
            string eventCode = EventCodeUtil.GetEventCodeFromProcess();
            if (eventCode != string.Empty)
            {
                parameters = new IDataParameter[1];
                querystring.Append(" WHERE EventCode = @EventCode ");
                parameters[0] = _storageBase.CreateParameter("@EventCode", eventCode);
            }


            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var eventType = new EventReasonActionType
                            {
                                EventCode = dataReader.GetString(dataReader.GetOrdinal("EventCode")),
                                ReasonCode = dataReader.GetString(dataReader.GetOrdinal("ReasonCode")),
                                ActionCode = dataReader.GetString(dataReader.GetOrdinal("ActionCode"))
                            };
                        result.Add(eventType);
                    }
                }
                return result;
            }
        }

        public List<DamageCode> GetDamageCodes(string langCode)
        {
            var result = new List<DamageCode>();

            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_DamageCodes);

            var parameters = new IDataParameter[1];
            parameters[0] = _storageBase.CreateParameter("@LangCode", langCode);

            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var damageCode = new DamageCode
                            {
                                Code = dataReader.GetString(dataReader.GetOrdinal("Code")),
                                Description = dataReader.GetString(dataReader.GetOrdinal("Description")),
                                LangCode = dataReader.GetString(dataReader.GetOrdinal("LangCode"))
                            };
                        result.Add(damageCode);
                    }
                }
                return result;
            }
        }

        public int GetDeviceSetting(DeviceSettingNames name, int defaultValue)
        {
            try
            {
                return Convert.ToInt16(GetDeviceSetting(name, Convert.ToString(defaultValue)));
            }
            catch (OverflowException)
            {
                Logger.LogEvent(Severity.Warning, "Failed to convert device settingElement.");
                return defaultValue;
            }
        }


        public string GetDeviceSetting(DeviceSettingNames name, string defaultValue)
        {
            var deviceSettings = new List<DeviceSetting>();

            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_DeviceSettings);
            querystring.Append(" WHERE PB_DeviceSettings.Name = @Name AND ");
            querystring.Append(" PB_DeviceSettings.DeviceType = @DeviceType ");

            var parameters = new IDataParameter[2];
            parameters[0] = _storageBase.CreateParameter("@Name", name.ToString());
            string deviceType = DeviceTypeUtil.GetDeviceType().ToString();
            parameters[1] = _storageBase.CreateParameter("@DeviceType", deviceType);

            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var deviceSetting = new DeviceSetting
                            {
                                Value = dataReader.GetString(dataReader.GetOrdinal("Value"))
                            };
                        deviceSettings.Add(deviceSetting);
                    }
                }
            }

            if (deviceSettings.Count == 0)
            {
                Logger.LogEvent(Severity.Warning, "Could not find device settingElement. Returning default.");
                return defaultValue;
            }


            return deviceSettings[0].Value;
        }

        /// <summary>
        /// Convert the string to first letter uppercase and rest all string to lowercase
        /// </summary>
        /// <param name="str">string to Format</param>
        /// <returns>Formatted string</returns>
        public string ToProperCase(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            return Char.ToUpper(str[0]) + str.Substring(1).ToLower();
        }



        public List<CauseMeasure> GetDistinctCombinationForReasonActionTypes(string role, string langCode)
        {
            var result = new List<CauseMeasure>();


            const string querystring = @"SELECT DISTINCT ERA.ReasonCode AS CauseCode, ERA.ActionCode AS MeasureCode, " +
                                       "R.Description AS CauseDescription, A.Description AS MeasureDescription, A.LangCode as LangCode " +
                                       "FROM PB_EventReasonAction AS ERA INNER JOIN PB_Actions AS A ON ERA.ActionCode = A.Code INNER JOIN " +
                                       "PB_Reasons AS R ON ERA.ReasonCode = R.Code WHERE " +
                                       "(ERA.EventCode = @EventCode) AND (ERA.Role = 'Any' or ERA.Role = @Role) " +
                                       " AND (A.LangCode = @LangCode) AND (R.LangCode = @LangCode)"
                                       + " ORDER BY CauseCode, MeasureCode, A.LangCode DESC";
            var parameters = new IDataParameter[3];
            parameters[0] = _storageBase.CreateParameter("@EventCode", EventCodeUtil.GetEventCodeFromProcess());
            parameters[1] = _storageBase.CreateParameter("@Role", role);
            parameters[2] = _storageBase.CreateParameter("@LangCode", langCode);

            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring, parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var eventType = new CauseMeasure
                                            {
                                                CauseCode = dataReader.GetString(dataReader.GetOrdinal("CauseCode")),
                                                MeasureCode = dataReader.GetString(dataReader.GetOrdinal("MeasureCode")),
                                                LangCode = dataReader.GetString(dataReader.GetOrdinal("LangCode")),

                                                MeasureDescription =
                                                    ToProperCase(dataReader.GetString(dataReader.GetOrdinal("MeasureDescription"))),
                                                CauseDescription =
                                                    ToProperCase(dataReader.GetString(dataReader.GetOrdinal("CauseDescription"))),
                                                CauseCodeAndCauseDescription =
                                                    dataReader.GetString(dataReader.GetOrdinal("CauseCode")) + " " +
                                                    ToProperCase(dataReader.GetString(dataReader.GetOrdinal("CauseDescription"))),
                                                MeasureCodeAndMeasureDescription =
                                                    dataReader.GetString(dataReader.GetOrdinal("MeasureCode")) + " " +
                                                    ToProperCase(dataReader.GetString(dataReader.GetOrdinal("MeasureDescription")))
                                            };
                        result.Add(eventType);
                    }
                }
            }


            return result.ToList();
        }

        /// <summary>
        /// Gets the possible Product Group Transitions for a Product Group
        /// </summary>
        public List<ProductGroupTransition> GetProductGroupTransitions(string productGroupCode, string languageCode)
        {
            var result = new List<ProductGroupTransition>();
            const string querystring =
                @"SELECT T.ToCode, P.Description FROM PB_ProductGroupTransitions " +
                "AS T INNER JOIN PB_ProductGroups AS P ON T.ToCode = P.Code " +
                "WHERE (T.FromCode = @ProductGroupCode) AND (P.LangCode = @LangCode)";

            var parameters = new[]
                {
                    _storageBase.CreateParameter("@ProductGroupCode", productGroupCode),
                    _storageBase.CreateParameter("@LangCode", languageCode)
                };

            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring, parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        string productGroup = dataReader.GetString(dataReader.GetOrdinal("ToCode"));
                        string description = dataReader.GetString(dataReader.GetOrdinal("Description"));
                        result.Add(new ProductGroupTransition(productGroup, description));
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Checks if OrgUnit Number is valid
        /// </summary>
        /// <param name="orgUnit"></param>
        /// <returns></returns>
        public string IsValidOrgUnit(string orgUnit)
        {
            var result = new List<OrgUnit>();
            string organisationName = string.Empty;
            const string querystring = @"select * from PB_OrgUnits where Number = @OrgUnit";
            var parameters = new IDataParameter[1];
            parameters[0] = _storageBase.CreateParameter("@OrgUnit", orgUnit);
            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring, parameters))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        organisationName = dataReader.GetString(dataReader.GetOrdinal("Name"));
                        var orgUnitNames = new OrgUnit
                            {
                                OrgNumber = dataReader.GetString(dataReader.GetOrdinal("Number")),
                                OrgName = dataReader.GetString(dataReader.GetOrdinal("Name")),
                                CountryCode = dataReader.GetString(dataReader.GetOrdinal("CountryCode")),
                                LocationId = dataReader.GetString(dataReader.GetOrdinal("LocationIdent")),
                            };
                        result.Add(orgUnitNames);
                    }
                }
                return organisationName;
            }
        }

        public static bool IsFixedClosingDate(string countryCode)
        {
            var result = false;
            var now = DateTime.Now;
            const string querystring =
                @"select * from PB_FixedClosingDates where PB_FixedClosingDates.Date = @date AND PB_FixedClosingDates.CountryCode = @countryCode";
            var parameters = new IDataParameter[2];
            parameters[0] = _storageBase.CreateParameter("@Date", now.Date);
            parameters[1] = _storageBase.CreateParameter("@CountryCode", countryCode);
            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring, parameters))
            {
                if (dataReader != null)
                {
                    if (dataReader.Read())
                    {
                        result = true;
                    }
                }
                return result;
            }
        }

        public static bool IsWithinLimitedOpeningHours(string countryCode, out bool isDateFound)
        {
            var result = false;
            isDateFound = false;
            var now = DateTime.Now;

            const string querystring =
                @"select * from PB_LimitedOpeningHours where PB_LimitedOpeningHours.Date = @date AND PB_LimitedOpeningHours.CountryCode = @countryCode";
            var parameters = new IDataParameter[2];
            //var date = now.Date.AddMonths(3);
            parameters[0] = _storageBase.CreateParameter("@Date", now.Date);
            // parameters[0] = _storageBase.CreateParameter("@Date", date);
            parameters[1] = _storageBase.CreateParameter("@CountryCode", countryCode);
            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring, parameters))
            {
                if (dataReader != null)
                {
                    if (dataReader.Read())
                    {
                        try
                        {
                            isDateFound = true;
                            var startTime = TimeSpan.Parse(dataReader["StartTime"].ToString());
                            var endTime = TimeSpan.Parse(dataReader["EndTime"].ToString());
                            if (startTime < now.TimeOfDay && endTime > now.TimeOfDay)
                                result = true;
                        }
                        catch (Exception)
                        {
                            result = false;
                        }

                    }
                }
                return result;
            }
        }

        public static bool IsWithinDefaultOpeningHours(string countryCode)
        {
            var result = false;
            var now = DateTime.Now;
            var dayIndex = (int)now.DayOfWeek;
            var time = now.TimeOfDay;

            const string querystring =
                @"Select * from PB_OpeningHours WHERE DayIndex = @dayIndex AND CountryCode = @countryCode";
            var parameters = new IDataParameter[2];
            parameters[0] = _storageBase.CreateParameter("@DayIndex", dayIndex);
            parameters[1] = _storageBase.CreateParameter("@CountryCode", countryCode);
            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring, parameters))
            {
                if (dataReader != null)
                {
                    if (dataReader.Read())
                    {
                        try
                        {
                            var startTime = TimeSpan.Parse(dataReader["StartTime"].ToString());
                            var endTime = TimeSpan.Parse(dataReader["EndTime"].ToString());
                            if (startTime < time && endTime > time)
                                result = true;
                        }
                        catch (Exception)
                        {
                            result = false;
                        }
                    }
                }
            }
            return result;
        }

        public static List<VasDetail> GetVasTypes()
        {
            var result = new List<VasDetail>();
            var querystring = new StringBuilder();
            querystring.Append("SELECT * FROM ");
            querystring.Append(TableName.PB_VasTypes);
            Logger.LogEvent(Severity.Debug, "Execute query: " + querystring);
            using (var dataReader = _sqlCeHelper.ExecuteDataReader(querystring.ToString(), null))
            {
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        var vasDetail = new VasDetail
                        {
                            VasId = dataReader.GetString(dataReader.GetOrdinal("Code")),
                            VasMeasure = dataReader.GetString(dataReader.GetOrdinal("Measure")),
                            VasType = dataReader.GetString(dataReader.GetOrdinal("Text"))
                        };
                        result.Add(vasDetail);
                    }
                }
                return result;
            }
        }



    }
}







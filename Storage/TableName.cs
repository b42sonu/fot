﻿namespace Com.Bring.PMP.PreComFW.Shared.Storage
{
    // Because of some unittests the numeric values in this enum should not be changed. Adds and (most) deletes are ok.
    // PB_Devicesettings and PB_OpeningHours must not be deleted without changing the tests in MasterDataTableStatusResponseTest.cs
    public enum TableName
    {
        // ReSharper disable InconsistentNaming
        PB_Settings = 0,
        PB_OrgUnits = 1,
        PB_Profile = 2,
        PMC_Communication_LoginCredentials = 3,
        PB_TMISettings = 4,
        PB_EventTypes = 5,
        PB_Reasons = 6,
        PB_Actions = 7,
        PB_EventReasonAction = 8,
        PB_DamageCodes = 9,
        PB_DeviceSettings = 10,
        PB_ProductGroups = 11,
        PB_ProductGroupTransitions = 12,
        PB_VasMatrixSettings = 13,
        PB_VasMatrixVasCodes = 14,
        PB_VasMatrixProductCodes = 15,
        PB_VasMatrixRoles = 16,
        PB_VasMatrixMessages = 17,
        PB_VasMatrixTmsNames = 18,
        PB_VasMatrixWorkProcesses = 19,
        PB_Help = 20,
        PB_LimitedOpeningHours = 21,
        PB_OpeningHours = 22,
        PB_FixedClosingDates = 23,
        PB_Messages = 24,
        PB_MessageStatus = 25,
        PB_Roles = 26,
        PB_VasTypes = 27,

        // ReSharper restore InconsistentNaming
    }
}

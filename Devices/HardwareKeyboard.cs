﻿using System;
using Com.Bring.PMP.PreComFW.Shared.Model;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Devices
{
    public enum KeyboardState
    {
        Unknown,
        Alpha,
        Numeric,
        AlphaLowerCase,
        AlphaUpperCase,

    }

    public interface IHardwareKeyboard
    {
        void SetState(KeyboardState state);
    }

    public static class HardwareKeyboard
    {
        public static EventHandler OnEnterKeyPressed { get; private set; }
        private static bool _executing;
        public static readonly object Lock = new object();
        public static void ClearEnterKeyPressedSubscription()
        {
            OnEnterKeyPressed = null;
        }

        public static void SubscribeEnterKeyPressed(EventHandler onEnterKeyPressed)
        {
            OnEnterKeyPressed = onEnterKeyPressed;
        }

        public static void FireEnterPressed()
        {
            try
            {
                if (OnEnterKeyPressed != null && _executing == false)
                {
                    lock (Lock)
                    {
                        _executing = true;
                        OnEnterKeyPressed(null, null);
                        _executing = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex, "HardwareKeyboard.FireEnterPressed");
            }
        }

        public static bool IsExecuting
        {
            get { return _executing; }
            set { _executing = value; }
        }

        private static IHardwareKeyboard _instance;
        public static IHardwareKeyboard Instance
        {
            get { return _instance ?? (_instance = ModelModules.Devices.Get<IHardwareKeyboard>()); }
        }
    }
}
﻿using System;
using System.Runtime.InteropServices;
using Com.Bring.PMP.PreComFW.Shared.Utility;

namespace Com.Bring.PMP.PreComFW.Shared.Devices
{
    public class KeysApi
    {
        private static IntPtr _hDefMessageWindow;

        [DllImport("coredll.dll", SetLastError = true)]
        public static extern bool RegisterHotKey(
            IntPtr hWnd, // handle to window
            int id, // hot key identifier
            KeyModifiers modifiers, // key-modifier options
            int key //virtual-key code
            );

        // Undocumented function to unregister keypress
        [DllImport("coredll.dll")]
        private static extern bool UnregisterFunc1(KeyModifiers modifiers,int keyId);

        [DllImport("coredll.dll")]
        public static extern short GetAsyncKeyState(int keyCode);


        public static void RegisterEnterKey()
        {
            if(_hDefMessageWindow != IntPtr.Zero)
                RegisterEnterKey(_hDefMessageWindow);
            else
                Logger.LogEvent(Severity.Error, "Tried to register enter key without default handle");
        }

        public static void RegisterEnterKey(IntPtr hWnd)
        {
            UnregisterFunc1(KeyModifiers.None, (int)KeysHardware.VK_RETURN);
            RegisterHotKey(hWnd, (int)KeysHardware.VK_RETURN, KeyModifiers.None, (int)KeysHardware.VK_RETURN);
            _hDefMessageWindow = hWnd;
        }


        public static void RegisterKeys(IntPtr hWnd, KeyModifiers modifiers, KeysHardware keysHardware)
        {
            UnregisterFunc1(modifiers, (int)keysHardware);
            RegisterHotKey(hWnd, (int)keysHardware, modifiers, (int)keysHardware);
        }

        public static void UnregisterEnterKey()
        {
            UnregisterFunc1(KeyModifiers.None, (int)KeysHardware.VK_RETURN);
        }

        public static void UnregisterKey(KeyModifiers keymodifier, int id)
        {
            UnregisterFunc1(keymodifier, id);
        }
    }

    public enum KeysHardware
    {
        // ReSharper disable InconsistentNaming
        VK_RETURN = 0x0D,
        VK_F1 = 0x70,
        VK_F2 = 0x71,
        VK_F3 = 0x72,
        VK_F4 = 0x73,
        VK_F5 = 0x74,
        VK_F6 = 0x75,
        VK_F7 = 0x76,
        VK_F8 = 0x77,
        VK_F9 = 0x78,
        VK_F10 = 0x79,
        VK_F11 = 0x7A,
        VK_F12 = 0x7B,
        VK_TSOFT1 = VK_F1, // Softkey 1
        VK_TSOFT2 = VK_F2, // Softkey 2
        VK_TTALK = VK_F3, // Talk = Green Phone Button
        VK_TEND = VK_F4, // Reset = Red Phone Button
        VK_APP1 = 0xC1, // up to 6 other hardware buttons
        VK_APP2 = 0xC2,
        VK_APP3 = 0xC3,
        VK_APP4 = 0xC4,
        VK_APP5 = 0xC5,
        VK_APP6 = 0xC6,
        VK_F22 = 0x85,
        VK_MULTIPLY = 0x6A,
        VK_LSHIFT = 0xA0,
        VK_NUMPAD0 = 0x60,
        VK_NUMPAD1 = 0x61,
        VK_1 = 0x31,
        

        RedPhoneButton = VK_TEND,
        GreenPhoneButton = VK_TTALK
        // ReSharper restore InconsistentNaming
    }

    [Flags]
    public enum KeyModifiers
    {
        None = 0,
        Alt = 1,
        Control = 2,
        Shift = 4,
        Windows = 8,
        Modkeyup = 0x1000,
    }

}
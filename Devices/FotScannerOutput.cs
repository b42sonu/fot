﻿using System;
using Com.Bring.PMP.CSharp;
using PreCom.Core.Communication;
using PreCom.MeasurementCore;
using Symbol.Barcode;

namespace Com.Bring.PMP.PreComFW.Shared.Devices
{
    public class FotScannerOutput
    {
        public FotScannerOutput()
        {
            Timing = new Timing { Type = TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Received, TimingComponent.User) };
            Transaction = new Transaction(Guid.NewGuid(), 0, Code);
        }

        public FotScannerOutput(DecoderTypes type, string code)
        {
            Type = type;
            Code = code;
            Timing = new Timing { Type = TimingMeasurement.CalculateTimingCategoryValue(TimingComponent.Client, TimingType.Received, TimingComponent.User) };
            Transaction = new Transaction(Guid.NewGuid(), 0, Code);
        }

        public string Code;
        public DecoderTypes Type;

        public Timing Timing { get; set; }
        public Transaction Transaction { get; set; }


    }
}

using System;
using Com.Bring.PMP.PreComFW.Shared.Model;

namespace Com.Bring.PMP.PreComFW.Shared.Devices
{
    public delegate void FotCodeScannedDelegate(FotScannerOutput output);

#if PROFILING
    public delegate void ProfilingDelegate();
#endif

    public interface IFotScanner
    {
        void Activate(FotCodeScannedDelegate scannerEventHandler);
        void Deactivate();
        void Suspend();
        void Resume();
        
        bool SetScannerType(ScannerType scannerType);
        ScannerType GetScannerType();

        bool IsActive { get; }

        event Action ScanEvent;

#if PROFILING
        void SetBarcodeScannedProfilingHandler(ProfilingDelegate tunerEventHandler);
#endif

    }

    public static class FotScanner
    {
        private static IFotScanner _instance;
        public static IFotScanner Instance
        {
            get { return _instance ?? (_instance = ModelModules.Devices.Get<IFotScanner>()); }
        }
    }

    public enum ScannerType
    {
        Internal,
        External,
        None
    }
}